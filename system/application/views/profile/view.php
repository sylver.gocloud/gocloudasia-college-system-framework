<?php @$this->load->view('layouts/_student_data'); ?>

<div class="tab-content">
	<p></p>
	<div class="btn-group btn-group-sm">
	  <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown">
	    <span class="entypo-print"></span> &nbsp; Registration Form <span class="caret"></span>
	  </button>
	  <ul class="dropdown-menu" role="menu">
	    <li><a target="_blank" href="<?= base_url('printables/registration_form/'. $enrollment_id.'/student'); ?>">Student's Copy</a></li>
	    <?if($this->session->userdata('userType') == "registrar"):?>
	    	<li><a target="_blank" href="<?= base_url('printables/registration_form/'. $enrollment_id.'/registrar'); ?>">Registrar's Copy</a></li>
	    <?elseif($this->session->userdata('userType') == "finance"):?>
	    	<li><a target="_blank" href="<?= base_url('printables/registration_form/'. $enrollment_id.'/finance'); ?>">Finance's Copy</a></li>
	    <?endif;?>
	  </ul>
	</div>
  <a target='_blank' href="<?= base_url('printables/certificate_of_enrollment/'. $enrollment_id); ?>" class="btn btn-warning btn-sm"><span class="glyphicon glyphicon-print"></span> Certificate of Enrollment</a>
	<a target='_blank' href="<?= base_url('printables/certificate_of_grade/'. $enrollment_id); ?>" class="btn btn-default btn-sm btn-sm"><span class="glyphicon glyphicon-print"></span> Certificate of Grade</a>
	<p></p>
	
	<?if($mydepartment->stud_edit_profile === "1"):?>
	<?php echo form_open('', ' id = "form_enrollment" class = "confirm"') ?><?endif;?>
	<?php @$this->load->view('layouts/student_data/_mavc_enrollment_form'); ?>
	<?if($mydepartment->stud_edit_profile === "1"):?>
		<p></p>
		<div style="padding:15px">
			<div class="pull-right">
				<input type="hidden" name="enrollment_id" value="<?php echo $enrollment_id;?>" />
				<input type="hidden" name="form_token" value="<?php echo $form_token;?>" />
				<input type="submit" name="update_data" value="Update Information"  />
			</div>
		</div>
		<br/>
		<?php echo form_close(); ?>
	<?endif;?>
</div>