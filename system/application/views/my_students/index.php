<form action="<?=site_url('my_students');?>/index/0" method="GET" class="form-inline" >
	
	
		<div class="form-group">
			<input class='form-control' id="studid" name="studid" size="30" type="text" value="<?=$studid?>" placeHolder="Student Id" />	
		</div>

		<div class="form-group">
			<input class='form-control' id="lastname" name="lastname" size="30" type="text"  value="<?=$lastname?>" placeHolder="Lastname" />
		</div>

		<div class="form-group">
			<input class='form-control' id="firstname" name="firstname" size="30" type="text" value="<?=$firstname?>" placeHolder="Firstname" />
		</div>

		<div class="form-group">
			<input class='form-control' id="middle" name="middle" size="30" type="text" value="<?=$middlename?>" placeHolder="Middlename" />
		</div>

		<div class="form-group">
			<?if($years):?>
				<?=form_dropdown('year_id', $years, $year_id);?>
			<?endif;?>
		</div>

		<div class="form-group">
			<button class="btn btn-sm btn-success" type="submit" name="search_students" value="search_students" ><i class="fa fa-search"></i>&nbsp; Search</button>
		</div>
	

</form>

<?php if(!empty($results)):?>
  <?= $links ;?>	
<?php endif;?>
<br/>
<div class="table-responsive">
<table class="paginated table table-hover table-padless">
	<thead>
	  <tr>
	    <th>Student ID</th>
	    <th>Fullname</th>
	    <th>Course Code</th>
	    <th>Year Level</th>
	    <th>Action</th>
	  </tr>
  </thead>
  
    
		<?php if(!empty($results)):?>
			<?php foreach($results as $s):?>
				<tr>
					 <td><?php echo $s->studid;?></td>
					  <td><?php echo $s->name;?></td>
					  <td><?php echo $s->course_code;?></td>
					  <td><?php echo $s->year;?></td>
					  <td>
					  	<?if($mydepartment->stud_grade === "1"):?>
						  	<div class="btn-group">
						  		<a href="<?=site_url('my_students/subjects/'.$s->id)?>" class="btn btn-xs btn-primary"><span class="entypo-book"></span>&nbsp; Subjects</a>
						  		<a target="_blank" href="<?=site_url('my_students/print_subjects/'.$s->id)?>" class="btn btn-xs btn-default"><span class="entypo-print"></span>&nbsp;</a>
						  	</div>
					  	<?else:?>
				  			-- No Access --
				  		<?endif;?>
					  </td>
				</tr>
			<?php endforeach;?>
		<?php else:?>
				<tr>
					<td><p>No Student Available</p></td>
				</tr>
		<?php endif;?>
</table>	
<?php if(!empty($results)):?>
  <?= $links ;?>	
<?php endif;?>
</div>

