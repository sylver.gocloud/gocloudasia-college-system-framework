<div id="enroll1">
	  <?=isset($system_message) ? $system_message : NULL;?>
	  <div id="enrollment_required_info">
        <p>All inputs with 
        <span>*</span> sign are required fields.</p>
      </div>
      <form action="<?=site_url('enrollment/new_student');?>" method="post">
        <div id="enrollment_student">
          <strong>Student Enrollment Data</strong>
          <hr />
          <div id="student_enrollment">
            <div class='f1'>
              <div>
				<label for="user_enrollments_attributes_0_sy_from">A.Y. :</label><strong><i><?=$this->header->academic_year;?></i></strong>
			  </div>
            </div>
            <div class="clear"></div>
            <div class="f2">
              <div>
				<label for="user_enrollments_attributes_0_studid">Login ID:</label><br>
				<input type="text"value="<?=$id;?>" disabled/><?=form_error('profile[middle_name]');?>
				<input type="hidden" name="login" value="<?=$id;?>">
			  </div>
              <div>
              <label for="user_enrollments_attributes_0_status">Status:</label><br>
				<input value="NEW" type="text" disabled/>
			  </div>
            </div>
            <div class="clear"></div>
            <div class="f2">
				  <div>
					<label for="course">Course:</label><br>
					<?=course_dropdown('course',set_value('course'));?><?=form_error('course');?>
				  </div>
              <div>
              <label for="year">Year Level:</label><br>
				<?=year_dropdown('year',set_value('year'));?><?=form_error('year');?>
			 </div>
            </div>
            <div class="clear"></div>
          </div>
          <strong>Student Personal Data</strong><hr/>
        <div class="f3">
			<div>
				<label>Last Name:</label><span class="req">*</span><br>
				<input name="profile[last_name]" value="<?=set_value('profile[last_name]',NULL);?>" size="25" type="text" required/><?=form_error('profile[last_name]');?>	
			</div>
			<div>
				<label>First Name:</label><span class="req">*</span><br>
				<input name="profile[first_name]" size="25" value="<?=set_value('profile[first_name]')?>" type="text" required/> <?=form_error('profile[first_name]');?>
			</div>
			<div>
				<label>Middle Name:</label><span class="req">*</span><br>
				<input name="profile[middle_name]" size="25" value="<?=set_value('profile[middle_name]')?>" type="text" required/><?=form_error('profile[middle_name]');?>
			</div>
		</div>
          <div class="clear"></div>
          <div class="f3">
            <div>
				<label for="Gender">Gender:</label><span class="req">*</span><br>
				<?
				$gender = array(''=>'Gender','Male'=>'Male','Female'=>'Female');
				echo form_dropdown('profile[gender]',$gender,set_value('profile[gender]'));
				?>
				<?=form_error('profile[gender]');?>
			</div>
			
			<div>
				<label for="civil_status">Civil Status:</label><span class="req">*</span><br>
			<?
				$civil_status = array(''=>'Civil Status','Single'=>'Single','Married'=>'Married','Widowed'=>'Widowed','Divorced'=>'Divorced');
				echo form_dropdown('profile[civil_status]',$civil_status,set_value('profile[civil_status]'));
			?>
			<?=form_error('profile[civil_status]');?>
			</div>
          </div>
          <div class="clear"></div>
          <div class="f4">
            <div>
            <label for="date_of_birth">Date of Birth:</label><span class="req">*</span><br>
            <input value="<?=set_value('profile[date_of_birth]');?>" name="profile[date_of_birth]" size="10"type="text" required/><?=form_error('profile[date_of_birth]');?>
            </div>
			<div>
				<label for="birth_place">Birth Place:</label><span class="req">*</span><br>
				<input value="<?=set_value('profile[place_of_birth]');?>" name="profile[place_of_birth]"size="20" type="text" required/><?=form_error('profile[place_of_birth]');?>
			</div>
			<div>
				<label for="age">Age:</label><span class="req">*</span><br>
				<input value="<?=set_value('profile[age]');?>"name="profile[age]" size="2"type="text" required/><?=form_error('profile[age]');?>
			</div>
            <div>
				<label for="disability">Disability:</label><br>
				<input value="<?=set_value('profile[disability]');?>" name="profile[disability]" size="10" type="text" /><?=form_error('profile[disability]');?>
			</div>
          </div>
          <div class="clear"></div>
          <div class="f4">
            <div>
				<label for="nationality">Nationality:</label><br>
				<input value="<?=set_value('profile[nationality]');?>" name="profile[nationality]" size="20" type="text" /></div><?=form_error('profile[nationality]');?>
            <div>
				<label for="religion">Religion:</label><span class="req">*</span><br>
				<input value="<?=set_value('profile[religion]');?>" name="profile[religion]" size="20"type="text" /><?=form_error('profile[religion]');?>
			</div>
            <div>
				<label for="mobile">Contact Number:</label><br>
				<input value="<?=set_value('profile[mobile]');?>" name="profile[mobile]" size="20"type="text" /><?=form_error('profile[mobile]');?>
			</div>
            <div>
				<label for="email">Email:</label><br>
				<input value="<?=set_value('profile[email]');?>" name="profile[email]" type="email" /><?=form_error('profile[email]');?>
			</div>
          </div>
		  
		  <div class="clear"></div>
		  <div class="f3">
			<div>
				<label for="present_address">Present Address:</label><span class="req">*</span><br>
				<input value="<?=set_value('profile[present_address]');?>"name="profile[present_address]" size="40" type="text" /><?=form_error('profile[present_address]');?>
			</div>
          </div>
          <div class="clear"></div>
          <div class="f3">
            <div>
            <label for="fathers_name">Father&#39;s Name:</label><span class="req">*</span><br>
            <input value="<?=set_value('profile[father_name]');?>" name="profile[father_name]"size="20" type="text"required /><?=form_error('profile[father_name]');?>
			</div>
            <div>
            <label for="father_occupation">Occupation:</label><span class="req">*</span><br>
            <input value="<?=set_value('profile[father_occupation]');?>" name="profile[father_occupation]" size="20" type="text" required/><?=form_error('profile[father_occupation]');?>
            </div>
			<div>
            <label for="father_contact_number">Contact Number:</label><span class="req">*</span><br>
            <input value="<?=set_value('profile[father_contact_no]');?>" name="profile[father_contact_no]" size="15" type="text" required/><?=form_error('profile[father_contact_no]');?>
			</div>
		  </div>
          <div class="clear"></div>
          <div class="f3">
            <div>
            <label for="mothers_name">Mother&#39;s Name:</label><span class="req">*</span><br>
            <input  value="<?=set_value('profile[mother_name]');?>" name="profile[mother_name]"size="20" type="text" required/><?=form_error('profile[mother_name]');?>
			</div>
            <div>
            <label for="mothers_occupation">Occupation:</label><span class="req">*</span><br>
            <input value="<?=set_value('profile[mother_occupation]');?>" name="profile[mother_occupation]" size="20" type="text" required/><?=form_error('profile[mother_occupation]');?>
			</div>
            <div>
            <label for="mother_contact_number">Contact Number:</label><span class="req">*</span><br>
            <input value="<?=set_value('profile[mother_contact_no]');?>" name="profile[mother_contact_no]" size="15" type="text" required/><?=form_error('profile[mother_contact_no]');?>
			</div>
          </div>
          <div class="clear"></div>
          <div class="f1">
            <div>
				<label for="parents_address">Parent&#39;s Address:</label><span class="req">*</span><br>
				<input value="<?=set_value('profile[parents_address]');?>" name="profile[parents_address]" size="40" type="text" required/><?=form_error('profile[parents_address]');?>
			</div>
          </div>
          <div class="f3">
            <div>
				<label for="guardian_name">Guardian Name:</label><span class="req">*</span><br> 
				<input value="<?=set_value('profile[guardian_name]');?>" name="profile[guardian_name]"size="20" type="text" required/><?=form_error('profile[guardian_name]');?>
			</div>
            <div>
				<label for="guardian_relation">Relationship:</label><br>
				<input name="profile[guardian_relation]" value="<?=set_value('profile[guardian_relation]');?>" size="20" type="text" required/><?=form_error('profile[guardian_relation]');?>
			</div>
            <div>
				<label for="guardian_contact_number">Contact Number:</label><span class="req">*</span><br> 
				<input name="profile[guardian_contact_no]" value="<?=set_value('profile[guardian_contact_no]');?>"size="15" type="text" required/><?=form_error('profile[guardian_contact_no]');?>
			</div>
          </div>
          <div class="clear"></div>
          <div class="f1">
            <div>
				<label for="guardian_address">Address of Guardian:</label><span class="req">*</span><br>
				<input name="profile[guardian_address]" value="<?=set_value('profile[guardian_address]');?>"size="50" type="text" required/><?=form_error('profile[guardian_address]');?>
			</div>
          </div>
          <div id="std3">
          <strong>Academic Data</strong>
          <hr />
          <table>
            <tr>
              <td>
                <label for="elementary"><strong>Elementary:</strong></label>
                <br />
              </td>
              <td> </td>
            </tr>
            <tr>
              <td>
              <span class="req">*</span>Name of School:</td>
              <td>
                <input value="<?=set_value('profile[elementary]');?>" name="profile[elementary]"size="50" type="text"required /><?=form_error('profile[elementary]');?>
              </td>
            </tr>
            <tr>
              <td>
              <span class="req">*</span>Address of School:</td>
              <td>
                <input value="<?=set_value('profile[elementary_address]');?>" name="profile[elementary_address]" size="50" type="text"required /><?=form_error('profile[elementary_address]');?>
              </td>
            </tr>
            <tr>
              <td>
              <span class="req">*</span>Year Graduated/ Year Last Attended:</td>
              <td>
                <input value="<?=set_value('profile[elementary_date]');?> "name="profile[elementary_date]"size="50" type="text" required/><?=form_error('profile[elementary_date]');?>
              </td>
            </tr>
            <tr>
              <td>
                <label><strong>Secondary:</strong></label>
                <br />
              </td>
            </tr>
            <tr>
              <td>
              <span class="req">*</span> Name of School:</td>
              <td>
                <input value="<?=set_value('profile[secondary]');?>" name="profile[secondary]" size="50" type="text" required/><?=form_error('profile[secondary]');?>
              </td>
            </tr>
            <tr>
              <td>
              <span class="req">*</span>Address of School:</td>
              <td>
                <input value="<?=set_value('profile[secondary_address]');?>" name="profile[secondary_address]" size="50" type="text" required/><?=form_error('profile[secondary_address]');?>
              </td>
            </tr>
            <tr>
              <td>
              <span class="req">*</span>Year Graduated/ Year Last Attended:</td>
              <td>
                <input value="<?=set_value('profile[secondary_date]');?>" name="profile[secondary_date]"size="50" type="text" required/><?=form_error('profile[secondary_date]');?>
              </td>
            </tr>
            <tr>
              <td>
                <label><strong>Vocational:</strong></label>
              </td>
            </tr>
            <tr>
              <td>Name of School:</td>
              <td>
                <input value="<?=set_value('profile[vocational]');?>" name="profile[vocational]" size="50" type="text" /><?=form_error('profile[vocational]');?>
              </td>
            </tr>
            <tr>
              <td>Address of School:</td>
              <td>
                <input value="<?=set_value('profile[vocational_address]');?>" name="profile[vocational_address]" size="50" type="text" /><?=form_error('profile[vocational_address]');?>
              </td>
            </tr>
            <tr>
              <td>Degree or Course:</td>
              <td>
                <input value="<?=set_value('profile[vocational_degree]');?>" name="profile[vocational_degree]" size="50" type="text" /><?=form_error('profile[vocational_degree]');?>
              </td>
            </tr>
            <tr>
              <td>Year Graduated/ Year Last Attended:</td>
              <td>
                <input value="<?=set_value('profile[vocational_date]');?>" name="profile[vocational_date]" size="50" type="text" /><?=form_error('profile[vocational_date]');?>
              </td>
            </tr>
            <tr>
              <td>
                <label><strong>Tertiary:</strong></label>
                <br />
              </td>
            </tr>
            <tr>
              <td>Name of School:</td>
              <td>
                <input value="<?=set_value('profile[tertiary]');?>" name="profile[tertiary]" size="50" type="text" /><?=form_error('profile[tertiary]');?>
              </td>
            </tr>
            <tr>
              <td>Address of School:</td>
              <td>
                <input value="<?=set_value('profile[tertiary_address]');?>" name="profile[tertiary_address]" size="50" type="text" /><?=form_error('profile[tertiary_adress]');?>
              </td>
            </tr>
            <tr>
              <td>Degree or Course:</td>
              <td>
                <input value="<?=set_value('profile[tertiary_degree]');?>" name="profile[tertiary_degree]" size="50" type="text" /><?=form_error('profile[tertiary_degree]');?>
              </td>
            </tr>
            <tr>
              <td>Year Graduated/ Year Last Attended:</td>
              <td>
                <input value="<?=set_value('profile[tertiary_date]');?>" name="profile[tertiary_date]" size="50" type="text" /><?=form_error('profile[tertiary_date]');?>
              </td>
            </tr>
            <tr>
              <td>
                <label><strong>Others:</strong></label>
                <br />
              </td>
            </tr>
            <tr>
              <td>Name of School:</td>
              <td>
                <input value="<?=set_value('profile[others]');?>" name="profile[others]" size="50" type="text" /><?=form_error('profile[others]');?>
              </td>
            </tr>
            <tr>
              <td>Address of School:</td>
              <td>
                <input value="<?=set_value('profile[others_address]');?>" name="profile[others_address]" size="50" type="text" /><?=form_error('profile[others_address]');?>
              </td>
            </tr>
            <tr>
              <td>Degree or Course:</td>
              <td>
                <input value="<?=set_value('profile[others_degree]');?>" name="profile[others_degree]" size="50" type="text" /><?=form_error('profile[others_degree]');?>
              </td>
            </tr>
            <tr>
              <td>Year Graduated/ Year Last Attended:</td>
              <td>
                <input value="<?=set_value('profile[others_date]');?>" name="profile[others_date]" size="50" type="text" /><?=form_error('profile[others_date]');?>
              </td>
            </tr>
			<tr>
				<td>Verify You are Human</td>
				<td>
					<script type="text/javascript"src="http://www.google.com/recaptcha/api/challenge?k=6Ledwd0SAAAAAC04UI9Kix2lXE75QWtZVo9InLyE"></script>
					<noscript>
						<iframe src="http://www.google.com/recaptcha/api/noscript?k=6Ledwd0SAAAAAC04UI9Kix2lXE75QWtZVo9InLyE"height="300" width="500" frameborder="0"></iframe><br>
						<textarea name="recaptcha_challenge_field" rows="3" cols="40" required></textarea>
						<input type="hidden" name="recaptcha_response_field" value="manual_challenge">
					</noscript>				
				</td>
			</tr>
          </table>
          <div class="clear"></div>

          <p>
            <input class="submit" id="user_submit" name="start_enrollment" type="submit" value="Submit" />
          </p>
		  <p>
            <a href="<?=site_url('enrollment/reset');?>" class="confirm" title="Are You sure you want to reset Enrollment process?">Reset</a>
          </p>
        </div>
      </form>
    </div>
	