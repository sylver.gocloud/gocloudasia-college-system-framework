<?php echo panel_head2('Upload',false,'primary') ?>
	<div class="alert alert-info">
	<p><i class="fa fa-info-circle"></i>&nbsp; Note! <a href="<?=site_url('master_subjects/download_master_subject_csv_format/'.__link(143))?>" target="_blank" class="bold text-danger">Download</a> and used this <a target="_blank" href="http://en.wikipedia.org/wiki/Comma-separated_values">CSV</a> format. Open and edit the downloaded file in <a target="_blank" href="http://en.wikipedia.org/wiki/Microsoft_Excel">Excel</a>.</p>
	</div>
	
	<?=isset($system_message)?$system_message:'';?>
	
	<div class="large-9 small-12 columns">
		<div class="upload">
			
			<ol type='1'>
				<li>Must be in <code>'Comma Separated Values'</code> file extension.</li>
				<li>File content must follow the exact format being downloaded above.</li>
				<li>Must not use <code>comma</code> because it serves as the separator of every field.</li>
				<li>Duplicate subject will not be added.</li>
				<li>All fields must be filled out. Incomplete row will not be added.</li>
				<li>Course No. must be unique. Duplicate will not be added.</li>
				<li>If no lec or lab unit put zero.</li>
			</ol>

		<?php echo form_open_multipart('');?>
			<input type="file" name="userfile" size="1000" required class='btn btn-default' />
			<input type="hidden" name="form_token" value="<?=$token?>">
			<p></p>
			<button class="btn btn-primary btn-sm" type="submit" name="upload_csv" value="upload_csv" ><i class="fa fa-upload"></i>&nbsp; Upload File</button>
			<a class='btn btn-sm btn-default' href="<?php echo site_url('master_subjects') ?>">Cancel</a>
		</form>
		</div>
	</div>
<?@panel_tail2();?>