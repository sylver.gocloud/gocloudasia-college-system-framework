<script type='text/javascript'>
	$( document ).ready(function() {
		$('.ui-widget-overlay').live("click",function(){
            $("#message").dialog("close");
        });  

		$('.units').keyup(function(event){
			var xlab = $('#txt_lab').val();
			var xlec = $('#txt_lec').val();

			xlab = isNumber(xlab) ? xlab : 0;
			xlec = isNumber(xlec) ? xlec : 0;

			var xunits = parseFloat(xlab) + parseFloat(xlec);

			xunits == isNumber(xunits) ? xunits : 0;

			$('#txt_units').val(xunits);
		})
		
		$('.not_blank').each(function(){
		
			var xdata = $(this).val().trim();
			
			if(xdata == "")
			{
				$(this).css('border-color','red');
			}
			
			$(this).focusout(function(){
				
				var xdata = $(this).val().trim();
				
				if(xdata != ""){
					$(this).css('border-color','gray');
				}else{
					
					$(this).css('border-color','red');
				}
			});
		})

	});
  
	function validate()
	{
		var ctr = 0;
		var msg = "<ul>";
		var xfocus = "";
		$('.not_blank').css('border-color','gray');
		$('.not_blank').each(function(){
			var xval = $(this).val().trim();
			if(xval == ""){
				ctr++;
				$(this).css('border-color','red');
				if(ctr == 1) { xfocus = $(this); }
				var label = $(this).parent().prev().text();
				var name = $(this).attr('name');
				
					msg += "<li>"+label+" should not be blank</li>";
			}
		});
		
		msg += "<ul>";
		
		if(ctr > 0)
		{
			xfocus.focus();
			
			custom_modal('Required', msg);
			
			return false;
		}
		else
		{
			return true;
		}
	}
	
	function CloseBlock(){
		$.unblockUI();
	}
</script>

<div class="row">
  <div class="col-md-3"><label for="code">Course No. *</label></div>
  <div class="col-md-4">
	<?php 
		$input = array(
              'name'        => 'subjects[code]',
              'placeHolder' => 'Course No. / Code',
              'value'       => set_value('subjects[code]',isset($subjects->code) ? $subjects->code : ''),
              'maxlength'   => '50',
			  			'class'       => 'not_blank',
			  			'required'	=> true				
            );

		echo form_input($input);
	?>
  </div>
</div>
<p></p>

<div class="row">
  <div class="col-md-3"><label for="subject">Description *</label></div>
  <div class="col-md-5">
	<?php 
		$input = array(
              'name'        => 'subjects[subject]',
              'placeHolder' => 'Subject Name',
              'value'       => set_value('subjects[subject]',isset($subjects->subject) ? $subjects->subject : ''),
              'maxlength'   => '150',
						  'class'       => 'not_blank',
						  'required'	=> true			
            );
		echo form_input($input);
	?>
  </div>
</div>
<p></p>

<div class="row">
  <div class="col-md-3"><label for="units">No. of Units *</label></div>
  <div class="col-md-3">
	<?php 
		$input = array(
              'name'        => 'subjects[units]',
              'value'       => set_value('subjects[units]',isset($subjects->units) ? $subjects->units : ''),
              'maxlength'   => '4',
			  			'class'       => '',
			  			'id'					=> 'txt_units',
			  			'required'		=> true,	
			  			'readonly'		=> true	
            );

		echo form_input($input);
	?>
  </div>
</div>
<p></p>

<div class="row">
  <div class="col-md-3"><label for="lec">Lec.</label></div>
  <div class="col-md-3">
	<?php 
		$input = array(
              'name'        => 'subjects[lec]',
              'value'       => set_value('subjects[lec]',isset($subjects->lec) ? $subjects->lec : ''),
              'maxlength'   => '4',
			  			'class'				=> 'units',
			  			'id'					=> 'txt_lec'	
            );

		echo form_input($input);
	?>
  </div>
</div>
<p></p>

<div class="row">
  <div class="col-md-3"><label for="lab">Lab.</label></div>
  <div class="col-md-3">
	<?php 
		$input = array(
              'name'        => 'subjects[lab]',
              'value'       => set_value('subjects[lab]',isset($subjects->lab) ? $subjects->lab : ''),
              'maxlength'   => '4',
			  			'class'				=> 'units',
			  			'id'					=> 'txt_lab'
            );

		echo form_input($input);
	?>
  </div>
</div>
<p></p>


<div id='message' title='Required Fields' style='display:none;'>
	<div id='message_list' class='alert alert-info'>
		Validating ... 
	</div>
</div>

