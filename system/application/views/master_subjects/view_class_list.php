<div class="row">
  <div class="table-responsive">
    <table class="table table-hover">
      <thead>
        <tr>
          <th>Course No.</th>
          <th>Description</th>
          <th>Unit</th>
          <th>Lec</th>
          <th>Lab</th>
          <th>Time</th>
          <th>Day</th>
          <th>Room</th>
          <th>Remaining Slots</th>
          <th>Instructor</th>
          <th>Academic Year</th>
        </tr>
      </thead>  
      <?php if(!empty($subject)):?>
        <tr>
              <td><?php echo $subject->code;?></td>
              <td><?php echo $subject->subject;?></td>
              <td><?php echo $subject->units;?></td>
              <td><?php echo $subject->lec;?></td>
              <td><?php echo $subject->lab;?></td>
              <td><?php echo $subject->time;?></td>
              <td><?php echo $subject->day;?></td>
              <td><?php echo $subject->room;?></td>
              <td><?php echo $subject->subject_load.'/'.$subject->original_load;?></td>
              <td> ------------</td>
              <td><?php echo $subject->year_from.' - '.$subject->year_to;?></td>
          </tr>
      <?php endif;?>
    </table>    
  </div>
</div>

<div class="row">
  <div class="table-responsive">
    <table class="table table-hover">
      <thead>
        <tr>
          <th>Student ID</th>
          <th>Name</th>
          <th>Year</th>
          <th>Course</th>
        </tr>
      </thead>  
      <?php if(!empty($enrollments)):?>
        <?foreach ($enrollments as $key => $value):?>
            <tr>
              <td><?php echo $value->studid;?></td>
              <td><?php echo $value->name;?></td>
              <td><?php echo $value->year;?></td>
              <td><?php echo $value->course;?></td>
            </tr>
        <?endforeach;?>
      <?php endif;?>
    </table>    
  </div>
</div>
<a class='btn btn-default btn-sm' href="<?=base_url('subjects')?>"><span class='glyphicon glyphicon-backward'></span>&nbsp; Back to list </a>
