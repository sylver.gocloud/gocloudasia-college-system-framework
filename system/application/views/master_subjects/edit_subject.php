<?php 

//Data from database
$id		= $subject_data->id;
$c_id		= $subject_data->course_id;
$y_id		= $subject_data->year_id;
$s_id		= $subject_data->semester_id;
$sc_id	= $subject_data->sc_id;
$code	= $subject_data->code;
$subject	= $subject_data->subject;
$unit	= $subject_data->units;
$lec	= $subject_data->lec;
$lab	= $subject_data->lab;
$time	= explode('-',$subject_data->time); // break the time to 2 ,time format from db is 0:00-0:00
$day	= $subject_data->day;
$room	= $subject_data->room;
$subject_load	= $subject_data->subject_load;
$original_load	= $subject_data->original_load;
$year_to	= $subject_data->year_to;
$year_from	= $subject_data->year_from;
$rle	= $subject_data->rle;
$pre	=$subject_data->prerequisite_subject_id;


// Drop down options for subjects
if(!empty($subjects))
{
	foreach($subjects as $s){
	$sub_opt[] = 'Select A Subject';
	$sub_opt[$s->id] = $s->code.' | '.$s->subject;
	// remove current selected subject on the option
	unset($sub_opt[$id]);
	}
}else{
	$sub_opt[] = 'No Data to show';
}


/* ############################### end of PHP Configuration ######################################### */
?>		

		<div id="right">
		  
		  <div id="right_top" >
		  <p id="right_title">Subject : Edit</p>
		  <!--<p id="back"></p>-->
		  </div>
		  <div id="right_bottom">
	
<?=$system_message;?>
<form action="<?=site_url('subjects/edit_subject').'/'.$id;?>"  method="post">
  
  <p>
  	<span class="label-short"><label for="subject_sc_id">Subject Code: </label></span>
  	<input id="subject_sc_id" name="sc_id" size="10" type="text" value="<?=$sc_id;?>" />
  </p>
  <p>
  	<span class="label-short"><label for="subject_code">Section Code: </label></span>
  	<input id="subject_code" name="code" size="10" type="text" value="<?=$code;?>" />
  </p>
  <p>
    <span class="label-short"><label for="subject_subject">Description: </label></span>
    <input id="subject_subject" name="subject" size="30" type="text" value="<?=$subject;?>" />
  </p>
  <p>
  	<span class="label-short"><label for="subject_units">No. of Units: </label></span>
  	<input id="subject_units" name="units" size="30" type="number" min="0" max="20" step="any" value="<?=$unit;?>" />
  </p>
  <p>
  	<span class="label-short"><label for="subject_lec">Lecture: </label></span>
  	<input id="subject_lec" name="lec" type="number" min="0" max="80" value="<?=$lec;?>" />
  </p>
  <p>
  	<span class="label-short"><label for="subject_lab">Lab: </label></span>
  	<input id="subject_lab" name="lab" size="30" type="number" min="0" max="20" step="any" value="<?=$lab;?>" />
  </p>
  <p>
  	<span class="label-short"><label for="subject_time">Time: </label></span>
  	<input name="time_from" size="30" type="text" value="<?=trim($time[0]);?>" class="time"/> TO <input class="time" name="time_to" size="30" type="text" value="<?=trim($time[1]);?>"/>
  </p>

  <p>
  	<span class="label-short"><label for="subject_day">Day: </label></span>
	<?=day_select($day);?>
  </p>
  <p>
  	<span class="label-short"><label for="subject_room">Room: </label></span>
  	<input id="subject_room" name="room" size="30" type="text" value="<?=$room;?>" />
  </p>
  <p>
  	<span class="label-short"><label for="subject_original_load">Maximum Slots: </label></span>
  	<input id="subject_original_load" name="original_load" size="30" type="number" min="0" max="80" value="<?=$original_load;?>" />
  </p>
  <p>
  	<span class="label-short"><label for="subject_rle">Related Experience Learning (RLE): </label></span>
  	<input id="subject_rle" name="rle" size="30" type="number" min="0" max="20" value="<?=$rle;?>" />
  </p>
    <p>
  	<span class="uniq"><input id="reset_subject_load" name="reset_subject_load" type="checkbox" value="1" /> Reset Subject Load</span>
  </p>
  <p>
	Subject Load Settings<br />
		<span class="uniq">
			<input type="radio" name="load" value="1">Add<br>
			<input id="add_load" name="add_value" size="5" type="number" min="0" max="<?=($original_load - $subject_load);?>"/>
		</span>
		<span class="uniq">
			<input type="radio" name="load" value="2">Subtract<br>
			<input id="add_load" name="sub_value" size="5" type="number" min="0" max="<?=$subject_load?>"/>
		</span>
			<input type="radio" name="load" value="0" checked>None

  </p>
  <p>
  	<span class="label-short"><label for="subject_subject_load">Remaining Slots: </label></span>
  	  	<?=($original_load - $subject_load);?>
  </p>
  
   <p>
		<span class="label-short"><label for="subject_year_id">Select which year this subject will be in</label></span>
		<?=year_dropdown('years',$y_id);?>
  </p>
  

   <p>
		<span class="label-short"><label for="subject_semester_id">Select which semester this subject will be in</label></span>
		<?=semester_dropdown('semesters',$s_id);?>
  </p>

  
  <p>
		<span class="label-short"><label for="subject_course_id">Select which course this subject will be in</label></span>
		<?=course_dropdown('courses',$c_id);?>
  </p>
  <p>
		<span class="label-short">Prerequisite</span>
		<?=form_dropdown('prerequisite_subject_id',$sub_opt,$pre);?>
  </p>
    <span class="label-short"><label for="subject_year_from">Academic Year</label></span>
  <input id="subject_year_from" name="year_from" size="5" type="number" min="2000" max="2099" value="<?=$year_from;?>" /> - <input id="subject_year_to" name="year_to" size="5" type="number" min="2000" max="2099" value="<?=$year_to;?>" />
  </p>
  <p>
	<input type="hidden" name="subject_id" value="<?=$id;?>">
	<input type="hidden" name="subject_load" value="<?=$subject_load;?>">
	<input id="subject_submit" name="update_subjects" type="submit" value="Save changes" />
  </p>
 </form>



			</div>
		</div>
