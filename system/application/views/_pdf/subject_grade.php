<?php
	$subject = $record['subject'];
	$grades = $record['grades'];
	$current_gp = $record['current_gp'];
	$profile = $record['profile'];
	$grading_period = $record['grading_period'];
?>
<?@$this->load->view('_pdf/_portrait_head');?>
<b>Student Grades Report</b><p></p>
<table class="table table-border">
	<tr>
		<td class="bold text-right" >Fullname :</td>
		<td colspan="3" class="bold"><?=$profile->fullname?> &nbsp; (<?=$profile->studid?>)</td>
		<td class="bold"><?=$profile->year?>-<?=$current_gp->grading_period?></td>
	</tr>

	<tr>
		<td class="bold text-right" >Course :</td>
		<td colspan="4" class="bold"><?=$profile->course?> &nbsp; (<?=$profile->course_code?>)</td>
	</tr>
</table>
<b>Subjects (<?=isset($grades->subjects)?count($grades->subjects):0?>)</b>
<?if($grades):?> 

	<table class="table table-border">
    <thead>
      <tr>
        <th>Course No.</th>
        <th class="hidden-xs" >Description</th>
        <?if(isset($grades->periods) && $grades->periods):?>
          <?foreach ($grades->periods as $gp_id => $p):?>
            <th><?=$p?></th>
          <?endforeach;?>
        <?endif;?>
        <th >Remarks</th>
      </tr>
    </thead>

    <?if(isset($grades->subjects) && $grades->subjects):?>
      <?foreach ($grades->subjects as $k => $subjects):?>

        <? $grade = $subjects['grade']; ?>
        <? $subject = $subjects['subject']; ?>
        
        <?if($subject):?>
          <tr>
            <td><?= $subject->code; ?></td>
            <td class="hidden-xs" ><?= ucwords(strtolower($subject->subject)); ?></td>
            
            <?if($grade):?>
              <?foreach ($grade as $g_k => $g):?>

                <td><?=($g->converted);?></td>

              <?endforeach;?>
            <?endif;?>
            <td><?=$subject->remarks?></td>

          </tr>
        <?endif;?>

      <?endforeach;?>
    <?else:?>
      <tr><td colspan="3" ><h4>&nbsp;<i class="fa fa-warning"></i>&nbsp; No Grades Available</h4></td></tr>
    <?endif;?>

  </table>
<?else:?>
	<p></p>
	<p>No Students Found for this Subject.</p>
<?endif;?>	
<?@$this->load->view('_pdf/_portrait_foot');?>