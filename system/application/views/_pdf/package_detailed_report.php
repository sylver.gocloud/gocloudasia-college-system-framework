<?@$this->load->view('_pdf/_portrait_head');?>
<?php
	$package = isset($record['package'])?$record['package']:false;
	$packages = isset($record['packages'])?$record['packages']:false;
?>
<div>
<?if($packages):?>
	<?foreach($packages as $k => $p):?>
		<?php
			$profile = $p->profile;
			$menus = $p->menus;
		?>
		<div class="">
			<table class="table bold table-border">
				<tr>
					<td>Package : <?=ucwords($profile->package)?></td>
					<td>Description &nbsp; :</td>
					<td><?=$profile->remarks?></td>
				</tr>
			</table>
			<h3><strong><u>Available Departments And Menus</u></strong></h3>
			<?$ctr=0;?>
			<?if($menus): foreach ($menus as $mk => $m):?>
					<?php
						$dep = $m['department'];
						$men = $m['menus'];
					?>
					<p><strong><?=++$ctr?>.&nbsp; <?=uc_words($dep->description)?></strong></p>
					<table class="table table-border" >
					<?if($men): foreach ($men as $men_k => $men_val):?>
						<?php
							$menu_group = $men_val['head']; 
							$menu_sub = $men_val['menus'];
						?>
						
							<tr>
								<td style="vertical-align: top" ><?=$menu_group->caption?></td>
								<td>
									<ul style="list-style-position: inside">
									<?if($menu_sub): foreach ($menu_sub as $men_sub_k => $men_sub_val):?>
										<li style="padding-left:100px" ><?=$men_sub_val->caption;?></li>
									<?endforeach; endif;?>
									</ul>
								</td>
							</tr>
						
					<?endforeach; endif;?>
					</table>
					<p></p>
			<?endforeach; endif;?>
		</div>
		<p></p>
		<p></p>
	<?endforeach;?>
<?endif;?>
	<table class="table table-border">
		<thead>
		<tr class="bg-gray">
			<th>Order</th>
			<th>Package</th>
			<th>Description</th>
			<th>Access Departments</th>
		</tr>
		</thead>

		
	</table>
</div>

<?@$this->load->view('_pdf/_portrait_foot');?>