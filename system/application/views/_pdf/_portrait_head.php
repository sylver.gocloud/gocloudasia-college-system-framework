<html>
	<head>
		<style>
			table.data{
				width:100%;
				margin:20px auto 0 auto;
				border:1px solid #000;
				border-collapse:collapse;
			}
			td.short{
				text-align:right;
				padding-right:20px;
			}
			tr.head td{
				border:1px solid #000;
			}
			.payment_head{
				text-align:center;
				background:#c0c0c0;
				font-weight:bold;
			}
			
			table#page-heading{
				margin:0 auto;
				width:100%;
			}
			table#page-heading tr td{
				border:none;
			}
			
			table.table-border, table.table-border th, table.table-border td {
			   border: 1px solid black;
			   border-collapse: collapse;
			   padding: 4px;
			}

			div.header{
				margin-bottom:10px;
				border-bottom:1px solid #000;
			}

			.text-right{
				text-align:right;
			}

			.text-left{
				text-align:left;
			}

			.text-center{
				text-align:center;
			}

			.v-align-center{
				vertical-align: center;
			}

			.bold{
				font-weight: bold;
			}

			.text-red{
				color:red;
			}

			.italic{
				font-style: italic;
			}

			.bg-gray{
				background: #CCCCCC;
			}

			.table{
				width: 100%;
				font-size: 9pt;
			}

			tr.underline td{
				border-bottom: 1px solid black;
			}

			body{
				font-family: Arial;
			}
		</style>
	</head>
	<body>
		<div class="header">
		<table id="page-heading" cellpadding="0" cellspacing="2">
			<tr>
				<td class="title">
					<p style="font-size:20; font-weight:bold;"><?=iset($school_name)?></p>
					<p style="font-size:12;text-align:center;"><?=iset($school_address)?></p>
					<p style="font-size:12;text-align:center;"><?=iset($school_telephone)?></p>
				</td>
			</tr>
		</table>
		</div>
