<?@$this->load->view('_pdf/_portrait_head');?>
<?php
	$packages = $record['packages'];
	$get_dp = $record['get_package_department'];
?>
<div>
	<table class="table table-border">
		<thead>
		<tr class="bg-gray">
			<th>Order</th>
			<th>Package</th>
			<th>Description</th>
			<th>Access Departments</th>
		</tr>
		</thead>

		<?if($packages):?>
			<?foreach ($packages as $k => $p):?>
				<tr>
					<td><?=$p->level?></td>
					<td><?=$p->package?></td>
					<td><?=$p->remarks?></td>
					<td><?=$get_dp($p->id);?></td>
				</tr>
			<?endforeach;?>
		<?else:?>
			<tr>
				<td colspan="6" class="text-center bold" >No History Found</td>
			</tr>
		<?endif;?>
	</table>
</div>

<?@$this->load->view('_pdf/_portrait_foot');?>