<?@$this->load->view('_pdf/_portrait_head');?>
<div class="">
	<table class="table bold">
		<tr>
			<td>Name &nbsp; :</td>
			<td><?=ucwords($_student->profile->fullname)?></td>
			<td>Student ID &nbsp; :</td>
			<td><?=$_student->profile->studid?></td>
		</tr>
		<tr>
			<td>Course &nbsp; :</td>
			<td><?=ucwords($_student->profile->course)?></td>
		</tr>
	</table>
</div>
<div>
	<table class="table">
		<thead>
		<tr class="bg-gray">
			<th>School Year</th>
			<th>Year - Semester</th>
			<th>Date</th>
			<th>Or. No.</th>
			<th>Description</th>
			<th>Amount</th>
		</tr>
		</thead>

		<?if($student_ledger):?>
			<?$outstanding_balance = 0;?>
			<?foreach ($student_ledger as $k => $val):?>
				<?php
					$sy = $val['sy'];
					$en = $val['enrollments'];
				?>

				<?if($en):?>
					<?foreach ($en as $e => $eval):?>
							<?php
								$combined = $eval['combined'];
								$profile = $eval['profile'];
								$totals = $eval['totals'];
								$outstanding_balance += $totals->total_balance;
							?>
							<tr>
								<td class="bold" ><?=$sy->sy_from?>-<?=$sy->sy_to?></td>
								<td><?=$profile->year?> - <?=$profile->semester?></td>
								<td colspan="3" class="bold">Total Tuition and Fees</td>
								<td class="text-right" ><span class="badge"> <?=m($profile->total_plan_due)?></span></td>
							</tr>
							<?foreach ($combined as $c => $cval):?>
								<?php
									$date = date('m/d/Y', strtotime($cval['date']));
									$value = $cval['value'];
									$type = $cval['type'];
									$amount = 0;
									$remarks = 0;
									$or_no = "";

									switch ($type) {
										case 'payment':
											$or_no = $value->spr_or_no;
											$remarks = $value->spr_remarks;
											$amount = $value->spr_ammt_paid;
											break;
										case 'scholarship':
											$or_no = "";
											$remarks = $value->scholarship_name;
											$amount = $value->scho_amount;
											break;
										case 'deduction':
											$or_no = "";
											$remarks = $value->deduction_name;
											$amount = $value->amount;
											break;
									}
								?>
								<tr>
									<td></td>
									<td></td>
									<td><?=$date?></td>	
									<td><?=$or_no?></td>	
									<td><?=$remarks?></td>			
									<td class ="text-right" >- <?=m($amount)?></td>			
								</tr>
							<?endforeach;?>
							
							<tr class="underline">
								<td colspan="5" class="bold text-right">Sub-Total</td>
								<td class="text-right" ><span class="bold"> <?=m($totals->total_deduction_amount + $totals->total_amount_paid)?></span></td>
							</tr>

							<?if($totals->total_balance):?>
								<tr class="danger">
									<td colspan="5" class="bold text-right">Previous Balance</td>
									<td class="text-right" ><span class="text-red"> <?=m($totals->total_balance)?></span></td>
								</tr>
							<?endif;?>

					<?endforeach;?>
				<?else:?>
					<td colspan="6" class="text-center bold" >No Record for this Year</td>
				<?endif;?>

			<?endforeach;?>
			<tfoot>
				<tr>
					<th colspan="5" class="bold text-center"><h4>OUTSTANDING BALANCE as of <?=date('F, d Y')?> </h4></th>
					<th class="text-right bold"><span class="text-red bold"><h3><?=m($outstanding_balance)?></span></h3></th>
				</tr>
			</tfoot>	
		<?else:?>
			<tr>
				<td colspan="6" class="text-center bold" >No History Found</td>
			</tr>
		<?endif;?>
	</table>
</div>

<?@$this->load->view('_pdf/_portrait_foot');?>