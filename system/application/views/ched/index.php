<?php
$formAttrib = array('id' => 'search_enrollee', 'class' => 'form-inline', 'method' => 'POST');

?>
<?echo form_open('',$formAttrib);?>
  <div class="form-group">
    <?=year_dropdown('year_id',isset($year_id)?$year_id:'',"", '')?>
  </div>
  
  <div class="form-group">
    <?=semester_dropdown('semester_id',isset($sem_id)?$sem_id:'',"", '')?>
  </div>
  
  <div class="form-group">
    <?=course_dropdown('course_id',isset($course_id)?$course_id:'',"", '')?>
  </div>
 <?echo form_submit('submit','Search');?>
 <?php echo form_submit('submit', 'Download Excel','target="_blank"'); ?>
 <?echo form_close();?>
 
<br/>

<table id="ched_table">
  <tr>
    <th colspan=2>Student Name</th>
	<?php for($i=1;$i<=$no_subjects;$i++){ ?>
    <th>Subject Code</th>
    <th>Earned Units</th>
	<?php } ?>
    <th>Total Earned Units</th>
  </tr>
<?php 
$num=1;
if($students)
{
	foreach($students as $student)
	{	
		$total_units = 0;
		$used_row = 0;
		?>
		<tr>
			<td><?=$num;?></td>
			<td><?=$student->name;?></td>
			<?
			if(isset($studentsubjects[$student->id]) && is_array($studentsubjects[$student->id]))
			{
				foreach($studentsubjects[$student->id] as $subject)
				{
					if($subject)
					{	
						$total_units += $subject->units;
						$used_row++;
						?>
							<td><?=$subject->sc_id;?></td>
							<td><?=$subject->units;?></td>
						<?	
					}
				}
			}
			//CREATE BLANK ROWS
			for($x = $used_row+1; $x <= $no_subjects; $x++)
			{
				echo "<td></td>";
				echo "<td></td>";
			}
			?>
			<td><div class='badge'><?=$total_units?></div></td>
		</tr>
		<?
		$num++;
	}
	?>
	<tr>
		<th colspan=2>Student Name</th>
		<?php for($i=1;$i<=$no_subjects;$i++){ ?>
		<th>Subject Code</th>
		<th>Earned Units</th>
		<?php } ?>
		<th>Total Earned Units</th>
	 </tr>
	<?
}
else
{
	?>
	<tr>
	<td colspan="3">
	No record to display
	</td>
	<?php
}
?>
</table>



	
	
