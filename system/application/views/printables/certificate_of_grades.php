<html>
  <head>
	 <link href="<?php echo base_url('assets/css/printables.css'); ?>" rel="stylesheet" type="text/css"  />
  </head>
  <body>
  <div id="certificate_of_enrollment">
   <div id="header-center">
       <table align=center>
         <tr>
		 <td> <img src="<?= base_url($setting->logo); ?>"   width=100 height=100></td>	
		 <td>
		    <h3><?= $setting->school_name; ?></h3><br />
		    <?= $setting->school_address; ?><br />
		    <?= $setting->school_telephone; ?><br />
		    <?= $setting->email; ?><br />
		 </td>
         </tr>
       </table> 
    </div>
    <div id="header-center-title">
    <h1>CERTIFICATE OF GRADE</h1>
    </div>
    <br />
    <h2>TO WHOM IT MAY CONCERN:</h2>
    <br /><br />
    <div class="content">
    <p>
This is to certify that <b><?= ucwords(strtolower($enrollment->full_name)); ?></b>  of <?= $enrollment->present_address; ?> is enrolled in the following subjects under the <b><?= $enrollment->course; ?></b> with the corresponding grades and
units
 this <?= $enrollment->name; ?> <b>SY <?= $enrollment->sy_from; ?> - <?= $enrollment->sy_to; ?></b>.
    </p>
    <br /><br />
	<table id="student_subjects">
	  <tr>
	    <th>Code</th>
	    <th>Description</th>
	    <th>Units</th>
	    <th>Final Grade</th>
	  </tr>
	  <? $num=0 ?>
	  <?if(!empty($subjects)) :?>
	    <? foreach($subjects as $sg ):?>
	    <tr>
	      <td><?= $sg->code ?></td>
	      <td><?= $sg->subject ?></td>
        <td align="center"><?= $sg->units ?></td>
        <td align="center" ><?= $sg->final_grade?></td>
	    </tr>
	    <? endforeach ;?>
	  <? endif ;?>
	</table>
    <br /><br />
    <p>
     
     This certification is being issued upon the request of <?= ucwords($title); ?> <?= ucwords(strtolower($enrollment->lastname)); ?> for
whatever legal purpose it may serve.
    </p>
    <p>
     Issued this <?= date('jS'); ?> date of <?= date('F'); ?> <?= date('Y'); ?> , at <?= $setting->school_address; ?>.
    </p>
    </div>
  </div>
  </body>
</html>