<table>
	<tr>
		<th>#</th>
		<th>Student ID</th>
		<th>Name</th>
		<th>Year</th>
		<th>Course</th>
	</tr>
	
<?
	if($result):
	$ctr = 1;
?>
	<?foreach($result as $obj):?>
	<tr>
		<td><?=$ctr++;?></td>
		<td><?=$obj->studid?></td>
		<td><?=strtoupper($obj->name)?></td>
		<td><?=$obj->year?></td>
		<td><?=$obj->course?></td>
	</tr>
	<?endforeach;?>
	<tr>
		<th>#</th>
		<th>Student ID</th>
		<th>Name</th>
		<th>Year</th>
		<th>Course</th>
	</tr>
<?else:?>
	<tr>
		<td>No Record Found.</td>
	</tr>
<?endif;?>
</table>