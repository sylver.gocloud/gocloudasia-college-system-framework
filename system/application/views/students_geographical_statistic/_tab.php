<ul class="nav nav-tabs">
	<li <?= ($this->router->class == "students_geographical_statistic" && $this->router->method == 'index') ? "class='active'" : '' ?>>
	  <a href="<?php echo base_url('students_geographical_statistic/index'); ?>">By Province</a></li>
	  
	<li <?= ($this->router->class == "students_geographical_statistic" && $this->router->method == 'by_municipality') ? "class='active'" : '' ?>>
	  <a href="<?php echo base_url('students_geographical_statistic/by_municipality'); ?>">By Municipality</a></li>
	  
</ul>