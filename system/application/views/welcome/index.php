<?if($dep_for_metro):?>
  <?foreach ($dep_for_metro as $k => $chunk):?>
    <div class="row">
      <?foreach ($chunk as $l => $dep):?>
        <div class="col-md-2 col-sm-4 col-xs-6">
            <p class="text-center">
              <a href="<?php echo base_url(); ?><?=$dep->controller?>/<?=$dep->department?>"><img class="welcome_icon" alt="<?=$dep->department?>" height="100" src="<?php echo base_url(); ?><?=$dep->icon?>" width="100"   /><br><span class='label label-login text-center' style='font-size:8pt' ><?=$dep->description?></span></a>
            </p>
        </div>
      <?endforeach;?>    
    </div>
  <?endforeach;?>
<?endif?>