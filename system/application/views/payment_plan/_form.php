<script type="text/javascript">
  function validate()
  {
		if($('#name').val().trim() == "")
		{
			$('#name').focus();
			notify_modal('Required Fields','Payment Plan Name must not be blank');
			return false;
		}
		else
		{
			var xdiv = $('#division').val().trim();
			if(xdiv == "")
			{
				$('#division').focus();
				notify_modal('Required Fields','Payment Division must not be blank');
				return false;
			}
			else
			{
				return true;
			}
		}
  }
</script>
 
  <p>
    <label for="name">Payment Plan Name *</label>
	<?php 
		$data = array(
              'name'        => 'payment_plan[name]',
              'value'       => isset($payment_plan->name) ? $payment_plan->name : '',
              'maxlength'   => '100',
              'size'        => '50',
			  'id'    		=> 'name'
            );

		echo form_input($data);
	?>
  </p>
  
  <p>
      <label for="division">Payment Division *</label>
	<?php 
		$xdsable = isset($edit) ? 'disabled' : '';
		unset($data);
		$data[''] = "Choose Payment Division";
		for($x = 1; $x <= 12; $x++)
		{
			$data[$x] = $x;
		}
		echo form_dropdown('payment_plan[division]',$data,isset($payment_plan->division) ? $payment_plan->division : '','id="division" '.$xdsable);
	?>
  </p>

