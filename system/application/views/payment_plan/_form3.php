<?if($payment_plan):?>
	
	<?
		$division = floatval($payment_plan->division);
	?>
	
	<input type="hidden" id='division' name='division' value='<?=$division?>' />
	
	<table>
			<tr>
				<th>Plan</th>
				<td><?=$payment_plan->name?></td>
				<th>Division</th>
				<td><?=$payment_plan->division?></td>
			</tr>
	</table>

	<?if($division > 0):?>

		<table>
			<tr>
				<th>Grading Period</th>
				<th>
					Cut Off Period
					<a class='btn btn-warning confirm' title='Are you sure to edit this cut off plan??' style='float:right' href='<?=current_url();?>/clear'><i class="fa fa-refresh"></i></span>&nbsp;  Click Here To Edit This Cut Off Period</a>
				</th>
			</tr>
		
		<?if($grading_periods):?>
			
			<?if($division == 1):?>
				
				<!--IF FULL PAYMENT--->
				
			<?else:?>
			
			<?
				$arr_div[''] = 'Please select';
				for($i = 2; $i <= $division; $i++):
					$nth = num_to_th($i);
					$arr_div[$i] = $nth.' Payment';
				endfor;
				if(count($grading_periods) > $division){
					$arr_div['0'] = 'NONE';
				}
			?>
			
			<!--FIRST SHOULD BE DOWNPAYMENT-->
			<tr>
					<td><b>DOWNPAYMENT (FIXED)</b></td>
					<td>
						1st Payment
						<input type='hidden' name='cut_off[DOWNPAYMENT]' value='1' />
					</td>
			</tr>
			<?$ctr = 2?>
			<?$ctr2 = 1?>
			<?foreach($grading_periods as $obj):?>
				
				<?
					$def = '';

					if($grading_period_req){
						foreach ($grading_period_req as $obj2) {

							if($obj->id == $obj2->grading_period_id){
								$def = $obj2->number;
								
								break;
							}
						}
					}

					$xenabled = $ctr == 2 ? 'disabled' : 'disabled';
					$xstyle = '';
					$xlast = $ctr2 == count($grading_periods) ? 1 : 0;
				?>
				
				<tr>
					<td style="<?=$xstyle?>" >
						<b><?=$obj->grading_period?></b>
						<br>
						<strong>
						<i>
						<div id='period_con_<?=$obj->id?>' >
							
						</div>
						</i>
						</strong>
					</td>
					<td style="<?=$xstyle?>" >
						<?=form_dropdown('period_'.$obj->id,$arr_div,$def,'id=period_'.$obj->id.' class="" '.$xenabled.' onchange = "select_change(this)" xid="'.$obj->id.'" xlast="'.$xlast.'" done ="0" ');?>
						<input type='hidden' id='hd_<?=$obj->id?>' name='cut_off[<?=$obj->id?>]' value='' />
					</td>
				</tr>
				
				<?$ctr++;?>
				<?$ctr2++;?>
				
			<?endforeach;?>	
			
			<?endif;?>
			
		<?endif;?>
		
		</table>
		
	<?endif;?>
	
<?else:?>
	<div class='alert alert-danger'><p><span class='glyphicon glyphicon-remove'></span>&nbsp;  Please go back and select again</p></div>
<?endif;?>