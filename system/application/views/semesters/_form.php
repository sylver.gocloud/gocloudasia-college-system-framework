
 
  <p>
    <label for="semester">Semester</label><br />
	<?php 
		$data = array(
              'name'        => 'semesters[name]',
              'value'       => isset($semesters->name) ? $semesters->name : '',
              'maxlength'   => '100',
              'size'        => '50',
              'style'       => 'width:50%',
            );

		echo form_input($data);
	?>
  </p>
  <div class="clear"></div>

