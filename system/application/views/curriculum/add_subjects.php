<div class="row">
	<div class="col-md-2 bold">
		<label>Curriculum Name : </label><br/>
		<span class="label label-info label-lg"><?=$curriculum->name;?></span>
	</div>
	<div class="col-md-3 bold">		
		<label>Course : </label><br>
		<span class="label label-info label-lg"><?=$curriculum->course;?></span>
	</div>
	<?if($curriculum->specialization):?>
		<div class="col-md-3 bold">		
			<label>Major : </label><br>
			<span class="label label-info label-lg"><?=$curriculum->specialization;?></span>
		</div>
	<?endif;?>
	<div class="col-md-1 bold">
		<label>Status : &nbsp; </label><br>
		<span class="label label-info label-lg"><?=$curriculum->is_deleted == 0?'Active':'In-active';?></span>
	</div><br>
	<div class="col-md-3 bold">
		<!-- Single button -->
		<div class="btn-group btn-group-sm">
		  <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown">
		    More <span class="caret"></span>
		  </button>
		  <ul class="dropdown-menu" role="menu">
		    <li><a href="<?php echo site_url('curriculum/edit/'.$hash_id) ?>"><span class="glyphicon glyphicon-edit" ></span> Edit Curriculum</a></li>
		    <li><a href="<?php echo site_url('curriculum/print_curriculum/'.$hash_id) ?>" target="_blank" ><span class="entypo-print" ></span> Print</a></li>
		    <li><a href="<?php echo site_url('curriculum/recalculate_units/'.$hash_id) ?>" ><span class="entypo-cw" ></span> Recalculate Units</a></li>
		  </ul>
		</div>
		<a href="<?php echo site_url('curriculum') ?>" class="btn btn-default btn-sm">Cancel</a>
	</div>
</div>
<hr>
<p></p>

<!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">
	<li class="<?=$add_type == "subjects" ? 'active' : ''?>" ><a href="#subjects" role="tab" data-toggle="tab"><i class="fa fa-book"></i>&nbsp; Subjects</a></li>
  <li class="<?=$add_type == "multiple" ? 'active' : ''?>"><a href="#multiple" role="tab" data-toggle="tab">Subject Multiple Assignment</a></li>
  <li class="<?=$add_type == "single" ? 'active' : ''?>" ><a href="#single" role="tab" data-toggle="tab">Assign Single Subject</a></li>
  <li class="<?=$add_type == "create_subject" ? 'active' : ''?>" ><a href="#create_subject" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-plus"></span> Create Master Subject</a></li>
  <li class="<?=$add_type == "edit_subject" ? 'active' : ''?>" ><a href="#edit_subject" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-edit"></span> Edit  Subject</a></li>  
</ul>

<!-- Tab panes -->
<div class="tab-content">

	<!-- MULTIPLE ASSIGNMENT TAB -->	
	<div class="tab-pane <?=$add_type == "multiple" ? 'active' : ''?>" id="multiple"><br/>

		<?=form_open('','class="multiple_subject_form form-horizontal" onsubmit="return validate_multiple(event,this)"')?>
		<div class="form-group">
		  <label class="col-md-2 control-label" for="textinput">Year</label>  
		  <div class="col-md-4">
		  <!-- <input id="textinput" name="textinput" type="text" placeholder="placeholder" > -->
		  <?=year_dropdown('year_id','','class="form-control input-md" required','Choose Year')?>
		  <span class="help-block">Select what year will the subject/s will be in.</span>  
		  </div>

		  <label class="col-md-2 control-label" for="textinput">Semester</label>  
		  <div class="col-md-4">
		  <?=semester_dropdown('semester_id','','class="form-control input-md" required','Choose Semester')?>
		  <span class="help-block">Select what semester will the subject/s will be in.</span>  
		  </div>
		</div>
		<hr class="hr">
		<div class="alert alert-info"><i class="fa fa-info-circle"></i>&nbsp; Duplicate subjects will not be added.</div>
		<?@$this->load->view('layouts/student_data/_student_subject_tab_sorted');?>

		<!-- Button (Double) -->
		<div class="form-group">
		  <div class="col-md-6">
		    <input type="hidden" name="event_action" value="save_multiple" />
		    <div class="btn-group">
		    	<?if($subject_master):?>
		    		<label for="mySubmit" class="btn btn-primary btn-sm"><i class="fa fa-save"></i>&nbsp; Add Subject Selected <span id = "subject_count" class="badge" >0</span></label>
			    	<input id="mySubmit" type="submit" value="Save Subject Selected" name="" class="hidden" />
		    	<?endif;?>
		    	<a href="<?=site_url('curriculum')?>" id="button2id" name="button2id" class="btn btn-default btn-sm"><i class="fa fa-arrow-left"></i>&nbsp; Go Back To List </a>
		    </div>
		  </div>
		</div>
		</form>
	</div>

	<!-- SINGLE ASSIGNMENT TAB -->	
	<div class="tab-pane <?=$add_type == "single" ? 'active' : ''?>" id="single">
		<!-- FORM STARTS HERE -->
		<?=form_open('','class="single_subject_form form-horizontal" ')?>

		<fieldset>

			<!-- Form Name -->
			<legend><small>Select Subject</small></legend>

			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-2" for="subject_id">Subject</label>  
			  <div class="col-md-6">
			  <?=form_dropdown('subject_id', $subject_dropdown, set_value('subject_id',''), 'class="form-control input-md" id="subject_id" class="form-control" required' );?>
			  </div>
			</div>

			<div class="form-group">
			  <label class="col-md-2" for="year_id">Year</label>  
			  <div class="col-md-6">
			  <!-- <input id="textinput" name="textinput" type="text" placeholder="placeholder" > -->
			  <?=year_dropdown('year_id',set_value('year_id',''),'class="form-control input-md" id="year_id" required','Choose Year')?>
			  <span class="help-block">Select what year will the subject will be in.</span>  
			  </div>
			</div>

			<div class="form-group">
			  <label class="col-md-2" for="semester_id">Semester</label>  
			  <div class="col-md-6">
			  <?=semester_dropdown('semester_id',set_value('semester_id',''),'class="form-control input-md" id="semester_id" required','Choose Semester')?>
			  <span class="help-block">Select what semester will the subject will be in.</span>  
			  </div>
			</div>
		</fieldset>

		<fieldset>

			<!-- Form Name -->
			<legend><small>Subject Prerequisite (Optional)</small></legend>

			<div class="form-group">
			  <label class="col-md-3 control-label" for="req_year_id">Required Year Level (Optional)</label>  
			  <div class="col-md-5">
			  <!-- <input id="textinput" name="textinput" type="text" placeholder="placeholder" > -->
			  <?=year_dropdown('req_year_id','','class="form-control input-md" id="req_year_id"','None')?>
			  <span class="help-block">Required Year Level for the subject selected above.</span>  
			  </div>
			</div>

			<div class="form-group">
			  <label class="col-md-3 control-label" for="total_units_req">Required Units (Optional)</label>  
			  <div class="col-md-5">
			  <input id="total_units_req" name="total_units_req" class="form-control input-md" type="number" step="any" placeholder="0" min = "0" max = "1000" />
			  <span class="help-block">Required units taken by the students.</span>  
			  </div>
			</div>

			<div class="form-group">
			  <label class="col-md-3 control-label" for="enrollment_units_req">Required Enrollment Units (Optional)</label>  
			  <div class="col-md-5">
			  <input id="enrollment_units_req" name="enrollment_units_req" class="form-control input-md" type="number" step="any" placeholder="0" min = "0" max = "1000" />
			  <span class="help-block">Required units taken by the students during enrollment.</span>  
			  </div>
			</div>

			<div class="form-group">
			  <label class="col-md-3 control-label" for="semester_id">Prerequisite Subject/s (Optional)</label>  
			  <div class="col-md-5">
			  <?for($i = 1; $i <= 7; $i++):?>
			  		<?=form_dropdown('subject_pre['.$i.']', $subject_dropdown, '', 'class="form-control input-md" id="subject_id" class="form-control"' );?>
			  		<input type="checkbox" name="subject_pre_coincide[<?php echo $i; ?>]" id=""> Can be taken simultaneously during enrollment.		  		
						<p></p>
			  <?endfor;?>
			  <span class="help-block">Duplicate Subjects will not be added.</span>  
			  </div>
			</div>
		</fieldset>

		<!-- Button (Double) -->
		<div class="form-group">
		  <label class="col-md-3 control-label" for="semester_id"></label>  
		  <div class="col-md-5">
		  	<input type="hidden" name="event_action" value="save_single" />  
		    <div class="btn-group">
		    	<label for="mySubmit1" class="btn btn-primary btn-sm"><i class="fa fa-save"></i>&nbsp; Assign Subject</label>
		    	<input id="mySubmit1" type="submit" value="Save Subject" name="" class="hidden" />
		    	<a href="<?=site_url('curriculum')?>" id="button2id" name="button2id" class="btn btn-default btn-sm"><i class="fa fa-arrow-left"></i>&nbsp; Go Back To List </a>
		    </div>

		  </div>
		</div>
		</form>
	</div>

	<!-- SUBJECTS TAB -->	
	<div class="tab-pane <?=$add_type == "subjects" ? 'active' : ''?>" id="subjects"><br/>
		<?@$this->load->view('curriculum/_curriculum_subjects');?>
	</div>

	<!-- CREATE SUBJECTS TAB -->	
	<div class="tab-pane <?=$add_type == "create_subject" ? 'active' : ''?>" id="create_subject"><br/>
		<?php echo form_open('','class="confirm" title="Please confirm that all information you have entered is correct and continue."') ?>
			<fieldset>
				<legend><small>Subject Form</small></legend>
				<?@$this->load->view('master_subjects/_form')?>
			</fieldset>

			<fieldset>

				<!-- Form Name -->
				<legend><small>Year & Semester</small></legend>

				<div class="row">
				  <label class="col-md-3" for="year_id">Year</label>  
				  <div class="col-md-5">
				  <!-- <input id="textinput" name="textinput" type="text" placeholder="placeholder" > -->
				  <?=year_dropdown('year_id',set_value('year_id',''),'class="form-control input-md" id="year_id" required','Choose Year')?>
				  <span class="help-block">Select what year will the subject will be in.</span>  
				  </div>
				</div>

				<div class="row">
				  <label class="col-md-3" for="semester_id">Semester</label>  
				  <div class="col-md-5">
				  <?=semester_dropdown('semester_id',set_value('semester_id',''),'class="form-control input-md" id="semester_id" required','Choose Semester')?>
				  <span class="help-block">Select what semester will the subject will be in.</span>  
				  </div>
				</div>
			</fieldset>

			<fieldset>

				<!-- Form Name -->
				<legend><small>Prerequisite (Optional)</small></legend>

				<div class="row">
				  <label class="col-md-3 control-label" for="req_year_id">Required Year Level (Optional)</label>  
				  <div class="col-md-5">
				  <!-- <input id="textinput" name="textinput" type="text" placeholder="placeholder" > -->
				  <?=year_dropdown('req_year_id',set_value('req_year_id'),'class="form-control input-md" id="req_year_id"','None')?>
				  <span class="help-block">Required Year Level for the subject selected above.</span>  
				  </div>
				</div>

				<div class="row">
				  <label class="col-md-3 control-label" for="total_units_req">Required Units (Optional)</label>  
				  <div class="col-md-5">
				  <input id="total_units_req" name="total_units_req" class="form-control input-md" value="<?=set_value('total_units_req')?>" type="number" step="any" placeholder="0" min = "0" max = "1000" />
				  <span class="help-block">Required units already finished by the enrollee.</span>  
				  </div>
				</div>

				<div class="row">
				  <label class="col-md-3 control-label" for="enrollment_units_req">Required Enrollment Units (Optional)</label>  
				  <div class="col-md-5">
				  <input id="enrollment_units_req" name="enrollment_units_req" class="form-control input-md" value="<?=set_value('enrollment_units_req')?>" type="number" step="any" placeholder="0" min = "0" max = "1000" />
				  <span class="help-block">Required units taken by the student during enrollment.</span>  
				  </div>
				</div>

				<div class="row">
				  <label class="col-md-3 control-label" for="semester_id">Prerequisite Subject/s (Optional)</label>  
				  <div class="col-md-5">
				  <?for($i = 1; $i <= 7; $i++):?>
				  		<?=form_dropdown('subject_pre['.$i.']', $subject_dropdown, '', 'class="form-control input-md" id="subject_id" class="form-control"' );?>
				  		<input type="checkbox" name="subject_pre_coincide[<?php echo $i; ?>]" id=""> Can be taken simultaneously during enrollment.		  		
							<p></p>
				  <?endfor;?>
				  <span class="help-block">Duplicate Subjects will not be added.</span>
				  </div>
				</div>
			</fieldset>

			<div class="row">
				<input type="hidden" name="event_action" value="create_subject" />
			  <label class="col-md-3 control-label" for=""></label>  
			  <div class="col-md-5"><button class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-save"></span> &nbsp; Create Subject and Add to Curriculum</button></div>
			</div>
		<?php echo form_close(); ?>
	</div>

	<!-- edit prerequisite TAB -->	
	<div class="tab-pane <?=$add_type == "edit_subject" ? 'active' : ''?>" id="edit_subject"><br/>
		<?@$this->load->view('curriculum/_edit_subject');?>
	</div>

</div>
<script>
	function validate_multiple (event, el) {
		var xcnt = $('.multiple_subject_form input[type="checkbox"]:checked').length;
		if(xcnt > 0){
			return true;
		}else{
			custom_modal('System Message','No Subject Selected.');
			return false;
		}

	}
</script>