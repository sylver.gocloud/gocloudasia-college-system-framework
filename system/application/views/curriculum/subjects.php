<div class="table-responsive">
<table class="table" border='0' width="100%" >
	<tr>
		<td colspan="2" class="fsize-12 bold" >
			<?=$curriculum->name?>&nbsp; | &nbsp;<?=$curriculum->course?>
			<div class="btn-group btn-group-sm">
					<a class="btn btn-xs btn-dropbox tp" href="<?=site_url('curriculum/add_subjects/'.$curriculum_id)?>"  data-toggle="tooltip" data-placement="top" title="Add subjects"><span class="entypo-plus"></span>&nbsp; Add Subjects</a>
					<a class="btn btn-xs btn-facebook tp" href="<?=site_url('curriculum/add_subjects/'.$curriculum_id.'/subjects')?>"  data-toggle="tooltip" data-placement="top" title="Edit subjects"><span class="glyphicon glyphicon-edit"></span>&nbsp; Edit Subjects</a>
					<a class="btn btn-xs btn-default tp" target="_blank" href="<?=site_url('curriculum/print_curriculum/'.$curriculum_id)?>"  data-toggle="tooltip" data-placement="top" title="Print Curicullum"><span class="entypo-print"></span>&nbsp; Print</a>
					<a class="btn btn-xs btn-default tp" href="<?=site_url('curriculum')?>"  data-toggle="tooltip" data-placement="top" title="Back to Curicullum"><i class="fa fa-arrow-left"></i>&nbsp;Go to Curicullum</a>
				</ul>
			</div>
		</td>
	</tr>
</table>
</div>
<hr/>
<?@$this->load->view('curriculum/_curriculum_subjects');?>