<div class="tab-content">
<?if($subject_profile):?>
	
	<?php
		$sub_pre = $get_pre_requisite($curriculum_id, $subject_profile->id);
	?>

	<div class="table-responsive">
		<table class="table table-bordered">
			<tr class="" >
				<td>
					<p><strong>Course No : </strong><span class="label label-info label-lg" > <?=$subject_profile->code?> </span> &nbsp;
					<strong><a href="<?=site_url('curriculum/remove_subject/'.__link($curriculum_id).'/'.__link($subject_profile->id).'/subjects')?>" class="text-danger confirm" title="Are sure you want to remove <?=$sub_val->subject?>?"><span class="entypo-trash"></span></a></strong>
					</p>
				</td>
				<td><strong>Description :</strong><span class="label label-info label-lg" ><?=$subject_profile->subject?></span></td>
			</tr>
			<tr class="bold" >
				<td><strong>Year : </strong><span class="label label-info label-lg" ><?=$subject_profile->year?></span></td>
				<td><strong>Semester : </strong><span class="label label-info label-lg" ><?=$subject_profile->semester?></span></td>
			</tr>
		</table>
	</div>
	<!-- SINGLE ASSIGNMENT TAB -->	
	<div class="">
		<!-- FORM STARTS HERE -->
		<?=form_open('','class="single_subject_form form-horizontal" ')?>

		<fieldset>

			<!-- Form Name -->
			<legend><small>Subject Info</small></legend>

			<div class="form-group">
			  <label class="col-md-2 control-label" for="year_id">Year</label>  
			  <div class="col-md-6">
			  <!-- <input id="textinput" name="textinput" type="text" placeholder="placeholder" > -->
			  <?=year_dropdown('year_id',set_value('year_id',$subject_profile->year_id),'class="form-control input-md" id="year_id" required','Choose Year')?>
			  <span class="help-block">Select what year will the subject will be in.</span>  
			  </div>
			</div>

			<div class="form-group">
			  <label class="col-md-2 control-label" for="semester_id">Semester</label>  
			  <div class="col-md-6">
			  <?=semester_dropdown('semester_id',set_value('semester_id',$subject_profile->semester_id),'class="form-control input-md" id="semester_id" required','Choose Semester')?>
			  <span class="help-block">Select what semester will the subject will be in.</span>  
			  </div>
			</div>
		</fieldset>

		<fieldset>

			<!-- Form Name -->
			<legend><small>Subject Prerequisite (Optional)</small></legend>

			<div class="form-group">
			  <label class="col-md-3 control-label" for="req_year_id">Required Year Level (Optional)</label>  
			  <div class="col-md-5">
			  <!-- <input id="textinput" name="textinput" type="text" placeholder="placeholder" > -->
			  <?=year_dropdown('req_year_id',$subject_profile->req_year_id,'class="form-control input-md" id="req_year_id"','None')?>
			  <span class="help-block">Required Year Level for the subject selected above.</span>  
			  </div>
			</div>

			<div class="form-group">
			  <label class="col-md-3 control-label" for="total_units_req">Required Units (Optional)</label>  
			  <div class="col-md-5">
			  <input id="total_units_req" name="total_units_req" class="form-control input-md" value="<?=$subject_profile->total_units_req?>" type="number" step="any" placeholder="0" min = "0" max = "1000" />
			  <span class="help-block">Required units already finished by the enrollee.</span>  
			  </div>
			</div>

			<div class="form-group">
			  <label class="col-md-3 control-label" for="enrollment_units_req">Required Enrollment Units (Optional)</label>  
			  <div class="col-md-5">
			  <input id="enrollment_units_req" name="enrollment_units_req" class="form-control input-md" value="<?=$subject_profile->enrollment_units_req?>" type="number" step="any" placeholder="0" min = "0" max = "1000" />
			  <span class="help-block">Required units taken by the student during enrollment.</span>  
			  </div>
			</div>

			<div class="form-group">
			  <label class="col-md-3 control-label" for="semester_id">Prerequisite Subject/s (Optional)</label>  
			  <div class="col-md-5">
			  <?for($i = 1; $i <= 7; $i++):?>
			  		<?=form_dropdown('subject_pre['.$i.']', $subject_dropdown, iset($sub_pre[$i-1]->ref_id), 'class="form-control input-md" id="subject_id" class="form-control"' );?>
						<input type="checkbox" name="subject_pre_coincide[<?php echo $i; ?>]" id="" <?php echo isset($sub_pre[$i-1]) ? ($sub_pre[$i-1]->taken_at_once == 1 ? 'checked' : '' ) : '' ?> > Can be taken simultaneously during enrollment.		  		
						<p></p>
			  <?endfor;?>
			  <span class="help-block">Duplicate Subjects will not be added.</span>
			  </div>
			</div>
		</fieldset>

		<!-- Button (Double) -->
		<div class="form-group">
		  <label class="col-md-3 control-label" for="semester_id"></label>  
		  <div class="col-md-6">
		  	<input type="hidden" name="event_action" value="update_subject" />  
		  	<button class="btn btn-sm btn-dropbox" value="Update Subject" name="update_subject"><i class="fa fa-save"></i>&nbsp; Update Subject</button>
		  </div>
		</div>
		</form>
	</div>
<?else:?>
	<div class="alert alert-info">No Subject Selected, you can choose from the subjects tab</div>
<?endif;?>
</div>