<p>
  <label for="Package">Package Name</label>
	<?php 
		$data = array(
              'name'        => 'package[package]',
              'value'       => isset($package->package) ? $package->package : '',
              'maxlength'   => '100',
              'size'        => '50',
              'placeHolder' => 'Name',
              'required' => TRUE,
            );

		echo form_input($data);
	?>
</p>

<p>
  <label for="Package">Remarks</label>
  <?php 
    $data = array(
              'name'        => 'package[remarks]',
              'value'       => isset($package->remarks) ? $package->remarks : '',
              'maxlength'   => '100',
              'size'        => '50',
              'placeHolder' => 'Description',
              'required' => TRUE,
            );

    echo form_input($data);
  ?>
</p>

<p>
  <label for="Package">Level</label>
  <?php 
    $data = array(
              'name'        => 'package[level]',
              'value'       => isset($package->level) ? $package->level : 0,
              'maxlength'   => '100',
              'size'        => '50',
              'type'        => 'number',
              'class'       => 'form-control',
              'placeHolder' => 'Level',
              'required' => TRUE,
              'min' => 1,
            );

    echo form_input($data);
  ?>
</p>
<p>
  <label for="Package">Color</label>
  <input type="color" name="package[color_code]" class="form-control" value="<?=isset($package->color_code) ? $package->color_code : '#ffffff'?>" />
</p>
