<div class="row">
	<?@$this->load->view('packages/_department_profile')?>
</div>
<p></p>
<div class="row">
	<?if($department_menus):?>
		<?=form_open('');?>
			<table class="table">
				<tr>
					<th>
						<?if($department_menus):?>
							<div class="btn-group btn-group-xs">
								<button type="submit" name="show_selected_menu_group" value="show_selected_menu_group" class="btn btn-xs btn-default tp" data-toggle="tooltip" data-placement="top" title="Show Selected Menu Group"><span class="glyphicon glyphicon-eye-open"></span></button>
								<button type="submit" name="hide_selected_menu_group" value="hide_selected_menu_group" class="btn btn-xs btn-default tp" data-placement="top" title="Hide Selected Menu Group"><span class="glyphicon glyphicon-eye-close"></span></button>
								<button type="submit" name="del_selected_menu_group" value="del_selected_menu_group" class="btn btn-xs btn-danger tp" data-placement="top" title="Remove Selected Menu Group"><span class="glyphicon glyphicon-remove"></span></button>
							</div>
						<?endif;?>
					</th>
					<th>Menu Group</th>
					<th>Order</th>
					<th>Visible</th>
					<th>Action</th>
					<th>Menu List</th>
					<th><i>Visible</i></th>
				</tr>
				<?foreach($department_menus as $content):?>
					<?
						$head = $content['head'];
						$list = $content['menus'];
						$method = isset($content['menu_method'])?$content['menu_method']:false;
						
					?>
					
					<?if($head):?>
						<tr>
							<td><input type="checkbox" name="select_menu_group[]" value="<?=$head->id?>"></td>
							<td><input type="text" name="head_menu[<?=$head->id?>][caption]" value="<?=$head->caption?>"></td>
							<td><input type="number" min = "1" name="head_menu[<?=$head->id?>][menu_num]" class="form-control" value="<?=$head->menu_num?>"></td>
							<td>
								<?php
									unset($v_ari);
									$v_ari[0] = "No";
									$v_ari[1] = "Yes";
									echo form_dropdown("head_menu[$head->id][visible]", $v_ari, $head->visible);
								?>
							</td>
							<td>
								<p><button type="submit" name="update_header_menu" value="update_header_menu" class="btn btn-xs btn-dropbox tp" data-toggle="tooltip" data-placement="top" title="Update Menu Group"><i class="fa fa-pencil"></i>&nbsp; Update</button></p>
							</td>
							<td colspan="2" >
								<?if($list):?>
										<ul class="list-group checkbox_ul_<?=$head->id?>">
											<?
												foreach($list as $menu):
												$xcheckbox = $menu->visible == 1 ? 'checked' : ''; 
												$xstyle = $menu->visible == 1 ? 'info' : 'danger';
												$xicon = $menu->visible == 1 ? 'glyphicon glyphicon-check' : 'glyphicon glyphicon-unchecked';
												$xlbl = $menu->visible == 1 ? ' ' : ' ';
											?>
											<li class="list-group-item list-group-item-<?=$xstyle?>">
												<input class='menu_checkbox' type='checkbox' name='menu_select[]' id='cb_<?=$menu->id;?>' value='<?=$menu->id?>'/>
												<span class="<?=$xicon?>"></span> <?=$xlbl?>
												<?=$menu->caption?>
												<div class="pull-right">
													<input type="text" name='sub_menu_num[<?php echo $menu->id ?>]' size = "3" maxlength="5" value="<?php echo $menu->menu_num ?>" /> 
												</div>
											</li>
											<?endforeach;?>
											<li class="list-group-item">
												<button type="button" class="btn btn-xs btn-default tp checkall-checkbox" checkbox-class = "checkbox_ul_<?=$head->id?>" data-toggle="tooltip" data-placement="top" title="Check All"><i class="fa fa-check-square-o"></i>&nbsp;</button>
		                		<button type="button" class="btn btn-xs btn-default tp uncheckall-checkbox" checkbox-class = "checkbox_ul_<?=$head->id?>" data-toggle="tooltip" data-placement="top" title="Uncheck All"><i class="fa fa-square-o"></i>&nbsp;</button>
											</li>
										</ul>
										<div class="btn-group btn-group-xs">
											<button type="submit" name="show_selected_menu" value="show_selected_menu" class="btn btn-xs btn-default tp" data-toggle="tooltip" data-placement="top" title="Show Selected menus"><span class="glyphicon glyphicon-eye-open"></span> Show</button>
											<button type="submit" name="hide_selected_menu" value="hide_selected_menu" class="btn btn-xs btn-default tp" data-placement="top" title="Hide Selected menus"><span class="glyphicon glyphicon-eye-close"></span> Hide</button>
											<button type="submit" name="save_submenu_num" value="save_submenu_num" class="btn btn-xs btn-primary tp" data-toggle="tooltip" data-placement="top" title="Save order">Update</button>
										</div>
								<?endif;?>
							</td>
						</tr>
					<?endif;?>
				<?endforeach;?>
			</table>
		<?=form_close();?>
	<?else:?>
		<div class='alert alert-info'>No menu available. Select Department above.</div>
	<?endif;?>
</div>