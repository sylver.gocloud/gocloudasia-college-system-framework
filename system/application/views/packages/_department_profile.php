
<div class="table-responsive">
	<table class="table">
		<tr>
			<td class="bold">Selected Department : <span class="badge"><?=isset($department)&&$department?ucwords($department->department):'-None-'?></span>
				<div class="btn-group btn-group-xs">
				  <button type="button" class="btn btn-default">Change Department</button>
				  <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
				    <span class="caret"></span>
				    <span class="sr-only">Toggle Dropdown</span>
				  </button>
				  <ul class="dropdown-menu" role="menu">
					<?if($departments): foreach($departments as $k => $d):?>
						<li><a href="<?=site_url('packages/profile/menus/'.$package->id.'/'.$d->department_id)?>"><?=ucwords($d->department);?></a></li>
					<?endforeach; endif;?>
					</ul>
				</div>
			</td>
		</tr>
	</table>
</div>