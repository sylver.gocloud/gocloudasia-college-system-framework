<?if($packages):?>
  <?echo form_open('');?>
    <div class="row">
      <div class="table-responsive">
        <table class="table">
          <tr>
            <td>
              <strong>Package Source : &nbsp; <span class="btn btn-sm btn-default bold"><?=isset($package->package)?$package->package:'- No selected -'?></span></strong>
              <div class="btn-group">
                <button type="button" class="btn btn-dropbox">Select Package Source</button>
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                  <span class="caret"></span>
                  <span class="sr-only">Toggle Dropdown</span>
                </button>
                <ul class="dropdown-menu" role="menu">
                  <?foreach ($packages as $k => $p):?>
                    <li><a href="<?=site_url('packages/clone_package/'.$p->id)?>" class=""><span class="glyphicon glyphicon-ok"></span>&nbsp; <?=$p->package?></a></li>
                  <?endforeach;?>
                </ul>
              </div>
              <a href="<?=site_url('packages')?>" class="btn btn-default">Cancel</a>
            </td>
          </tr>
        </table>
      </div>

      <div class="table-responsive">
        <table class="table">
          <tr>
            <td class="bold">Copy From Package Departments :</td>
            <td>
              <?if($departments):?>
                <ul class="checkbox_dep_<?=$package->id?>" style=" list-style-type: none;">

                  <?foreach ($departments as $k => $dp):?>
                    <li class="">
                      <input type="checkbox" name="clone_department[]" checked value="<?=$dp->id?>">
                      <i class="fa fa-arrow-right"></i>&nbsp;
                      <strong><?=$dp->department?></strong>
                    </li>  
                  <?endforeach;?>
                    <li class="">
                      <button type="button" class="btn btn-xs btn-default tp checkall-checkbox" checkbox-class = "checkbox_dep_<?=$package->id?>" data-toggle="tooltip" data-placement="top" title="Check All"><i class="fa fa-check-square-o"></i>&nbsp;</button>
                      <button type="button" class="btn btn-xs btn-default tp uncheckall-checkbox" checkbox-class = "checkbox_dep_<?=$package->id?>" data-toggle="tooltip" data-placement="top" title="Uncheck All"><i class="fa fa-square-o"></i>&nbsp;</button>
                    </li>
                </ul>
              <?else:?>
                <div class="alert alert-info">No available departments.</div>
              <?endif;?>
            </td>
          </tr>
        </table>
      </div>
    </div>

    <?if($package):?>
      <p></p>
      <div class="row">
        <?@$this->load->view('packages/_form')?>
        <p>
          <button type="submit" name="clone_package" value="clone_package" class="btn btn-sm btn-success" ><span class="entypo-install"></span>&nbsp; Clone Package</button>
        </p>
      </div>

    <?else:?>
      <p></p>
      <div class="row">
        <div class="alert alert-info">
          No Package Source Selected
        </div>
      </div>
    <?endif;?>
  <?=form_close();?>
<?else:?>
  <div class="row">
    <div class="alert alert-info">No Package Source Available
      <a href="<?=site_url('packages/create')?>" class="btn btn-sm btn-dropbox"><i class="fa fa-plus"></i>&nbsp; Create Package</a>
      <a href="<?=site_url('packages')?>" class="btn btn-sm btn-default"> Cancel</a>
    </div>
  </div>
<?endif;?>