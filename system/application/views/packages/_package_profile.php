<div class="row">
	<div class="table-responsive">
		<table class="table">
			<tr style="background: <?=isset($package)?$package->color_code:''?>">
				<td class="bold" >Package : <span class="badge"><?=isset($package)?$package->package:''?></span></td>
				<td class="bold">Remarks : <span class="badge"><?=isset($package)?$package->remarks:''?></span></td>
				<td class="bold">Level : <span class="badge"><?=isset($package)?$package->level:''?></span></td>
				<td>
					<a href="<?=site_url('packages/destroy/'.$package->id)?>" class="btn btn-sm btn-danger confirm" title="Are you sure you want to remove this package and all its content?"><span class="entypo-trash"></span> Delete this Package</a>
					<a target="_blank" href="<?=site_url('packages/print_detailed/'.$package->id)?>" class="btn btn-sm btn-default"><span class="entypo-print"></span> Print</a>
					<a href="<?=site_url('packages')?>" class="btn btn-sm btn-default tp" data-toggle="tooltip" data-placement="top" title="Go Back to Package Lists"><span class="entypo-list"></span>Packages</a>
					</td>
			</tr>
		</table>
	</div>
</div>