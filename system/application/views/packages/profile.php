<?@$this->load->view('packages/_package_profile')?>

    <ul class="nav nav-tabs" id="myTab">
      <li class="<?=$tab == "edit" ? "active" : ''?>"><a href="#edit" data-toggle="tab"><span class="entypo-box"></span>&nbsp; Package Info</a></li>
      <li class="<?=$tab == "departments" ? "active" : ''?>" ><a href="#departments" data-toggle="tab"><span class="entypo-users"></span>&nbsp; Departments</a></li>
      <li class="<?=$tab == "menus" ? "active" : ''?>" ><a href="#menus" data-toggle="tab"><span class="entypo-list"></span>&nbsp; Menus</a></li>
      <li class="<?=$tab == "add_menu" ? "active" : ''?>" ><a href="#add_menu" data-toggle="tab"><span class="entypo-list-add"></span>&nbsp; Add Menu</a></li>
    </ul>

    <div class="tab-content">
        <div class="tab-pane <?=$tab == "edit" ? "active" : ''?>" id="edit">
          <?@$this->load->view('packages/_package_info')?>
        </div>
        <div class="tab-pane <?=$tab == "departments" ? "active" : ''?>" id="departments">
            <?@$this->load->view('packages/_package_departments')?>
        </div>
        <div class="tab-pane <?=$tab == "menus" ? "active" : ''?>" id="menus">
            <?@$this->load->view('packages/_package_menus')?>
        </div>
        <div class="tab-pane <?=$tab == "add_menu" ? "active" : ''?>" id="add_menu">
            <?@$this->load->view('packages/_add_menu')?>
        </div>
    </div>