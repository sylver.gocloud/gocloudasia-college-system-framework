<?if($input_save):
	$amt = $input_save['amount_paid'] == FALSE ? NULL : $input_save['amount_paid'];
	$rem = $input_save['spr_remarks'] == FALSE ? NULL : $input_save['spr_remarks'];
	$pdt = $input_save['spr_payment_date'] == FALSE ? NULL : $input_save['spr_payment_date'];
	$orn = $input_save['spr_or_no'] == FALSE ? NULL : $input_save['spr_or_no'];
	$mop = $input_save['spr_mode_of_payment'] == FALSE ? NULL : $input_save['spr_mode_of_payment'];
else:
	$amt = NULL;
	$rem = NULL;
	$pdt = NULL;
	$orn = NULL ;
	$mof = NULL ;
	$mop = NULL ;
endif;?>

<div class="row">
	<?echo form_open('', 'class="form-horizontal"');?>
	<div class="col-md-6">
		<?@$this->load->view('fees/account_summary');?>

		<div>
			<div class="panel panel-default tp" style="background:rgba(125,140,157,0.8);border:2px solid #000;" data-toggle="tooltip" data-placement="top" title="2. After selecting your payment division or other fees, you can edit the amount and payment details here.">
			  <div class="panel-heading" style="background:rgba(125,140,157,0.8);">
			    <h3 class="panel-title"><label class="label label-inverse black">Payment Record Form</label></h3>
			  </div>
			  <div class="panel-body">
			    <?if(isset($system_message))echo $system_message;?>
					<? echo validation_errors();?>
					<div id="warning_div" >
						
					</div>
					<div>
						<h5><label class="label label-primary"><strong>Amount</strong></label></h5>
						<input id="amount_money" type="text" name="ammount_paid"  placeholder="Amount" value="<?php echo set_value('ammount_paid',$amt);?>" autocomplete="off" required>
					</div>

					<br/>

					<div>
						<h5><label class="label label-primary"><strong>OR Number</strong></label></h5>
						<input type="text" name="spr_or_no" placeholder="O.R Number" value="<?php echo set_value('spr_or_no',$orn);?>" required>
					</div> 

					<br/>

					<div>
						<h5><label class="label label-primary"><strong>Date Of Payment (YYYY-MM-DD)</strong></label></h5>
						<input type="text" name="date_of_payment"  placeholder="Date: yyyy-mm-dd" class ="datepicker3" value="<?php echo set_value('date_of_payment',$pdt == NULL ? date('Y-m-d') : $pdt);?>" required>
					</div>

					<br/>
					
					<div>
						<h5><label class="label label-primary"><strong>Remarks</strong></label></h5>
						<textarea class="getremarks form-control" required url="<?=site_url('output/getremarks');?>" name="remarks"><?php echo set_value('remarks',$rem);?></textarea>
					</div>

					<br/>

					<div>
						<h5><label class="label label-primary"><strong>Type of Payment</strong></label></h5>
						<?=form_dropdown('payment_type',$payment_type,set_value('payment_type',$mop),'id="payment_type" required');?>
					</div>

					<br/>

					<div id='check_detail' style='display:none;'>

						<div>
							<h5><label class="label label-primary"><strong>Check Date (YYYY-MM-DD)</strong></label></h5>
							<input id="check_date" type="text" name="check_date" class='datepicker3' placeholder="Check Date" value="<?=date('Y-m-d')?>">
						</div>

						<br/>

						<div>
							<h5><label class="label label-primary"><strong>Check Number</strong></label></h5>
							<input type="text" name="check_number" placeholder="Check Number" value="">
						</div>

					</div>

					<input type="hidden" name="enrollment_id" value="<?echo $enrollment_id;?>">
					<?if($payment_totals->total_balance > 0):?>
					<input type="submit" class="btn btn-default btn-sm" name="add_payment_record" id="add_payment_record" value="Add Payment Record" disabled>
				<?endif;?>
			  </div>
			</div>
		</div>
	</div>

	
	<input type="hidden" id="total_payment_balance" value="<?=$_student->student_payment_totals->total_balance?>" />

	<div class="col-md-6">
		
		<div class="panel panel-default tp" id="panel_division" data-toggle="tooltip" data-placement="top" title="1. Select first your payment division or clearance fees here.">
		  <div class="panel-heading black center"><strong>Student Current Payment Division</strong> <i style="font-size:7pt;" >( Student Applied Payment Plan / Payment Division )</i></div>
		  <div class="panel-body">
		    <div class="table-responsive">
				<table class="table">
				<?$student_current_paymentmode = $_student->student_plan_modes?>
				<?if($student_current_paymentmode):?>
					<tbody>
						<thead>
							<td>#</td>
							<td style="text-align:right;">Payment Due</td>
							<td style="text-align:right;">Paid</td>
						</thead>
						<?foreach($student_current_paymentmode as $v):?>
								<?
									$sum['due'][] = $v->value;
									$sum['paid'][] = $v->amount_paid;
								?>
								<tr>
									<td>
											<?if($v->is_paid == 0):?>
												<?if($v->is_check == 1):?>
													<i data-tooltip class="fa fa-credit-card has-tip" style='cursor:pointer' title='Waiting for the cheque to be applied.'></i>
												<?else:?>
													<i data-tooltip class="fa fa-reply has-tip" xid='<?=$v->id?>' xamount='<?=$v->value?>' style='cursor:pointer' title='Copy this Payment Amount to Form'></i>
													<input type='checkbox' name='payment_for[tuition][]' class='tuition_checkbox' value='<?=$v->id?>' xid='<?=$v->id?>' xamount='<?=$v->value - $v->amount_paid?>' />
												<?endif;?>
											<?else:?>
													<i data-tooltip class="fa fa-check has-tip" style='cursor:pointer' title='PAID'></i>
											<?endif;?>

										<?=$v->name;?>
									</td>
									<td  style="text-align:right;"><?=mowney($v->value);?></td>
									<?if($v->amount_paid == 0.00):?>
										<td style="color:#f00;font-weight:bold;text-align:right;"><?=mowney($v->amount_paid);?></td>
									<?else:?>
										<td  style="color:#035F11;font-weight:bold;text-align:right;"><?=mowney($v->amount_paid);?></td>
									<?endif;?>
								</tr>
						<?endforeach;?>
								<tr class="info">
									<td></td>
									<td style="color:#1212FC;font-weight:bold;text-align:right;"><?=mowney(array_sum($sum['due']));?></td>
									<td style="color:#1212FC;font-weight:bold;text-align:right;"><?=mowney(array_sum($sum['paid']));?></td>
								</tr>
						<?$others = $_student->student_other_fees?>
						<?if($others):?>
							<tr>
								<td colspan="3" style="font-weight:bold;">Additional / Other Fees</td>
							</tr>
							<?foreach($others as $k => $v):?>
								<?if($type == 'spb'):?>
								<tr>
									<td><?=$v->spb_fee_name;?></td>
									<td style="text-align:right;"><?=mowney($v->sef_fee_rate);?></td>
									<td style="text-align:right;"><?=mowney($v->spb_amount_paid);?></td>
								</tr>
								<?else:?>
									<?
										$oth_sum['due'][] = $v->sef_fee_rate;
										$oth_sum['paid'][] = $v->amount_paid;
									?>
									<tr>
										<td>
												<?if($v->is_paid == 0):?>
													<?if($v->is_check == 1):?>
														<i data-tooltip class="fa fa-credit-card has-tip" style='cursor:pointer' title='Waiting for the cheque to be applied.'></i>
													<?else:?>
														<i data-tooltip class="fa fa-reply has-tip" xid='<?=$v->sef_id?>' xamount='<?=$v->sef_fee_rate?>' style='cursor:pointer' title='Copy this Payment Amount to Form'></i>
														<input type='checkbox' name='payment_for[other][]' class='tuition_checkbox' value='<?=$v->sef_id?>' xid='<?=$v->sef_id?>' xamount='<?=$v->sef_fee_rate - $v->amount_paid?>' />
													<?endif;?>
												<?else:?>
													<i data-tooltip class="fa fa-check has-tip" style='cursor:pointer' title='PAID'></i>
												<?endif;?>
												<?=$v->sef_fee_name;?>
										</td>
										<td><?=mowney($v->sef_fee_rate);?></td>
										<?if($v->amount_paid == 0.00):?>
											<td style="color:#f00;font-weight:bold;text-align:right;"><?=mowney($v->amount_paid);?></td>
										<?else:?>
											<td  style="color:#035F11;font-weight:bold;text-align:right;"><?=mowney($v->amount_paid);?></td>
										<?endif;?>
									</tr>
								<?endif?>
							<?endforeach;?>
								<tr class="info">
									<td></td>
									<td style="color:#1212FC;font-weight:bold;text-align:right;"><?=mowney(array_sum($oth_sum['due']));?></td>
									<td style="color:#1212FC;font-weight:bold;text-align:right;"><?=mowney(array_sum($oth_sum['paid']));?></td>
								</tr>
						<?endif;?>

						<?$prev_accounts = $_student->student_previous_account?>
						<?if($prev_accounts):?>
							<tr>
								<td colspan="3" style="font-weight:bold;">Old Accounts</td>
							</tr>
							<?foreach($prev_accounts as $k => $v):?>
								<?
									$prev_sum['due'][] = $v->value;
									$prev_sum['paid'][] = $v->amount_paid;
								?>
								<tr>
									<td>
											<?if($v->is_paid == 0):?>
												<?if($v->is_check == 1):?>
													<i data-tooltip class="fa fa-credit-card has-tip" style='cursor:pointer' title='Waiting for the cheque to be applied.'></i>
												<?else:?>
													<i data-tooltip class="fa fa-reply has-tip" xid='<?=$v->id?>' xamount='<?=$v->value?>' style='cursor:pointer' title='Copy this Payment Amount to Form'></i>
													<input type='checkbox' name='payment_for[previous][]' class='tuition_checkbox' value='<?=$v->id?>' xid='<?=$v->id?>' xamount='<?=$v->value - $v->amount_paid?>' />
												<?endif;?>
											<?else:?>
													<i data-tooltip class="fa fa-check has-tip" style='cursor:pointer' title='PAID'></i>
											<?endif;?>
											<?=$v->year;?> ( <?=$v->sy_from;?>-<?=$v->sy_to;?> )
									</td>
									<td><?=mowney($v->value);?></td>
									<?if($v->amount_paid == 0.00):?>
										<td style="color:#f00;font-weight:bold;text-align:right;"><?=mowney($v->amount_paid);?></td>
									<?else:?>
										<td  style="color:#035F11;font-weight:bold;text-align:right;"><?=mowney($v->amount_paid);?></td>
									<?endif;?>
								</tr>
							<?endforeach;?>
								<tr class="info">
									<td></td>
									<td style="color:#1212FC;font-weight:bold;text-align:right;"><?=mowney(array_sum($prev_sum['due']));?></td>
									<td style="color:#1212FC;font-weight:bold;text-align:right;"><?=mowney(array_sum($prev_sum['paid']));?></td>
								</tr>
						<?endif;?>
						
					</tbody>
				<?else:?>
				<?endif;?>
				</table>
			</div>
		  </div>
		</div>
	</div>
	</form>
</div>