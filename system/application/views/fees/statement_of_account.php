<div class="row">
	<?@$this->load->view('fees/student_profile_head');?>
</div>

<div class="row">
	<div class="well">
		<div class="btn-group">
			<a href="<?=site_url('fees/view_fees/'.$_student->enrollment_id)?>" class="btn btn-sm btn-bitbucket"><i class="glyphicon glyphicon-asterisk"></i>&nbsp; Fees</a>	
			<a target="_blank" href="<?=site_url('fees/print_statement_of_account/'.$_student->enrollment_id)?>" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-print"></span>&nbsp; Print SOA</a>	
		</div>

		<div class="btn-group">
			<a href="<?=site_url('fees/student_ledger/'.$_student->enrollment_id)?>" class="btn btn-sm btn-facebook"><span class="glyphicon glyphicon-stats"></span>&nbsp; Student Ledger</a>
		</div>

	</div>
</div>

<div class="row">

	<?//@$this->load->view('layouts/_panel_head', array('panel_title'=>'STUDENT STATEMENT'))?>
	<?=panel_head2('Student Statement of Account')?>
		<div class="table-responsive">
			<table class="table">
				<tr>
					<td class="bold info" >Previous Balance</td>
					<td class="text-right info" ><?=money($_student->student_payment_totals->total_previous_amount)?></td>
				</tr>

				<tr>
					<td class="bold info">Amount Due</td>
					<td class="text-right info" ><strong><?=money($_student->student_payment_totals->total_amount_due)?></strong></td>
				</tr>

				<!-- TUITION FEE -->
				<?if($student_of_account->tuition_fee):?>
				<?$tuition_fee = (object)$student_of_account->tuition_fee;?>
				<tr>
					<td class="bold" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <?=$tuition_fee->name?> (<?=money($_student->profile->tuition_fee_multiplier);?> * <?=money($_student->profile->total_units);?>)</td>
					<td class="text-right" ><?=money($_student->profile->tuition_fee)?>&nbsp;&nbsp;&nbsp;</td>
				</tr>
				<?endif;?>

				<!-- LAB FEE -->
				<tr>
					<td class="bold" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Lab Fee</td>
					<td class="text-right bold" ><?=money($_student->profile->lab_fee)?>&nbsp;&nbsp;&nbsp;</td>
				</tr>

				<!-- LAB BREAKDOWN -->
				<?if(isset($student_of_account->lab_fee) && $student_of_account->lab_fee):?>
					
					<?foreach ($student_of_account->lab_fee as $key => $value):?>
					<?if(strtolower($key) === "total"){ continue; }?>
					<tr>
						<?$value = (object)$value;?>
						<td class="italic" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Lab Fee<?=$value->name?> (<?=money($value->rate);?> * <?=money($value->units);?>)</td>
						<td class="text-right" ><?=money($value->amount)?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
					</tr>
					<?endforeach;?>
					
				<?endif;?>

				<!-- MISC FEE -->
				<tr>
					<td class="bold" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Miscellaneous Fee</td>
					<td class="text-right bold" ><?=money($_student->profile->misc_fee)?>&nbsp;&nbsp;&nbsp;</td>
				</tr>
				<!-- MISC BREAKDOWN -->
				<?if(isset($student_of_account->misc_fee) && $student_of_account->misc_fee):?>
					
					<?foreach ($student_of_account->misc_fee as $key => $value):?>
					<?if(strtolower($key) === "total"){ continue; }?>
					<tr>
						<?$value = (object)$value;?>
						<td class="italic" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <?=$value->name?></td>
						<td class="text-right" ><?=money($value->amount)?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
					</tr>
					<?endforeach;?>
					
				<?endif;?>

				<!-- DEDUCTION AND SCHOLARSHIPS AND EXCESS PAYMENT-->
				<tr>
					<td class="bold info" >Discounts / Adjustments</td>
					<td class="text-right bold info" ><?=money($_student->student_payment_totals->total_deduction+$_student->student_payment_totals->total_scholarship+$_student->student_payment_totals->total_excess_amount)?></td>
				</tr>

				<!-- Scholarship -->
				<tr>
					<td class="bold" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Scholarship <?=$student_of_account->scholarships?'':' (None) '?></td>
					<td class="text-right bold" ><?=money($_student->student_payment_totals->total_scholarship)?>&nbsp;&nbsp;&nbsp;</td>
				</tr>
				<!-- Scholarship BREAKDOWN -->
				<?if(isset($student_of_account->scholarships) && $student_of_account->scholarships):?>
					
					<?foreach ($student_of_account->scholarships as $key => $value):?>
					<?if(strtolower($key) === "total"){ continue; }?>
					<tr>
						<td class="italic" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <?=$value->scholarship_name?></td>
						<td class="text-right" ><?=money($value->scho_amount)?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
					</tr>
					<?endforeach;?>
					
				<?endif;?>

				<!-- Deduction -->
				<tr>
					<td class="bold" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Deduction <?=$student_of_account->deductions?'':' (None) '?></td>
					<td class="text-right bold" ><?=money($_student->student_payment_totals->total_deduction)?>&nbsp;&nbsp;&nbsp;</td>
				</tr>
				<!-- Deduction BREAKDOWN -->
				<?if(isset($student_of_account->deductions) && $student_of_account->deductions):?>
					
					<?foreach ($student_of_account->deductions as $key => $value):?>
					<?if(strtolower($key) === "total"){ continue; }?>
					<tr>
						<td class="italic" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <?=$value->deduction_name?></td>
						<td class="text-right" ><?=money($value->amount)?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
					</tr>
					<?endforeach;?>
					
				<?endif;?>

				<!-- Excess Payment -->
				<tr>
					<td class="bold" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Advanced Payments <?=$student_of_account->excess_payments?'':' (None) '?></td>
					<td class="text-right bold" ><?=money($_student->student_payment_totals->total_deduction)?>&nbsp;&nbsp;&nbsp;</td>
				</tr>
				<!-- Deduction BREAKDOWN -->
				<?if(isset($student_of_account->excess_payments) && $student_of_account->excess_payments):?>
					
					<?foreach ($student_of_account->excess_payments as $key => $value):?>
					<?if(strtolower($key) === "total"){ continue; }?>
					<tr>
						<td class="italic" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <?=$value->year?> (<?=$value->sy_from?>-<?=$value->sy_to?>)</td>
						<td class="text-right" ><?=money($value->amount)?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
					</tr>
					<?endforeach;?>
					
				<?endif;?>

				<!-- Clearance fees/other fees-->
				<tr>
					<td class="bold info" >Clearance / Other fees</td>
					<td class="text-right bold info" ><?=money($_student->profile->other_fee)?></td>
				</tr>
				<!-- Other fees BREAKDOWN -->
				<?if(isset($student_of_account->other_fee) && $student_of_account->other_fee):?>
					
					<?foreach ($student_of_account->other_fee as $key => $value):?>
					<?if(strtolower($key) === "total"){ continue; }?>
					<?$value = (object)$value;?>
					<tr>
						<td class="" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <?=$value->name?></td>
					<td class="text-right" ><?=money($value->amount)?>&nbsp;&nbsp;&nbsp;</td>
					</tr>
					<?endforeach;?>
					
				<?endif;?>


				<!-- Total Amount Paid -->
				<tr>
					<td class="bold info" >Total Amount Paid </td>
					<td class="text-right bold info" ><?=money($_student->student_payment_totals->total_amount_paid)?></td>
				</tr>
				<!-- Payments Record -->
				<?if(isset($student_of_account->payment_record) && $student_of_account->payment_record):?>
					
					<?$ctr = 1;?>
					<?foreach ($student_of_account->payment_record as $key => $value):?>
					<? $value = (object)$value; ?>
					<tr>
						<td class="" >
							<div class="row">
								<div class="col-md-3" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <?=$ctr++;?>. &nbsp; <?=$value->spr_or_no?>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
								<div class="col-md-3" ><?=date('m-d-Y',strtotime($value->spr_payment_date))?></div>
							</div>
						</td>
					<td class="text-right" ><?=money($value->spr_ammt_paid)?>&nbsp;&nbsp;&nbsp;</td>
					</tr>
					<?endforeach;?>
					
				<?endif;?>

				<!-- Total Balance -->
				<tr>
					<td class="bold info" >Current Balance </td>
					<td class="text-right bold info" ><?=money($_student->student_payment_totals->total_balance)?></td>
				</tr>

				<!-- Required Payment this current grading -->
				<tr>
					<td class="bold info" >Required for '<?=$this->current_grading_period->grading_period;?>'</td>
					<td class="text-right bold info" ><?=money($req_payment)?></td>
				</tr>

				<!-- In Charge -->
				<tr>
					<td colspan = '2' class="bold info text-center" >In-charge - Accounting</td>
				</tr>
			</table>
		</div>

	<?//@$this->load->view('layouts/_panel_foot')?>
	<?=panel_tail2();?>
	</div>