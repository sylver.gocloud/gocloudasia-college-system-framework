<?php $search = isset($search_type)?'/'.$search_type:''; ?>
<div class='panel panel-primary'>
	<!-- Heading -->
  <div class='panel-heading'>
  	<h3 class='panel-title'>Enrollment Profile<span class='pull-right clickable' style = 'cursor:pointer;'><i class='glyphicon glyphicon-chevron-up'></i></span></h3>
	</div>
	
	<!-- Body -->
	<div class='panel-body' style='padding : 3px !important;'>
  	<div class=''>
			<div class="table-responsive">	  	
				<table class="table table-bordered">
					<tr>
						<td><span class="bold">Student Name:</span></td>
						<td  colspan="4" class="student-name">
							<strong><?php echo ucwords($_student->profile->full_name);?></strong>
							&nbsp;&nbsp;
							<?if(isset($show_tri_menu) && $show_tri_menu ):?>
								<?if($mydepartment->stud_profile == 1):?>
								<a class="btn btn-sm btn-primary" href="<?=base_url('profile/view/'.$_student->enrollment_id.$search)?>"><i class="fa fa-file-text"></i>&nbsp;  Profile</a>
								<?endif;?>
								
								<?if($mydepartment->stud_fees == 1):?>
								<a class="btn btn-sm btn-primary" href="<?=base_url('fees/view_fees/'.$_student->enrollment_id.$search)?>"><i class="fa fa-list-alt"></i>&nbsp;  Fees</a>
								<?endif;?>

								<?if(isset($show_payment_btn) && $show_payment_btn):?>
									<?if($this->router->method == "show_payment" || $this->router->method == "add_student_payment" && $mydepartment->stud_fees == 1):?>
									<a class="btn btn-sm btn-primary" href="<?=base_url('fees/payment_record/'.$_student->enrollment_id.$search)?>"><i class="fa fa-list-alt"></i>&nbsp;  Payment Records</a>
									<?endif;?>
									<?if($this->router->method == "payment_record" && $mydepartment->stud_add_fees === "1"):?>
									<a class="btn btn-sm btn-success confirm" href="<?=base_url('fees/add_student_payment/'.$_student->enrollment_id.$search)?>"><i class="fa fa-list-alt"></i>&nbsp;  Add Payment</a>
									<?endif;?>

								<?endif;?>
								
							<?endif;?>
						</td>
					</tr>
					<tr>
						<td class="student-name">
							<span class="bold">ID Number</span>
						</td>
						<td class="student-name">	
							<span class="bold">Course & Major</span>
						</td>
						<td class="student-name">
							<span class="bold">Year</span>
						</td>
						<td class="student-name">
							<span class="bold">Semester</span>
						</td>
						<td class="student-name">
							<span class="bold">School Year</span>
						</td>
						<td class="student-name">
							<span class="bold">Payment Status</span>
						</td>
					</tr>
					<tr>
						<td class="student-name">
							<span class="label label-default label-lg"><?php echo ucwords($_student->studid);?></span>
						</td>
						<td class="student-name">
							<span class="label label-default label-lg"><?php echo ucwords($_student->profile->course);?></span>	
							<?if($_student->profile->specialization):?>
								<p></p>
								<span class="label label-default label-lg"><?php echo ucwords($_student->profile->specialization);?></span>
							<?endif;?>
						</td>
						<td class="student-name">
							<span class="label label-default label-lg"><?php echo $_student->profile->year;?></span>
						</td>
						<td class="student-name">
							<span class="label label-default label-lg"><?php echo $_student->profile->name;?></span>
						</td>
						<td class="student-name">
							<span class="label label-default label-lg"><?php echo $_student->profile->sy_from;?>-<?php echo $_student->profile->sy_to;?></span>
						</td>
						<td class="student-name">
							<span class="label label-default label-lg"><?php echo $_student->profile->payment_status;?></span>
						</td>
					</tr>
					</tr>
				</table>
			</div>
		</div> 
	</div>

	<!-- Footer -->
	<?if($_student->profile->is_paid == "1"
	     && ($mydepartment->stud_portal_activation == "1")):?>
		<div class="panel-footer" style="padding:2px">

			<!-- Student Portal Activation Buttons -->
			<?php
				$spa = $_student->profile->is_activated;
				$to_url = $spa == "1" ? "0" : "1";
				$spa_check_icon = $spa == "1" ? '<i class="fa fa-check-square-o"></i>' : '<i class="fa fa-square-o"></i>';
				$spa_active = $spa == "1" ? "de-activate" : "activate";
			?>
			<div class="row">
				<div class="col-lg-12">
					<div class="btn-group dropup btn-xs">
						<a href="#" class="btn btn-default"><?php echo $_student->profile->is_activated == "1" ? "Student Portal is <span class='text-success'>Active</span>" : "Student Portal is <span class='text-danger'>Inactive</span>" ?></a>
					  <a href="<?=site_url('profile/activate_portal/'.__link($_student->profile->id).'/'.$to_url)?>" class="btn btn-default dropdown-toggle confirm" title="Are you sure to <span class='bold text-danger'><?=$spa_active?></span> Student Portal of <span class='bold txt-facebook'><?=$_student->profile->fullname?></span>?">
					  	<?php echo $spa_check_icon ?>
					  </a>
					  <ul class="dropdown-menu" role="menu">
					    <li><a href="#">Separated link</a></li>
					  </ul>
					</div>
				</div>
			</div>
		</div>		
	<?endif;?>

</div>