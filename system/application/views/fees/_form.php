
 <?php 
		$name = array(
              'name'        => 'fees[name]',
              'value'       => isset($fees->name) ? $fees->name : '',
              'maxlength'   => '100',
              'size'        => '50',
              'style'       => 'width:50%',
              'required'       => '',
              'placeHolder'       => 'Required',
            );

		$value = array(
              'name'        => 'fees[value]',
              'value'       => isset($fees->value) ? $fees->value : '',
              'maxlength'   => '100',
              'size'        => '50',
              'style'       => 'width:50%',
              'required'       => '',
              'placeHolder'       => '0.00',
            );
		

		$is_deduction = array(
			'name'        => 'fees[is_deduction]',
			'value'		  => 'yes',
			'checked'     => isset($fees->is_deduction) ? ($fees->is_deduction == 1 ? true : false) : false,
			);
		
		$active = array(
			'name'        => 'fees[is_active]',
			'value'		  => 'yes',
			'checked'     => isset($fees->is_active) ? ($fees->is_active == 1 ? true : false) : true,
			);
			
		$donot_show_in_old = array(
			'name'        => 'fees[donot_show_in_old]',
			'value'		  => 'yes',
			'checked'     => isset($fees->donot_show_in_old) ? ($fees->donot_show_in_old == 1 ? true : false) : false,
			);
		
		$is_tuition_fee = array(
			'name'        => 'fee_type',
			'value'		  => 'is_tuition_fee',
			'checked'     => isset($fees->is_tuition_fee) ? ($fees->is_tuition_fee == 1 ? true : false) : $fee_type == "TUITION" ? true : false,
			);
			
		$is_lab = array(
			'name'        => 'fee_type',
			'value'		  => 'is_lab',
			'checked'     => isset($fees->is_lab) ? ($fees->is_lab == 1 ? true : false) : $fee_type == "LAB" ? true : false,
			);
			
		$is_misc = array(
			'name'        => 'fee_type',
			'value'		  => 'is_misc',
			'checked'     => isset($fees->is_misc) ? ($fees->is_misc == 1 ? true : false) : $fee_type == "MISC" ? true : false,
			);
		$is_other = array(
			'name'        => 'fee_type',
			'value'		  => 'is_other',
			'checked'     => isset($fees->is_other) ? ($fees->is_other == 1 ? true : false) : $fee_type == "OTHER" ? true : false,
			);	
		
		$is_nstp = array(
			'name'        => 'fee_type',
			'value'		  => 'is_nstp',
			'checked'     => isset($fees->is_nstp) ? ($fees->is_nstp == 1 ? true : false) : $fee_type == "NSTP" ? true : false,
			);

		$is_other_school = array(
			'name'        => 'fee_type',
			'value'		  => 'is_other_school',
			'checked'     => isset($fees->is_other_school) ? ($fees->is_other_school == 1 ? true : false) : $fee_type == "OTHER_SCHOOL" ? true : false,
			);
			
?>

  <p>
    <label for="fee">Fee Name</label><br/>
	<?php 
		echo form_input($name);
	?>
  </p>

   <p>
    <label for="fee">Default Value</label><br/>
	<?php 
		echo form_input($value);
	?>
  </p>
  
 <!--  <p>
	<?php 
		//echo form_checkbox($is_deduction);
	?>
    <label for="is_deduction">Is Deduction ?</label><br />
  </p> -->
  
  <p>
	<?php 
		echo form_checkbox($active);
	?>
	<label for="is_active">Is Active ?</label><br />
  </p>  
  
 <!--  <p>
	<?php 
		//echo form_checkbox($donot_show_in_old);
	?>
	<label for="donot_show_in_old">Do not show in old student ?</label><br />
  </p> -->
  
    <p>
	<label for="is_misc">Is Tuition Fee ?</label>
	<?php 
		echo form_radio($is_tuition_fee);
	?>
  </p>

  <p>
	<label for="is_misc">Is Lab. ?</label>
	<?php 
		echo form_radio($is_lab);
	?>
  </p>
  
   <p>
	<label for="is_misc">Is Misc. ?</label>
	<?php 
		echo form_radio($is_misc);
	?>
  </p>

  <p>
	<label for="is_misc">Is Other School Fee ?</label>
	<?php 
		echo form_radio($is_other_school);
	?>
  </p>
  
  <p>
	<label for="is_nstp">Is NSTP ?</label>
	<?php 
		echo form_radio($is_nstp);
	?>
  </p>

  <p>
	<label for="is_other">Is Other Fee ?</label>
	<?php 
		echo form_radio($is_other);
	?>
  </p>  

