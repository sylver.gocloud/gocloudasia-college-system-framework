<?=panel_head2('Account Summary')?>
	<div class="table-responsive">
		<table style="width:100%;">
			<tbody class="table table-bordered table-hover table-striped">
				<tr class="tr_account_summary" >
					<td>Total Payables:  <br/><code>( Tuition + Misc. + Lab + Nstp + Other School Fees + Old Account + Clearance Fees )</code></td>
					<td>
						<span style="font-size:14px;" >&#8369;</span>
						<span style="color:#000;font-size:20px;font-weight:bold;"><?=num_format(floatval($_student->student_payment_totals->total_amount_due));?></span>
					</td>
				</tr>
				<?if($_student->has_old_account):?>
				<!-- <tr class="tr_account_summary" >
					<td>Total Previous Unpaid Account:</td>
					<td>
						<span style="font-size:14px;" >&#8369;</span>
						<span style="color:#000;font-size:20px;font-weight:bold;"><?=num_format(floatval($_student->student_payment_totals->total_previous_amount));?></span>
					</td>
				</tr> -->
				<?endif;?>
				<tr class="tr_account_summary" >
					<td>Total Deduction: <code>(Deduction + Scholarship + Excess Payments)</code></td>
					<td>
						<span style="font-size:14px;" >&#8369;</span>
						<span style="color:#000;font-size:20px;font-weight:bold;"><?=num_format(floatval($_student->student_payment_totals->total_deduction_amount));?></span>
					</td>
				</tr>
				<tr class="tr_account_summary" >
					<td>Total Paid:</td>
					<td>
						<span style="font-size:14px;" >&#8369;</span>
						<span style="color:#000;font-size:20px;font-weight:bold;"><?=num_format(floatval($_student->student_payment_totals->total_amount_paid));?></span>
					</td>
				</tr>
				<tr class="tr_account_summary" >
					<td>Current balance:</td>
					<td>
						<span style="font-size:14px;" >&#8369;</span>
						<span style="color:#AB4705;font-size:20px;font-weight:bold;"><?=num_format(floatval($_student->student_payment_totals->total_balance));?></span>
						<input type='hidden' id='txt_total_balance' value='<?=$_student->student_payment_totals->total_balance?>'>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
<?=panel_tail2();?>