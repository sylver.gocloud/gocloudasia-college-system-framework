<style>
	input#amount_money
	{
		padding:10px 20px;
		font-size:30px;
		font-family:DigitaldreamFatNarrowRegular;
		text-align:right;
	}
	span#amount_money_span
	{
		position:absolute;
		font-size:30px;
		margin-top:12px;
		margin-left:12px;
	}
</style>

<script type="text/javascript">
	
	$(document).ready(function(){
		$('.tuition_checkbox').on('change', function(){
			
			var xid = $(this).attr('xid');
			var xamount = Number($(this).attr('xamount'));
			
			var xcur_amount = Number($('#amount_money').val().trim() == "" ? 0 : $('#amount_money').val());
			var xsel_amount = get_selected_amount();
			
			xcur_amount = isNaN(xcur_amount) ? 0 : xcur_amount;					

			if($(this).is(':checked'))
			{
				if(xsel_amount > xcur_amount){
					var xbal = xsel_amount - xcur_amount;

					var xnew_amount = xcur_amount + xbal;
					$('#amount_money').val(xnew_amount.toFixed(2));
				}
				$(this).parent().parent().css('background-color','#08c');
			}else{
				if(xcur_amount > 0){
					var xnewamount = (parseFloat(xcur_amount) - parseFloat(xamount)).toFixed(2);
					$('#amount_money').val(xnewamount <= 0 ? 0 : xnewamount);
					$(this).parent().parent().css('background-color','#FFF');
				}
			}

			check_amount();

		});

		// check_amount();

		$('#amount_money').on('keyup', function(){
			check_amount();
		})

		$('#payment_type').change(function(){
			var xval = $(this).val();
			if(xval == 'CHECK'){
				$('#check_detail').show('slow');
			}else{
				$('#check_detail').hide();
			}
		});

		var xmsg = "<?=$system_message?>";

		if(xmsg != "" && xmsg != null && xmsg != "NULL"){
			growl('Payment Record Added', xmsg);
		}

		//VALIDATE FORM
		$('.add_form').submit(function(event){
			
			var xtot_bal = parseFloat($('#txt_total_balance').val().trim());
			var xtot_amount_paid = parseFloat($('#amount_money').val().trim());
			if( isNumber($('#amount_money').val()) == true)
			{
				if(xtot_bal <= 0){
					event.preventDefault();
					confirm_message('Warning','Student Already Paid');
				}else if(xtot_amount_paid > xtot_bal){
					event.preventDefault();
					confirm_message('Warning','<p>Amount is already greater than the current balance. This maybe because of the applied deduction of the student. Please edit the amount and try again.</p>', 300, 450);
				}else{}
			}else{
				event.preventDefault();
				confirm_message('Error','Invalid Amount');
			}

		})

	});

	function check_amount(){

		var xamount = $('#amount_money').val().trim();
		var xcheck = $('.tuition_checkbox:checked').size();

		//set button to panel-default
		$('#add_payment_record').removeClass('btn-success');
		$('#add_payment_record').addClass('btn-default');
		
		if(xcheck <= 0){

			//IF NO SELECTED PAYMENT DIVISION OR OTHER FEES
			$('#add_payment_record').attr('disabled', true);
			$('#panel_division').removeClass('panel-default');
			$('#panel_division').addClass('panel-danger');

			$('#warning_div').html('<p class="alert alert-danger alert-xs" >No payment division or other fee/s seleted. </p>');
		}else{

			if(parseFloat(xamount) > 0){

				var xtotal_balance = parseFloat($('#total_payment_balance').val());

				if(parseFloat(xamount) > xtotal_balance){

					//IF THE ENTERED AMOUNT IS GREATER THAN THE TOTAL BALANCE
					$('#warning_div').html('<p class="alert alert-danger alert-xs" >Entered Amount is already greater than the total balance.</p>');
					$('#add_payment_record').attr('disabled', true);
				}
				else
				{
					var xsel_amount = get_selected_amount();

					if(parseFloat(xamount) > xsel_amount){

						//IF THE Entered AMOUNT IS GREATER THAN THE SELECTED AMOUNT OF PAYMENT DIVISION OR OTHER FEES
						$('#warning_div').html('<p class="alert alert-danger alert-xs" >Entered Amount is already greater total of selected division / other fees.</p>');
						$('#add_payment_record').attr('disabled', true);		
					}
					else
					{
						$('#add_payment_record').attr('disabled', false);
						$('#panel_division').addClass('panel-default');
						$('#panel_division').removeClass('panel-danger');

						$('#warning_div').html('');

						$('#add_payment_record').removeClass('btn-default');
						$('#add_payment_record').addClass('btn-success');
					}
				}

			}else{

				//IF AMOUNT IS INVALID OR 0
				$('#add_payment_record').attr('disabled', true);
				$('#panel_division').removeClass('panel-default');
				$('#panel_division').addClass('panel-danger');

				$('#warning_div').html('<p class="alert alert-danger alert-xs" >No amount or invalid amount. </p>');
			}
		}
	}

	function get_selected_amount() {

		var xamount = 0;

		$('.tuition_checkbox:checked').each(function(){
			xamount = xamount + Number($(this).attr('xamount'));
		});

		return xamount.toFixed(2);
	}

</script>
<?if(isset($action) && $action === "print_form"):?>
	<?="<script>"?>
		<?= 	"var win = window.open('".site_url('printables/registration_form/'.$enrollment_id)."', '_bank');" ?>
		<?= 	'if(win){'; ?>
		<?= 		'win.focus();' ?>
		<?= 	'}else{;' ?>
		<?=	 	"alert('Please enable Browser Pop-out feature');" ?>
		<?= 	'}'; ?>
	<?="</script>"?>
<?endif;?>
<div class="row">

	<?php
		$search_param = array(
				'type' => 'all',
				'destination' => site_url('fees/add_student_payment'),
			);

		@$this->load->view('layouts/search_student', $search_param);?>
	<p></p>
	<?@$this->load->view('fees/student_profile_head');?>
	<div class="section-container auto" data-section data-options="deep_linking: true">
		<ul class="nav nav-tabs">
		  <li class="active"><a href="#">Fees</a></li>
		</ul>
		<hr/>
		<div>
			<?@$this->load->view('fees/apr_structured')?>
		</div>
	</div>
</div>
