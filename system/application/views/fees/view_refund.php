
<table >
	<tr>
		<th>Date</th>
		<th>Refund Amount</th>
		<th>Remarks</th>
		<!--th>Action</th-->
	</tr>
	<?
	$ctr = 0;
	?>
	<?if($student_refund):?>
	<?$total_amount = 0;?>
	<?foreach($student_refund as $obj):?>
		<?$total_amount += $obj->amount?>
		<tr>	
			<td><?=$obj->date;?></td>
			<td>&#8369;&nbsp;<?=number_format($obj->amount, 2, '.',' ');?></td>
			<td><?=$obj->remarks;?></td>
			<!--td>
				<a class='confirm' href='<?=site_url("payments/destroy_refund/".$obj->id)?>' class=''><span class='glyphicon glyphicon-trash'></span>&nbsp;Delete</a>
			</td-->
		</tr>
	<?endforeach;?>
	<tr class=''>
		<td colspan = 1 style='text-align:right'>Total Amount</td>
		<td><b>&#8369;&nbsp;<?=number_format($total_amount, 2, '.',' ');?></b></td>
	</tr>
	<?else:?>
	<tr>
		<td colspan = '13'>No record found.</td>
	</tr>
	<?endif;?>
</table>

 <a class='btn btn-default btn-sm' href="<?php echo site_url('fees/view_fees').'/'.$eid; ?>">Back To Student Fees</a> 
