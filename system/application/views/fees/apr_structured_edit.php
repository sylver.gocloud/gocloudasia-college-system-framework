<?if($input_save):
	$amt = $input_save['amount_paid'] == FALSE ? NULL : $input_save['amount_paid'];
	$rem = $input_save['spr_remarks'] == FALSE ? NULL : $input_save['spr_remarks'];
	$pdt = $input_save['spr_payment_date'] == FALSE ? NULL : $input_save['spr_payment_date'];
	$orn = $input_save['spr_or_no'] == FALSE ? NULL : $input_save['spr_or_no'];
	$mop = $input_save['spr_mode_of_payment'] == FALSE ? NULL : $input_save['spr_mode_of_payment'];
else:
	$amt = NULL;
	$rem = NULL;
	$pdt = NULL;
	$orn = NULL ;
	$mof = NULL ;
	$mop = NULL ;
endif;?>

<div class="row">
	<?echo form_open('', 'class="form-horizontal"');?>
	<div class="col-md-6">
		<?@$this->load->view('fees/account_summary');?>

		<div>
			<div class="panel panel-default" style="background:rgba(125,140,157,0.8);border:2px solid #000;">

			<!-- START OF THE FORM -->

			  <div class="panel-heading" style="background:rgba(125,140,157,0.8);">
			    <h3 class="panel-title"><label class="label label-inverse black">Payment Record Form</label></h3>
			  </div>
			  <div class="panel-body">
			    <?if(isset($system_message))echo $system_message;?>
					<? echo validation_errors();?>

					<div>
						<h5><label class="label label-primary"><strong>Amount</strong></label></h5>
						<input id="amount_money" type="text" name="ammount_paid"  placeholder="Amount" value="<?php echo $payment_record_data->spr_ammt_paid;?>" disabled>
					</div>

					<br/>

					<div>
						<h5><label class="label label-primary"><strong>OR Number</strong></label></h5>
						<input type="text" name="spr_or_no" placeholder="O.R Number" value="<?php echo $payment_record_data->spr_or_no;?>" autocomplete="off" required>
					</div> 

					<br/>

					<div>
						<h5><label class="label label-primary"><strong>Date Of Payment (YYYY-MM-DD)</strong></label></h5>
						<input type="text" name="date_of_payment"  placeholder="Date: yyyy-mm-dd" class ="date_pick" value="<?php echo $payment_record_data->spr_payment_date;?>" required>
					</div>

					<br/>
					
					<div>
						<h5><label class="label label-primary"><strong>Remarks</strong></label></h5>
						<textarea class="getremarks form-control" required url="<?=site_url('output/getremarks');?>" name="remarks"><?php echo $payment_record_data->spr_remarks;?></textarea>
					</div>

					<br/>

					<div>
						<h5><label class="label label-primary"><strong>Type of Payment</strong></label></h5>
						<input type="text" name=""  value="<?php echo $payment_record_data->spr_mode_of_payment;?>" disabled>
					</div>

					<br/>
					<?if($payment_record_data->spr_mode_of_payment == "CHECK"):?>
					<div id='check_detail'>

						<div>
							<h5><label class="label label-primary"><strong>Check Date (YYYY-MM-DD)</strong></label></h5>
							<input id="check_date" type="text" name="check_date" class='date_pick' placeholder="Check Date" value="<?=date('Y-m-d', strtotime($payment_record_data->check_date))?>">
						</div>

						<br/>

						<div>
							<h5><label class="label label-primary"><strong>Check Number</strong></label></h5>
							<input type="text" name="check_number" placeholder="Check Number" value="<?=$payment_record_data->check_number?>">
						</div>
					</div>
					<?endif;?>

					<input type="hidden" name="enrollment_id" value="<?echo $enrollment_id;?>">
					<input type="hidden" name="validate_unique_data" value="<?echo $payment_record_data->spr_or_no;?>">
					<input type="submit" class="btn btn-primary" name="update_payment_record" id="update_payment_record" value="Update Details">
			  </div>
			</div>
		</div>
	</div>

	<div class="col-md-6">
		
		<div class="panel panel-default">
		  <div class="panel-heading black center"><strong>Student Current Payment Mode</strong></div>
		  <div class="panel-body">
		    <div class="table-responsive">
				<table class="table">
				<?$student_current_paymentmode = $_student->student_plan_modes?>
				<?if($student_current_paymentmode):?>
					<tbody>
						<thead>
							<td>#</td>
							<td style="text-align:right;">Payment Due</td>
							<td style="text-align:right;">Paid</td>
						</thead>

						<?foreach($student_current_paymentmode as $v):?>
								<?
							$sum['due'][] = $v->value;
							$sum['paid'][] = $v->amount_paid;
							$style_bg = "";
							$ico = "";
							
							if(in_array($v->id, $fee_ids['TUITION'])){
								// vp($v->id);
								// vp(in_array($v->id, $fee_ids['TUITION']));
								// vd($fee_ids['TUITION']);	
								$style_bg = "background-color: rgb(0, 136, 204) !important;";
								$ico = '<i class="fa fa-anchor"></i>&nbsp;';
							}
						?>
							<tr style="<?=$style_bg?>">
							<td>
								<?=$ico?>

								<?if($v->is_paid == 0):?>
									
								<?else:?>
										<i data-tooltip class="fa fa-check has-tip" style='cursor:pointer' title='PAID'></i>
								<?endif;?>

								<?=$v->name;?>
							</td>
							<td  style="text-align:right;"><?=mowney($v->value);?></td>
							<?if($v->amount_paid == 0.00):?>
								<td style="color:#f00;font-weight:bold;text-align:right;"><?=mowney($v->amount_paid);?></td>
							<?else:?>
								<td  style="color:#035F11;font-weight:bold;text-align:right;"><?=mowney($v->amount_paid);?></td>
							<?endif;?>
						</tr>

						<?endforeach;?>
								<tr class="info">
									<td></td>
									<td style="color:#1212FC;font-weight:bold;text-align:right;"><?=mowney(array_sum($sum['due']));?></td>
									<td style="color:#1212FC;font-weight:bold;text-align:right;"><?=mowney(array_sum($sum['paid']));?></td>
								</tr>

						<!-- DISPLAY OTHER FEES -->
						<?$others = $_student->student_other_fees?>
						<?if($others):?>
							<tr>
								<td colspan="3" style="font-weight:bold;">Additional / Other Fees</td>
							</tr>
							<?foreach($others as $k => $v):?>
								<?if($type == 'spb'):?>
								<tr>
									<td><?=$v->spb_fee_name;?></td>
									<td style="text-align:right;"><?=mowney($v->sef_fee_rate);?></td>
									<td style="text-align:right;"><?=mowney($v->spb_amount_paid);?></td>
								</tr>
								<?else:?>
									<?
										$oth_sum['due'][] = $v->sef_fee_rate;
										$oth_sum['paid'][] = $v->amount_paid;
										$style_bg = "";
										$ico = "";
										if(in_array($v->sef_id, $fee_ids['OTHER'])){
											$style_bg = "background-color: rgb(0, 136, 204) !important;";
											$ico = '<i class="fa fa-anchor"></i>&nbsp;';
										}
									?>
									<tr style="<?=$style_bg?>">
										<td>
												<?=$ico;?>
												<?if($v->is_paid == 0):?>
												<?else:?>
													<i data-tooltip class="fa fa-check has-tip" style='cursor:pointer' title='PAID'></i>
												<?endif;?>
												<?=$v->sef_fee_name;?>
										</td>
										<td><?=mowney($v->sef_fee_rate);?></td>
										<td><?=mowney($v->amount_paid);?></td>
									</tr>
								<?endif?>
							<?endforeach;?>
								<tr class="info">
									<td></td>
									<td style="color:#1212FC;font-weight:bold;text-align:right;"><?=mowney(array_sum($oth_sum['due']));?></td>
									<td style="color:#1212FC;font-weight:bold;text-align:right;"><?=mowney(array_sum($oth_sum['paid']));?></td>
								</tr>
						<?endif;?>
						<!-- END OF OTHER FEES -->

						<!-- DISPLAY STUDENT PREVIOUS ACCOUTS -->
						<?$prev_accounts = $_student->student_previous_account?>
						<?if($prev_accounts):?>
							<tr>
								<td colspan="3" style="font-weight:bold;">Old Accounts</td>
							</tr>
							<?foreach($prev_accounts as $k => $v):?>
								<?
									$prev_sum['due'][] = $v->value;
									$prev_sum['paid'][] = $v->amount_paid;
									$style_bg = "";
									$ico = "";
									if(in_array($v->id, $fee_ids['PREVIOUS'])){
										$style_bg = "background-color: rgb(0, 136, 204) !important;";
										$ico = '<i class="fa fa-anchor"></i>&nbsp;';
									}
								?>
									<tr style="<?=$style_bg?>">
									<td>
											<?=$ico?>
											<?if($v->is_paid == 0):?>
											<?else:?>
													<i data-tooltip class="fa fa-check has-tip" style='cursor:pointer' title='PAID'></i>
											<?endif;?>
											<?=$v->level_desc;?> - <?=$v->ay;?>
									</td>
									<td><?=mowney($v->value);?></td>
									<td><?=mowney($v->amount_paid);?></td>
								</tr>
							<?endforeach;?>
								<tr class="info">
									<td></td>
									<td style="color:#1212FC;font-weight:bold;text-align:right;"><?=mowney(array_sum($prev_sum['due']));?></td>
									<td style="color:#1212FC;font-weight:bold;text-align:right;"><?=mowney(array_sum($prev_sum['paid']));?></td>
								</tr>
						<?endif;?>
						<!-- END OF PREVIOUS ACCOUNTS -->
					</tbody>
				<?else:?>
				<?endif;?>
				</table>
			</div>
		  </div>
		</div>
	</div>
	</form>
</div>