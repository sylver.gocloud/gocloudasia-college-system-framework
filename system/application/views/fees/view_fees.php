<?php @$this->load->view('layouts/_student_data'); ?>
<div class="tab-content">
	<div class="row">

	  <div class="col-md-12">

	  	<div class="btn-group">
			  <button type="button" class="btn btn-default btn-sm">Payment Plan : </button>
			  <?if(isset($_student) && $_student->profile && $_student->profile->payment_plan && $_student->profile->payment_division):?>
			  	<?
			  		$i_love_you = "btn-default";
			  		if(strtoupper($_student->profile->payment_status) == "FULLY PAID" ){
			  			$i_love_you = "btn-success";
			  		}else if(strtoupper($_student->profile->payment_status) == "PARTIAL PAID" ){
			  			$i_love_you = "btn-warning";
			  		}else{}
			  	?>
			  	<button type="button" class="btn <?=$i_love_you?> btn-sm btn-sm tp" = title="<?=$_student->profile->payment_status?>" ><?=$_student->profile->payment_plan?></button>
			  <?else:?>
			  	<a href="<?=site_url('fees/set_payment_plan/'.$enrollment_id);?>" class="btn btn-sm btn-success"><i class="fa fa-lightbulb-o"></i>&nbsp; No Payment Plan Set! Click Here to Set.</a>
			  <?endif;?>
			</div>
		
			<!-- RECOMPUTE BUTTON -->
			<?if($this->_student->has_payment == false):?>
				<div class="btn-group btn-group-sm">
				  <a href="<?php echo site_url(); ?>fees/recompute/<?php echo $enrollment_id; ?>" title="Are you sure to re-compute or re-assess this student fees? This cannot be reverted back." class="btn btn-danger btn-sm confirm"><i class="fa fa-refresh"></i>&nbsp; Recompute Fees</a>
				</div><br/><br/>
			<?else:?>
				<div class="btn-group btn-group-sm">
				  <a href="<?php echo site_url(); ?>fees/recompute/<?php echo $enrollment_id; ?>" title="Warning! Student Already has a record of payment. Are you sure to re-compute or re-assess this student fees? This cannot be reverted back. The payments be re-apply automatically." class="btn btn-danger btn-sm confirm"><i class="fa fa-refresh"></i>&nbsp; Recompute Fees</a>
				</div><br/><br/>
			<?endif;?>

	  	<div class="hidden-xs well">
	  		<div class="row">

					<div class="btn-group btn-group-sm">
						<?if($mydepartment->stud_add_fees):?>
							<?//if($_student->is_full_paid === false):?>
					  		<a href="<?=site_url('fees/add_student_payment/'.$enrollment_id)?>" class="btn btn-bitbucket btn-sm btn-xs confirm"><i class="fa fa-money"></i>&nbsp; Add Payment</a>
					  	<?//endif;?>
					  <?endif?>
					  <a href="<?=site_url('fees/payment_record/'.$enrollment_id)?>" class="btn btn-github btn-sm btn-xs"><i class="fa fa-list"></i></i>&nbsp; Payment Record</a>
					</div>

					<?if($mydepartment->stud_add_fees):?>

						<div class="btn-group btn-group-sm">
						  <a href="<?=site_url('fees/pending_cheque/'.$enrollment_id)?>" class="btn btn-bitbucket btn-sm btn-xs"><i class="fa fa-credit-card"></i>&nbsp; Pending Checks</a>
						  <a href="<?=site_url('fees/declined_cheque/'.$enrollment_id)?>" class="btn btn-github btn-sm btn-xs"><i class="fa fa-credit-card"></i>&nbsp; Declined Checks</a>
						</div>

					<?endif;?>

					<div class="btn-group">
					  <button type="button" class="btn btn-bitbucket btn-sm">More</button>
					  <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
					    <span class="caret"></span>
					    <span class="sr-only">Toggle Dropdown</span>
					  </button>
					  <ul class="dropdown-menu" role="menu">
						  <li><a href="<?=site_url('fees/add_other_fees/'.$enrollment_id)?>" class="confirm"><i class="fa fa-plus-circle"></i>&nbsp; Add Other/Clearance Fee</a></li>
						  <li><a href="<?=site_url('fees/add_old_accounts/'.$enrollment_id)?>" class="confirm"><i class="fa fa-plus-square"></i>&nbsp; Add Old Accounts</a></li>
					  </ul>
					</div>
				
					<div class="btn-group">
					  <button type="button" class="btn btn-bitbucket btn-sm"><i class="glyphicon glyphicon-print"></i></button>
					  <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
					    <span class="caret"></span>
					    <span class="sr-only">Toggle Dropdown</span>
					  </button>
					  <ul class="dropdown-menu" role="menu">
						  <li><a href="<?=site_url('fees/statement_of_account/'.$enrollment_id)?>" class=""><i class="fa fa-file-text-o"></i>&nbsp; Statement of Account</a></li>
					  	<li><a href="<?=site_url('fees/print_statement_of_account/'.$enrollment_id)?>" class="tp" title="Print Statement of Account" target="_blank"><i class="glyphicon glyphicon-print"></i>&nbsp; Print SOA</i></a></li>
					  	<li class="divider"></li>
					  	<li><a href="<?=site_url('fees/student_ledger/'.$enrollment_id)?>" class=""><span class="glyphicon glyphicon-stats"></span>&nbsp; Student Ledger</a></li>
					  	<li><a target="_blank" href="<?=site_url('pdf/student_ledger/'.$enrollment_id)?>" class=""><span class="entypo entypo-print"></span>&nbsp; Print Ledger</a></li>
					  	<li><a target="_blank" href="<?=site_url('excels/student_ledger/'.$enrollment_id)?>" class=""><span class="entypo entypo-download"></span>&nbsp; Download (XLS)</a></li>
					  </ul>
					</div>
				</div>
			</div>

			<div class="visible-xs well">
				<!-- Split button -->
				<div class="btn-group">
				  <button type="button" class="btn btn-bitbucket btn-sm">Payments</button>
				  <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
				    <span class="caret"></span>
				    <span class="sr-only">Toggle Dropdown</span>
				  </button>
				  <ul class="dropdown-menu" role="menu">
				    <li><a href="<?=site_url('fees/add_student_payment/'.$enrollment_id)?>" class=" confirm"><i class="fa fa-money"></i>&nbsp; Add Payment</a></li>
					  <li><a href="<?=site_url('fees/payment_record/'.$enrollment_id)?>" class=""><i class="fa fa-list"></i></i>&nbsp; Payment Record</a></li>
					  <li class="divider"></li>
					  <li><a href="<?=site_url('fees/pending_cheque/'.$enrollment_id)?>" class=""><i class="fa fa-credit-card"></i>&nbsp; Pending Checks</a></li>
					  <li><a href="<?=site_url('fees/declined_cheque/'.$enrollment_id)?>" class=""><i class="fa fa-credit-card"></i>&nbsp; Declined Checks</a></li>
				  </ul>
				</div>

				<div class="btn-group">
				  <button type="button" class="btn btn-github btn-sm">Fees</button>
				  <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
				    <span class="caret"></span>
				    <span class="sr-only">Toggle Dropdown</span>
				  </button>
				  <ul class="dropdown-menu" role="menu">
					  <li><a href="<?=site_url('fees/add_other_fees/'.$enrollment_id)?>" class=" confirm"><i class="fa fa-plus-circle"></i>&nbsp; Add Other/Clearance Fee</a></li>
					  <li><a href="<?=site_url('fees/add_old_accounts/'.$enrollment_id)?>" class=" confirm"><i class="fa fa-plus-square"></i>&nbsp; Add Old Accounts</a></li>
				  </ul>
				</div>

				<div class="btn-group">
				  <button type="button" class="btn btn-tumblr btn-sm">Reports</button>
				  <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
				    <span class="caret"></span>
				    <span class="sr-only">Toggle Dropdown</span>
				  </button>
				  <ul class="dropdown-menu" role="menu">
					  <li><a href="<?=site_url('fees/statement_of_account/'.$enrollment_id)?>" class=""><i class="fa fa-file-text-o"></i>&nbsp; Statement of Account</a></li>
					  <li><a href="<?=site_url('fees/print_statement_of_account/'.$enrollment_id)?>" class=" tp" title="Print Statement of Account" target="_blank"><i class="glyphicon glyphicon-print"></i>&nbsp; Print SOA</i></a></li>
					  <li class="divider"></li>
					  	<li><a href="<?=site_url('fees/student_ledger/'.$enrollment_id)?>" class=""><span class="glyphicon glyphicon-stats"></span>&nbsp; Ledger</a></li>
				  </ul>
				</div>
			</div>

	  </div>

	</div>

	<?php if($_student->coursefinance_id == false) :?>
	<div class="row">
		<div class="col-md-12">
			<div class="alert alert-danger">
				Student course finance doesn't exist. Please click on 'Recompute fees'. If it still doesn't work, please contact your administrator. 
			</div>
		</div>
	</div>	
	<?php endif ;?>

	<?if($student_finance):?>
	<div>
		<fieldset class="scheduler-border">
			<div class="row">

				<div class="col-md-12">

					<div class="table-responsive">
					<table class='table'>
					  <tr>
						  <td class='bold' >Course Finance : &nbsp;
						  <a href="javascript:;" class="btn btn-xs btn-default bold"><?php echo $student_finance->code; ?></a>
						  </td>
						  <td class="bold" >Has NSTP : &nbsp;
						  	<a href="javascript:;" class="btn btn-xs btn-default bold"><?php echo $_student->has_nstp ? "Yes" : "No"; ?></a>
						  </td>
					  </tr>
					</table>
					</div>
				</div>
			</div>

			<div class="row">
				
				<div class="col-md-12">

				<!-- TUITION FEES START HERE -->
				<div class="table-responsive">
					<?
						$_tuition_fees = isset($_student_fees->tuition_fee) ? $_student_fees->tuition_fee : false;
						$_lab_fees = isset($_student_fees->lab_fee) ? $_student_fees->lab_fee : false;
						$_misc_fees = isset($_student_fees->misc_fee) ? $_student_fees->misc_fee : false;
						$_other_school_fees = isset($_student_fees->other_school_fee) ? $_student_fees->other_school_fee : false;
						$_other_fees = isset($_student_fees->other_fee) ? $_student_fees->other_fee : false;
						$_nstp_fees = isset($_student_fees->nstp_fee) ? $_student_fees->nstp_fee : false;
						$_old_accounts = isset($_student_fees->old_accounts) ? $_student_fees->old_accounts : false;					

						$_deduction = isset($_student_fees->deductions) ? $_student_fees->deductions : false;					
						$_scholarship = isset($_student_fees->scholarships) ? $_student_fees->scholarships : false;					
						$_excess = isset($_student_fees->excess_payments) ? $_student_fees->excess_payments : false;					
					?>
					<table class="table" style='border:2px dotted gray;' >
						<?if($_tuition_fees):?>
						<? $_tuition_fees = (object)$_tuition_fees ?>
							<tr class='info' >
								<td colspan="2" class="bold" ><i class="fa fa-tag"></i>&nbsp; Tuition Fee</td>
							</tr>
							<tr>
								<td class="bold" >
									&nbsp; &nbsp; &nbsp; &nbsp;<i class="fa fa-bookmark"></i>&nbsp; <?=$_tuition_fees->name?>
									(<?=m($_tuition_fees->units)?> &nbsp; * &nbsp; <?=m($_tuition_fees->rate)?>)
								</td>
								<td class='bold text-right' ><span class='total_number'><?=peso();?><?=m($_tuition_fees->amount);?></span></td>
							</tr>

						<?endif;?>

						<?if($_lab_fees):?>
						<? $_lab_fees = (object)$_lab_fees ?>
							<tr class="info">
								<td colspan="2" class="bold" ><i class="fa fa-tag"></i>&nbsp; Lab Fee</td>
							</tr>
							<?foreach ($_lab_fees as $key => $value):?>

							<?$value = (object)$value;?>
							
							<tr>
								<td class="bold" >
									&nbsp; &nbsp; &nbsp; &nbsp;<i class="fa fa-bookmark"></i>&nbsp;<?=$value->name?>
									(<?=m($value->units)?> &nbsp; * &nbsp; <?=m($value->rate)?>)
								</td>
								<td class='bold text-right' ><span class='total_number'><?=peso();?><?=m($value->amount);?></span></td>
							</tr>
							<?endforeach;?>

						<?endif;?>

						<?if($_misc_fees):?>
						<? $_misc_fees = (object)$_misc_fees ?>
							<tr class="info">
								<td colspan="2" class="bold" ><i class="fa fa-tag"></i>&nbsp; Miscellaneous Fee</td>
							</tr>
							<tr>
								<td class="bold" >
									&nbsp; &nbsp; &nbsp; &nbsp;<i class="fa fa-bookmark"></i>&nbsp; Miscellaneous Fee
									
									
									<div class="btn-group btn-group-xs">
									<a class="btn btn-default btn-xs" href="javascript:;" onclick='show_misc()' ><i class="fa fa-eye"></i>&nbsp; Show Breakdown</a>
									<a class="btn btn-default btn-xs" href="javascript:;" onclick='unshow_misc()'><i class="fa fa-eye-slash"></i>&nbsp; Hide Breakdown</a>
									</div>
									<script type="text/javascript">
									function show_misc(){
										$('#div_misc').removeClass('hidden');
									}
									function unshow_misc(){
										$('#div_misc').addClass('hidden');	
									}
									</script>
									<ul id='div_misc' class="list-group hidden">
										<?foreach ($_misc_fees as $key => $value):?>
											<?if($key === "total"){continue;}?>
											<?$value = (object)$value;?>
											<li class="list-group-item list-group-item-xs">
										    <span class="badge"><?=m($value->amount);?></span>
										    <?=($value->name);?>
										  </li>
										<?endforeach;?>
									</ul>
								</td>
								<td class='bold text-right' ><span class='total_number'><?=peso();?><?=m($_misc_fees->total);?></span></td>
							</tr>
							
						<?endif;?>
						
						<?if($_other_school_fees):?>
						<? $_other_school_fees = (object)$_other_school_fees ?>
							<tr class="info">
								<td colspan="2" class="bold" ><i class="fa fa-tag"></i>&nbsp; Other School Fees</td>
							</tr>
							<tr>
								<td class="bold" >
									&nbsp; &nbsp; &nbsp; &nbsp;<i class="fa fa-bookmark"></i>&nbsp; Other School Fees
									
									
									<div class="btn-group btn-group-xs">
									<a class="btn btn-default btn-xs" href="javascript:;" onclick='show_osf()' ><i class="fa fa-eye"></i>&nbsp; Show Breakdown</a>
									<a class="btn btn-default btn-xs" href="javascript:;" onclick='unshow_osf()'><i class="fa fa-eye-slash"></i>&nbsp; Hide Breakdown</a>
									</div>
									<script type="text/javascript">
									function show_osf(){
										$('#div_osf').removeClass('hidden');
									}
									function unshow_osf(){
										$('#div_osf').addClass('hidden');	
									}
									</script>
									<ul id='div_osf' class="list-group hidden">
										<?foreach ($_other_school_fees as $key => $value):?>
											<?if($key === "total"){continue;}?>
											<?$value = (object)$value;?>
											<li class="list-group-item list-group-item-xs">
										    <span class="badge"><?=m($value->amount);?></span>
										    <?=($value->name);?>
										  </li>
										<?endforeach;?>
									</ul>
								</td>
								<td class='bold text-right' ><span class='total_number'><?=peso();?><?=m($_other_school_fees->total);?></span></td>
							</tr>
							
						<?endif;?>

						<?if($_nstp_fees):?>
						<? $_nstp_fees = (object)$_nstp_fees ?>
							<tr class="info">
								<td colspan="2" class="bold" ><i class="fa fa-tag"></i>&nbsp; NSTP Fee</td>
							</tr>
							<tr>
								<td class="bold" >
									&nbsp; &nbsp; &nbsp; &nbsp;<i class="fa fa-bookmark"></i>&nbsp; <?=$_nstp_fees->name?>
								</td>
								<td class='bold text-right' ><span class='total_number'><?=peso();?><?=m($_nstp_fees->total);?></span></td>
							</tr>
							
						<?endif;?>
						
						<?if($_old_accounts):?>
							<tr class="info" >
								<td colspan="2" class="bold" ><i class="fa fa-tag"></i>&nbsp; Old Accounts</td>
							</tr>
							<tr>
								<td class="bold" >
									&nbsp; &nbsp; &nbsp; &nbsp; <i class="fa fa-bookmark"></i>&nbsp; Unsettled Accounts
									<div class="btn-group btn-group-xs">
									  <a class="btn btn-default btn-xs" href="javascript:;" onclick='show_old()' ><i class="fa fa-eye"></i>&nbsp; Show Breakdown</a>
									  <a class="btn btn-default btn-xs" href="javascript:;" onclick='unshow_old()'><i class="fa fa-eye-slash"></i>&nbsp; Hide Breakdown</a>
									</div>
									
									<script type="text/javascript">
									function show_old(){
										$('#div_old').removeClass('hidden');
									}
									function unshow_old(){
										$('#div_old').addClass('hidden');	
									}
									</script>
									<ul id='div_old' class="list-group hidden">
										<?foreach ($_old_accounts as $key => $obj):?>
											<li class="list-group-item list-group-item-xs">
										    <span class="badge"><?=m($obj->value);?></span>
										    <!-- ONLY ALLOW DELETE IF NO PAYMENT MAID -->
										    <?if($obj->amount_paid <= 0):?>
										    <a href="<?=site_url('fees/destroy_old_account/'.$enrollment_id.'/'.$obj->id)?>" class='confirm btn btn-xs btn-default btn-sm' title="Are you sure you want to remove this old account? This action cannot be reverted back. Student fees will adjust automaticaly." ><i class="fa fa-trash-o"></i></a>&nbsp; 
										    <?endif;?>
										    	<?=($obj->year);?> (<?=($obj->sy_from);?>-<?=($obj->sy_to);?>)
										  	</li>
										<?endforeach;?>
									</ul>
								</td>
								<td class='bold text-right' ><span class='total_number'><?=peso();?><?=m($_student->student_payment_totals->total_previous_amount);?></span></td>
							</tr>
						<?endif;?>

					</table>
					<?if($_other_fees):?>
						<table class="table" style='border:2px dotted gray;' >
							<? $_other_fees = (object)$_other_fees ?>
								<tr class="info" >
									<td colspan="2" class="bold" ><i class="fa fa-tag"></i>&nbsp; Other/Clearance Fee</td>
								</tr>
								<tr>
									<td class="bold" >
										&nbsp; &nbsp; &nbsp; &nbsp; <i class="fa fa-bookmark"></i>&nbsp; Other/Clearance Fee 
										<div class="btn-group btn-group-xs">
										  <a class="btn btn-default btn-xs" href="javascript:;" onclick='show_other()' ><i class="fa fa-eye"></i>&nbsp; Show Breakdown</a>
										  <a class="btn btn-default btn-xs" href="javascript:;" onclick='unshow_other()'><i class="fa fa-eye-slash"></i>&nbsp; Hide Breakdown</a>
										</div>
										
										<script type="text/javascript">
										function show_other(){
											$('#div_other').removeClass('hidden');
										}
										function unshow_other(){
											$('#div_other').addClass('hidden');	
										}
										</script>
										<ul id='div_other' class="list-group hidden">
											<?foreach ($_other_fees as $key => $value):?>
												<?if($key === "total"){ continue; }?>
												<?$value = (object)$value;?>
												<li class="list-group-item list-group-item-xs">
											    <span class="badge"><?=m($value->amount);?></span>
											    <!-- ONLY ALLOW DELETE IF NO PAYMENT MAID -->
											    <?if($value->amount_paid <= 0):?>
											    <a href="<?=site_url('fees/destroy_other_fee/'.$enrollment_id.'/'.$value->id)?>" class='confirm btn btn-xs btn-default btn-sm' title="Are you sure you want to remove this <?=$value->name?> fee? This action cannot be reverted back. Student fees will adjust automaticaly." ><i class="fa fa-trash-o"></i></a>&nbsp; 
											    <?endif;?>
											    	<?=($value->name);?>
											  	</li>
											<?endforeach;?>
										</ul>
									</td>
									<td class='bold text-right' ><span class='total_number'><?=peso();?><?=m($_other_fees->total);?></span></td>
								</tr>
						</table>
					<?endif;?>
				</div><br/>

				<div class="table-responsive">
					<table class="table" style='border:2px dotted red;'>
						<tr class='warning' >
							<td class="bold">Deductions
								<div class="btn-group btn-group-xs">
								  <a class="btn btn-xs btn-default btn-sm confirm" href="<?=site_url('fees/add_deduction_fees/'.$enrollment_id)?>" ><i class="fa fa-plus"></i>&nbsp; Add </a>
								  <a class="btn btn-xs btn-default" href="javascript:;" onclick="show_deduction()" ><i class="fa fa-eye"></i>&nbsp; Show </a>
								  <a class="btn btn-xs btn-default" href="javascript:;" onclick="hide_deduction()"><i class="fa fa-eye-slash"></i>&nbsp; Hide </a>
								</div>

								<script type="text/javascript">
									function show_deduction(){
										$('#div_deduct').removeClass('hidden');
									}
									function hide_deduction(){
										$('#div_deduct').addClass('hidden');	
									}
								</script>
								&nbsp; &nbsp; &nbsp;
								
								<ul id='div_deduct' class="list-group hidden">
								<?if($_deduction):?>

									<?foreach ($_deduction as $key => $value):?>
										<li class="list-group-item list-group-item-xs">
									    <span class="badge"><?=m($value->amount);?></span>
									    <a href="<?=site_url('fees/destroy_deduction/'.$enrollment_id.'/'.$value->id)?>" class='confirm btn btn-xs btn-default btn-sm' title="Are you sure you want to remove this <?=$value->deduction_name?> deduction? This action cannot be reverted back. Student fees and current payment will adjust automaticaly." >
									    	<i class="fa fa-trash-o"></i></a>&nbsp; <?=($value->deduction_name);?>

									  </li>
									<?endforeach;?>
								<?else:?>
									<li class="list-group-item list-group-item-xs">No Deduction Record</li>
								<?endif;?>
								</ul>
								
							</td>
							<td class="bold text-right total_number">- &nbsp; &nbsp; &nbsp;<?=peso();?><?=m($_student->student_payment_totals->total_deduction
							);?></td>
						</tr>

						<tr class='warning' >
							<td class="bold">Scholarships
								<div class="btn-group btn-group-xs">
								  <a class="btn btn-xs btn-default btn-sm confirm" href="<?=site_url('fees/add_student_scholarship/'.$enrollment_id)?>" ><i class="fa fa-plus"></i>&nbsp; Add </a>
								  <a class="btn btn-xs btn-default" href="javascript:;" onclick="show_scholarship()" ><i class="fa fa-eye"></i>&nbsp; Show </a>
								  <a class="btn btn-xs btn-default" href="javascript:;" onclick="hide_scholarship()"><i class="fa fa-eye-slash"></i>&nbsp; Hide </a>
								</div>

								<script type="text/javascript">
								function show_scholarship(){
									$('#div_sholarship').removeClass('hidden');
								}
								function hide_scholarship(){
									$('#div_sholarship').addClass('hidden');	
								}
								</script>
								&nbsp; &nbsp; &nbsp;
								<ul id='div_sholarship' class="list-group hidden">
								<?if($_scholarship):?>
									<?foreach ($_scholarship as $key => $value):?>
										<li class="list-group-item list-group-item-xs">
									    <span class="badge"><?=m($value->scho_amount);?></span>
									    <a href="<?=site_url('fees/destroy_scholarship/'.$enrollment_id.'/'.$value->id)?>" class='confirm btn btn-xs btn-default btn-sm' title="Are you sure you want to remove this <?=$value->scholarship_name?> scholarship? This action cannot be reverted back. Student fees and current payment will adjust automaticaly." >
									    	<i class="fa fa-trash-o"></i></a>&nbsp; <?=($value->scholarship_name);?>

									  </li>
									<?endforeach;?>
								<?else:?>
									<li class="list-group-item list-group-item-xs">No Scholarship Record</li>
								<?endif;?>
								</ul>
							</td>
							<td class="bold text-right total_number">- &nbsp; &nbsp; &nbsp;<?=peso();?><?=m($_student->student_payment_totals->total_scholarship);?></td>
						</tr>
						<tr class='warning' >
							<td class="bold">Excess from Previous Enrollments
								<div class="btn-group btn-group-xs">
								  <a class="btn btn-xs btn-default btn-sm" href="<?=site_url('fees/add_excess_payment/'.$_student->enrollment_id)?>" ><i class="fa fa-plus"></i>&nbsp; Add </a>
								  <a class="btn btn-xs btn-default" href="javascript:;" onclick="show_excess()" ><i class="fa fa-eye"></i>&nbsp; Show </a>
								  <a class="btn btn-xs btn-default" href="javascript:;" onclick="hide_excess()"><i class="fa fa-eye-slash"></i>&nbsp; Hide </a>
								</div>

								<script type="text/javascript">
								function show_excess(){
									$('#div_excess').removeClass('hidden');
								}
								function hide_excess(){
									$('#div_excess').addClass('hidden');	
								}
								</script>
								&nbsp; &nbsp; &nbsp;
								<ul id='div_excess' class="list-group hidden">
								<?if($_excess):?>
									<?foreach ($_excess as $key => $value):?>
										<li class="list-group-item list-group-item-xs">
									    <span class="badge"><?=m($value->amount);?></span>
									    <a href="<?=site_url('fees/destroy_excess_payment/'.$enrollment_id.'/'.$value->id)?>" class='confirm btn btn-xs btn-default btn-sm' title="Are you sure you want to remove this record? This action cannot be reverted back. Student fees and current payment will adjust automaticaly." >
									    	<i class="fa fa-trash-o"></i></a>
									    	<?=($obj->year);?> (<?=($obj->sy_from);?>-<?=($obj->sy_to);?>)
									  </li>
									<?endforeach;?>
								<?else:?>
									<li class="list-group-item list-group-item-xs">No Excess Payment Record</li>
								<?endif;?>
								</ul>
							</td>
							<td class="bold text-right total_number">- &nbsp; &nbsp; &nbsp;<?=peso();?><?=m($_student->student_payment_totals->total_excess_amount);?></td>
						</tr>
						
					</table>
				</div>
				
				<!-- PAYMENT TOTAL START HERE -->
				<div class="table-responsive">
					<table class="table" style='border:2px dotted blue;'>
						<tr class='warning' >
							<td class="bold">Total Applied Payment Plan <code>( Tuition + Misc. + Lab + Nstp + Other School Fees + Old Account ) - (Deduction + Scholarship + Excess Payments)</code></td>
							<td class="bold text-right total_number"><?=peso();?><?=m($_student->profile->total_plan_due);?></td>
						</tr>
						<tr class='warning' >
							<td class="bold">Other/Clearance Fees</td>
							<td class="bold text-right total_number">+ &nbsp; &nbsp; &nbsp;<?=peso();?><?=m($_student->profile->other_fee);?></td>
						</tr>
						<tr class='warning' >
							<td class="bold">Old Accounts</td>
							<td class="bold text-right total_number">+ &nbsp; &nbsp; &nbsp;<?=peso();?><?=m($_student->profile->total_previous_amount);?></td>
						</tr>
					</table>
				</div><br/>

				<?@$this->load->view('fees/account_summary');?>

				</div>
			</div>
		</fieldset>

	</div>

	<?endif;?>

	<?
		#if yes the link will not used ajax, it will go directly the link url
		$push_data['is_redirect'] = 'yes';
	?>
	<?php @$this->load->view("layouts/student_data/_student_subjects",$push_data); ?>	
		
	<?if($mydepartment->stud_edit_subject === "1"):?>
	<a class="btn btn-sm btn-default confirm" href="<?=site_url('fees/add_subject/'.$_student->enrollment_id)?>"><i class="fa fa-plus"></i>&nbsp; Add Subject</a>
	<?endif;?>
	
</div>