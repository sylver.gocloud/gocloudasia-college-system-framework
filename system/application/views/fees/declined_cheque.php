<script type="text/javascript">
	$(document).ready(function(){
		$('.clear_forms').submit(function(event){
			var xtitle = $(this).attr('title');
			var ans = confirm('Are you sure?');
			if(ans){

			}else{
				event.preventDefault();
			}
		})
	});
</script>
<div class="row">
	<?@$this->load->view('fees/student_profile_head');?>
	<div class="section-container auto" data-section data-options="deep_linking: true">
		<table class='table'>
			<tr>
					<th>Check Date</th>
					<th>Check Number</th>
					<th>Amount</th>
					<th>Remarks</th>
			</tr>
		<?if($pending_checks):?>	
			<?foreach ($pending_checks as $key => $value): ?>
				<tr>
						<td><?=date('d-m-Y',strtotime($value->check_date))?></td>
						<td><?=$value->check_number?></td>
						<td><strong><?=money($value->spr_ammt_paid);?> </strong></td>
						<td><?=$value->check_remarks?></td>
				</tr>
			<?endforeach;?>
		<?else:?>
			<tr>
				<td colspan='5' ><p>No declined checks available</p></td>
			</tr>
		<?endif;?>
		</table>
	</div>
</div>