
<?php echo panel_head2('By Course & Year (Current Semester)',false,'inverse') ?>
	<?echo form_open('','class="xform-inline" role="form"');?>
		
		<div class="row" style="margin:5px" >
			<div class="col-lg-12">
				<div class="form-group">
					<label class="" for="year_id">Year Level</label>
					<?echo year_dropdown('year_id','',"", "All" , ' class="form-control"');?>
				</div>

				<p></p>
				<div class="form-group">
					<label class="" for="course_id">Course</label>
					<?echo course_dropdown('course_id','',"", "All");?>
				</div>

				<p></p>
				<div class="form-group">
					<label class="" for="course_id">Order</label>
					<?php 
						$report_order = array(
								'spr.spr_or_no' => 'OR. No.',
								'spr.spr_payment_date' => 'Payment Date',
							);
					?>
					<?php echo form_dropdown('report_order',$report_order) ?>
				</div>

				<p></p>
				<div class="form-group">
					<label class="" for="course_id">Report Type</label>
					<?php 
						$report_type = array(
								'list' => 'List',
								'summary_course' => 'Summary By Course',
								'summary_year' => 'Summary By Year Level',
							);
					?>
					<?php echo form_dropdown('report_type',$report_type) ?>
				</div>

				<input id="download" type="submit" value="Download" name="download_by_course_year" class="btn btn-primary btn-sm" />
			</div>
		</div>
	<?echo form_close();?>
<?php echo panel_tail2(); ?>

<?php echo panel_head2('By School Year',false,'inverse') ?>
	<?echo form_open('','class="xform-inline" role="form"');?>
		
		<div class="row" style="margin:5px" >
			<div class="col-lg-12">
				<div class="form-group">
					<label class="" for="ay_id">School Year</label>
					
					<?echo academic_year_dropdown('ay_id',$academic_year_id,'');?>
				</div>

				<p></p>
			 	<div class="form-group">
					<label class="" for="semester_id">Semester</label>
					<?echo semester_dropdown('semester_id','',"", "All" , ' class="form-control"');?>
				</div>
				
				<p></p>
			 	<div class="form-group">
					<label class="" for="year_id">Year Level</label>
					<?echo year_dropdown('year_id','',"", "All" , ' class="form-control"');?>
				</div>

				<p></p>
				<div class="form-group">
					<label class="" for="course_id">Course</label>
					<?echo course_dropdown('course_id','',"", "All");?>
				</div>

				<p></p>
				<div class="form-group">
					<label class="" for="course_id">Order</label>
					<?php 
						$report_order = array(
								'spr.spr_or_no' => 'OR. No.',
								'spr.spr_payment_date' => 'Payment Date',
							);
					?>
					<?php echo form_dropdown('report_order',$report_order) ?>
				</div>

				<p></p>
				<div class="form-group">
					<label class="" for="course_id">Report Type</label>
					<?php 
						$report_type = array(
								'list' => 'List',
								'summary_semester' => 'Summary By Semester',
								'summary_course' => 'Summary By Course',
								'summary_year' => 'Summary By Year Level',
							);
					?>
					<?php echo form_dropdown('report_type',$report_type) ?>
				</div>

				<input id="download" type="submit" value="Download" name="download_by_ay" class="btn btn-primary btn-sm" />
			</div>
		</div>
	<?echo form_close();?>
<?php echo panel_tail2(); ?>