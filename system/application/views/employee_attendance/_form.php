
 
  <p>
    <label for="year">Year</label><br />
	<?php 
		$data = array(
              'name'        => 'years[year]',
              'value'       => isset($years->year) ? $years->year : '',
              'maxlength'   => '100',
              'size'        => '50',
              'style'       => 'width:50%',
            );

		echo form_input($data);
	?>
  </p>
  <div class="clear"></div>
