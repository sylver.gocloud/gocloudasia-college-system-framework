<div id="right">

	<div id="right_top" >
		  <p id="right_title">Credits</p>
	</div>
	<div id="right_bottom">

	<div>
	
<table>
  <tr>
    <th>Course</th>
    <th>Subject</th>
    <th>Value</th>
  </tr>
  <?php foreach( $credits as $credit ): ?>
    <tr>
      <td><?php echo $credit->course; ?></td>
      <td><?php echo $credit->subject; ?></td>
      <td><?php echo $credit->value; ?></td>
      <td><a href="/credits/edit/<?php echo $credit->id; ?>">Edit</a></td>
      <td><a href="/credits/destroy/<?php echo $credit->id; ?>/<?php echo $credit->summary_of_credit_id; ?>" class="confirm" title="destroy credit <?php echo $credit->subject; ?>?">Destroy</a></td>
    </tr>
  <?php endforeach; ?>
</table>

<p><a href="/credits/create/<?php echo $credit_id; ?>">New Credit</a></p>
	
	</div>
	<div class="clear"></div>

	</div>
	
	<div class="clear"></div>
	
</div>