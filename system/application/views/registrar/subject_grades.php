<div id="right">

	
	<div id="right_bottom">
		<form action="<?=site_url('registrar/subject_grades');?>" method="POST" class="form-inline">
			<div class="form-group">
				<input id="search_sc_id_eq" name="sc_id" size="30" type="text" value="<?=set_value('sc_id');?>" placeholder="Subject Code"/>
			</div>
			<div class="form-group">
				<input id="search_subject_eq" placeholder="Description"  name="subject" size="30" type="text" value="<?=set_value('subject');?>"/>
			</div>
			<div class="form-group">
				<input id="search_code_eq" placeholder="Section Code" name="code" size="30" type="text" value="<?=set_value('code');?>"/>
			</div>
				<input id="search_submit" name="search_subjects" type="submit" value="Submit" />
		</form>
<br>
<?php if(!empty($results)):?>
  <?= $links ;?>	
<?php endif;?>
<table>
	<thead>
		<tr>
			<th>Subject Code</th>
			<th>Description</th>
			<th>Subject Code</th>
			<th>Time</th>
			<th>Day</th>
			<th>Room</th>
			<th>Action</th>
		  </tr>
	</thead>
	
	<tbody>
		<? if(!empty($results)):?>
			<?foreach($results as $r):
			  $l = _se($r->id);
			?>
				<tr>
					<td><?=$r->sc_id;?></td>
					<td><?=$r->subject;?></td>
					<td><?=$r->code;?></td>
					<td><?=$r->time;?></td>
					<td><?=$r->day;?></td>
					<td><?=$r->room;?></td>
					<td><a href="<?=site_url('registrar/view_subject_grades?id='.$l->link.'&di='.$l->hash);?>">view</a></td>
				<tr>
			<?endforeach;?>
		<?endif;?>
	</tbody>
</table>
  <?= $links ;?>	
</div>
	
</div>

