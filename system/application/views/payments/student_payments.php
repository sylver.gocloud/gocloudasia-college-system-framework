<div class="table-responsive">
<table >
	<tr>
		<th>Student ID</th>
		<th>Name</th>
		<th>Course</th>
		<th>Year Level</th>
		<th>Payments</th>
	</tr>
	<?
	$ctr = 0;
	?>
	<?if($enrollments): $total_amt = 0;?>
	<?foreach($enrollments as $obj):?>
		<tr>	
			<td class='bold' ><?=$obj->studid;?></td>
			<td class='bold' ><?=$obj->name?></td>
			<td><?=$obj->course;?></td>
			<td><?=$obj->year;?></td>	
			<td>
				<table class = "table table-condensed table-bordered">
					<tr class='gray'>
						<th>OR #</th>
						<th>Date</th>
						<th>Remarks</th>
						<th>Amount</th>
					</tr>
					<?if(isset($payments[$obj->id]) && $payments[$obj->id]):?>
						<?foreach($payments[$obj->id] as $val):?>
							<?
								$total_amt += $val->total;
							?>
							<tr>
								<td><?=$val->or_no;?></td>
								<td><?=date('m-d-Y', strtotime($val->date));?></td>	
								<td><?=$val->remarks;?></td>
								<td align="right" class='bold' ><?=number_format($val->total,2);?></td>
							</tr>
						<?endforeach;?>
					<?endif;?>
				</table>
			</td>
		</tr>
	<?endforeach;?>
	<tr class=''>
		<td colspan = "2" class='bold' style='text-align:right'>Total No. of Records</td>
		<td><div class='badge'><?=$total_rows?></div></td>
		<td colspan = "2" align="right" class='bold' >Total Amount <div class='badge'><?=number_format($total_amt,2);?></div></td>
	</tr>
	<?else:?>
	<tr>
		<td colspan = '13'>No record found.</td>
	</tr>
	<?endif;?>
</table>
<?= $links;?>
</div>