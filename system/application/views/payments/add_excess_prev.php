<script type="text/javascript">
	function amount_change(e)
	{
		var xamount =  $('#amount').val();
		var xtotal_excess = numVal($('#total_excess').val());
		var xtotal_balance = numVal($('#total_balance').val());
		
		var xexcess_payment_bal = numVal($('#excess_payment_bal').text());
		var xaccount_balance = numVal($('#account_balance').text());
		
		xamount = xamount == '' ? 0 : numVal(xamount);
		
		if(xamount > 0)
		{
			xbal_excess = xtotal_excess - xamount;
			xbal_accnt = xtotal_balance - xamount;
			
			
			xbal_excess = $.number( xbal_excess, 2, '.', ' ' ); 
			xbal_accnt = $.number( xbal_accnt, 2, '.', ' ' ); 
			$('#excess_payment_bal').text(xbal_excess);
			$('#account_balance').text(xbal_accnt);
			
		}
		else
		{
			$('#excess_payment_bal').text($.number( xtotal_excess, 2, '.', ' ' ));
			$('#account_balance').text($.number( xtotal_balance, 2, '.', ' ' ));
		}
	}
	
	function check_balance(e)
	{
		var xaccount_balance = numVal($('#account_balance').text());
		console.log(xaccount_balance);
		if(xaccount_balance <= 0)
		{
			console.log(xaccount_balance);
			e.preventDefault();
			return false;
		} 
	}
	
	function validate()
	{
		var xtotal_balance = numVal($('#total_balance').val());
		
		if(xtotal_balance <= 0)
		{
			custom_modal('Required','Student account balance was already settled.');
			return false;
		}
		else
		{
			var xamount =  $('#amount').val();
			if(xamount == "")
			{	
				custom_modal('Required','No applied amount.');
				return false;
			}
			else
			{
				xamount = numVal(xamount);
				if(xamount <= 0)
				{
					custom_modal('Required','Applied amount must be greater than zero.');
					return false;
				}
				else
				{
					
				}
			}
		}
	}
</script>

<table>
	<tr>
		<th colspan='6' style='text-align:center'>Enrollments With Over Payment</th>
	</tr>
	<tr>
		<th>#</th>
		<th>School Year</th>
		<th>Semester</th>
		<th>Year</th>
		<th>Course</th>
		<th>Excess Payment</th>
	</tr>
	<?
		$total= 0;
	?>
	<?if($excess_prev_sem):?>
		<?$ctr= 0;?>
		<?foreach($excess_prev_sem as $obj):?>
			<?$total += ($obj->balance * -1)?>
			<tr>
				<td><?=++$ctr;?></td>
				<td><?=$obj->sy_from?>-<?=$obj->sy_to?></td>
				<td><?=$obj->semester;?></td>
				<td><?=$obj->year;?></td>
				<td><?=$obj->course;?></td>
				<td><b>&#8369; &nbsp;<?=number_format(($obj->balance * -1), 2, '.',' ');?></p></td>
			</tr>
		<?endforeach;?>
		<tr>
				<td colspan='5' style='text-align:right;' ><b>Total Excess</b></td>
				<td>
					<b>&#8369; &nbsp;<?=number_format($total, 2, '.',' ');?></p>
				</td>
		</tr>
	<?else:?>
	<tr>
		<td align='center'>No Previous Enrollments with Excess Amount</td>
	</th>
	<?endif;?>
</table>

<!--HIDEEN VALUES-->

<input type="hidden" id="total_balance" name="total_balance" value="<?=($student_total) ? $student_total->balance : 0?>" />
<input type='hidden' name='total_excess' id='total_excess' value='<?=strval($total)?>'>
<input type="hidden" id="enrollment_id" name="enrollment_id" value="<?=$enrollment_id;?>" />

<?if($student_total && $student_total->balance > 0):?>
	<?if($total > 0):?>
	<?= form_open();?>
		
		<div class="row">
		  <div class="col-md-3"><b>Excess Payment Balance</b></div>
		  <div class="col-md-3">
			<div id='excess_payment_bal' name='excess_payment_bal' class='total_number'><?=number_format($total, 2, '.', ' ');?></div>
		  </div>
		  
		  <div class="col-md-3"><b>Account Balance</b></div>
		  <div class="col-md-3">
			<div id='account_balance' name='account_balance' class='total_number'><?=number_format(($student_total->balance), 2, '.', ' ');?></div>
		  </div>
		</div>
		
		<br/>
		<div class="row">
		  <div class="col-md-3"><b>Apply Amount</b></div>
		  <div class="col-md-9">
			<input type='text' class='total_number not_blank form-control currency ' id='amount' name='amount' onkeyup='return amount_change(event)' onkeypress='return check_balance(event)' maxLength='12' />
		  </div>
		</div>
		
		<br/>
		<div class="row">
		  <div class="col-md-3"><b>Remarks</b></div>
		  <div class="col-md-9">
			<textarea class='form-control' id='remarks' name='remarks'>
			</textarea>
		  </div>
		</div>
		
		<br/>
		<div class="row">
		  <div class="col-md-12">
		  <?= form_submit('submit','Save','onclick="return validate(this)"');?>
		  <a class='btn btn-default btn-sm' href='<?=base_url()?>fees/view_fees/<?=$enrollment_id?>'><span class='glyphicon glyphicon-backward'></span>&nbsp;Back to Fees</a>
		  </div>
		</div>
	
	<?= form_close();?>
		
	<?endif;?>
<?else:?>
	<div class='alert alert-info'>
		<b>Enrollments is already paid.</b>
	</div>
<?endif;?>