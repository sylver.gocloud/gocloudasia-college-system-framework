	<div class="well">
		<?echo form_open('','class="form-inline" role="form"');?>
		<div class="form-group">
			Search In
		</div>
		 <div class="form-group">
			<?
				$xfields = array(
					"items.serial" => "Serial",
					"items.item" => "Item",
					"items.category" => "Category",
					"items.brand" => "Brand",
					"items.kind" => "Kind",
				);
				echo form_dropdown('fields', $xfields, isset($fields)?$fields:'serial');
			?>
		</div>
		<div class="form-group">
			<label class="sr-only" for="author">Keyword</label>
			<input type='text' name='keyword' value="<?=isset($keyword)?$keyword:''?>" placeHolder="Keyword" />
		</div>
		<div class="form-group">
			Status
		</div>
		<div class="form-group">
			<?
				$xstatus = array(
					"" => "ALL",
					"stable" => "STABLE",
					"needs re-stocking" => "NEEDS RE-STOCKING",
				);
				echo form_dropdown('status', $xstatus, isset($status)?$status:'');
			?>
		</div>
		<?
			echo form_submit('submit','Search');		?>
			<a class="btn btn-default btn-sm" href="<?php echo base_url(); ?>inventory/create" rel="facebox"><span class = 'glyphicon glyphicon-plus'></span>&nbsp; New Item</a>
			<?echo form_close();?>
	</div>

	<table id="tabled_inv">
		<thead>
			<tr>
				<th class="header">Serial</th>
				<th class="header">Item</th>
				<th class="header">Category</th>
				<th class="header">Brand</th>
				<th class="header">Kind</th>
				<th class="header">Measure</th>
				<th class="header">Unit</th>
				<th class="header">Unit Left</th>
				<th class="header">Purchase Price</th>
				<!--th class="header">Minimum</th>
				<th class="header">Maximum</th-->
				<th class="header">Status</th>
				<th class="header">Action</th>
			</tr>
		</thead>
		<tbody>
			<?if($items):?>
			<?php foreach ($items as $item): ?>
				<td><?php echo $item->serial; ?></td>
				<td><?php echo $item->item; ?></td>
				<td><?php echo $item->category; ?></td>
				<td><?php echo $item->brand; ?></td>
				<td><?php echo $item->kind; ?></td>
				<td><?php echo $item->size; ?></td>
				<td><?php echo $item->unit; ?></td>
				<td><?php echo $item->unit_left; ?></td>
				<td><?php echo $item->purchase_price; ?></td>
				<!--td><?php echo $item->min; ?></td>
				<td><?php echo $item->max; ?></td-->
				<?	
					$style = "";
					switch(strtoupper($item->status)){
						case "STABLE":
							$style='success';
						break;
						case "REACHED MAXIMUM":
							$style='warning';
						break;
						case "NEEDS RE-STOCKING":
							$style='danger';
						break;
						default:
							$style = "primary";
						break;
					}
				?>
				<td><div class='label label-<?=$style?>'><?php echo $item->status; ?></div></td>
				<td>
				<?=badge_link('inventory/edit/'.$item->id, 'primary', 'Edit')?>
				<?=badge_link('inventory/destroy/'.$item->id, 'primary', 'Destroy', '', true)?>
				</td>
			</tr>
			<?php endforeach; ?>
			<tr><td colspan="10" style='text-align:right;'>No. of Record</td><td><div class='badge'><?=$total_rows;?></div></td></tr>
			<?else:?>
			<tr><td colspan="12">No record found.</td></tr>
			<?endif;?>
		</tbody>
	</table>
	
	<?=$links?>
