  <script type='text/javascript'>
	
	function validate()
	{	
		var ctr = 0;
		var msg = "<ul>";
		var xfocus = "";
		$('.not_blank').css('border-color','gray');
		
		$('.not_blank').each(function(){
			var xval = $(this).val().trim();
			var xid = $(this).attr('id');
			if(xval == ""){
				ctr++;
				$(this).css('border-color','red');
				if(ctr == 1) { xfocus = $(this); }
				var label = $('label[for='+xid+']').text();
				
				msg += "<li>"
				+label+" should not be blank.</li>";
			}
		});
		
		msg += "<ul>";
		
		if(ctr > 0)
		{
			msg += "<ul>";
			xfocus.focus();
			$('#alertModal_Label').text('Required Fields'); //SET MODAL TITLE
			$('#alertModal_Body').html(msg); //SET MODAL CONTENT
			$('#alertModal').modal('show'); //SHOW MODAL
			
			return false;
		}
		else
		{
			
			msg = "<ul>";
			var ctr2 = 0;
			$('.is_number').each(function(){
				var xval = $(this).val().trim();
				var isnum = isNumber(xval);
				var xid = $(this).attr('id');
				
				if(isnum == false)
				{
					ctr2++;
					$(this).css('border-color','red');
					$(this).focus();
					$(this).val('');
					var label = $('label[for='+xid+']').text();
					msg += "<li>"+label+" should be numeric.</li>";
			
				}
			});
			
			if(ctr2 > 0){
				msg += "</ul>";
				$('#alertModal_Label').text('Required Fields'); //SET MODAL TITLE
				$('#alertModal_Body').html(msg); //SET MODAL CONTENT
				$('#alertModal').modal('show'); //SHOW MODAL
				
				return false;
			}else
			{
				//VALIDATE mininum ang maximum should not exceed the unit
				var xunit = parseFloat($('#unit').val());
				var xmin = parseFloat($('#min').val());
				var xmax = parseFloat($('#max').val());
				
				
				if(xmin > xunit){
					$('#alertModal_Label').text('Required Fields'); //SET MODAL TITLE
					$('#alertModal_Body').html('<p>Minimum should not be greater than the unit</p>'); //SET MODAL CONTENT
					$('#alertModal').modal('show'); //SHOW MODAL
					$('#min').css('border-color','red');
					return false;
				}else
				{
					if(xmax > unit){
						// $('#alertModal_Label').text('Required Fields'); //SET MODAL TITLE
						// $('#alertModal_Body').html('<p>Maximum should not be greater than the unit</p>'); //SET MODAL CONTENT
						// $('#alertModal').modal('show'); //SHOW MODAL
						// $('#max').css('border-color','red');
						// return false;
					}
					else
					{
						// if(xmax < xmin){
							// $('#alertModal_Label').text('Required Fields'); //SET MODAL TITLE
							// $('#alertModal_Body').html('<p>Minimum should not be greater than the maximum</p>'); //SET MODAL CONTENT
							// $('#alertModal').modal('show'); //SHOW MODAL
							// $('#min').css('border-color','red');
							// return fase;
						// }
					}
				}
			}
		}
	}
</script>
  <div class="form-group">
  	<label for="serial">Serial *</label>
  	<input class="form-control not_blank" id="serial" name="item[serial]" size="50" type="text" value='<?=isset($items)?$items->serial:''?>' />
  </div><br/>
  <div class="form-group">
  	<label for="item">Item *</label>
  	<input class="form-control not_blank" id="item" name="item[item]" size="50" type="text" value='<?=isset($items)?$items->item:''?>' />
  </div><br/>
   <div class="form-group">
  	<label for="category">Category *</label>
  	<input class="form-control not_blank" id="category" name="item[category]" size="50" type="text" value='<?=isset($items)?$items->category:''?>' />
  </div><br/>
   <div class="form-group">
  	<label for="brand">Brand</label><br>
  	<input class="form-control" id="brand" name="item[brand]" size="50" type="text" value='<?=isset($items)?$items->brand:''?>' />
  </div><br/>
   <div class="form-group">
  	<label for="kind">Kind</label>
  	<input class="form-control" id="kind" name="item[kind]" size="50" type="text" value='<?=isset($items)?$items->kind:''?>' />
  </div><br/>
   <div class="form-group">
  	<label for="size">Measure </label>
  	<input class="form-control" id="size" name="item[size]" size="50" type="text" value='<?=isset($items)?$items->size:''?>' />
  </div><br/>
   <div class="form-group">
  	<label for="unit">Unit *</label>
  	<input class="form-control is_number" id="unit" name="item[unit]" size="30" type="text" value='<?=isset($items)?$items->unit:''?>' <?=isset($items)?'disabled':''?> />
  </div><br/>
   <div class="form-group">
  	<label for="purchase_price">Purchase Price</label>
  	<input class="form-control is_number" id="purchase_price" name="item[purchase_price]" size="30" type="text" value='<?=isset($items)?$items->purchase_price:''?>' />
  </div><br/>
   <div class="form-group">
  	<label for="min">Minimum *</label> <span class='label label-info'>Note : Changes item status if item remaining unit is less than or equal to the mininum. </span>
	<input class="form-control is_number" id="min" name="item[min]" size="10" type="text" value='<?=isset($items)?$items->min:''?>' />
  </div><br/>
  <!--div class="form-group">
  	<label for="max">Maximum *</label> <span class='label label-info'>Note : Changes item status if item remaining unit reached maximum.</span><br>
	<input class="form-control is_number" id="max" name="item[max]" size="10" type="text" />
  </div--><br/><br/>