
			<table>
				<tr>
				  <th>Student ID</th>
				  <th>Name</th>
				  <th>Year</th>
				  <th>Course</th>
				  <th>Action</th>
				</tr>
			<? $num = 0 ?>
			<?php if(empty($search_results) == FALSE):?>
				<? foreach($search_results as $s): ?>
					<? $l= _se($s->enrollment_id);?>
					<tr>
					  
						<td><?= $s->studid ?></td>
						<td><?=strtoupper($s->name);?></td>
						<td><?= $s->year ?></td>
						<td><?= $s->course ?></td>
						<td>
							<?= badge_link('fees/view_fees/'.$s->enrollment_id, "primary", 'fees'); ?>
							<?= badge_link('profile/view/'.$s->enrollment_id, "primary", 'profile'); ?>
						</td>
					</tr>
				<? endforeach;?>
			<?php else:?>
				<tr>
					<td colspan=5 class="text-center">----NO data to show---</td>
				</tr>	
			<?php endif;?>
			  <tr>
				  <td colspan=4></td>
				 <td>TOTAL:<div class='badge'><? echo $total_rows;?></div></td>
				</tr>
		</table>
		<?= $links;?>
