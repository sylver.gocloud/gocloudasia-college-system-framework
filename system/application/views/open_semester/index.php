<div class="alert alert-info" >
    <p><span class="fa fa-info-circle" ></span> <span class="bold text-success" >Use</span> is the <strong>Current Semester and School Year</strong> of the system. Checked from the option and click <strong>"Set as Current Semester"</strong> button to update.</p>
    <p>&nbsp;&nbsp;&nbsp;&nbsp; <strong>Enrollment</strong> can only be open and close in the <strong>current open semester</strong>. <strong>Changing</strong> open semester will <strong>close</strong> the enrollment in the previous.</p>
</div>

<a class='btn btn-default btn-sm' href="<?php echo base_url(); ?>open_semester/create" rel="facebox"><span class = 'glyphicon glyphicon-plus'></span>&nbsp;  New Open Semester</a><br/><br/>

<p></p>

<?php 
  $xmsg = "<strong><span class='text-danger' >Warning!</span></strong> Enrollment for current open semester is still open."; 
  $xmsg = ($this->cos->enrollment) ? $xmsg : "";
?>

<?echo form_open('','class="confirm" title="'.$xmsg.' Are you sure to change the system current open semester?"');?>

<div class="table-responsive">
  <table class="table table-sortable table-bordered">
    <thead>
      <tr>
        <th>Use</th>
        <th>Academic Year</th>
        <th>Semester</th>
        <th>Is enrollment open?</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
    
  		 <?php if($open_semesters){ ?>
    <?php foreach( $open_semesters as $open_semester): ?>
      <tr class="<?php echo $open_semester->use === "1" ? "success" : "" ?>">
        <td>
          <?php 
        		$sel = $open_semester->use == '1' ? 'checked' : '';
        		$sel_caption = $open_semester->use == '1' ? 'Current' : '';
        	?>
        	<input type='radio' name='current' <?=$sel?> value='<?=$open_semester->id;?>' /> <?=$sel_caption?>
  		  </td>
        <td><strong><?php echo $open_semester->academic_year; ?></strong></td>
        <td><strong><?php echo $open_semester->name; ?></strong></td>

        <td>
          <?if($open_semester->use === "1"):?>

            <?if($open_semester->open_enrollment == "1"):?> 
              <span class="entypo-lock-open"></span> &nbsp; <span class="bold text-success">Open</span>
              <a href="<?=site_url('open_semester/open_enrollment/'.$open_semester->id.'/0')?>" class="btn btn-default btn-sm confirm" title = "Are you sure to close enrollment for <?=$open_semester->name?> school year <?=$open_semester->academic_year;?>" >Click to Close</a>
            <?else:?>   
              <span class="entypo-lock"></span> &nbsp; <span class="bold text-danger">Close</span>
              <a href="<?=site_url('open_semester/open_enrollment/'.$open_semester->id.'/1')?>" class="btn btn-default btn-sm confirm" title = "Are you sure to open enrollment for <?=$open_semester->name?> school year <?=$open_semester->academic_year;?>" >Click to Open</a>
            <?endif;?>

          <?else:?>   
            <span class="entypo-lock"></span>&nbsp; Close
          <?endif;?>  

        </td>

        <td><a href="<?php echo base_url()."open_semester/edit/".$open_semester->id; ?>" rel="facebox" class="actionlink confirm" title="Edit <?=$open_semester->name?>?"><span class='glyphicon glyphicon-pencil' ></span>&nbsp;  Edit</a> &nbsp; 
        <a href="<?php echo base_url()."open_semester/destroy/".$open_semester->id; ?>" rel="facebox" class="actionlink confirm"><span class='glyphicon glyphicon-trash' ></span>&nbsp; Destroy</a></td>
      </tr>
    <?php endforeach; ?>
    <?php } ?>
    </tbody>
  </table>
</div>

<?
	echo form_submit('','Set as Current Semester');
	?>
	| <a class='btn btn-default btn-sm' href="<?php echo base_url(); ?>open_semester/create" rel="facebox"><span class = 'glyphicon glyphicon-plus'></span>&nbsp;  New Open Semester</a>
	<?
	echo form_close();
?>