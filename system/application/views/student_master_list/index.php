<?$this->load->view('student_master_list/_tab');?>
<?echo form_open('student_master_list/index/0','method="GET"')?>
<div class="well">
	<div class="row">
	  <div class="col-md-2">Sort Record By</div>
	  <div class="col-md-3">	
			<?php
				$a = array(
				  'enrollments.studid' => 'Student ID #',
				  'enrollments.name' => 'Student Name',
				  'enrollments.lastname' => 'Last Name',
				  'enrollments.firstname' => 'First Name',
				  'enrollments.middle' => 'Middle Name',
				  'years.year' => 'Year',
				  'courses.course' => 'Course',
				  'enrollments.sex' => 'Gender',
				  'enrollments.date_of_birth' => 'Birthdate',
				  'enrollments.province' => 'Province',
				);
				
				echo form_dropdown('sort_by',$a,isset($sort_by)?$sort_by:'','id="sort_by"');
			?>
			<?php
				$a = array(
					'ASC' => 'Ascending',
					'DESC' => 'Descending',
				);
			?>
	  </div>
	  <div class="col-md-3">	
			<?php
				$a = array(
					'ASC' => 'Ascending',
					'DESC' => 'Descending',
				);
			?>
			<?=form_dropdown('sort_type',$a,isset($sort_type)?$sort_type:'','id="sort_type"');?>

	  </div>
	   <div class="col-md-3">
			<?=form_submit('submit','Sort')?>
			<?=form_submit('submit','Print','class="btn btn-default btn-sm"')?>
	   </div>
	</div>
</div>
<?echo form_close();?>

<div id="right">
<?echo isset($links) ? $links : NULL;?>
	<?php
	if(isset($search)){
	?>
	<br />
	<table>
		<tr>
		  <th>Student ID</th>
		  <th>Name</th>
		  <th>Year</th>
		  <th>Course</th>
		  <th>Birthdate</th>
		  <th>Gender</th>
		  <th>Province</th>
		</tr>
		<tbody>
		<?php
		$male = 0;
		$female = 0;
		if(!empty($search))
		{
			foreach($search as $student):
				if(strtoupper($student->sex) == "MALE"):
					$male++;
				endif;
				if(strtoupper($student->sex) == "FEMALE"):
					$female++;
				endif;
			?>
			<tr>
			<td><?php echo $student->studid; ?></td>
			<td><?php echo ucfirst($student->name); ?></td>
			<td><?php echo $student->year; ?></td>
			<td><?php echo $student->course; ?></td>
			<td><?php echo date('m-d-Y', strtotime($student->date_of_birth)); ?></td>
			<td><?php echo $student->sex; ?></td>
			<td><?php echo $student->province; ?></td>
			</tr>
			<?php 
			endforeach; ?>
			<tr>
				<td colspan='5' align='right'>
					<b>Total Male</b>
					<span class='badge'><?=($male) ? $male : 0;?></span>
					|
					<b>Total Female</b>
					<span class='badge'><?=($female) ? $female : 0;?></span>
				</td>
				<td colspan='1' align='right'><b>Total Students</b></td>
				<td><span class='badge'><?=($total_rows) ? $total_rows : 0;?></span></td>
			</tr>
			<?php
		}
		else
		{
		?>
		<tr>
		<td colspan="5">
			Student Not Found
		</td>
		</tr>
		<?php
		}
		?>
		</tbody>
		<tr>
		  <th>Student ID</th>
		  <th>Name</th>
		  <th>Year</th>
		  <th>Course</th>
		  <th>Birthdate</th>
		  <th>Gender</th>
		  <th>Province</th>
		</tr>
	</table>
	<?echo isset($links) ? $links : NULL;?>
	<?php
	}
	?>
	</div>
	
	<div class="clear"></div>
</div>
