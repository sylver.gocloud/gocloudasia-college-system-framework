<?$this->load->view('student_master_list/_tab');?>
<?echo form_open('','method="GET"')?>
<?$this->load->view('student_master_list/_filter');?>
<?echo form_close();?>
<table>
	<tr>
	  <th>Years</th>
	  <th>Male</th>
	  <th>Female</th>
	  <th>Total Students</th>
	  <th>Action</th>
	</tr>
	<tbody>
	<?php
	if(!empty($search))
	{
		$t_male = 0;
		$t_female = 0;
		
		foreach($search as $obj):
		
		$student = $obj['data'];
		$male = $obj['male'];
		$t_male += $male;
		$female = $obj['female'];
		$t_female += $female;
		$total_rows = $obj['total'];
		$total = $male;
		
		?>
		<tr>
			<td><?php echo $student->year; ?></td>
			<td><span class='badge'><?=$male?></span></td>
			<td><span class='badge'><?=$female?></span></td>
			<td><span class='badge'><?=$total_rows?></span></td>
			<td>
				<?if($total_rows > 0):?>
				
				<a class='btn btn-default btn-sm btn-sm' href="view_students/year/<?=$student->id?>"><span class='glyphicon glyphicon-th-list' ></span>&nbsp; View </a>
				<a class='btn btn-default btn-sm btn-sm' href="view_students/year/<?=$student->id?>?sort_by=enrollments.studid&sort_type=ASC&submit=Print"><span class='glyphicon glyphicon-print' ></span>&nbsp; Print </a>
				
				<?endif;?>
			</td>
		</tr>
		<?php 
		endforeach; ?>
		<tr>
			<td>Total years <span class='badge'><?=count($search)?></span></td>
			<td>Total Male <span class='badge'><?=$t_male?></span></td>
			<td>Total Female <span class='badge'><?=$t_female?></span></td>
			<td>Total Students <span class='badge'><?=$t_male + $t_female?></span></td>
		</tr>
		<?php
	}
	else
	{
	?>
	<tr>
	<td colspan="5">
		No record found.
	</td>
	</tr>
	<?php
	}
	?>
	</tbody>
</table>