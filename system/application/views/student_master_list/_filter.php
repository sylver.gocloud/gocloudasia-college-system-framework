<div class="well">
	<div class="row">
	  <div class="col-md-2">Sort Record To</div>
	  <div class="col-md-3">	
			<?php
				$a = array(
					'ASC' => 'Ascending',
					'DESC' => 'Descending',
				);
			?>
			<?=form_dropdown('sort_type',$a,isset($sort_type)?$sort_type:'','id="sort_type"');?>

	  </div>
	   <div class="col-md-3">
			<?=form_submit('submit','Sort')?>
			<?=form_submit('submit','Print','class="btn btn-default btn-sm"')?>
	   </div>
	</div>
</div>