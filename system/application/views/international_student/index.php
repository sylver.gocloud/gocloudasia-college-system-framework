<div id="right">

	<?php 
		$formAttrib = array('id' => 'search_enrollee', 'class' => 'form-inline', 'method' => 'GET');
		echo form_open('', $formAttrib);
	?>
    
 <div class="form-group">
    <input id="lastname" class="form-control" type="text" value="<?=isset($lastname)?$lastname:''?>" name="lastname" placeholder="Last Name">
  </div>
   
  <div class="form-group">
    <input id="fname" class="form-control" type="text" value="<?=isset($fname)?$fname:''?>" name="fname" placeholder="First Name">
  </div>
   
  <div class="form-group">
    <input id="studid" class="form-control" type="text" value="<?=isset($studid)?$studid:''?>" name="studid" placeholder="Student ID">
  </div>
  <div class="form-group">
    <?=year_dropdown('year_id',isset($year_id)?$year_id:'',"", '')?>
  </div>
   
	<?php
	echo form_hidden('semester_id', $this->open_semester->id);
	echo form_hidden('sy_from', $this->open_semester->year_from);
	echo form_hidden('sy_to', $this->open_semester->year_to);
	echo form_hidden('is_paid', 1);
	?>
   		<?php echo form_submit('submit', 'Search'); ?>
   		<?php echo form_submit('submit', 'Print'); ?>
		<?echo form_close();?>
<br>
<?echo isset($links) ? $links : NULL;?>
	<?php
	if(isset($search)){
	?>
	<br />
	<table>
		<tr>
		  <th>Student ID</th>
		  <th>Name</th>
		  <th>Year</th>
		  <th>Course</th>
		  <th>Nationality</th>
		  <th>View</th>
		</tr>
		<tbody>
		<?php
		if(!empty($search))
		{
			foreach($search as $student):
			$l = _se($student->id);
			?>
			<tr>
			<td><?php echo $student->studid; ?></td>
			<td><?php echo ucfirst($student->name); ?></td>
			<td><?php echo $student->year; ?></td>
			<td><?php echo $student->course; ?></td>
			<td><?php echo strtoupper($student->nationality); ?></td>
			<td>
				<?
					//RESTRICT ACCESS TO PROFILE
					switch(strtolower($this->session->userdata['userType']))
					{
						case "custodian":
						case "librarian":
							//CAN ONLY ACCESS ISSUES
							echo badge_link('issues/view/'.$student->id.'/'.$student->user_id, "primary", 'issues');
						break;
						case "guidance":
							//CANT ACCESS FEES
							echo badge_link('profile/view/'.$student->id, "primary", 'profile');
						break;
						default:
							echo badge_link('profile/view/'.$student->id, "primary", 'profile');
							echo badge_link('fees/view_fees/'.$student->id, "primary", 'fees');
						break;
					}
					
					
				?>
				
			</td>
			</tr>
			<?php 
			endforeach; ?>
			<tr>
				<td colspan='5' align='right'><b>Total Records</b></td>
				<td><span class='badge'><?=($total_rows) ? $total_rows : 0;?></span></td>
			</tr>
			<?php
		}
		else
		{
		?>
		<tr>
		<td colspan="5">
			Student Not Found
		</td>
		</tr>
		<?php
		}
		?>
		</tbody>
		<tr>
		  <th>Student ID</th>
		  <th>Name</th>
		  <th>Year</th>
		  <th>Course</th>
		  <th>Nationality</th>
		  <th>Action</th>
		</tr>
	</table>
	<?echo isset($links) ? $links : NULL;?>
	<?php
	}
	?>
	</div>
	
	<div class="clear"></div>
</div>
