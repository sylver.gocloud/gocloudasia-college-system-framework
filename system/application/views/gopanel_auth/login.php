<style type="text/css">
  body{
    /** Ref : http://lea.verou.me/css3patterns/#starry-night*/
    background-color:black;
    background-image:
    radial-gradient(white, rgba(255,255,255,.2) 2px, transparent 40px),
    radial-gradient(white, rgba(255,255,255,.15) 1px, transparent 30px),
    radial-gradient(white, rgba(255,255,255,.1) 2px, transparent 40px),
    radial-gradient(rgba(255,255,255,.4), rgba(255,255,255,.1) 2px, transparent 30px);
    background-size: 550px 550px, 350px 350px, 250px 250px, 150px 150px; 
    background-position: 0 0, 40px 60px, 130px 270px, 70px 100px;
  }
</style>

<div id="login-overlay" class="modal-dialog">
  <div class="modal-content">
      <div class="modal-header">
          <h4 class="modal-title" id="myModalLabel"><img src="<?=site_url('assets/images/gocloud_logo.png')?>" width="60" height="30"> GoPanel Login</h4>
      </div>
      <div class="modal-body">
      		<div class="row"><?=isset($system_message)?$system_message:""?></div>
          <div class="row">
              <div class="col-xs-6">
                  <div class="well">
                      <?=form_open('')?>
                          <div class="form-group">
                              <label for="username" class="control-label">Username</label>
                              <input type="text" class="form-control" id="username" name="username" value="<?=set_value('username')?>"  title="Please enter you username" placeholder="Username" autocomplete="off" required>
                              <span class="text-danger"><?=form_error('username')?></span>
                          </div>
                          <div class="form-group">
                              <label for="password" class="control-label">Password</label>
                              <input type="password" class="form-control" id="password" name="password" value="<?=set_value('password')?>"  title="Please enter your password" placeholder="Password" autocomplete="off" required>
                              <span class="text-danger"><?=form_error('password')?></span>
                          </div>
                          <div id="loginErrorMsg" class="alert alert-error hide">Wrong username og password</div>
                          <div class="form-group">
                              <label for="captcha" class="control-label">Captcha(Enter Words Below)</label>
                              <input type="text" class="form-control" id="captcha" name="captcha" value=""  placeholder="captcha" autocomplete="off" required>
                              <span class="text-danger"><?=form_error('captcha')?></span>
                          </div>
                          <div class="form-group">
                              <div>
                              	<?=$captcha_image;?>
																<input type="hidden" name="fit" value="<?=$form_token;?>"/>
                              </div>
                              <span class="help-block"></span>
                          </div>
                          
                          <button type="submit" name="login_me" value="login_me" class="btn btn-success btn-block">Login</button>
                          <!-- <a href="/forgot/" class="btn btn-default btn-block">Help to login</a> -->
                      </form>
                  </div>
              </div>
              <div class="col-xs-6">
                  <p class="lead"><span class="text-danger">Restricted Area</span></p>
                  <ul class="list-unstyled" style="line-height: 2">
                      <li><span class="fa fa-check text-success"></span> For authorized user only.</li>
                  </ul>
              </div>
          </div>
      </div>
  </div>
</div>

<?/** LAYOUT REF : http://bootsnipp.com/snippets/featured/login-form-in-modal */?>