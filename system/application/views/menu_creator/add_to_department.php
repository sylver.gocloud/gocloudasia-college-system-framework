<div class="row">
	<?=panel_head2('Menu to add')?>

		<div class="table-responsive">
			<table class="table">
				<tr class="success bold text-bold">
					<td>Controller</td>
					<td>Caption</td>
				</tr>
				<tr>
					<td><?=$menu->controller?></td>
					<td><?=$menu->caption?></td>
				</tr>
			</table>
		</div>

	<?=panel_tail2();?>


	<?=panel_head2('Select Department')?>

		<div class="col col-md-6">

			<?if($departments):?>
				
				<?=form_open();?>

				<ul class="list-group">
					
					<?foreach ($departments as $k => $v):?>
						<li class="list-group-item">
							<span class="pull-left"><input type="checkbox" name="dep[]" value="<?=$v->id?>"></span> &nbsp; <i class="fa fa-hand-o-right"></i>&nbsp;
							<?=ucwords($v->department)?>
						</li>					
					<?endforeach;?>
				</ul>

				<button type="submit" name="add_to_deparment" value="add_to_deparment" class="btn btn-sm btn-primary" > Submit</button>

				<?=form_close();?>

			<?else:?>

				<div class="alert alert-danger"><i class="fa fa-info"></i>&nbsp; No Department available.</div>
			<?endif;?>

		</div>

	<?=panel_tail2();?>

</div>