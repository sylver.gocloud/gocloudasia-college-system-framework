<?$this->load->view('layouts/menu_creator/_header');?>
<table>
	<tr>
		<th>Department</th>
		<th>Menus</th>
	</tr>
<?if($departments):?>
	<?foreach($departments as $dep):?>
		<tr>
			<td><?=strtoupper($dep->department)?></td>
			<td>
				<table>
					<tr>
						<th>Header Menu <a class='btn btn-default btn-sm btn-sm' href='<?base_url()?>menu_creator/create_header_menu/<?=$dep->department?>'>ADD</a></th>
						<th>Order</th>
						<th>Submenu</th>
					</tr>
					<?if(isset($headers[$dep->department])):?>
						<?foreach($headers[$dep->department] as $head):?>
							<tr>
								<td><?=$head->caption;?> &nbsp; <a href='<?base_url()?>menu_creator/delete_header/<?=$dep->department?>/<?=$head->menu_grp?>' title='Delete Header Menu' class='confirm' ><span class='glyphicon glyphicon-trash'></span></a>
								
								<?if($head->visible == 0):?>
								<a href='<?base_url()?>menu_creator/show_menu/<?=$head->id?>' title='Show Menu' class='' ><span class='glyphicon glyphicon-eye-open'></span></a>
								<?else:?>
								<a href='<?base_url()?>menu_creator/hide_menu/<?=$head->id?>' title='Hide Menu' class='' ><span class='glyphicon glyphicon-eye-close'></span></a>
								<?endif;?>
								
								</td>
								<td><?=$head->menu_num;?></td>
								<td>
									<a class='btn btn-default btn-sm btn-sm' href='<?=base_url()?>/menu_creator/create_sub_menu/<?=$dep->department?>/<?=$head->menu_sub?>/<?=$head->caption?>'>Create Submenu</a>
									<table>
										<tr>
											<th>Caption</th>
											<th>Controller</th>
											<th>Menu Group</th>
											<th>Menu Sub</th>
											<th>Menu Number</th>
											<th>Menu Level</th>
											<th>Visible</th>
											<th>Action</th>
										</tr>
										<?if(isset($level2_menus[$dep->department][$head->menu_sub])):?>
										<?foreach($level2_menus[$dep->department][$head->menu_sub] as $menus):?>
											<tr>
												<td><?=$menus->caption?></td>
												<td><?=$menus->controller?></td>
												<td><?=$menus->menu_grp?></td>
												<td><?=$menus->menu_sub?></td>
												<td><?=$menus->menu_num?></td>
												<td><?=$menus->menu_lvl?></td>
												<td><?=$menus->visible?></td>
												<td>
													<a href='<?base_url()?>menu_creator/delete/<?=$menus->id?>' title='Delete Menu' class='confirm' ><span class='glyphicon glyphicon-trash'></span></a>
													<?if($menus->visible == 0):?>
													<a href='<?base_url()?>menu_creator/show_menu/<?=$menus->id?>' title='Show Menu' class='' ><span class='glyphicon glyphicon-eye-open'></span></a>
													<?else:?>
													<a href='<?base_url()?>menu_creator/hide_menu/<?=$menus->id?>' title='Hide Menu' class='' ><span class='glyphicon glyphicon-eye-close'></span></a>
													<?endif;?>
												</td>
											</tr>
										<?endforeach;?>
										<?endif;?>
									</table>
								</td>
							</tr>
						<?endforeach;?>
					<?endif;?>
				</table>
			</td>
		</tr>
	<?endforeach;?>
	
<?endif;?>
</table>
<?$this->load->view('layouts/menu_creator/_footer');?>
