  <div class="row">
	<div class="col-md-6">     
		<? $UserType_desc = str_replace("_"," ", $UserType); ?>               
        <!-- <div class="header"><h3><strong><?echo ucwords($UserType_desc)?></strong></h3></div> -->
        <div class="panel panel-default" style='border:1px solid gray; !important'>

            <div class="panel-heading black">
                <div class="panel-title white-font"><strong>User Authentication</strong></div>
            </div>     

            <div style="padding-top:30px" class="panel-body" >
                
            	

			    <?php if(isset($system_message)) echo $system_message;
				  echo validation_errors('<div class="alert alert-danger">', '</div>');
			    ?>
                    
                <?=form_open('','id="loginform" class="form-horizontal" role="form"')?>
                    
                    <div class="form-group">
					  <label class="col-md-2 control-label" for="login">Username</label>
					  <div class="col-md-9">
					    <div class="input-group">
					      <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
					      <input autofocus id="login" name="login" class="form-control" placeholder="Login / Username" value="<?=set_value('login')?>" type="text" required>
					    </div>
					  </div>
					</div>

					<div class="form-group">
					  <label class="col-md-2 control-label" for="password">Password</label>
					  <div class="col-md-9">
					    <div class="input-group">
					      <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
					      <input id="password" name="password" class="form-control" placeholder="Password" value="<?=set_value('password')?>" type="password" autoComplete="Off" required>
					    </div>
					  </div>
					</div>

					<div class="form-group">
					  <label class="col-md-2 control-label" for="department">Department</label>
					  <div class="col-md-9">
					    <div class="input-group">
					      <span class="input-group-addon"><i class="fa fa-users"></i></span>
					      <strong><?=form_dropdown('department', $departments, set_value('department', $department), 'class="form-control" required');?></strong>
					    </div>
					  </div>
					</div>

					<div class="form-group">
					  <label class="col-md-2 control-label" for="captcha">Captcha</label>

					  <div class="col-md-4">
					    <div class="input-group">
					      <span class="input-group-addon"><i class="glyphicon glyphicon-barcode"></i></span>
					      <input id="captcha" name="captcha" class="form-control" placeholder="Captcha" value="<?=set_value('captcha')?>" type="text" autocomplete="off" required>
					    </div>
					  </div>

					  <div class="col-md-4">
					    <div class="input-group">
					     	<?echo $cap_image;?>
					     		<script>
						     		$('img').addClass('img-responsive');
						     	</script>
					    </div>
					        <p class="help-block">Verify that you are human.</p>
					  </div>
					</div>

					<div class="form-group">
					  <label class="col-md-2 control-label" for="prependedtext"></label>
					  <div class="col-md-9">
					    <div class="btn-group">

					      <!-- <input class="btn btn-default btn-sm btn-sm" name="commit" type="submit" value="Login" /> -->
					      <label for="mySubmit" class="btn btn-primary btn-sm btn-sm"><i class="fa fa-unlock"></i>&nbsp; Login</label>
					      <input id="mySubmit" type="submit" value="Login" name="" class="hidden" />

                          <a id="btn-clr" href="javascript:;" onclick="clear_e()" class="btn btn-default btn-default btn-sm  btn-sm"><i class="fa fa-refresh"></i>&nbsp; Clear Entries</a>
                          <script type="text/javascript">
                          	function clear_e()
                          	{
                          		$('input[type="text"], input[type="password"]').val('');
                          	}
                          </script>
                          <a href="<?=site_url()?>" class="btn btn-default btn-default btn-sm  btn-sm"><i class="fa fa-home"></i>&nbsp; Home</a>
					    </div>
					  </div>
					</div>


                    <!-- <div class="form-group">
                        <div class="col-md-12 control">
                            <div style="border-top: 1px solid#888; padding-top:15px; font-size:85%" >
                                Don't have an account! 
                            <a href="#" onClick="$('#loginbox').hide(); $('#signupbox').show()">
                                Sign Up Here
                            </a>
                            </div>
                        </div>
                    </div>  -->

                </form> 
            </div>                     
        </div>  
    </div>
    <div class="col-md-6">                    
        <!-- <div class="header"><h3><strong><br/></strong></h3></div> -->
        <div class="panel panel-default">

            <div class="panel-heading">
                <span>INSTRUCTIONS</span>
            </div>     

            <div style="padding-top:30px" class="panel-body" >
            	<ul>
            		<li>To sign-in, specify your user name, password and captcha and click the login button.</li>
            		<li>To clear entries, click on the Clear Entries button.</li>
            		<li>To change the captcha, just refresh the page.</li>
            	</ul>
				
            </div>                     
        </div>  
    </div>
</div>

<!-- <div>
  <div class="col-md-4 col-md-offset-4">
    <p class="text-center">  
    <?php if(isset($system_message)) echo $system_message;
	  echo validation_errors('<div class="alert alert-danger">', '</div>');
    ?>
    </p>
  </div>
	<?php echo form_open('', "class='form-signin'");?>
	<?php
		$UserType_desc = str_replace("_"," ", $UserType);
	?>
	<h2 class="form-signin-heading"><?php echo ucwords($UserType_desc); ?></h2>
		<input class="form-control" type="text" value="" name="login" placeholder="Username">
		<input class="form-control"type="password" value="" name="password" placeholder="Password">
		<input class="form-control" type="text" value="" name="captcha" placeholder="Captcha" autoComplete="Off" >
		<h5 class="form-signin-heading">Verify that you are human. Enter the letters below.</h5>
		<?echo $cap_image;?>
		<br/>
		<br/>
		<input class="btn btn-lg btn-default btn-sm btn-block" name="commit" type="submit" value="Login" />
		<br/>
		<a href='<?=base_url()?>'><span class='glyphicon glyphicon-hand-left'></span>&nbsp; Go back to departments</a>
  <?php
	echo form_hidden('userType', $UserType);
	echo form_close();
  ?>
</div> -->

