<?php
	#FIRST GROUP THE MENUS 
	$group_menus = array();

	if(isset($user_menus) && $user_menus){
		$grp_key = 0;
		foreach ($user_menus as $key => $men) {

			if(trim($men->controller) === "#"){ //this will become the Header Menu
				$grp_key = $key;
				$group_menus[$grp_key]['head'] = $men;
			}else{ //this are the sub-menus
				$group_menus[$grp_key]['sub'][] = $men;
			}
		}
	}

	$men_ctr = 0;

	/** REF : http://startbootstrap.com/template-overviews/sb-admin-2/ */
?>
		
<?foreach ($group_menus as $key => $value):?>
	<?php 
		$value = (object)$value;
	?>
	<li>
		<a href="#"><i class="fa fa-files-o fa-fw"></i> <?=$value->head->caption?><span class="fa arrow"></span></a>
		<ul class="nav nav-second-level">
			<?if($value->sub):?>
    		<?foreach ($value->sub as $sk => $sub_menu):?>
    			<?if($sub_menu->controller === "confirm_enrollees"
							|| $sub_menu->controller === "confirm_enrollees/lists"
							|| $sub_menu->controller === "search/search_enrollee"
							|| $sub_menu->controller === "search/search_student"
						):
							$s_count = 0;
							switch ($sub_menu->controller) {
								case 'confirm_enrollees':
									$s_count = count_confirm_enrollees();
									break;
								case 'confirm_enrollees/lists':
									$s_count = count_list_confirm_enrollees();
									break;
								case 'search/search_enrollee':
									$s_count = count_enrollees();
									break;
								case 'search/search_student':
								$s_count = count_official_enrollees();
								break;
									
							}?>

							<li><a href="<?=site_url()?><?=$sub_menu->controller;?>" class="menu_tp" data-toggle="tooltip" data-placement="right" title="<?=isset($this->syspar->show_menu_tip) && $this->syspar->show_menu_tip == 1 ? $sub_menu->remarks : ""?>"><i class="<?=$sub_menu->menu_icon?>"></i> &nbsp; <span class="badge badge-success bold"><?=$s_count?></span> <?=$sub_menu->caption?> </a></li>
					<?else:?>
							<li><a href="<?=site_url()?><?=$sub_menu->controller;?>" class="menu_tp" data-toggle="tooltip" data-placement="right" title="<?=isset($this->syspar->show_menu_tip) && $this->syspar->show_menu_tip == 1 ? $sub_menu->remarks : ""?>"><i class="<?=$sub_menu->menu_icon?>"></i> &nbsp; <?=$sub_menu->caption?> </a></li>
					<?endif;?>
    		<?endforeach;?>
    	<?endif;?>		
		</ul>
  </li>
<? $men_ctr++; endforeach;?>