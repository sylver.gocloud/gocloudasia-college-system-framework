<?$has_others = false;?>
<div class="row" >
	<div class="col-md-12">
		<?if($subject_master):?>
			<ul class="nav nav-pills nav-pills-xs" id="subTab">
			<?foreach ($subject_master as $key => $value):?>
				<?
					if($key == "others"){
						$has_others = true;
						continue;
					}
				?>
				<li><a class="btn btn-xs" href="#<?=$key?>" data-toggle="tab"><?=strtoupper($key)?></a></li>
					
			<?endforeach;?>
			<?if($has_others):?>
				<li><a class="btn btn-xs" href="#others" data-toggle="tab">Others</a></li>
			<?endif;?>
			</ul>

			<!-- Tab panes -->
			<div class="tab-content">
				<?foreach ($subject_master as $key => $value):?>
					<div class="tab-pane active" id="<?=$key?>">

						<div class="table-responsive">
							<table class=" table-sortable table">
								<thead>
							  <tr>
							    <th>Check</th>
							    <th>Course No.</th>
							    <th>Description</th>
							    <th>Unit</th>
							    <th>Lec</th>
							    <th>Lab</th>
							  </tr>
							  </thead>
							  	<?if($value):?>
								<?foreach($value as $subject):?>
									<tr>
										<td>
											<input type='checkbox' name='subject[]' value='<?=$subject->id;?>' class="checkbox_subject" />
										</td>
										<td class="bold" ><?=$subject->code;?></td>
										<td class="bold" ><?=$subject->subject;?></td>
										<td><?=$subject->units;?></td>
										<td><?=$subject->lec;?></td>
										<td><?=$subject->lab;?></td>
									</tr>
								<?endforeach;?>
								<?else:?>
									<tr>
										<td colspan="11" >No Subjects Available</td>
									</tr>
								<?endif;?>
							</table>
						</div>	

					</div>
			  	<?endforeach;?>
			</div>

		<?else:?>
			<div class="alert alert-info"><span class="fa fa-info-circle"></span>&nbsp; No Subject found. You can create <a class='btn btn-primary btn-xs' target="_blank" href="<?=site_url('master_subjects/create')?>">here</a>.</div>
		<?endif;?>
	</div>
</div>

<script>
  $(function () {
    $('#subTab a:first').tab('show');

    $('.checkbox_subject').on('change', function(){
    	var count = parseFloat($('#subject_count').text().trim());
    	var status = $(this).attr('checked');

    	if(status){
    		count += 1;
    	}else{
    		count -= 1;
    	}

    	$('#subject_count').text(count);
    })

  })
</script>

