<style>
	.mylabel{
		font-size:11pt;
	}
	.row > div{
		#border : 1px solid gray;
	}
	
	input select {
		border-color: gray;
	}
</style>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/enrollment.js"></script>

<fieldset class="scheduler-border">
			<legend class="scheduler-border">Personal Information</legend>		
			<p></p>

			<div class="row">
				<div class="col-sm-2 mylabel">Full Name *</div>
				<div class="col-sm-3 mylabel"><input placeHolder="Last Name" label="Last Name" class="not_blank" id="lastname" name="user[enrollment_attributes][lastname]" maxlength="100" title="Last Name" type="text"  value='<?=set_value('user[enrollment_attributes][lastname]',isset($enroll) ? $enroll->lastname: "")?>' /><?php echo form_error('user[enrollment_attributes][lastname]'); ?></div>
				<div class="col-sm-3 mylabel"> <input placeHolder="First Name" label="First Name" class="not_blank" id="fname" name="user[enrollment_attributes][fname]" maxlength="100" title="First Name" type="text" value='<?=set_value('user[enrollment_attributes][fname]',isset($enroll) ? $enroll->fname: "")?>' /><?php echo form_error('user[enrollment_attributes][fname]'); ?></div>
				<div class="col-sm-3 mylabel"> <input placeHolder="Middle Name" label="Middle Name" class="not_blank" id="middlename" name="user[enrollment_attributes][middle]" maxlength="100" title="Middle Name" type="text" value='<?=set_value('user[enrollment_attributes][middle]',isset($enroll) ? $enroll->middle: "")?>' /><?php echo form_error('user[enrollment_attributes][middle]'); ?></div>
				<div class="col-sm-1 mylabel"> <input placeHolder="Ext. (Jr)" id="name_ext" name="user[enrollment_attributes][name_ext]" maxlength="15" title="Name Extention (Jr)" type="text" value='<?=set_value('user[enrollment_attributes][name_ext]',isset($enroll) ? $enroll->name_ext: "")?>' /></div>
			</div>
			<p></p>

			<div class="row">
				<div class="col-sm-2 mylabel">Address *</div>
				<div class="col-sm-2 mylabel"><input placeHolder="Street No. / House No." id="user_enrollment_attributes_street_no" name="user[enrollment_attributes][street_no]" label="Street No. / House No." class='not_blank' size="10" type="text" value='<?=set_value('user[enrollment_attributes][street_no]',isset($enroll) ? $enroll->street_no: "")?>' /><?php echo form_error('user[enrollment_attributes][street_no]'); ?></div>
				<div class="col-sm-2 mylabel"><input placeHolder="Barangay" id="user_enrollment_attributes_street_no" name="user[enrollment_attributes][barangay]" label="Barangay" class='not_blank' size="10" type="text" value='<?=set_value('user[enrollment_attributes][barangay]',isset($enroll) ? $enroll->barangay: "")?>' /><?php echo form_error('user[enrollment_attributes][barangay]'); ?></div>
				<div class="col-sm-3 mylabel"><input placeHolder="City/Municipality" label="City/Municipality" class="not_blank" name="user[enrollment_attributes][municipal]" value='<?=set_value('user[enrollment_attributes][municipal]',isset($enroll) ? $enroll->municipal: "")?>' size="30" type="text" /><?php echo form_error('user[enrollment_attributes][municipal]'); ?></div>
				<div class="col-sm-3 mylabel"><input placeHolder="Province" label="Province" class="not_blank" name="user[enrollment_attributes][province]" value='<?=set_value('user[enrollment_attributes][province]',isset($enroll) ? $enroll->province: "")?>' size="30" type="text" /><?php echo form_error('user[enrollment_attributes][province]'); ?></div>
			</div>
			<p></p>
			
			<div class="row">
				<div class="col-sm-2 mylabel">Date of Birth *</div>
				<div class="col-sm-2 mylabel"><input placeHolder="Birth Date" label="Birth Date" class='not_blank date_pick' id="birthdate" name="user[enrollment_attributes][date_of_birth]" size="10" type="text" value='<?=set_value('user[enrollment_attributes][date_of_birth]',isset($enroll) ? $enroll->date_of_birth: "")?>' /><?php echo form_error('user[enrollment_attributes][date_of_birth]'); ?></div>
				<div class="col-sm-2 mylabel">Birth Place *</div>
				<div class="col-sm-3 mylabel"> <input placeHolder="Place of Birth" label="Place of Birth" class="not_blank" id="birthplace" name="user[enrollment_attributes][place_of_birth]" size="20" type="text" value='<?=set_value('user[enrollment_attributes][place_of_birth]',isset($enroll) ? $enroll->place_of_birth: "")?>' /><?php echo form_error('user[enrollment_attributes][place_of_birth]'); ?></div>
				<div class="col-md-1 mylabel">Gender *</div>
			  <div class="col-md-2">
					<?						
						$gender = array(
							"Male" => "Male",
							"Female" => "Female"
						)
					?>
					<?=form_dropdown('user[enrollment_attributes][sex]', $gender,set_value('user[enrollment_attributes][sex]',isset($enroll)?$enroll->sex:''),'label="Gender" class="not_blank" required')?>
					<?php echo form_error('user[enrollment_attributes][sex]'); ?>
			  </div>
			</div>
			<p></p>
			<div class="row">
			  <div class="col-md-2 mylabel">Civil Status *</div>
			  <div class="col-md-2">
					<?						
						$cs = array(
							"" => "Select Civil Status",
							"Single" => "Single",
							"Married" => "Married",
							"Widowed" => "Widowed",
							"Divorced" => "Divorced",
						)
					?>
					<?=form_dropdown('user[enrollment_attributes][civil_status]', $cs,set_value('user[enrollment_attributes][civil_status]',isset($enroll)?$enroll->civil_status:''),'label="Civil Status" class="not_blank"')?>
			  	<?php echo form_error('user[enrollment_attributes][civil_status]'); ?>
			  </div>
			  <div class="col-sm-2 mylabel">Contact Number *</div>
				<div class="col-sm-2 mylabel"><input placeHolder="Cp / Tel. No" label="Cellphone or Telephone No." class="not_blank" id="mobile" name="user[enrollment_attributes][mobile]" size="20" type="text" value='<?=set_value('user[enrollment_attributes][mobile]',isset($enroll) ? $enroll->mobile: "")?>' /><?php echo form_error('user[enrollment_attributes][mobile]'); ?></div>
				<div class="col-sm-1 mylabel">Email *</div>
				<div class="col-sm-3 mylabel"><input label="Email Address" class="not_blank form-control" id="fake_email" name="user[enrollment_attributes][fake_email]" size="10" type="email" value='<?=set_value('user[enrollment_attributes][fake_email]',isset($enroll) ? $enroll->fake_email: "")?>' /><?php echo form_error('user[enrollment_attributes][fake_email]'); ?></div>
			</div>
			
			<p></p>
			<div class="row">
				<div class="col-sm-2 mylabel">Nationality *</div>
				<div class="col-sm-2 mylabel"><input placeHolder="Nationality" label="Nationality" class="not_blank" id="nationality" name="user[enrollment_attributes][nationality]" size="20" type="text" value='<?=set_value('user[enrollment_attributes][nationality]',isset($enroll) ? $enroll->nationality: "Filipino")?>' /><?php echo form_error('user[enrollment_attributes][nationality]'); ?></div>
				<div class="col-sm-2 mylabel">Are you a foreigner?</div>
				<div class="col-sm-2 mylabel">
					<?						
						$lp = array(
							"0" => "No",
							"1" => "Yes"
						)
					?>
					<?=form_dropdown('user[enrollment_attributes][is_foreigner]', $lp,set_value('user[enrollment_attributes][is_foreigner]',isset($enroll)?$enroll->is_foreigner:''),'class=""')?>
					<?php echo form_error('user[enrollment_attributes][is_foreigner]'); ?>
				</div>
				<div class="col-sm-1 mylabel">Religion *</div>
				<div class="col-sm-3 mylabel"> <input placeHolder="Religion" label="Religion" class="not_blank" id="religion" name="user[enrollment_attributes][religion]" size="20" type="text" value='<?=set_value('user[enrollment_attributes][religion]',isset($enroll) ? $enroll->religion: "")?>' /><?php echo form_error('user[enrollment_attributes][religion]'); ?></div>
			</div>
</fieldset>

<fieldset class="scheduler-border">
			<legend class="scheduler-border">Parent's Information</legend>	
			<div class="row">
				<div class="col-sm-2 mylabel">Father's Name *</div>
				<div class="col-sm-6 mylabel"><input placeHolder="Father's Name" label="Father's Name" class='not_blank' id="father_name" name="user[enrollment_attributes][father_name]" size="20" type="text" value='<?=set_value('user[enrollment_attributes][father_name]',isset($enroll) ? $enroll->father_name: "")?>' /><?php echo form_error('user[enrollment_attributes][father_name]'); ?></div>
				<div class="col-sm-2 mylabel">Contact Number *</div>
				<div class="col-sm-2 mylabel"> <input label="Father's Contact No." id="father_contact_no" class="not_blank" name="user[enrollment_attributes][father_contact_no]" size="15" type="text" value='<?=set_value('user[enrollment_attributes][father_contact_no]',isset($enroll) ? $enroll->father_contact_no: "")?>' /><?php echo form_error('user[enrollment_attributes][father_contact_no]'); ?></div>
			</div>
			
			<p></p>
			<div class="row">
				<div class="col-sm-2 mylabel">Mother's Name *</div>
				<div class="col-sm-6 mylabel"><input placeHolder="Mother's Name" label="Mother's Name" id="mother_name" name="user[enrollment_attributes][mother_name]" size="20" type="text" class='not_blank' value='<?=set_value('user[enrollment_attributes][mother_name]',isset($enroll) ? $enroll->mother_name: "")?>' /><?php echo form_error('user[enrollment_attributes][mother_name]'); ?></div>
				<div class="col-sm-2 mylabel">Contact Number *</div>
				<div class="col-sm-2 mylabel"><input label="Mother's Contact No." id="mother_contact_no" name="user[enrollment_attributes][mother_contact_no]" size="15" type="text" class="not_blank" value='<?=set_value('user[enrollment_attributes][mother_contact_no]',isset($enroll) ? $enroll->mother_contact_no: "")?>' /><?php echo form_error('user[enrollment_attributes][mother_contact_no]'); ?></div>
			</div>

			<p></p>
			<div class="row">
				<div class="col-sm-2 mylabel">Guardian's Name *</div>
				<div class="col-sm-6 mylabel"><input placeHolder="Guardian's Name" label="Guardian's Name" id="guardian_name" name="user[enrollment_attributes][guardian_name]" size="20" type="text" value='<?=set_value('user[enrollment_attributes][guardian_name]',isset($enroll) ? $enroll->guardian_name: "")?>' /><?php echo form_error('user[enrollment_attributes][guardian_name]'); ?></div>
				<div class="col-sm-2 mylabel">Contact Number *</div>
				<div class="col-sm-2 mylabel"><input label="Guardian's Contact No." id="guardian_contact_no" name="user[enrollment_attributes][guardian_contact_no]" size="15" type="text" value='<?=set_value('user[enrollment_attributes][guardian_contact_no]',isset($enroll) ? $enroll->guardian_contact_no: "")?>' /><?php echo form_error('user[enrollment_attributes][guardian_contact_no]'); ?></div>
			</div>
</fieldset>

<fieldset class="scheduler-border">
			<legend class="scheduler-border">Preliminary Education</legend>		
			<strong><span class="label label-default">Elementary</span></strong>
			<div class="row">
				<div class="col-sm-2 mylabel">Name of School *</div>
				<div class="col-sm-6 mylabel">
					<input placeHolder = "Name of Elementary School" label="Name of Elementary School" class="not_blank" id="elem_name" name="user[enrollment_attributes][elementary]" size="50" title="" type="text" value='<?=set_value('user[enrollment_attributes][elementary]',isset($enroll) ? $enroll->elementary: "")?>' />
					<?php echo form_error('user[enrollment_attributes][elementary]'); ?>
				</div>
				<div class="col-sm-2 mylabel">Date of Graduation *</div>
				<div class="col-sm-2 mylabel"><input label="Date of Graduation for Elementary" class="not_blank" id="elem_date" name="user[enrollment_attributes][elementary_date]" size="50" type="text" value='<?=set_value('user[enrollment_attributes][elementary_date]',isset($enroll) ? $enroll->elementary_date: "")?>' /><?php echo form_error('user[enrollment_attributes][elementary_date]'); ?></div>
			</div>
			
			<strong><span class="label label-default">Secondary</i></strong>
			<div class="row">
				<div class="col-sm-2 mylabel">Name of School *</div>
				<div class="col-sm-6 mylabel">
					<input placeHolder = "Name of High School" label = "Name of High School" class="not_blank" id="sec_name" name="user[enrollment_attributes][secondary]" size="50" type="text" value='<?=set_value('user[enrollment_attributes][secondary]',isset($enroll) ? $enroll->secondary: "")?>' />
					<?php echo form_error('user[enrollment_attributes][secondary]'); ?>
				</div>
				<div class="col-sm-2 mylabel">Date of Graduation *</div>
				<div class="col-sm-2 mylabel"><input label="Date of Graduation in High School" class="not_blank" id="sec_date" name="user[enrollment_attributes][secondary_date]" size="50" type="text" value='<?=set_value('user[enrollment_attributes][secondary_date]',isset($enroll) ? $enroll->secondary_date: "")?>' /><?php echo form_error('user[enrollment_attributes][secondary_date]'); ?></div>
			</div>
	
			<div id='message' title='Required Fields' style='display:none;'>
				<div id='message_list' class='alert alert-info'>
					Validating ... 
				</div>
			</div>
</fieldset>

<script>
	$('input[type="text"]').each(function(){
		$(this).addClass('tp').attr('title', $(this).attr("placeHolder"));
	})
</script>