<ul class="nav navbar-nav navbar-right">

  <li class="dropdown li_usertype">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-calendar"></i>&nbsp; : Open Semester <b class="caret"></b></a>
    <ul class="dropdown-menu li_account_menu">
      <li class="tp" data-toggle="tooltip" data-placement="left" title="System Current Academic Year / School Year & Semester" ><a href="#"><i class="fa fa-calendar"></i>&nbsp; SY : <?=isset($this->cos->summary)?$this->cos->summary->system:'No System Open Semester Set'?></a></li>
      <li class="tp" data-toggle="tooltip" data-placement="left" title="Your Current Academic Year / School Year & Semester" ><a href="#"><i class="fa fa-calendar-o"></i>&nbsp; U-SY : <?=isset($this->cos->summary)?$this->cos->summary->user:'No User Open Semester Set'?></a></li>
      <li class="tp" data-toggle="tooltip" data-placement="left" title="Current Grading Period" ><a href="#">GP : <?=isset($this->cos->grading_period)?$this->cos->grading_period->grading_period:'No Current Grading Period Set'?></a></li>
    </ul>
  </li>
</ul>