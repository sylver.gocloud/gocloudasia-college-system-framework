<?php
	#FIRST GROUP THE MENUS 
	$group_menus = array();

	if(isset($user_menus) && $user_menus){
		$grp_key = 0;
		foreach ($user_menus as $key => $men) {

			if(trim($men->controller) === "#"){ //this will become the Header Menu
				$grp_key = $key;
				$group_menus[$grp_key]['head'] = $men;
			}else{ //this are the sub-menus
				$group_menus[$grp_key]['sub'][] = $men;
			}
		}
	}
	// vd($this->syspar->is_menu_accordion);
?>

<!-- SLY POGI
	TWO TYPE OF MENUS 
	1. ACCORDION
	2. COLLAPSIBLE
-->

<?if($this->syspar->is_menu_accordion == 1):?>
<!-- ACCORDION TYPE OF MENU -->
<div id="MainMenu">
  <div class="list-group panel">
		
	<?php $men_ctr = 0;?>

  	<?foreach ($group_menus as $key => $value):?>
	<?php 
		$value = (object)$value;
	?>  	
	    <a href="#menu_<?=$key?>" class="list-group-item active-menus strong" data-toggle="collapse" data-parent="#MainMenu"> <?=$value->head->caption?> <i class="fa fa-caret-down float-right"></i></a>
	    <div class="collapse" id="menu_<?=$key?>">
	    	<?if($value->sub):?>
	    		<?foreach ($value->sub as $sk => $sub_menu):?>
	    			<?if($sub_menu->controller === "confirm_enrollees"
						|| $sub_menu->controller === "confirm_enrollees/lists"
						|| $sub_menu->controller === "search/search_enrollee"
						|| $sub_menu->controller === "search/search_student"
					):?>
					<?php
						$s_count = 0;
						switch ($sub_menu->controller) {
							case 'confirm_enrollees':
								$s_count = count_confirm_enrollees();
								break;
							case 'confirm_enrollees/lists':
								$s_count = count_list_confirm_enrollees();
								break;
							case 'search/search_enrollee':
								$s_count = count_enrollees();
								break;
							case 'search/search_student':
								$s_count = count_official_enrollees();
								break;
						}
					?>
						<a href="<?=site_url()?><?=$sub_menu->controller;?>" class="list-group-item menu_tp" data-toggle="tooltip" data-placement="right" title="<?=isset($this->syspar->show_menu_tip) && $this->syspar->show_menu_tip == 1 ? $sub_menu->remarks : ""?>"><i class="<?=$sub_menu->menu_icon?>"></i> &nbsp; <span class="badge bold" style="color:#48649f !important"><?=$s_count?></span> <?=$sub_menu->caption?> </a>
					<?else:?>
						<a href="<?=site_url()?><?=$sub_menu->controller;?>" class="list-group-item menu_tp" data-toggle="tooltip" data-placement="right" title="<?=isset($this->syspar->show_menu_tip) && $this->syspar->show_menu_tip == 1 ? $sub_menu->remarks : ""?>"><i class="<?=$sub_menu->menu_icon?>"></i> &nbsp; <?=$sub_menu->caption?> </a>
					<?endif;?>
	    		<?endforeach;?>
	    	<?endif;?>
	    </div>

	<?php $men_ctr++;?>
    <?endforeach;?>
  </div>
</div>
<?else:?>
<!-- COLLAPSIBLE TYPE OF MENU -->
<div id="MainMenu">
  <div class="list-group panel">
		
	<?php $men_ctr = 0;?>

  	<?foreach ($group_menus as $key => $value):?>
	<?php $value = (object)$value; ?>  	
	    <a href="javascript:;" onclick="collapsemenu(this)" menu_key = "<?=$key?>" colap = 0 class="list-group-item active-menus strong" data-parent="#MainMenu"> <?=$value->head->caption?> <i class="fa fa-caret-down float-right"></i></a>
	    <div id="menu_<?=$key?>">
	    	<?if($value->sub):?>
	    		<?foreach ($value->sub as $sk => $sub_menu):?>
	    			<?if($sub_menu->controller === "confirm_enrollees" 
	    			|| $sub_menu->controller === "confirm_enrollees/lists" 
	    			|| $sub_menu->controller === "search/search_enrollee"
	    			|| $sub_menu->controller === "search/search_student"
	    		):?>
					<?php
						$s_count = 0;
						switch ($sub_menu->controller) {
							case 'confirm_enrollees':
								$s_count = count_confirm_enrollees();
								break;
							case 'confirm_enrollees/lists':
								$s_count = count_list_confirm_enrollees();
								break;
							case 'search/search_enrollee':
								$s_count = count_enrollees();
								break;
							case 'search/search_student':
								$s_count = count_official_enrollees();
								break;
						}
					?>
						<a href="<?=site_url()?><?=$sub_menu->controller;?>" class="list-group-item menu_tp" data-toggle="tooltip" data-placement="right" title="<?=isset($this->syspar->show_menu_tip) && $this->syspar->show_menu_tip == 1 ? $sub_menu->remarks : ""?>"><i class="<?=$sub_menu->menu_icon?>"></i> &nbsp; <span class="badge bold" style="color:#48649f !important"><?=$s_count?></span> <?=$sub_menu->caption?> </a>
					<?else:?>
						<a href="<?=site_url()?><?=$sub_menu->controller;?>" class="list-group-item menu_tp" data-toggle="tooltip" data-placement="right" title="<?=isset($this->syspar->show_menu_tip) && $this->syspar->show_menu_tip == 1 ? $sub_menu->remarks : ""?>"><i class="<?=$sub_menu->menu_icon?>"></i> &nbsp; <?=$sub_menu->caption?> </a>
					<?endif;?>
	    		<?endforeach;?>
	    	<?endif;?>
	    </div>

	<?php $men_ctr++;?>
    <?endforeach;?>
  </div>
</div>
<?endif;?>

<style>
	/*Change the size here*/
	div.tooltip-inner {
	    max-width: 350px;
	}
</style>

<!-- MY SOURCE :D http://www.bootply.com/Wa6eNctyrN -->