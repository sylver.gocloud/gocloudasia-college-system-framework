<ul class="nav" id="side-menu">
    <li>
        <a href="<?=site_url('gopanel')?>"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
    </li>

    <li>
        <a href="<?=site_url('packages/set')?>" class=""><span class="entypo-dropbox"></span>&nbsp; Packages</a>
    </li>

    <li>
        <a href="#"><i class="fa fa-cog"></i>&nbsp; Settings<span class="fa arrow"></span></a>
        <ul class="nav nav-second-level">
            <li>
                <a href="<?=site_url('gopanel/school_settings')?>"><span class="entypo-globe"></span>&nbsp; School Settings</a>
                <a href="<?=site_url('gopanel/system_parameters')?>"><span class="entypo-pinterest"></span>&nbsp; System Parameters</a>
                <a href="<?=site_url('departments_cpanel')?>"><i class="fa fa-users"></i>&nbsp; Departments</a>
            </li>
        </ul>
    </li>
    
    <li>
        <a href="#"><i class="fa fa-list fa-fw"></i> Master Files<span class="fa arrow"></span></a>
        <ul class="nav nav-second-level">
            <li>
                <a href="<?=site_url('gopanel/system_users')?>"><i class="fa fa-users"></i>&nbsp; System Users</a>
                <a href="<?=site_url('gopanel/users')?>"><i class="fa fa-users"></i>&nbsp; Department Users</a>
                <a href="<?=site_url('menu_creator')?>">Menu Creator</a>
                <a href="<?=site_url('packages')?>"><span class="iconicfill-box"></span>&nbsp; Package Creator</a>
            </li>
        </ul>
        <!-- /.nav-second-level -->
    </li>

    <li>
        <a href="#"><span class="entypo-tools"></span>&nbsp; Tools <span class="fa arrow"></span></a>
        <ul class="nav nav-second-level">
            <li>
                <a href="<?=site_url('gopanel/kabit')?>"><span class="entypo-flow-tree"></span>&nbsp; Database Connection</a>
                <a href="<?=site_url('gopanel/kwery')?>"><span class="entypo-qq"></span>&nbsp; Kwery</a>
                <a href="<?=site_url('gopanel/migration')?>"><span class="entypo-rocket"></span>&nbsp; Migration</a>
                <a href="<?=site_url('gopanel/adminer')?>"><span class="entypo-database"></span>&nbsp; Adminer</a>
                <a href="<?=site_url('gopanel/backup')?>"><span class="entypo-database"></span>&nbsp; Backup Database</a>
                <a href="<?=site_url('gopanel/system_patches')?>" class=""><span class="entypo-leaf"></span>&nbsp; System Patches</a>
            </li>
        </ul>
        <!-- /.nav-second-level -->
    </li>

    <li>
        <a href="#">Utilities <span class="fa arrow"></span></a>
        <ul class="nav nav-second-level">
            <li>
                <a href="<?=site_url('gopanel/activity_log')?>"><span class="entypo-chart-line"></span>&nbsp; User Activity Log</a>
                <a href="<?=site_url('gopanel/encrypt_util')?>"><span class="entypo-eye"></span>&nbsp; Encrypt/Decrypt</a>
            </li>
        </ul>
        <!-- /.nav-second-level -->
    </li>
    <!-- <li>
        <a href="#"><i class="fa fa-sitemap fa-fw"></i> Multi-Level Dropdown<span class="fa arrow"></span></a>
        <ul class="nav nav-second-level">
            <li>
                <a href="#">Second Level Item</a>
            </li>
            <li>
                <a href="#">Second Level Item</a>
            </li>
            <li>
                <a href="#">Third Level <span class="fa arrow"></span></a>
                <ul class="nav nav-third-level">
                    <li>
                        <a href="#">Third Level Item</a>
                    </li>
                    <li>
                        <a href="#">Third Level Item</a>
                    </li>
                    <li>
                        <a href="#">Third Level Item</a>
                    </li>
                    <li>
                        <a href="#">Third Level Item</a>
                    </li>
                </ul>
            </li>
        </ul>
    </li> -->
</ul>