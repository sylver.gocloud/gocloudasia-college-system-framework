<style>
	.tab-content {
    border-left: 1px solid #ccc !important;
    border-right: 1px solid #ccc !important;
    border-bottom: 1px solid #ccc !important;
    padding: 10px !important;
	}

	.nav-tabs {
	    margin-bottom: 0px !important;
	}
</style>
<?php
		$search = isset($search_type)?'/'.$search_type:'';
		$search_param = array(
				'type' => isset($search_type)?$search_type:'all',
			);

		@$this->load->view('layouts/search_student', $search_param);?>

<? @$this->load->view('fees/student_profile_head') ?>

<ul class="nav nav-tabs">
	<?if($mydepartment->stud_profile == 1):?>
	<li <?= ($this->router->class == "profile" && $this->router->method == 'view') ? "class='active'" : '' ?>>
	  <a href="<?php echo base_url('profile/view/'.$student->id.$search); ?>"><span class="entypo entypo-user"></span>&nbsp; Student Profile</a></li>
	<?endif;?>

	<?if($mydepartment->stud_subject == 1):?>
	<li <?= ($this->router->class == "fees" && $this->router->method == 'add_subject') ? "class='active'" : '' ?>>
	  <a href="<?php echo base_url('fees/add_subject/'.$student->id.'/0'.$search); ?>"><span class="entypo entypo-newspaper"></span>&nbsp; Student Schedule</a></li>
	<?endif;?>
	
	<?if($mydepartment->stud_grade == 1):?>
	<li <?= ($this->router->class == "grade" && $this->router->method == 'view') ? "class='active'" : '' ?>>
	  <a href="<?php echo base_url('grade/view/'.$student->id.$search); ?>"><i class="glyphicon glyphicon-star-empty"></i>&nbsp; Student Grade</a></li>
	<?endif;?>
	
	<?if($mydepartment->stud_fees == 1):?>
	<li <?= ($this->router->class == "fees" && $this->router->method == 'view_fees') ? "class='active'" : '' ?>>
	  <a href="<?php echo base_url('fees/view_fees/'.$student->id.$search); ?>"><i class="fa fa-money"></i>&nbsp; Student Fees</a></li>
	<?endif;?>
	  
	<?if($mydepartment->stud_issues == 1):?>
	<li <?= ($this->router->class == "issues" && $this->router->method == 'view') ? "class='active'" : '' ?>>
	  <a href="<?php echo base_url('issues/view/'.$student->id.$search); ?>"><span class="glyphicon glyphicon-info-sign"></span>&nbsp; Issues <span class="badge"><?= $total_issues;?></span></a></li>
	<?endif;?>
	  
	<?if($mydepartment->stud_otr == 1):?>
	<li <?= ($this->router->class == "transcript" && $this->router->method == 'view') ? "class='active'" : '' ?>>
	  <a href="<?php echo base_url('transcript/view/'.$student->id.$search); ?>">Transcript</a></li>
	<li >
	<?endif;?>
	
	<!--ONLY REGISTRAR CAN Delete/Dropped STUDENT PROFILE-->
	<?if(strtolower($this->session->userdata['userType']) == "registrar"):?>
	  <a  class='confirm btn btn-danger' title="Are you sure you want to drop/delete this student?" href="<?=base_url()?>registrar/dropped_student/<?=$student->id.$search?>"><i class="fa fa-trash-o"></i>&nbsp;	 Delete</a></li>
	 <?endif;?>
</ul>
