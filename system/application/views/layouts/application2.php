<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
		<link REL="SHORTCUT ICON" HREF="<?=base_url('assets/images/favicon.ico');?>">
	    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	    <title><?php echo $this->setting->school_name; ?><?=$this->router->class!= "" ? " | ".ucwords(str_replace('_', ' ', $this->router->class)) : ''?></title>
	    <?php $xbootstrap = $this->syspar->bootstrap_theme ? strtolower($this->syspar->bootstrap_theme) : 'default';?>
	    <link href="<?php echo base_url('assets/css/bootstrap.'.$xbootstrap.'.min.css'); ?>" rel="stylesheet" type="text/css" media="screen" />
	    <link href="<?php echo base_url('assets/css/glyphicons.css'); ?>" rel="stylesheet" type="text/css" />
	    <link href="<?php echo base_url('assets/css/font-awesome-4.0.3/css/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css" media="screen" />
	    <link href="<?php echo base_url('assets/css/entypo/entypo.css'); ?>" rel="stylesheet" type="text/css" />
	    <link href="<?php echo base_url('assets/css/iconicfill/iconicfill.css'); ?>" rel="stylesheet" type="text/css" />
	    <link href="<?php echo base_url('assets/css/pace.css'); ?>" rel="stylesheet" type="text/css" />
	    <link href="<?php echo base_url('assets/css/custom-theme/jquery-ui-1.8.18.custom.css'); ?>" rel="stylesheet" type="text/css" media="all" />
	    <link href="<?php echo base_url('assets/js/jnotify/jNotify.jquery.css'); ?>" rel="stylesheet" type="text/css" media="all" />
	    <link href="<?php echo base_url('assets/css/animate.css'); ?>" rel="stylesheet" type="text/css" />
	    <link href="<?php echo base_url('assets/css/overwrite.css'); ?>" rel="stylesheet" type="text/css" />
	    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-1.11.2.min.js"></script>
    	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-migrate-1.1.1.min.js"></script>
		 	<!-- <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-1.7.1.min.js"></script> -->
	    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-ui-1.8.18.custom.min.js"></script>
	    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.number.min.js"></script>
	    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jnotify/jNotify.jquery.min.js"></script>

			<script type='text/javascript'>
				$(document).ready(function() {      
					var base_url = "<?=site_url()?>";
					check_unread_message(base_url);
					/*setInterval(function(){
						  check_unread_message(base_url);
					},5000);*/

					<?if(isset($system_message) && $system_message):?>
						<?$xmsg = str_replace('"',"'",$system_message);?>
						<?$xmsg = str_replace('\r\n'," ",$xmsg);?>
						<?$xmsg = str_replace('\n'," ",$xmsg);?>
						<?$xmsg = str_replace('\r'," ",$xmsg);?>
						try{
						  	growl('<p>System Message</p>',"<?=$xmsg?>");
						}
						catch(err){
						  //Handle errors here
						}
					<?else:?>
					<?endif;?>		
				});
			
				function check_unread_message(base_url)
				{
					var userid = "<?=$this->session->userdata['userid'];?>";
					if(userid == null || userid == "") 
					{ return false; }
											
					var controller = 'ajax_message';
					
					$.ajax({
						'url' : base_url + '' + controller + '/count_all_unread_message',
						'type' : 'POST', 
						'async': false,
						'data' : {},
						'dataType' : 'json',
						'success' : function(data){ 
							
							var old = $('#menu_unread_badge').text().trim();
							
							if(data.unread != old)
							{
								$('#menu_unread_badge').text(data.unread);
							}
						}
					})
				}
	    </script>

    </head>
    <body>	
   <?php $this->load->view('layouts/_alert'); ?>
  <!-- Wrap all page content here -->
    <div class="wrap">
		
		<div class="row"> <!-- ROW FOR HEADER/LOGO -->

			<div class="btn btn-default btn-sm hidden-xs dock-button tp" hide = "<?=$this->syspar->show_menu?>" id = "dock-button" data-toggle="tooltip" data-placement="top" title="<?=$this->syspar->show_menu == 1 ? 'Hide Menus' : 'Show Menus'?>" base_url="<?=site_url();?>" ><i class="<?=$this->syspar->show_menu == 1 ? 'fa fa-chevron-left' : 'fa fa-chevron-right'?> dock-fa-icon"></i>&nbsp;</div>
			<div class="btn btn-default btn-sm hidden-xs colap-button tp" hide = "<?=$this->syspar->is_menu_accordion?>" id = "colap-button" data-toggle="tooltip" data-placement="top" title="<?=$this->syspar->is_menu_accordion == 1 ? 'Collapsible Menus' : 'Accordion Menus'?>" base_url="<?=site_url('');?>" ><i class="fa <?=$this->syspar->is_menu_accordion == 1 ? 'fa-chevron-down' : 'fa-chevron-up'?> dock-fa-icon"></i>&nbsp;</div>

    	<!-- LOGO WRAPPER FOR DESKTOP -->
	    	<div class="hidden-xs black-nav system-nav-logo">
	    		<div class="col-md-4">
	    			<a class="navbar-brand thumbnails" href="#"><img class="school_logo" src="<?=site_url($this->setting->logo)?>"/></a>
	    		</div>
	    		<div class="col-md-8">
	    			<div class="navbar account_container2 hidden-xs top-menus">
			      		<?@$this->load->view('layouts/_top_account_menu')?>
				   	</div>
	    		</div>
	    	</div>
      <!-- END OF DESKTOP -->

      <!-- FOR MOBILE -->
	      <div class="row visible-xs" style="background-color:#333333 !important;">
	    		<div class="col-md-12">
	    			<h4><span class="white-font school_name">&nbsp;&nbsp;&nbsp;<?=$this->setting->school_name?></span></h4>
	    		</div>
	    	</div>

	    	<div class="row visible-xs top-menus">
	    		<div class="col-md-12">
	    			<?@$this->load->view('layouts/_mobile_menus')?>
	    		</div>
	    	</div>
			<!-- END OF MOBILE -->

		</div>

		<div class="row"> <!-- ROW FOR CONTENT -->
      
      <!-- Begin page content -->
      <div class="center_container">

      	<?php  /**  GLOBAL MESSAGE - LOOP ARRAY */?>
	        <?if(isset($global_message) && is_array($global_message) && $global_message ): foreach($global_message as $gmsg): ?>
		        		<div class="alert alert-info"><strong style="font-size:11pt" ><span class="entypo-lamp"></span>&nbsp; <?=$gmsg;?></strong></div>
		      <?endforeach; endif;?>
      
  			<!-- FOR DESKTOP -->
        <div class="col-md-2 col-sm-2 hidden-xs menu-container" style="<?=$this->syspar->show_menu == 0 ? 'display:none !important;' : ''?>" >

	        <?if(!$disable_menus):?>
	        	<?@$this->load->view('layouts/_menus');?>
	        <?endif;?>
					<?php $this->load->view("layouts/_account_menu"); ?>		
				</div>
        
        <div class="<?=$this->syspar->show_menu == 1 ? 'col-md-10 col-sm-7 col-xs-5' : 'col-md-12 col-sm-9 col-xs-6'?> yield-container">
		        
			    <!-- PAGE NAME OR PAGE HEADER -->
	          <div class="page-header">
	          	<div class="row">
		            	<div class="col-md-11 col-xs-11">
				            <h3><?= ucwords(str_replace('_', ' ', $this->router->class)) ?>
											<?if(isset($custom_title) && $custom_title):?>
												<small><?=$custom_title;?></small>
											<?else:?>
												<small><?= (ucwords(str_replace('_', ' ', $this->router->method)) == "Index") ? "" : ucwords(str_replace('_', ' ', $this->router->method)) ?></small>
											<?endif;?>
										</h3>
									</div>

									<?if(isset($help) && $help):?>
										<div class="col-md-1 col-sm-1 text-right">
											<a class="tp" href="javascript:;" data-toggle="tooltip" data-placement="top" title="<?=$help?>"><i class="fa fa-question fa-2x"></i></a>
										</div>
									<?endif;?>
							</div>
	          </div>
							
					<!-- SYSTEM MESSAGES AND VALIDATION ERRORS -->
	            <?if(isset($system_message))echo $system_message; echo validation_errors('<div class="alert alert-danger">', '</div>');?>
							
					<!-- OUTPUT -->
					<div class="">
            	<?php echo $yield; ?>
          </div>
				</div>

      </div>

      </div>

    </div>

    <div id="footer">
      <div class="container">
        <p class="text-muted credit">&copy; Copyright Gocloud Asia 2013</p>
      </div>
    </div>
    
  
		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!-- Include all compiled plugins (below), or include individual files as needed -->
   
    <link href="<?php echo base_url('assets/css/bootstrap-switch.css'); ?>" rel="stylesheet" type="text/css" />
     <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/pace.js"></script>
     <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
     <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/custom.js"></script>
     <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap-switch.js"></script>
     <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap-growl.min.js"></script>
     <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.tablesorter.min.js"></script>
     <!-- <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.timepicker.min.js"></script> -->
     <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/myjs.js"></script>
     <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/blockui.js"></script>
		
		 <!-- Jquery Form Validation PLugin -->
     <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.validate.min.js"></script> 
     <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/additional-methods.min.js"></script> 

     <?/** REF for this plugin : https://github.com/nakupanda/bootstrap3-dialog */?>
     <link href="<?php echo base_url('assets/bootstrap_dialog/css/bootstrap-dialog.min.css'); ?>" rel="stylesheet" type="text/css" media="screen" />
     <script type="text/javascript" src="<?php echo base_url(); ?>assets/bootstrap_dialog/js/bootstrap-dialog.min.js"></script>
	
   <!-- General Modal For Alert -->
	<div class="modal fade" id="alertModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h4 class="modal-title" id="alertModal_Label">Alert</h4>
		  </div>
		  <div class="modal-body" id="alertModal_Body">
			...
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
		  </div>
		</div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
	 <!-- End of alert modal -->
	
    <script>
      $('.radio1').on('switch-change', function () {
        $('.radio1').bootstrapSwitch('toggleRadioStateAllowUncheck', true);
      });
    </script>
	
	<!--PLEASE WAIT-->
	<div id='please_wait' style='display:none;'>
		<img src="<?=base_url()?>assets/images/loading.gif" ></img>
		&nbsp;
		Just a moment please.
	</div>
	
    </body>
</html>
