<ul class="nav navbar-nav navbar-right autodown">
  
  <?if($this->router->class !== "home"):?>
  <li class='' ><a href="<?=site_url('home');?>" class="tp" data-toggle="tooltip" data-placement="bottom" title="Go to Home" ><i class="glyphicon glyphicon-home"></i>&nbsp;</a></li>
  <?endif;?>

  <li class="dropdown">
    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
        <span class="hidden-xs" ><i class="fa fa-calendar fa-fw"></i>&nbsp; <?=isset($this->cos->summary)?$this->cos->summary->user:'No System Open Semester Set'?>  <i class="fa fa-caret-down"></i></span>
        <span class="visible-xs" ><i class="fa fa-calendar fa-fw"></i>&nbsp; <i class="fa fa-caret-down"></i></span>
    </a>
    <ul class="dropdown-menu dropdown-alerts">
        <li>
            <a href="#">
                <div>
                    <span class="text-info entypo-record"></span> <?php echo isset($this->cos->user->semester)?$this->cos->user->semester:'No Open Semester'; ?>
                    <span class="pull-right text-muted small">My Open Semester</span>
                </div>
            </a>
        </li>
        <li>
            <a href="#">
                <div>
                    <span class="text-info entypo-record"></span> <?php echo isset($this->cos->user->year_from)?$this->cos->user->year_from:'No School Year Set'; ?>-<?php echo isset($this->cos->system->year_to)?$this->cos->system->year_to:''; ?>
                    <span class="pull-right text-muted small">My Open School Year</span>
                </div>
            </a>
        </li>
        <li class="divider"></li>
        <li>
            <a href="#">
                <div>
                    <span class="text-success entypo-record"></span> <?=isset($this->cos->grading_period)?$this->cos->grading_period->grading_period:'No Current Grading Period Set'?>
                    <span class="pull-right text-muted small">System Open Grading Period</span>
                </div>
            </a>
        </li>
        <li>
            <a href="#">
                <div>
                    <span class="text-success entypo-record"></span> <?php echo isset($this->cos->system->semester)?$this->cos->system->semester:'No Open Semester'; ?>
                    <span class="pull-right text-muted small">System Open Semester</span>
                </div>
            </a>
        </li>
        <li>
            <a href="#">
                <div>
                    <span class="text-success entypo-record"></span> <?php echo isset($this->cos->system->year_from)?$this->cos->system->year_from:'No School Year Set'; ?>-<?php echo isset($this->cos->system->year_to)?$this->cos->system->year_to:''; ?>
                    <span class="pull-right text-muted small">System Open School Year</span>
                </div>
            </a>
        </li>
        <li class="divider"></li>
        <li>
            <a class="text-center" href="<?php echo site_url('open_semester_employee_settings') ?>">
                <strong>Change My Open Semester</strong>
                <i class="fa fa-angle-right"></i>
            </a>
        </li>
    </ul>
    <!-- /.dropdown-alerts -->
  </li>
  
  <!-- <li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-calendar"></i>&nbsp;<?=isset($this->cos->summary)?$this->cos->summary->user:'No System Open Semester Set'?></a>
    <ul class="dropdown-menu li_account_menu">
      <li class="tp" data-toggle="tooltip" data-placement="left" title="Current Grading Period" ><a href="#">GP : <?=isset($this->cos->grading_period)?$this->cos->grading_period->grading_period:'No Current Grading Period Set'?></a></li>
    </ul>
  </li> -->
  
  <li class="dropdown">
    <a href="#" class="tp" data-toggle="tooltip" data-placement="bottom" title="User Account Type">
      <i class="fa fa-user"></i>&nbsp; <?=capital_fwords($this->session->userdata['userType'])?>  
    </a>
  </li>

  <li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?=capital_fwords($this->session->userdata['username']);?> <i class="fa fa-chevron-down"></i></a>
    <ul class="dropdown-menu li_account_menu">
      <li><a href="<?=base_url()?>home"  class=""><i class="fa fa-home"></i>&nbsp;Home</a></li>
      <li><a href="<?=base_url()?>messages/inbox"  class=""><i class="fa fa-inbox"></i>&nbsp; Messages<span class='badge' id='menu_unread_badge' ></span></a></li>
      <li><a href="<?php echo base_url(); ?>news_and_events"  class="">Bulletin Board</a></li>
      <li><a href="<?=base_url()?>change_password/index/<?php echo $this->session->userdata('userid'); ?>"><i class="fa fa-lock"></i>&nbsp;Change Password</a></li>
      <li class="divider"></li>
      <li><a href="<?php echo base_url(); ?>auth/logout"><i class="fa fa-power-off"></i>&nbsp; Logout</a></li>
    </ul>
  </li>
</ul>