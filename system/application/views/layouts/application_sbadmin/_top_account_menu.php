<!-- HOME LINK -->
<?if($this->router->class !== "home"):?>
  <li class='' ><a href="<?=site_url('home');?>" class="tp" data-toggle="tooltip" data-placement="bottom" title="Go to Home" ><i class="glyphicon glyphicon-home text-success"></i>&nbsp;</a></li>
<?endif;?>

<?if($this->isadmin === TRUE):?>
  <li class="dropdown">
      <a class="dropdown-toggle" data-toggle="dropdown" href="#">
          <i class="fa fa-bell fa-fw <?php echo (isset($bar_notification) && $bar_notification) ? "text-warning" : '' ?>"></i>  <i class="fa fa-caret-down"></i>
      </a>
      <ul class="dropdown-menu dropdown-messages">
          <?php if (isset($bar_notification) && $bar_notification): ?>
            <?php $bar_notification = array_reverse($bar_notification, true); ?>
            <?php foreach ($bar_notification as $k => $v): $v = (object)$v;?>
              <li>
                <a href="<?php echo $v->url ? $v->url : "#" ?>">
                    <div>
                        <strong><u><?php echo $v->title ?></u></strong>
                        <span class="pull-right text-muted">
                            <em><?php echo $v->muted ?></em>
                        </span>
                    </div>
                    <div><small><?php echo $v->msg; ?></small></div>
                </a>
              </li>
              <li class="divider"></li>
            <?php endforeach ?>
          <?php else: ?>
            <li>
                <a class="text-center" href="#">
                    <strong>No alert at this moment.</strong>
                </a>
            </li>
          <?php endif ?>
      </ul>
      <!-- /.dropdown-alerts -->
  </li>
<?endif;?>

<!-- CALENDAR LINK -->
<li class="dropdown">
  <a class="dropdown-toggle" data-toggle="dropdown" href="#">
      <span class="hidden-xs" ><i class="fa fa-calendar fa-fw"></i>&nbsp; <?=isset($this->cos->summary)?$this->cos->summary->user:'No System Open Semester Set'?>  <i class="fa fa-caret-down"></i></span>
      <span class="visible-xs" ><i class="fa fa-calendar fa-fw"></i>&nbsp; <i class="fa fa-caret-down"></i></span>
  </a>
  <ul class="dropdown-menu dropdown-messages">
      <li>
          <a href="#">
              <div>
                  <span class="text-info entypo-record"></span> <?php echo isset($this->cos->user->semester)?$this->cos->user->semester:'No Open Semester'; ?>
                  <span class="pull-right text-muted small">My Open Semester</span>
              </div>
          </a>
      </li>
      <li>
          <a href="#">
              <div>
                  <span class="text-info entypo-record"></span> <?php echo isset($this->cos->user->year_from)?$this->cos->user->year_from:'No School Year Set'; ?>-<?php echo isset($this->cos->system->year_to)?$this->cos->system->year_to:''; ?>
                  <span class="pull-right text-muted small">My Open School Year</span>
              </div>
          </a>
      </li>
      <li class="divider"></li>
      <li>
          <a href="#">
              <div>
                  <span class="text-success entypo-record"></span> <?=isset($this->cos->grading_period)?$this->cos->grading_period->grading_period:'No Current Grading Period Set'?>
                  <span class="pull-right text-muted small">System Open Grading Period</span>
              </div>
          </a>
      </li>
      <li>
          <a href="#">
              <div>
                  <span class="text-success entypo-record"></span> <?php echo isset($this->cos->system->semester)?$this->cos->system->semester:'No Open Semester'; ?>
                  <span class="pull-right text-muted small">System Open Semester</span>
              </div>
          </a>
      </li>
      <li>
          <a href="#">
              <div>
                  <span class="text-success entypo-record"></span> <?php echo isset($this->cos->system->year_from)?$this->cos->system->year_from:'No School Year Set'; ?>-<?php echo isset($this->cos->system->year_to)?$this->cos->system->year_to:''; ?>
                  <span class="pull-right text-muted small">System Open School Year</span>
              </div>
          </a>
      </li>
      <li class="divider"></li>
      <li>
          <a class="text-center" href="<?php echo site_url('open_semester_employee_settings') ?>">
              <strong>Change My Open Semester</strong>
              <i class="fa fa-angle-right"></i>
          </a>
      </li>
  </ul>
  <!-- /.dropdown-alerts -->
</li>

<!-- ACCOUNT LINKG -->
<li class="dropdown autodown">
    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
        <i class="fa fa-user fa-fw"></i>
        <?=capital_fwords(ellipsis($this->session->userdata['username'],12));?> 
        &nbsp; (<?=capital_fwords($this->session->userdata['userType'])?>)  <i class="fa fa-caret-down"></i>
    </a>
    <ul class="dropdown-menu dropdown-user">
        <li><a href="#"><span class='text-success bold'><i class="fa fa-user"></i>&nbsp; <?=capital_fwords(strtolower($this->session->userdata['username']));?></span></a></li>
        <li class="divider"></li>
        <li><a href="<?=base_url()?>messages/inbox"  class=""><i class="fa fa-fw fa-inbox"></i>&nbsp; Messages<span class='badge' id='menu_unread_badge' ></span></a></li>
        <li><a href="<?php echo base_url(); ?>news_and_events"class=""><i class="fa fa-paperclip"></i></i>&nbsp;News & Events</a></li>
        <li><a href="<?=base_url()?>change_password/index/<?php echo $this->session->userdata('userid'); ?>"><i class="fa fa-fw fa-lock"></i>&nbsp;Change Password</a></li>
        <li class="divider"></li>
        <li><a href="<?php echo base_url(); ?>auth/logout"><i class="fa fa-fw fa-power-off"></i>&nbsp; Logout</a></li>
    </ul>
    <!-- /.dropdown-user -->
</li>