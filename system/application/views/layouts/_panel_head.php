<?php 
	$panel_title = isset($panel_title)?$panel_title:'Title'; 
	$panel_name = isset($panel_name)?$panel_name:'panel_'.rand(); 
?>

<div class="panel panel-default">
  <div class="panel-heading" style="padding:0px 0px 0px 10px !important; background-color:#222222 !important;" >
    <div class="row" style="height: 100% !important;">
      <div class="col-md-3" style="padding-top:5px !important;">
        <strong class="white-font"><?=$panel_title;?></strong>
      </div>

      <div class="col-md-9">
        <div style="float:right;" >
        	<div class="btn-group">
		          <?php $panel_btn = isset($panel_btn)?$panel_btn:false;?>
		          <?if($panel_btn):?>
		            <?foreach ($panel_btn as $key => $buto):?>
		              <?
		                $panel_btn_class = isset($buto['class'])?$buto['class']:'';
                    $panel_btn_attr = isset($buto['attr'])?$buto['attr']:'';
		                $panel_btn_url = isset($buto['url'])?$buto['url']:'';
		                $panel_btn_tp = isset($buto['tooltip'])?$buto['tooltip']:'Click Me';
		                $panel_btn_title = isset($buto['title'])?$buto['title']:'BTN ME';
		              ?>
		              <a style="margin-right 0px !important;" class="btn btn-xs btn-default tp xtooltip <?=$panel_btn_class?>" href="<?=$panel_btn_url?>" <?=$panel_btn_attr?> data-toggle="tooltip" data-placement="top" title="<?=$panel_btn_tp?>" ><?=$panel_btn_title;?></a>
		            <?endforeach;?>
		          <?endif;?>

          		<a class="btn btn-xs btn-default facebook_color xtooltip"  href="javascript:;" onclick="hide_sylver_panel(this,'<?=$panel_name?>')" id="sylver_i_hide-<?=$panel_name?>" data-toggle="tooltip" data-placement="top" title="Hide Panel" ><i class="glyphicon glyphicon-chevron-down"></i></a>
          		<a class="btn btn-xs btn-default facebook_color xtooltip" style="display:none;"  href="javascript:;" onclick="show_sylver_panel(this,'<?=$panel_name?>')" id="sylver_i_show-<?=$panel_name?>" data-toggle="tooltip" data-placement="top" title="Show Panel" ><i class="glyphicon glyphicon-chevron-up"></i></a>
          	</div>
        </div>
      </div>

    </div>

  </div>
  <script type="text/javascript">
  	function hide_sylver_panel (el,panel_name) {
  		$('#sylver-panel-'+panel_name).hide('slow');
  		$(el).hide();
  		$('#sylver_i_show-'+panel_name).show();
  	}

  	function show_sylver_panel (el,panel_name) {
  		$('#sylver-panel-'+panel_name).show('slow');
  		$(el).hide();
  		$('#sylver_i_hide-'+panel_name).show();
  	}
  </script>
  <div class="panel-body" id="sylver-panel-<?=$panel_name?>">