<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>

    <link REL="SHORTCUT ICON" HREF="<?=base_url('assets/images/favicon.ico');?>">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="gocloudasia">

    <title><?php echo $this->setting->school_name; ?><?=$this->router->class!= "" ? " | ".ucwords(str_replace('_', ' ', $this->router->class)) : ''?></title>

    <!-- Bootstrap Core CSS -->
    <?php $xbootstrap = $this->syspar->bootstrap_theme ? strtolower($this->syspar->bootstrap_theme) : 'default';?>
    <link href="<?php echo base_url('assets/css/bootstrap.'.$xbootstrap.'.min.css'); ?>" rel="stylesheet" type="text/css" media="screen" />

    <!-- Custom Fonts -->
    <link href="<?php echo base_url('assets/css/glyphicons.css'); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/css/font-awesome-4.0.3/css/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css" media="screen" />
    <link href="<?php echo base_url('assets/css/entypo/entypo.css'); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/css/iconicfill/iconicfill.css'); ?>" rel="stylesheet" type="text/css" />

    <!-- CUSTOM AND PLUGIN CSS -->

    <link href="<?=site_url('assets/gopanel/css/metisMenu.min.css')?>" rel="stylesheet">
    <link href="<?=site_url('assets/gopanel/css/sb-admin-2.css')?>" rel="stylesheet">

    <link href="<?php echo base_url('assets/css/custom-theme/jquery-ui-1.8.18.custom.css'); ?>" rel="stylesheet" type="text/css" media="all" />
    <link href="<?php echo base_url('assets/js/jnotify/jNotify.jquery.css'); ?>" rel="stylesheet" type="text/css" media="all" />
    <link href="<?php echo base_url('assets/css/animate.css'); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/css/overwrite.css'); ?>" rel="stylesheet" type="text/css" />
    

    <!-- JQUERY Script -->
    
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-migrate-1.1.1.min.js"></script>

    <!-- <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-1.7.1.min.js"></script> -->
    <!-- <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-ui-1.8.18.custom.min.js"></script> -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-ui.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.number.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jnotify/jNotify.jquery.min.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script type='text/javascript'>
        $(document).ready(function() {      
            var base_url = "<?=site_url()?>";
            check_unread_message(base_url);
            setInterval(function(){
                //check_unread_message(base_url);
            },50000);

            <?if(isset($system_message) && $system_message):?>
                <?$xmsg = str_replace('"',"'",$system_message);?>
                <?$xmsg = str_replace('\r\n'," ",$xmsg);?>
                <?$xmsg = str_replace('\n'," ",$xmsg);?>
                <?$xmsg = str_replace('\r'," ",$xmsg);?>
                try{
                    growl('<p>System Message</p>',"<?=$xmsg?>");
                }
                catch(err){
                  //Handle errors here
                }
            <?else:?>
            <?endif;?>      
        });
        
        function check_unread_message(base_url)
        {
            var userid = "<?=$this->session->userdata['userid'];?>";
            if(userid == null || userid == "") 
            { return false; }
                                    
            var controller = 'ajax_message';
            
            $.ajax({
                'url' : base_url + '' + controller + '/count_all_unread_message',
                'type' : 'POST', 
                'async': true,
                'data' : {},
                'dataType' : 'json',
                'success' : function(data){ 
                    $('.menu_unread_badge').text(data.unread);
                }
            })
        }
    </script>
</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#"><?echo $this->setting->school_name; ?></a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <?php echo @$this->load->view('layouts/application_sbadmin/_top_account_menu') ?>
            </ul>
            <!-- /.navbar-top-links -->
            
            <div class="navbar-default sidebar sidebar-desktop" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <!-- <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </li> -->
                        <li>
                            <a href="<?php echo site_url('home') ?>" class="bold" ><i class="fa fa-home fa-fw"></i> Home</a>
                        </li>
                        <?php echo @$this->load->view('layouts/application_sbadmin/_sbmenus') ?>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            <?if(isset($page_title) && $page_title):?>
                                <?= $page_title ?>
                            <?else:?>
                                <?= ucwords(str_replace('_', ' ', $this->router->class)) ?>
                            <?endif;?>
                            
                            <?if(isset($custom_title) && $custom_title):?>
                                <small><?=$custom_title;?></small>
                            <?else:?>
                                <small><?= (ucwords(str_replace('_', ' ', $this->router->method)) == "Index") ? "" : ucwords(str_replace('_', ' ', $this->router->method)) ?></small>
                            <?endif;?>
                        </h1>

                        <!-- Global Message -->
                        <?php  /**  GLOBAL MESSAGE - LOOP ARRAY */?>
                        <?if(isset($global_message) && is_array($global_message) && $global_message ): foreach($global_message as $gmsg): ?>
                                    <div class="alert alert-info"><strong style="font-size:11pt" ><span class="entypo-lamp"></span>&nbsp; <?=$gmsg;?></strong></div>
                        <?endforeach; endif;?>

                        <?if(isset($system_message))echo $system_message; echo validation_errors('<div class="alert alert-danger">', '</div>');?>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->

                <!-- Yield Content -->
                <div class="row">
                    <div class="col-lg-12">
                        <?php echo $yield; ?>
                    </div>
                </div>

                <!-- Footer -->
                <br />
                <br />
                <div id="footer">
                    <p class="text-muted">
                        <a href="#"><span class="label label-info">&copy; Copyright Gocloud Asia 2013</span></a> 
                        | <i class="icon-comment"></i> <a href="#">Time Exec: <?=isset($elapsed_time) ? $elapsed_time: '0.0';?></a>
                        | <i class="icon-share"></i> <a href="#">Memory: <?=isset($memory_usage) ? $memory_usage : '0.0';?></a>
                    </p>
                </div>
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Hidden Forms -->
    <input type="hidden" name="baseurl" id="baseurl" value="<?php echo site_url() ?>" />
    
    <span id="sidebarhider" class="sidebarhider"><a href="#" id="hider" value="<?php echo $this->syspar->show_menu ?>" ><?=$this->syspar->show_menu == 1 ? "Hide Sidebar" : "Show Sidebar"?></a></span>

    <?php @$this->load->view('layouts/_alert'); ?>

    <link href="<?php echo base_url('assets/css/bootstrap-switch.css'); ?>" rel="stylesheet" type="text/css" />

    <!-- Bootstrap JS -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>

    <!-- SB ADMIN & MENU JS -->
    <script src="<?=site_url('assets/gopanel/js/metisMenu.min.js')?>"></script>
    <script src="<?=site_url('assets/gopanel/js/sb-admin-2.js')?>"></script>

    <!-- PLUGIN JS -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap-growl.min.js"></script>    
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap-switch.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.tablesorter.min.js"></script>
    <!-- <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.timepicker.min.js"></script> -->

    <?/** REF for this plugin : https://github.com/nakupanda/bootstrap3-dialog */?>
     <link href="<?php echo base_url('assets/bootstrap_dialog/css/bootstrap-dialog.min.css'); ?>" rel="stylesheet" type="text/css" media="screen" />
     <script type="text/javascript" src="<?php echo base_url(); ?>assets/bootstrap_dialog/js/bootstrap-dialog.min.js"></script>

    <!-- Jquery Form Validation PLugin -->
     <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.validate.min.js"></script> 
     <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/additional-methods.min.js"></script> 

    <!-- CUSTOM JS -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/custom.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/myjs.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/blockui.js"></script>

</body>

</html>
