<ul class="nav navbar-nav">
  <!-- <li class="active"><a href="#">Link</a></li>
  <li><a href="#">Link</a></li> -->
  <li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cog"></i>&nbsp; Settings <span class="caret"></span></a>
    <ul class="dropdown-menu" role="menu">
      <li><a href="<?=site_url('gopanel/school_settings')?>"><span class="entypo-globe"></span>&nbsp; School Settings</a></li>
      <li><a href="<?=site_url('gopanel/system_parameters')?>"><span class="entypo-pinterest"></span>&nbsp; System Parameters</a></li>
      <li><a href="<?=site_url('departments_cpanel')?>"><i class="fa fa-users"></i>&nbsp; Departments</a></li>
    </ul>
  </li>
  <li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-list"></i>&nbsp; Master Files <span class="caret"></span></a>
    <ul class="dropdown-menu" role="menu">
      <li><a href="<?=site_url('menu_creator')?>">Menu Creator</a></li>
      <li><a href="<?=site_url('packages')?>"><span class="iconicfill-box"></span>&nbsp; Package Creator</a></li>
    </ul>
  </li>
  <li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="entypo-tools"></span>&nbsp; Tools <span class="caret"></span></a>
    <ul class="dropdown-menu" role="menu">
      <li><a href="<?=site_url('gopanel/kabit')?>"><span class="entypo-flow-tree"></span>&nbsp; Database Connection</a></li>
      <li><a href="<?=site_url('gopanel/kwery')?>"><span class="entypo-qq"></span>&nbsp; Kwery</a></li>
      <li><a href="<?=site_url('gopanel/migration')?>"><span class="entypo-rocket"></span>&nbsp; Migration</a></li>
      <li><a href="<?=site_url('gopanel/adminer')?>"><span class="entypo-database"></span>&nbsp; Adminer</a></li>
      <li><a href="<?=site_url('gopanel/backup')?>"><span class="entypo-database"></span>&nbsp; Backup Database</a></li>
      <li class="divider" ></li>
      <li><a href="<?=site_url('gopanel/system_patches')?>" class=""><span class="entypo-leaf"></span>&nbsp; System Patches</a></li>
    </ul>
  </li>
  <li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Others <span class="caret"></span></a>
    <ul class="dropdown-menu" role="menu">
      <li><a href="<?=site_url('gopanel/activity_log')?>"><span class="entypo-chart-line"></span>&nbsp; User Activity Log</a></li>
    </ul>
  </li>
  <li><a href="<?=site_url('packages/set')?>" class=""><span class="entypo-dropbox"></span>&nbsp; Packages</a></li>
</ul>