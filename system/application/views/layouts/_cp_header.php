<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
	<link REL="SHORTCUT ICON" HREF="<?=base_url('assets/images/favicon.ico');?>">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Go Panel</title>
    <?php $xbootstrap = 'default';?>
    <link href="<?php echo base_url('assets/css/bootstrap.'.$xbootstrap.'.min.css'); ?>" rel="stylesheet" type="text/css" media="screen" />
    <link href="<?php echo base_url('assets/css/pace.css'); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/css/custom-theme/jquery-ui-1.8.18.custom.css'); ?>" rel="stylesheet" type="text/css" media="all" />
    <link href="<?php echo base_url('assets/css/glyphicons.css'); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/css/font-awesome-4.0.3/css/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css" media="screen" />
    <link href="<?php echo base_url('assets/css/animate.css'); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/css/entypo/entypo.css'); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/css/iconicfill/iconicfill.css'); ?>" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-ui-1.8.18.custom.min.js"></script>
    <link href="<?php echo base_url('assets/css/bootstrap-switch.css'); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/css/login.css'); ?>" rel="stylesheet" type="text/css" />
    
     <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/pace.js"></script>
    </head>
    <body>
    <div id="msgDialog"></div>
    <?php $this->load->view('layouts/_alert'); ?>
  <!-- Wrap all page content here -->
    <div id="wrap">
      <?if($this->loggin):?>
       <nav class="navbar navbar-black" role="navigation">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#"><span class="entypo-tools"></span>&nbsp; <strong>Go CPanel</strong></a>
        </div>

        
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <?@$this->load->view('layouts/_cp_menus');?>
          <ul class="nav navbar-nav navbar-right">
            <li><a href="#"><i class="fa fa-user"></i>&nbsp;</a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <!-- <li class="divider"></li> -->
                <li><a href="<?=site_url('gopanel/logout')?>"><i class="fa fa-power-off"></i>&nbsp; Exit</a></li>
              </ul>
            </li>
          </ul>
        </div>
      </nav>
      <?endif;?>
      <!-- Begin page content -->
      <div class="container">
        <div class="col-md-12">