<?@$this->load->view('layouts/_cp_header')?>
<?php
	$page_title = isset($page_title) ? $page_title : ucwords(strtolower(str_replace('_',' ',$this->router->class)));
	$page_subtitle = isset($custom_title) ? $custom_title : ucwords(strtolower(str_replace('_',' ',$this->router->method)));
?>
<div class="row">
	<div class="page-header">
	  <h1><?=$page_title?> <small><?=$page_subtitle?></small></h1>
	</div>
	<?=isset($system_message)?$system_message:'';?>
  <?=validation_errors();?>
</div>
<?php echo $yield; ?>
<?@$this->load->view('layouts/_footer')?>