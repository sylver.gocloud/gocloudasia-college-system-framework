<?php $this->load->view('student_previous_accounts/_student_data'); ?>

<table>
  <tr>
    <th>Subject Code</th>
    <th>Section Code</th>
    <th>Description</th>
    <th>Preliminary</th>
    <th>Midterm</th>
    <th>Finals</th>
    <th>Remarks</th>
  </tr>
  <? $num=0 ?>
	<?if($student_grades) :?>
    <? foreach($student_grades as $sg ) :?>
    <tr>
      <td><?= $sg['sc_id'] ?></td>
      <td><?= $sg['code'] ?></td>
      <td><?= $sg['subject_desc'] ?></td>
      <? foreach($sg['grades'] as $sgg ) :?>
      <td>
        <?echo form_open(base_url('/ajax/edit_grade/'.$student->id), "class='form-inline edit-grade-form'");?>
        <div class="form-group">
          <?= form_input('value',NULL , "class='form-control smaller value' placeholder='".$sgg['value']."' autoComplete='off' disabled"); ?>	
        </div>
        <?
        echo form_hidden('sgc_id', $sgg['sgcid']);
        echo form_close();
        ?>
      </td>
      <? endforeach ;?>
      <td><?= $sg['remarks'] ?></td>
    </tr>
    <? endforeach ;?>
    <? endif ;?>
</table>

