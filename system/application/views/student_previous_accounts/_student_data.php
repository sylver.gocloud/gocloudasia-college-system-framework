<div class="alert alert-info text-center">
  <?php echo $student->studid; ?><br />
  <?php echo ucfirst($student->last_name).', '.ucfirst($student->first_name).' '.ucfirst($student->middle_name); ?><br>
  <?php echo $student->year . ' | '.$student->name . ' | '. $student->course; ?>
</div>

<ul class="nav nav-tabs">
	<li <?= ($this->router->class == "student_previous_accounts" && $this->router->method == 'view_previous_balance') ? "class='active'" : '' ?>>
	  <a href="<?php echo base_url('student_previous_accounts/view_previous_balance/index/'.$student->id); ?>">Student Profile</a></li>
	  
	<li <?= ($this->router->class == "student_previous_accounts" && $this->router->method == 'grade') ? "class='active'" : '' ?>>
	  <a href="<?php echo base_url('student_previous_accounts/grade/'.$student->id); ?>">Student Grade</a></li>
	
	<li <?= ($this->router->class == "student_previous_accounts" && $this->router->method == 'view_fees') ? "class='active'" : '' ?>>
	  <a href="<?php echo base_url('student_previous_accounts/view_fees/'.$student->id); ?>">Student Fees</a></li>
</ul>



<br>
