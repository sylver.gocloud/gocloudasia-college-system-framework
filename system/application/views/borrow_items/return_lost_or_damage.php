	
	<script>
  
		function validate()
		{
			var ctr = 0;
			var msg = "<ul>";
			var xfocus = "";
			$('.not_blank').css('border-color','gray');
			$('.not_blank').each(function(){
				var xval = $(this).val().trim();
				var xid = $(this).attr('id');
				if(xval == ""){
					ctr++;
					$(this).css('border-color','red');
					if(ctr == 1) { xfocus = $(this); }
					var label = $('label[for='+xid+']').text();
					
					msg += "<li>"
					+label+" should not be blank</li>";
				}
			});
			
			msg += "<ul>";
			
			if(ctr > 0)
			{
				xfocus.focus();
				$('#alertModal_Label').text('Required Fields'); //SET MODAL TITLE
				$('#alertModal_Body').html(msg); //SET MODAL CONTENT
				$('#alertModal').modal('show'); //SHOW MODAL
				
				return false;
			}
		}
  </script>	
  <style>
	label , .label-info{
		font-size : 12pt;
	}
  </style>
  <div class="well">
	<p><label>Borrow No.</label> : <label class = 'label label-info'><?=$borrow_items->borrow_no;?></label></p>
	<p><label>Date Borrowed</label> : <label class = 'label label-info'><?=$borrow_items->date;?></label></p>
	<p><label>Borrower</label> : <label class = 'label label-info'><?=$borrow_items->name;?></label></p>
	<p><label>Items</label> : <label class = 'label label-info'><?=$borrow_items->item;?></label></p>
	<p><label>Unit Borrowed</label> : <label class = 'label label-info'><?=$borrow_items->unit;?></label></p>
	<p><label>Unit To Be Return</label> : <label class = 'label label-info'><?=$borrow_items->unit_unreturn;?></label></p>
  </div>
  <?echo form_open('','onsubmit="return validate()"');?>
  <p>
    <label for="date_return">Date</label><br />
    <input id="date_return" class="not_blank date_pick" name="date_return" size="30" type="text" value='<?=date('Y-m-d')?>' />
  </p>
  <p>
    <label for="unit">Quantity *</label><br />
	<?
		$ret = array();
		for($x = (float)$borrow_items->unit_unreturn; $x > 0; $x--)
		{
			$ret[$x] = $x;
		}
		echo form_dropdown('unit_return',$ret,'id="unit"');
	?>
  </p>
  <p>
    <label for="unit">Category</label><br />
	<?
		$ret = array(
			'DAMAGE' => 'DAMAGE',
			'BROKEN' => 'BROKEN',
			'LOST' => 'LOST',
		);
		echo form_dropdown('category',$ret,'id="category"');
	?>
  </p>
   <p>
    <label for="remarks">Remarks</label><br />
    <textarea cols="60" id="remarks" name="remarks" rows="10" maxLength="255">
	</textarea>
  </p>
  <?echo form_submit('','Save Changes');?>
		<?echo form_close();?>