   <style>
	label , .label-info{
		font-size : 12pt;
	}
  </style>
 <div class="well">
	<p><label>Borrow No.</label> : <label class = 'label label-info'><?=$borrow_items->borrow_no;?></label></p>
	<p><label>Date Borrowed</label> : <label class = 'label label-info'><?=$borrow_items->date;?></label></p>
	<p><label>Borrower</label> : <label class = 'label label-info'><?=$borrow_items->name;?></label></p>
	<p><label>Items</label> : <label class = 'label label-info'><?=$borrow_items->item;?></label></p>
	<p><label>Unit Borrowed</label> : <label class = 'label label-info'><?=$borrow_items->unit;?></label></p>
 </div>
 
 <p>
	<a class='btn btn-default btn-sm' target="_blank" href="<?=base_url()?>borrow_items/print_return/<?=$demands_id?>">Print</a> |
	<a  class='btn btn-default btn-sm' href="<?=base_url()?>borrow_items/records">Back to list of Records</a>
 </p>
 
 <table>
	<tr>
		<th colspan='3' style='text-align:center'>Item Return</th>
	</tr>
	<tr>
		<th>Date</th>
		<th>Quantity</th>
		<th>Remarks</th>
	</tr>
	<?if($demands_return):?>
		<?foreach($demands_return as $obj):?>
			<tr>
				<td><?=$obj->date_return?></td>
				<td><?=$obj->unit_return?></td>
				<td><?=$obj->remarks?></td>
			</tr>
		<?endforeach;?>
	<?else:?>
	<?endif;?>
 </table>
  <table>
	<tr>
		<th colspan='4' style='text-align:center'>Item Lost/Damage Return</th>
	</tr>
	<tr>
		<th>Date</th>
		<th>Quantity</th>
		<th>Category</th>
		<th>Remarks</th>
	</tr>
	<?if($demands_return_lost):?>
		<?foreach($demands_return_lost as $obj):?>
			<tr>
				<td><?=$obj->date_return?></td>
				<td><?=$obj->unit_return?></td>
				<td><?=$obj->category?></td>
				<td><?=$obj->remarks?></td>
			</tr>
		<?endforeach;?>
	<?else:?>
	<?endif;?>
 </table>