<?if($event):?>	

	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
			  <div class="panel-heading">
			  	<h4>
			  		<?=$event->title?> 
			  		<?if($event->is_holiday == "1"):?>
			  			<small>(Holiday)</small>
			  		<?endif;?>
			  		<small class='pull-right' ><?=date('m-d-Y h:i a', strtotime($event->start_date))?>-<?=date('m-d-Y h:i a', strtotime($event->end_date))?></small>
			  	</h4>
			  </div>
			  <div class="panel-body">
			    <?=html_entity_decode($event->description,ENT_COMPAT);?>
			  </div>
			</div>
		</div>
	</div>

<?endif;?>