<div id="right">
	<div id="right_bottom">


<div id="announcement_show" class='well'>

<h4><div class='label label-info'><?php echo $event->title; ?></div></h4><br>

<?php echo "<b>FROM</b> : " . date('m-d-Y',strtotime($event->start_date)).' To '. date('m-d-Y',strtotime($event->end_date)); ?>
<p><br/>
<b>About</b> : <?php echo $event->description; ?><br />
</p>
<p><br/>
<b>Is holiday ?</b> : <?php echo $event->is_holiday == 1 ? "YES" : "NO"; ?><br />
</p>
</div>

<p><a class='btn btn-default btn-sm' href="<?php echo base_url(); ?>events">Back To List Of Events</a></p>

	</div>

</div>