<script type='text/javascript'>
	$(function() {
		$( "#select_student" ).dialog({
		  autoOpen: false,
		  height: 300,
		  width: 550,
		  modal: true,
		  title: "Generate Exam Permit Per Student",
		});
	});
	
	function select_student(e)
	{
		event.preventDefault();
		alert('x');
	}
	
	function Search_students(page)
	{
		var page = page == null || page == '' || page == '#' ?  0 : page;
		$.blockUI({ message: "" }); 
		var xreturn = false;
		var controller = 'ajax';
		var base_url = '<?php echo base_url(); ?>'; 
		var year_from = '<?php echo $this->open_semester->year_from; ?>'; 
		var year_to = '<?php echo $this->open_semester->year_to; ?>'; 
		var year_id = $('#year_id').val();
		var semester_id = '<?php echo $this->open_semester->id; ?>'; 
		var course_id = $('#course_id').val();
		
		var xdata = $('#select_student *').serialize()
					+"&year_id="+year_id
					+"&semester_id="+semester_id
					+"&course_id="+course_id;
		
		$.ajax({
			'url' : base_url + '' + controller + '/search_students_for_grade_slip/'+page+'/'+year_from+'/'+year_to,
			'type' : 'POST', 
			'async': false,
			'data' : xdata,
			'dataType' : 'json',
			'success' : function(data){ 
				// console.log(data);
				$('#search_links').html(data.links);
				
				
				$('#search_links li a').click(function(event){
					event.preventDefault(); //PREVENT LINKS FROM REDIRECTING
					var href = $(this).attr("href");
					next_page = (href.substr(href.lastIndexOf('/') + 1));
			
					Search_students(next_page);
				})
				$('#tbody').html('');
				
				var tr = "";
				
				if(data.total_rows > 0){
					for(var x = 0 ; x <= data.count; x++){
						if(data.users[x] != null){
						var xid = data.users[x].id;
						var xstudid = data.users[x].studid;
						var xname = data.users[x].name;
					
						tr += "<tr>";
						tr += "<td>"+xstudid+"</td>";
						tr += "<td>"+xname+"</td>";
						tr += "<td><a href='"+base_url+"student/generate_soa_per_student/"+xid+"' target='_blank'><div class='badge'>Generate SOA</div></a></td>";
						tr += "</tr>";
						}
					}
					tr += "<tr><td colspan='2'>No of Records</td><td><span class='badge'>"+data.total_rows+"</span></td></tr>";
					
				}else{
					tr = "<tr><td colspan='2'>No records found.</td></tr>";
				}
				
				$('#items_tbody').html(tr);
			}
		})
		.done(function() {
			  $.unblockUI(); 
		  });
		
	}
</script>
<?php
$formAttrib = array('id' => 'search_enrollee', 'class' => 'form-inline', 'method' => 'POST', 'target' => '_blank');

?>
<?echo form_open('',$formAttrib);?>
		
<div class="well">
	<div class="form-group">
    <?=year_dropdown('year_id',isset($year_id)?$year_id:'',"id='year_id'", 'All')?>
  </div><br/><br/>
  
  <div class="form-group">
    <?=course_dropdown('course_id',isset($course_id)?$course_id:'',"id='course_id'", '')?>
  </div><br/><br/>
</div>

 <div class="well">
	<?echo form_submit('submit','Generate Statement of Account');?>
	<button class='btn btn-default btn-sm' onclick='$("#select_student").dialog("open"); return false;' >Select a student</button>
 </div>
 <?echo form_close();?>
 <div id='select_student'>
		<table>
		  <tr>
			<td> <div class="form-group">
				<?
					$xfields = array(
						"enrollments.studid" => "Student ID No.",
						"enrollments.name" => "Name",
					);
					echo form_dropdown('fields', $xfields, isset($item_field)?$item_field:'serial','id="item_field"');
				?>
				</div>
			</td>
			<td>
				<div class="form-group">
					<label class="sr-only" for="author">Keyword</label>
					<input type='text' name='keyword' id='keyword' value="<?=isset($keyword)?$keyword:''?>" placeHolder="Keyword" />
				</div>
			</td>
			<td><button type="button" class="btn btn-default btn-sm" onclick='Search_students()'>Search</button></td>
		  </tr>
		</table>
			<table id='results' style='font-size:7pt'>
			  <tr>
				<th>Student ID</th>
				<th>Name</th>
				<th>ACTION</th>
			  </tr>
				<tbody id='items_tbody'>
				</tbody>
			</table>
			<div id='search_links' style='font-size:7pt'></div>
</div>