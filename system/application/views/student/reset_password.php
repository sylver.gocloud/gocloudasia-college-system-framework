<div class="row">
	<!-- Nav tabs -->
	<ul class="nav nav-tabs">
	  <li class="active"><a href="#single" data-toggle="tab">Select Student</a></li>
	</ul>

	<!-- Tab panes -->

	<div class="tab-content">
		<br/>
		<div class="tab-panel active" id="single">
			
				<?php $formAttrib = array('id' => 'search_enrollee', 'class' => 'form-inline', 'method' => 'GET'); ?>
				<?php echo form_open('student/reset_password/0', $formAttrib);?>
			 <div class="form-group">
			    <input id="lastname" class="form-control" type="text" value="<?=isset($lastname)?$lastname:''?>" name="lastname" placeholder="Last Name">
			  </div>
			   
			  <div class="form-group">
			    <input id="fname" class="form-control" type="text" value="<?=isset($fname)?$fname:''?>" name="fname" placeholder="First Name">
			  </div>
			   
			  <div class="form-group">
			    <input id="studid" class="form-control" type="text" value="<?=isset($studid)?$studid:''?>" name="studid" placeholder="Student ID">
			  </div>
			  <div class="form-group">
			    <?=year_dropdown('year_id',isset($year_id)?$year_id:'',"", '')?>
			  </div>
			   <?php echo form_submit('submit', 'Search','class="btn btn-default btn-sm"'); ?>
				<?echo form_close();?>
				<br>
				<?echo isset($links) ? $links : NULL;?>
				
				<br />
				<table>
					<tr>
					  <th>Student ID</th>
					  <th>Name</th>
					  <th>Year</th>
					  <th>Course</th>
					  <th>Birth Date</th>
					  <th>Action</th>
					</tr>
					<tbody>
					
					<?if($search):?>
						<?foreach ($search as $student):?>
						<? $l = _se($student->id); ?>
						<tr>
						<td class='bold txt-facebook' ><?php echo $student->studid; ?></td>
						<td class='bold txt-facebook'><?php echo ucfirst($student->name); ?></td>
						<td><?php echo $student->year; ?></td>
						<td><?php echo $student->course; ?></td>
						<td><?php echo $student->date_of_birth == "0000-00-00" ? "Not Set (0000-00-00)" : date('m-d-Y', strtotime($student->date_of_birth)); ?></td>
						<td>
							<button class="btn btn-xs btn-default" onclick="reset_dis('<?=$student->id?>')" >Reset Password</button>
						</td>
						</tr>
						<?endforeach;?>
						<tr>
							<td colspan='4' align='right'><b>Total Records</b></td>
							<td><span class='badge'><?=($total_rows) ? $total_rows : 0;?></span></td>
						</tr>
					<?else:?>
					<tr>
						<td colspan="5">
							Student Not Found
						</td>
					</tr>
					<?endif;?>
					</tbody>
				</table>
				<?echo isset($links) ? $links : NULL;?>
		</div>
	</div>
</div>

<?php
	$dd = array();
	if($search){
		foreach ($search as $key => $value) {
			$dd[$value->id] = $value->name;
		}
	}
?>

<!-- MODAL RESET FORM -->
<div class="modal fade" id="reset_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Change Password Form</h4>
      </div>
      <div class="modal-body">
        	<?=form_open('student/reset_password/0'.$suffix,' class="form-horizontal"')?>
        	<div class="row" id="system_error" >
        	<?if(isset($system_error) && $system_error):?>
        		<div class="alert alert-danger" ><?=$system_error?></div>
        	<?endif;?>
        	</div>
			<div class="table-responsive" >
				<table>
					<tr>
						<td><label class="col-md-4 control-label" for="textinput">Student Selected</label></td>
					  	<td>
					  		<?=form_dropdown('eid',$dd,'','class="form-control input-md" id="eid" required')?>
					  	</td>
					</tr>

					<!-- Multiple Radios (inline) -->
					<tr>
					  	<td><label class="col-md-4 control-label" for="radios">Password Type</label></td>
					  	<td> 
						    <label class="radio-inline" for="radios-0">
						      <input type="radio" name="radios" id="radios-0" value="lastname" checked="checked" onchange="to_lastname(this)" >
						      Reset Password to Lastname
						    </label> 
						    <label class="radio-inline" for="radios-1">
						      <input type="radio" name="radios" id="radios-1" value="custom" onchange="to_custom(this)" >
						      Custom Password
						    </label>
						    <script type="text/javascript">
						    	function to_lastname (el) {
						    		if($(el).attr('checked', true)){
						    			$('.tr_new_pass').hide();
						    		}
						    	}

						    	function to_custom (el) {
						    		if($(el).attr('checked', true)){
						    			$('.tr_new_pass').show();
						    		}
						    	}
						    </script>
					  	</td>
					</tr>

					<!-- Password input-->
					<tr class="tr_new_pass" hidden >
						<td><label class="col-md-4 control-label" for="passwordinput">New Password</label></td>
						<td>
						    <input id="newpassword" minlength="6" name="newpassword" type="password" placeholder="New Password" class="form-control input-md">
						</td>
					</tr>

					<!-- Password input-->
					<tr class="tr_new_pass" hidden >
						<td><label class="col-md-4 control-label" for="passwordinput">Confirm Password</label></td>
						<td>
						    <input id="confirmpassword" minlength="6" name="confirmpassword" type="password" placeholder="Confirm Password" class="form-control input-md">
						</td>
					</tr>
				</table>
			</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
        <input type="submit" id="change_password" name="change_password" class="btn btn-sm btn-success" value="Change Password" />
        </form>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
	$('#myTab a').click(function (e) {
	  e.preventDefault()
	  $(this).tab('show')
	});

	function reset_dis(uid) {
		$('#reset_modal').modal('show')
		$('#eid').val(uid);
		$('#system_error').html('');
		$('#newpassword').val('');
		$('#confirmpassword').val('');
	}
	$(document).ready(function(){
		<?if(isset($system_error)):?>
	
			$('#reset_modal').modal('show');
			$('#eid').val('<?=$s_eid?>');
		
		<?endif;?>	
	})
</script>