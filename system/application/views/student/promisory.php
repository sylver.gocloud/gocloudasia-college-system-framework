<?php 
	$formAttrib = array('id' => 'search_enrollee', 'class' => 'form-inline', 'method' => 'GET');
	echo form_open('', $formAttrib);
?>

<div class="well">
	
	<div class="form-group">
		<b>Current Grading Period :  <?=$current_period->grading_period?> </b>
	</div>
   
	<br/><br/>
   
	<div class="form-group">
		<input id="studid" class="form-control" type="text" value="<?=isset($studid)?$studid:''?>" name="studid" placeholder="Student ID">
	</div>
   
	<div class="form-group">
		<input id="name" class="form-control" type="text" value="<?=isset($name)?$name:''?>" name="name" placeholder="Full Name">
	</div>
	
	<br/><br/>
	
	<div class="form-group">
		<?=year_dropdown('year_id',isset($year_id)?$year_id:'',"", 'All Years')?>
	</div>
	
	<div class="form-group">
		<?=course_dropdown('course_id',isset($course_id)?$course_id:'',"", 'All Courses')?>
	</div>
   
	<?php
	echo form_hidden('semester_id', $this->open_semester->id);
	echo form_hidden('sy_from', $this->open_semester->year_from);
	echo form_hidden('sy_to', $this->open_semester->year_to);
	echo form_hidden('is_paid', 1);
	?>
		<?php echo form_submit('submit', 'Search'); ?>
		<?php echo form_submit('submit', 'Print','class="btn btn-default btn-sm"'); ?>
		<?echo form_close();?>
	
</div>

<?echo isset($links) ? $links : NULL;?>
<?php
if(isset($search)){
?>
<br />
<table>
	<tr>
	  <th>Student ID</th>
	  <th>Name</th>
	  <th>Year</th>
	  <th>Course</th>
	  <th>Balance</th>
	</tr>
	<tbody>
	<?php
	if(!empty($search))
	{
		foreach($search as $key => $student):
		$l = _se($student->id);
		?>
		<tr>
		<td><?php echo $student->studid; ?></td>
		<td><?php echo ucfirst($student->name); ?></td>
		<td><?php echo $student->year; ?></td>
		<td><?php echo $student->course; ?></td>
		<td>
			<?if(isset($details[$key]) && $details[$key]):?>
				<?=number_format($details[$key]->balance, 2, ',',' ');?>
			<?else:?>
				0
			<?endif;?>
			<?echo badge_link('fees/view_fees/'.$student->id, "primary", '&nbsp; fees');?>
		</td>
		</tr>
		<?php 
		endforeach; ?>
		<tr>
			<td colspan='4' align='right'><b>Total Records</b></td>
			<td><span class='badge'><?=($total_rows) ? $total_rows : 0;?></span></td>
		</tr>
		<?php
	}
	else
	{
	?>
	<tr>
	<td colspan="5">
		Student Not Found
	</td>
	</tr>
	<?php
	}
	?>
	</tbody>
	<tr>
	  <th>Student ID</th>
	  <th>Name</th>
	  <th>Year</th>
	  <th>Course</th>
	  <th>Balance</th>
	</tr>
</table>
<?echo isset($links) ? $links : NULL;?>
<?php
}
?>
</div>
