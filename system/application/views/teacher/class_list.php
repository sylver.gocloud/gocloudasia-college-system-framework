<table>
  <tr>
    <th>Subject Code</th>
    <th>Section Code</th>
    <th>Description</th>
    <th>Time</th>
    <th>Day</th>
    <th>Room</th>
  </tr>
  <tr>
	<?if($subject):?>
    <td><?= $subject->sc_id ?></td>
    <td><?= $subject->code ?></td>
    <td><?= $subject->subject ?></td>
    <td><?= $subject->time ?></td>
    <td><?= $subject->day ?></td>
    <td><?= $subject->room ?></td>
	<?else:?>
	<td colspan= '5' >No record found.</td>
	<?endif;?>
  </tr>
</table>

<table>
  <tr>
    <th>#</th>
    <th>Student ID</th>
    <th>Name</th>
    <th>Year</th>
    <th>Course</th>
    <th>Gender</th>
  </tr>
  <? $num = 0 ?>
  <?if($students):?>
  <? foreach($students as $student) :?>
  <tr>
    <td><?= $num += 1 ?></td>
    <td><?= $student->studid ?></td>
    <td><?= $student->student_name ?></td>
    <td><?= $student->year ?></td>
    <td><?= $student->course ?></td>
    <td><?= $student->sex ?></td>
  </tr>
  <? endforeach ;?>
  <?else:?>
	<tr>
		<td colspan= '6' >No record found.</td>
	</tr>
  <?endif;?>
</table>
