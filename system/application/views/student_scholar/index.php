<div id="right">
	<?php 
		$formAttrib = array('id' => 'search_enrollee', 'class' => 'form-inline', 'method' => 'POST');
		echo form_open('', $formAttrib);
		
	?>
    
  <div class="form-group">
    <input id="lastname" class="form-control" type="text" value="<?=isset($lastname)?$lastname:''?>" name="lastname" placeholder="Last Name">
  </div>
   
  <div class="form-group">
    <input id="fname" class="form-control" type="text" value="<?=isset($fname)?$fname:''?>" name="fname" placeholder="First Name">
  </div>
   
  <div class="form-group">
    <input id="studid" class="form-control" type="text" value="<?=isset($studid)?$studid:''?>" name="studid" placeholder="Student ID">
  </div>
   
	<?php
	echo form_hidden('semester_id', $this->open_semester->id);
	echo form_hidden('sy_from', $this->open_semester->year_from);
	echo form_hidden('sy_to', $this->open_semester->year_to);
	echo form_hidden('is_paid', 1);
	?>
   		<?php echo form_submit('submit', 'Search'); ?>
   		<?php echo form_submit('submit', 'Print','target="_blank"'); ?>
<br><br>
	<?php
	if(isset($search)){
	?>
	<br />
	<table>
		<tr>
		  <th>Student ID</th>
		  <th>Name</th>
		  <th>Year</th>
		  <th>Course</th>
		  <th>View</th>
		</tr>
		<tbody>
		<?php
		if(!empty($search))
		{
			foreach($search as $student):
			?>
			<tr>
			<td><?php echo $student->studid; ?></td>
			<td><?php echo ucfirst($student->name); ?></td>
			<td><?php echo $student->year; ?></td>
			<td><?php echo $student->course; ?></td>
			<td>
				<?
					echo badge_link('profile/view/'.$student->id, "primary", 'profile');
					echo badge_link('fees/view_fees/'.$student->id, "primary", 'fees');					
				?>
				
			</td>
			</tr>
			<?php 
			endforeach; 
			?>
				<tr>
				  <th style='text-align:right' colspan='4'>Total Scholars</th>
				  <td><div class='badge'><?=$total_rows?></div></td>
				</tr>
			<?
		}
		else
		{
		?>
		<tr>
		<td colspan="5">
			Student Not Found
		</td>
		</tr>
		<?php
		}
		?>
		</tbody>
	</table>
	<?echo isset($links) ? $links : NULL;?>
	<?php
	}
	?>
	</div>
	
	<div class="clear"></div>
</div>
