<div id="right">

	<div id="right_top" >
		  <p id="right_title">Certificate Of Enrollment Settings</p>
	</div>
	<div id="right_bottom">

	<div>

<table>
  <tr>
    <th>First Paragraph</th>
    <th>Second Paragraph</th>
    <th>Signatory</th>
    <th>Signatory Position</th>
  </tr>

    <tr>
      <td><?php echo htmlentities($coes->first_paragraph); ?></td>
      <td><?php echo htmlentities($coes->second_paragraph); ?></td>
      <td><?php echo htmlentities($coes->signatory); ?></td>
      <td><?php echo htmlentities($coes->signatory_position); ?></td>
      <td>
		<a href="/certificate_of_enrollment_settings/show/<?php echo $coes->id; ?>">Show</a><br />
		<a href="/certificate_of_enrollment_settings/edit/<?php echo $coes->id; ?>">Edit</a>
	  </td>
    </tr>

</table>

	</div>
	<div class="clear"></div>

	</div>
	
	<div class="clear"></div>
	
</div>
