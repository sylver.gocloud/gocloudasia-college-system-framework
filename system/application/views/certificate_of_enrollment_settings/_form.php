<?
	if($coes)
	{
		$signatory_val = $coes->signatory;
		$signatoryp_val = $coes->signatory_position;
		$firstp_val = $coes->first_paragraph;
		$secondp_val = $coes->second_paragraph;
	}else{
		$signatory_val = '';
		$signatoryp_val = '';
		$firstp_val = '';
		$secondp_val = '';
	}
	$s_att = array('name'=>'signatory','value'=>$signatory_val);
	$sip_att = array('name'=>'signatory_position','value'=>$signatoryp_val);
	$fp_att = array('name'=>'first_paragraph','value'=>$firstp_val);
	$sp_att = array('name'=>'second_paragraph','value'=>$secondp_val);
 ?>

  <p>
	<label for="first_paragraph">First Paragraph</label><br />
	<?php echo form_textarea($fp_att); ?>
  </p>
  <p>
	<label for="second_paragraph">Second Paragraph</label><br />
	<?php echo form_textarea($sp_att); ?>
  </p>
  <p>
	<label for="signatory">Signatory</label><br />
    <?php echo form_input($s_att); ?>
  </p>
  <p>
	<label for="signatory_position">Signatory Position</label><br />
    <?php echo form_input($sip_att); ?>
  </p>

