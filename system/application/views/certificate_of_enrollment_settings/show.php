<div id="right">

	<div id="right_top" >
		  <p id="right_title">Certificate Of Enrollment Setting</p>
	</div>
	<div id="right_bottom">

	<div>

<p>
  <strong>First Paragraph:</strong>
  <?php echo htmlentities($coes->first_paragraph); ?>
</p>
<br />
<p>
  <strong>Second Paragraph:</strong>
  <?php echo htmlentities($coes->second_paragraph); ?>
</p>
<br />
<p>
  <strong>Signatory:</strong>
  <?php echo htmlentities($coes->signatory); ?>
</p>
<br />
<p>
  <strong>Signatory Position:</strong>
  <?php echo htmlentities($coes->signatory_position); ?>
</p>
<br />
<p>
  <a href="/certificate_of_enrollment_settings/edit/<?php echo $coes->id; ?>">Edit</a>
</p>

	</div>
	<div class="clear"></div>

	</div>
	
	<div class="clear"></div>
	
</div>