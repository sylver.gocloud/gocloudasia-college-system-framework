<script src="<?=base_url('assets/tinymce/tinymce.min.js')?>"></script>
<script>
        tinymce.init({selector:'textarea',
					  resize: false,
					  theme:'modern',
					  height:300,
					  entity_encoding :'raw',
					  plugins: [
        "advlist autolink lists link image charmap textcolor print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste"
    ],
					  toolbar: " undo redo | fontsizeselect styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | preview  fullpage anchor| forecolor backcolor emoticons"
					  });
</script>

<div class="row">
	<div class="col-md-12">
		<div class="alert alert-github">
			<div class="row">
				<div class="col-md-2 bold">Announcement for : </div>
				<div class="col-md-2">
					  
				    <input type="checkbox" name="to_whom[]" value="all" 
						<?php if($announcement)
						{
						echo $announcement->to_all=='' || $announcement->to_all=='0' ? "":"checked"; 
						}
						?> 
						/>
					  <label for="to_all">To All</label>
				</div>
				<div class="col-md-2">
					<p>
					<input type="checkbox" name="to_whom[]" value="employee" 
					<?php if($announcement)
					{
					echo $announcement->to_employee=='' || $announcement->to_employee=='0' ? "":"checked"; 
					}
					?> 
					/>
				    <label for="to_employee">To Employees</label>
				  </p>
				</div>
				<div class="col-md-2">
					  <p>
					    <input type="checkbox" name="to_whom[]" value="student" 
						<?php if($announcement)
						{
						echo $announcement->to_student=='' || $announcement->to_student=='0' ? "":"checked"; 
						}
						?>
						/>
					    <label for="to_student">To Students</label>
					  </p>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					 	<p>
					 		<?php
					 			if($announcement)
								{
									$title = $announcement->title;
								}else{
									$title = '';
								}
								$t_att = array('name'=>'title','value'=>$title, 'class'=>'form-control','required' => true);
					 		?>
				    	<label for="date">Title</label><br />
							<?php echo form_input($t_att); ?>
				  	</p>

				  	<p>
					 		<?php
					 			if($announcement)
								{
									$date_val = date('m/d/Y',strtotime($announcement->date));
								}else{
									$date_val = '';
								}
								$d_att = array('name'=>'date','value'=>$date_val, 'class'=>'datepicker', 'size'=>'20x20' ,'required' => true);
					 		?>
				    	<label for="date">Date</label><br />
							<?php echo form_input($d_att); ?>
				  	</p>
				</div>
			</div>
		</div>
	</div>
</div>
<p></p>

<div class="row">
	<div class="col-md-12">
		<?$message = isset($announcement) && $announcement ? $announcement->message : '';?>
		<textarea name="message"><?=html_entity_decode($message,ENT_COMPAT);?></textarea>
	</div>
</div>
<p></p>