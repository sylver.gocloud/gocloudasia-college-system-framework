<div id="row">
  <?php echo form_open('announcements/index/0',' class="form-inline" method="GET"') ?>

    <div class="col-md-12">
      <div class="form-group">
        <input id="date" class="form-control date_pick" type="text" value="<?=isset($date)?$date:''?>" name="date" placeholder="Date">
      </div>

      <div class="form-group">
        <input id="title" class="form-control" type="text" value="<?=isset($title)?$title:''?>" name="title" placeholder="Title">
      </div>

      <div class="form-group">
        <button type="submit" name='search' value='search' class='btn btn-facebook btn-sm' ><i class="fa fa-search"></i>&nbsp; Find</button>
      </div>

      <div class="form-group">
        <?if(isset($mydepartment) && $mydepartment && $mydepartment->edit_announcement == "1"):?>
          <a class='btn btn-primary btn-sm' href="<?php echo base_url(); ?>announcements/create" rel="facebox"><span class = 'glyphicon glyphicon-plus'></span>&nbsp;  New</a>
        <?endif;?>
      </div>
      <p></p>
    </div>
    
  	<div class="col-md-12">
      <div id="table-responsive">
        <table id="table table-stripped table-sortable">
          
        	<tr class='gray'>
            <th width="18%" >Date</th>
            <th width="30%">Title</th>
            <th>Created By</th>
            <th>Action</th>
          </tr>
          <tbody>

            <?php if($announcements){ ?>
            <?php foreach( $announcements as $announcement): ?>
              <tr>
                <td class='bold txt-facebook' ><?php echo date('M d, Y g:h A', strtotime($announcement->date)) ; ?></td>
                <td class='bold txt-facebook' ><?php echo $announcement->title ; ?></td>
                <td class='bold' ><?php echo ucwords($announcement->username) ; ?></td>
                <td>
                  <?if($mydepartment->edit_announcement == "1"):?>
                    <a href="<?php echo base_url()."announcements/edit/".__link($announcement->id); ?>" rel="facebox" class="btn btn-primary btn-xs"><i class="fa fa-eye"></i>&nbsp;  Open</a>
                    <a href="<?php echo base_url()."announcements/destroy/".__link($announcement->id); ?>" rel="facebox" class="btn btn-google-plus btn-xs confirm"><span class='glyphicon glyphicon-trash' ></span>&nbsp; Destroy</a>
                  <?else:?>
                    <a href="<?php echo base_url()."announcements/view/".__link($announcement->id); ?>" rel="facebox" class="btn btn-primary btn-xs"><i class="fa fa-eye"></i>&nbsp;  Open</a>
          	     <?endif;?>
                 </td>
              </tr>
            <?php endforeach; ?>
            <?php } ?>
          </tbody>
        </table>
        <?=$links?>
      </div>
    </div>
  <?=form_close();?>
</div>