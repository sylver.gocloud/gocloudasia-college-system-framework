
 
  <p>
    <label for="title">Title</label><br />
	<?php 
		$data = array(
              'name'        => 'permit_settings[name]',
              'value'       => isset($permit_settings->name) ? $permit_settings->name : '',
              'maxlength'   => '100',
              'size'        => '50',
              'style'       => 'width:200px',
            );

		echo form_input($data);
	?>
  </p>
  <div class="clear"></div>

