<div class="well">
	<?echo form_open('','class="form-inline" role="form"');?>
	<div class="form-group">
		Search In
	</div>
	 <div class="form-group">
		<?
			$xfields = array(
				"admission_slips.date" => "Date (Y-m-d)",
				"admission_slips.subject" => "Subjects",
				"admission_slips.inclusive_dates_of_absences" => "Inclusive Dates of Abscenses",
			);
			echo form_dropdown('fields', $xfields, isset($fields)?$fields:'admission_slips.date');
		?>
	</div>
	<div class="form-group">
		<label class="sr-only" for="author">Author</label>
		<input type='text' name='keyword' value="<?=isset($keyword)?$keyword:''?>" placeHolder="Keyword" />
	</div>
	<div class="form-group">
			Category
	</div>
	 <div class="form-group">
		<?
			$xfields = array(
				"" => "ALL",
				"Excuse" => "Excuse",
				"Unexcuse" => "Unexcuse",
				"Warning" => "Warning",
			);
			echo form_dropdown('category', $xfields, isset($category)?$category:'');
		?>
	</div>
	<?
		echo form_submit('submit','Search');		?>
		<a class='btn btn-default btn-sm' href='<?=base_url()?>admission_slips/issue/<?=$id?>'><span class = 'glyphicon glyphicon-plus'></span>&nbsp;   Issue Admission Slip</a>
		<?echo form_close();?>
</div>
<table>
	  <tr>
		<th>Date</th>
		<th>Subject</th>
		<th>Inclusive Dates of Abscenses</th>
		<th>Reason</th>
		<th>Category</th>
		<th>Action</th>
	  </tr>
	<? $num = 0 ?>
	<?php if(empty($admission_slip) == FALSE):?>
		<? foreach($admission_slip as $obj): ?>
			<?//vp($obj);?>
			<tr>
				<td><?= $obj->date;?></td>
				<td><?= $obj->subject;?></td>
				<td><?= $obj->inclusive_dates_of_absences ?></td>
				<td><?= $obj->reason ?></td>
				<td><?= $obj->category ?></td>
				<td>
					<?= badge_link('admission_slips/edit/'.$obj->id.'/'.$id, "primary", 'Edit'); ?>
					<?= badge_link('admission_slips/destroy/'.$obj->id, "primary confirm", 'Destroy','title="Are you sure to delete this record?"'); ?>
				</td>
			</tr>
		<? endforeach;?>
		<tr>
			<td colspan=5 class="text-right">Total No. of Records</td>
			<td><div class='badge'><?=$total_rows;?></div></td>
		</tr>
	<?php else:?>
		<tr>
			<td colspan=6 class="text-center">----NO data to show---</td>
		</tr>	
	<?php endif;?>
</table>
<?= $links;?>

<a class='btn btn-default btn-sm' href='<?=base_url("admission_slips")?>'><i class="fa fa-arrow-left"></i>&nbsp; Back to List</a>