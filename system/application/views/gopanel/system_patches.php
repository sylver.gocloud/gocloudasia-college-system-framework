<div class="btn-group">
  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
    Run Updates <span class="caret"></span>
  </button>
  <ul class="dropdown-menu" role="menu">
    <li><a href="<?=site_url('gopanel/system_patches/create_default_packages')?>" class="confirm" title="Are you sure? This packages might be outdated.">1. Create Default Packages (<i>Bronze, Silver, Gold, Diamond</i>)</a></li>
    <li><a href="<?=site_url('gopanel/system_patches/restore_default_packages')?>" class="confirm" title="Are you sure? This packages might be outdated.">2. Restore Default Packages - delete current packages and add(<i>Bronze, Silver, Gold, Diamond</i>)</a></li>
    <li><a href="<?=site_url('gopanel/system_patches/fix_main_menu_id_of_menus')?>" class="confirm">3. Fix Menus Main Menu ID (Search and Update Main Menu ID of table 'menus')</a></li>
    <li><a href="<?=site_url('gopanel/system_patches/fix_main_menu_remarks')?>" class="confirm">4. Update Main menu remarks for menu tooltip update.</a></li>
    <li><a href="<?=site_url('gopanel/system_patches/add_library_main_menus')?>" class="confirm">5. Add Library Main Menus (Should Add Manually to Department Menus and Package Menus Afterwards).</a></li>
    <li><a href="<?=site_url('gopanel/system_patches/fix_users_withno_employee_record')?>" class="confirm">6. Fix Users without employee record.</a></li>
    <li><a href="<?=site_url('gopanel/system_patches/add_system_custom_message_menu')?>" class="confirm">7. Create System Custom Message Menu (Should Add Manually to Department Menus and Package Menus Afterwards).</a></li>
    <li><a href="<?=site_url('gopanel/system_patches/add_system_custom_message_data')?>" class="confirm">8. Add Default data for system custom message.</a></li>
    <li><a href="<?=site_url('gopanel/system_patches/add_master_subject_menus')?>" class="confirm">5. Add Master Subject Menus (Should Add Manually to Department Menus and Package Menus Afterwards).</a></li>
    <li><a href="<?=site_url('gopanel/system_patches/add_grading_system_menu')?>" class="confirm">6. Add Grading System Menu (Should Add Manually to Department Menus and Package Menus Afterwards).</a></li>
    <li><a href="<?=site_url('gopanel/system_patches/add_advance_settings_menu')?>" class="confirm">7. Add Advance Settings Menu (Should Add Manually to Department Menus and Package Menus Afterwards).</a></li>
    <li><a href="<?=site_url('gopanel/system_patches/fix_confirm_enrollees_menus')?>" class="confirm">8. Fix Confirm Enrollees Menus</a></li>
  </ul>
</div>