<div id="right">
	<?php 
		$formAttrib = array('id' => 'search_employee', 'class' => 'form-inline', 'method' => 'GET');
		echo form_open('gopanel/users/0', $formAttrib);
	?>
    
 <div class="form-group">
    <input id="name" class="form-control" type="text" value="<?=isset($name)?$name:''?>" name="name" placeholder="Name">
  </div>
   
  <div class="form-group">
    <input id="login" class="form-control" type="text" value="<?=isset($_GET['login'])?$_GET['login']:''?>" name="login" placeholder="Employee ID / Login ID">
  </div>

  <div class="form-group">
    <b>Role : </b>&nbsp;
  </div>

  <div class="form-group">
	<?
    echo departments_dropdown('role',isset($role) ? $role : '','','All');
	?>
	
  </div>
   		<?php echo form_submit('submit', 'Search'); ?>
   		<a href="<?=site_url('employees/create')?>" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i>&nbsp; Create Employee</a>
		<?echo form_close();?>
<br>
<?echo isset($links) ? $links : NULL;?>
	<?php
	if(isset($search)){
	?>
	<br />
	<table>
		<tr>
			<th>Name</th>
			<th>Login</th>
			<th>Role</th>
			<th>Email</th>
			<th>Action</th>
		</tr>
		<tbody>
		<?php
		if(!empty($search))
		{
			foreach($search as $s):
			?>
			<tr>
				<td><?=strtoupper($s->fullname);?></td>
				<td><?=$s->employeeid;?></td>
				<td><?=$s->role;?></td>
				<td><?=$s->email;?></td>
				<td>
					<div class="btn-group btn-group-sm">
					<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
					
						<i class="fa fa-caret-square-o-down"></i> <span class="caret"></span>
						</button>
						<ul class="dropdown-menu pull-right" role="menu">
							<li><a href="<?= site_url('employees/edit/'.$s->id.'/'. $s->employeeid );?>"><span class='glyphicon glyphicon-edit' ></span>&nbsp;Edit Profile</a></li>
							<li><a href="<?= site_url('change_password/reset_employee/'.$s->id.'/'. $s->employeeid );?>"><span class='glyphicon glyphicon-refresh' ></span>&nbsp;Change Password</a></li>
						</ul>
					</div>
				</td>
			</tr>
			<?php 
			endforeach; ?>
			<tr>
				<td colspan="4" align='right'><b>Total Records</b></td>
				<td><span class='badge'><?=($total_rows) ? $total_rows : 0;?></span></td>
			</tr>
			<?php
		}
		else
		{
		?>
		<tr>
		<td colspan="1">
			Student Not Found
		</td>
		</tr>
		<?php
		}
		?>
		</tbody>
	</table>
	<?echo isset($links) ? $links : NULL;?>
	<?php
	}
	?>
	</div>
	
	<div class="clear"></div>
</div>