<?php
  $username = isset($user->username) ? $user->username : '';
  $email = isset($user->email) ? $user->email : '';
  $name = isset($user->name) ? $user->name : '';
  $expiration = isset($user->expiration) ? $user->expiration : '';
?>

<!-- Form Name -->
<legend>Fill Up User Details</legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-2 control-label" for="username">Username</label>  
  <div class="col-md-4">
  <input id="username" name="user[username]" type="text" value="<?=set_value('user[username]',$username)?>" placeholder="Username" class="form-control input-md" maxlength="100" required>
  <span class="text-danger"><?=form_error('user[username]')?></span>
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-2 control-label" for="email">Email</label>  
  <div class="col-md-4">
  <input id="email" name="user[email]" type="email" value="<?=set_value('user[email]',$email)?>" placeholder="Email" class="form-control input-md" maxlength="150" required>
  <span class="help-block">Valid email address.</span>  
  <span class="text-danger"><?=form_error('user[email]')?></span>
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-2 control-label" for="name">Name</label>  
  <div class="col-md-4">
  <input id="name" name="user[name]" type="name" value="<?=set_value('user[name]',$name)?>" placeholder="name" class="form-control input-md" maxlength="150" required>
  <span class="text-danger"><?=form_error('user[name]')?></span>
  </div>
</div>

<?if(!isset($user)):?>
  
  <!-- Text input-->
  <div class="form-group">
    <label class="col-md-2 control-label" for="password">Password</label>  
    <div class="col-md-4">
    <input id="password" name="password" type="text" placeholder="Password" class="form-control input-md"  pattern=".{6,}" title="6 characters minimum" required>
    <span class="text-danger"><?=form_error('password')?> </span>
    <input id="verify_password" name="verify_password" type="text" placeholder="Verify Password" class="form-control input-md" pattern=".{6,}" title="6 characters minimum" required>
    <span class="text-danger"><?=form_error('verify_password')?>  </span>
    </div>
  </div>
<?endif;?>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-2 control-label" for="expiration">Expiration (Y-m-d)</label>  
  <div class="col-md-4">
  <input id="expiration" name="user[expiration]" type="text" value="<?=set_value('user[expiration]',$expiration)?>" placeholder="expiration" class="form-control input-md">
  <span class="help-block">If no expiration account is unlimited.</span>  
  <span class="text-danger"><?=form_error('user[expiration]')?></span>
  </div>
</div>

<!-- DATETIMEPICKER -->
<link href="<?=site_url('assets/js/datetimepicker/jquery.datetimepicker.css')?>" rel="stylesheet">
<script src="<?php echo site_url('assets/js/datetimepicker/jquery.datetimepicker.js'); ?>"></script>

<script type="text/javascript">
  $(function(){
    jQuery('#expiration').datetimepicker({
      format:'Y-m-d H:i',
    });
  })
</script>