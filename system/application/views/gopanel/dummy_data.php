<?php
	echo panel_head(array(
			"panel_title" => "Insert Default Data for Tables :"
		));
?>
	<div class="table-responsive">
		<?=form_open('')?>
			<div class="table-responsive">
				<table>
					<tr class="info" >
						<td>Select Tables</td>
					</tr>

					<tr>
						<td>
							<ul class="list-group" >
								<li  class="list-group-item" >
									<table>
										<tr>
											<td>
												<input class="grp_sy" type="checkbox" name="table[]" value="academic_years">&nbsp; Academic / School Year
												<ul class="list-group">
													<li  class="list-group-item" style="border:0px !important;" ><input disabled class="sub_grp_sy" type="checkbox" name="table[]" value="semesters">&nbsp; Create Semester for the School Year</li>
												</ul>
											</td>
											<td>&nbsp; Year From : <input type="text" name="sy_from" maxlength="4"></td>
											<td>&nbsp; Year To : <input type="text" name="sy_to" maxlength="4"></td>
										</tr>
									</table>
								</li>

								<li  class="list-group-item" ><input type="checkbox" name="table[]" value="academic_years">&nbsp; Academic / School Year</li>
							</ul>
						</td>	
					</tr>

					<tr>
						<td><input type="submit" value="Update" name="update_tables"></td>
					</tr>
				</table>
			</div>
		</form>
	</div>
<? echo panel_tail(); ?>

<script type="text/javascript" >
	$('.grp_sy').on('change', function(){
		var state = $(this).is(':checked');
		if(state){
			$('.sub_grp_sy').removeAttr('disabled');
		}else{
			$('.sub_grp_sy').attr('disabled', true);
		}
	})
</script>