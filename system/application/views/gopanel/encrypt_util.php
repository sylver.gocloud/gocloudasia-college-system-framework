<?php echo panel_head2('Encrypt/Descrypt') ?>
	
	<?php if (isset($param_val)): ?>
		<h4>From URL : <span class="label label-info label-lg" >	<?php echo $param_val ?> </span></h4>
		<p><?php echo vp($param_con) ?></p>
	<?php endif; ?>

	<?php echo form_open('') ?>
		<h4>Enter Integer Encrypt (Seperate them by '/' e.g 1/2/3/4)</h4>
		<div class="table-responsive">
			<table class="table">
				<tr>
					<td><input type="text" name="ekeyword" value="<?php echo isset($ekeyword)?$ekeyword:'' ?>" placeHolder="Keyword"  ></td>
					<td><?php echo form_submit('encrypt','Encrypt'); ?></td>
					<td><P><?php echo isset($eresult) ? vp($eresult) : ""; ?></P></td>
				</tr>
			</table>
		</div>
		<h4>Enter Words Decrypt</h4>
		<div class="table-responsive">
			<table class="table">
				<tr>
					<td><input type="text" name="dkeyword" value="<?php echo isset($dkeyword)?$dkeyword:'' ?>" placeHolder="Keyword"  ></td>
					<td><?php echo form_submit('decrypt','Descrypt'); ?></td>
					<td><P><?php echo isset($dresult) ? vp($dresult) : ""; ?></P></td>
				</tr>
			</table>
		</div>
	<?php echo form_close(); ?>

<?php echo panel_tail2() ?>