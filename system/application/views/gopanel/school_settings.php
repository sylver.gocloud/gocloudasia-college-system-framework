<style>
	form p {
		border-top: 1px solid #ccc;
		padding: 5px 0 10px 0;
	}
	
	p {
		line-height: 16px;
	}
</style>

<div id="right">
		<form action="<?=site_url('gopanel/school_settings');?>"  enctype="multipart/form-data" method="POST" style="margin:0;padding:0;display:inline">
		
		  <p>
			<label for="setting_school_name">School Name: </label><br />
			<input id="setting_school_name" name="setting[school_name]" size="30" type="text" value="<?=$settings->school_name;?>" />
		  </p>
		  <p>
			<label for="setting_school_address">School Address: </label><br />
			<input id="setting_school_address" name="setting[school_address]" size="30" type="text" value="<?=$settings->school_address;?>" />
		  </p>
		  <p>
			<label for="setting_school_telephone">School Telephone: </label><br />
			<input id="setting_school_telephone" name="setting[school_telephone]" size="30" type="text" value="<?=$settings->school_telephone?>" />
		  </p>
		  <p>
			<label for="setting_email">Email: </label><br />
			<input id="setting_email" name="setting[email]" size="30" type="text" value="<?=$settings->email?>" />
		  </p>
		  <p>
			<label for="setting_president">President: </label><br />
			<input id="setting_president" name="setting[president]" size="30" type="text" value="<?=$settings->president;?>" />
		  </p>
		  
		  <p>
			<label for="setting_logo">School Logo: (JPG and PNG only) (Ideal Size 238 x 84)</label><br />
			<div style=''>
				<?if($settings->logo):?>
				<img src="<?=base_url().$settings->logo;?>"/>
				<?else:?>
				<img src="<?=base_url().'assets/images/no_photo.jpg';?>"/>
				<?endif;?>
			</div>
			<input type="file" name="userfile" size="20" />
		  </p>
		  
		  <p><input id="setting_submit" name="commit" type="submit" value="Save changes" /></p>
		  <input type="hidden" name="id" value="<?=$settings->id;?>" />
		</form>

		</div>
</div>
