<?if(isset($profile) && $profile):?>	
	<div class="well">
		<div class="row">
			<div class="col-md-1 hidden-xs">
				<div class="media">
		        <button tabindex="-1" type="button" class="btn btn-facebook btn-circle btn-circle-sm" readonly=""><i class="fa fa-user fa-3x"></i></button>
		    </div>
			</div>
			<div class="col-md-6">
				<div class="media-body">
		            <h3 class="media-heading text-success"><?=capital_fwords($profile->name)?></h3>
		    				<p>
		    					<span class="label label-facebook label-lg tp" title="Login"><?=$profile->employeeid?></span> 
		    					<span class="label label-warning label-lg tp" title="Department"><?=$profile->department?></span>
		    				</p>
		            <p>
		                <a href="<?=site_url('messages')?>" class="btn btn-xs btn-default"><span class="glyphicon glyphicon-comment"></span> Messages <span class="badge badge-success menu_unread_badge"></span></a>
		                <!-- <a href="#" class="btn btn-xs btn-default"><span class="glyphicon glyphicon-heart"></span> Favorite</a>
		                <a href="#" class="btn btn-xs btn-default"><span class="glyphicon glyphicon-ban-circle"></span> Unfollow</a> -->
		            </p>
		        </div>
			</div>
		</div>
	</div>
<?endif;?>
<h5><div class='alert alert-success'></i><i class="fa fa-hand-o-left"></i>&nbsp; Please select a menu to your left.<span style='float:right;' class='glyphicon glyphicon-remove sys_m_close'></span></div></h5>
<p></p>

<?if(isset($announcements) && $announcements):?>
	<div class="row">
		<div class="col-md-12">
			<?=panel_head2('Announcements',false,'info')?>
				<?@$this->load->view('announcements/_carousel')?>
			<?=panel_tail2();?>
		</div>
	</div>
<?endif;?>

<?if(isset($events) && $events):?>
	<div class="row">
		<div class="col-md-12">
			<?=panel_head2('Incoming Events',false,'danger')?>
				<?@$this->load->view('events/_carousel')?>
			<?=panel_tail2();?>
		</div>
	</div>
<?endif;?>
