<script type='text/javascript'>
  
	function validate()
	{
		var ctr = 0;
		var msg = "<ul>";
		var xfocus = "";
		$('.not_blank').css('border-color','gray');
		$('.not_blank').each(function(){
			var xval = $(this).val().trim();
			var xid = $(this).attr('id');
			if(xval == ""){
				ctr++;
				$(this).css('border-color','red');
				if(ctr == 1) { xfocus = $(this); }
				var label = $('label[for='+xid+']').text();
				
				msg += "<li>"
				+label+" should not be blank</li>";
			}
		});
		
		msg += "<ul>";
		
		if(ctr > 0)
		{
			xfocus.focus();
			$('#alertModal_Label').text('Required Fields'); //SET MODAL TITLE
			$('#alertModal_Body').html(msg); //SET MODAL CONTENT
			$('#alertModal').modal('show'); //SHOW MODAL
			
			return false;
		}
		else
		{
			return true; 
		}
	}
</script>
  <p>
    <label for="library_bar_code">Bar code</label><br />
    <input id="library_bar_code" name="library[bar_code]" size="30" type="text" value='<?=isset($libraries->bar_code)?$libraries->bar_code:""?>' />
  </p>
  <p>
    <label for="library_year">Year Published *</label><br />
    <input id="library_year" name="library[year]" size="30" type="text" class='not_blank' value='<?=isset($libraries->year)?$libraries->year:""?>' />
  </p>
  <p>
    <label for="library_title">Title *</label><br />
    <input id="library_title" name="library[title]" size="30" type="text" class='not_blank' value='<?=isset($libraries->title)?$libraries->title:""?>' />
  </p>
  <p>
    <label for="library_author">Author *</label><br />
    <input id="library_author" name="library[author]" size="30" type="text" class='not_blank' value='<?=isset($libraries->author)?$libraries->author:""?>' />
  </p>
  <p>
    <label for="library_date_published">Date Published</label><br />
    <input id="library_date_published" name="library[date_published]" size="30" type="text" class='date_pick' value='<?=isset($libraries->date_published)?date('Y-m-d', strtotime($libraries->date_published)):""?>' />
  </p>
  <p>
    <label for="library_section">Section</label><br />
    <input id="library_section" name="library[section]" size="30" type="text" value='<?=isset($libraries->section)?$libraries->section:""?>' />
  </p>
  <p>
    <label for="librarycategory_id">Category</label><br />
	<?=librarycategory_dropdown('library[librarycategory_id]');?>
  </p>
   <p>
    <label for="library_status">Status</label><br />
	<?=librarystatus_dropdown('library[status]');?>
  </p>
  
  

