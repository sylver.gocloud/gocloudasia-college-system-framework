<div class="well">
	<?echo form_open('','class="form-inline" role="form"');?>
	<div class="form-group">
		Search In
	</div>
	 <div class="form-group">
		<?
			$xfields = array(
				"libraries.bar_code" => "Bar Code",
				"libraries.title" => "Title",
				"libraries.author" => "Author",
				"libraries.year" => "Year Published",
				"librarycategory.category" => "Category",
				"libraries.status" => "Status",
			);
			echo form_dropdown('fields', $xfields, isset($fields)?$fields:'bar_code');
		?>
	</div>
	<div class="form-group">
		<label class="sr-only" for="author">Author</label>
		<input type='text' name='keyword' value="<?=isset($keyword)?$keyword:''?>" placeHolder="Keyword" />
	</div>
	<?
		echo form_submit('submit','Search');		?>
		<a class='btn btn-default btn-sm' href='<?=base_url()?>books/create'><span class = 'glyphicon glyphicon-plus'></span>&nbsp;  Create New Book</a>
		<?echo form_close();?>
</div>
<table>
	  <tr>
		<th>Title</th>
		<th>Author</th>
		<th>Year</th>
		<th>Bar Code</th>
		<th>Category</th>
		<th>Status</th>
		<th>Action</th>
	  </tr>
	<? $num = 0 ?>
	<?php if(empty($libraries) == FALSE):?>
		<? foreach($libraries as $obj): ?>
			<tr>
			  
				<td><?= $obj->title;?></td>
				<td><?= $obj->author;?></td>
				<td><?= $obj->year ?></td>
				<td><?= $obj->bar_code ?></td>
				<td><?= $obj->category ?></td>
				<?
					switch(strtoupper($obj->status)){
						case "AVAILABLE":
							$class = "label label-success";
						break;
						case "BORROWED":
							$class = "label label-warning";
						break;
						case "LOST":
							$class = "label label-danger";
						break;
						default:
							$class = "label label-primary";
						break;
					}
				?>
				<td><div class="<?=$class?>"><?= $obj->status ?></div></td>
				<td>
					
					<div class="btn-group btn-group-sm">
					  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
						<i class="fa fa-caret-square-o-down"></i> <span class="caret"></span>
					  </button>
					  <ul class="dropdown-menu" role="menu">
						<li><a href="<?=base_url('books/create_library_card/'.$obj->id)?>"><span class = 'glyphicon glyphicon-credit-card'></span>&nbsp;  Create Library Card</a></li>
						
						<li><a href="<?=base_url('books/show_library_card/'.$obj->id)?>"><span class='glyphicon glyphicon-folder-open'></span>&nbsp;  View Library Card</a></li>
						
						<li><a href="<?=base_url('books/edit/'.$obj->id)?>"><span class='glyphicon glyphicon-pencil' ></span>&nbsp;   Edit</a></li>
						
						<li class="divider"></li>
						
						<li><a title="Are you sure to delete this record?" class='confirm' href="<?=base_url('books/destroy/'.$obj->id)?>"><span class='glyphicon glyphicon-trash' ></span>&nbsp;   Destroy</a></li>
						
					  </ul>
					</div>
				</td>
			</tr>
		<? endforeach;?>
		<tr>
			<td colspan=6 class="text-right">Total No. of Records</td>
			<td><div class='badge'><?=$total_rows;?></div></td>
		</tr>
	<?php else:?>
		<tr>
			<td colspan=7 class="text-center">----NO data to show---</td>
		</tr>	
	<?php endif;?>
</table>
<?= $links;?>