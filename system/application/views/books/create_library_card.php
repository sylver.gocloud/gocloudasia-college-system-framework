<script type='text/javascript'>
  
	function validate()
	{
		var ctr = 0;
		var msg = "<ul>";
		var xfocus = "";
		$('.not_blank').css('border-color','gray');
		$('.not_blank').each(function(){
			var xval = $(this).val().trim();
			var xid = $(this).attr('id');
			if(xval == ""){
				ctr++;
				$(this).css('border-color','red');
				if(ctr == 1) { xfocus = $(this); }
				var label = $('label[for='+xid+']').text();
				
				msg += "<li>"
				+label+" should not be blank</li>";
			}
		});
		
		msg += "<ul>";
		
		if(ctr > 0)
		{
			xfocus.focus();
			$('#alertModal_Label').text('Required Fields'); //SET MODAL TITLE
			$('#alertModal_Body').html(msg); //SET MODAL CONTENT
			$('#alertModal').modal('show'); //SHOW MODAL
			
			return false;
		}
		else
		{
			return true; 
		}
	}
</script>
<?
	switch(strtoupper($book->status)){
		case "AVAILABLE":
			$style = "success";
		break;
		case "LOST":
			$style="danger";
		break;
		case "BORROWED":
			$style="warning";
		break;
		default:
			$style='default';
		break;
	}
?>
<h4><div class='label label-default'>Bar Code</div>:<div class='label label-info'><?=$book->bar_code;?></div>
<?
	switch(strtoupper($book->status)){
		case "AVAILABLE":
			$style = "success";
		break;
		case "LOST":
			$style="danger";
		break;
		case "BORROWED":
			$style="warning";
		break;
		default:
			$style='default';
		break;
	}
?>
| <div class='label label-default'>Status</div>:<div class='label label-<?=$style?>'><?=$book->status;?></div>
</h4>
<div class="well">
	<label><b>Title : <?=$book->title;?></b></label><br/>
	<label><b>Author : <?=$book->author;?></b></label><br/>
	<label><b>Year Publish : <?=$book->year;?></b></label><br/>
	<label><b>Section : <?=$book->section;?></b></label><br/>
</div>
<?=form_open('','onsubmit="return validate()"')?>
  <div class="form-group">
    <label for="date">Date</label>
    <input type="text" class="form-control date_pick not_blank" name='library[date]' id="date" placeholder="Date">
  </div>
  <br/>
  <button type="submit" class="btn btn-default btn-sm">Submit</button> |
  <a class='btn btn-default btn-sm' href="<?php echo base_url(); ?>books">Back to list of Books</a>
</form>