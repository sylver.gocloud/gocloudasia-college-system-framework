	
 <script>
  $(function() {
    $( "#advance_search" ).dialog({
      autoOpen: false,
      height: 300,
      width: 550,
      modal: true,
      title: "Advance Search",
    });
  });
  
  function validate()
	{
		var ctr = 0;
		var msg = "<ul>";
		var xfocus = "";
		$('.not_blank').css('border-color','gray');
		$('.not_blank').each(function(){
			var xval = $(this).val().trim();
			var xid = $(this).attr('id');
			if(xval == ""){
				ctr++;
				$(this).css('border-color','red');
				if(ctr == 1) { xfocus = $(this); }
				var label = $('label[for='+xid+']').text();
				
				msg += "<li>"
				+label+" should not be blank</li>";
			}
		});
		
		msg += "<ul>";
		
		if(ctr > 0)
		{
			xfocus.focus();
			$('#alertModal_Label').text('Required Fields'); //SET MODAL TITLE
			$('#alertModal_Body').html(msg); //SET MODAL CONTENT
			$('#alertModal').modal('show'); //SHOW MODAL
			
			return false;
		}
		else
		{
			return true; 
		}
	}
	
	function Select(id)
	{
		$('#users_id').val(id);
		$('#advance_search').dialog('close');
	}
	
	function Search(page)
	{
		var page = page == null ? 0 : page;
		$.blockUI({ message: "" }); 
		var xreturn = false;
		var controller = 'ajax';
		var base_url = '<?php echo base_url(); ?>'; 
		var xdata = $('#advance_search *').serialize();
		
		$.ajax({
			'url' : base_url + '' + controller + '/search_users/'+page,
			'type' : 'POST', 
			'async': false,
			'data' : xdata,
			'dataType' : 'json',
			'success' : function(data){ 
				// console.log(data);
				$('#links').html(data.links);
				
				//PREVENT LINKS FROM REDIRECTING
				$('#links li a').click(function(event){
					event.preventDefault();
					var href = $(this).attr("href");
					next_page = (href.substr(href.lastIndexOf('/') + 1));
					Search(next_page);
				})
				$('#tbody').html('');
				
				var tr = "";
				
				if(data.total_rows > 0){
					for(var x = 0 ; x <= data.count; x++){
						if(data.users[x] != null){
						var xid = data.users[x].id;
						var xlogin = data.users[x].login;
						var xname = data.users[x].name;
						// console.log(xid);
						// console.log(data.users[x]);
						tr += "<tr>";
						tr += "<td>"+xlogin+"</td>";
						tr += "<td>"+xname+"</td>";
						tr += "<td><a href='javascript:;'><div class='badge' onclick=\"Select('"+xid+"')\">Select</div></a></td>";
						tr += "</tr>";
						}
					}
					tr += "<tr><td colspan='2'>No of Records</td><td><span class='badge'>"+data.total_rows+"</span></td></tr>";
					
				}else{
					tr = "<tr><td colspan='3'>No records found.</td></tr>";
				}
				
				$('#tbody').html(tr);
			}
		})
		.done(function() {
			  $.unblockUI(); 
		  });
		
	}
  </script>	
<div class="well">
	<label><b>Title : <?=$book->title;?></b></label><br/>
	<label><b>Author : <?=$book->author;?></b></label><br/>
	<label><b>Year Publish : <?=$book->year;?></b></label><br/>
	<label><b>Section : <?=$book->section;?></b></label><br/>
</div>
<div class="form-group">
		<button type="button" class="btn btn-default btn-sm btn-xs" onclick='$("#advance_search").dialog("open");'>Advance Search</button>
	</div><br/>
	<div class="form-group">
		<label for="users_id">Borrowers Name</label><br />
		<?=users_dropdown('borrower[users_id]',isset($borrower->users_id)?$borrower->users_id:'', 'Choose Borrowers', 'class="not_blank" id="users_id"');?>
	</div><br/>
	<div class="form-group">
		  <label for="borrower_date_borrowed">Date Borrowed</label><br />
		 <input class="date_pick not_blank" id="borrower_date_borrowed" name="borrower[date_borrowed]" size="100" type="text" value="<?=isset($borrower->date_borrowed)?$borrower->date_borrowed:""?>" />
	</div><br/>
	<div class="form-group">
		<label for="borrower_date_to_be_returned">Date to be Returned</label><br />
		<input class="date_pick" id="borrower_date_to_be_returned" name="borrower[date_to_be_returned]" size="100" type="text" value="<?=isset($borrower->date_to_be_returned)?$borrower->date_to_be_returned:""?>" />
	</div><br/>
	<?
		//ONLY SHOW THE DATE RETURN WHEN EDITING
		$action = strtolower($this->uri->segment(2));
		
		if($action != "create_borrower")
		{
	?>
	<div class="form-group">
		<label for="borrower_date_return">Date Returned</label><label class='label label-warning'>Note : Having date return signifies that the book will be returned. Removing it makes the book unavailable again.</label><br />
		<input class="date_pick" id="borrower_date_return" name="borrower[date_return]" size="100" type="text" value="<?=isset($borrower->date_return)?$borrower->date_return:''?>" />
	</div><br/>
		<?}?>
	<br/>
	<div id='advance_search'>
		<table>
		  <tr>
			<td><input type="text" class="form-control" id="borrower_login" name="borrower_login" placeholder="ID/Login" style='width:150px'></td>
			<td><input type="text" class="form-control" id="borrower_name" name="borrower_name" placeholder="Name" style='width:150px'></td>
			<td><button type="button" class="btn btn-default btn-sm" onclick='Search()'>Search</button></td>
		  </tr>
		</table>
			<table id='results' style='font-size:7pt'>
			  <tr>
				<th>ID/Login</th>
				<th>NAME</th>
				<th>ACTION</th>
			  </tr>
				<tbody id='tbody'>
				</tbody>
			</table>
			<div id='links' style='font-size:7pt'></div>
	</div>
 
  

