<div id="right">
	<div id="right_top" >
	<p id="right_title">All Payment Reports</p>
		  <!--<p id="back"></p>-->
	</div>
	<div id="right_bottom">
		<table>
			<thead>
				<tr>
					<td>Start Date</td>
					<td>End Date</td>
					<td>&nbsp;</td>
				</tr>
			</thead>
				<tbody>
					<? if(!empty($group_payments)):?>
						<? foreach($group_payments as $gp):?>
							<tr class="profile_box">
								<td><?=date('M d, Y',strtotime($gp->start_date));?></td>
								<td><?=date('M d, Y',strtotime($gp->end_date));?></td>
								<? $l = _se($gp->id)?>
								<td><a href="<?=site_url('finance/show_payment_data/?id='.$l->link.'&di='.$l->hash.'&stat=new');?>">Show</a> | <a href="<?=site_url('finance/del_group/'.$gp->id);?>" class="confirm-auto" title="Are you sure you want to delete Group?">Delete</a></td>
							</tr>
						<? endforeach;?>
					<? endif;?>
				</tbody>
		</table>
		<a href="<?=site_url('finance/add_payment_report');?>">New Group Record</a>
	</div>
</div>



