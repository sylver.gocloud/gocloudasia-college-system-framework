<?
$this->load->helper(array('url_encrypt'));
$link = _se(sha1('print_data'));
?>

<div id="right">
	<div id="right_top" >
	<p id="right_title">Student Charges</p>
		  <!--<p id="back"></p>-->
	</div>
	<pre>
	<div id="right_bottom">
		  <a href="<?=site_url('finance/generate_student_charges/?verify='.$link->link.'&code='.$link->hash);?>" class="confirm" title="Continue Download?">Download Student Charges Excel</a><br />
		<table>
			<thead>
				<tr>
					<td>Fullname</td>
					<td>Total Units</td>
					<td>Tuition Fee / Unit</td>
					<td>Lab Fee / Unit</td>
					<td>Total Misc Fee</td>
					<td>Total Other Fee</td>
					<td>Tuition Fee</td>
					<td>Lab Fee</td>
					<td>Less NSTP Fee</td>
					<td>Additional Charge</td>
					<td>Total Charge</td>
				</tr>
			</thead>
				<tbody>
					<? if(!empty($results)):?>
						<? foreach($results as $gp):?>
							<tr>
								<td><?=$gp->fullname;?></td>
								<td><?=m($gp->total_units);?></td>
								<td><?=m($gp->tuition_fee_per_unit);?></td>
								<td><?=m($gp->lab_fee_per_unit);?></td>
								<td><?=m($gp->total_misc_fee);?></td>
								<td><?=m($gp->total_other_fee);?></td>
								<td><?=m($gp->tuition_fee);?></td>
								<td><?=m($gp->lab_fee);?></td>
								<td><?=m($gp->less_nstp);?></td>
								<td><?=m($gp->additional_charge,1);?></td>
								<td><?=m($gp->total_charge,1);?></td>
							</tr>
						<? endforeach;?>
					<? endif;?>
				</tbody>
		</table>
	
	<?php echo $pagination_links;?>
	</div>
</div>