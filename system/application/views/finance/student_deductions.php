<?php
$link = sha1('new');
$search_message = '<div class="alert alert-success"><strong>'.htmlentities($search_strings).'</i>';
$search_message .='<br><a href="'.site_url("finance/student_deductions/?new=".$link).'">Clear Search History</a></div>';
?>

<div id="right">
	<div id="right_top" >
	<p id="right_title">Student Deductions / Search</p>
		  <!--<p id="back"></p>-->
	</div>
	<div id="right_bottom">
			<?= validation_errors() == '' ? NULL : '<div class="alert alert-danger">'.validation_errors().'</div>';?>
			<?= isset($system_message) ? $system_message : NULL;?>
			
			<form action="<?=site_url('finance/student_deductions');?>" method="post">
				<p>
					<label for="group_payment_start_date">NAME</label>
					<input name="name" size="30" type="text" /><br />
					<label for="group_payment_end_date">Student ID</label>
					<input name="student_id" size="30" type="text" />
					<input name="form_token" type="hidden" value="<?=$form_token;?>"/><br />
					<input name="search" type="submit" value="Search" />
				</p>
			</form>
		<div>
		<?=isset($search_strings) ? $search_message : NULL;?>
			<table>
				<thead>
					<tr>
						<td>Fullname</td>
						<td>Date Created</td>
						<td>Amount Deducted</td>
						<td>Remarks</td>
					</tr>
				</thead>
				<tbody>
					<? if(!empty($results)):?>
						<?foreach($results as $r):?>
						
						<tr>
							<td><?=$r->fullname;?></td>
							<td><?=$r->date_created;?></td>
							<td><?=$r->amount;?></td>
							<td><?=$r->remarks;?></td>
						</tr>
						<?endforeach;?>
					<? else:?>
						<div class="notice">No data found</div>
					<?endif;?>
				</tbody>
			</table>
		</div>
		<?=isset($pagination_links) ? $pagination_links : NULL;?>
	</div>
</div>
