<ul class="nav nav-tabs">
	  
	<? if(strtolower($this->session->userdata['userType']) == "hrd" || strtolower($this->session->userdata['userType']) == "admin") :?>
		<li <?= ($this->router->class == "employees" && $this->router->method == 'edit') ? "class='active'" : '' ?>>
		<a href="<?php echo base_url('employees/edit/'.$id.'/'.$login); ?>">Profile</a></li>
	<? endif;?>
	
	<?if(isset($personal_info) && $personal_info):?>
	
	<? if(strtolower($personal_info->role) == "teacher") :?>

		<!-- <li <?= ($this->router->class == "employees" && $this->router->method == 'add_assign_course') ? "class='active'" : '' ?>>
	  	<a href="<?php echo base_url('employees/add_assign_course/'.$id.'/'.$login); ?>">Assign Course</a></li> -->
	<?endif;?>
	
	<?endif;?>
	  
	<li <?= ($this->router->class == "change_password" && $this->router->method == 'reset_employee') ? "class='active'" : '' ?>>
	  <a href="<?php echo base_url('change_password/reset_employee/'.$id.'/'.$login); ?>">Change Password</a></li>

	<li>
	  <a href="<?php echo base_url('employees/index'); ?>"><i class="fa fa-arrow-left"></i>&nbsp; Back to Search</a></li>
	
</ul>

<br/>