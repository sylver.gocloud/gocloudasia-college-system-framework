<?
	$this->load->view('employees/_tab');
	$par = "edit";
?>

<?=panel_head2('Activate / De-activate')?>
	<?=form_open()?>
		<p></p>
		<h4>
			<strong>&nbsp; Is User Active ? </strong><div class="label label-info"><?=$personal_info->is_activated == 1 ? "Yes":"No"?></div>
			
			<?if($personal_info->is_activated == 1):?>
				<a href="<?=site_url('users/activate/'.$personal_info->user_id."/0/{$personal_info->id}/edit_profile")?>" class="btn btn-sm btn-danger confirm"><i class="fa fa-lock"></i>&nbsp; De-activate User</a>
			<?else:?>
				<a href="<?=site_url('users/activate/'.$personal_info->user_id."/1/{$personal_info->id}/edit_profile")?>" class="btn btn-sm btn-success confirm"><i class="fa fa-unlock"></i>&nbsp; Activate User</a>
			<?endif;?>
		</h4>
	<?=form_close()?>
<?=panel_tail2()?>

<?echo form_open('','onsubmit="return validate()"');?>

<?$this->load->view('employees/_form')?>

<?echo form_submit('','Save Changes');?>&nbsp;
<a href="<?=site_url('employees')?>" class="btn btn-sm btn-default"> Back to List</a>
<?echo form_close();?>