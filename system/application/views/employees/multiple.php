<?php echo form_open('') ?>
	<div class="table-responsive">
		<table class="table">
			<thead>
					<tr class="gray">
						<th>#</th>
						<th>Employee ID</th>
						<th>Department</th>
						<th>Last Name</th>
						<th>Middle Name</th>
						<th>First Name</th>
						<th>Date of Birth</th>
						<th>Sex</th>
						<th>Civil Status</th>
					</tr>
			</thead>
			<?
				$marital_status = array(
							'Single' => 'Single',
							'Married' => 'Married',
							'Annulled' => 'Annulled',
							'Widowed' => 'Widowed',
							'Separated' => 'Separated',
							'Other' => 'Other',
						);
				$g = array(
							'Male' => 'Male',
							'Female' => 'Female',
						);
			?>
			<?for($i = 1; $i<=20;$i++):?>
				<tr>
					<td><?php echo $i; ?>.</td>
					<td><input placeHolder='Login ID' class='grid' id="employee_login" name="user[<?=$i?>][employees_attributes][personal_info][employeeid]" maxlength="50" type="text" /></td>
					<td><?=departments_dropdown('user['.$i.'][employees_attributes][personal_info][role]','','class="grid"','Choose Role');?></td>
					<td><input placeHolder="Lastname" name="user[<?=$i?>][employees_attributes][personal_info][last_name]" class='grid' maxlength="100" type="text" /></td>
					<td><input placeHolder="Firstname" name="user[<?=$i?>][employees_attributes][personal_info][first_name]" class='grid' maxlength="100" type="text" /></td>
					<td><input placeHolder="Middlename" name="user[<?=$i?>][employees_attributes][personal_info][middle_name]" class='grid' maxlength="100" type="text" /></td>
					<td><input name="user[<?=$i?>][employees_attributes][personal_info][dob]" class="date_pick grid" maxlength="10" type="text" /></td>
					<td><?php echo form_dropdown('user['.$i.'][employees_attributes][personal_info][gender]',$g,'Male'); ?></td>
					<td><?php echo form_dropdown('user['.$i.'][employees_attributes][personal_info][martial_status]',$marital_status); ?></td>
				</tr>
			<?endfor?>
		</table>
	</div>
	<button type='submit' class='btn btn-primary' name='insert' value='insert'><i class="fa fa-save"></i>&nbsp; Insert</button>
	<a href="<?=site_url('employees')?>" class="btn btn-default">Cancel</a>
<?php echo form_close(); ?>