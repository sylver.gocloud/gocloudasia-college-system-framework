<style>
	div#header{
		font-size:14pt;
		font-weight: bold;
	}
</style>
<script type='text/javascript'>
	$( document ).ready(function() {
		$('.ui-widget-overlay').live("click",function(){
            $("#message").dialog("close");
        });  
		
		$('.not_blank').each(function(){
		
			var xdata = $(this).val().trim();
			
			if(xdata == "")
			{
				$(this).css('border-color','red');
				$(this).attr('placeHolder','Required');
			}
			
			$(this).focusout(function(){
				
				var xdata = $(this).val().trim();
				
				if(xdata != ""){
					$(this).css('border-color','gray');
				}else{
					
					$(this).css('border-color','red');
					$(this).attr('placeHolder','Required');
				}
			});
		})
	});
  
	function validate()
	{
		run_pleasewait();
		var ctr = 0;
		var msg = "<ul>";
		var xfocus = "";
		$('.not_blank').css('border-color','gray');
		$('.not_blank').each(function(){
			var xval = $(this).val().trim();
			if(xval == ""){
				ctr++;
				$(this).css('border-color','red');
				if(ctr == 1) { xfocus = $(this); }
				var label = $(this).parent().prev().text();
				
				msg += "<li>"
				+label+" should not be blank</li>";
			}
		});
		
		msg += "<ul>";
		
		if(ctr > 0)
		{
			close_pleasewait();
			xfocus.focus();
			$('#alertModal_Label').text('Required Fields'); //SET MODAL TITLE
			$('#alertModal_Body').html(msg); //SET MODAL CONTENT
			$('#alertModal').modal('show'); //SHOW MODAL
			
			// $('#message_list').html(msg);
			// $( "#message" ).dialog({
			  // height : 300,
			  // modal : true,
			  // buttons: {
				// Close: function() {
				  // $( this ).dialog( "close" );
				// }
			  // }
			// });
			// $.blockUI({ message: $('#message_list') }); 
			// $.blockUI.defaults.applyPlatformOpacityRules = false;
			// $('.blockOverlay').attr('title','Click to unblock').click($.unblockUI);
			return false;
		}
		else
		{
			var par = "<?=isset($personal_info->id) ? "edit" : "create";?>";
			var id = "<?=isset($personal_info->id) ? $personal_info->id : "";?>";
			
			$.blockUI({ message: "<h3>Please wait...</h3>" }); 
			var xreturn = false;
			var controller = 'ajax';
			var base_url = '<?php echo base_url(); ?>'; 
			$.ajax({
				'url' : base_url + '' + controller + '/check_employee_duplicate',
				'type' : 'POST', 
				'async': false,
				'data' : {
							'employeeid' : $('#employee_login').val(),
							'par' : par,
							'id' :  id
							},
				'dataType' : 'json',
				'success' : function(data){ 
					
					if(data.isduplicate)
					{
						close_pleasewait();
						var msg = "Employee ID is already in used.</h3>";
						
						custom_modal('Duplicate',msg);
						
						
						xreturn = false;
					}
					else
					{
						xreturn = true;
					}
				}
			})
			.done(function() {
				  $.unblockUI(); 
			  });
			 
			return xreturn; 
		}
	}
	
	function CloseBlock(){
		$.unblockUI();
	}
</script>
		
	<?=panel_head2('I. Personal Information');?>

		<table>
		   <tr>
			  <th>Employee ID *</th>
			  <td>
			  	<?if(isset($personal_info->employeeid)):?>
						<strong><?=$personal_info->employeeid?></strong>	  		
			  	<?else:?>
			  		<input class='not_blank' id="employee_login" name="user[][employees_attributes][personal_info][employeeid]" size="30" type="text"  value='<?=isset($personal_info->employeeid) ? $personal_info->employeeid : "" ?>' />
			  	<?endif;?>
			  </td>
		   </tr>
		   <tr>
			  <th>Deparment *</th>
			  <td>
				<?php
					$roles = array(
						'Employee' => 'Employee',
						'Teacher' => 'Teacher',
						'Registrar' => 'Registrar',
						'Finance' => 'Finance',
						'HRD' => 'HRD',
						'Librarian' => 'Librarian',
						'Dean' => 'Dean',
						'Custodian' => 'Custodian',
						'Cashier' => 'Cashier',
						'Guidance' => 'Guidance',
						'Assistant_To_The_President' => 'Assistant To The President',
					);
					echo departments_dropdown('user[employees_attributes][personal_info][role]',isset($personal_info->role) ? $personal_info->role : '','class="not_blank"','Choose Role');
					// echo form_dropdown('user[employees_attributes][personal_info][role]',$roles, isset($personal_info->role) ? $personal_info->role : '','class="not_blank"','');
				?>
			  </td>
		   </tr>
		   <tr>
			  <th>Join Date *</th>
			  <td><input class="date_pick not_blank" id="user_employees_attributes_0_joining_date" name="user[employees_attributes][personal_info][joining_date]" size="30" type="text" value='<?=isset($personal_info->joining_date) ? $personal_info->joining_date : "" ?>' /></td>
		   </tr>
		   <tr>
			  <th>Last Name *</th>
			  <td><input class="not_blank" id="user_employees_attributes_0_last_name" name="user[employees_attributes][personal_info][last_name]" size="30" type="text" value='<?=isset($personal_info->last_name) ? $personal_info->last_name : "" ?>' /></td>
		   </tr>
		   <tr>
			  <th>First Name *</th>
			  <td><input class="not_blank" id="user_employees_attributes_0_first_name" name="user[employees_attributes][personal_info][first_name]" size="30" type="text" value='<?=isset($personal_info->first_name) ? $personal_info->first_name : "" ?>' /></td>
		   </tr>
		   <tr>
			  <th>Middle Name *</th>
			  <td><input class="not_blank" id="user_employees_attributes_0_middle_name" name="user[employees_attributes][personal_info][middle_name]" size="30" type="text" value='<?=isset($personal_info->middle_name) ? $personal_info->middle_name : "" ?>' /></td>
		   </tr>
		   <tr>
			  <th>Date of Birth *</th>
			  <td><input class="not_blank date_pick" id="user_employees_attributes_0_dob" name="user[employees_attributes][personal_info][dob]" size="30" type="text" value='<?=isset($personal_info->dob) ? $personal_info->dob : "" ?>' /></td>
		   </tr>
		   <tr>
			  <th>Place of Birth</th>
			  <td><input id="user_employees_attributes_0_place_of_birth" name="user[employees_attributes][personal_info][place_of_birth]" size="50" type="text" value='<?=isset($personal_info->place_of_birth) ? $personal_info->place_of_birth : "" ?>' /></td>
		   </tr>
		   <tr>
			  <th>Sex</th>
			  <td>
				<?php
					if(isset($personal_info->id)){
						$xmale = isset($personal_info->gender) ? ($personal_info->gender == 'Male' ? 'checked' : '') : '';
						$xfmale = isset($personal_info->gender) ? ($personal_info->gender == 'Female' ? 'checked' : '') : '';
					}
					else{
						$xmale = "checked";
						$xfmale = "";
					}
				?>
				 <input id="user_employees_attributes_0_gender_male" name="user[employees_attributes][personal_info][gender]" type="radio" value="Male" <?=$xmale;?> /> Male 
				 <input id="user_employees_attributes_0_gender_female" name="user[employees_attributes][personal_info][gender]" type="radio" value="Female" <?=$xfmale;?> /> Female 
			  </td>
		   </tr>
		   <tr>
			  <th>Civil Status *</th>
			  <td>
				<?php
					$marital_status = array(
						'Single' => 'Single',
						'Married' => 'Married',
						'Annulled' => 'Annulled',
						'Widowed' => 'Widowed',
						'Separated' => 'Separated',
						'Other' => 'Other',
					);
					
					echo form_dropdown('user[employees_attributes][personal_info][martial_status]',$marital_status, isset($personal_info->martial_status) ? $personal_info->martial_status : '','class="not_blank"');
				?>
			  </td>
		   </tr>
		</table>

		<table>
		   <tr>
			  <th>Citizenship</th>
			  <td><input id="user_employees_attributes_0_citizenship" name="user[employees_attributes][personal_info][citizenship]" size="30" type="text" value='<?=isset($personal_info->citizenship) ? $personal_info->citizenship : "" ?>' /></td>
			  <th>Residential Address</th>
			  <td><input id="user_employees_attributes_0_residential_address" name="user[employees_attributes][personal_info][residential_address]" size="30" type="text" value='<?=isset($personal_info->residential_address) ? $personal_info->residential_address : "" ?>' /></td>
		   </tr>
		   <tr>
			  <th>Religion</th>
			  <td><input id="user_employees_attributes_0_religion" name="user[employees_attributes][personal_info][religion]" size="30" type="text" value='<?=isset($personal_info->religion) ? $personal_info->religion : "" ?>' /></td>
			  <th>Permanent Address</th>
			  <td><input id="user_employees_attributes_0_permanent_address" name="user[employees_attributes][personal_info][permanent_address]" size="30" type="text" value='<?=isset($personal_info->permanent_address) ? $personal_info->permanent_address : "" ?>' /></td>
		   </tr>
		   <tr>
			  <th>SSS ID No.</th>
			  <td><input id="user_employees_attributes_0_sss_id_no" name="user[employees_attributes][personal_info][sss_id_no]" size="30" type="text" value='<?=isset($personal_info->sss_id_no) ? $personal_info->sss_id_no : "" ?>' /></td>
			  <th>E-mail (if any)</th>
			  <td><input id="user_employees_attributes_0_email" name="user[employees_attributes][personal_info][email]" size="30" type="text" /></td>
		   </tr>
		   <tr>
			  <th>Pag-ibig No.</th>
			  <td><input id="user_employees_attributes_0_pagibig_no" name="user[employees_attributes][personal_info][pagibig_no]" size="30" type="text" value='<?=isset($personal_info->pagibig_no) ? $personal_info->pagibig_no : "" ?>' /></td>
			  <th>Cellphone No. (if any)</th>
			  <td><input id="user_employees_attributes_0_cellphone" name="user[employees_attributes][personal_info][cellphone]" size="30" type="text" value='<?=isset($personal_info->cellphone) ? $personal_info->cellphone : "" ?>' /></td>
		   </tr>
		   <tr>
			  <th>Philhealth No.</th>
			  <td><input id="user_employees_attributes_0_philhealth_no" name="user[employees_attributes][personal_info][philhealth_no]" size="30" type="text" value='<?=isset($personal_info->philhealth_no) ? $personal_info->philhealth_no : "" ?>' /></td>
			  <th>TIN</th>
			  <td><input id="user_employees_attributes_0_tin" name="user[employees_attributes][personal_info][tin]" size="30" type="text"  value='<?=isset($personal_info->tin) ? $personal_info->tin : "" ?>'/></td>
		   </tr>
		</table>
	
	<?=panel_tail2();?>
	
	<?=panel_head2('II. FAMILY BACKGROUND');?>

		<p></p>
		<table>
		   <tr>
			  <th>&nbsp;</th>
			  <th>Spouse</th>
			  <th>Name of Child</th>
			  <th>Date of Birth</th>
		   </tr>
		   <tr>
			  <th>Last Name</th>
			  <td><input id="user_employees_attributes_0_spouse_last_name" name="user[employees_attributes][personal_info][spouse_last_name]" size="30" type="text" value='<?=isset($personal_info->spouse_last_name) ? $personal_info->spouse_last_name : "" ?>' /></td>
			  <td><input id="user_employees_attributes_0_child1" name="user[employees_attributes][personal_info][child1]" size="30" type="text" value='<?=isset($personal_info->child1) ? $personal_info->child1 : "" ?>' /></td>
			  <td><input class="date_pick" id="user_employees_attributes_0_child1bday" name="user[employees_attributes][personal_info][child1bday]" size="10" type="text" value='<?=isset($personal_info->child1bday) ? $personal_info->child1bday : "" ?>' /></td>
		   </tr>
		   <tr>
			  <th>First Name</th>
			  <td><input id="user_employees_attributes_0_spouse_first_name" name="user[employees_attributes][personal_info][spouse_first_name]" size="30" type="text" value='<?=isset($personal_info->spouse_first_name) ? $personal_info->spouse_first_name : "" ?>' /></td>
			  <td><input id="user_employees_attributes_0_child2" name="user[employees_attributes][personal_info][child2]" size="30" type="text" value='<?=isset($personal_info->child2) ? $personal_info->child2 : "" ?>' /></td>
			  <td><input class="date_pick" id="user_employees_attributes_0_child2bday" name="user[employees_attributes][personal_info][child2bday]" size="10" type="text" value='<?=isset($personal_info->child2bday) ? $personal_info->child2bday : "" ?>' /></td>
		   </tr>
		   <tr>
			  <th>Middle Name</th>
			  <td><input id="user_employees_attributes_0_spouse_middle_name" name="user[employees_attributes][personal_info][spouse_middle_name]" size="30" type="text" value='<?=isset($personal_info->spouse_middle_name) ? $personal_info->spouse_middle_name : "" ?>' /></td>
			  <td><input id="user_employees_attributes_0_child3" name="user[employees_attributes][personal_info][child3]" size="30" type="text" value='<?=isset($personal_info->child3) ? $personal_info->child3 : "" ?>' /></td>
			  <td><input class="date_pick" id="user_employees_attributes_0_child3bday" name="user[employees_attributes][personal_info][child3bday]" size="10" type="text" value='<?=isset($personal_info->child3bday) ? $personal_info->child3bday : "" ?>' /></td>
		   </tr>
		   <tr>
			  <th>Occupation</th>
			  <td><input id="user_employees_attributes_0_spouse_occupation" name="user[employees_attributes][personal_info][spouse_occupation]" size="30" type="text" value='<?=isset($personal_info->spouse_occupation) ? $personal_info->spouse_occupation : "" ?>' /></td>
			  <td><input id="user_employees_attributes_0_child4" name="user[employees_attributes][personal_info][child4]" size="30" type="text" value='<?=isset($personal_info->child4) ? $personal_info->child4 : "" ?>' /></td>
			  <td><input class="date_pick" id="user_employees_attributes_0_child4bday" name="user[employees_attributes][personal_info][child4bday]" size="10" type="text" value='<?=isset($personal_info->child4bday) ? $personal_info->child4bday : "" ?>' /></td>
		   </tr>
		   <tr>
			  <th>Employer/ Bus. Name</th>
			  <td><input id="user_employees_attributes_0_spouse_employer" name="user[employees_attributes][personal_info][spouse_employer]" size="30" type="text" value='<?=isset($personal_info->spouse_employer) ? $personal_info->spouse_employer : "" ?>' /></td>
			  <td><input id="user_employees_attributes_0_child5" name="user[employees_attributes][personal_info][child5]" size="30" type="text" value='<?=isset($personal_info->child5) ? $personal_info->child5 : "" ?>' /></td>
			  <td><input class="date_pick" id="user_employees_attributes_0_child5bday" name="user[employees_attributes][personal_info][child5bday]" size="10" type="text" value='<?=isset($personal_info->child5bday) ? $personal_info->child5bday : "" ?>' /></td>
		   </tr>
		   <tr>
			  <th>Business Address</th>
			  <td><input id="user_employees_attributes_0_spouse_business_address" name="user[employees_attributes][personal_info][spouse_business_address]" size="30" type="text" value='<?=isset($personal_info->spouse_business_address) ? $personal_info->spouse_business_address : "" ?>' /></td>
			  <td><input id="user_employees_attributes_0_child6" name="user[employees_attributes][personal_info][child6]" size="30" type="text" value='<?=isset($personal_info->child6) ? $personal_info->child6 : "" ?>' /></td>
			  <td><input class="date_pick" id="user_employees_attributes_0_child6bday" name="user[employees_attributes][personal_info][child6bday]" size="10" type="text" value='<?=isset($personal_info->child6bday) ? $personal_info->child6bday : "" ?>' /></td>
		   </tr>
		   <tr>
			  <th>Telephone No.</th>
			  <td><input id="user_employees_attributes_0_spouse_telephone" name="user[employees_attributes][personal_info][spouse_telephone]" size="30" type="text" value='<?=isset($personal_info->spouse_telephone) ? $personal_info->spouse_telephone : "" ?>' /></td>
			  <td><input id="user_employees_attributes_0_child7" name="user[employees_attributes][personal_info][child7]" size="30" type="text" value='<?=isset($personal_info->child7) ? $personal_info->child7 : "" ?>' /></td>
			  <td><input class="date_pick" id="user_employees_attributes_0_child7bday" name="user[employees_attributes][personal_info][child7bday]" size="10" type="text" value='<?=isset($personal_info->child7bday) ? $personal_info->child7bday : "" ?>' /></td>
		   </tr>
		</table>
		<br />
		<table>
		   <tr>
			  <th>Father</th>
			  <th>&nbsp;</th>
		   </tr>
		   <tr>
			  <th>Last Name</th>
			  <td><input id="user_employees_attributes_0_father_last_name" name="user[employees_attributes][personal_info][father_last_name]" size="30" type="text" value='<?=isset($personal_info->father_last_name) ? $personal_info->father_last_name : "" ?>' /></td>
		   </tr>
		   <tr>
			  <th>First Name</th>
			  <td><input id="user_employees_attributes_0_father_first_name" name="user[employees_attributes][personal_info][father_first_name]" size="30" type="text" value='<?=isset($personal_info->father_first_name) ? $personal_info->father_first_name : "" ?>' /></td>
		   </tr>
		   <tr>
			  <th>Middle Name</th>
			  <td><input id="user_employees_attributes_0_father_middle_name" name="user[employees_attributes][personal_info][father_middle_name]" size="30" type="text" value='<?=isset($personal_info->father_middle_name) ? $personal_info->father_middle_name : "" ?>' /></td>
		   </tr>
		</table>
		<br />
		<table>
		   <tr>
			  <th>Mother</th>
			  <th>&nbsp;</th>
		   </tr>
		   <tr>
			  <th>Last Name</th>
			  <td><input id="user_employees_attributes_0_mother_last_name" name="user[employees_attributes][personal_info][mother_last_name]" size="30" type="text" value='<?=isset($personal_info->mother_last_name) ? $personal_info->mother_last_name : "" ?>' /></td>
		   </tr>
		   <tr>
			  <th>First Name</th>
			  <td><input id="user_employees_attributes_0_mother_first_name" name="user[employees_attributes][personal_info][mother_first_name]" size="30" type="text" value='<?=isset($personal_info->mother_last_name) ? $personal_info->mother_last_name : "" ?>' /></td>
		   </tr>
		   <tr>
			  <th>Middle Name</th>
			  <td><input id="user_employees_attributes_0_mother_middle_name" name="user[employees_attributes][personal_info][mother_middle_name]" size="30" type="text" value='<?=isset($personal_info->mother_middle_name) ? $personal_info->mother_middle_name : "" ?>' /></td>
		   </tr>
		</table>

	<?=panel_tail2();?>
	
	<?=panel_head2('III. EDUCATIONAL BACKGROUND');?>

		<p></p>
		<table>
		   <tr>
			  <th>LEVEL</th>
			  <th>NAME OF SCHOOL</th>
			  <th>DEGREE COURSE</th>
			  <th>YEAR GRADUATED</th>
		   </tr>
		   <tr>
			  <th>Elementary</th>
			  <td><input id="user_employees_attributes_0_elementary_name" name="user[employees_attributes][personal_info][elementary_name]" size="30" type="text" value='<?=isset($personal_info->elementary_name) ? $personal_info->elementary_name : "" ?>' /></td>
			  <td><input id="user_employees_attributes_0_elementary_degree" name="user[employees_attributes][personal_info][elementary_degree]" size="30" type="text" value='<?=isset($personal_info->elementary_degree) ? $personal_info->elementary_degree : "" ?>' /></td>
			  <td><input id="user_employees_attributes_0_elementary_year" name="user[employees_attributes][personal_info][elementary_year]" size="10" type="text" value='<?=isset($personal_info->elementary_year) ? $personal_info->elementary_year : "" ?>' /></td>
		   </tr>
		   <tr>
			  <th>Secondary</th>
			  <td><input id="user_employees_attributes_0_secondary_name" name="user[employees_attributes][personal_info][secondary_name]" size="30" type="text" value='<?=isset($personal_info->secondary_name) ? $personal_info->secondary_name : "" ?>' /></td>
			  <td><input id="user_employees_attributes_0_secondary_degree" name="user[employees_attributes][personal_info][secondary_degree]" size="30" type="text" value='<?=isset($personal_info->secondary_degree) ? $personal_info->secondary_degree : "" ?>' /></td>
			  <td><input id="user_employees_attributes_0_secondary_year" name="user[employees_attributes][personal_info][secondary_year]" size="10" type="text" value='<?=isset($personal_info->secondary_year) ? $personal_info->secondary_year : "" ?>' /></td>
		   </tr>
		   <tr>
			  <th>Vocational</th>
			  <td><input id="user_employees_attributes_0_vocational_name" name="user[employees_attributes][personal_info][vocational_name]" size="30" type="text" value='<?=isset($personal_info->vocational_name) ? $personal_info->vocational_name : "" ?>' /></td>
			  <td><input id="user_employees_attributes_0_vocational_degree" name="user[employees_attributes][personal_info][vocational_degree]" size="30" type="text" value='<?=isset($personal_info->vocational_degree) ? $personal_info->vocational_degree : "" ?>' /></td>
			  <td><input id="user_employees_attributes_0_vocational_year" name="user[employees_attributes][personal_info][vocational_year]" size="10" type="text" value='<?=isset($personal_info->vocational_year) ? $personal_info->vocational_year : "" ?>' /></td>
		   </tr>
		   <tr>
			  <th>College</th>
			  <td><input id="user_employees_attributes_0_college_name" name="user[employees_attributes][personal_info][college_name]" size="30" type="text" value='<?=isset($personal_info->college_name) ? $personal_info->college_name : "" ?>' /></td>
			  <td><input id="user_employees_attributes_0_college_degree" name="user[employees_attributes][personal_info][college_degree]" size="30" type="text" value='<?=isset($personal_info->college_degree) ? $personal_info->college_degree : "" ?>' /></td>
			  <td><input id="user_employees_attributes_0_college_year" name="user[employees_attributes][personal_info][college_year]" size="10" type="text" value='<?=isset($personal_info->college_year) ? $personal_info->college_year : "" ?>' /></td>
		   </tr>
		   <tr>
			  <th>Graduate Studies</th>
			  <td><input id="user_employees_attributes_0_graduate_name" name="user[employees_attributes][personal_info][graduate_name]" size="30" type="text" value='<?=isset($personal_info->graduate_name) ? $personal_info->graduate_name : "" ?>' /></td>
			  <td><input id="user_employees_attributes_0_graduate_degree" name="user[employees_attributes][personal_info][graduate_degree]" size="30" type="text" value='<?=isset($personal_info->graduate_degree) ? $personal_info->graduate_degree : "" ?>' /></td>
			  <td><input id="user_employees_attributes_0_graduate_year" name="user[employees_attributes][personal_info][graduate_year]" size="10" type="text" value='<?=isset($personal_info->graduate_year) ? $personal_info->graduate_year : "" ?>' /></td>
		   </tr>
		</table>
	<?=panel_tail2();?>

	<?=panel_head2('IV. Career Service Certification');?>
		<p></p>
		<table>
		   <tr>
			  <th>Career Service/ RA 1080 (BOARD/BAR)</th>
			  <th>Rating</th>
			  <th>Date of examination/ conferment</th>
		   </tr>
		   <?php
		   for($x = 0; $x <= 4; $x++)
		   {?>
				<tr>
				  <td>
					 <input id="user_employees_attributes_<?=$x;?>_career_services_attributes_<?=$x;?>_career" name="user[employees_attributes][career_services_attributes][<?=$x?>][career]" size="30" type="text" value='<?=isset($career_services[$x]->career) ? $career_services[$x]->career : ""?>' />
				  </td>
				  <td>
					 <input id="user_employees_attributes_<?=$x;?>_career_services_attributes_<?=$x;?>_rating" name="user[employees_attributes][career_services_attributes][<?=$x?>][rating]" size="30" type="text" value='<?=isset($career_services[$x]->rating) ? $career_services[$x]->rating : ""?>' />
				  </td>
				  <td>
					 <input class="date_pick" id="user_employees_attributes_<?=$x;?>_career_services_attributes_<?=$x;?>_date_of_examination" name="user[employees_attributes][career_services_attributes][<?=$x?>][date_of_examination]" size="30" type="text" value='<?=isset($career_services[$x]->date_of_examination) ? $career_services[$x]->date_of_examination : ""?>' />
				  </td>
			   </tr>
		   <?php
		   }
		   ?>
		</table>

	<?=panel_tail2();?>

	<?=panel_head2('V. WORK EXPERIENCE (Include self employement. Start from your current work)');?>

		<p></p>
		<table>
			<?php
		   for($x = 0; $x <= 4; $x++)
		   {?>
				<tr>
				  <th colspan=2><?=$x+1;?> Inclusive Dates</th>
				  <th rowspan=2>Position Title <br />(Write in full)</th>
				  <th rowspan=2>Agency/ Office/ Company <br />(Write in full)</th>
			    </tr>
			    <tr>
				  <th>from</th>
				  <th>to present</th>
			    </tr>
				<tr>
				  <td>
					 <input class="date_pick" id="user_employees_attributes_<?=$x;?>_work_experiences_attributes_<?=$x;?>_from" name="user[employees_attributes][work_experiences_attributes][<?=$x;?>][from]" size="5" type="text" value='<?=isset($work_experiences[$x]->from) ? $work_experiences[$x]->from : ""?>' />
				  </td>
				  <td>
					 <input class="date_pick" id="user_employees_attributes_<?=$x;?>_work_experiences_attributes_<?=$x;?>_to" name="user[employees_attributes][work_experiences_attributes][<?=$x;?>][to]" size="5" type="text" value='<?=isset($work_experiences[$x]->to) ? $work_experiences[$x]->to : ""?>' />
				  </td>
				  <td>
					 <input id="user_employees_attributes_<?=$x;?>_work_experiences_attributes_<?=$x;?>_position" name="user[employees_attributes][work_experiences_attributes][<?=$x;?>][position]" size="30" type="text" value='<?=isset($work_experiences[$x]->position) ? $work_experiences[$x]->position : ""?>' />
				  </td>
				  <td>
					 <input id="user_employees_attributes_<?=$x;?>_work_experiences_attributes_<?=$x;?>_agency" name="user[employees_attributes][work_experiences_attributes][<?=$x;?>][agency]" size="30" type="text" value='<?=isset($work_experiences[$x]->agency) ? $work_experiences[$x]->agency : ""?>' />
					</td>
			   </tr>
		   <?php
		   }
		   ?>
		</table>
		<br />
		<table>
		   <tr>
			  <th>Voluntary work or involvement in civic/ non-government/ people /voluntary organizations</th>
		   </tr>
		</table>
		<table>
		   <tr>
			  <th>Name</th>
			  <th>Address</th>
		   </tr>
		   <?php
		   for($x = 0; $x <= 4; $x++)
		   {?>
			
				<tr>
					<td><?=$x+1;?><input id="user_employees_attributes_<?=$x;?>_voluntary_works_attributes_<?=$x;?>_name" name="user[employees_attributes][voluntary_works_attributes][<?=$x;?>][name]" size="30" type="text" value='<?=isset($voluntary_works[$x]->name) ? $voluntary_works[$x]->name : ""?>' /></td>
					<td><input id="user_employees_attributes_<?=$x;?>_voluntary_works_attributes_<?=$x;?>_address" name="user[employees_attributes][voluntary_works_attributes][<?=$x;?>][address]" size="30" type="text" value='<?=isset($voluntary_works[$x]->address) ? $voluntary_works[$x]->address : ""?>' /></td>
				</tr>
		   <?php
		   }
		   ?>
		</table>

	<?=panel_tail2()?>

	<?=panel_head2('VII. TRAINING PROGRAMS (Start from the most recent training.)');?>
	
		<p></p>
		<table>
		   <tr>
			  <th rowspan = 2>TITLE OF SEMINAR/CONFERENCE/WORKSHOP/SHORT COURSES<br />(Write in full)</th>
			  <th colspan=2>INCLUSIVE DATES OF ATTENDANCE</th>
			  <th rowspan = 2>CONDUCTED/SPONSORED BY <br />(Write in full)</th>
		   </tr>
		   <tr>
			  <th>From</th>
			  <th>To</th>
		   </tr>
		   <?php
		   for($x = 0; $x <= 4; $x++)
		   {?>
			
				<tr>
				  <td>
					 <input id="user_employees_attributes_<?=$x;?>_training_programs_attributes_<?=$x;?>_title" name="user[employees_attributes][training_programs_attributes][<?=$x;?>][title]" size="30" type="text" value='<?=isset($training_programs[$x]->title) ? $training_programs[$x]->title : ""?>' />
				  </td>
				  <td>
					 <input class="date_pick" id="user_employees_attributes_<?=$x;?>_training_programs_attributes_<?=$x;?>_from" name="user[employees_attributes][training_programs_attributes][<?=$x;?>][from]" size="10" type="text" value='<?=isset($training_programs[$x]->from) ? $training_programs[$x]->from : ""?>' />
				  </td>
				  <td>
					 <input class="date_pick" id="user_employees_attributes_<?=$x;?>_training_programs_attributes_<?=$x;?>_to" name="user[employees_attributes][training_programs_attributes][<?=$x;?>][to]" size="10" type="text" value='<?=isset($training_programs[$x]->to) ? $training_programs[$x]->to : ""?>' />
				  </td>
				  <td>
					 <input id="user_employees_attributes_<?=$x;?>_training_programs_attributes_<?=$x;?>_conducted" name="user[employees_attributes][training_programs_attributes][<?=$x;?>][conducted]" size="30" type="text" value='<?=isset($training_programs[$x]->conducted) ? $training_programs[$x]->conducted : ""?>' />
				  </td>
			   </tr>
		   <?php
		   }
		   ?>
		</table>
	<?=panel_tail2()?>
	<?=panel_head2('VIII. OTHER INFORMATION');?>
		
		<p></p>
		<table>
		   <tr>
			  <th>NON-ACADEMIC DISTINCTIONS/RECOGNITION</th>
			  <th>MEMBERSHIP IN ASSOCIATION ORGANIZATION</th>
		   </tr>
		   <?php
		   for($x = 0; $x <= 4; $x++)
		   {?>
			
				 <tr>
				  <td><input id="user_employees_attributes_<?=$x;?>_other_informations_attributes_<?=$x;?>_recognition" name="user[employees_attributes][other_informations_attributes][<?=$x;?>][recognition]" size="30" type="text" value='<?=isset($other_informations[$x]->recognition) ? $other_informations[$x]->recognition : ""?>' /></td>
				  <td><input id="user_employees_attributes_<?=$x;?>_other_informations_attributes_<?=$x;?>_organization" name="user[employees_attributes][other_informations_attributes][<?=$x;?>][organization]" size="30" type="text" value='<?=isset($other_informations[$x]->organization) ? $other_informations[$x]->organization : ""?>' /></td>
			   </tr>
		   <?php
		   }
		   ?>
		</table>
		<br />
		<table>
		   <tr>
			  <th>Community Tax Certificate No.</th>
			  <td><input id="user_employees_attributes_0_community_tax" name="user[employees_attributes][personal_info][community_tax]" size="30" type="text" value='<?=isset($personal_info->community_tax) ? $personal_info->community_tax : ""?>' /></td>
		   </tr>
		   <tr>
			  <th>Issued At</th>
			  <td><input id="user_employees_attributes_0_issued_at" name="user[employees_attributes][personal_info][issued_at]" size="30" type="text" value='<?=isset($personal_info->issued_at) ? $personal_info->issued_at : ""?>' /></td>
		   </tr>
		   <tr>
			  <th>Issued On</th>
			  <td><input class="date_pick" id="user_employees_attributes_0_issued_on" name="user[employees_attributes][personal_info][issued_on]" size="30" type="text" value='<?=isset($personal_info->issued_on) ? $personal_info->issued_on : ""?>' /></td>
		   </tr>
		</table>
	<?=panel_tail2()?>
  
	<div id='message' title='Required Fields' style='display:none;'>
		<div id='message_list' class='alert alert-info'>
			Validating ... 
		</div>
	</div>
