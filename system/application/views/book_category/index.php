<a class='btn btn-default btn-sm' href="<?php echo base_url(); ?>book_category/create"><span class = 'glyphicon glyphicon-plus'></span>&nbsp;  New Category</a><br/><br/>
<table>
	  <tr>
		<th>Category</th>
		<th>Action</th>
	  </tr>
	<? $num = 0 ?>
	<?php if(empty($librarycategory) == FALSE):?>
		<? foreach($librarycategory as $obj): ?>
			<tr>
			  
				<td><?= $obj->category;?></td>
				<td>
					<?= badge_link('book_category/edit/'.$obj->id, "primary", 'Edit'); ?>
					<?= badge_link('book_category/destroy/'.$obj->id, "primary confirm", 'Destroy','title="Are you sure to delete this record?"'); ?>
				</td>
			</tr>
		<? endforeach;?>
		<tr>
			<td class="text-right">Total No. of Records</td>
			<td><div class='badge'><?=$total_rows;?></div></td>
		</tr>
	<?php else:?>
		<tr>
			<td colspan=2 class="text-center">----NO data to show---</td>
		</tr>	
	<?php endif;?>
</table>
<?= $links;?>