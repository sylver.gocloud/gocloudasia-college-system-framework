<div id="right">
	<?php 
		$formAttrib = array('id' => 'search_department', 'class' => 'form-inline', 'method' => 'GET');
		echo form_open('departments_cpanel/index/0', $formAttrib);
	?>
    
 <div class="form-group">
    <input id="department" class="form-control" type="text" value="<?=isset($department)?$department:''?>" name="department" placeholder="Department">
  </div>
  
  <div class="form-group">
    <?php
		echo form_submit('submit', 'Search');
	?>
  </div>
  
  <div class="form-group">
    <a class='btn btn-default btn-sm' href='<?=base_url('departments_cpanel/create');?>'><span class = 'glyphicon glyphicon-plus'></span>&nbsp; Create New Department</a>
  </div>
  
	
		<?echo form_close();?>
<br>
<?echo isset($links) ? $links : NULL;?>
	<?php
	if(isset($search)){
	?>
	<br />
	<table>
		<tr>
		  <th>Icon</th>
		  <th>Code</th>
		  <th>Description</th>
		  <th>Visible</th>
		  <th>Order</th>
		  <th>Action</th>
		</tr>
		<tbody>
		<?php
		if(!empty($search))
		{
			foreach($search as $obj):
			?>
			<tr>
			<td>
				<img class='imgx' width = '80' src='<?=base_url()?><?=$obj->icon?>' >
				</img>
			</td>
			<td><?php echo ($obj->department); ?></td>
			<td><?php echo $obj->description; ?></td>
			<td><?php echo $obj->visible ==1 ? "YES" : "NO"; ?></td>
			<td><?php echo $obj->ord; ?></td>
			<td>
				
				<div class="btn-group btn-group-sm">
					<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
					
					<i class="fa fa-caret-square-o-down"></i> <span class="caret"></span>
					</button>
					<ul class="dropdown-menu" role="menu">
					
					<li><a href="<?=base_url('departments_cpanel/edit/'.$obj->id);?>" title='Update <?=$obj->description?>' ><span class='glyphicon glyphicon-pencil'></span>&nbsp; Edit Profile</a></li>
				
					<?if($obj->visible == 0):?>
					
					<li><a href="<?=base_url('departments_cpanel/show/'.$obj->id);?>" title='Show <?=$obj->description?>' ><span class='glyphicon glyphicon-eye-open'></span>&nbsp; Show</a></li>
					
					<?else:?>
					
					<li><a href="<?=base_url('departments_cpanel/hide/'.$obj->id);?>" title='Hide <?=$obj->description?>' ><span class='glyphicon glyphicon-eye-close'></span>&nbsp; Hide</a></li>
					
					<?endif;?>
					
					<li><a href="<?=base_url('departments_cpanel/student_profile_access/'.$obj->id);?>" title='Update <?=$obj->description?> for student profile access' ><i class="fa fa-unlock"></i>&nbsp; Student Profile Access</a></li>
					
					<li><a href="<?=base_url('departments_cpanel/menus/'.$obj->id);?>" title='Menus of <?=$obj->description?>' ><span class='glyphicon glyphicon-list'></span>&nbsp; Menus</a></li>
					
				 </ul>
				</div>
			</td>
			</tr>
			<?php 
			endforeach; ?>
			<tr>
				<td colspan='4' align='right'><b>Total Records</b></td>
				<td><span class='badge'><?=($total_rows) ? $total_rows : 0;?></span></td>
			</tr>
			<?php
		}
		else
		{
		?>
		<tr>
		<td colspan="5">
			Record Not Found
		</td>
		</tr>
		<?php
		}
		?>
		</tbody>
	</table>
	<?echo isset($links) ? $links : NULL;?>
	<?php
	}
	?>
	</div>
	
	<div class="clear"></div>
</div>
