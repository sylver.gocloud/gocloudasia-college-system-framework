<?$this->load->view('departments_cpanel/_tab');?>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/fontawesome.dropdown.js"></script>
<script type="text/javascript">
	function validate()
	{
		run_pleasewait();
		var ctr = 0;
		var msg = "<ul>";
		var xfocus = "";
		$('.not_blank').css('border-color','gray');
		$('.not_blank').each(function(){
			var xval = $(this).val().trim();
			if(xval == ""){
				ctr++;
				$(this).css('border-color','red');
				if(ctr == 1) { xfocus = $(this); }
				var label = $(this).parent().prev().text();
				
				msg += "<li>"
				+label+" should not be blank</li>";
			}
		});
		
		msg += "<ul>";
		
		if(ctr > 0)
		{
			close_pleasewait();
			xfocus.focus();
			custom_modal('Required',"Please don't leave anything blank.");
			
			return false;
		}
		else
		{
			
		}
	}
</script>
<?if($menu_list):?>
	
	<?echo form_open('','onsubmit="return validate()"');?>
	
	<table>
		
		<tr>
			<th>Icon</th>
			<th>Caption</th>
			<th>Visibilty</th>
			<th>Order</th>
		</tr>
	
	<?foreach($menu_list as $obj):?>
	
		<tr>
			<td>
				<div class="row">
					<div class="col-md-12">
						<input type="hidden" name="menu[<?=$obj->id?>][menu_icon]" value="<?=$obj->menu_icon?>" class='fontawesome_dropdown_hidden_txt'>
						<?php echo fontawesome_dropdown($obj->menu_icon); ?>
					</div>
				</div>
			</td>
			<td>
				<?
					$data = array(
						  'name'        => "menu[$obj->id][caption]",
						  'class'		=> 'not_blank',
						  'value'       => isset($obj->caption) ? $obj->caption : '',
						  'maxLength'   => 30,
						  'placeHolder'	=> 'Required',
						);
			
					echo form_input($data);
				?>
			</td>
			
			<td>
				<?
					$data = array(
						  'name'        => "menu[$obj->id][visible]",
						  isset($obj->visible) ? 'checked' : '' => isset($obj->visible) ? $obj->visible : FALSE,
						);

					echo form_checkbox($data);
				?>
			</td>
			
			<td>
				<?
					$data = array(
						  'name'        => "menu[$obj->id][menu_num]",
						  'class'       => 'numeric form-control not_blank',
						  'maxLength'   => 3,
						  'value' 	=> isset($obj->menu_num) ? $obj->menu_num : '',
						  'placeHolder'	=> '0'
						);

					echo form_input($data);
				?>
			</td>
		
		</tr>
	
	<?endforeach;?>
	
	</table>
	
	<?=form_submit('submit','Save Changes')?>
	
	<?=form_close();?>
	
<?else:?>
	
	<div class='alert alert-danger'>
		
		<b>
			<span class='glyphicon glyphicon-remove-circle'></span> &nbsp;
			No Menu Group Selected
		</b>
		
	</div>
	
<?endif;?>



