<script type="text/javascript">
	function validate()
	{
		run_pleasewait();
		var ctr = 0;
		var msg = "<ul>";
		var xfocus = "";
		$('.not_blank').css('border-color','gray');
		$('.not_blank').each(function(){
			var xval = $(this).val().trim();
			if(xval == ""){
				ctr++;
				$(this).css('border-color','red');
				if(ctr == 1) { xfocus = $(this); }
				var label = $(this).parent().prev().text();
				
				msg += "<li>"
				+label+" should not be blank</li>";
			}
		});
		
		msg += "<ul>";
		
		if(ctr > 0)
		{
			close_pleasewait();
			xfocus.focus();
			$('#alertModal_Label').text('Required Fields'); //SET MODAL TITLE
			$('#alertModal_Body').html(msg); //SET MODAL CONTENT
			$('#alertModal').modal('show'); //SHOW MODAL
			
			return false;
		}
		else
		{
			
		}
	}
</script>

<div class="row">
  <div class="col-md-3"><label for="department">Code</label></div>
  <div class="col-md-6">
	<?
		$data = array(
              'name'        => 'departments[department]',
              'id'   	     => 'department',
			  'class'		=> 'not_blank',
              'value'       => isset($departments->department) ? $departments->department : '',
              'maxLength'   => 30,
			  'placeHolder'	=> 'Required',
			   isset($edit) ? 'readonly' : '' => isset($edit) ? TRUE : FALSE,
            );

		echo form_input($data);
	?>
  </div>
</div>

<div class="row">
  <div class="col-md-3"><label for="description">Description</label></div>
  <div class="col-md-6">
	<?
		$data = array(
              'name'        => 'departments[description]',
              'id'        => 'department',
			  'class'		=> 'not_blank',
              'value'       => isset($departments->description) ? $departments->description : '',
               'maxLength'   => 50,
			   'placeHolder'	=> 'Required'
            );

		echo form_input($data);
	?>
  </div>
</div>

<div class="row">
  <div class="col-md-3"><label for="visible">Visible ?</label></div>
  <div class="col-md-6">
	<?
		$data = array(
              'name'        => 'departments[visible]',
              'id'        => 'department',
			  isset($departments->visible) ? 'checked' : '' => isset($departments->visible) ? $departments->visible : FALSE,
            );

		echo form_checkbox($data);
	?>
  </div>
</div>

<div class="row">
  <div class="col-md-3"><label for="ord">Order (Numeric)</label></div>
  <div class="col-md-6">
	<?
		$data = array(
              'name'        => 'departments[ord]',
              'id'        	=> 'ord',
              'class'       => 'numeric form-control not_blank',
              'maxLength'   => 3,
			  'value' 	=> isset($departments->ord) ? $departments->ord : '',
			  'placeHolder'	=> '0'
            );

		echo form_input($data);
	?>
  </div>
</div>

<div class="row">
  <div class="col-md-3"><label for="level">Tile Option</label></div>
  <div class="col-md-6">
	<?
		$data = array(
              '1'        => 'Single Vertical',
              '2'        	=> 'Tile Horizontal (By Two)',
              '3'        	=> 'Tile Horizontal (By Three)',
              '4'        	=> 'Tile Horizontal (By Four)',
            );

		echo form_dropdown('departments[level]',$data,isset($departments->level) ? $departments->level : '');
	?>
  </div>
</div>

<div class="row">
  <div class="col-md-3"><label for="icon">Icon (Recommended : JPG and PNG only, small size)</label></div>
  <div class="col-md-6">
		<div style=''>
			<?if(isset($departments->icon) && $departments->icon):?>
			<img src="<?=base_url().$departments->icon;?>"/>
			<?else:?>
			<img src="<?=base_url().'assets/images/no_photo.jpg';?>"/>
			<?endif;?>
		</div>
		<input type="file" name="userfile" size="20" />
  </div>
</div>

<label for="setting_logo">School Logo: (JPG and PNG only)</label><br />
			