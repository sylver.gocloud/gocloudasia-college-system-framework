<?php
	$f_years = array();
	$f_courses = array();
	if(isset($years)){
		foreach($years as $obj){
			$f_years[$obj->id] = $obj->year;
		}
	}
	if(isset($courses)){
		foreach($courses as $obj){
			$f_courses[$obj->id] = $obj->course;
		}
	}
?>
<div id="right">
	<div id="right_top" >
		  <p id="right_title">Assign Course : New</p>
	</div>
	
	<div id="right_bottom">
	
	<?  
		echo validation_errors('<div class="alert alert-danger">', '</div>');
		
		echo form_open('');
		
		echo form_hidden('courses[block_system_setting_id]',$block_system_settings->id);
		
		echo form_hidden('courses[academic_year_id]',$block_system_settings->academic_year_id);
		
		echo form_hidden('courses[semester_id]',$block_system_settings->semester_id);
	?>
	
	<br/>
	
	<p>
		<label for="year">Year</label><br />
			<?php echo form_dropdown('courses[year_id]', $f_years); ?>
	</p>
	
	<p>
		<label for="year">Course</label><br />
			<?php echo form_dropdown('courses[course_id]', $f_courses); ?>
	</p>		
	<?
		echo form_submit('','Save Changes');
		echo form_close();
	?>
		
	</div>
	<div class="clear"></div>

</div>
	
	<div class="clear"></div>
</div>