<script>
	function Validate()
	{
		var ctr = 0;
		var xfocus = "";
		$('.not_blank').css('border-color','gray');
		$('.not_blank').each(function(){
			var xval = $(this).val().trim();
			if(xval == ""){
				ctr++;
				$(this).css('border-color','red');
				if(ctr == 1) { xfocus = $(this); }
			}
		});
		
		if(ctr > 0)
		{
			xfocus.focus();
			$('#message_list').html("<p>Some fields are required.</p>");
			$( "#message" ).dialog({
			  height : 300,
			  modal : true,
			  buttons: {
				Close: function() {
				  $( this ).dialog( "close" );
				}
			  }
			});
			
			return false;
		}
	}
</script>
<?php
	$cos = $this->cos->user;
?>
<?echo form_open('','onsubmit="return Validate()" class="confirm form-horizontal"');?>		
	
	<div class="form-group">
	  <label class="col-md-2 control-label" for="academic_year_id">Academic Year</label>  
	  <div class="col-md-6">
	  <?=school_year_dropdown("assign_coursefee[academic_year_id]",set_value('assign_coursefee[academic_year_id]',$cos?$cos->academic_year_id:''),'class="not_blank" required');?>
	  </div>
	</div>
	<hr>
	<div class="form-group">
	  <label class="col-md-2 control-label" for="year_id">Select Year Level</label>  
	  <div class="col-md-6">
	  <?=year_dropdown("assign_coursefee[year_id]",'','class="not_blank" required');?>
	  <span class="help-block"><input type="checkbox" name="create_all_year" > &nbsp; Or Check to create for <strong>All</strong> year levels (Duplicate will not be added).</span>    
	  </div>
	</div>

	<div class="form-group">
	  <label class="col-md-2 control-label" for="semester_id">Select Semester</label>  
	  <div class="col-md-6">
	  <?=semester_dropdown("assign_coursefee[semester_id]",set_value('assign_coursefee[semester_id]',$cos?$cos->semester_id:''),'class="not_blank" required');?>
	  <span class="help-block"><input type="checkbox" name="create_all_semester" > &nbsp; Or Check to create for <strong>All</strong> semesters (Duplicate will not be added).</span>    
	  </div>
	</div>

	<div class="form-group">
	  <label class="col-md-2 control-label" for="course_id">Select Course</label>
	  <div class="col-md-6">
	  <?=course_dropdown("assign_coursefee[course_id]",'','class="not_blank" required');?>
	  </div>
	</div>
	
	<!-- Button (Double) -->
	<div class="form-group">
	  <label class="col-md-2 control-label" for="button1id"></label>
	  <div class="col-md-6">
	    <button type="submit" name="save" id="save" value="save" class="btn btn-success">Save</button>
	    <a href="<?php echo base_url(); ?>coursefinances/view_assigned_course/<?=$id?>" class="btn btn-danger">Cancel</a>
	  </div>
	</div>
		
<? echo form_close(); ?>	

<div id='message' title='Required Fields' style='display:none;'>
	<div id='message_list' class='alert alert-info'>
		Validating ... 
	</div>
</div>