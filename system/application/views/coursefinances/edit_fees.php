<?$this->load->view('coursefinances/_tab');?>
<?echo form_open('');?>
	
<?php
	
	if($coursefees){

		$tuition_fees = array();
		$misc_fees = array();
		$other_fees = array();
		$nstp_fees = array();
		$lab_fees = array();
		$other_school = array();

		foreach($coursefees as $obj){
			// vp($obj);
			if($obj->is_tuition_fee == 1){
				$tuition_fees[] = $obj;
			}
			if($obj->is_misc == 1){
				$misc_fees[] = $obj;
			}
			if($obj->is_other == 1){
				$other_fees[] = $obj;
			}
			if($obj->is_lab == 1){
				$lab_fees[] = $obj;
			}
			if($obj->is_nstp == 1){
				$nstp_fees[] = $obj;
			}
			if($obj->is_other_school == 1){
				$other_school[] = $obj;
			}
		}
	?>
	<?if(count($tuition_fees) > 0):?>
		<?=panel_head(array('panel_title'=>'Tuition Fees'));?>
		<table class="table table-hover">
		<?foreach ($tuition_fees as $key => $obj):?>
			<?unset($data);
			$data= array(
				  'name'        => 'coursefees['.$obj->id.']',
				  'value'       => isset($obj->value) ? $obj->value : '',
				  'maxlength'   => '15',
				  'size'        => '50',
				  'class'       => 'form-control'
				);
			
			?>
			<tr>
			  <td class="v-align-center" ><label class="bold" for="<?=$obj->name;?>"><?=$obj->name;?></label> </td>
			  <td class="for-input" ><?php echo form_input($data);?> </td>
			</tr>
		<?endforeach;?>
		</table>
		<?=panel_foot();?>
	<?endif;?>

	<?if(count($lab_fees) > 0):?>
		<?=panel_head(array('panel_title'=>'Laboratory Fees'));?>
		<table class="table table-hover">
		<?foreach ($lab_fees as $key => $obj):?>
			<?unset($data);
			$data= array(
				  'name'        => 'coursefees['.$obj->id.']',
				  'value'       => isset($obj->value) ? $obj->value : '',
				  'maxlength'   => '15',
				  'size'        => '50',
				  'class'       => 'form-control'
				);
			
			?>
			<tr>
			  <td class="v-align-center" ><label class="bold" for="<?=$obj->name;?>"><?=$obj->name;?></label> </td>
			  <td class="for-input" ><?php echo form_input($data);?> </td>
			</tr>
		<?endforeach;?>
		</table>
		<?=panel_foot();?>
	<?endif;?>

	<?if(count($misc_fees) > 0):?>
		<?=panel_head(array('panel_title'=>'Miscellaneous Fees'));?>
		<table class="table table-hover">
		<?foreach ($misc_fees as $key => $obj):?>
			<?unset($data);
			$data= array(
				  'name'        => 'coursefees['.$obj->id.']',
				  'value'       => isset($obj->value) ? $obj->value : '',
				  'maxlength'   => '15',
				  'size'        => '50',
				  'class'       => 'form-control'
				);
			
			?>
			<tr>
			  <td class="v-align-center" ><label class="bold" for="<?=$obj->name;?>"><?=$obj->name;?></label> </td>
			  <td class="for-input" ><?php echo form_input($data);?> </td>
			</tr>
		<?endforeach;?>
		</table>
		<?=panel_foot();?>
	<?endif;?>

	<?if(count($other_school) > 0):?>
		<?=panel_head(array('panel_title'=>'Other School Fees'));?>
		<table class="table table-hover">
		<?foreach ($other_school as $key => $obj):?>
			<?unset($data);
			$data= array(
				  'name'        => 'coursefees['.$obj->id.']',
				  'value'       => isset($obj->value) ? $obj->value : '',
				  'maxlength'   => '15',
				  'size'        => '50',
				  'class'       => 'form-control'
				);
			
			?>
			<tr>
			  <td class="v-align-center" ><label class="bold" for="<?=$obj->name;?>"><?=$obj->name;?></label> </td>
			  <td class="for-input" ><?php echo form_input($data);?> </td>
			</tr>
		<?endforeach;?>
		</table>
		<?=panel_foot();?>
	<?endif;?>

	<?if(count($other_fees) > 0):?>
		<?=panel_head(array('panel_title'=>'Other / Clearance Fees'));?>
		<table class="table table-hover">
		<?foreach ($other_fees as $key => $obj):?>
			<?unset($data);
			$data= array(
				  'name'        => 'coursefees['.$obj->id.']',
				  'value'       => isset($obj->value) ? $obj->value : '',
				  'maxlength'   => '15',
				  'size'        => '50',
				  'class'       => 'form-control'
				);
			
			?>
			<tr>
			  <td class="v-align-center" ><label class="bold" for="<?=$obj->name;?>"><?=$obj->name;?></label> </td>
			  <td class="for-input" ><?php echo form_input($data);?> </td>
			</tr>
		<?endforeach;?>
		</table>
		<?=panel_foot();?>
	<?endif;?>
	
	<?if(count($nstp_fees) > 0):?>
		<?=panel_head(array('panel_title'=>'NSTP Fees (This will only apply if student has NSTP subjects)'));?>
		<table class="table table-hover">
		<?foreach ($nstp_fees as $key => $obj):?>
			<?unset($data);
			$data= array(
				  'name'        => 'coursefees['.$obj->id.']',
				  'value'       => isset($obj->value) ? $obj->value : '',
				  'maxlength'   => '15',
				  'size'        => '50',
				  'class'       => 'form-control'
				);
			
			?>
			<tr>
			  <td class="v-align-center" ><label class="bold" for="<?=$obj->name;?>"><?=$obj->name;?></label> </td>
			  <td class="for-input" ><?php echo form_input($data);?> </td>
			</tr>
		<?endforeach;?>
		</table>
		<?=panel_foot();?>
	<?endif;?>

	<?		
	}
?>

<?
	echo form_submit('','Save Changes');
	?>
		<a class='btn btn-default btn-sm' href="<?php echo base_url(); ?>coursefinances" rel="facebox" class="actionlink"><span class='glyphicon glyphicon-backward'></span>&nbsp;  Back to list of Course Finance</a>
	<?
	echo form_close();
?>