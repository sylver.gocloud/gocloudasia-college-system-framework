<?php
	$value= array(
              'name'        => 'coursefees[value]',
              'id'        => 'fee_value',
              'value'       => '',
              'maxlength'   => '100',
              'size'        => '50',
              'style'       => 'width:200px',
            );
		
	$f_fees = array();
	if(isset($fees)){
		foreach($fees as $obj){
			$pass = false;
			//exclude those are already added
			//exclude nstp fees when is already added

			$nstp = $obj->is_nstp == 1 ? true : false;

			if(isset($coursefees))
			{
				foreach($coursefees as $obj2){

					if($obj2->is_nstp == 1)
					{
						$pass = true;
						break;
					}

					if($obj2->fee_id == $obj->id)
					{
						$pass = true;
						break;
					}
				}
			}
			
			if(!$pass)
			{
				$fee_type = " ";
				if($obj->is_tuition_fee == 1){
					$fee_type = "-[Tuition Fee]";
				}else if($obj->is_misc == 1){
					$fee_type = "-[Misc. Fee]";
				}else if($obj->is_other == 1){
					$fee_type = "-[Other Fee]";
				}else if($obj->is_lab == 1){
					$fee_type = "-[Lab Fee]";
				}else if($obj->is_other_school == 1){
					$fee_type = "-[Other School Fee]";
				}
				else if($obj->is_nstp == 1){
					$fee_type = "-[NSTP Fee]";
				}else{}
				$f_fees[$obj->id] = $obj->name.$fee_type.' - '.$obj->value;
			}
		}
	}
?>
	<?if(count($f_fees) > 0):?>
	<p>
		<label for="fees">Fees : </label><br />
		<?=form_dropdown('coursefees[fee_id]',$f_fees, '','id = "fee_dropdown"');?>
	</p>
	<script>
		$('#fee_dropdown').on('change', function(){
			var txt = ($(this).find(":selected").text());
			var txt_x = txt.split("-");
			if(txt_x){
				var x = txt_x[txt_x.length-1];
				$('#fee_value').val(parseFloat(x));
			}
		})
	</script>
	<p>
    <label for="value">Value</label>
	<?php 
		echo form_input($value);
	?>
	</p>
	<p>
		<?echo form_submit('','Save Changes');?>
		<a class='btn btn-default btn-sm' href="<?php echo base_url(); ?>coursefinances" rel="facebox" class="actionlink"><span class='glyphicon glyphicon-backward'></span>&nbsp;  Back to list of Course Finance</a>
	</p>
  <?else:?>
  	<div class='alert alert-danger' >No fees available or all fees already added</div>
  	<a class='btn btn-default btn-sm' href="<?php echo base_url(); ?>coursefinances" rel="facebox" class="actionlink"><span class='glyphicon glyphicon-backward'></span>&nbsp;  Back to list of Course Finance</a>
  <?endif;?>
  
	<?echo form_close();?>


