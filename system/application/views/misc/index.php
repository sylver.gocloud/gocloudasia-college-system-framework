<div id="right">

	<div id="right_top" ><p id="right_title">News And Events</p></div>
	
	<div id="right_bottom">

<div id="top_rb1" >
	 <p id="top_rb1_title" class="box_title">News</p>
	 <div id="top_rb1_con">
	<?php if($announcements){ ?>
   <ul class="event_list">
	 <?php foreach($announcements as $announcement): ?>
     <li>
	   <div id="announcement_messages"><p><?php echo $announcement->message ; ?></p></div> 
	   <div id="announcement_date"><?php echo date('m/d/Y',strtotime($announcement->date)) ; ?></div>
	   <div class="clear">&nbsp;</div>
     </li>
	 <?php endforeach; ?>
   </ul><a href="/announcements">Read All</a>
	<?php } ?>
	 </div>
</div>

<div id="top_rb2" >
	 <p id="top_rb2_title" class="box_title">Events</p>  
	 <div id="top_rb2_con">
	<?php if($events){ ?>
   <ul class="event_list">
	   <?php foreach($events as $event): ?>
        <li>
	         <div id="announcement_messages"><p><?php echo $event->title; ?></p></div> 
	         <div id="announcement_date"><?php echo date('m-d-Y',strtotime($event->start_date)).' - '.date('m-d-Y',strtotime($event->end_date)); ?></div>
	         <div class="clear">&nbsp;</div>
        </li> 
	   <?php endforeach; ?>
   </ul><a href="/events">Read All</a>
	<?php } ?>
	 </div>
</div>

	</div>
	
</div>