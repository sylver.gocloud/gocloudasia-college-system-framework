<p><a class='btn btn-default btn-sm' href="<?php echo base_url(); ?>grading_periods/create" rel="facebox"><span class = 'glyphicon glyphicon-plus'></span>&nbsp;  New Grading Period</a></li></p>
<?echo form_open('');?>
<table class="paginated">
  <tr>
    <th>Use</th>
    <th>Grading Period</th>
    <th>Order</th>
    <th>Action</th>
  </tr>
  
		 <?php if($grading_periods){ ?>
  <?php foreach( $grading_periods as $grading_period): ?>
    <tr>
      <td><?php 
		$sel = $grading_period->is_set == '1' ? 'checked' : '';
		$sel_caption = $grading_period->is_set == '1' ? 'Current' : '';
		?>
		<input type='radio' name='current' <?=$sel?> value='<?=$grading_period->id;?>' /> <?=$sel_caption?>
		</td>
      <td><?php echo $grading_period->grading_period; ?></td>
      <td><?php echo $grading_period->orders; ?></td>
      <td>
      <a href="<?php echo base_url()."grading_periods/edit/".$grading_period->id; ?>" rel="facebox" class="actionlink"><span class='glyphicon glyphicon-pencil' ></span>&nbsp;  Edit</a> |
      <a href="<?php echo base_url()."grading_periods/destroy/".$grading_period->id; ?>" rel="facebox" class="actionlink confirm"><span class='glyphicon glyphicon-trash' ></span>&nbsp; Destroy</a></td>
    </tr>
  <?php endforeach; ?>
  <?php } ?>
  
</table>
 <tr>
    <th>Use</th>
    <th>Grading Period</th>
	<th>Order</th>
    <th>Action</th>
  </tr>
<?
	echo form_submit('','Set as Grading Period');
	echo form_close();
?>	