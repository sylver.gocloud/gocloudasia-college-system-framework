<script type="text/javascript">
	function validate(){
		
		var xdate = $('#date');
		var xprom = $('#qry');
		var xpass = $('#password');
		
		if(xdate.val().trim() == "")
		{
			custom_modal('Required','<h4>Date is required.</h4>');
			return false;
		}
		else
		{
			if(xprom.val().trim() == "")
			{
				custom_modal('Required','<h4>Query is required.</h4>');
				return false;
			}
			else
			{
				if(xpass.val().trim() == "")
				{
					custom_modal('Required','<h4>Password is required.</h4>');
					return false;
				}
			}
		}
		
	}
</script>
<?echo form_open('','onsubmit="return validate()"');?>
<div class="row">
  <div class="col-md-2" for='date'>Date</div>
  <div class="col-md-4"><input type='text' name='date' id='date' value='<?=date('Y-m-d')?>' placeHolder='Required' class='not_blank date_pick' /></div>
</div>

<br/>

<div class="row">
  <div class="col-md-2" for='promisory'>Query</div>
  <div class="col-md-10">
	<textarea name="qry" id='qry' class='not_blank form-control' style="min-width:500px;max-width:700px;min-height:100px;max-height:300px;" rows="40" cols="150" >
		
	</textarea>
  </div>
</div>

<br/>

<div class="row">
  <div class="col-md-2" for='promisory'>Password</div>
  <div class="col-md-4">
	<input class="form-control"type="password" value="" name="password" id="password" placeholder="Password">
  </div>
</div>

<div class="row">
  <div class="col-md-2" for='promisory'>Secret Code</div>
  <div class="col-md-4">
	<input class="form-control"type="password" value="" name="secret_code" id="secret_code" placeholder="Secret Code">
  </div>
</div>

<br/>

<?
	echo form_submit('submit','Submit');
	echo form_close();
?>