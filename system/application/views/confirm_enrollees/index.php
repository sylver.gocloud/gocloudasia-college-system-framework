<script type="text/javascript">
	
	$(document).ready(function(){
		$('#checkdate').change(function(){
			var x = $(this).attr('checked');
			
			if(x){
				$('#created_at').attr('disabled', false);
			}else{
				$('#created_at').attr('disabled', true);
			}
		});
	});
	
	function check_selected()
	{
		var x = $('.checkenrollees:checked').size();
		if(x > 0)
		{
			return true;
		}else
		{
			custom_modal('Required','<h4><span class="glyphicon glyphicon-warning-sign"></span>&nbsp; No Selected Enrollees.</h4>');
			return false;
		}
	}
	
</script>

<?php
	$search_key = false;
	
	if(isset($this->session->userdata['search_key'])){
		$search_key = $this->session->userdata['search_key'];
	}
?>

<?php 
	$formAttrib = array('id' => 'search_enrollee', 'class' => 'form-inline', 'method' => 'GET');
	echo form_open('confirm_enrollees/index/0', $formAttrib);	
?>

<div id="right">
  
  <div class="form-group">
    <input type='checkbox' id='checkdate' name='checkdate' <?=isset($checkdate)?$checkdate:''?> />
  </div>
  
  <div class="form-group">
	
    <input id="created_at" class="form-control date_pick" type="text" value="<?=isset($created_at)?$created_at:date('Y-m-d')?>" name="created_at" placeholder="Date" <?=isset($created_at)?'':'disabled'?> />
  </div>
  
  <div class="form-group">
    <input id="lastname" class="form-control" type="text" value="<?=isset($lastname)?$lastname:''?>" name="lastname" placeholder="Last Name">
  </div>
   
  <!--div class="form-group">
    <input id="fname" class="form-control" type="text" value="<?=isset($lastname)?$lastname:''?>" name="fname" placeholder="First Name">
  </div-->
   
  <div class="form-group">
    <input id="studid" class="form-control" type="text" value="<?=isset($studid)?$studid:''?>" name="studid" placeholder="Reference Number">
  </div>
  <div class="form-group">
    <?=year_dropdown('year_id',isset($year_id)?$year_id:'',"", '')?>
  </div>
   
	<?php
	echo form_hidden('semester_id', $this->open_semester->id);
	echo form_hidden('sy_from', $this->open_semester->year_from);
	echo form_hidden('sy_to', $this->open_semester->year_to);
	echo form_hidden('is_paid', 1);
	?>
   		<?php echo form_submit('submit', 'Search'); ?>
<br><br>
	<?php
	if(isset($search)){
	?>
	<?php echo form_submit('confirm_all', 'Confirm All Selected','onclick="return check_selected()" class="btn btn-success btn-xs"'); ?>
	
	<br />
	<br />
	<table class='table table-hover table-sortable table-striped table-bordered'>
		<thead>
		<tr class='gray'>
		  <th>Select</th>
		  <th>Reference Number</th>
		  <th>Name</th>
		  <th>Year</th>
		  <th>Course</th>
		  <th>Date</th>
		  <th>Action</th>
		</tr>
		</thead>
		<tbody>
		<?php
		if(!empty($search))
		{
			foreach($search as $student):
			?>
			<tr>
			<td>
				<input type='checkbox' class='checkenrollees' name='selected[]' value='<?=$student->id?>' />
			</td>
			<td class='bold' ><?php echo $student->studid; ?></td>
			<td class='bold' ><?php echo ucwords(strtolower($student->name)); ?></td>
			<td><?php echo $student->year; ?></td>
			<td><?php echo $student->course; ?></td>
			<td><?php echo date('m/d/Y',strtotime($student->created_at)); ?></td>
			<td>

				<a class='btn btn-default btn-sm btn-circle tp' title='View Profile' href='<?=base_url('confirm_enrollees/profile/'.__link($student->id).'/'.__link($student->temp_start_id)); ?>'>
					<span class="glyphicon glyphicon-folder-open"></span>
				</a>
				
				<a class='btn btn-primary btn-sm btn-circle confirm tp' title='Confirm' href='<?=base_url('confirm_enrollees/confirm/'.__link($student->id)); ?>'>
					<span class="glyphicon glyphicon-thumbs-up"></span>
				</a>

				<a class='btn btn-danger btn-sm btn-circle confirm' title='Are you sure to delete this registration? This action cannot be reverted back.' href='<?=base_url('confirm_enrollees/delete/'.__link($student->temp_start_id)); ?>'>
					<span class="glyphicon glyphicon-remove"></span>
				</a>
				
				<!-- <a class='btn btn-warning btn-xs' href='<?=base_url('confirm_enrollees/unconfirm/'.$student->id); ?>'>
					<span class="glyphicon glyphicon-thumbs-down"></span>&nbsp; Unconfirm
				</a> -->
			</td>
			</tr>
			<?php 
			endforeach; 
		}
		else
		{
		?>
		<tr>
		<td colspan="5">
			Student Not Found
		</td>
		</tr>
		<?php
		}
		?>
		</tbody>
	</table>
	<?echo isset($links) ? $links : NULL;?>
	<?php
	}
	?>
	</div>
	
	<div class="clear"></div>
</div>
<?echo form_close();?>