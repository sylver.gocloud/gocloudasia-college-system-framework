<script type="text/javascript">
	
	$(document).ready(function(){
		$('#checkdate').change(function(){
			var x = $(this).attr('checked');
			
			if(x){
				$('#created_at').attr('disabled', false);
			}else{
				$('#created_at').attr('disabled', true);
			}
		});
	});
	
	function check_selected()
	{
		var x = $('.checkenrollees:checked').size();
		if(x > 0)
		{
			return true;
		}else
		{
			custom_modal('Required','<h4><span class="glyphicon glyphicon-warning-sign"></span>&nbsp; No Selected Enrollees.</h4>');
			return false;
		}
	}
	
</script>

<?php
	$search_key = false;
	
	if(isset($this->session->userdata['search_key'])){
		$search_key = $this->session->userdata['search_key'];
	}
?>

<?php 
	$formAttrib = array('id' => 'search_enrollee', 'class' => 'form-inline', 'method' => 'POST');
	echo form_open('', $formAttrib);	
?>

<div id="right">
  
  <div class="form-group">
    <input type='checkbox' id='checkdate' name='checkdate' <?=isset($created_at)?'checked':''?> />
  </div>
  
  <div class="form-group">
	
    <input id="created_at" class="form-control date_pick" type="text" value="<?=isset($created_at)?$created_at:date('Y-m-d')?>" name="created_at" placeholder="Date" <?=isset($created_at)?'':'disabled'?> />
  </div>
  
  <div class="form-group">
    <input id="lastname" class="form-control" type="text" value="<?=isset($search_key)?$search_key['lastname']:''?>" name="lastname" placeholder="Last Name">
  </div>
   
  <!--div class="form-group">
    <input id="fname" class="form-control" type="text" value="<?=isset($search_key)?$search_key['fname']:''?>" name="fname" placeholder="First Name">
  </div-->
   
  <div class="form-group">
    <input id="studid" class="form-control" type="text" value="<?=isset($search_key)?$search_key['studid']:''?>" name="studid" placeholder="Reference Number">
  </div>
  <div class="form-group">
    <?=year_dropdown('year_id',isset($search_key)?$search_key['year_id']:'',"", '')?>
  </div>
   
	<?php
	echo form_hidden('semester_id', $this->open_semester->id);
	echo form_hidden('sy_from', $this->open_semester->year_from);
	echo form_hidden('sy_to', $this->open_semester->year_to);
	echo form_hidden('is_paid', 1);
	?>
   		<?php echo form_submit('submit', 'Search'); ?>
<br><br>
	<?php
	if(isset($search)){
	?>
	<?php echo form_submit('submit', 'Confirm All Selected','onclick="return check_selected()" class="btn btn-success btn-xs"'); ?>
	
	<br />
	<br />
	<table>
		<tr>
		  <th>Select</th>
		  <th>Reference Number</th>
		  <th>Name</th>
		  <th>Year</th>
		  <th>Course</th>
		  <th>Action</th>
		  
		</tr>
		<tbody>
		<?php
		if(!empty($search))
		{
			foreach($search as $student):
			$l = _se($student->id);
			?>
			<tr>
			<td>
				<input type='checkbox' class='checkenrollees' name='selected[]' value='<?=$student->id?>' />
			</td>
			<td><?php echo $student->studid; ?></td>
			<td><?php echo ucfirst($student->name); ?></td>
			<td><?php echo $student->year; ?></td>
			<td><?php echo $student->course; ?></td>
			<td>
				<div class="btn-group">
				<a class='btn btn-default btn-xs' href='<?=base_url('confirm_enrollees/profile/'.$student->id.'/'.$student->temp_start_id); ?>'>
					<span class="glyphicon glyphicon-folder-open"></span>&nbsp;View Profile
				</a>
				
				<a class='btn btn-primary btn-xs ' href='<?=base_url('confirm_enrollees/confirm/'.$student->id); ?>'>
					<span class="glyphicon glyphicon-thumbs-up"></span>&nbsp; Confirm
				</a>
				
				<!-- <a class='btn btn-warning btn-xs' href='<?=base_url('confirm_enrollees/unconfirm/'.$student->id); ?>'>
					<span class="glyphicon glyphicon-thumbs-down"></span>&nbsp; Unconfirm
				</a> -->
				</div>
			</td>
			</tr>
			<?php 
			endforeach; 
		}
		else
		{
		?>
		<tr>
		<td colspan="5">
			Student Not Found
		</td>
		</tr>
		<?php
		}
		?>
		</tbody>
		<tr>
		  <th>Select</th>
		  <th>Reference Number</th>
		  <th>Name</th>
		  <th>Year</th>
		  <th>Course</th>
		  <th>Action</th>
		</tr>
	</table>
	<?echo isset($links) ? $links : NULL;?>
	<?php
	}
	?>
	</div>
	
	<div class="clear"></div>
</div>
<?echo form_close();?>