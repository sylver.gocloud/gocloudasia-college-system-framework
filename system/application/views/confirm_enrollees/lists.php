<script type="text/javascript">
	
	$(document).ready(function(){
		$('#checkdate').change(function(){
			var x = $(this).attr('checked');
			
			if(x){
				$('#created_at').attr('disabled', false);
			}else{
				$('#created_at').attr('disabled', true);
			}
		});
	});
	
	function check_selected()
	{
		var x = $('.checkenrollees:checked').size();
		if(x > 0)
		{
			return true;
		}else
		{
			custom_modal('Required','<h4><span class="glyphicon glyphicon-warning-sign"></span>&nbsp; No Selected Enrollees.</h4>');
			return false;
		}
	}
	
</script>

<?php
	$search_key = false;
	
	if(isset($this->session->userdata['search_key'])){
		$search_key = $this->session->userdata['search_key'];
	}
?>

<?php 
	$formAttrib = array('id' => 'search_enrollee', 'class' => 'form-inline', 'method' => 'GET');
	echo form_open('confirm_enrollees/lists/0', $formAttrib);	
?>

<div id="right">
  
  <div class="form-group">
    <input type='checkbox' id='checkdate' name='checkdate' <?=isset($checkdate)?$checkdate:''?> />
  </div>
  
  <div class="form-group">
	
    <input id="confirm_date" class="form-control date_pick" type="text" value="<?=isset($confirm_date)?$confirm_date:date('Y-m-d')?>" name="confirm_date" placeholder="Date" <?=isset($confirm_date)?'':'disabled'?> />
  </div>
  
  <div class="form-group">
    <input id="lastname" class="form-control" type="text" value="<?=isset($lastname)?$lastname:''?>" name="lastname" placeholder="Last Name">
  </div>
   
  <div class="form-group">
    <input id="studid" class="form-control" type="text" value="<?=isset($studid)?$studid:''?>" name="studid" placeholder="Reference Number">
  </div>
  <div class="form-group">
    <?=year_dropdown('year_id',isset($year_id)?$year_id:'',"", '')?>
  </div>
  <?php echo form_submit('submit', 'Search'); ?>
<br><br>
	<?if($search):?>
		<div class="table-responsive">
			<table class='table table-hover table-bordered table-sortable'>
				<thead>
					<tr class='gray' >
					  <th>Confirm Date</th>
					  <th>Reference Number</th>
					  <th>Name</th>
					  <th>Year</th>
					  <th>Course</th>
					  <th>Action</th>
					  
					</tr>
				</thead>
				<tbody>
				<?php
				if(!empty($search))
				{
					foreach($search as $student):
					$l = _se($student->id);
					?>
					<tr>
						<td><?php echo date('m-d-Y',strtotime($student->confirm_date)); ?></td>
						<td><?php echo $student->studid; ?></td>
						<td><?php echo ucfirst($student->name); ?></td>
						<td><?php echo $student->year; ?></td>
						<td><?php echo $student->course; ?></td>
						<td><a class='btn btn-default btn-sm tp btn-circle' data-toggle="tooltip" title='View Profile' href='<?=base_url('confirm_enrollees/profile/'.__link($student->id).'/'.__link($student->temp_start_id)); ?>'><span class="glyphicon glyphicon-folder-open"></span></a></td>
					</tr>
					<?php 
					endforeach; 
				}
				else
				{
				?>
				<tr>
				<td colspan="5">
					Student Not Found
				</td>
				</tr>
				<?php
				}
				?>
				</tbody>
			</table>
		</div>
	<?echo isset($links) ? $links : NULL;?>
	<?endif;?>
	</div>
	
	<div class="clear"></div>
</div>
<?echo form_close();?>