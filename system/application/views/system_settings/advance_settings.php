<div class="row">
	<div class="col-md-12">
		<?php echo form_open(); ?>
		<div class="panel panel-primary">
			
				<div class="panel-heading">
            <i class="fa fa-bell fa-fw"></i> Student Portal
        </div>
        <div class="panel-body">
          <div class="list-group">
          	<?if(isset($sys_par) && $sys_par):?>
	            <div class="list-group-item">
	                <input type="checkbox" <?php echo $sys_par->enable_portal == "1" ? 'checked' : '' ?> name="syspar[enable_portal]"> &nbsp;
										Enable Student Portal ? 
	                <span class="pull-right text-muted small"><em><?php echo $sys_par->enable_portal == "1" ? 'Uncheck box to disable' : 'Check box to enable' ?></em>
	                </span>
	            </div>
            <?else:?>
            	<div class="alert alert-google-plus">System Parameters was not set, please call your school admin.</div>
            <?endif;?>
          </div>
          <button type="submit" name='update_portal' value='update_portal' class='btn btn-facebook' >Update</button>
        </div>
    </div>
    <?php echo form_close() ?>
	</div>
</div>