<script src="<?=base_url('assets/tinymce/tinymce.min.js')?>"></script>
<script>
        tinymce.init({selector:'textarea',
					  resize: false,
					  theme:'modern',
					  height:500,
					  entity_encoding :'raw',
					  plugins: [
        "advlist autolink lists link image charmap textcolor print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste"
    ],
					  toolbar: " undo redo | fontsizeselect styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | preview  fullpage anchor| forecolor backcolor emoticons"
					  });
</script>

<div class="row">
	<div class="panel"><strong><i class="fa fa-tag"></i>&nbsp; <?=$message->desc?></strong></div>
	<?=form_open('');?>
	<div class="large-12 columns">
		<textarea name="message_mes"><?=html_entity_decode($message->message,ENT_COMPAT);?></textarea>
	</div>
	<br class="clearfix">
	<div class="large-12 columns">
		<hr class="clearfix">
		<input type="hidden" name="message_meta" value="<?=$id;?>">
		<input type="submit" name="set_message" value="Set Message" class="btn btn-primary btn-sm">
			<a href="<?=site_url('system_settings/system_custom_message');?>" class="btn btn-default btn-sm">Go Back</a>
	</div>
	</form>

</div>
	<hr class="clearfix">