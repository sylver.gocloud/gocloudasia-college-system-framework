
 
  <p>
    <label for="province">Province</label>
	<?php 
		$data = array(
              'name'        => 'province[province]',
              'value'       => isset($province->province) ? $province->province : '',
              'maxlength'   => '100',
              'size'        => '50',
              'style'       => 'width:50%',
            );

		echo form_input($data);
	?>
  </p>
  <div class="clear"></div>

