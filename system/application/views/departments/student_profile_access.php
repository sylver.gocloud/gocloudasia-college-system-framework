<?$this->load->view('departments/_tab');?>

<div class='alert alert-success' >
	<p><i class="fa fa-info-circle fa-2x"></i>&nbsp;  This feature can only be applied to those departments that can access to students menu like "Student Search".</p>
</div>
<?echo form_open('');?>

<?
	$stud_profile = (isset($spa->stud_profile) && $spa->stud_profile == 1) ? 'checked' : '';
	$stud_grade = (isset($spa->stud_grade) && $spa->stud_grade == 1) ? 'checked' : '';
	$stud_fees = (isset($spa->stud_fees) && $spa->stud_fees == 1) ? 'checked' : '';
	$stud_otr = (isset($spa->stud_otr) && $spa->stud_otr == 1) ? 'checked' : '';
	$stud_issues = (isset($spa->stud_issues) && $spa->stud_issues == 1) ? 'checked' : '';
	
?>

<div class="well">
	<ol>
		<li><input type='checkbox' id ='stud_profile' name='stud_profile' <?=$stud_profile?> /> &nbsp; Profile</li>
		<li><input type='checkbox' id ='stud_grade' name='stud_grade' <?=$stud_grade?> /> &nbsp; Grade</li>
		<li><input type='checkbox' id ='stud_fees' name='stud_fees' <?=$stud_fees?> /> &nbsp; Fees</li>
		<li><input type='checkbox' id ='stud_otr' name='stud_otr' <?=$stud_otr?> /> &nbsp; Transcript</li>
		<li><input type='checkbox' id ='stud_issues' name='stud_issues' <?=$stud_issues?> /> &nbsp; Issues</li>
	<ol>
</div>
	
<?
	echo form_submit('submit','Save Changes');
	?>
	| <a class='btn btn-default btn-sm' href="<?php echo base_url(); ?>departments" rel="facebox" class="actionlink"><i class="fa fa-arrow-left"></i>&nbsp;  Back to list</a>
	<?
	echo form_close();
?>