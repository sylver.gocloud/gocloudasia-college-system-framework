<div class="well">
<table >
	<tr>
		<th>Enrollment ID</th>
		<th>Name</th>
		<th>Course</th>
		<th>Year Level</th>
		<th>Category</th>
		<th>Tuition Fee Per Unit</th>
		<th>Lab Fee Per Unit</th>
		<th>Misc Fee</th>
		<th>Total Units</th>
		<th>Total Lab Units </th>
		<th>Tuition Fee</th>
		<th>Lab Fee</th>
		<th>Additional Charges</th>
		<th>Total Charges</th>
		<th>Previous Account</th>
		<th>Total Deductions</th>
		<th>Total Amount</th>
		<th>Total Payment</th>
		<th>Remaining Balance</th>
	</tr>
	<?
	$ctr = 0;
	?>
	<?if($enrollments):?>
	<?foreach($enrollments as $obj):?>
		<?
			// vd($obj);
			$student = $obj['student'];
			$student_profile = $obj['student_profile'];
			$student_total = $obj['student_total'];
			$fullname = $student_profile->last_name.', '.$student_profile->first_name.' '.$student_profile->middle_name;
			
			
			$lab_fee = $obj['lab_a_fee_total'] + $obj['lab_b_fee_total'] + $obj['lab_c_fee_total'];
			
			$student_total['remaining_balance'] = $student_total['remaining_balance'] <= 0 ? 0 : $student_total['remaining_balance'];
		?>
		<tr>	
			<td><?=$obj['enrollment_id'];?></td>
			<td><?=$fullname?></td>
			<td><?=$student->course;?></td>
			<td><?=$student->year;?></td>	
			<td><?=isset($obj['student_finance']->category2) ? $obj['student_finance']->category2 : "";?></td>	
			<td><?=number_format($student_total['tuition_fee_per_unit'], 2);?></td>	
			<td><?=number_format($student_total['lab_fee_per_unit'], 2);?></td>	
			<td><?=number_format($student_total['total_misc_fee'], 2);?></td>	
			<td><?=number_format($student_total['total_units'], 2);?></td>	
			<td><?=number_format($student_total['total_lab_units'], 2);?></td>	
			<td><?=number_format($student_total['tuition_fee'], 2);?></td>	
			<td><?=number_format($lab_fee, 2);?></td>	
			<td><?=number_format($student_total['additional_charge'], 2);?></td>	
			<td><?=number_format($student_total['total_charge'], 2);?></td>	
			<td><?=number_format($student_total['previous_account'], 2);?></td>	
			<td><?=number_format($student_total['total_deduction'], 2);?></td>	
			<td><?=number_format($student_total['total_amount'], 2);?></td>	
			<td><?=number_format($student_total['total_payment'], 2);?></td>	
			<td><?=number_format($student_total['remaining_balance'], 2);?></td>	
		</tr>
	<?endforeach;?>
	<tr class=''>
		<td colspan = 18 style='text-align:right'>Total No. of Records</td>
		<td><div class='badge'><?=$total_rows?></div></td>
	</tr>
	<?else:?>
	<tr>
		<td colspan = '13'>No record found.</td>
	</tr>
	<?endif;?>
</table>
<?= $links;?>
</div>