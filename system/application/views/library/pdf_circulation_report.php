<!DOCTYPE html>
<html>
<head>
	<title>Media Master List by Subject Report</title>
	<style>
		.hdr{
			border-bottom: 2px solid gray;
			/*border-top: 2px solid gray;*/
		}
		table{
			width:100% !important;
		}

		.bold{
			font-weight: bold;
		}

		td{
			font-size: 9pt;
		}
	</style>
</head>
<body>

<!--  REPORT BY BORROWED DATE  -->
<?if(isset($report_type) && $report_type === "trndte"):?>
	<h3>Circulation Report By Borrowed Date</h3>

	<?if($transactions):?>
		
		<table cellpadding="3" >
			<tr>
				<td class="hdr bold" >Borr. Date</td>
				<td class="hdr bold">Acc. No.</td>
				<td class="hdr bold">Call No.</td>
				<td class="hdr bold">Title</td>
				<td class="hdr bold">Description</td>
				<td class="hdr bold">Barcode</td>
				<td class="hdr bold">Type</td>
				<td class="hdr bold">Subject</td>
				<td class="hdr bold">Status</td>
				<td class="hdr bold">Day(s)</td>
				<td class="hdr bold">Exp. Ret.</td>
				<td class="hdr bold">Ret. Date</td>
				<td class="hdr bold">Borr. Type</td>
				<td class="hdr bold">Borrower</td>
			</tr>
			<?php
				$c_date = false;
				$c_pass = false;
				$ret_count = 0;
				$unret_count = 0;
				$tot_ret_count = 0;
				$tot_unret_count = 0;
				$l_ctr = 1;
				$all_ctr;
			?>
			<?foreach ($transactions as $k => $t):?>

				<?php 
					/** DISPLAY TOTALS FOR EACH DATE **/
						if($k === 0){
							$c_date = $t->trndte;
						}else{
							if($c_date !== $t->trndte){
								?>
									<tr>
										<td colspan="10" >
											<p><strong>Sub-total &nbsp; : &nbsp; <?=$l_ctr;?></strong></p>
										</td>
									</tr>
								<?
								$l_ctr = 1;
								$c_date = $t->trndte;
							}else{
								$l_ctr++;
							}
						}
				?>

				<tr class="body_tr" >
					<td><?=$t->trndte?></td>
					<td><?=$t->accession_number?></td>
					<td><?=$t->call_number?></td>
					<td><?=$t->book_name?></td>
					<td><?=$t->book_desc?></td>
					<td><?=$t->book_barcode?></td>
					<td><?=$t->media_type?></td>
					<td><?=$t->category?></td>
					<td><?=ucwords($t->cir_status)?></td>
					<td><?=ucwords($t->day)?></td>
					<td><?=date('m-d-Y',strtotime($t->trndte_raw) + (24*3600*$t->day));?></td>
					<td><?=$t->retdte?></td>
					<td><?=ucwords($t->usertype)?></td>
					<td>
						<?php
							if(isset($borrowers[$t->id])){

								$u = $borrowers[$t->id];

								if($t->usertype === "student"){
									$fullname = $u->full_name." (". $u->course_code.' | '. $u->year .") ";
								}else{
									// vp($u);
									$fullname = $u->fullname." (".ucwords($u->department).")";
								}

								echo ucwords($fullname);
							}
						?>
					</td>
				</tr>
			<?endforeach;?>
			<tr>
				<td colspan="10" >
					<p><strong>Sub-total &nbsp; : &nbsp; <?=$l_ctr;?></strong></p>
				</td>
			</tr>
		</table>
		<p><strong>Total of all Copies &nbsp; : &nbsp; <?=count($transactions);?></strong></p>
	<?endif;?>
<?endif;?>

<!--  REPORT BY RETURN DATE  -->
<?if(isset($report_type) && $report_type === "retdte"):?>
	<h3>Circulation Report By Return Date</h3>

	<?if($transactions):?>
		
		<table cellpadding="3" >
			<tr>
				<td class="hdr bold" >Ret. Date</td>
				<td class="hdr bold">Acc. No.</td>
				<td class="hdr bold">Call No.</td>
				<td class="hdr bold">Title</td>
				<td class="hdr bold">Description</td>
				<td class="hdr bold">Barcode</td>
				<td class="hdr bold">Type</td>
				<td class="hdr bold">Subject</td>
				<td class="hdr bold">Status</td>
				<td class="hdr bold">Day(s)</td>
				<td class="hdr bold">Exp. Ret.</td>
				<td class="hdr bold">Borr. Date</td>
				<td class="hdr bold">Borr. Type</td>
				<td class="hdr bold">Borrower</td>
			</tr>
			<?php
				$c_date = false;
				$c_pass = false;
				$ret_count = 0;
				$unret_count = 0;
				$tot_ret_count = 0;
				$tot_unret_count = 0;
				$l_ctr = 1;
				$all_ctr;
			?>
			<?foreach ($transactions as $k => $t):?>

				<?php 
					/** DISPLAY TOTALS FOR EACH DATE **/
						if($k === 0){
							$c_date = $t->retdte;
						}else{
							if($c_date !== $t->retdte){
								?>
									<tr>
										<td colspan="10" >
											<p><strong>Sub-total &nbsp; : &nbsp; <?=$l_ctr;?></strong></p>
										</td>
									</tr>
								<?
								$l_ctr = 1;
								$c_date = $t->retdte;
							}else{
								$l_ctr++;
							}
						}
				?>

				<tr class="body_tr" >
					<td><?=$t->retdte?></td>
					<td><?=$t->accession_number?></td>
					<td><?=$t->call_number?></td>
					<td><?=$t->book_name?></td>
					<td><?=$t->book_desc?></td>
					<td><?=$t->book_barcode?></td>
					<td><?=$t->media_type?></td>
					<td><?=$t->category?></td>
					<td><?=ucwords($t->cir_status)?></td>
					<td><?=ucwords($t->day)?></td>
					<td><?=date('m-d-Y',strtotime($t->trndte_raw) + (24*3600*$t->day));?></td>
					<td><?=$t->trndte?></td>
					<td><?=ucwords($t->usertype)?></td>
					<td>
						<?php
							if(isset($borrowers[$t->id])){

								$u = $borrowers[$t->id];

								if($t->usertype === "student"){
									$fullname = $u->full_name." (". $u->course_code.' | '. $u->year .") ";
								}else{
									// vp($u);
									$fullname = $u->fullname." (".ucwords($u->department).")";
								}

								echo ucwords($fullname);
							}
						?>
					</td>
				</tr>
			<?endforeach;?>
			<tr>
				<td colspan="10" >
					<p><strong>Sub-total &nbsp; : &nbsp; <?=$l_ctr;?></strong></p>
				</td>
			</tr>
		</table>
		<p><strong>Total of all Copies &nbsp; : &nbsp; <?=count($transactions);?></strong></p>
	<?endif;?>
<?endif;?>

<!--  REPORT BY BORROWER  -->
<?if(isset($report_type) && $report_type === "borrower"):?>

	<!-- REPORT BY STUDENT -->
	<?if($usertype === "student"):?>
		<h3>Circulation Report By Borrower (Student)</h3>

		<?if($transactions):?>
					
			<table cellpadding="3" border = "1" >
				<tr>
					<td class="hdr bold" >Borr.Date</td>
					<td class="hdr bold">Acc. No.</td>
					<td class="hdr bold">Call No.</td>
					<td class="hdr bold">Title</td>
					<td class="hdr bold">Description</td>
					<td class="hdr bold">Barcode</td>
					<td class="hdr bold">Type</td>
					<td class="hdr bold">Category</td>
					<td class="hdr bold">Status</td>
					<td class="hdr bold">Day(s)</td>
					<td class="hdr bold">Exp. Ret.</td>
					<td class="hdr bold">Ret. Date</td>
				</tr>
				<?php
					$c_date = false;
					$c_pass = false;
					$ret_count = 0;
					$unret_count = 0;
					$tot_ret_count = 0;
					$tot_unret_count = 0;
					$l_ctr = 1;
					$all_ctr;
					$b_id = false;
				?>
				<?foreach ($transactions as $k => $t):?>

					<?php 
						// vp($t);
						/** DISPLAY TOTALS FOR EACH DATE **/
							if($k === 0){
								$b_id = $t->borrower_id;

								?>
										<!-- <tr>
											<td colspan="10" >
												<p><strong>Sub-total &nbsp; : &nbsp; <?=$l_ctr;?></strong></p>
											</td>
										</tr> -->

										<!-- Display Borrowers in First Loop -->
										<tr>
											<td colspan="10" >
												<p><strong><?=$t->borrowers_fullname;?> &nbsp; | &nbsp; <?=$t->course_code?> | <?=$t->year_level?></strong></p>
											</td>
										</tr>
								<?
							}else{
								if($b_id !== $t->borrower_id){
									?>
										<tr>
											<td colspan="10" >
												<p><strong>Sub-total &nbsp; : &nbsp; <?=$l_ctr;?></strong></p>
											</td>
										</tr>

										<!-- Display Borrowers in First Loop -->
										<tr>
											<td colspan="10" >
												<p><strong><?=$t->borrowers_fullname;?> &nbsp; | &nbsp; <?=$t->course_code?> | <?=$t->year_level?></strong></p>
											</td>
										</tr>
									<?
									$l_ctr = 1;
									$b_id = $t->borrower_id;
								}else{
									$l_ctr++;
								}
							}
					?>

					<tr class="body_tr" >
						<td><?=$t->trndte?></td>
						<td><?=$t->accession_number?></td>
						<td><?=$t->call_number?></td>
						<td><?=$t->book_name?></td>
						<td><?=$t->book_desc?></td>
						<td><?=$t->book_barcode?></td>
						<td><?=$t->media_type?></td>
						<td><?=$t->category?></td>
						<td><?=ucwords($t->cir_status)?></td>
						<td><?=ucwords($t->day)?></td>
						<td><?=date('m-d-Y',strtotime($t->trndte_raw) + (24*3600*$t->day));?></td>
						<td><?=$t->retdte?></td>
					</tr>
				<?endforeach;?>

				<?if($l_ctr > 1):?>
					<tr>
						<td colspan="10" >
							<p><strong>Sub-total &nbsp; : &nbsp; <?=$l_ctr;?></strong></p>
						</td>
					</tr>
				<?endif;?>

			</table>
			<p><strong>Total of all Copies &nbsp; : &nbsp; <?=count($transactions);?></strong></p>
		<?else:?>
			<p>No transaction found.</p>
		<?endif;?>
	<?endif;?>

	<!-- REPORT BY EMPLOYEE -->
	<?if($usertype === "employee"):?>
		<h3>Circulation Report By Borrower (Employee)</h3>

		<?if($transactions):?>
			
			<table cellpadding="3" >
				<tr>
					<td class="hdr bold" >Borr.Date</td>
					<td class="hdr bold">Acc. No.</td>
					<td class="hdr bold">Call No.</td>
					<td class="hdr bold">Title</td>
					<td class="hdr bold">Description</td>
					<td class="hdr bold">Barcode</td>
					<td class="hdr bold">Type</td>
					<td class="hdr bold">Category</td>
					<td class="hdr bold">Status</td>
					<td class="hdr bold">Day(s)</td>
					<td class="hdr bold">Exp. Ret.</td>
					<td class="hdr bold">Ret. Date</td>
				</tr>
				<?php
					$c_date = false;
					$c_pass = false;
					$ret_count = 0;
					$unret_count = 0;
					$tot_ret_count = 0;
					$tot_unret_count = 0;
					$l_ctr = 1;
					$all_ctr;
					$b_id = false;
				?>
				<?foreach ($transactions as $k => $t):?>

					<?php 
						// vp($t);
						/** DISPLAY TOTALS FOR EACH DATE **/
							if($k === 0){
								$b_id = $t->borrower_id;

								?>
										<!-- Display Borrowers in First Loop -->
										<tr>
											<td colspan="10" >
												<p><strong><?=$t->borrowers_fullname;?> &nbsp;   ( <?= ucwords($t->role) ?> )</strong></p>
											</td>
										</tr>
								<?
							}else{
								if($b_id !== $t->borrower_id){
									?>
										<tr>
											<td colspan="10" >
												<p><strong>Sub-total &nbsp; : &nbsp; <?=$l_ctr;?></strong></p>
											</td>
										</tr>

										<!-- Display Borrowers in First Loop -->
										<tr>
											<td colspan="10" >
												<p><strong><?=$t->borrowers_fullname;?> &nbsp;   ( <?= ucwords($t->role) ?> )</strong></p>
											</td>
										</tr>
									<?
									$l_ctr = 1;
									$b_id = $t->borrower_id;
								}else{
									$l_ctr++;
								}
							}
					?>

					<tr class="body_tr" >
						<td><?=$t->trndte?></td>
						<td><?=$t->accession_number?></td>
						<td><?=$t->call_number?></td>
						<td><?=$t->book_name?></td>
						<td><?=$t->book_desc?></td>
						<td><?=$t->book_barcode?></td>
						<td><?=$t->media_type?></td>
						<td><?=$t->category?></td>
						<td><?=ucwords($t->cir_status)?></td>
						<td><?=ucwords($t->day)?></td>
						<td><?=date('m-d-Y',strtotime($t->trndte_raw) + (24*3600*$t->day));?></td>
						<td><?=$t->retdte?></td>
					</tr>
				<?endforeach;?>
				<tr>
					<td colspan="10" >
						<p><strong>Sub-total &nbsp; : &nbsp; <?=$l_ctr;?></strong></p>
					</td>
				</tr>
			</table>
			<p><strong>Total of all Copies &nbsp; : &nbsp; <?=count($transactions);?></strong></p>
		<?else:?>
			<p>No transaction found.</p>
		<?endif;?>
	<?endif;?>
<?endif;?>

<!--  REPORT BY RETURN MEDIA  -->
<?if(isset($report_type) && $report_type === "media"):?>
	<h3>Circulation Report By Media</h3>

	<?if($transactions):?>
		
		<table cellpadding="3" >
			<tr>
				<td class="hdr bold">Acc. No.</td>
				<td class="hdr bold">Call No.</td>
				<td class="hdr bold">Title</td>
				<td class="hdr bold">Description</td>
				<td class="hdr bold">Barcode</td>
				<td class="hdr bold">Type</td>
				<td class="hdr bold">Category</td>
				<td class="hdr bold">Borr. Date</td>
				<td class="hdr bold">Day(s)</td>
				<td class="hdr bold">Status</td>
				<td class="hdr bold">Exp. Ret.</td>
				<td class="hdr bold">Exp. Ret.</td>
				<td class="hdr bold">Borr. Type</td>
				<td class="hdr bold">Borrower</td>
			</tr>
			<?php
				$book_name = false;
				$c_pass = false;
				$ret_count = 0;
				$unret_count = 0;
				$tot_ret_count = 0;
				$tot_unret_count = 0;
				$l_ctr = 1;
				$all_ctr;
			?>
			<?foreach ($transactions as $k => $t):?>

				<?php 
					/** DISPLAY TOTALS FOR EACH DATE **/
						if($k === 0){
							$book_name = $t->book_name;
						}else{
							if($book_name !== $t->book_name){
								?>
									<tr>
										<td colspan="10" >
											<p><strong>Sub-total &nbsp; : &nbsp; <?=$l_ctr;?></strong></p>
										</td>
									</tr>
								<?
								$l_ctr = 1;
								$book_name = $t->book_name;
							}else{
								$l_ctr++;
							}
						}
				?>

				<tr class="body_tr" >
					<td><?=$t->accession_number?></td>
					<td><?=$t->call_number?></td>
					<td><?=$t->book_name?></td>
					<td><?=$t->book_desc?></td>
					<td><?=$t->book_barcode?></td>
					<td><?=$t->media_type?></td>
					<td><?=$t->category?></td>
					<td><?=$t->trndte?></td>
					<td><?=ucwords($t->cir_status)?></td>
					<td><?=ucwords($t->day)?></td>
					<td><?=date('m-d-Y',strtotime($t->trndte_raw) + (24*3600*$t->day));?></td>
					<td><?=$t->retdte?></td>
					<td><?=ucwords($t->usertype)?></td>
					<td>
						<?php
							if(isset($borrowers[$t->id])){

								$u = $borrowers[$t->id];

								if($t->usertype === "student"){
									$fullname = $u->full_name." (". $u->course_code.' | '. $u->year .") ";
								}else{
									// vp($u);
									$fullname = $u->fullname." (".ucwords($u->department).")";
								}

								echo ucwords($fullname);
							}
						?>
					</td>
				</tr>
			<?endforeach;?>
			<tr>
				<td colspan="10" >
					<p><strong>Sub-total &nbsp; : &nbsp; <?=$l_ctr;?></strong></p>
				</td>
			</tr>
		</table>
		<p><strong>Total of all Copies &nbsp; : &nbsp; <?=count($transactions);?></strong></p>
	<?endif;?>
<?endif;?>
</body>
</html>