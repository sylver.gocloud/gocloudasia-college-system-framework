<?php
  //get value of the dropdown
  $dropvalue = $usertype == 'students' ? 'Student ID Number' : 'User Id / User Name';
  $formurl = $usertype == 'students' ? 'librarian/students/0' : 'librarian/employees/0';
  if(isset($search_param) && $search_param){
    switch ($search_param) {
      case 'student_id':
        $dropvalue = $usertype == 'students' ? 'Student ID Number' : 'User Id / User Name';
        break;
      case 'last_name':
        $dropvalue = 'Last Name';
        break;
      case 'first_name':
        $dropvalue = 'First Name';
        break;
      case 'middle_name':
        $dropvalue = 'Middle Name';
        break;
      case 'all':
        $dropvalue = 'Show All';
        break;
      default:
        # code...
        break;
    }
  }
?>
<!-- Nav tabs -->
  <?if($usertype === "students"):?>
  	<ul class="nav nav-tabs" role="tablist">
  	  <li class="active"><a style='color:blue' href="#home" role="xtab" data-toggle="xtab"><i class="fa fa-user"></i>&nbsp; Students</a></li>
  	  <li><a href="<?=site_url('librarian/employees')?>" role="xtab" data-toggle="xtab"><i class="fa fa-users"></i>&nbsp; Employees</a></li>
  	</ul>
  <?else:?>
    <ul class="nav nav-tabs" role="tablist">
      <li><a href="<?=site_url('librarian/students')?>" role="xtab" data-toggle="xtab"><i class="fa fa-user"></i>&nbsp; Students</a></li>
      <li class="active"><a style='color:blue' href="#profile" role="xtab" data-toggle="xtab"><i class="fa fa-users"></i>&nbsp; Employees</a></li>
    </ul>
  <?endif;?>
  <script type="text/javascript" src="<?php echo script_url('custom/bootstrapv3.2.0'); ?>"></script>
  <script>
  $(document).ready(function(e){

    $('.search-panel span#search_concept').text("<? echo $dropvalue; ?>");

      $('.search-panel .dropdown-menu').find('a').click(function(e) {
        e.preventDefault();
        var param = $(this).attr("href").replace("#","");
        var concept = $(this).text();
        $('.search-panel span#search_concept').text(concept);
        $('.input-group #search_param').val(param);
      });
    });
</script>
	
	<?=form_open($formurl,'method="GET"')?>
	 <div class="input-group">
        <div class="input-group-btn search-panel">
            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
            	<span id="search_concept">Filter by</span> <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" role="menu">
              <?if($usertype === "students"):?>
                <li><a href="#student_id">Student ID Number</a></li>
              <?else:?>
                <li><a href="#student_id">User Id / User Name</a></li>
              <?endif;?>
              <li><a href="#last_name">Last Name</a></li>
              <li><a href="#first_name">First name</a></li>
              <li><a href="#middle_name">Middle name</a></li>
              <li class="divider"></li>
              <li><a href="#all">Show All</a></li>
            </ul>
        </div>
        <input type="hidden" name="search_param" value="<?=isset($search_param)?$search_param:'student_id';?>" id="search_param">         
        <input type="text" class="form-control" name="keyword" value="<?=isset($keyword)?$keyword:''?>" placeholder="Search term...">
        <span class="input-group-btn">
            <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></span></button>
        </span>
    </div>
  </form>