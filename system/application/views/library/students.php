<div class="row">

	<h3>Search <small>Borrower</small></h3>
	
	<?@$this->load->view('library/_borrower_tab');?>
	<p></p>
	<!-- Tab panes -->
	<div class="tab-content">
	  <div class="tab-pane active" id="home">
			<?if(!empty($search)):?>
				<table class="table table-striped table-hover">
				<thead>
				<tr>
					<th>Student ID</th>
					<th>Student Name</th>
					<th>Course</th>
					<th>Grade Level</th>
					<th>&nbsp;</th>
				</tr>
				</thead>
				<tbody>
				<?foreach($search as $k => $student):?>
					<tr>
						<td><?=$student->studid?></td>
						<td><?=ucwords(strtolower($student->name));?></td>
						<td><?=ucwords($student->course);?></td>
						<td><?=$student->year;?></td>
						<td>
							<a href="<?=site_url('library/circulation/student/'.$student->id)?>" class="btn btn-sm btn-success"><i class="fa fa-book"></i>&nbsp; Profile</a>
						</td>
					</tr>
				<?endforeach;?>
				</tbody>
				</table>
			<?else:?>
				<div class="alert-box">
					<i class="icon-exclamation-sign icon-white"></i> Nothing found!
				</div>
			<?endif;?>
			<?=$links;?>
			
	  </div>

	  <div class="tab-pane" id="profile"></div>
	</div>

</div>