<!DOCTYPE html>
<html>
<head>
	<title>Media Master List by Category Report</title>
</head>
<body>
<h3><?=ucwords($school_name);?></h3>
<p><strong>Report : Master List of Library Media</strong></p>
<hr/>
<style>
	.hdr{
		border-bottom: 2px solid gray;
		/*border-top: 2px solid gray;*/
	}
	table{
		width:100% !important;
	}
</style>
<div class="table-responsive">
	<?if($search):?>
		<table style="width:100%; border: 1px solid black;">
			<tr style="background: gray;">
    			<td class="hdr" >Type</td>
    			<td class="hdr" >Category</td>
    			<td class="hdr"  >Item Name</td>
    			<td class="hdr"  >Description</td>
    			<td class="hdr"  >Barcode</td>
    			<td class="hdr"  >Author</td>
    			<td class="hdr"  >ISBN</td>
    			<td class="hdr"  >Publisher</td>
    			<td class="hdr"  >Date Pub.</td>
    			<td class="hdr"  >Qty</td>
    			<!-- <td class="hdr"  >Available Copy</td> -->
			</tr>
			<?php
				$total_copy = 0;
			?>
			<?foreach ($search as $key => $v):?>
				<?php
					$total_copy += $v->book_copies;
					$c_total += $v->book_copies;
				?>
				<tr>
					<td><?=$v->media_type?></td>
					<td><?=$v->category?></td>
					<td><?=$v->book_name?></td>
					<td><?=$v->book_desc?></td>
					<td><?=$v->book_barcode?></td>
					<td><?=$v->book_author?></td>
					<td><?=$v->book_isbn?></td>
					<td><?=$v->book_publisher?></td>
					<td><?= $v->book_dop && $v->book_dop !== "1970-01-01" ? date('m/d/Y',strtotime($v->book_dop)) : '';?></td>
					<td><?=$v->book_copies?></td>
					<!-- <td><?=$v->available?></td> -->
				</tr>
			<?endforeach;?>
			<tr>
				<td colspan="9" >&nbsp; <br></td>
			</tr>
			<tr>
				<td colspan="9" ><strong></td>
			</tr>
		</table>
		<p>Total Media : &nbsp; <?=count($search);?></strong></p>
		<p>Total Copies : &nbsp; <?=$total_copy?></strong></p>
	<?else:?>
	<p>----------No Record Found.------------</p>
	<?endif;?>
</div>
</body>
</html>