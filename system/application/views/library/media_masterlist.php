<?=panel_head('Media Master List')?>

	<ul class="nav nav-tabs" role="tablist" id="myTab">
	  <li class="active"><a href="#type" role="tab" data-toggle="tab">By Type</a></li>
	  <li><a href="#category" role="tab" data-toggle="tab">By Category</a></li>
	  <li><a href="#all" role="tab" data-toggle="tab">All Media</a></li>
	</ul>
	<p></p>
	<div class="tab-content">
	  <div class="tab-pane active" id="type">
			<div class="table-responsive">
				<?if(($type)):?>
				
				<div class="btn-group">
				  <a target="_blank" href="<?=site_url('pdf/media_type_master')?>" class="btn btn-success"><i class="fa fa-print"></i>&nbsp; Print All (PDF)</a>
				  <a target="_blank" href="<?=site_url('library/download_media_type_master')?>" class="btn btn-default"><i class="fa fa-download"></i>&nbsp; Download All (CSV)</a>
				</div><p/>

				<table class="table">
					<tr class="info">
						<td>Types <span class="badge"><?=count($type)?></span> </td>
						<td>Media or Item Count</td>
						<td>Action</td>
					</tr>
					<?php
						$m_total = 0;
					?>
					<?foreach ($type as $kt => $t):?>
						<?php
							$head = $t['head']; // type
							$tail = $t['tail']; // books
							$count = $t['tail'] ? count($t['tail']) : 0; // books
							$m_total += $count;
						?>
						<tr>
							<td><?=$head->media_type?></td>
							<td><span class="badge"> <?=$count?></span></td>
							<td>
								<a target="_blank" href="<?=site_url('pdf/media_type_master/'.$head->id)?>" class="confirm btn btn-default btn-mini top"  data-toggle="tooltip" data-placement="top" title="Print PDF" ><i class="glyphicon glyphicon-print"></i></a>
								<a target="_blank" href="<?=site_url('library/download_media_type_master/'.$head->id)?>" class="confirm btn btn-default btn-mini top"  data-toggle="tooltip" data-placement="top" title="Download CSV" ><i class="glyphicon glyphicon-download-alt"></i></a>
								<a target="_blank" href="<?=site_url('library/index/0?item_type='.$head->id)?>" class="btn btn-default btn-mini top"  data-toggle="tooltip" data-placement="top" title="Go to Search" ><i class="fa fa-folder-open"></i>&nbsp;</a>
							</td>
						</tr>
					<?endforeach;?>
					<tr class="info" >
						<td>Total</td>
						<td><span class="badge"> <?=$m_total?></span></td>
						<td></td>
					</tr>
				</table>
				<?else:?>
					<p>No record Found</p>
				<?endif;?>
			</div>
	  </div>

	  <div class="tab-pane" id="category">
	  		<div class="table-responsive">
				<?if(($category)):?>
				
				<div class="btn-group">
				  <a target="_blank" href="<?=site_url('pdf/media_category_master')?>" class="btn btn-success"><i class="fa fa-print"></i>&nbsp; Print All (PDF)</a>
				  <a target="_blank" href="<?=site_url('library/download_media_category_master')?>" class="confirm btn btn-default"><i class="fa fa-download"></i>&nbsp; Download All (CSV)</a>
				</div><p/>

				<table class="table">
					<tr class="info">
						<td>Category</td>
						<td>Media or Item Count</td>
						<td>Action</td>
					</tr>
					<?php
						$m_total = 0;
					?>
					<?foreach ($category as $kt => $c):?>
						<?php
							$head = $c['head']; // caterogy
							$tail = $c['tail']; // books
							$count = $c['tail'] ? count($c['tail']) : 0; // books
							$m_total += $count;
						?>
						<tr>
							<td><?=$head->lbc_name?></td>
							<td><span class="badge"> <?=$count?></span></td>
							<td>
								<a target="_blank" href="<?=site_url('pdf/media_category_master/'.$head->id)?>" class="confirm btn btn-default btn-mini top"  data-toggle="tooltip" data-placement="top" title="Print PDF" ><i class="glyphicon glyphicon-print"></i></a>
								<a target="_blank" href="<?=site_url('library/download_media_category_master/'.$head->id)?>" class="confirm btn btn-default btn-mini top"  data-toggle="tooltip" data-placement="top" title="Download CSV" ><i class="glyphicon glyphicon-download-alt"></i></a>
								<a target="_blank" href="<?=site_url('library/index/0?item_category='.$head->id)?>" class="btn btn-default btn-mini top"  data-toggle="tooltip" data-placement="top" title="Go to Search" ><i class="fa fa-folder-open"></i>&nbsp;</a>
							</td>
						</tr>
					<?endforeach;?>
					<tr class="info" >
						<td>Total</td>
						<td><span class="badge"> <?=$m_total?></span></td>
						<td></td>
					</tr>
				</table>
				<?else:?>
					<p>No record Found</p>
				<?endif;?>
			</div>
	  </div>

	  <div class="tab-pane" id="all">
	  		<div class="btn-group">
			  <a target="_blank" href="<?=site_url('pdf/all_library_media')?>" class="btn btn-success"><i class="fa fa-print"></i>&nbsp; Print All (PDF)</a>
			  <a target="_blank" href="<?=site_url('library/download_all_media_master')?>" class="confirm btn btn-default"><i class="fa fa-download"></i>&nbsp; Download All (CSV)</a>
			</div>		
	  </div>
	</div>


<?=panel_tail();?>