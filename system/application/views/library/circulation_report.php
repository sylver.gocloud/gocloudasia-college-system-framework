<?=panel_head('Circulation Report')?>
	<br>
	<ul class="nav nav-tabs" role="tablist" id="myTab">
	  <li class="active"><a href="#date" role="tab" data-toggle="tab"><i class="fa fa-calendar"></i>&nbsp; By Borrowed Date</a></li>
	  <li class=""><a href="#retdate" role="tab" data-toggle="tab"><i class="fa fa-calendar"></i>&nbsp; By Return Date</a></li>
	  <li class=""><a href="#borrower" role="tab" data-toggle="tab"><i class="fa fa-user"></i>&nbsp; By Borrower</a></li>
	  <li><a href="#Media" role="tab" data-toggle="tab"><i class="fa fa-book"></i>&nbsp; By Media</a></li>
	</ul>

	<div class="tab-content">

		<div class="tab-pane active" id="date">
				<?=form_open('pdf/circulation_report');?>
				<input type="hidden" name="tab" value="date" />
	  		<div class="responsive">
	  			<table class="table">
	  				<tr>
	  					<td>Borrowed Date From</td>
	  					<td><input class="trndte_from" placeHolder="yyyy-mm-dd" type="text" name="trndte[from]" value="<?=date('Y-m-1')?>" required></td>
	  				</tr>

	  				<tr>
	  					<td>Borrowed Date To</td>
	  					<td><input class="trndte_to" placeHolder="yyyy-mm-dd" type="text" name="trndte[to]" value="<?=date('Y-m-d')?>" required></td>
	  				</tr>

	  				<tr>
	  					<td>Type</td>
	  					<td><?=form_dropdown('trndte[media_type_id]', $media_types, isset($item_type)?$item_type:'');?></td>
	  				</tr>

	  				<tr>
	  					<td>Subject</td>
	  					<td><?=form_dropdown('trndte[book_category]', $book_category, isset($item_category)?$item_category:'');?></td>
	  				</tr>

	  				<tr>
	  					<td>Status</td>
	  					<td>
	  						<?php $status_o = array(''=>'ALL STATUS','RETURN'=>'RETURN','UNRETURN'=>'UNRETURN') ?>
	  						<?=form_dropdown('trndte[cir_status]', $status_o);?>
	  					</td>
	  				</tr>

	  				<tr>
	  					<td>Report Orientation</td>
	  					<td>
	  						<?php $ori = array('P'=>'Portrait','L'=>'Landscape') ?>
	  						<?=form_dropdown('trndte[orientation]', $ori);?>
	  					</td>
	  				</tr>

	  				<tr>
	  					<td></td>
	  					<td><button class="btn btn-success" type="submit" name="print_by_date" value="print_by_date" ><i class="fa fa-print"></i>&nbsp; Generate Report</button></td>
	  				</tr>
	  			</table>
	  		</div>
	  		<?=form_close();?>
	  </div>

	  <div class="tab-pane" id="retdate">
				<?=form_open('pdf/circulation_report');?>
				<input type="hidden" name="tab" value="date" />
	  		<div class="responsive">
	  			<table class="table">
	  				<tr>
	  					<td>Return Date From</td>
	  					<td><input class="retdte_from" placeHolder="yyyy-mm-dd" type="text" name="retdte[from]" value="<?=date('Y-m-d')?>" required></td>
	  				</tr>

	  				<tr>
	  					<td>Return Date To</td>
	  					<td><input class="retdte_to" placeHolder="yyyy-mm-dd" type="text" name="retdte[to]" value="<?=date('Y-m-d')?>" required></td>
	  				</tr>

	  				<tr>
	  					<td>Type</td>
	  					<td><?=form_dropdown('item_type', $media_types, isset($item_type)?$item_type:'');?></td>
	  				</tr>

	  				<tr>
	  					<td>Subject</td>
	  					<td><?=form_dropdown('item_category', $book_category, isset($item_category)?$item_category:'');?></td>
	  				</tr>

	  				<tr>
	  					<td>Report Orientation</td>
	  					<td>
	  						<?php $ori = array('P'=>'Portrait','L'=>'Landscape') ?>
	  						<?=form_dropdown('retdte[orientation]', $ori);?>
	  					</td>
	  				</tr>

	  				<tr>
	  					<td></td>
	  					<td><button class="btn btn-success" type="submit" name="print_by_ret_date" value="print_by_ret_date" ><i class="fa fa-print"></i>&nbsp; Generate Report</button></td>
	  				</tr>
	  			</table>
	  		</div>
	  		<?=form_close();?>
	  </div>

	  <div class="tab-pane" id="borrower">
			<?=form_open('pdf/circulation_report');?>
	  		<div class="responsive">
	  			<table class="table">
	  				<tr>
	  					<td>Borrowers Type</td>
	  					<td>
	  						<input type="radio" name="borrower[usertype]" value="student" checked id="bor_inc_stud"> &nbsp; Student <br/>
	  						<input type="radio" name="borrower[usertype]" value="employee" id="bor_inc_emp"> &nbsp; Employee
	  					</td>
	  				</tr>

	  				<tr class="borr_borruser_student" >
	  					<td>Borrowers From</td>
	  					<td><?=form_dropdown('borrower[user_from]', $students,'');?></td>
	  				</tr>

	  				<tr class="borr_borruser_student" >
	  					<td>Borrowers To</td>
	  					<td><?=form_dropdown('borrower[user_to]', $students, '');?></td>
	  				</tr>

						<tr class="borr_borruser_employee" style="display:none;" >
	  					<td>Employee From</td>
	  					<td><?=form_dropdown('borrower[emp_from]', $employees, '');?></td>
	  				</tr>

	  				<tr class="borr_borruser_employee" style="display:none;" >
	  					<td>Employee To</td>
	  					<td><?=form_dropdown('borrower[emp_to]', $employees, '');?></td>
	  				</tr>


	  				<tr>
	  					<td>Borrowed Date From</td>
	  					<td><input class="borr_from" placeHolder="yyyy-mm-dd" type="text" name="borrower[from]" value="<?=date('Y-m-1')?>" required></td>
	  				</tr>

	  				<tr>
	  					<td>Borrowed Date To</td>
	  					<td><input class="borr_to" placeHolder="yyyy-mm-dd" type="text" name="borrower[to]" value="<?=date('Y-m-d')?>" required></td>
	  				</tr>

	  				<tr>
	  					<td>Type</td>
	  					<td><?=form_dropdown('borrower[media_type_id]', $media_types, isset($item_type)?$item_type:'');?></td>
	  				</tr>

	  				<tr>
	  					<td>Subject</td>
	  					<td><?=form_dropdown('borrower[book_category]', $book_category, isset($item_category)?$item_category:'');?></td>
	  				</tr>

	  				<tr>
	  					<td>Status</td>
	  					<td>
	  						<?php $status_o = array(''=>'ALL STATUS','RETURN'=>'RETURN','UNRETURN'=>'UNRETURN') ?>
	  						<?=form_dropdown('borrower[cir_status]', $status_o);?>
	  					</td>
	  				</tr>

	  				<tr>
	  					<td>Report Orientation</td>
	  					<td>
	  						<?php $ori = array('P'=>'Portrait','L'=>'Landscape') ?>
	  						<?=form_dropdown('borrower[orientation]', $ori);?>
	  					</td>
	  				</tr>

	  				<tr>
	  					<td></td>
	  					<td><button class="btn btn-success" type="submit" name="print_by_borrower" value="print_by_borrower" ><i class="fa fa-print"></i>&nbsp; Generate Report</button></td>
	  				</tr>
	  			</table>
	  		</div>
	  		<?=form_close()?>
	  </div>

	  <div class="tab-pane" id="Media">
	  		<?=form_open('pdf/circulation_report');?>
				<input type="hidden" name="tab" value="date" />
	  		<div class="responsive">
	  			<table class="table">
	  				<tr>
	  					<td>Media From</td>
	  					<td><?=form_dropdown('media[media_from]', $medias);?></td>
	  				</tr>

	  				<tr>
	  					<td>Media To</td>
	  					<td><?=form_dropdown('media[media_to]', $medias);?></td>
	  				</tr>

	  				<tr>
	  					<td>Borrowed Date From</td>
	  					<td><input class="media_from" placeHolder="yyyy-mm-dd" type="text" name="media[from]" value="<?=date('Y-m-1')?>" required></td>
	  				</tr>

	  				<tr>
	  					<td>Borrowed Date To</td>
	  					<td><input class="media_to" placeHolder="yyyy-mm-dd" type="text" name="media[to]" value="<?=date('Y-m-d')?>" required></td>
	  				</tr>

	  				<tr>
	  					<td>Type</td>
	  					<td><?=form_dropdown('media[media_type_id]', $media_types, isset($item_type)?$item_type:'');?></td>
	  				</tr>

	  				<tr>
	  					<td>Subject</td>
	  					<td><?=form_dropdown('media[book_category]', $book_category, isset($item_category)?$item_category:'');?></td>
	  				</tr>

	  				<tr>
	  					<td>Status</td>
	  					<td>
	  						<?php $status_o = array(''=>'ALL STATUS','RETURN'=>'RETURN','UNRETURN'=>'UNRETURN') ?>
	  						<?=form_dropdown('media[cir_status]', $status_o);?>
	  					</td>
	  				</tr>

	  				<tr>
	  					<td>Report Orientation</td>
	  					<td>
	  						<?php $ori = array('P'=>'Portrait','L'=>'Landscape') ?>
	  						<?=form_dropdown('media[orientation]', $ori);?>
	  					</td>
	  				</tr>

	  				<tr>
	  					<td></td>
	  					<td><button class="btn btn-success" type="submit" name="print_by_media" value="print_by_media" ><i class="fa fa-print"></i>&nbsp; Generate Report</button></td>
	  				</tr>
	  			</table>
	  		</div>
	  		<?=form_close();?>
	  </div>
	</div>


<?=panel_tail();?>

<script>
	$(function(){

		$( ".trndte_from" ).datepicker({
    	numberOfMonths: 2,
      onSelect: function(selected) {
         $(".trndte_to").datepicker("option","minDate", selected)
        },
      changeMonth: true,
      changeYear: true,
      dateFormat: 'yy-mm-dd',
	   	yearRange: "-40:+0"
    });

    $( ".trndte_to" ).datepicker({
    	numberOfMonths: 2,
      onSelect: function(selected) {
         $(".trndte_from").datepicker("option","maxDate", selected)
        },
      changeMonth: true,
      changeYear: true,
      dateFormat: 'yy-mm-dd',
	   	yearRange: "-40:+0"
    });

    $( ".retdte_from" ).datepicker({
    	numberOfMonths: 2,
      onSelect: function(selected) {
         $(".retdte_to").datepicker("option","minDate", selected)
        },
      changeMonth: true,
      changeYear: true,
      dateFormat: 'yy-mm-dd',
	   	yearRange: "-40:+0"
    });

    $( ".retdte_to" ).datepicker({
    	numberOfMonths: 2,
      onSelect: function(selected) {
         $(".retdte_from").datepicker("option","maxDate", selected)
        },
      changeMonth: true,
      changeYear: true,
      dateFormat: 'yy-mm-dd',
	   	yearRange: "-40:+0"
    });

     $( ".borr_from" ).datepicker({
    	numberOfMonths: 2,
      onSelect: function(selected) {
         $(".borr_to").datepicker("option","minDate", selected)
        },
      changeMonth: true,
      changeYear: true,
      dateFormat: 'yy-mm-dd',
	   	yearRange: "-40:+0"
    });

    $( ".borr_to" ).datepicker({
    	numberOfMonths: 2,
      onSelect: function(selected) {
         $(".borr_from").datepicker("option","maxDate", selected)
        },
      changeMonth: true,
      changeYear: true,
      dateFormat: 'yy-mm-dd',
	   	yearRange: "-40:+0"
    });

    $( ".media_from" ).datepicker({
    	numberOfMonths: 2,
      onSelect: function(selected) {
         $(".media_to").datepicker("option","minDate", selected)
        },
      changeMonth: true,
      changeYear: true,
      dateFormat: 'yy-mm-dd',
	   	yearRange: "-40:+0"
    });

    $( ".media_to" ).datepicker({
    	numberOfMonths: 2,
      onSelect: function(selected) {
         $(".media_from").datepicker("option","maxDate", selected)
        },
      changeMonth: true,
      changeYear: true,
      dateFormat: 'yy-mm-dd',
	   	yearRange: "-40:+0"
    });


    /* RADIO BUTTON IN BY BORROWER */
    $('input[type="radio"]').on('change', function(e){
    	if($(this).val() === "student"){
    		$('.borr_borruser_student').show('slow');
    		$('.borr_borruser_employee').hide();
    	}else{
    		$('.borr_borruser_employee').show('slow');
    		$('.borr_borruser_student').hide();
    	}
    });

	})
</script>