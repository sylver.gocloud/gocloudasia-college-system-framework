<div class="row">
	
	<h3>Search <small>Borrower</small></h3>
	
	<?@$this->load->view('library/_borrower_tab');?>
	<p></p>
	<!-- Tab panes -->
	<div class="tab-content">
	  <div class="tab-pane" id="home">
	  	
	  </div>

	  <div class="tab-pane active" id="profile">
	  	<?if(!empty($search)):?>
			<table class="table table-striped table-hover">
			<thead>
			<tr>
				<th>Username</th>
				<th>Fullname</th>
				<th>Role</th>
				<th>&nbsp;</th>
			</tr>
			</thead>
			<tbody>
			<?foreach($search as $k => $emp):?>
				<tr>
					<td><?=$emp->username?></td>
					<td><?=ucwords(strtolower($emp->fullname));?></td>
					<td><?=ucwords($emp->department);?></td>
					<td>
						<a href="<?=site_url('library/circulation/employee/'.$emp->id)?>" class="btn btn-sm btn-success"><i class="fa fa-book"></i>&nbsp; Profile</a>
					</td>
				</tr>
			<?endforeach;?>
			</tbody>
			</table>
		<?else:?>
			<div class="alert alert-danger">
				<i class="icon-exclamation-sign icon-white"></i> Nothing found!
			</div>
		<?endif;?>
		<?=$links;?>
	  </div>
	</div>

</div>