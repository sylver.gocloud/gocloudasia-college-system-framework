<style>
	select{
		padding:4px 4px !important;
	}
	.large-12{
		padding-left: 0px !important;
		padding-right: 0px !important;
	}
</style>
<?@$this->load->view('library/circulation_head');?>
<div class="panel panel-primary">
  <div class="panel-heading">
    <h3 class="panel-title"><i class="fa fa-shopping-cart"></i>&nbsp; Media/Book Cart <span class="badge"><?=!empty($library_cart)?count($library_cart):'0'?></span>  </h3>
    <span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
  </div>
  <div class="panel-body">
  	<?if(!empty($library_cart)):?>
  		<?=form_open('')?>
  		<div class="table-responsive">
  			Transaction Date &nbsp;
  			<input type="text" name="trndte" value="<?=date('Y-m-d')?>" class="form-control datepicker_ymd" required />
  			<input type="hidden" name="form_token" value="<?=$token?>" />
  			<input type="hidden" name="id" value="<?=$id?>">
  			<input type="hidden" name="usertype" value="<?=$usertype?>">
  			<table class="table" >
  				<tr>
  					<td>#</td>
  					<td>Title</td>
  					<!--<td>Description</td>-->
					<td>Acc. No.</td>
					<td>Call No.</td>
  					<td>Author</td>
  					<td>ISBN</td>
  					<td>Available Copy</td>
  					<td>Quantity</td>
  					<td>Days (Borrow)</td>
  					<td></td>
  				</tr>
  				<?$ctr = 1;?>
  				<?foreach ($library_cart as $key => $book):?>
  					<?php
  						$available_copy = $book->book_copies - $book->book_borrowed;
  						if($available_copy > 0){
  							$tran_type['borrow'] = "BORROW";
  						}
  						$tran_type['reserve'] = "RESERVE";
  					?>
  					<tr>
  						<td><?=$ctr++?></td>
  						<td><?=$book->book_name?></td>
  						<!--<td><?//=$book->book_desc?></td>-->
						<td><?=$book->accession_number?></td>
						<td><?=$book->call_number?></td>
  						<td><?=$book->book_author?></td>
  						<td><?=$book->book_isbn?></td>
  						<td><?=$book->book_copies - $book->book_borrowed?></td>
  						<!-- <td><?//=form_dropdown("cart[".$book->id."][trntype]", $tran_type,'','required');?></td> --> <!-- Hide For The Mean Time -->
  						<td class="hidden" ><input type="hidden" name="cart[<?=$book->id?>][trntype]" value="borrow" ></td>
  						<td><input type="number" value="1" min = '1' name="cart[<?=$book->id?>][qty]" max = '<?=$book->book_copies - $book->book_borrowed?>' placeHolder='1-<?=$book->book_copies - $book->book_borrowed?>' required ></td>
  						<td><input type="number" value="1" min = '0' name="cart[<?=$book->id?>][day]" placeHolder='Number of Days' required ></td>
  						<td><a href="<?=site_url('circulation/remove_item_in_cart/'.$usertype.'/'.$id.'/'.$book->id)?>" class="btn btn-mini btn-danger"><i class="fa fa-trash-o"></i>&nbsp;</a></td>
  					</tr>
  				<?endforeach?>
  			</table>
  			<div class="row">
  				<div class="col-md-1">Remarks</div>
  				<div class="col-md-8"><input type="text" name="remarks" class="form-control" id=""></div>
  				<div class="col-md-3"><button style="float:right !important;" name="checkout" value="checkout" type="submit" class="btn btn-sm btn-success" ><i class="fa fa-save"></i>&nbsp; Proccess or Check Out</button></div>
  			</div>
  		</div>
  		</form>
  	<?else:?>
  		<p>No item added to cart yet.</p>
  	<?endif;?>
  </div>
</div>

<div class="panel panel-primary">
  <div class="panel-heading">
  	 <h3 class="panel-title">
  	 	<i class="fa fa-search"></i>&nbsp; Search Media/Book 
  	 	<span class="badge"><?=$total_rows?></span>
  	 </h3>
  	 <span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
  </div>

  <?=form_open("library/borrow/".$usertype.'/'.$id.'/0','method="GET" role="search"')?>
  <div class="panel-body" style="padding : 3px !important;">
    <div class="table-responsive">
    	<table class="table">
    		<tr class="primary">
    			<td>
    				<div class="large-12 columns">
			        	<label>Title</label>
			        	<input type="text" name="item_name" placeholder="Media or Book Name" value="<?=isset($item_name)?$item_name:''?>" >
			      	</div>
			    </td>
				<!--
    			<td>
    				<div class="large-12 columns">
			        	<label>Description</label>
			        	<input type="text" name="item_description" placeholder="Description" value="<?//=isset($item_description)?$item_description:''?>" >
			      	</div>
    			</td>
				-->
				<td>
    				<div class="large-12 columns">
			        	<label>Acc. No.</label>
			        	<input type="text" name="accession_number" placeholder="Accession Number" value="<?=isset($accession_number)?$accession_number:''?>" >
			      	</div>
    			</td>
				<td>
    				<div class="large-12 columns">
			        	<label>Call No.</label>
			        	<input type="text" name="call_number" placeholder="Call Number" value="<?=isset($call_number)?$call_number:''?>" >
			      	</div>
    			</td>
    			<td>
	    				<div class="large-12 columns">
				        	<label>Barcode</label>
				        	<input type="text" name="item_barcode" placeholder="Bar Code" value="<?=isset($item_barcode)?$item_barcode:''?>" >
				      	</div>
	    			</td>
    			<td>
    				<div class="large-12 columns">
			        	<label>Author</label>
			        	<input type="text" name="item_author" placeholder="Author" value="<?=isset($item_author)?$item_author:''?>" >
			      	</div>
    			</td>
    			<td>
    				<div class="large-12 columns">
			        	<label>ISBN</label>
			        	<input type="text" name="item_isbn" placeholder="ISBN" value="<?=isset($item_isbn)?$item_isbn:''?>" >
			      	</div>
    			</td>
    			<td>
    				<div class="large-12 columns">
			        	<label>Type</label>
			        	<?=form_dropdown('item_type', $media_types, isset($item_type)?$item_type:'');?>
			      	</div>
    			</td>
    			<td>
    				<div class="large-12 columns">
			        	<label>Subject</label>
			        	<?=form_dropdown('item_category', $book_category, isset($item_category)?$item_category:'');?>
			      	</div>
    			</td>
    			<td>
    				<div class="large-12 columns">
			        	<label>Available Copy</label>
			      	</div>
    			</td>
    			<td>
    				<button type="submit" class='btn btn-default' ><i class="glyphicon glyphicon-search"></i>&nbsp;</button>
    				<a href="<?=current_url();?>" class="btn btn-default"><i class="glyphicon glyphicon-refresh"></i>&nbsp;</a>
    			</td>
    		</tr>
    		<?if($search):?>
    			<?foreach ($search as $key => $v):?>
    				<tr class="<?=($v->available) <= 0 ? 'danger' : ''?>">
    					<td><?=$v->book_name?></td>
    					<!--<td><?//=$v->book_desc?></td>-->
						<td><?=$v->accession_number?></td>
						<td><?=$v->call_number?></td>
    					<td><?=$v->book_barcode?></td>
    					<td><?=$v->book_author?></td>
    					<td><?=$v->book_isbn?></td>
    					<td><?=$v->media_type?></td>
    					<td><?=$v->category?></td>
    					<td><?=$v->available?></td>
    					<td>
    						<!-- <a href="<?=site_url('circulation/borrow_item/'.$usertype.'/'.$id.'/'.$v->id)?>" class="btn" title="Add to Cart?" ><i class="fa fa-shopping-cart"></i>&nbsp;</a> -->
    						<a href="<?=$current_url,'&add_to_cart='.$v->id?>" class="btn confirm" title="Add to Library Cart?" ><i class="fa fa-shopping-cart"></i>&nbsp;</a>
    					</td>
    				</tr>
    			<?endforeach;?>
			<?else:?>
				<tr>
					<td colspan="8" >
						No Item Found.
					</td>
				</tr>
			<?endif;?>
    	</table>
    	<?=$links?>
    </div>
  </div>
  </form>
</div>