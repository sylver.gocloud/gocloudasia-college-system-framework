<?@panel_head2('Upload From CSV (Comma Separated Value)');?>
	<div class="alert alert-info">
	<p><i class="fa fa-info"></i>&nbsp; : Please <a class="btn btn-default" href="<?=site_url('library/download_media_csv_format')?>" target="_blank"><i class="fa fa-download"></i>&nbsp; download</a> and used this <a target="_blank" href="http://en.wikipedia.org/wiki/Comma-separated_values">CSV</a> format. Open and edit the downloaded file in <a target="_blank" href="http://en.wikipedia.org/wiki/Microsoft_Excel">Excel</a>.</p>
	</div>

	<p class="alert alert-success">Must be in <code>'csv'</code> Format.</p>
	<p></p>
	<div class="col col-md-10">
		<div class="upload">
		<?php echo form_open_multipart('');?>
			<input type="file" name="userfile" class="form-control" size="1000" required />
			<input type="hidden" name="form_token" value="<?=$token?>">
			<p></p>
			<button class="btn btn-primary" type="submit" name="upload_csv" value="upload_csv" ><i class="fa fa-upload"></i>&nbsp; Upload File</button>
		</form>
		</div>
	</div>
<?@panel_tail();?>