<?=panel_head2('Add Course Specialization')?>
  <?=form_open('')?>
    <div class="div">
      <label class="col-md-2 control-label" for="textinput">Specialization</label>  
      <div class="col-md-4">
      <input type="text" name="specialization" id="specialization" maxlength="150" required>
      </div>

      <div class="col-md-4">
      <button name="create_specialty" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i>&nbsp; Submit</button>
      </div>

    </div>
  <?=form_close()?>
<?=panel_tail();?>