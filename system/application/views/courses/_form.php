
 <?php 
		$f_course_code = array(
              'name'        => 'courses[course_code]',
              'id'        	=> 'course_code',
              'value'       => set_value('courses[course_code]',isset($courses->course_code) ? $courses->course_code : ''),
              'maxLength'   => '25',
              'required'	=> true
            );

		$f_course = array(
              'name'        => 'courses[course]',
              'id'        	=> 'course',
              'value'       => set_value('courses[course]',isset($courses->course) ? $courses->course : ''),
              'maxLength'   => '120',
              'required'	=> true
            );
?>

<fieldset>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-2 control-label" for="textinput">Course Code</label>  
  <div class="col-md-4">
  <?=form_input($f_course_code);?>
  <span class="help-block">Unique short represention of the course.</span>  
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-2 control-label" for="textinput">Course Title</label>  
  <div class="col-md-5">
  <?=form_input($f_course);?>
  <span class="help-block">Full Course Description</span>  
  </div>
</div>

<div class="form-group">
  <label class="col-md-2 control-label" for="textinput">Course Specialization</label>  
  <div class="col-md-5">
    <ul class="list-group">
      <?if(isset($specialty) && $specialty):?>
        <?foreach ($specialty as $k => $v):?>
          <li class="list-group-item click-me-parent">

            <div class="view-me-form">
              <span class="loader"></span>
              <i class="fa fa-tag"></i>&nbsp;
              <?=$v->specialization?>
              
              <a url="<?=site_url('ajax/delete_course_specialization/'.$v->id)?>" class="delete-me-link pull-right color-red" parent="click-me-parent" ><span class="glyphicon glyphicon-remove"></span></a>
              <div class="pull-right click-me-link" style="cursor: pointer"><i class="fa fa-edit"></i>&nbsp;</div> 
            </div>

            <div class="form-group edit-me-form" style="display:none;">
              <div class="col-md-12">
                <div class="input-group">
                  <input id="prependedcheckbox" name="prependedcheckbox" class="form-control edit-me-input" type="text" value="<?=$v->specialization?>" org="<?=$v->specialization?>" minlen = "1" url="<?=site_url('ajax/update_course_specialization/'.$v->id)?>" placeholder="Specialization Name">
                  <span class="input-group-addon loader">     
                      <span class="glyphicon glyphicon-pencil"></span>
                  </span>
                </div>
              </div>
            </div>

          </li>  
        <?endforeach;?>
      <?else:?>
        <li class="list-group-item">
          No Specialization, you can create below.
        </li>
      <?endif;?>
      <a href="#" class="list-group-item active"></a>
      <li class="list-group-item">
        <!-- <table>
          <tr>
            <td><input type="text" maxlength="150" placeHolder="Specialization Name" name="specialization" id="specialization" maxlength="150"></td>
            <td><button name="create_specialty" value="create_specialty" class="btn btn-sm btn-success">Create</button></td>
          </tr>
        </table> -->
        <div class="form-group">
          <div class="col-md-12">
            <div class="input-group">
              <input type="text" maxlength="150" placeHolder="Specialization Name" name="specialization" id="specialization" maxlength="150">
              <span class="input-group-addon">     
                  <button name="create_specialty" value="create_specialty" class="btn btn-xs btn-success">Create</button>
              </span>
            </div>
          </div>
        </div>

      </li>
    </ul>
  </div>
</div>

</fieldset>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/custom/form_ajax.js"></script>