<?echo form_open('','class="form-horizontal"');?>
<?@$this->load->view('courses/_form')?>
<!-- Button (Double) -->
<div class="form-group">
  <label class="col-md-2 control-label" for="button1id"></label>
  <div class="col-md-6">
    <?= form_submit('save_course','Save Changes');?>
    <a class='btn btn-default btn-sm' href="<?php echo base_url(); ?>courses" rel="facebox" class="actionlink"><span class='glyphicon glyphicon-backward'></span>&nbsp;  Back to list of Courses</a>
  </div>
</div>
</form>