<div id="right">
	<?php 
		$formAttrib = array('id' => 'search_employee', 'class' => 'form-inline', 'method' => 'GET');
		echo form_open('users/activate_users/index/0', $formAttrib);
	?>
    
 <div class="form-group">
    <input id="name" class="form-control" type="text" value="<?=isset($name)?$name:''?>" name="name" placeholder="Name">
  </div>
   
  <div class="form-group">
    <input id="login" class="form-control" type="text" value="<?=isset($login)?$login:''?>" name="login" placeholder="Employee ID / Login ID">
  </div>

  <div class="form-group">
    <b>Role : </b>&nbsp;
  </div>

  <div class="form-group">
	<?
    echo departments_dropdown('role',isset($role) ? $role : '','','All');
	?>
  </div>
  <div class="form-group">
    <b>Status : </b>&nbsp;
  </div>

  <div class="form-group">
	<?
		$active_arr = array(
				'0' => "In-Active",
				'1' => "Active",
				'' => "All"
			);
    echo form_dropdown('active',$active_arr, isset($active) ? $active : '');
	?>
  </div>
   
	<?php
	echo form_hidden('semester_id', $this->open_semester->id);
	echo form_hidden('sy_from', $this->open_semester->year_from);
	echo form_hidden('sy_to', $this->open_semester->year_to);
	echo form_hidden('return_url', $return_url);
	echo form_hidden('is_paid', 1);
	?>
   		<?php echo form_submit('submit', 'Search'); ?>
		<?echo form_close();?>
<br>
<?echo isset($links) ? $links : NULL;?>
	<?php
	if(isset($search)){
	?>
	<br />
	<table>
		<tr>
			<th>Name</th>
			<th>Login</th>
			<th>Role</th>
			<th>Is Active (Yes/No)</th>
			<th>Activation Date</th>
			<th>Action</th>
		</tr>
		<tbody>
		<?php
		if(!empty($search))
		{
			foreach($search as $s):
			
			?>
			<tr>
				<td><?=strtoupper($s->fullname);?></td>
				<td><?=$s->employeeid;?></td>
				<td><?=$s->department;?></td>
				<td><?=$s->is_activated == 1 ? "Yes" : "No";?></td>
				<td><?=date('m-d-Y', strtotime($s->activated_at)) == '01-01-1970' ? '' : date('m-d-Y', strtotime($s->activated_at)) ?></td>
				<td>
					<?if($s->is_activated == 0):?>
						<a title='Are you sure to activate <?=$s->fullname?>?' class='btn btn-success btn-xs confirm' href='<?=base_url("users/activate/$s->users_id/1")?>' ><span class='glyphicon glyphicon-thumbs-up' > Activate</span> </a>
					<?else:?>
						<a title='Are you sure to deactivate <?=$s->fullname?>?' class='btn btn-danger btn-xs confirm' href='<?=base_url("users/activate/$s->users_id/0")?>' ><span class='glyphicon glyphicon-thumbs-down' > De-activate</span> </a>
					<?endif;?>
					
				</td>
			</tr>
			<?php 
			endforeach; ?>
			<tr>
				<td colspan='1' align='right'><b>Total Records</b></td>
				<td><span class='badge'><?=($total_rows) ? $total_rows : 0;?></span></td>
			</tr>
			<?php
		}
		else
		{
		?>
		<tr>
		<td colspan="1">
			Student Not Found
		</td>
		</tr>
		<?php
		}
		?>
		</tbody>
	</table>
	<?echo isset($links) ? $links : NULL;?>
	<?php
	}
	?>
	</div>
	
	<div class="clear"></div>
</div>