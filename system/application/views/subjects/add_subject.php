<div id="user_subjects">
<table>
	<tr>
		<th>Subject Code</th>
		<th>Section Code</th>
		<th>Description</th>
		<th>Units</th>
		<th>Lab</th>
		<th>Lec</th>
		<th>Time</th>
		<th>Day</th>
		<th>Room</th>
		<th>Action</th>
	</tr>
<?php if(!empty($subjects)):?>
<?php foreach($subjects as $subject): ?>
	<tr>
		<td><?php echo $subject->sc_id; ?></td>
	    <td><?php echo $subject->code; ?></td>
	    <td><?php echo $subject->subject; ?></td>
	    <td><?php echo $subject->units; ?></td>
	    <td><?php echo $subject->lab; ?></td>
	    <td><?php echo $subject->lec; ?></td>
		<td><?php echo $subject->time; ?></td>
		<td><?php echo $subject->day; ?></td>
		<td><?php echo $subject->room; ?></td>
		<td>
			
			<?= badge_link('ajax/destroy_subject/'.$y.'/'.$c.'/'.$s.'/'.$eid.'/'.$subject->id.'/'.$subject->subjectid.'/nf', "danger delete_subject", 'delete', "") ?>
			<!--JAVASCRIPT ACTION IN assets/js/myjs.js (delete_subject CLASS)-->
		</td>
  </tr>
	<?php endforeach; ?>
	<?php endif; ?>
	<tr id="user_subjects_total">
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>Total</td>
		<td id="total_units"><?php if(!empty($subject_units))echo number_format($subject_units['total_units'],2,'.',' '); ?></td>
		<td id="total_labs"><?php if(!empty($subject_units))echo number_format($subject_units["total_lab"],2,'.',' '); ?></td>
		<td id="total_lec"><?php if(!empty($subject_units))echo number_format($subject_units["total_units"] - $subject_units["total_lab"],2,'.',' '); ?></td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
</table>
</div>

<?= $links ?> 

		<p>
			<a href="<?=site_url('profile/view/'.$eid);?>" class="btn btn-default btn-sm">Back To Profile</a>
		</p>
<div id="subject_list"> 
			 <table>
					<th>&nbsp;</th>
					<th>Subject Code</th>
					<th>Section Code</th>
					<th>Subject</th>
					<th>Units</th>
					<th>Lab</th>
					<th>Lec</th>
					<th>TIME</th>
					<th>Day</th>
					<th>Room</th>
					<th>Remaining Load</th>
				<? if(empty($subject_list) == false):?>
					<? foreach($subject_list as $sub):?>
						<tr>
							<!--JAVASCRIPT ACTION IN assets/js/myjs.js  (subject_list CLASS)-->
							<td class="text-center">
							<? if($sub->subject_load > 0) :?>
							<a href="<?=site_url('ajax/add_subject_ajax/'.$sub->id.'/'.$y.'/'.$c.'/'.$s.'/'.$eid);?>" class="btn btn-default btn-sm">Add</a>
							<? else :?>
							FULL
							<? endif ;?>
							</td>
							<td><?=$sub->sc_id;?></td>
							<td><?=$sub->code;?></td>
							<td><?=$sub->subject;?></td>
							<td><?=$sub->units;?></td>
							<td><?=$sub->lab;?></td>
							<td><?=$sub->lec;?></td>
							<td><?=$sub->time;?></td>
							<td><?=$sub->day;?></td>
							<td><?=$sub->room;?></td>
							<td><?=$sub->subject_load;?></td>
						</tr>
					<?endforeach;?>
				<? endif;?>
			 </table>
		<p>
			<a href="<?=site_url('profile/view/'.$eid);?>" class="btn btn-default btn-sm">Back To Profile</a>
		</p>
</div>
<?= $links ?>
