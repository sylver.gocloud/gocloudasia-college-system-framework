<!-- 
	Not Yet Finished
	- to be continue later
	- stick muna ako sa fix time, bec ganun ung nasa form
-->

<link href="<?=site_url('assets/js/datetimepicker/jquery.datetimepicker.css')?>" rel="stylesheet">
<link href="<?=site_url('assets/css/tagbox.css')?>" rel="stylesheet">
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/datetimepicker/jquery.datetimepicker.js"></script>
<?
	/* 
		REF:  https://github.com/xdan/datetimepicker 
				  http://xdsoft.net/jqplugins/datetimepicker/
	*/
?>

<script type='text/javascript'>

	var xerror_msg = "<div class='alert alert-info'>Ajax Error, something went wrong.</div>";
	var xsite_url = $('#site_url').val();

	$( document ).ready(function() {


		$('.ui-widget-overlay').live("click",function(){
        $("#message").dialog("close");
    });  

		$('.units').keyup(function(event){
			var xlab = $('#txt_lab').val();
			var xlec = $('#txt_lec').val();

			xlab = isNumber(xlab) ? xlab : 0;
			xlec = isNumber(xlec) ? xlec : 0;

			var xunits = parseFloat(xlab) + parseFloat(xlec);

			xunits == isNumber(xunits) ? xunits : 0;

			$('#txt_units').val(xunits);
		})

		$('.select_time').on('change', function(){
			var xs_hr = ($('#class_start_hr').val() == "") ? '00' : $('#class_start_hr').val();
				xs_hr = str_pad(xs_hr, 2, '0', 'STR_PAD_LEFT');

			var xs_min = ($('#class_start_hr').val() == "") ? '00' : $('#class_start_min').val();
				xs_min = str_pad(xs_min, 2, '0', 'STR_PAD_LEFT');

			var xe_hr = ($('#class_end_hr').val() == "") ? '00' : $('#class_end_hr').val();
				xe_hr = str_pad(xe_hr, 2, '0', 'STR_PAD_LEFT');

			var xe_min = ($('#class_end_min').val() == "") ? '00' : $('#class_end_min').val();
				xe_min = str_pad(xe_min, 2, '0', 'STR_PAD_LEFT');

			var xtime = xs_hr + ':'+ xs_min + '-' + xe_hr + ':' + xe_min;

			$('#time').val(xtime);
		});

		$('.day_select').on('click', function(){
			
			var xname = $(this).attr('name').toUpperCase();
			get_day_name();
		});

		$('.sched_time').datetimepicker({
			datepicker:false,
  		format:'H:i',
  		minTime:'05:00',
  		defaultTime:'07:00',
  		step: 15,
  		onSelectTime:function(dp,$input){

		    if($input.attr('id') === "sched_time_to"){
		    	$('#btn_add_time').click();
		    }
		  }
		});

		ajax_search_subject();
		ajax_subject_on_change();
		ajax_view_checklist();
		create_time(); //time tag box

	});

	function ajax_search_subject () {
		$('#search_key').on('keyup',function(){
			
			var x = $(this);
			var xurl = x.attr("url");
			var xdiv_con = x.attr("ajax-container-id");
			var xparams = {keyword:$(this).val()}

			$.ajax({
				type: "POST",
				url: xurl,
				data: xparams,
				dataType : 'html',
			}).done(function(output){
				$('#'+xdiv_con).html(output);
				selecting_subject();
			}).fail(function(jqXHR, textStatus, errorThrown){
					$('#'+xdiv_con).html(xerror_msg);
			});
		})
	}

	function selecting_subject () {
		$('.select-subject').on('click', function(){
			$('#ref_id').val($(this).attr("ref_id")).change();
			$('#search_subject_modal').modal('hide');
			BootstrapDialog.closeAll();
			close_mymodal('alertModal');
			focusdiv('subject_well');
		})
	}

	/**
	 * Viewing of Course Checklist
	 * Display in Modal
	 */
	function ajax_view_checklist () {
		$('.btn_checklist').on('click', function(){
			
			var x = $(this);
			var xurl = x.attr("url");
			var xcourse_id = $('#course_id').val();
			var xparams = {course_id:xcourse_id}
			
			if(xcourse_id){
				$.ajax({
					type: "POST",
					url: xurl,
					data: xparams,
					dataType : 'html',
				}).done(function(output){
					
					custom_modal('Curriculum Checklist',output,'success');
					selecting_subject();

				}).fail(function(jqXHR, textStatus, errorThrown){
					bsAlert('Ajax Error',xerror_msg);
				});
			}else{
				bsAlert('error','No course selected');
			}
		})
	}

	function ajax_subject_on_change () {
		$('#ref_id').on('change', function(){
			
			var x = $(this);
			var xurl = x.attr("url");
			var xparams = {ref_id:$(this).val()}

			$.ajax({
				type: "POST",
				url: xurl,
				data: xparams,
				dataType : 'json',
			}).done(function(output){
				
				$('#subjects_code').val(output.code).css('border-color','gray');
				$('#subjects_subject').val(output.subject).css('border-color','gray');
				$('#txt_units').val(output.units).css('border-color','gray');
				$('#txt_lec').val(output.lec).css('border-color','gray');
				$('#txt_lab').val(output.lab).css('border-color','gray');

				//show lab fee
				if(parseFloat(output.lab) > 0){
					$('#row_lab_fee').removeClass('hidden');
					$('#subjects_lab_fee_id').attr('required',true).css('border-color','red').css('border-style','dotted');
				}else{
					$('#row_lab_fee').addClass('hidden');
					$('#subjects_lab_fee_id').removeAttr('required').css('border-color','gray').css('border-style','dotted');
				}
			}).fail(function(jqXHR, textStatus, errorThrown){
				custom_modal('Ajax Error',xerror_msg);
			});
		})
	}

	function get_day_name()
	{
		var xday = '';
		$('.day_select:checked').each(function(){
			var xname = $(this).attr('name').toUpperCase();
			if(xday == '')
			{
				xday = get_day_short(xname);
			}
			else
			{
				xday = xday +'-'+ get_day_short(xname);	
			}
		})

		$('#day').val(xday);
	}

	function create_time () {

		// Add Time
		$('#btn_add_time').on('click', function(e){
			e.preventDefault();		

			var checked = $('#time_container input[type="checkbox"]:checked');
			var from = $('#sched_time_from').val();
			var to = $('#sched_time_to').val();
			var time_hid = $('#time');

			if(checked.length > 0){
				if(from && to){
					var time = from + '-' + to;
					var day_hidden = time_hid.val() ? time_hid.val().trim() : time_hid.val();
					$(checked).each(function(){

						// tags
						var day = '<span class="tag"><strong>' + $(this).val() + '</strong>(' + time + ')' + '</span>';
						$('.def_tag').remove();
						$('#tags').append(day);
						$('#sched_time_from').val('');
						$('#sched_time_to').val('');
						$(checked).attr('checked', false);

						// hidden field for saving
						var day_save = $(this).val() + '(' + time + ')';
						day_hidden = day_hidden ? day_hidden + ", " + day_save : day_save;
					})
					time_hid.val(day_hidden);
				}else{
					bs_growl('<strong>Warning!</strong> &nbsp;','Invalid time inputed.','danger','glyphicon glyphicon-warning-sign');	
				}
			}else{
				bs_growl('<strong>Warning!</strong> &nbsp;','No day selected.','danger','glyphicon glyphicon-warning-sign');
			}
		})

		// Delete Time
		$('#tags').on('click','.tag',function(){
	    
	    var xtime = $(this).text().trim();
	    var day_hidden = $('#time').val().trim();
	    if(day_hidden){
	    	day_hidden = day_hidden.replace(xtime,'').trim();
	    	day_hidden = day_hidden.replace(/\,$/, '').trim();	
	    }
	    
	    $('#time').val(day_hidden);

	    $(this).remove(); 
	  });
	}
  
	function validate()
	{
		var ctr = 0;
		var msg = "<ul>";
		var xfocus = "";
		$('.not_blank').css('border-color','gray');
		$('.not_blank').each(function(){
			var xval = $(this).val().trim();
			if(xval == ""){
				ctr++;
				$(this).css('border-color','red');
				if(ctr == 1) { xfocus = $(this); }
				var label = $(this).parent().prev().text();
				var name = $(this).attr('name');
				
					msg += "<li>"+label+" should not be blank</li>";
			}
		});
		
		msg += "<ul>";
		
		if(ctr > 0)
		{
			xfocus.focus();
			
			custom_modal('Required', msg);
			
			return false;
		}
		else
		{
			return true;
		}
	}
	
	function CloseBlock(){
		$.unblockUI();
	}
</script>

<div class="alert alert-info alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <i class="fa fa-info-circle"></i>&nbsp;<strong>Please create subject schedule based on the course curriculum.</strong>
</div>

<div class="well">
	<div class="row">
	  <div class="col-md-4"><label for="course_id">Select course ?</label></div>
	  <div class="col-md-5">
		<?php 
			$dropdown = array();
			if(isset($courses)&&$courses){
				foreach($courses as $obj){
					$dropdown[$obj->id] = $obj->course;
				}
			}else{
				$dropdown[''] = "No academic course created.";
			}
			echo form_dropdown('subjects[course_id]',$dropdown, isset($subjects->course_id) ? $subjects->course_id : '', "id='course_id' ")
		?>
	  </div>
	  <div class="col-md-3">
	  	<button class="btn btn-success btn-sm btn_checklist" type="button" name="btnchecklist" url="<?php echo site_url('ajax/select_subject_in_checklist'); ?>">Checklist</button>
	  </div>
	</div>
	<p></p>

	<div class="row">
	  <div class="col-md-4"><label for="year_id">Select which year level this subject will be in ?</label></div>
	  <div class="col-md-6">
		<?php 
			$dropdown = array();
			if(isset($years)&&$years){
				foreach($years as $obj){
					$dropdown[$obj->id] = $obj->year;
				}
			}else{
				$dropdown[''] = "No academic year level created.";
			}
			echo form_dropdown('subjects[year_id]',$dropdown, isset($subjects->year_id) ? $subjects->year_id : '')
		?>
	  </div>
	</div>
	<p></p>
	  
	<div class="row">
	  <div class="col-md-4"><label for="semester_id">Select which semester this subject will be in ?</label></div>
	  <div class="col-md-6">
		<?php 
			$dropdown = array();
			if(isset($semesters)&&$semesters){
				foreach($semesters as $obj){
					$dropdown[$obj->id] = $obj->name;
				}
			}else{
				$dropdown[''] = "No academic semester created.";
			}
			echo form_dropdown('subjects[semester_id]',$dropdown, isset($subjects->semester_id) ? $subjects->semester_id : $cos->semester_id)
		?>
	  </div>
	</div>
	<p></p>
	   
	<div class="row">
	  <div class="col-md-4"><label for="academic_year_id">Which academic year this subject will be in ?</label></div>
	  <div class="col-md-6">
		<?php 
			$dropdown = array();
			if(isset($academic_years)&&$academic_years){
				foreach($academic_years as $obj){
					$dropdown[$obj->id] = $obj->name;
				}
			}else{
				$dropdown[''] = "No academic year created.";
			}
			echo form_dropdown('academic_year_id',$dropdown, isset($subjects->academic_year_id) ? $subjects->academic_year_id : $cos->academic_year_id)
		?>
	  </div>
	</div>
	<p></p>
</div>

<div class="well" id="subject_well">
	<div class="row">
		<div class="col-md-3"><label for="code">Select Subject. *</label></div>
		<div class="col-md-4">
			<?$site_url = site_url('ajax/onchange_select_subject')?>
			<?php echo form_dropdown("subjects[ref_id]",$subject_list?$subject_list:array(''=>'No Subject Created'), set_value("subjects['ref_id']", isset($subjects->ref_id)?$subjects->ref_id:''), 'id="ref_id" url="'.$site_url.'" required') ?>
		</div>
		<div class="col-md-5">
			<button type="button" data-toggle="modal" data-target="#search_subject_modal" class="btn btn-sm btn-success" >Adv. Search</button>
			<a class="btn btn-sm btn-default" target="_blank" href="<?php echo site_url('master_subjects') ?>">Go to Master Subjects</a>
		</div>
	</div>
	<p></p>

	<div class="row">
	  <div class="col-md-3"><label for="code">Course No. *</label></div>
	  <div class="col-md-7">
		<?php 
			$input = array(
	              'name'        => 'code',
	              'id'        	=> 'subjects_code',
	              'value'       => isset($subjects->code) ? $subjects->code : '',
	              'maxlength'   => '50',
				  			'class'       => '',
				  			'required'		=> true,
				  			'readonly'		=> true,
	            );

			echo form_input($input);
		?>
	  </div>
	</div>
	<p></p>

	<div class="row">
	  <div class="col-md-3"><label for="subject">Description *</label></div>
	  <div class="col-md-7">
		<?php 
			$input = array(
	              'name'        => 'subject',
	              'id'	        => 'subjects_subject',
	              'value'       => isset($subjects->subject) ? $subjects->subject : '',
	              'maxlength'   => '150',
							  'class'       => 'not_blank',
							  'required'	=> true,
							  'readonly'		=> true,
	            );

			echo form_input($input);
		?>
	  </div>
	</div>
	<p></p>

	<div class="row">
	  <div class="col-md-3"><label for="units">No. of Units *</label></div>
	  <div class="col-md-7">
		<?php 
			$input = array(
	              'name'        => 'units',
	              'value'       => isset($subjects->units) ? $subjects->units : '',
	              'maxlength'   => '2',
				  			'class'       => 'numeric',
				  			'id'					=> 'txt_units',
				  			'readonly'		=> true,		
				  			'required'		=> true		
	            );

			echo form_input($input);
		?>
	  </div>
	</div>
	<p></p>

	<div class="row">
	  <div class="col-md-3"><label for="lec">Lec.</label></div>
	  <div class="col-md-7">
		<?php 
			$input = array(
	              'name'        => 'lec',
	              'id'        	=> 'subjects_lec',
	              'value'       => isset($subjects->lec) ? $subjects->lec : '',
	              'maxlength'   => '10',
				  			'class'				=> 'numeric units',
				  			'id'					=> 'txt_lec',
				  			'readonly'		=> true,	
	            );

			echo form_input($input);
		?>
	  </div>
	</div>
	<p></p>

	<div class="row">
	  <div class="col-md-3"><label for="lab">Lab.</label></div>
	  <div class="col-md-7">
		<?php 
			$input = array(
	              'name'        => 'lab',
	              'id'	        => 'subjects_lab',
	              'value'       => isset($subjects->lab) ? $subjects->lab : '',
	              'maxlength'   => '10',
				  			'class'				=> 'numeric units',
				  			'id'					=> 'txt_lab',
				  			'readonly'		=> true,
	            );

			echo form_input($input);
		?>
	  </div>
	</div>
	<p></p>

	<div class="row <?php echo isset($subjects)&&$subjects->lab>0?'':'hidden' ?>" id="row_lab_fee">
		<div class="col-md-3"><label for="lab">Lab Fee (If has lab unit) *</label></div>
	  <div class="col-md-7">
	  	<?echo form_dropdown('subjects[lab_fee_id]', $lab_fees?$lab_fees:array(''=>'No Lab Fee Created'), isset($subjects->lab_fee_id) ? $subjects->lab_fee_id : '',' id="subjects_lab_fee_id"');?>
	  </div>
	</div>

</div>

<div class="well" >

	<div class="row">
	  <div class="col-md-3"><label for="time">Time *</label></div>
	  <div class="col-md-7">
		  <div id="tags">
		    <span class="tag def_tag">No time added</span>
	  	</div>
		<?php 
			$input = array(
	              'name'        => 'subjects[time]',
	              'id'        => 'time',
	              'value'       => isset($subjects->time) ? $subjects->time : '',
							  'class'       => 'form-control',
							  'type'				=> 'hidden'
	            );

			echo form_input($input);
		?>
		<p>
	  </div>
	</div>
	<p></p>

	<div class="row">
	  <div class="col-md-3">Create time here <i class="fa fa-hand-o-right"></i>&nbsp;	 </div>
	  <div class="col-md-7" style="border:1px solid gray" id="time_container" >
	  	<p></p>
			<p>
				<? foreach(_get_week_days() as $code => $day): ?>
					<input type="checkbox" name="sel_day" value="<?=$code?>" id="cb_<?=$day?>"> <strong><?php echo $day ?></strong> &nbsp;&nbsp;
				<? endforeach; ?>
			<p>
				<div class="row">
					<div class="col-md-2 bold">Time</div>
					<div class="col-md-2 col-xs-4"><input class="sched_time" id='sched_time_from' type="text" ></div>
					<div class="col-md-1 col-xs-1 bold" style="text-align:center" >-</div>
					<div class="col-md-2 col-xs-4"><input class="sched_time" id='sched_time_to' type="text" ></div>
					<div class="col-md-2"><button type="button" class='btn btn-sm btn-primary' id='btn_add_time' >Add</button></div>
				</div>
			</p>
	  </div>
	</div>
	<p></p>
</div>

<div class = 'well' >
	<div class="row">
	  <div class="col-md-3"><label for="room_id">Room * </label></div>
	  <div class="col-md-7">
		<?php 
			echo $rooms ? (form_dropdown('subjects[room_id]',$rooms,$subjects->room_id,'class="form-control"')) : 'No Rooms Available';
		?>
	  </div>
	</div>
	<p></p>

	<div class="row">
	  <div class="col-md-3"><label for="original_load">Maximum Slots *</label></div>
	  <div class="col-md-7">
		<?php 
			$input = array(
	              'name'        => 'subjects[original_load]',
	              'value'       => isset($subjects->original_load) ? $subjects->original_load : '',
	              'maxlength'   => '100',
	              'style'       => 'width:150px',
				   'class'       => 'not_blank numeric',
				   'required'	=> true			
	            );

			echo form_input($input);
		?>
	  </div>
	</div>
	<p></p>

	<div class="row">
	  <div class="col-md-3"><label for="teacher_user_id">Instructor *</label></div>
	  <div class="col-md-7">
		<?php 
			echo $teachers ? (form_dropdown('subjects[teacher_user_id]',$teachers,isset($subjects)?$subjects->teacher_user_id:'','class="form-control"')) : 'No teachers Available';
		?>
	  </div>
	</div>
	<p></p>

	<div class="row">
	  <div class="col-md-3"></div>
	  <div class="col-md-3">
		<?php 
				$input = array(
		              'name'        => 'is_nstp',
		              'value'       => '1',
		              'checked'     => isset($subjects->is_nstp) ? ($subjects->is_nstp == 1 ? true : false) : false,
		            );
				echo form_checkbox($input);
				echo " &nbsp;";
				echo "<b>Is NTSP?</b>";
		?>
	  </div>
	</div>
	<p></p>
</div>
<!-- /END OF FORM -->

<div id='message' title='Required Fields' style='display:none;'>
	<div id='message_list' class='alert alert-info'>
		Validating ... 
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="search_subject_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Advance Subject Search</h4>
      </div>
      <div class="modal-body">
        <div class="row">
        	<div class="col-md-12">
        		<input type="text" name="search_key" id="search_key" url="<?php echo site_url('ajax/search_subject') ?>" ajax-container-id="search_body" placeHolder="Type to toggle search">
        	</div>
        	
        	<div class="col-md-12">
        		<div class="well" id="search_body">
        			<p>Type keywords above.</p>
        		</div>
        	</div>
        </div>
      </div>
    </div>
  </div>
</div>