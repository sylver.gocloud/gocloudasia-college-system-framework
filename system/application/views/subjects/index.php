<form action="<?=site_url('subjects');?>/index/0" method="GET" class="" id="search_form" >
	<?php
		$course_id = isset($course_id) ? $course_id : "";
		$course_no = isset($_GET['code'])?$_GET['code']:'';
		$subject = isset($_GET['subject'])?$_GET['subject']:'';
		$major = isset($_GET['major'])?$_GET['major']:'';
	?>

	<div class="row">

		<div class="col-md-3 col-xs-4">
			<label>Course</label><br>
			<?php echo form_dropdown('course_id',$course_list, $course_id) ?>
		</div>

		<div class="col-md-3 col-xs-4">
			<label>Major</label>
			<input class='form-control' id="search_major_id" name="major" size="30" type="text"  value="<?=$major?>" placeHolder="Major" />
		</div>

		<div class="col-md-3 col-xs-4">
			<label>Course No.</label>
			<input class='form-control' id="search_code_eq" name="code" size="30" type="text"  value="<?=$course_no?>" placeHolder="Course No" />
		</div>

		<div class="col-md-3 col-xs-4">
			<label>Description</label>
			<input class='form-control' id="search_subject_eq" name="subject" size="30" type="text" value="<?=$subject?>" placeHolder="Description" />
		</div>

	</div><p></p>

	<div class='row'>

		<div class="col-md-3 col-xs-4">
			<label>Semester</label><br>
			<?=semester_dropdown('semester_id',isset($semester_id)?$semester_id:$this->cos->user->semester_id,"")?>
		</div>

		<div class="col-md-3 col-xs-4">
			<label>Year Level</label><br>
			<?=year_dropdown('year_id',isset($year_id)?$year_id:'',"",'All')?>
		</div>

		<div class="col-md-3 col-xs-4">
			<label>School Year</label><br>
			<?=school_year_dropdown('academic_year_id',isset($academic_year_id)?$academic_year_id:$this->cos->user->academic_year_id,"")?>
		</div>

		<div class="col-md-3 col-xs-6">
			<label></label><br>
			<button id="search_submit" name="search_subjects" type="submit" value="Search" class="btn btn-sm btn-success" ><span class="fa fa-search" ></span>&nbsp; Search</button>
			<button class="btn btn-sm btn-default clear-form" form-id="search_form" type="button" ><span class="fa fa-refresh" ></span></button>
		</div>
	</div>
	
</form>
<p></p>
<div class="well">
  <a class='btn btn-primary btn-sm' href="<?=site_url();?>subjects/create" ><i class="fa fa-plus"></i>&nbsp; New Schedule</a>
	<a href="<?php echo site_url('block_section_settings') ?>" class="btn btn-sm btn-success">Go to Block Sections</a>
  <div class="btn-group btn-group-sm">
	  <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown">
	    <span class="fa fa-print"></span> Print <span class="caret"></span>
	  </button>
	  <ul class="dropdown-menu" role="menu">
	    <li><a class='' href="<?=site_url();?>subjects/download_in_excel/<?=$get_url;?>" class="confirm" title="Download subject in excel form ?"><i class="fa fa-download"></i>&nbsp; Download All Subjects in Excel</a></li>
 			<li><a class='' href="<?=site_url();?>subjects/print_all_in_pdf/<?=$get_url;?>" ><i class="fa fa-print"></i>&nbsp; View All Subjects (PDF)</a></li>
	  </ul>
	</div>
</div>
<p></p>



<div class="table-responsive">
	<?php if(!empty($results)):?>
		  <?= $links ;?>	
	<?php endif;?>
	<table class="table table-striped table-hover table table-condensed table-bordered table-sortable" no-sort-icon = "1">
		<thead>
	  <tr class='gray' >
	    <th>Course Code</th>
	    <th>Major</th>
	    <th>Level</th>
	    <th>Course No.</th>
	    <th>Description</th>
	    <th>Unit</th>
	    <th>Lec</th>
	    <th>Lab</th>
	    <th>Time</th>
	    <th>Day</th>
	    <th>Room</th>
	    <th>Load</th>
	    <th>Instructor</th>
	    <th>Action</th>
	  </tr>
	  </thead>
	  <tbody>
			<?php if(!empty($results)):?>
				<?php foreach($results as $s):?>
					<tr>
						  <td><strong><?php echo $s->course_code;?></strong></td>
						  <td><strong><?php echo $s->major;?></strong></td>
						  <td><strong><?php echo $s->year;?></strong></td>
						  <td><strong><?php echo $s->code;?></strong></td>
						  <td><strong><?php echo $s->subject;?></strong></td>
						  <td><?php echo $s->units;?></td>
						  <td><?php echo $s->lec;?></td>
						  <td><?php echo $s->lab;?></td>
						  <td><?php echo $s->is_open_time === "0" ? _convert_to_12($s->time) : $s->time;?></td>
						  <td><?php echo $s->day;?></td>
						  <td><?php echo $s->room;?></td>
						  <td><?php echo $s->subject_taken;?>/<?php echo $s->original_load;?></td>
						  <td><?php echo $s->instructor;?></td>
						  <td>
						  	<div class="btn-group">
								  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
								    <span class="caret"></span>
								  </button>
								  <ul class="dropdown-menu pull-right" role="menu">
								    <li><a href="<?php echo site_url('subjects/edit'.'/'.$s->id) ?>"><span class="glyphicon glyphicon-edit" ></span> Edit</a></li>
								    <li><a href="<?php echo site_url('subjects/destroy'.'/'.$s->id) ?>"><span class="glyphicon glyphicon-remove" ></span> Delete</a></li>
								    <li class="divider"></li>
								    <li><a href="<?php echo site_url('subjects/view_class_list/'. $s->id) ?>">Class List</a></li>
								  </ul>
								</div>
						</td>
					</tr>
				<?php endforeach;?>
			<?php else:?>
				<tr><td colspan="14" > No Record found.</td></tr>
			<?php endif;?>
		</tbody> 
	</table>	
	<?php if(!empty($results)):?>
	  <?= $links ;?>	
	<?php endif;?>
</div>

