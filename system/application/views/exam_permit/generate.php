<div id="right">

	<div id="right_top" >
		  <p id="right_title">Generate Exam Permit</p>
	</div>
	<div id="right_bottom">

	<div>
<?php $attributes = array('target' => '_blank'); ?>	
<?echo form_open('exam_permit/print_generate',$attributes);?>
<table>
	<tr>
		<th colspan ="2">Generate Exam Permit By Course</th>
	</tr>
	<tr>
		<td>Select Course</td>
		<td>
		<?php
		foreach($courses as $course):
            $options[''.$course->id.''] = $course->course;
		endforeach;
			
		echo form_dropdown('course', $options,'');
		?>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td><?php echo form_submit('','Generate'); ?></td>
	</tr>
</table>
<?php
	echo form_close();
?>
		
	</div>
	<div class="clear"></div>

	</div>
	
	<div class="clear"></div>
	
</div>