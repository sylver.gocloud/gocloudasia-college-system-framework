<?php $this->load->view('layouts/_student_data'); ?>
<div class="tab-content">
  <?if($mydepartment->stud_add_issues):?>
  <a href="<?php echo base_url(); ?>issues/create/<?= $student->id ?>/<?= $student->user_id ?>" class="btn btn-default btn-sm btn-sm"><i class="fa fa-plus"></i>&nbsp; Add Issue</a>
  <br/>
  <br/>
  <?endif;?>

  <?=panel_head2('Unresolved Issues')?>
    <div class="table-responsive">
      <table class='table table-hover table-portable table-bordered'>
        <thead>
          <tr class="gray" >
            <th>Comment</th>
            <th>Issued By (Department)</th>
            <th>Is Resolved?</th>
            <th>SY(Semester)</th>
            <th>Action</th>
          </tr>
        </thead>
        <? if(!empty($issues)) :?>
          <tbody>
          <? foreach($issues as $i ) :?>
            <tr>
              <td width="40%" ><strong><?= $i->comment ?></strong></td>
              <td><?= ucwords($i->issued_by)?></td>
              <td class='text-bold <?= $i->resolved === "Yes" ? "color-success" : "color-danger"; ?>' ><?= $i->resolved === "Yes" ? "Yes" : "No" ?></td>
              <td><?=$i->sy_from?>-<?=$i->sy_to?>(<?=$i->semester?>)</td>
              <td>
              
                <?if($mydepartment->stud_add_issues):?>
                <a class="tp" title="Edit" href="<?php echo base_url(); ?>issues/edit/<?= $i->id ?>/<?= $student->id ?>/<?= $student->user_id ?>" ><i class="glyphicon glyphicon-edit"></i></a>
                <a class="tp confirm" title="Delete?" href="<?php echo base_url(); ?>issues/destroy/<?= $i->id ?>/<?= $student->id ?>"><i class="glyphicon glyphicon-remove color-danger"></i></a>
                <?else:?>
                -- No Access --
                <?endif;?>
              
              </td>
            </tr>
          <? endforeach ;?>
          </tbody>
        <? else : ?>
        <tr>
          <td colspan="5" class="text-center"> NO ISSUES</td>
        </tr>
        <? endif ;?>
      </table>
    </div>
  <?=panel_tail2();?>

  <?if($res_issues):?>
    <?=panel_head2('Resolved Issues',false,'default')?>
      <div class="table-responsive">
        <table class='table table-hover table-portable table-bordered'>
          <thead>
            <tr class="gray" >
              <th>Comment</th>
              <th>Issued By</th>
              <th>Resolved BY</th>
              <th>Resolved Date</th>
              <th>SY(Semester)</th>
              <th>Resolved Remark</th>
            </tr>
          </thead>
            <? if(!empty($res_issues)) :?>
              <tbody>
              <? foreach($res_issues as $i ) :?>
                <tr>
                  <td width="30%" ><strong><?= $i->comment ?></strong></td>
                  <td><?= ucwords($i->issued_by)?></td>
                  <td><?=ucwords($i->resolved_by)?></td>
                  <td><?=date('m-d-Y', strtotime($i->resolved_date))?></td>
                  <td><?=$i->sy_from?>-<?=$i->sy_to?>(<?=$i->semester?>)</td>
                  <td width="20%" ><strong><?= $i->resolved_remark ?></strong></td>
                </tr>
              <? endforeach ;?>
              </tbody>
            <? else : ?>
          <tr>
            <td colspan=4 class="text-center"> NO ISSUES</td>
          </tr>
          <? endif ;?>
        </table>
      </div>
    <?=panel_tail2();?>
  <?endif;?>
</div>