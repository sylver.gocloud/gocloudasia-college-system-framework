<table>
    <tr>
      <th>Subject Code</th>
      <th>Section Code</th>
      <th>Description</th>
      <th>Time</th>
      <th>Day</th>
      <th>Room</th>
      <th>View</th>
    </tr>
	<? if(!empty($assignsubjects)) :?>
  <? foreach($assignsubjects as $row) :?>
    <tr>
      <td><?= $row->sc_id;?></td>
      <td><?= $row->code;?></td>
      <td><?= $row->subject;?></td>
      <td><?= $row->time;?></td>
      <td><?= $row->day;?></td>
      <td><?= $row->room;?></td>
      <td>
          <a href="<?=site_url('teacher/class_list/' . $row->id);?>" >Class</a> | 
          <a href="<?=site_url('teacher/class_grades/' . $row->id);?>">Grades</a>
      </td>
    </tr>
  <? endforeach;?>
  <? else : ?>
    <tr>
      <td align=center colspan=7> No subjects assigned. Please contact the HR for your subjects.</td>
    </tr>
  <? endif;?>
</table>
