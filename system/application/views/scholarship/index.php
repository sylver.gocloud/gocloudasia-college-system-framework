<div id="right">
	<?php 
		$formAttrib = array('id' => 'search_scholarship', 'class' => 'form-inline', 'method' => 'GET');
		echo form_open('scholarship/index/0', $formAttrib);
	?>
    
 	<div class="form-group">
    	<input id="name" class="form-control" type="text" value="<?=isset($name)?$name:''?>" name="name" placeholder="Name">
  	</div>
  	<div class="form-group">
    	<input id="desc" class="form-control" type="text" value="<?=isset($desc)?$desc:''?>" name="desc" placeholder="Description">
  	</div>
   
	<?php
	echo form_hidden('semester_id', $this->open_semester->id);
	echo form_hidden('sy_from', $this->open_semester->year_from);
	echo form_hidden('sy_to', $this->open_semester->year_to);
	echo form_hidden('is_paid', 1);
	?>

	<div class="form-group">
		<?php echo form_submit('submit', 'Search'); ?>
	</div>
	<div class="form-group">
    	<a class="btn btn-default btn-sm" href="<?=site_url('scholarship/create')?>" ><i class="fa fa-plus"></i>&nbsp; Create New </a>
  	</div>

  	<?echo form_close();?>
<br>
<?echo isset($links) ? $links : NULL;?>
	<?php
	if(isset($search)){
	?>
	<br />
	<div class="table-responsive">
	<table class="table" >
		<tr>
		  <th>Scholarship Name</th>
		  <th>Type</th>
		  <th>Applied To</th>
		  <th>Percentage(%)</th>
		  <th>Amount(Cash)</th>
		  <th>Action</th>
		</tr>
		<tbody>
		<?php
		if(!empty($search))
		{
			foreach($search as $student):?>
			<tr>
				<td><?php echo $student->name; ?></td>
				<td><?php echo $student->type; ?></td>
				<td><?php echo $student->applied_to; ?></td>
				<td><?php echo $student->percentage?$student->percentage:''; ?></td>
				<td><?php echo $student->cash_amount ? m($student->cash_amount,2) : ''; ?></td>
				<td>
					<div class="btn-group">
						<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
						<i class="fa fa-caret-square-o-down"></i> <span class="caret"></span>
						</button>
						<ul class="dropdown-menu" role="menu">
							<li><a class="confirm" href="<?=site_url('scholarship/edit/'.$student->id)?>"><i class="fa fa-pencil"></i>&nbsp; Edit</a></li>
							<li><a class="confirm" href="<?=site_url('scholarship/delete/'.$student->id)?>"><i class="fa fa-trash-o"></i>&nbsp; Delete</a></li>						
						</ul>
					</div>
				</td>
			</tr>
			<?php 
			endforeach; ?>
			<tr>
				<td colspan='4' align='right'><b>Total Records</b></td>
				<td><span class='badge'><?=($total_rows) ? $total_rows : 0;?></span></td>
			</tr>
			<?php
		}
		else
		{
		?>
		<tr>
		<td colspan="5">
			Student Not Found
		</td>
		</tr>
		<?php
		}
		?>
		</tbody>
		<tr>
		  <th>Scholarship Name</th>
		  <th>Type</th>
		  <th>Percentage</th>
		  <th>Applied To(%)</th>
		  <th>Amount(Cash)</th>
		  <th>Action</th>
		</tr>
	</table>
	</div>
	<?echo isset($links) ? $links : NULL;?>
	<?php
	}
	?>
	</div>
	
	<div class="clear"></div>
</div>

