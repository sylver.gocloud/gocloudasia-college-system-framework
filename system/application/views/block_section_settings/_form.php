<p>
    <label for="year">Name</label>
	<?php 
		$data= array(
              'name'        => 'block_section_settings[name]',
              'value'       => isset($block_section_settings->name) ? $block_section_settings->name : '',
              'maxlength'   => '100',
              'required'    => true
            );

		echo form_input($data);
  ?>
  </p>

  <p>
    <?php
      $def_sem = isset($this->cos->enrollment)?$this->cos->enrollment->semester_id:$this->cos->user->semester_id;
    ?>
    <label for="semester_id">Semester</label>
    <?echo semester_dropdown('semester_id',isset($block_section_settings)?$block_section_settings->semester_id:$def_sem,"required");?>
  </p>
  
