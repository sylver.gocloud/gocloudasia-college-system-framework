<div id="right">
	<div id="right_top" >
		  <p id="right_title">Block_system_setting : Show</p>
	</div>
	<div id="right_bottom">

	<div>
	<?  
	echo validation_errors('<div class="alert alert-danger">', '</div>');
	?>
	<table class="paginated">

	   <tr><th colspan='11' style='text-align:center;'>Assigned Subjects</th><tr>
	  <tr>
		<th>Subject Code</th>
		<th>Section Code</th>
		<th>Description</th>
		<th>Unit</th>
		<th>Lec</th>
		<th>Lab</th>
		<th>Time</th>
		<th>Day</th>
		<th>Room</th>
		<th>Remaining Load</th>
	  </tr>
	  <?php
		if(is_array($block_subjects)){
		foreach($block_subjects as $block_subject){?>
			<tr>
				<td><?=$block_subject->sc_id;?></td>
				<td><?=$block_subject->code;?></td>
				<td><?=$block_subject->subject;?></td>
				<td><?=$block_subject->units;?></td>
				<td><?=$block_subject->lec;?></td>
				<td><?=$block_subject->lab;?></td>
				<td><?=$block_subject->time;?></td>
				<td><?=$block_subject->day;?></td>
				<td><?=$block_subject->room;?></td>
				<td><?=$block_subject->load;?></td>
			</tr>
		<?php
		}
		}
	  ?>
	
	</table>
	<div class="clear"></div>
	<br/>
	<p>
		<a href = '<?=base_url();?>block_section_settings/edit/<?=$block_section_settings->id;?>' class='actionlink' >Edit</a> |
		<a href = '<?=base_url();?>block_section_settings/destroy/<?=$block_section_settings->id;?>' class='actionlink confirm' rel='facebox'>Destroy</a> |
		<a href = '<?=base_url();?>block_section_settings' class="actionlink" rel='facebox'>Show All</a>
	</p>
	</div>
</div>