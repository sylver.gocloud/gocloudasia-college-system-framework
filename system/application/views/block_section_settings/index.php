
<?php 
  $formAttrib = array('id' => 'search_block', 'class' => 'form-inline', 'method' => 'GET');
  echo form_open('block_section_settings/index/0', $formAttrib);
?>
    
      <div class="form-group">
        <input id="name" class="form-control" type="text" value="<?=isset($name)?$name:''?>" name="name" placeholder="Last Name">
      </div>

      <div class="form-group">
        <?=semester_dropdown('semester_id',isset($semester_id)?$semester_id:$this->cos->user->semester_id,"", 'All Semester')?>
      </div>   

       <div class="form-group">
        <?=form_dropdown('academic_year_id',$sy, isset($academic_year_id)?$academic_year_id:$this->cos->user->academic_year_id);?>
      </div>   

      <div class="form-group">
        <?php echo form_submit('submit', 'Search','class="btn btn-default btn-sm"'); ?>
      </div>   

      <div class="form-group">
        <a class='btn btn-primary btn-sm' href="<?php echo base_url(); ?>block_section_settings/create" rel="facebox"><span class = 'glyphicon glyphicon-plus'></span>&nbsp;  New Block System Settings</a>
      </div>   

<?echo form_close();?>
<p></p>
<strong>Total Rows</strong> <span class="badge badge-info" >  <?=($total_rows) ? $total_rows : 0;?></span>
<hr>
<p></p>

<?php if(isset($search)):?>
  <?echo isset($links) ? $links : NULL;?>
  <div class="table-reponsive" >
    <table class="table table-condensed table-sortable table-bordered" >
      <thead>
        <tr class='gray' >
          <th>Block Name</th>
          <th>Semester</th>
          <th>Academic Year</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
      <?php
      if(!empty($search))
      {
        foreach($search as $obj):
        ?>
        <tr>
        <td class="bold" ><?php echo $obj->name; ?></td>
        <td><?php echo $obj->semester; ?></td>
        <td><?php echo $obj->year_from; ?>-<?php echo $obj->year_to; ?></td>
        <td>
          
          <div class="btn-group btn-group-sm">
            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
            Action<span class="caret"></span>
            </button>
            <ul class="dropdown-menu" role="menu">
            
            <li><a href="<?php echo base_url()."block_section_settings/course_blocks/".__link($obj->id); ?>"><span class='glyphicon glyphicon-list' ></span>&nbsp;  Assign Course</a></li>
            <li><a href="<?php echo base_url()."block_section_settings/edit/".__link($obj->id); ?>"><span class='glyphicon glyphicon-pencil' ></span>&nbsp;  Schedule</a></li>
            <li><a href="<?php echo base_url()."block_section_settings/destroy/".__link($obj->id); ?>"><span class='glyphicon glyphicon-trash' ></span>&nbsp;  Destroy</a></li>
            
           </ul>
          </div>
          
        </td>
        </tr>
        <?php 
        endforeach; ?>
        <?php
      }
      else
      {
      ?>
      <tr>
      <td colspan="5">
        No record found.
      </td>
      </tr>
      <?php
      }
      ?>
      </tbody>
    </table>
  </div>
  <?echo isset($links) ? $links : NULL;?>
<?php endif; ?>
