<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Library extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->session_checker->open_semester();
		$this->load->helper(['my_dropdown','library']);
		$this->load->library(['form_validation']);

		$this->load->model(array(
				'M_media_types',
				'M_library_books',
				'M_library_book_category',
				'M_employees',
				'M_enrollments'
			));
		$this->load->model('M_lib_circulationfile1','m_lib_c1');
		$this->load->model('M_lib_circulationfile2','m_lib_c2');
	}

	public function index(){ show_404(); }

	/**
	 * CRUD for Media Types - Uses the Dynamic Crud CLass
	 */
	public function media_types()
	{
		$this->menu_access_checker();

		$this->view_data['system_message'] = $this->_msg();
		//LOAD CRUD LIBRARY
		unset($config);
		$config['title'] = 'Media Types';
		$config['type'] = 'query';
		$config['sql'] = "SELECT id, media_type FROM media_types";
		$config['table'] = "media_types";
		$config['order'] = "media_type";
		$config['enable_activity_log'] = true;
		$config['activity_log_table'] = "activity_log";
		$config['disable_system_message'] = true;
		// $config['additional_button'] = array(
		// 		'0' => array(
		// 				'url' => site_url('menu_creator/clone_menu'),
		// 				'title' => 'Clone Menu',
		// 				'class' => 'btn btn-default btn-sm',
		// 			),
		// 	);
		$config['pager'] = array(
			"per_page" => 10,
			"num_links" => 3,
			"uri_segment" => 3
		);
		$config['search'] = true;

		$config['crud'] = array(
			0 => array(					
					'field' => 'media_type',
					'type'	=> 'text',
					'label' => 'Media Type',
					'rules' => 'required|is_unique[media_types.media_type]',
					'help'  => 'Type of media (e.g Books)',
					'attr'  => array(
						'name'        	=> 'media_type',
						'id'          	=> 'media_type',
						'placeHolder'   => 'Required',
						'maxlength'   	=> '100',
						'required'		=> null
						)
				)
			);

		$this->load->library('_sylvercrud',$config);

		$this->view_data['data_record'] = $this->_sylvercrud->_html;
	}

	/**
	 * CRUD for Subjects - Uses the Dynamic Crud CLass
	 */
	public function subjects()
	{
		$this->menu_access_checker();

		$this->view_data['system_message'] = $this->_msg();
		//LOAD CRUD LIBRARY
		unset($config);
		$config['title'] = 'Media Subject / Category';
		$config['type'] = 'query';
		$config['sql'] = "SELECT id, lbc_name, lbc_desc FROM library_book_category";
		$config['table'] = "library_book_category";
		$config['order'] = "lbc_name";
		$config['disable_system_message'] = true;
		$config['search_columns'] = array("lbc_name"=>"Subject","lbc_desc"=>"Description");
		$config['show_columns'] = array("lbc_name"=>"Subject","lbc_desc"=>"Description");
		// $config['additional_button'] = array(
		// 		'0' => array(
		// 				'url' => site_url('menu_creator/clone_menu'),
		// 				'title' => 'Clone Menu',
		// 				'class' => 'btn btn-default btn-sm',
		// 			),
		// 	);
		$config['pager'] = array(
			"per_page" => 10,
			"num_links" => 3,
			"uri_segment" => 3
		);
		$config['search'] = true;

		$config['crud'] = array(
			0 => array(					
					'field' => 'lbc_name',
					'type'	=> 'text',
					'label' => 'Subject Name',
					'rules' => 'required|is_unique[library_book_category.lbc_name]',
					'help'  => 'Subject or Category of library media',
					'attr'  => array(
						'name'        	=> 'lbc_name',
						'id'          	=> 'lbc_name',
						'placeHolder'   => 'Required',
						'maxlength'   	=> '100',
						'required'		=> null
						)
				),
			1 => array(					
					'field' => 'lbc_desc',
					'type'	=> 'text',
					'label' => 'Description',
					'rules' => 'required',
					'help'  => '',
					'attr'  => array(
						'name'        	=> 'lbc_desc',
						'id'          	=> 'lbc_desc',
						'placeHolder'   => 'Required',
						'maxlength'   	=> '100',
						'required'		=> null
						)
				)
			);

		$this->load->library('_sylvercrud',$config);

		$this->view_data['data_record'] = $this->_sylvercrud->_html;
	}

	/**
	 * CRUD for Media Items
	 * @param int $page 
	 */
	public function search_media($page = 0)
	{
		$this->menu_access_checker();

		$this->view_data['media_types'] = $this->M_media_types->get_for_dd(array('id','media_type'), false, 'All Types','media_type');
		$this->view_data['book_category'] = $this->M_library_book_category->get_for_dd(array('id','lbc_name'), false, 'All Subjects','lbc_name');

		/*** For PAGINATION ***/

		$filter = false;
		$like = false;
		$order_by = false;
		
		$arr_filters = array();
		$suffix = "";
		
		/* GET SEARCH KEYWORDS AND ADD TO FILTER */
		if($_GET)
		{	
			if(isset($_GET['item_name']) && $_GET['item_name'] != ""){
				$this->view_data['item_name'] = $like['library_books.book_name'] = trim($_GET['item_name']); }

			if(isset($_GET['accession_number']) && $_GET['accession_number'] != ""){
				$this->view_data['accession_number'] = $like['library_books.accession_number'] = trim($_GET['accession_number']); }
			
			if(isset($_GET['call_number']) && $_GET['call_number'] != ""){
				$this->view_data['call_number'] = $like['library_books.call_number'] = trim($_GET['call_number']); }

			if(isset($_GET['item_barcode']) && $_GET['item_barcode'] != ""){
					$this->view_data['item_barcode'] = $like['library_books.book_barcode'] = trim($_GET['item_barcode']); }

			if(isset($_GET['item_author']) && $_GET['item_author'] != ""){
				$this->view_data['item_author'] = $like['library_books.book_author'] = trim($_GET['item_author']); }

			if(isset($_GET['item_isbn']) && $_GET['item_isbn'] != ""){
				$this->view_data['item_isbn'] = $like['library_books.book_isbn'] = trim($_GET['item_isbn']); }

			if(isset($_GET['item_type']) && $_GET['item_type'] != ""){
				$this->view_data['item_type'] = $filter['library_books.media_type_id'] = trim($_GET['item_type']); }

			if(isset($_GET['item_category']) && $_GET['item_category'] != ""){
				$this->view_data['item_category'] = $filter['library_books.book_category'] = trim($_GET['item_category']); }
		}
		
		//CONFIGURATION
		$get['fields'] = array(
				'library_books.*',
				'library_book_category.lbc_name as category',
				'book_copies - book_borrowed as available',
				'media_types.media_type',
		);
		
		$get['where'] = $filter;
		$get['like'] = $like;

		$get['join'][] = array(
				'table' => 'media_types',
				'on' => 'library_books.media_type_id = media_types.id',
				'type' => 'left'
			);

		$get['join'][] = array(
				'table' => 'library_book_category',
				'on' => 'library_books.book_category = library_book_category.id',
				'type' => 'left'
			);
		
		$get['order'] = "library_books.book_name";
		$get['all'] = true; //GET ALL EXCLUDE LIMIT
		$get['count'] = true; //RETURN COUNT NOT ROW
		$get['array'] = false; //RETURN OBJECT NOT ARRAY
		$get['single'] = false; //RETURN ALL NOT SINGLE
		
	
		$this->load->library("pagination");
		$config = $this->pagination_style();
		$config["base_url"] = base_url() ."circulation/index";
		$suffix = '?'.http_build_query($_GET, '', "&");
		$suffix = str_replace("&submit=Search", "", $suffix);
		$config['suffix'] = $suffix;
		$this->view_data['current_url'] = $config['first_url'] = $config['base_url'].$config['suffix']; 
		
		$this->view_data['total_rows'] = $config["total_rows"] = $this->M_library_books->get_record(false, $get);
		
		$config["per_page"] = 25;
		$config['num_links'] = 10;
		$config["uri_segment"] = 5;
		// $config['use_page_numbers'] = TRUE;
		$this->pagination->initialize($config);
		
		//FOR PAGINATION
		$get['all'] = false;
		$get['count'] = false;
		$get['start'] = $page;
		$get['limit'] = $config['per_page'];
		
		$this->view_data['search'] = $search = $this->M_library_books->get_record(false, $get);
		$this->view_data['links'] = $this->pagination->create_links();

		/*** End of PAGINATION ***/
	
	}

	/**
	 * Create Media or Item
	 */
	public function add_media()
	{
		$this->menu_access_checker('library/search_media');
		$this->load->library('form_validation');

		$this->view_data['system_message'] = $this->_msg();
		$this->load->model('M_media_types','mt');
		$this->view_data['categories'] = $this->M_library_book_category->get_for_dd(array('id','lbc_name'), false, '--SELECT TYPES--', 'lbc_name');
		$this->view_data['media_types'] = $this->mt->get_for_dd(array('id','media_type'), false, '--SELECT TYPES--', 'media_type');
		
		if($this->input->post('add_book'))
		{	
			$this->form_validation->set_rules('book[accession_number]','Accession Number','required|trim|htmlspecialchars|is_unique[library_books.accession_number]');
			$this->form_validation->set_rules('book[call_number]','Call Number','required|trim|htmlspecialchars');
			$this->form_validation->set_rules('book[book_name]','Item Name','required|trim|htmlspecialchars');
			$this->form_validation->set_rules('book[book_desc]','Item Description','required|trim|htmlspecialchars');
			$this->form_validation->set_rules('book[book_isbn]','Item ISBN Number','required|trim|htmlspecialchars|alpha_dash');
			$this->form_validation->set_rules('book[book_barcode]','Bar Code','trim|htmlspecialchars|alpha_dash');
			$this->form_validation->set_rules('book[book_author]','Item Author','required|trim|htmlspecialchars');
			$this->form_validation->set_rules('book[book_publisher]','Item Publisher','required|trim|htmlspecialchars');
			$this->form_validation->set_rules('book[book_dop]','Date published','required|trim|htmlspecialchars|callback__mdf');
			$this->form_validation->set_rules('book[book_category]','Item Subject','required|trim|htmlspecialchars');
			$this->form_validation->set_rules('book[media_type_id]','Item Type','required|trim|htmlspecialchars');
			$this->form_validation->set_rules('book[book_copies]','Item No of Copies','required|numeric|trim|htmlspecialchars');
		
			if($this->form_validation->run() !== FALSE)
			{	
				$book = $this->input->post('book');
				$book['book_dop'] = date('Y-m-d', strtotime($book['book_dop']));
				
				$add = (object)$this->M_library_books->insert($book);
				if($add->status)
				{
					$book['id'] = $add->id;
					activity_log('Create library_books', false, arr_str($book));

					$this->_msg('s','Book was successfully added.', current_url());
				}else{
					$this->view_data['system_message'] = $this->_msg('e','Unable to add book to database.');
				}
			}
		}
	}

	/**
	 * Udpate Media or Item
	 */
	public function update_media($id = false)
	{
		if($id === false){ show_404(); }

		$this->menu_access_checker('library/search_media');
		$this->load->library('form_validation');

		$this->view_data['system_message'] = $this->_msg();
		if(is_numeric($id))
		{
			$this->load->model('M_media_types','mt');
			$this->view_data['categories'] = $this->M_library_book_category->get_for_dd(array('id','lbc_name'), false, '--SELECT TYPES--', 'lbc_name');
			$this->view_data['media_types'] = $this->mt->get_for_dd(array('id','media_type'), false, '--SELECT TYPES--', 'media_type');
			$this->view_data['book_profile'] = $this->M_library_books->pull($id);
			$this->view_data['id'] = $id;

			if($this->input->post('update_book'))
			{
				$this->form_validation->set_rules('book[accession_number]','Accession Number','required|trim|htmlspecialchars');
				$this->form_validation->set_rules('book[call_number]','Call Number','required|trim|htmlspecialchars');
				$this->form_validation->set_rules('book[book_name]','Item Name','required|trim|htmlspecialchars');
				$this->form_validation->set_rules('book[book_desc]','Item Description','required|trim|htmlspecialchars');
				$this->form_validation->set_rules('book[book_isbn]','Item ISBN Number','required|trim|htmlspecialchars|alpha_dash');
				$this->form_validation->set_rules('book[book_barcode]','Bar Code','trim|htmlspecialchars|alpha_dash');
				$this->form_validation->set_rules('book[book_author]','Item Author','required|trim|htmlspecialchars');
				$this->form_validation->set_rules('book[book_publisher]','Item Publisher','required|trim|htmlspecialchars');
				$this->form_validation->set_rules('book[book_dop]','Date published','required|trim|htmlspecialchars|callback__mdf');
				$this->form_validation->set_rules('book[book_category]','Item Subject','required|trim|htmlspecialchars');
				$this->form_validation->set_rules('book[media_type_id]','Item Type','required|trim|htmlspecialchars');
				$this->form_validation->set_rules('book[book_copies]','Item No of Copies','required|numeric|trim|htmlspecialchars');
			
				if($this->form_validation->run() !== FALSE)
				{	
					$book = $this->input->post('book');
					$book['book_dop'] = date('Y-m-d', strtotime($book['book_dop']));
					
					$add = $this->M_library_books->update($id, $book);
					if($add)
					{
						$book['id'] = $add->id;
						activity_log('Update library_books', false, arr_str($book));

						$this->_msg('s','Media was successfully updated.', current_url());
					}else{
						$this->view_data['system_message'] = $this->_msg('e','Unable to update media.');
					}
				}
			}
		}
	}

	/**
	 * Library Borrowers - Student and Employers Tab to borrow items
	 */
	public function borrowers()
	{
		$this->menu_access_checker();
		redirect('library/students');
	}

	public function students($page = 0)
	{
		$this->menu_access_checker('library/borrowers');

		$this->view_data['usertype'] = 'students';

		$filter = false;
		$like = false;
		$order_by = false;
		
		$filter['enrollments.semester_id'] = $this->open_semester->id;
		$filter['enrollments.sy_from'] = $this->open_semester->year_from;
		$filter['enrollments.sy_to'] = $this->open_semester->year_to;
		$filter['enrollments.is_deleted'] = 0;
		$filter['enrollments.is_drop'] = 0;
		$filter['enrollments.is_paid'] = 1;
		
		
		$arr_filters = array();
		$suffix = "";

		/* GET SEARCH KEYWORDS AND ADD TO FILTER */
		if($_GET)
		{	
			if(isset($_GET['search_param']) && trim($_GET['search_param']) == 'all'){
				$this->view_data['search_param'] = $keyfield = trim($_GET['search_param']);
			}else{

				if(isset($_GET['search_param'])){
					$this->view_data['search_param'] = $keyfield = trim($_GET['search_param']);
					$this->view_data['keyword'] = $keyword = trim($_GET['keyword']); 
					
					if(trim($keyword)){ 
						
						if($keyfield == 'last_name'){ $like['enrollments.lastname'] = $keyword; }
						if($keyfield == 'middle_name'){ $like['enrollments.middle'] = $keyword; }
						if($keyfield == 'first_name'){ $like['enrollments.firstname'] = $keyword; }
						if($keyfield == 'student_id'){ $like['enrollments.studid'] = $keyword; }
					}
				}
			}
		}
		
		//CONFIGURATION
		$get['fields'] = array(
				'enrollments.id',
				'enrollments.studid',
				'enrollments.name' ,
				'years.year',
				'courses.course',
				'enrollments.user_id',
				'enrollments.date_of_birth as dob',
		);
		
		$get['where'] = $filter;
		$get['like'] = $like;
		
		$get['join'] = array(
			
			1 => array(
				"table" => "courses",
				"on"	=> "courses.id = enrollments.course_id",
				"type"  => "LEFT"
			),
			2 => array(
				"table" => "years",
				"on"	=> "years.id = enrollments.year_id",
				"type"  => "LEFT"
			)
		);
		$get['order'] = "enrollments.name";
		$get['all'] = true; //GET ALL EXCLUDE LIMIT
		$get['count'] = true; //RETURN COUNT NOT ROW
		$get['array'] = false; //RETURN OBJECT NOT ARRAY
		$get['single'] = false; //RETURN ALL NOT SINGLE
		
	
		$this->load->library("pagination");
		$config = $this->pagination_style();
		$config["base_url"] = base_url() ."library/students";
		$suffix = '?'.http_build_query($_GET, '', "&");
		$suffix = str_replace("&submit=Search", "", $suffix);
		$config['suffix'] = $suffix;
		$config['first_url'] = $config['base_url'].$config['suffix']; 
		
		$this->view_data['total_rows'] = $config["total_rows"] = $this->M_core_model->get_record("enrollments", $get);
		
		$config["per_page"] = 30;
		$config['num_links'] = 10;
		$config["uri_segment"] = 3;
		// $config['use_page_numbers'] = TRUE;
		$this->pagination->initialize($config);
		
		//FOR PAGINATION
		$get['all'] = false;
		$get['count'] = false;
		if($this->input->post('submit') == "Print")
		{
			$get['all'] = true;
		}
		$config['start'] = $page;
		$config['limit'] = $config['per_page'];
		
		$get['start'] = $page;
		$get['limit'] = $config['per_page'];
		
		$this->view_data['search'] = $search = $this->M_core_model->get_record("enrollments", $get);
		$this->view_data['links'] = $this->pagination->create_links();
		// vd($search);
	}	

	public function employees($page = 0)
	{
		$this->menu_access_checker('library/borrowers');

		$this->view_data['usertype'] = 'employees';	

		$filter = false;
		$like = false;
		$order_by = false;
		
		$filter['employees.status'] = 1;
		
		$arr_filters = array();
		$suffix = "";
		
		/* GET SEARCH KEYWORDS AND ADD TO FILTER */
		if($_GET)
		{	
			if(isset($_GET['search_param']) && trim($_GET['search_param']) == 'all'){
				$this->view_data['search_param'] = $keyfield = trim($_GET['search_param']);
			}else{

				if(isset($_GET['search_param'])){
					$this->view_data['search_param'] = $keyfield = trim($_GET['search_param']);
					$this->view_data['keyword'] = $keyword = trim($_GET['keyword']); 
					
					if(trim($keyword)){ 
						
						if($keyfield == 'last_name'){ $like['employees.last_name'] = $keyword; }
						if($keyfield == 'middle_name'){ $like['employees.middle_name'] = $keyword; }
						if($keyfield == 'first_name'){ $like['employees.first_name'] = $keyword; }
						if($keyfield == 'student_id'){ $like['employees.employeeid'] = $keyword; }
						if($keyfield == 'department'){ $like['users.department'] = $keyword; }
					}
				}
			}
		}
		
		//CONFIGURATION
		$get['fields'] = array(
				'employees.id',
				'employees.employeeid as username',
				'users.department',
				"concat_ws('',employees.last_name,' , ',employees.first_name,' ',employees.middle_name) as fullname",
		);
		
		$get['where'] = $filter;
		$get['like'] = $like;
		$get['join'][] = array(
				"table" => "users",
				"on"	=> "users.login = employees.employeeid",
				"type"  => "LEFT"
			);
		
		$get['order'] = "employees.last_name";
		$get['all'] = true; //GET ALL EXCLUDE LIMIT
		$get['count'] = true; //RETURN COUNT NOT ROW
		$get['array'] = false; //RETURN OBJECT NOT ARRAY
		$get['single'] = false; //RETURN ALL NOT SINGLE
		
	
		$this->load->library("pagination");
		$config = $this->pagination_style();
		$config["base_url"] = base_url() ."library/employees";
		$suffix = '?'.http_build_query($_GET, '', "&");
		$suffix = str_replace("&submit=Search", "", $suffix);
		$config['suffix'] = $suffix;
		$config['first_url'] = $config['base_url'].$config['suffix']; 
		
		$this->view_data['total_rows'] = $config["total_rows"] = $this->M_employees->get_record(false, $get);
		
		$config["per_page"] = 30;
		$config['num_links'] = 10;
		$config["uri_segment"] = 3;
		// $config['use_page_numbers'] = TRUE;
		$this->pagination->initialize($config);
		
		//FOR PAGINATION
		$get['all'] = false;
		$get['count'] = false;
		$get['start'] = $page;
		$get['limit'] = $config['per_page'];
		
		$this->view_data['search'] = $search = $this->M_employees->get_record(false, $get);
		$this->view_data['links'] = $this->pagination->create_links();
		// vd($search);
	}

	/**
	 * Circulation Borrower's Profile
	 * @param string $usertype
	 * @param int $id Enrollment ID or Employee ID
	 */
	public function circulation($usertype=false, $id = false)
	{
		if(!$usertype){ show_404(); }
		if(!$id){ show_404(); }

		$this->load->model(['M_enrollments','M_employees']);

		$this->view_data['usertype'] = $usertype;
		$this->view_data['id'] = $id;
		if($usertype === "student"){
 			$this->view_data['student'] = $user = $this->M_enrollments->profile($id);
 		}else{
 			$this->view_data['employee'] = $user = $this->M_employees->get_profile($id);
 			// vd($user);
 		}

 		if(!$user){ show_404(); }

 		$this->view_data['transactions'] = $this->m_lib_c1->get_active_transactions($usertype, $id);

 		/******** CATCH THE RETURN SUBMIT BUTTON ********/
 		if($this->input->post('return_selected')){

 			if($this->input->post('chk_return')){
 				$rs = $this->m_lib_c2->return_details($this->input->post('chk_return'));
 				if($rs){

 					$this->_msg('s','Record was successfully return.', current_url());
 				}else{
 					$this->_msg('e','Transaction failed, please try again', current_url());
 				}
 			}else{
 				$this->_msg('e','No selected item to be return.', current_url());
 			}
 		}

 		/******** CATCH THE DELETE ITEM BUTTON ********/
 		if($this->input->post('remove_selected')){

 			if($this->input->post('chk_return')){
 				$rs = $this->m_lib_c2->remove_details($this->input->post('chk_return'));
 				if($rs){

 					$this->_msg('s','Record was successfully deleted.', current_url());
 				}else{
 					$this->_msg('e','Transaction failed, please try again', current_url());
 				}
 			}else{
 				$this->_msg('e','No selected item to be deleted.', current_url());
 			}
 		}
	}

	public function borrow($usertype = false, $id = false, $page = 0)
	{
		if($id === false){ show_404(); }
		if($usertype === false){ show_404(); }

		$this->load->model(['M_enrollments','M_employees']);

		$this->view_data['usertype'] = $usertype;
		$this->view_data['id'] = $id;
		if($usertype === "student"){
 			$this->view_data['student'] = $user = $this->M_enrollments->profile($id);
 		}else{
 			$this->view_data['employee'] = $user = $this->M_employees->get_profile($id);
 			// vd($user);
 		}

		$this->view_data['id'] = $id;
		$this->view_data['usertype'] = $usertype;
		$this->view_data['library_cart'] = $this->_get_library_cart($id); // get save item in the session to be shoot inside the cart
		// vp($this->session->userdata);
		//Load dropdowns
		$this->view_data['media_types'] = $this->M_media_types->get_for_dd(array('id','media_type'), false, 'All Types','media_type');
		$this->view_data['book_category'] = $this->M_library_book_category->get_for_dd(array('id','lbc_name'), false, 'All Subjects','lbc_name');

		/*** For PAGINATION ***/

		$filter = false;
		$like = false;
		$order_by = false;
		
		$arr_filters = array();
		$suffix = "";
		
		/* GET SEARCH KEYWORDS AND ADD TO FILTER */
		if($_GET)
		{	
			if(isset($_GET['item_name']) && $_GET['item_name'] != ""){
				$this->view_data['item_name'] = $like['library_books.book_name'] = trim($_GET['item_name']); }

			if(isset($_GET['accession_number']) && $_GET['accession_number'] != ""){
				$this->view_data['accession_number'] = $like['library_books.accession_number'] = trim($_GET['accession_number']); }
			
			if(isset($_GET['call_number']) && $_GET['call_number'] != ""){
				$this->view_data['call_number'] = $like['library_books.call_number'] = trim($_GET['call_number']); }	

			if(isset($_GET['item_barcode']) && $_GET['item_barcode'] != ""){
					$this->view_data['item_barcode'] = $like['library_books.book_barcode'] = trim($_GET['item_barcode']); }

			if(isset($_GET['item_author']) && $_GET['item_author'] != ""){
				$this->view_data['item_author'] = $like['library_books.book_author'] = trim($_GET['item_author']); }

			if(isset($_GET['item_isbn']) && $_GET['item_isbn'] != ""){
				$this->view_data['item_isbn'] = $like['library_books.book_isbn'] = trim($_GET['item_isbn']); }

			if(isset($_GET['item_type']) && $_GET['item_type'] != ""){
				$this->view_data['item_type'] = $filter['library_books.media_type_id'] = trim($_GET['item_type']); }

			if(isset($_GET['item_category']) && $_GET['item_category'] != ""){
				$this->view_data['item_category'] = $filter['library_books.book_category'] = trim($_GET['item_category']); }

			// CATCH BORROWING ITEM
			if(isset($_GET['add_to_cart']) && $_GET['add_to_cart'] != ""){

				$addc = $this->_add_to_cart($usertype, $id, $_GET['add_to_cart']);
				if($addc->status){
					$this->_msg('s',$addc->msg, current_url());
				}else{
					$this->_msg('e',$addc->msg, current_url());
				}
			}
		}
		
		//CONFIGURATION
		$get['fields'] = array(
				'library_books.*',
				'library_book_category.lbc_name as category',
				'book_copies - book_borrowed as available',
				'media_types.media_type',
		);

		$filter['(book_copies - book_borrowed) > '] = 0;
		
		$get['where'] = $filter;
		$get['like'] = $like;
		$get['not_in'] = array(
				'field' => 'library_books.id',
				'data' => $this->_get_library_cart($id, true)
			);

		$get['join'][] = array(
				'table' => 'media_types',
				'on' => 'library_books.media_type_id = media_types.id',
				'type' => 'left'
			);

		$get['join'][] = array(
				'table' => 'library_book_category',
				'on' => 'library_books.book_category = library_book_category.id',
				'type' => 'left'
			);
		
		$get['order'] = "library_books.book_name";
		$get['all'] = true; //GET ALL EXCLUDE LIMIT
		$get['count'] = true; //RETURN COUNT NOT ROW
		$get['array'] = false; //RETURN OBJECT NOT ARRAY
		$get['single'] = false; //RETURN ALL NOT SINGLE
		
	
		$this->load->library("pagination");
		$config = $this->pagination_style();
		$config["base_url"] = base_url() ."library/borrow/".$usertype.'/'.$id;
		$suffix = '?'.http_build_query($_GET, '', "&");
		$suffix = str_replace("&submit=Search", "", $suffix);
		$config['suffix'] = $suffix;
		$this->view_data['current_url'] = $config['first_url'] = $config['base_url'].$config['suffix']; 
		
		$this->view_data['total_rows'] = $config["total_rows"] = $this->M_library_books->get_record(false, $get);
		
		$config["per_page"] = 25;
		$config['num_links'] = 10;
		$config["uri_segment"] = 5;
		// $config['use_page_numbers'] = TRUE;
		$this->pagination->initialize($config);
		
		//FOR PAGINATION
		$get['all'] = false;
		$get['count'] = false;
		$get['start'] = $page;
		$get['limit'] = $config['per_page'];
		
		$this->view_data['search'] = $search = $this->M_library_books->get_record(false, $get);
		$this->view_data['links'] = $this->pagination->create_links();

		/*** End of PAGINATION ***/


		$this->view_data['system_message'] = $this->_msg();
		$this->load->library('token');
		$this->token->set_token();
		$this->view_data['token'] = $this->token->get_token();

		// CAPTURE BORROWING AND RESERVING PROCESS
		if($this->input->post('checkout') && $this->input->post('cart')){

			if($this->token->validate_token($this->input->post('form_token')))
			{
				$this->form_validation->set_rules('trndte','Transaction Date','required|trim|date|htmlspecialchars');
				if($this->form_validation->run() !== FALSE)
				{
					$rs = $this->m_lib_c1->check_out();

					/** Check if saved errors **/
					if($rs->status){

						if($rs->unavailable){ // if some book was already unavaliable

							$msg = " Some Item/s was already unavaliable";

							$this->_msg('s',$rs->msg.$msg, current_url());
						}else{

							unset($this->session->userdata['library_cart'][$id]);

							$this->_msg('s',$rs->msg, 'library/circulation/'.$usertype.'/'.$id);
						}
					}else{
						$this->_msg('e',$rs->msg, current_url());
					}

				}else{
					$this->view_data['system_message'] = '<div class="alert alert-danger">'.validation_errors().'</div>';
				}
			}
			else
			{
				$this->_msg('e','Submit failed form token Expired, Please try again ',current_url());
			}
		}
	}

	/**
	 * Get Library Cart
	 * @return array of item/media/books
	 */
	private function _get_library_cart($id, $return_ids = false)
	{
		$cart = $this->session->userdata('library_cart');

		if(isset($cart[$id]) && $cart[$id]){
			
			$cart = $cart[$id];
			// vp($cart);
			$items = false;
			foreach ($cart as $key => $_id) {
				// vp($_id);
				$get = $this->M_library_books->get(array('id'=>$_id));
				if($get){
					$items[] = $get;
				}
			}
			
			if($return_ids){
				return $cart;
			}

			return $items;

		}		

		return false;
	}

	/**
	 * Add to cart
	 * Save item in the session
	 */
	private function _add_to_cart($usertype = false, $id = false, $book_id = false)
	{
		if($usertype===false){ show_404(); }
		if($id===false){ show_404(); }
		if($book_id===false){ show_404(); }

		$sly = new stdClass;
		$sly->status = false;
		$sly->msg = "";

		// check if book exist and available
		$item = $this->M_library_books->get($book_id);
		if(!$item){ show_404(); }

		if(($item->book_copies - $item->book_borrowed) <= 0){
			$sly->msg = "Item was already unavailable";
			return $sly;
		}
		
		if($this->session->userdata('library_cart') === false){
			unset($books);
			$books[$id][] = $book_id;
			$this->session->set_userdata('library_cart', $books);
		}else{
			
			$data = $this->session->userdata('library_cart');

			if(!isset($data[$id])){
				unset($books);
				$data[$id][] = $book_id;
				$this->session->set_userdata('library_cart', $data);
			}else
			{
				$added_books = $data[$id];
				
				// check if book_id already in array
				if(is_array($added_books) && in_array($book_id, $added_books)){
					$sly->msg = "Item Already in the Cart";
					return $sly;
				}

				$data[$id][] = $book_id;
				$this->session->set_userdata('library_cart', $data);
			}
		}
		// vd($this->session->set_userdata('library_cart'));
		$sly->status = true;
		$sly->msg = "Item was successfully added to cart.";
		return $sly;
	}

	/**
	 * Delete Transaction
	 * Remove borrowed transaction - only update is_deleted to 1
	 */
	public function delete_transaction($usertype = false, $id = false, $lib_id = false)
	{
		if($usertype===false){ show_404(); }
		if($id===false){ show_404(); }
		if($lib_id===false){ show_404(); }

		$item  = $this->m_lib_c1->pull($lib_id, 'id');
		if(!$item){ show_404(); }
		if(has_return_item($lib_id)){ // if transaction already has a return
			$this->_msg('e','Transaction cannot be deleted.','library/circulation/'.$usertype.'/'.$id);
		}

		if($this->m_lib_c1->delete_transaction($lib_id)){
			$this->_msg('s','Transaction was removed successfully.','library/circulation/'.$usertype.'/'.$id);	
		}
		
		$this->_msg('e','Transaction failed please try again.','library/circulation/'.$usertype.'/'.$id);
	}

	/**
	 * Borrow History
	 * @param string $usertype student / employee
	 * @param int $id Enrollment Id / Employee Ed
	 * @param int $page
	 */
	public function history($usertype = false, $id = false, $page = 0)
	{
		if(!$usertype){ show_404(); }
		if(!$id){ show_404(); }

		$this->load->model(['M_enrollments','M_employees']);

		$this->view_data['usertype'] = $usertype;
		$this->view_data['id'] = $id;
		if($usertype === "student"){
 			$this->view_data['student'] = $user = $this->M_enrollments->profile($id);
 		}else{
 			$this->view_data['employee'] = $user = $this->M_employees->get_profile($id);
 			// vd($user);
 		}

 		if(!$user){ show_404(); }

 		/*** For PAGINATION ***/

		$like = false;
		$order_by = false;
		$filter['lib_circulationfile2.is_deleted'] = 0;
		$filter['lib_circulationfile1.usertype'] = $usertype;
		$filter['lib_circulationfile1.borrower_id'] = $id;
		$filter['lib_circulationfile1.is_deleted'] = 0;
		$filter['lib_circulationfile1.status'] = 'RETURN';
		
		/* GET SEARCH KEYWORDS AND ADD TO FILTER */
		if($_GET)
		{	
			if(isset($_GET['item_name']) && $_GET['item_name'] != ""){
				$this->view_data['item_name'] = $like['library_books.book_name'] = trim($_GET['item_name']); }
		}
		
		//CONFIGURATION
		$get['fields'] = array(
				'library_books.book_name',
				'library_books.book_desc',
				'library_books.book_barcode',
				'library_books.accession_number',
				'library_books.call_number',
				'lib_circulationfile1.trndte',
				'lib_circulationfile1.remarks',
				'lib_circulationfile2.retdte',
				'lib_circulationfile2.cir_status',
				'lib_circulationfile2.day',
				'lib_circulationfile2.is_late',
		);
		
		$get['where'] = $filter;
		$get['like'] = $like;

		$get['join'][] = array(
				'table' => 'lib_circulationfile1',
				'on' => 'lib_circulationfile1.id = lib_circulationfile2.circulation_id',
				'type' => 'left'
			);

		$get['join'][] = array(
				'table' => 'library_books',
				'on' => 'library_books.id = lib_circulationfile2.media_id',
				'type' => 'left'
			);
		
		$get['order'] = "lib_circulationfile1.trndte DESC,lib_circulationfile2.id DESC";
		$get['all'] = true; //GET ALL EXCLUDE LIMIT
		$get['count'] = true; //RETURN COUNT NOT ROW
		$get['array'] = false; //RETURN OBJECT NOT ARRAY
		$get['single'] = false; //RETURN ALL NOT SINGLE
		
	
		$this->load->library("pagination");
		$config = $this->pagination_style();
		$config["base_url"] = base_url() ."library/circulation/".$usertype.'/'.$id;
		$suffix = '?'.http_build_query($_GET, '', "&");
		$suffix = str_replace("&submit=Search", "", $suffix);
		$config['suffix'] = $suffix;
		$this->view_data['current_url'] = $config['first_url'] = $config['base_url'].$config['suffix']; 
		
		$this->view_data['total_rows'] = $config["total_rows"] = $this->m_lib_c2->get_record(false, $get);
		
		$config["per_page"] = 25;
		$config['num_links'] = 10;
		$config["uri_segment"] = 5;
		// $config['use_page_numbers'] = TRUE;
		$this->pagination->initialize($config);
		
		//FOR PAGINATION
		$get['all'] = false;
		$get['count'] = false;
		$get['start'] = $page;
		$get['limit'] = $config['per_page'];
		
		$this->view_data['search'] = $search = $this->m_lib_c2->get_record(false, $get);
		$this->view_data['links'] = $this->pagination->create_links();

		/*** End of PAGINATION ***/
	}

	public function list_of_borrowed_media($page = 0)
	{
		$this->menu_access_checker();

		$this->view_data['media_types'] = $this->M_media_types->get_for_dd(array('id','media_type'), false, 'All Types','media_type');
		$this->view_data['book_category'] = $this->M_library_book_category->get_for_dd(array('id','lbc_name'), false, 'All Categories','lbc_name');

 		/*** For PAGINATION ***/

		$like = false;
		$order_by = false;
		$filter['library_books.book_borrowed > '] = 0;
		
		/* GET SEARCH KEYWORDS AND ADD TO FILTER */
			if($_GET)
			{	
				if(isset($_GET['item_name']) && $_GET['item_name'] != ""){
					$this->view_data['item_name'] = $like['library_books.book_name'] = trim($_GET['item_name']); }

				if(isset($_GET['accession_number']) && $_GET['accession_number'] != ""){
					$this->view_data['accession_number'] = $like['library_books.accession_number'] = trim($_GET['accession_number']); }
			
				if(isset($_GET['call_number']) && $_GET['call_number'] != ""){
					$this->view_data['call_number'] = $like['library_books.call_number'] = trim($_GET['call_number']); }

				if(isset($_GET['item_barcode']) && $_GET['item_barcode'] != ""){
					$this->view_data['item_barcode'] = $like['library_books.book_barcode'] = trim($_GET['item_barcode']); }

				if(isset($_GET['item_author']) && $_GET['item_author'] != ""){
					$this->view_data['item_author'] = $like['library_books.book_author'] = trim($_GET['item_author']); }

				if(isset($_GET['item_isbn']) && $_GET['item_isbn'] != ""){
					$this->view_data['item_isbn'] = $like['library_books.book_isbn'] = trim($_GET['item_isbn']); }

				if(isset($_GET['item_type']) && $_GET['item_type'] != ""){
					$this->view_data['item_type'] = $filter['library_books.media_type_id'] = trim($_GET['item_type']); }

				if(isset($_GET['item_category']) && $_GET['item_category'] != ""){
					$this->view_data['item_category'] = $filter['library_books.book_category'] = trim($_GET['item_category']); }
			}
		
		/* CONFIGURATION */
			$get['fields'] = array(
					'library_books.*',
					'library_book_category.lbc_name as category',
					'book_copies - book_borrowed as available',
					'media_types.media_type',
			);
			
			$get['where'] = $filter;
			$get['like'] = $like;
			
			$get['join'][] = array(
					'table' => 'media_types',
					'on' => 'library_books.media_type_id = media_types.id',
					'type' => 'left'
				);

			$get['join'][] = array(
					'table' => 'library_book_category',
					'on' => 'library_books.book_category = library_book_category.id',
					'type' => 'left'
				);
			
			$get['order'] = "library_books.book_name";
			$get['all'] = true; //GET ALL EXCLUDE LIMIT
			$get['count'] = true; //RETURN COUNT NOT ROW
			$get['array'] = false; //RETURN OBJECT NOT ARRAY
			$get['single'] = false; //RETURN ALL NOT SINGLE
		
		$this->load->library("pagination");
		$config = $this->pagination_style();
		$config["base_url"] = base_url() ."library/list_of_borrowed_media";
		$suffix = '?'.http_build_query($_GET, '', "&");
		$suffix = str_replace("&submit=Search", "", $suffix);
		$config['suffix'] = $suffix;
		$this->view_data['current_url'] = $config['first_url'] = $config['base_url'].$config['suffix']; 
		
		$this->view_data['total_rows'] = $config["total_rows"] = $this->M_library_books->get_record(false, $get);
		
		$config["per_page"] = 2;
		$config['num_links'] = 10;
		$config["uri_segment"] = 3;
		// $config['use_page_numbers'] = TRUE;
		$this->pagination->initialize($config);
		
		//FOR PAGINATION
		$get['all'] = false;
		$get['count'] = false;
		$get['start'] = $page;
		$get['limit'] = $config['per_page'];
		
		$this->view_data['search'] = $search = $this->M_library_books->get_record(false, $get);
		$this->view_data['links'] = $this->pagination->create_links();

		/*** End of PAGINATION ***/

		/** get borrowers **/
		$borrowers = false;
		if($search){
			foreach ($search as $key => $i) {
				$borrowers[$i->id] = $this->m_lib_c2->get_borrowers($i->id);
			}
		}
		$this->view_data['borrowers'] = $borrowers;
	}

	public function media_masterlist($id = false)
	{
		$this->menu_access_checker();
		$this->view_data['type'] = $t = $this->M_library_books->get_master_list_by_type();
		$this->view_data['category'] = $this->M_library_books->get_master_list_by_category();
	}

	/**
	 * Download Media CSV format
	 */
	public function download_media_csv_format()
	{
		$this->menu_access_checker('library/upload_media');
		$this->load->helper('csv');
    	
		array_to_csv($this->_media_csv_format(), 'media_format_v1.csv');
	}

	private function _media_csv_format($return_fields = false){

		$fields = array();
		$fields[0][0] = 'Media Type';
		$fields[0][1] = 'Media Subject';
		$fields[0][2] = 'Media Accession Number';
		$fields[0][3] = 'Media Call Number';
		$fields[0][4] = 'Media Title';
		$fields[0][5] = 'Media Description';
		$fields[0][6] = 'Number of Copies (Integer)';	
		$fields[0][7] = 'Media ISBN';
		$fields[0][8] = 'Media Author';	
		$fields[0][9] = 'Media Publisher';	
		$fields[0][10] = 'Date Publish (yyyy-mm-dd)';
		$fields[0][11] = 'Barcode';

		if($return_fields){
			$fields = array( 
				'Subject',
				'Type',
	    		'Acc. No.',
				'Call No.',
				'Title',
	    		'Description',
	    		'Barcode',
	    		'Author',
	    		'ISBN',
	    		'Publisher',
	    		'Date Pub.',
	    		'Qty');
		}

		return $fields;
	}

	public function download_media_category_master($id = false)
	{
		$this->menu_access_checker('library/media_masterlist');

		$this->load->helper('csv');

		$cat = $this->M_library_books->get_master_list_by_category($id);

		$format = $this->_media_csv_format(true);

		$sly[] = array();
		$sly[] = $format;
		
		if($cat){
			foreach ($cat as $key => $c) {
				$h = $c['head'];
				$t = $c['tail'];
				
				if(!$t){ continue; }

				foreach ($t as $k => $b) {

					$ct = $k == 0 ? $h->lbc_name : '';

					$sly[] = array(
						$ct,
						$b->media_type,
						$b->accession_number,
						$b->call_number,
						$b->book_name,
						$b->book_desc,
						$b->book_barcode,
						$b->book_author,
						$b->book_isbn,
						$b->book_publisher,
						$b->book_dop && $b->book_dop !== "1970-01-01" ? date('m/d/Y', strtotime($b->book_dop)) : '',
						$b->book_copies,
					);
				}

			}
		}

		array_to_csv($sly, 'category_master_list.csv');
	}

	public function download_media_type_master($id = false)
	{
		$this->menu_access_checker('library/media_masterlist');

		$this->load->helper('csv');

		$cat = $this->M_library_books->get_master_list_by_type($id);

		$format = $this->_media_csv_format(true);

		// $sly[] = array();
		$sly[] = $format;
		
		if($cat){
			foreach ($cat as $key => $c) {
				$h = $c['head'];
				$t = $c['tail'];
				
				if(!$t){ continue; }

				foreach ($t as $k => $b) {

					$ct = $k == 0 ? $h->media_type : '';

					$sly[] = array(
						$ct,
						$b->media_type,
						$b->accession_number,
						$b->call_number,
						$b->book_name,
						$b->book_desc,
						$b->book_barcode,
						$b->book_author,
						$b->book_isbn,
						$b->book_publisher,
						$b->book_dop && $b->book_dop !== "1970-01-01" ? date('m/d/Y', strtotime($b->book_dop)) : '',
						$b->book_copies,
					);
				}
			}
		}
		array_csv($sly, 'media_type_master_list.csv');
		exit();
	}

	public function download_all_media_master()
	{
		$this->menu_access_checker('library/media_masterlist');

		$this->load->helper('csv');

		set_time_limit(0);

		$this->load->model('M_settings');

		$settings = $this->M_settings->get_settings();

		unset($get);
		$data['school_name'] = $settings->school_name;
		$get['order'] = 'book_name';
		$get['fields'] = array(
				'library_books.*',
				'library_book_category.lbc_name as category',
				'book_copies - book_borrowed as available',
				'media_types.media_type',
		);

		$get['join'][] = array(
				'table' => 'media_types',
				'on' => 'library_books.media_type_id = media_types.id',
				'type' => 'left'
			);

		$get['join'][] = array(
				'table' => 'library_book_category',
				'on' => 'library_books.book_category = library_book_category.id',
				'type' => 'left'
			);
		
		$search = $this->M_library_books->get_record(false, $get);

		$sly[] = $this->_media_csv_format(true);
		if($search){
			foreach ($search as $key => $b) {
				
				$sly[] = array(
						$b->media_type,
						$b->category,
						$b->accession_number,
						$b->call_number,
						$b->book_name,
						$b->book_desc,
						$b->book_barcode,
						$b->book_author,
						$b->book_isbn,
						$b->book_publisher,
						$b->book_dop && $b->book_dop !== "1970-01-01" ? date('m/d/Y', strtotime($b->book_dop)) : '',
						$b->book_copies,
					);
			}
		}

		array_to_csv($sly, 'media_master_list.csv');
	}

	public function circulation_report()
	{
		$this->menu_access_checker();

		//Load dropdowns
		$this->view_data['media_types'] = $this->M_media_types->get_for_dd(array('id','media_type'), false, 'All Types','media_type');
		$this->view_data['book_category'] = $this->M_library_book_category->get_for_dd(array('id','lbc_name'), false, 'All Subjects','lbc_name');
		$this->view_data['students'] = $this->M_enrollments->get_student_for_dd("name, name");
		$this->view_data['employees'] = $this->M_employees->get_employees_for_dd("name, name");
		$this->view_data['medias'] = $this->M_library_books->get_for_dd(array('book_name','book_name'),false, "Select Media",'book_name');

	}

		/**
	 * Upload Media CSV
	 * Upload Data From CSV File
	 */
	public function upload_media()
	{
		$this->menu_access_checker();

		$this->load->library('token');
		$this->token->set_token();
		$this->view_data['token'] = $this->token->get_token();		

		if($this->input->post('upload_csv') && $_FILES['userfile']['name'] ){
			
			if($this->token->validate_token($this->input->post('form_token'))){

				set_time_limit(0);
				$config['upload_path'] = './assets/uploads/school';
			  $config['allowed_types'] = 'csv';
				$config['max_size']	= '10000';
				$config['file_name'] = 'library_media_upload';
				$config['overwrite'] = TRUE;
				$config['width']	 = 188;
				$config['height'] = 187;
				$this->load->library('upload', $config);
				
				if ( ! $this->upload->do_upload()){

					$this->view_data['system_message'] = $this->_msg('e',' '.$this->upload->display_errors());
				}else{
					
					$data = array('upload_data' => $this->upload->data());
					//resize start
					$source_path = $data['upload_data']['full_path'];

					$up = $this->M_library_books->upload_from_csv($source_path);

					if($up->status){

						$this->_msg('s', $up->msg, current_url());
					}else{
						$this->_msg('e',$up->msg,current_url());
					}	
				}

			}else{
				$this->_msg('e','Upload Form breached!', current_url());
			}
		}
	}
}