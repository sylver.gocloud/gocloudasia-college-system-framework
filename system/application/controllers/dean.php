<?php

class Dean Extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->session_checker->secure_page(array('dean'));
		$this->session_checker->open_semester();
		$this->load->model(array('M_enrollments'));
		$this->load->helper('my_dropdown');
	
	}
	
	public function index()
	{
		
	
	
	}
	
	public function deleted_accounts()
	{
		
		$this->session_checker->check_if_alive();
		if($this->input->post('search_deleted_user'))
		{
			$lastname = $this->input->post('lastname',TRUE);
			$firstname = $this->input->post('firstname',TRUE);
			$idno = $this->input->post('idno',TRUE);
			$semester_id_eq = $this->input->post('semester_id_eq',TRUE);
			$sy_from_eq = $this->input->post('sy_from_eq',TRUE);
			$sy_to_eq = $this->input->post('sy_to_eq',TRUE);
			$is_paid_eq = $this->input->post('is_paid_eq',TRUE);
			
			$this->view_data['search'] = $this->M_enrollments->search_deleted_accounts($firstname, $lastname, $idno, $semester_id_eq, $sy_from_eq, $sy_to_eq);
		}
	}
	
	/* master list */
	public function master_list_by_course()
	{
		$this->session_checker->check_if_alive();
		$this->load->model('M_courses');
		$this->view_data['masterlist'] = $this->M_courses->masterlist();
		$this->view_data['unassigned'] = $this->M_courses->unassigned();
	}
	
	public function master_list($course_id = false,$year = false)
	{
	
		if($course_id !== false AND (int)$course_id)
		{
			$this->load->model('M_courses');
			$this->view_data['course_id'] = $course_id;
			$this->view_data['course_id'] = $course_id;
			$this->view_data['search_results'] = $this->M_courses->masterlist_course_enrollment_profile($course_id);
		
		}else{
			if($this->input->post('masterlist') !== FALSE)
			{
				
				$this->load->model('M_courses');
				$course_id = $this->input->post('course');
				$year_id = $this->input->post('years');
				$this->view_data['search_results'] = $this->M_courses->masterlist_course_enrollment_profile($course_id,$year_id);
				$this->view_data['course_id'] = $course_id;
			}else
			{
				$this->view_data['course_id'] = NULL;
			}
		}
	}
	
	public function destroy_enrollment($id = false)
	{
		$jquery = $this->input->post('jquery');
		if($jquery == false)
		{
			if($this->M_enrollments->destroy_enrollment($id))
			{
				log_message('info','Destroy Enrollment By: '.$this->user. '; Id: '.$id.';');
				$this->session->set_flashdata('system_message', '<div class="alert alert-success">Student Deleted</div>');
				redirect(current_url());
			}else
			{
				log_message('error','Destroy Enrollment By: '.$this->user. 'FAILED  on dean/destroy_enrollment; Id: '.$id.';');
				$this->view_data['system_message'] = '<div class="alert alert-danger">An error was encountered while processing your request</div>';
			}
		}else{
			if($this->M_enrollments->destroy_enrollment($id))
			{
				log_message('info','Destroy Enrollment By: '.$this->user. '; Id: '.$id.';');
				echo 'true';
				die();//stops php from sending the views so that browser will only receive true
			}else
			{
				log_message('error','Destroy Enrollment By: '.$this->user. 'FAILED on dean/destroy_enrollment; Id: '.$id.';');
				echo 'false';
				die();//stops php from sending the views so that browser will only receive false
			}
		}
	}
	
	public function restore_enrollment($id = false)
	{
		$jquery = $this->input->post('jquery');
		if($jquery == false)
		{
			if($this->M_enrollments->restore_enrollment($id))
			{	
				log_message('info','Restore Enrollment By: '.$this->user. '; Id: '.$id.';');
				$this->session->set_flashdata('system_message', '<div class="alert alert-success">Student Restored</div>');
				redirect(current_url());
			}else
			{
				log_message('info','Restore Enrollment By: '.$this->user. 'Failed on dean/restore_enrollment; Id: '.$id.';');
				$this->view_data['system_message'] = '<div class="alert alert-danger">An error was encountered while processing your request</div>';
			}
		}else{
			if($this->M_enrollments->restore_enrollment($id))
			{
				log_message('info','Restore Enrollment By: '.$this->user. '; Id: '.$id.';');
				echo 'true';
				die();//stops php from sending the views so that browser will only receive true
			}else
			{
				log_message('error','Restore Enrollment By: '.$this->user. 'FAILED  on dean/restore_enrollment; Id: '.$id.';');
				echo 'false';
				die();//stops php from sending the views so that browser will only receive false
			}
		}
	}

	







}