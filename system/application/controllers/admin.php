<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->session_checker->open_semester();
		$this->session_checker->check_if_alive();
		$this->load->helper(array('url_encrypt'));

		if(ENVIRONMENT === "production"){
			$this->session_checker->secure_page(array('admin'));
		}
	}

	public function index(){
		// $this->load->model('M_users');
		// $this->load->library(array('form_validation'));
		// $this->load->helper(array('url','form'));
		// $this->load->library(array('form_validation','login'));		
		
		
	}
	
	public function payment_record_fix()
	{
		// $this->load->model('M_menus');
		
		// $rs = $this->M_menus->payment_record_fix();
	}
	
	public function run_sql()
	{
		show_404(); //SYLVER - I TRANSFER THIS FUNCTION THE CONTROL PANEL
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		
		if($this->input->post('submit') == "Submit")
		{
			$this->load->model(array('M_core_model'));
			$date = $this->input->post('date');
			$qry = trim($this->input->post('qry'));
			$password = trim($this->input->post('password'));
			$sec_code = trim($this->input->post('secret_code'));
			
			$this->view_data['qry'] = $qry;
			$this->view_data['date'] = $date;
			
			$this->load->library(array('form_validation','login'));		
			
			if($password != '' && $qry != '')
			{
				$val = $this->login->_validate_password($this->session->userdata['userlogin'],$_POST['password'], $this->session->userdata['userType']);
				
				if($val == FALSE)
				{
					$this->session->set_flashdata('system_message','<div class="alert alert-danger">Invalid password.</div>');
					// redirect('auth/login/'.$userType);
					redirect(current_url());
				}
				else
				{
					/*SYLVER*/
					//VERIFY SECRET CODE
					$s_hash = "5d8f5c1acb3a29cdd472827023c276edbc999262";
					$s_salt = "76843bb69a9d8e5066a9d8dddc90d586c54c797c";
					if($this->login->_validate_code($sec_code , $s_hash, $s_salt))
					{
						$this->M_core_model->run_sql($qry);
					}
					else
					{
						$this->session->set_flashdata('system_message','<div class="alert alert-danger">Invalid secret password.</div>');
						redirect(current_url());
					}
				}
			}
		}
	}

	public function fix_student_grade()
	{
		$this->disable_menus = true;
		$this->disable_views = true;
		$this->load->model('M_student_subjects');

		$this->M_student_subjects->fix_student_grade();		
	}

	public function back_up_database(){
		// Load the DB utility class
		$this->load->dbutil();

		// Backup your entire database and assign it to a variable
		$backup =& $this->dbutil->backup(); 

		// Load the file helper and write the file to your server
		$this->load->helper('file');
		write_file('/path/to/mybackup.gz', $backup); 

		// Load the download helper and send the file to your desktop
		$this->load->helper('download');
		force_download('mybackup.gz', $backup);
	}

}
?>
