<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Transcript extends MY_Controller {
  
	public function __construct(){
		parent::__construct();
		$this->session_checker->open_semester();
		$this->load->helper(array('url_encrypt'));
		
		$this->menu_access_checker(array(
			'search/search_student',
			'enrollees/daily_enrollees',
			'search/master_list_by_course',
			'search/search_enrollee',
			'search/list_students/paid',
			'search/list_students/unpaid',
			'search/list_students/fullpaid',
			'search/list_students/partialpaid',
		));
	}
	
	public function view($id = false, $search_type = "all")
	{
	  //CHECK DEPARTMENT STUDENT PROFILE ACCESS 
		$this->check_stud_access('stud_otr');
	  $this->load->model(array("M_student_grades", "M_enrollments","M_transcript","M_grades_file"));
	  
	  //Top header data of student
		$this->view_data['student'] = $p = $this->M_enrollments->profile($id); if($p === false){ show_404(); }
		$this->view_data['enrollment_id'] = $id;

		//Top Search Parameter
		$this->view_data['search_type'] = $search_type;		

	  #load student library
		set_time_limit(0);
		$this->load->library('_student',array('enrollment_id'=>$id));
		$this->view_data['_student'] = $this->_student;
		$this->view_data['transcript'] = $tr = $this->M_transcript->get_transcript($p->studid);
		$this->view_data['get_final_grade'] = function($eid,$ss_id){ return $this->M_grades_file->get_final_grade($eid,$ss_id); };
	  
	  //Count Issues
	  $this->issue_count($p->user_id);
	}
}
