<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Years extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->session_checker->open_semester();
		// $this->session_checker->secure_page('admin');
		$this->menu_access_checker('years');
	}
	
	// Create
	public function create()
	{
		$this->load->model(array('M_years'));
		
		$this->view_data['year'] = FALSE;
		
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		
		if($_POST)
		{
			$data = $this->input->post('years');
			$data['created_at'] = date('Y-m-d H:i:s');
			$data['updated_at'] = date('Y-m-d H:i:s');
			
			$result = $this->M_years->create_years($data);
			
			if($result['status'])
			{
				log_message('error','Years Created by: '.$this->user.'Success; Years Id: '.$id);
				$this->session->set_flashdata('system_message', '<div class="alert alert-danger">Year successfully added.</div>');
				redirect('years');
			}
		}
	}
	
	// Retrieve
	public function index()
	{
		//LOAD CRUD LIBRARY
		unset($config);
		$config['title'] = 'Years';
		$config['type'] = 'query';
		$config['sql'] = "SELECT id, year, level FROM years";
		$config['table'] = "years";
		$config['order'] = "level";
		// $config['column_name'] = array('Year','Level');
		$config['disable_system_message'] = true;
		$config['disable_title'] = true;
		$config['pager'] = array(
			"per_page" => 10,
			"num_links" => 3,
			"uri_segment" => 3
		);
		$config['search'] = true;

		$config['crud'] = array(
			0 => array(					
					'field' => 'year',
					'type'	=> 'text',
					'label' => 'Name of Year',
					'rules' => 'required|is_unique[years.year]',
					// 'help'  => 'Name of the Controller to be used.',
					'attr'  => array(
						'name'        	=> 'year',
						'id'          	=> 'year',
						'placeHolder'   => 'Required',
						'maxlength'   	=> '100',
						'required'		=> null
						)
				),
			1 => array(					
					'field' => 'level',
					'type'	=> 'text',
					'label' => 'Order',
					'rules' => 'required|numeric',
					'attr'  => array(
						'name'        	=> 'level',
						'id'          	=> 'level',
						'placeHolder'   => 'Required',
						'maxlength'   	=> '3',
						'required'		=> null
						)
				),
			// 6 => array(					
			// 		'field' => 'visible',
			// 		'type'	=> 'select',
			// 		'label' => 'Is Active',
			// 		'rules' => 'required',
			// 		'attr'  => array(
			// 			'name'        	=> 'visible',
			// 			'id'          	=> 'visible',
			// 			'required'		=> null
			// 			),
			// 		'select_option'		=> array(
			// 			1               => 'Yes',
			// 			0				=> 'No'
			// 			)
			// 	),
			);

		$this->load->library('_sylvercrud',$config);

		$this->view_data['data_record'] = $this->_sylvercrud->_html;
	}
	
	public function index_old()
	{
		#WAS CHANGED
		show_404();
		$this->load->model(array('M_years'));
		$this->view_data['years'] = $this->M_years->find_all();
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
	}
	
	public function display($id)
	{
		$this->load->model(array('M_years'));
		
		$this->view_data['years'] = $this->M_years->get_years($id);
	}
	
	// Update
	public function edit($id)
	{
		$this->load->model(array('M_years'));
		
		$this->view_data['years'] = $this->M_years->get_years($id);
		
		if($_POST)
		{
			$data = $this->input->post('years');
			$data['updated_at'] = date('Y-m-d H:i:s');
			
			$result = $this->M_years->update_years($data, $id);
			
			if($result['status'])
			{
				log_message('error','Years Updated by: '.$this->user.'Success; Years Id: '.$id);
				$this->session->set_flashdata('system_message', '<div class="alert alert-danger">Year successfully updated.</div>');
				redirect('years');
			}
		}
	}
	
	public function destroy($id)
	{
		$this->load->model(array('M_years'));
		
		$result = $this->M_years->delete_years($id);
		log_message('error','Years Deleted by: '.$this->user.'Success; Years Id: '.$id);
		$this->session->set_flashdata('system_message', '<div class="alert alert-danger">Year successfully deleted.</div>');
		redirect('years');
	}
}