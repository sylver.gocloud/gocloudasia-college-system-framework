<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Payment_record extends MY_Controller 
{

	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('url','form','url_encrypt'));
		$this->load->library(array('form_validation'));
		$this->load->model(array('M_settings','M_enrollments','M_payment_records','M_student_total_file'));
		$this->session_checker->open_semester();
		$this->session_checker->check_if_alive();
		
	}
	
	public function index(){
		//vd($this->session);
	}
	
	public function create(){
	
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		$this->token->set_token();
		$this->view_data['form_token'] = $this->token->get_token();
		
		$this->form_validation->set_message('is_unique', 'The %s Entered is already in Use');
		
		if($this->form_validation->run('record_payment2') == true)
		{	
			$form_token = $this->input->post('form_token');
			
			if($this->token->validate_token($form_token) == TRUE)
			{
				$input['receipt_number'] = $this->input->post('receipt_number',TRUE);
				$input['group_or_no'] = $this->input->post('group_or_no',TRUE);
				$input['date_of_payment'] = $this->input->post('date_of_payment',TRUE);
				$input['amount'] = $this->input->post('amount',TRUE);
				$input['old_account'] = $this->input->post('old_account',TRUE);
				$input['other'] = $this->input->post('other',TRUE);
				$input['remarks'] = $this->input->post('remarks',TRUE);
				$input['student_id'] = $this->input->post('studid',TRUE);
				
				//GET LAST ENROLLMENT WITH BALANCE
				$xenroll = $this->M_enrollments->get_last_enrollment_with_balance($input['student_id']);
				
				if($xenroll)
				{	
					$enrollment_id = $xenroll->id;
					
					$result = $this->M_payment_records->add_payment_record($input,$enrollment_id);
					
					if($result['status'] == 'true')
					{
						log_message('info','add payment record by: '.$this->user.' OR# '.$input['receipt_number'].'; Amount: '.$input['amount'].'; From student enrollment id: '.$enrollment_id );
						$this->session->set_flashdata('system_message' ,'<div class="alert alert-success">'.$result['log'].'</div>');					
						$this->token->destroy_token();
						redirect(current_url());
					}else{
						log_message('error',$result['log']);
						$this->view_data['system_message'] ='<div class="alert alert-danger">'.$result['log'].'</div>';					
					}
					
					$this->update_enrollment_status($enrollment_id); //UPDATE ENROLLMENT IF PAID, FULL , PARTIAL
				}else{
					$this->view_data['system_message'] ='<div class="alert alert-danger">No updaid transaction of this student found.</div>';		
				}
			}else{
				$this->view_data['system_message'] ='<div class="alert alert-danger">Invalid FOrm Submit</div>';		
			}
		}
	}
	
	public function add($enrollment_id = false)
	{
		if($enrollment_id !== false AND ctype_digit($enrollment_id))
		{	
			$this->view_data['system_message'] = $this->session->flashdata('system_message');
			$this->session_checker->check_if_alive();
			$this->token->set_token();
			$this->view_data['form_token'] = $this->token->get_token();
			$this->view_data['enrollment_id'] = $enrollment_id;
		
			$this->form_validation->set_message('is_unique', 'The %s Entered is already in Use');
			if($this->form_validation->run('record_payment') !== FALSE)
			{
				$form_token = $this->input->post('form_token');
				if($this->token->validate_token($form_token) == TRUE)
				{
					$input['receipt_number'] 	= $this->input->post('receipt_number',TRUE);
					$input['group_or_no'] 		= $this->input->post('group_or_no',TRUE);
					$input['date_of_payment'] 	= $this->input->post('date_of_payment',TRUE);
					$input['amount'] = $amount 	= $this->input->post('amount',TRUE);
					$input['old_account'] 		= $old_account = $this->input->post('old_account',TRUE);
					$input['other'] = $other 	= $this->input->post('other',TRUE);
					$total_amount 				= floatval($amount) + floatval($old_account) + floatval($other);
					$input['remarks'] 			= $this->input->post('remarks',TRUE);
					
					$enrollment_id = $this->input->post('enrollment_id',TRUE);
					
					$result = $this->M_payment_records->add_payment_record($input,$enrollment_id);
					
					if($result['status'] == 'true')
					{
						//DEDUCT TOTAL AMOUNT TO STUDENT TOTAL BALANCE
						$this->M_student_total_file->deduct_student_payment($enrollment_id, $total_amount);
						
						//UPDATE ENROLLMENT IF PAID, FULL , PARTIAL AND UPDATE SUBJECT LOADS
						$this->M_enrollments->update_enrollments_status($enrollment_id); 
						
						log_message('info','add payment record by: '.$this->user.' OR# '.$input['receipt_number'].'; Amount: '.$input['amount'].'; From student enrollment id: '.$enrollment_id );
						$this->session->set_flashdata('system_message' ,'<div class="alert alert-success">'.$result['log'].'</div>');					
						$this->token->destroy_token();
						redirect(current_url());
					}else
					{
						log_message('error',$result['log']);
						$this->view_data['system_message'] ='<div class="alert alert-danger">'.$result['log'].'</div>';					
					}
				}else{
					$this->view_data['system_message'] ='<div class="alert alert-danger">Invalid FOrm Submit</div>';		
				}
			}
		}else{
			show_404();
		}
	}
	
	public function add_deduction($enrollment_id = false)
	{
		if($enrollment_id !== false AND ctype_digit($enrollment_id))
		{
			$this->view_data['system_message'] = $this->session->flashdata('system_message');
			$this->session_checker->check_if_alive();
			$this->view_data['enrollment_id'] = $enrollment_id;
			$this->token->set_token();
			$this->view_data['form_token'] = $this->token->get_token();
			
			$this->form_validation->set_rules('date','Date','required');
			$this->form_validation->set_rules('amount','Amount','required');
			if($this->form_validation->run() !== FALSE)
			{
				$form_token = $this->input->post('form_token');
				if($this->token->validate_token($form_token) == TRUE)
				{
					$input['amount'] =  $total_amount = $this->input->post('amount',TRUE);
					$input['remarks'] = $this->input->post('remarks',TRUE);
					$input['enrollment_id'] = $this->input->post('enrollment_id',TRUE);
					$result = $this->M_payment_records->add_deduction($input,$enrollment_id);
					if($result['status'] == 'true')
					{
					 
						log_message('info','add deductions: '.$this->user.'; Amount: '.$input['amount'].'; To student enrollment id: '.$input['enrollment_id']);
						$this->session->set_flashdata('system_message' ,'<div class="alert alert-success">'.$result['log'].'</div>');

						//DEDUCT TOTAL AMOUNT TO STUDENT TOTAL BALANCE
						$this->M_student_total_file->deduct_student_payment($enrollment_id, $total_amount, "DEDUCTION");
						
						//UPDATE ENROLLMENT IF PAID, FULL , PARTIAL AND UPDATE SUBJECT LOADS
						$this->M_enrollments->update_enrollments_status($enrollment_id); 
						
						$this->token->destroy_token();
						//redirect(current_url());
						
					}else
					{
						log_message('error',$result['log']);
						$this->view_data['system_message'] ='<div class="alert alert-danger">'.$result['log'].'</div>';					
					}
				}
				else
				{
						log_message('error','Invalid FOrm Submit');
						$this->view_data['system_message'] ='<div class="alert alert-danger">Invalid FOrm Submit</div>';
				}
			}
		}else
		{
			show_404();
		}
	}
	
	
	public function add_deduction_old($enrollment_id = false)
	{
		show_404(); //BACK UP
		
		if($enrollment_id !== false AND ctype_digit($enrollment_id))
		{
			$this->view_data['system_message'] = $this->session->flashdata('system_message');
			$this->session_checker->check_if_alive();
			$this->view_data['enrollment_id'] = $enrollment_id;
			$this->token->set_token();
			$this->view_data['form_token'] = $this->token->get_token();
			
			$this->form_validation->set_rules('date','Date','required');
			$this->form_validation->set_rules('amount','Amount','required');
			if($this->form_validation->run() !== FALSE)
			{
				$form_token = $this->input->post('form_token');
				if($this->token->validate_token($form_token) == TRUE)
				{
					$input['amount'] = $this->input->post('amount',TRUE);
					$input['remarks'] = $this->input->post('remarks',TRUE);
					$input['enrollment_id'] = $this->input->post('enrollment_id',TRUE);
					$result = $this->M_payment_records->add_deduction($input,$enrollment_id);
					if($result['status'] == 'true')
					{
					  $this->update_enrollment_status($enrollment_id);
						log_message('info','add deductions: '.$this->user.'; Amount: '.$input['amount'].'; To student enrollment id: '.$input['enrollment_id']);
						$this->session->set_flashdata('system_message' ,'<div class="alert alert-success">'.$result['log'].'</div>');					
						$this->token->destroy_token();
						redirect(current_url());
						
					}else
					{
						log_message('error',$result['log']);
						$this->view_data['system_message'] ='<div class="alert alert-danger">'.$result['log'].'</div>';					
					}
				}else
				{
						log_message('error',$result['log']);
						$this->view_data['system_message'] ='<div class="alert alert-danger">'.$result['log'].'</div>';
				}
			}
		}else
		{
			show_404();
		}
	}
	
	public function get_enrollment_total_fees($enrollment_id = false)
	{
			if($enrollment_id == false){
				
				return false;
			}
			
			$this->load->model(array('M_enrollments','M_student_finances','M_student_fees', 'M_student_previous_accounts', 'M_student_subjects', 'M_student_deductions','M_coursefees','M_assign_coursefees','M_studentpayments', 'M_additional_charges'));
			
#			Top header data of student
			$p = $this->M_enrollments->profile($enrollment_id);
			$this->view_data['enrollment_id'] = $enrollment_id;
			$this->view_data['is_paid'] = $p->is_paid;
			
			// Count Issues
			$this->issue_count($p->user_id);
			
#			Used for layouts/student_data
			$subjects =  $this->M_student_subjects->get_studentsubjects($enrollment_id);
			$subject_units = $this->M_student_subjects->get_studentsubjects_total($subjects);
		
			$this->view_data['student_profile'] = $this->view_data['student'] = $p;
			
			$student_finance =  $this->M_student_finances->get_student_finance($enrollment_id);
			
			$this->view_data['student_finance'] = $student_finance;
			
			if(!empty($student_finance))
			{
				// for total payment variables
				$totalprevious_account = 0.00;
				
				//Get course Finance
				$enrollee = $p;
				$data[] = $enrollee->year_id;
				$data[] = $enrollee->course_id;
				$data[] = $enrollee->sem_id;
				$data[] = $this->open_semester->academic_year_id;
				$assigned_course_fee = $this->M_assign_coursefees->get_assign_course_fee($data);
				if($assigned_course_fee){
					$studentfinance['coursefinance_id'] = $assigned_course_fee->coursefinance_id;
					$studentfinance['student_id']  = $enrollee->user_id;
					$studentfinance['enrollmentid']  = $enrollment_id;
					$studentfinance['year_id'] = $enrollee->year_id;
					$studentfinance['semester_id'] = $enrollee->sem_id;
					$studentfinance['course_id'] = $enrollee->course_id;
			
					$this->M_student_finances->add_student_finance($studentfinance);
				}
				
				$nstp_exist = 0;
				$lab_exist = 0;
				$lec_exist = 0;
				$bsba_units = 0;
				$lab1_units = 0;
				$lab2_units = 0;
		    
				if (isset($subjects))
				{
					foreach($subjects as $k => $s):
					  
						if(preg_match("/^nstp/", strtolower($s->code))){
							$nstp_exist++;
						}
						
						if($s->lab_kind == "BSBA"){
							$bsba_units += $s->lab;
						}
						
						if($s->lab_kind == "LAB1"){
							$lab1_units += $s->lab;
						}
						
						if($s->lab_kind == "LAB2"){
							$lab2_units += $s->lab;
						}
					endforeach;
				}
				
					
				$this->view_data["nstp"] = $nstp_exist;
				$student_total_units = $subject_units['total_units'];
				//End Student Subject Units
		
				// Loop through Coursefees
				//Temp-Variable
				$tuition_fee = 0.0;
				$lab_fee = 0.0;
				$rle_fee = 0.0;
				$misc_fee = 0.0;
				$other_fee = 0.0;
				$lab_a_fee = 0.0;
				$lab_b_fee = 0.0;
				$lab_c_fee = 0.0;
				$bsba_fee = 0.0;
				$lab1_fee = 0.0;
				$lab2_fee = 0.0;
			
				if($assigned_course_fee){
					$coursefees = $this->M_coursefees->get_all_course_fees($assigned_course_fee->coursefinance_id);
					
					if($coursefees)
					{
						foreach($coursefees as $cf):
							$studentfee['student_id'] = $enrollee->user_id;
							$studentfee['studentfinance_id'] = $student_finance->id;
				  
							if( strtolower($cf->name) == "tuition fee per unit" )
							{
								$studentfee['fee_id'] = $cf->fee_id;
								$tuition_fee += $cf->value;
							}
				  
							if( strtolower($cf->name) == "laboratory fee per unit" )
							{
								$studentfee['fee_id'] = $cf->fee_id;
								$lab_fee += $cf->value;
							}
							
							if (preg_match("/bsba laboratory/",$cf->name)){
							  $bsba_fee += $cf->value;
							}
							
							if (preg_match("/laboratory 1/",$cf->name)){
							  $lab1_fee += $cf->value;
							}
							
							if (preg_match("/laboratory 2/",$cf->name)){
							  $lab2_fee += $cf->value;
							}
							
							
				  
							if( $cf->is_misc == 1 )
							{
								$misc_fee += $cf->value;
							}
				  
							if( $cf->is_other == 1 )
							{
								$other_fee += $cf->value;
							}
				  
							$studentfee['fee_id'] = $cf->fee_id;
							$studentfee['value'] = $cf->value;
							$studentfee['position'] = $cf->position;
					
							#$this->M_student_fees->insert_student_fees($studentfee);
						endforeach;
					}
					
				}
			
			  $this->view_data['lab_a'] = $bsba_units;
			  $this->view_data['lab_b'] = $lab1_units;
			  $this->view_data['lab_c'] = $lab2_units;
			
			  $this->view_data['lab_a_fee'] = $bsba_fee;
			  $this->view_data['lab_b_fee'] = $lab1_fee;
			  $this->view_data['lab_c_fee'] = $lab2_fee;
			
			  $this->view_data['lab_a_fee_total'] = $lab1_total_fee = $bsba_units*198.00;
			  $this->view_data['lab_b_fee_total'] = $lab2_total_fee = $lab1_units*263.00;
			  $this->view_data['lab_c_fee_total'] = $lab3_total_fee = $lab2_units*318.00;
			  $total_custom_lab = $lab1_total_fee + $lab2_total_fee + $lab3_total_fee;
				
				
		
				# Computations for student total
				$student_total['enrollment_id'] = $enrollment_id;
				$student_total['total_units'] = $student_total_units;
				$student_total['total_lab_units'] = $subject_units['total_lab'];
				
				$student_total['tuition_fee_per_unit'] = $tuition_fee;
				$student_total['lab_fee_per_unit'] = $lab_fee;
				$student_total['total_misc_fee'] = $misc_fee;
				$student_total['total_other_fee'] = $other_fee;
				$student_total['additional_charge'] = $p->total_additional_charges;
				$student_total['previous_account'] = $totalprevious_account;
    
    
				$total_tuition_fee = $tuition_fee*$student_total_units;
				$total_misc = $misc_fee;
				$total_other = $other_fee;
				# End Computations
	  
				# Save Computation Totals
	  
				# Tuition Fee
				$student_total['tuition_fee'] = $total_tuition_fee;
				# RLE Fee
#				$student_total['total_rle'] = $total_rle;
				
				$total_stud_payment = $this->M_studentpayments->get_sum_student_payments($enrollment_id);
				$total_stud_dec = $this->M_student_deductions->get_sum_of_deductions($enrollment_id);
				if($total_stud_payment->account_recivable_student != '')
				{
					$student_total['total_payment'] = $total_stud_payment->account_recivable_student;
				}
				else
				{
					$student_total['total_payment'] = 0.00;
				}
				if($total_stud_dec->amount != '')
				{
					$student_total['total_deduction'] = $total_stud_dec->amount;
				}
				else
				{
					$student_total['total_deduction'] = 0.00;
				}
				
				$student_total["total_student_deduction"] = $total_stud_payment->account_recivable_student + $total_stud_dec->amount;
    
    
				# Total Charge
				$student_total['total_charge'] = $total_tuition_fee  + $total_misc + $total_other+$total_custom_lab;
				# Total Payable/ Amount
				$student_total['total_amount'] = $student_total['total_charge'] + $totalprevious_account;
				# Calculate Remaining Balance
				$student_total['remaining_balance'] = $student_total['total_amount'] - ($total_stud_dec->amount + $total_stud_payment->account_recivable_student);
				
				
				$this->view_data['student_total'] = $student_total;

				# End Save Computation Totals
				
				$this->view_data['sfid'] = $student_finance->id;		
			}
			
			
			$this->view_data['total_deductions'] = $this->M_student_deductions->sum_of_deductions($enrollment_id);
			
			// $student_fees = $this->M_student_fees->get_student_fees($student_finance->id);
			$this->view_data['previous'] =  $this->M_student_previous_accounts->get_student_previous_accounts($enrollment_id);
			
			$this->view_data['additional_charge'] =  $this->M_additional_charges->get_additional_charge($enrollment_id);
			
			/* get student subjects where student course_id,sem_id,year_id equal to subjects*/
			// $student_subjects = $this->M_student_subjects->get_student_subjects($enrollment_id,$p->course_id,$p->year_id,$p->sem_id);
			//$this->view_data['student_subjects'] =  $student_subjects;
			
			
			
			
			$this->view_data['course'] = $p->course_id;
			$this->view_data['year'] = $p->year_id;
			$this->view_data['semester'] = $p->sem_id;
			$this->view_data['enrollment_id'] = $p->id;
			
			//Get payment count for updating subject load
			// $this->view_data['payment_count'] =  $this->M_studentpayments->get_count_student_payments($enrollment_id);
			
			//Get payment count for updating subject load
			// $this->view_data['deduction_count'] =  $this->M_student_deductions->student_deductions_count($enrollment_id);
			
			
			return $this->view_data;
	}
	
	public function update_enrollment_status($enrollment_id = false){
		//UPDATES ENROLLMENTS is_paid, full_paid , partial_paid
		$this->load->model(array("M_subjects","M_student_total_file"));
		if(!$enrollment_id){ return false; }
		
		$total = $this->M_student_total_file->get_student_total_file($enrollment_id);
		
		if($total)
		{
			if(strtoupper($total->status) == "UNPAID")
			{
				$data['updated_at'] = NOW;
				$data['is_paid'] = 0;
				$data['full_paid'] = 0;
				$data['partial_paid'] = 0;
			}
			else if(strtoupper($total->status) == "PAID")
			{
				$data['updated_at'] = NOW;
				$data['is_paid'] = 1;
				$data['full_paid'] = 1;
				$data['partial_paid'] = 0;
			}
			else
			{
				$data['updated_at'] = NOW;
				$data['is_paid'] = 1;
				$data['full_paid'] = 0;
				$data['partial_paid'] = 1;
			}
			
			$result = $this->M_enrollments->update_enrollments($data, $enrollment_id);
		}
	}

	
	public function view_payment_record($enrollment_id = false)
	{
		if($enrollment_id !== false AND ctype_digit($enrollment_id))
		{
			$this->load->model(array('M_studentpayments','M_student_total_file'));
			$this->view_data['system_message'] = $this->session->flashdata('system_message');
			$this->view_data['payment_record'] = $this->M_studentpayments->get(false,array('id','remarks','created_at','account_recivable_student','or_no','old_account','other','total'),array('enrollmentid'=>$enrollment_id));
			$this->view_data['enrollment_id'] = $enrollment_id;
			$this->view_data['student_total'] = $this->M_student_total_file->get_student_total_file($enrollment_id);
		}else
		{
			show_404();
		}
	}
	
	public function show_payment($id = false,$eid = false)
	{
		if(($eid !== false AND ctype_digit($eid)) AND ($id !== false AND ctype_digit($id)))
		{
			$this->load->model('M_studentpayments');
			$this->view_data['payment_record'] = $this->M_studentpayments->get(false,array('id','remarks','created_at','account_recivable_student','or_no','group_or_no','old_account','other'),array('id'=>$id,'enrollmentid'=>$eid));
			$this->view_data['eid'] = $eid;
			$this->token->set_token();
			$this->view_data['form_token'] = $this->token->get_token();
		}else
		{
			if($this->input->post('delete_payment_record'))
			{
				$post_pid = $this->input->post('pid');
				$post_eid = $this->input->post('eid');
				$post_amt = $this->input->post('amount');
				$post_orno = $this->input->post('orno');
				
				$this->load->model('M_studentpayments');
				if($this->M_studentpayments->destroy_payment_record($post_pid,$post_eid))
				{
					$this->load->model('M_student_totals');
					$st = $this->M_student_totals->get_student_total($post_eid);
					$sp = $this->M_student_totals->get(false,array('total_payment'),array('enrollment_id'=>$post_eid));
					$rb = $st->remaining_balance + $post_amt;
					$tp = $sp[0]->total_payment  - $post_amt;
					if($this->M_student_totals->update_student_totals(array('remaining_balance'=>$rb,'total_payment'=>$tp),array('enrollment_id'=>$post_eid)))
					{
						log_message('info','destroy payment record by: '.$this->user.' OR# '.$post_orno.' Amount: '.$post_amt.' From student enrollment id: '.$post_eid);
						$this->session->set_flashdata('system_message','<div class="alert alert-success">Your Request was Succssfully completed</div>');
						$this->token->destroy_token();
						redirect('payment_record/view_payment_record/'.$post_eid);
						
					}else{
							log_message('error','payment_record/show_payments/update_student_totals Unable to update and add '.$rb.' to remaining_balance and subtract ' .$tp.' to totalpayments table,enrollment_id is:'.$post_eid);
							$this->session->set_flashdata('system_message','<div class="alert alert-danger">Payment record destroyed,but unaible to update student totals</div>');
							redirect('payment_record/show_payment/'.$post_pid.'/'.$post_eid);
					}
				}else{
					log_message('error','payment_record/show_payments/destroy_payment_record unable to delete payment record id no: '.$post_pid);
					$this->session->set_flashdata('system_message','<div class="alert alert-danger">Unable to destroy payment record, error processing request</div>');
					redirect('payment_record/show_payment/'.$post_pid.'/'.$post_eid);
				}
			}else
			{
				show_404();
			}
		}
	}
	
	public function delete_deduction($id, $eid)
	{
		$this->load->model(array('M_student_deductions'));
		
		$total = $this->M_student_deductions->get($id);
		
		if($total)
		{
			//REVERSE TOTAL AMOUNT TO STUDENT TOTAL BALANCE
			$this->M_student_total_file->reverse_student_total($eid, $total->amount,'DEDUCTION');
			
			//UPDATE ENROLLMENT IF PAID, FULL , PARTIAL AND UPDATE SUBJECT LOADS
			$this->M_enrollments->update_enrollments_status($eid); 
		}
		
		$result = $this->M_student_deductions->destroy_deducted_fee($id, $eid);
		
		if($result)
		{
			log_message('error','Deduction Deleted by: '.$this->user.'Success; Deduction Id: '.$id);
			$this->session->set_flashdata('system_message', '<div class="alert alert-success">Deducted successfully deleted.</div>');
			redirect('/fees/view_deducted_fees/'.$eid);
		}
	}
	
	
	public function destroy($id, $enrollment_id)
	{
		$this->load->model(array('M_studentpayments', 'M_student_subjects','M_student_total_file'));
		
		$total = $this->M_studentpayments->get($id);
		
		if($total)
		{
			//REVERSE TOTAL AMOUNT TO STUDENT TOTAL BALANCE
			$this->M_student_total_file->reverse_student_total($enrollment_id, $total->total);
			
			//UPDATE ENROLLMENT IF PAID, FULL , PARTIAL AND UPDATE SUBJECT LOADS
			$this->M_enrollments->update_enrollments_status($enrollment_id); 
		}
		
		$result = $this->M_studentpayments->destroy_payment_record($id, $enrollment_id);
		
		if($result)
		{
			log_message('error','Payment Deleted by: '.$this->user.'Success; Payment Id: '.$id.' Amount : '.$total->total);
			$this->session->set_flashdata('system_message', '<div class="alert alert-success">Payment successfully deleted.</div>');
			redirect('/payment_record/view_payment_record/'.$enrollment_id);
		}
	}
	
	public function student_payments($page=0)
	{
		$this->load->Model(array('M_enrollments','M_fees','M_studentpayments'));
	
		$this->load->library("pagination");
		$config = $this->pagination_style();
		$config["base_url"] = base_url() ."payment_record/student_payments";
		$this->view_data['total_rows'] = $config["total_rows"] = $this->M_enrollments->count_all_updaid_enrollments('paid');
		$config["per_page"] = 30;
		$config['num_links'] = 10;
		$config["uri_segment"] = 3;
		
		$filter[' AND is_paid = '] = 1; //ADD FILTER
		$this->pagination->initialize($config);
		$this->view_data['enrollments'] = $rs = $this->M_enrollments->get_all_updaid_enrollments($config["per_page"], $page, $filter);
		
		$data = false;
		$payments = false;
		
		if($rs){
			
			foreach($rs as $obj)
			{
				$payments[$obj->id] = $this->M_studentpayments->get_payments_enrollment_id($obj->id);
			}	
		}
		$this->view_data['payments'] = $payments;
		$this->view_data['links'] = $this->pagination->create_links();
	}
	
	public function remaining_balance_report($page = 0)
	{
		$this->load->Model(array('M_enrollments','M_fees','M_studentpayments'));
	
		$this->load->library("pagination");
		$config = $this->pagination_style();
		$config["base_url"] = base_url() ."payment_record/remaining_balance_report";
		$this->view_data['total_rows'] = $config["total_rows"] = $this->M_enrollments->count_all_updaid_enrollments('unpaid');
		$config["per_page"] = 30;
		$config['num_links'] = 10;
		$config["uri_segment"] = 3;
		
		$filter[' AND full_paid <> '] = 1; //ADD FILTER
		$this->pagination->initialize($config);
		$rs = $this->M_enrollments->get_all_updaid_enrollments($config["per_page"], $page, $filter);
		
		$data = false;
		
		if($rs){
			
			foreach($rs as $obj)
			{
				$data[] = $this->get_enrollment_total_fees($obj->id);
			}	
		}
		
		$this->view_data['enrollments'] = $data;
		$this->view_data['links'] = $this->pagination->create_links();
	}
	
	public function download_remaining_balance_report()
	{
		$this->load->Model(array('M_enrollments','M_fees','M_studentpayments'));
		
		$this->load->helper('my_dropdown');
		
		if($_POST)
		{
			$filter[' AND full_paid <> '] = 1; //ADD FILTER
			$year_id = $this->input->post('year_id');
			if($year_id != ""){
				$filter[' AND year_id = '] = $year_id;
			}
			$course_id = $this->input->post('course_id');
			if($course_id != ""){
				$filter[' AND course_id = '] = $course_id;
			}
			
			$rs = $this->M_enrollments->get_all_updaid_enrollments(0, 0, $filter, true);
			
			$data = false;
			
			if($rs)
			{
					$data = false;		
					
					foreach($rs as $obj)
					{
						$data[] = $this->get_enrollment_total_fees($obj->id);
					}	
					
					$this->load->library('../controllers/_the_downloadables');
					$this->_the_downloadables->_generate_remaining_balance_report($data);					
				
			}else{
				$this->session->set_flashdata('system_message', '<div class="alert alert-danger">No record found.</div>');
				redirect(current_url());
			}
		}
	}
}
