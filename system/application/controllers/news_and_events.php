<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class News_and_events extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->session_checker->open_semester();
		$this->session_checker->secure_page('all');
	}
	
	public function index()
	{
		$this->load->model(array('M_announcements','M_events'));
		$this->view_data['news'] = $news = $this->M_announcements->for_employee(12);
		$this->view_data['events'] = $events = $this->M_events->get_incoming_events();
	}
}