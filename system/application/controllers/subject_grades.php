<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Subject_grades extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->session_checker->open_semester();
		$this->session_checker->check_if_alive();
		$this->menu_access_checker();
		$this->load->helper(array('url_encrypt'));
		$this->load->helper(['my_dropdown','time']);
		$this->load->model(array(
			'M_subjects',
			'M_core_model'
		));
		
	}
	
	public function index($page = 0)
	{
		$this->view_data['system_message'] = $this->session->flashdata('system_message');

		//load models
		$this->load->model(['M_courses']);

		//FOR DROPDOWN
		$this->view_data['course_list'] = $cl = $this->M_courses->get_for_dd(['id','course_code'], false, 'All', 'course_code');
		
		$like = false;
		$filter = false;

		$filter['subjects.year_from'] = $this->cos->user->year_from;
		$filter['subjects.year_to'] = $this->cos->user->year_to;
		$filter['subjects.semester_id'] = $this->cos->user->semester_id;
		
		if($_GET)
		{	
			if(isset($_GET['code']) && trim($_GET['code'])){
				$this->view_data['code'] = $code = trim($_GET['code']);
				$like['master_subjects.code'] = $code;
			}

			if(isset($_GET['course_id']) && trim($_GET['course_id'])){
				$this->view_data['course_id'] = $course_id = trim($_GET['course_id']);
				$filter['subjects.course_id'] = $course_id;
			}

			if(isset($_GET['major']) && trim($_GET['major'])){
				$this->view_data['major'] = $major = trim($_GET['major']);
				$like['course_specialization.specialization'] = $major;
			}
			
			if(isset($_GET['subject']) && trim($_GET['subject'])){
				$this->view_data['subject'] = $subject = trim($_GET['subject']);
				$like['master_subjects.subject'] = $subject;
			}

			if(isset($_GET['semester_id']) && trim($_GET['semester_id'])){
				$this->view_data['semester_id'] = $semester_id = trim($_GET['semester_id']);
				$filter['subjects.semester_id'] = $semester_id;
			}

			if(isset($_GET['year_id']) && trim($_GET['year_id'])){
				$this->view_data['year_id'] = $year_id = trim($_GET['year_id']);
				$filter['subjects.year_id'] = $year_id;
			}

			if(isset($_GET['academic_year_id']) && trim($_GET['academic_year_id'])){
				$this->view_data['academic_year_id'] = $academic_year_id = trim($_GET['academic_year_id']);

				$this->load->model('M_academic_years');
				$sy = $this->M_academic_years->get($academic_year_id);
				$filter['subjects.year_from'] = $sy->year_from;
				$filter['subjects.year_to'] = $sy->year_to;
			}
		}
		else
		{
			$filter['subjects.year_from'] = $this->cos->user->year_from;
			$filter['subjects.year_to'] = $this->cos->user->year_to;
			$filter['subjects.semester_id'] = $this->cos->user->semester_id;
		}
		
		//CONFIGURATION
		$get['fields'] = array(
				'subjects.id',
				'master_subjects.code',
				'master_subjects.subject',
				'master_subjects.units',
				'master_subjects.lec',
				'master_subjects.lab',
				'subjects.time',
				'subjects.day',
				'subjects.room_id',
				'subjects.subject_load',
				'subjects.subject_taken',
				'subjects.original_load',
				'subjects.year_to',
				'subjects.year_from',
				'subjects.teacher_user_id',
				'rooms.name AS room',
				'users.name AS instructor' 
		);

		$get['fields'] = array(
				'subjects.id',
				'master_subjects.code',
				'master_subjects.subject',
				'master_subjects.units',
				'master_subjects.lec',
				'master_subjects.lab',
				'subjects.is_open_time',
				'subjects.time',
				'subjects.day',
				'subjects.room_id',
				'subjects.subject_load',
				'subjects.subject_taken',
				'subjects.original_load',
				'subjects.year_to',
				'subjects.year_from',
				'subjects.teacher_user_id',
				'rooms.name AS room',
				'courses.course_code',
				'courses.course as course_name',
				'course_specialization.specialization as major',
				'users.name AS instructor',
				'years.year' 
		);
		
		$get['where'] = $filter;
		$get['like'] = $like;
		
		$get['join'] = array(
			
			1 => array(
				"table" => "master_subjects",
				"on"	=> "master_subjects.ref_id = subjects.ref_id",
				"type"  => "LEFT"
			),
			2 => array(
				"table" => "rooms",
				"on"	=> "rooms.id = subjects.room_id",
				"type"  => "LEFT"
			),
			3 => array(
				"table" => "courses",
				"on"	=> "courses.id = subjects.course_id",
				"type"  => "LEFT"
			),
			4 => array(
				"table" => "users",
				"on"	=> "users.id = subjects.teacher_user_id",
				"type"  => "LEFT"
			),
			5 => array(
				"table" => "years",
				"on"	=> "years.id = subjects.year_id",
				"type"  => "LEFT"
			),
			6 => array(
				"table" => "course_specialization",
				"on"	=> "course_specialization.id = subjects.specialization_id",
				"type"  => "LEFT"
			)
		);
		$get['order'] = "master_subjects.subject";
		$get['all'] = true; //GET ALL EXCLUDE LIMIT
		$get['count'] = true; //RETURN COUNT NOT ROW
		$get['array'] = false; //RETURN OBJECT NOT ARRAY
		$get['single'] = false; //RETURN ALL NOT SINGLE
		
	
		$this->load->library("pagination");
		$config = $this->pagination_style();
		$config["base_url"] = base_url() ."subject_grades/index";
		$suffix = '?'.http_build_query($_GET, '', "&");
		$suffix = str_replace("&submit=Search", "", $suffix);
		$config['suffix'] = $suffix;
		$config['first_url'] = $config['base_url'].$config['suffix']; 
		
		$this->view_data['total_rows'] = $config["total_rows"] = $this->M_core_model->get_record("subjects", $get);
		
		$config["per_page"] = 30;
		$config['num_links'] = 10;
		$config["uri_segment"] = 3;
		// $config['use_page_numbers'] = TRUE;
		$this->pagination->initialize($config);
		
		//FOR PAGINATION
		$get['all'] = false;
		$get['count'] = false;
		
		$config['start'] = $page;
		$config['limit'] = $config['per_page'];
		
		$get['start'] = $page;
		$get['limit'] = $config['per_page'];
		
		$this->view_data['results'] = $search = $this->M_core_model->get_record("subjects", $get);
		$this->view_data['links'] = $this->pagination->create_links();
	}
	public function view_subject_grades($id = false)
	{
	
		if($id == false) { show_404(); }
		
		$this->load->library('pagination');
		$this->load->model('m_subjects','ms');
		$this->load->model('m_student_grades','mss');
		$this->load->model('M_grades_file','m_gf');
		$this->load->model('M_grading_periods','m_gp');
		
		$this->view_data['grading_periods'] = $this->m_gp->get_all(); 
		$this->view_data['subjects'] = $this->get_all_subjects(); 
		$this->view_data['id'] = $id; 
		#load subject library
		$this->load->library('_Subject', array('id'=>$id));
		$this->view_data['subject_profile'] = $this->_subject->profile;
		$this->view_data['grades'] = $p = $this->_subject->get_grades();
	}

	private function get_all_subjects(){

		$like = false;
		$filter = false;

		$filter['subjects.year_from'] = $this->cos->user->year_from;
		$filter['subjects.year_to'] = $this->cos->user->year_to;
		$filter['subjects.semester_id'] = $this->cos->user->semester_id;
		
		//CONFIGURATION
		$get['fields'] = array(
				'subjects.id',
				'master_subjects.code',
				'master_subjects.subject',
				'master_subjects.units',
				'master_subjects.lec',
				'master_subjects.lab',
				'subjects.time',
				'subjects.day',
				'subjects.room_id',
				'subjects.subject_load',
				'subjects.subject_taken',
				'subjects.original_load',
				'subjects.year_to',
				'subjects.year_from',
				'subjects.teacher_user_id',
				'rooms.name AS room',
				'users.name AS instructor' 
		);
		
		$get['where'] = $filter;
		$get['like'] = $like;
		
		$get['join'] = array(
			
			0 => array(
				"table" => "master_subjects",
				"on"	=> "master_subjects.ref_id = subjects.ref_id",
				"type"  => "LEFT"
			),
			1 => array(
				"table" => "rooms",
				"on"	=> "rooms.id = subjects.room_id",
				"type"  => "LEFT"
			),
			2 => array(
				"table" => "users",
				"on"	=> "users.id = subjects.teacher_user_id",
				"type"  => "LEFT"
			)
		);
		$get['order'] = "master_subjects.subject";
		$get['all'] = true; //GET ALL EXCLUDE LIMIT
		$get['array'] = false; //RETURN OBJECT NOT ARRAY
		$get['single'] = false; //RETURN ALL NOT SINGLE
		
		$result = array();

		if($rs = $this->M_core_model->get_record("subjects", $get)){ 
			foreach ($rs as $k => $v) {
				$result[$v->id] = $v->code .' | '. $v->subject;
			}
		}

		return $result;
	}
}

?>