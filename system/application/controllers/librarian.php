<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Librarian extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->session_checker->open_semester();
		$this->session_checker->secure_page(array('librarian'));
		$this->load->helper(array('url_encrypt'));
	}

	public function index(){
		
	}
	
}
?>
