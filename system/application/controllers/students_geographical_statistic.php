<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Students_geographical_statistic Extends MY_Controller
{
	public function __construct(){
		parent::__construct();
		$this->session_checker->open_semester();
		$this->load->model(array('M_enrollments'));
		$this->load->helper(array('url_encrypt'));
		$this->menu_access_checker();
		$this->load->model('M_core_model');
		$this->load->helper('my_dropdown');
	}
	
	public function by_municipality()
	{
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		
		$this->view_data['group_municipal'] = $group_municipal = $this->M_enrollments->get_enrollment_group_by_municipal();
		
		$data = array();
		
		if($group_municipal):
			foreach($group_municipal as $municipal):
				$data[$municipal->id]['municipal'] = $municipal->municipal;
				$data[$municipal->id]['data'] = $this->M_enrollments->get_enrollment_by_municipal($municipal->municipal);
			endforeach;
		endif;
		
		$this->view_data['result'] = $data;
	}
	
	public function index($page = 0)
	{	
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		
		$this->view_data['group_province'] = $group_province = $this->M_enrollments->get_enrollment_group_by_province();
		
		$this->view_data['custom_title'] = "By Province";
		
		$data = array();
		
		if($group_province):
			foreach($group_province as $province):
				$data[$province->id]['province'] = $province->province;
				$data[$province->id]['data'] = $this->M_enrollments->get_enrollment_by_province($province->province);
			endforeach;
		endif;
		
		$this->view_data['result'] = $data;
	}

	public function view_all($par1 = false, $par2 = false)
	{
		if($par1 == false) { show_404(); }
		if($par2 == false) { show_404(); }
		
		$this->view_data['custom_title'] = "STUDENT FROM ".strtoupper($par2);
		
		$this->view_data['result'] = false;
		
		if(strtolower($par1) == "province")
		{
			$this->view_data['result'] = $this->M_enrollments->get_enrollment_profile_by_province($par2);
		}
		
		if(strtolower($par1) == "municipal")
		{
			$this->view_data['result'] = $this->M_enrollments->get_enrollment_profile_by_municipal($par2);
		}
	}
	
	public function view_student_by_gender($par1 = false, $par2 = false, $sex = '')
	{
		if($par1 == false) { show_404(); }
		if($par2 == false) { show_404(); }
		
		$this->view_data['custom_title'] = strtoupper($sex)." FROM ".strtoupper($par2);
		
		$this->view_data['result'] = false;
		
		
		
		if(strtolower($par1) == "province")
		{
			$this->view_data['result'] = $this->M_enrollments->get_enrollment_profile_by_province_and_sex($par2,$sex);
		}
		
		if(strtolower($par1) == "municipal")
		{
			$this->view_data['result'] = $this->M_enrollments->get_enrollment_profile_by_municipal_and_sex($par2,$sex);
		}
	}
}
