<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Payment_type extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->session_checker->open_semester();
		$this->menu_access_checker();
		$this->load->model(array('M_payment_type'));
	}
	
	// Create
	public function create()
	{	
		$this->view_data['payment_type'] = FALSE;
		
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		
		if($_POST)
		{
			$data = $this->input->post('payment_type');
			
			$result = $this->M_payment_type->insert($data);
			
			if($result['status'])
			{
				$id = $result['id'];
				activity_log('create payment_type',$this->userlogin,'Created by: '.$this->userlogin.'Success;Payment Type Id : '.$id);
				
				log_message('error','Payment Type Created by: '.$this->user.'Success; Payment Type Id: '.$id);
				$this->session->set_flashdata('system_message', '<div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span>&nbsp; Payment Type successfully added.</div>');
				redirect(current_url());
			}
		}
	}
	
	// Retrieve
	public function index()
	{
		$this->view_data['payment_type'] = $this->M_payment_type->fetch_all();
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
	}
	
	public function display($id)
	{
		$this->load->model(array('M_payment_type'));
		
		$this->view_data['payment_type'] = $this->M_payment_type->get_years($id);
	}
	
	// Update
	public function edit($id =  false)
	{
		if(!$id) { show_404(); }
		
		$this->load->model(array('M_payment_type'));
		
		$this->view_data['payment_type'] = $this->M_payment_type->get($id);
		
		if($_POST)
		{
			$data = $this->input->post('payment_type');
			$data['updated_at'] = date('Y-m-d H:i:s');
			
			$result = $this->M_payment_type->update($id, $data);
		
			if($result)
			{
				activity_log('updated payment_type',$this->userlogin,'Updated by: '.$this->userlogin.'Success;Payment Type Id : '.$id);
				
				log_message('error','Payment Type Updated by: '.$this->user.'Success; Payment Type Id: '.$id);
				$this->session->set_flashdata('system_message', '<div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span>&nbsp; Payment Type successfully updated.</div>');
				redirect('payment_type');
			}
		}
	}
	
	public function destroy($id = false)
	{
		if(!$id){ show_404(); }
		$this->load->model(array('M_payment_type'));
		$p = $this->M_payment_type->get($id);
		if($p):
		$result = $this->M_payment_type->delete($id);
		activity_log('delete payment_type',$this->userlogin,'Deleted by: '.$this->userlogin.'Success;Payment Type Id : '.$id.' Type : '.$p->payment_type);
		log_message('error','Payment Type Deleted by: '.$this->user.'Success; Payment Type Id: '.$id);
		$this->session->set_flashdata('system_message', '<div class="alert alert-danger">Payment Type successfully deleted.</div>');
		redirect('payment_type');
		endif;
	}
}

?>