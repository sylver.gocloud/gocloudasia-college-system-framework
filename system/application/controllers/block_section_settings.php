<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Block_section_settings extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->menu_access_checker('block_section_settings');
		$this->load->helper(['my_dropdown','time']);
		$this->load->library(array('form_validation'));
	}

	public function index($page = 0){
		
		$this->load->model(array('M_academic_years'));

		//PAGINATION
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		
		$filter = false;
		
		$like = false;
		$order_by = false;
		
		$arr_filters = array();
		$suffix = "";
		
		if($_GET)
		{
			if(isset($_GET['submit']) && $_GET['submit'] == "Search")
			{
				$page = 0;
			}
			
			if(isset($_GET['name']) && trim($_GET['name']) != ''){
				$this->view_data['name'] = $name = trim($_GET['name']);
				$like['block_system_settings.name'] = $name;
				$arr_filters['name'] = $name;
			}

			if(isset($_GET['semester_id']) && trim($_GET['semester_id']) != ''){
				$this->view_data['semester_id'] = $semester_id = trim($_GET['semester_id']);
				$like['block_system_settings.semester_id'] = $semester_id;
				$arr_filters['semester_id'] = $semester_id;
			}

			if(isset($_GET['academic_year_id']) && trim($_GET['academic_year_id']) != ''){
				$this->view_data['academic_year_id'] = $academic_year_id = trim($_GET['academic_year_id']);
				$filter['block_system_settings.academic_year_id'] = $academic_year_id;
				$arr_filters['academic_year_id'] = $academic_year_id;
			}
		}
		
		$filter['block_system_settings.academic_year_id'] = $this->cos->user->academic_year_id;
		
		//CONFIGURATION
		$get['fields'] = array(
				'block_system_settings.*',
				'semesters.name as semester',
				'academic_years.year_from' ,
				'academic_years.year_to'
		);
		
		$get['where'] = $filter;
		$get['like'] = $like;
		
		$get['join'] = array(
			
			1 => array(
				"table" => "semesters",
				"on"	=> "semesters.id = block_system_settings.semester_id",
				"type"  => "LEFT"
			),
			2 => array(
				"table" => "academic_years",
				"on"	=> "academic_years.id = block_system_settings.academic_year_id",
				"type"  => "LEFT"
			)
		);
		$get['order'] = "`academic_years`.`year_from` DESC,`block_system_settings`.`name`";
		$get['all'] = true; //GET ALL EXCLUDE LIMIT
		$get['count'] = true; //RETURN COUNT NOT ROW
		$get['array'] = false; //RETURN OBJECT NOT ARRAY
		$get['single'] = false; //RETURN ALL NOT SINGLE
		
	
		$this->load->library("pagination");
		$config = $this->pagination_style();
		$config["base_url"] = base_url() ."search/search_student";
		$suffix = '?'.http_build_query($_GET, '', "&");
		$suffix = str_replace("&submit=Search", "", $suffix);
		$config['suffix'] = $suffix;
		$config['first_url'] = $config['base_url'].$config['suffix']; 
		
		$this->view_data['total_rows'] = $config["total_rows"] = $this->M_core_model->get_record("block_system_settings", $get);
		
		$config["per_page"] = 30;
		$config['num_links'] = 10;
		$config["uri_segment"] = 3;
		// $config['use_page_numbers'] = TRUE;
		$this->pagination->initialize($config);
		
		//FOR PAGINATION
		$get['all'] = false;
		$get['count'] = false;
		
		$config['start'] = $page;
		$config['limit'] = $config['per_page'];
		
		$get['start'] = $page;
		$get['limit'] = $config['per_page'];
		
		$this->view_data['search'] = $search = $this->M_core_model->get_record("block_system_settings", $get);
		$this->view_data['links'] = $this->pagination->create_links();
		$this->view_data['sy'] = $this->M_academic_years->get_for_dd(array('id','year_from','year_to'), false);
	}
	
	// Create
	public function create()
	{
		$this->load->model(array('M_block_section_settings','M_subjects','M_block_subjects'));
		
		$this->view_data['Block_section_settings'] = FALSE;
		$this->view_data['subject_list'] = $sl = $this->M_subjects->available_schedules();
		$this->view_data['subject_master'] = $s = $this->sort_data_alpha($sl, 'code');
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
	
		if($_POST)
		{	
			
			$data = $this->input->post('block_section_settings');
			if(trim($data['name']) != ""){
				$data['created_at'] = date('Y-m-d H:i:s');
				$data['updated_at'] = date('Y-m-d H:i:s');
				$data['academic_year_id'] = $this->open_semester->academic_year_id;
				$data['semester_id'] = $this->input->post('semester_id');
				
				$result = $this->M_block_section_settings->create_block_section_settings($data);
				
				if($result['status'])
				{
					$id = $result['id'];
					activity_log('create Block section settings',$this->userlogin,'Block_section_settings Created by: '.$this->user.'Success; Block_section_settings Id: '.$id);
					log_message('error','Block_section_settings Created by: '.$this->user.'Success; Block_section_settings Id: '.$id);
					$this->session->set_flashdata('system_message', '<div class="alert alert-success">Block Section Settings successfully added.</div>');
						
					if($this->input->post('subject')){
						foreach($this->input->post('subject') as $subject_id){
							unset($data);
							$data['subject_id'] = $subject_id;
							$data['block_system_setting_id'] = $id;
							$data['created_at'] = date('Y-m-d H:i:s');
							$data['updated_at'] = date('Y-m-d H:i:s');
							$res = $this->M_block_subjects->create_block_subjects($data);
						}
					}

					if($this->input->post('save_block')){
						$this->_msg('s','Block section was <strong>successfully</strong> created','block_section_settings/edit/'.__link($result['id']));
					}else{
						$this->_msg('s','Block section was <strong>successfully</strong> created, continue to create schedule here.','block_section_settings/create_schedule/'.__link($result['id']));
					}
				}
			}else{
				$this->session->set_flashdata('system_message', '<div class="alert alert-danger">Name should not be blank.</div>');
			}
		}
	}

	public function create_schedule($hashid)
	{
		$id = $this->check_hash($hashid);
		$this->load->model(array('M_block_section_settings','M_course_blocks','M_block_subjects','M_subjects'));
		$this->load->model(array(
			'M_subjects',
			'M_years',
			'M_courses',
			'M_semesters',
			'M_academic_years',
			'M_coursefinances',
			'M_fees',
			'M_rooms',
			'M_users2',
			'M_master_subjects'
		));
		
		$this->view_data['block_section_settings'] = $b = $this->M_block_section_settings->get_block_section_settings($id);
		$this->view_data['course_blocks'] = $cb = $this->M_course_blocks->get_course_blocks($id);
		$this->view_data['block_subjects'] = $bs = $this->M_block_subjects->get_block_subjects($id);
		$this->view_data['hash'] = $hashid;
		$this->view_data['custom_title'] = "Create Schedule for $b->name";
		
		//Data needed to subject form
		$this->view_data['cos'] = $this->cos->user; // current open semester
		
		//FOR DROPDOWN
		$get = array('is_deleted'=>0);
		$this->view_data['subject_list'] = $this->M_master_subjects->get_for_dd(['ref_id','code','subject'],$get,'Choose Subject','code'); //available master subjects
		$this->view_data['years'] = $this->M_years->find_all();
		$this->view_data['courses'] = $this->M_courses->find_all();
		$this->view_data['semesters'] = $this->M_semesters->find_all();
		$this->view_data['academic_years'] = $this->M_academic_years->find_all();
		$this->view_data['lab_fees'] = $this->M_fees->get_lab_fees();
		$this->view_data['nstp_fees'] = $this->M_fees->get_nstp_fees();

		$room_fld = array(
				'id',
				'name',
				'description'
			);
		$room_where = array(
				'academic_year_id' => $this->cos->user->academic_year_id
			);
		$this->view_data['rooms'] = $r = $this->M_rooms->get_for_dd($room_fld, $room_where,'Select Room');
		$this->view_data['teachers'] = $t = $this->M_users2->get_for_dd(array('id','name'), array('department'=>'teacher'), 'Choose Instructor');

		if($this->input->post()){
			if($this->form_validation->run('subjects') !== FALSE){
				
				$data = $this->input->post('subjects');

				//Get Master Subject
				$ms_subject = $this->M_master_subjects->pull(array('ref_id'=>$data['ref_id']));

				if($ms_subject){

					// $data['created_at'] = date('Y-m-d H:i:s');
					// $data['updated_at'] = date('Y-m-d H:i:s');
					// $data['subject_load'] = $data['original_load'];
					
					// if(isset($_POST['is_nstp']))
					// {
					// 	$data['is_nstp'] = 1;
					// }
					
					// $year = $this->M_academic_years->get_academic_years($this->input->post('academic_year_id'));
					
					// if($year){
					// 	$data['year_from'] = $year->year_from;
					// 	$data['year_to'] = $year->year_to;
					// }

					// $data['class_start'] = str_pad($this->input->post('class_start_hr'), 2, "0", STR_PAD_LEFT).':'.str_pad($this->input->post('class_start_min'),2,'0',STR_PAD_LEFT);
					// $data['class_end'] = str_pad($this->input->post('class_end_hr'),2,'0',STR_PAD_LEFT).':'.str_pad($this->input->post('class_end_min'),2,'0',STR_PAD_LEFT);
					
					// $result = $this->M_subjects->create_subjects($data); //insert schedule
					$result  = $this->M_subjects->create_subject_from_post($this->input->post());
					
					if($result['status'])
					{
						$subject_id = $result['id'];

						// ---- Add the Schedule to the block
						unset($save);
						$save['subject_id'] = $subject_id;
						$save['block_system_setting_id'] = $id;
						$rs = (object)$this->M_block_subjects->insert($save);

						if($rs->status){
							activity_log('Add Schedule to Block',false,"block_section_settings Table Id:$id Data : ".arr_str($save));

							$this->_msg('s','The schedule was successfully created and added to this block.', current_url());
						}else{

							$this->_msg('e','The schedule was successfully created but fail to be added in this block section. Please try again, you can add them manually in the edit mode.', current_url());
						}
					}else{
						
						$this->view_data['system_message'] = "<div class='alert alert-danger'>Something went wrong, transaction not saved. Please try again.</div>";
					}
				}else{
					$this->_msg('e','Subject selected was invalid please try again.', current_url());
				}	
			}
		}
	}
	
	public function course_blocks($hashid = false){
		
		$id = $this->check_hash($hashid);
		$this->load->model(array('M_block_section_settings','M_years','M_courses','M_course_blocks'));
		
		$this->view_data['years'] = $this->M_years->find_all();
		$this->view_data['courses'] = $this->M_courses->get_all_courses($id);
		$this->view_data['block_system_settings'] = $this->M_block_section_settings->get_block_section_settings($id);
		$this->view_data['hash'] = $hashid;

		if($_POST){

			$data = $this->input->post('courses');
			$data['created_at'] = date('Y-m-d H:i:s');
			$data['updated_at'] = date('Y-m-d H:i:s');
			if($this->input->post('specialization_id')){
				$data['specialization_id'] = $this->input->post('specialization_id');
			}

			//CHECK IF ALREADY ADDED
			unset($get);
			$get['where']['block_system_setting_id'] = $id;
			$get['where']['year_id'] = $data['year_id'];
			$get['where']['course_id'] = $data['course_id'];
			if($this->input->post('specialization_id')){
				$get['where']['specialization_id'] = $this->input->post('specialization_id');
			}
			$get['single'] = true;
			$q_res = $this->M_course_blocks->get_record(false,$get);

			if($q_res == false)
			{

				$result = $this->M_course_blocks->create_course_blocks($data);
				
				if($result['status'])
				{
					$id = $result['id'];
					
					activity_log('assign course blocks',$this->user,'Assign course blocks Set by: '.$this->user.'Success; Assign course blocks Id: '.$id);
					
					log_message('error','Assign course blocks Set by: '.$this->user.'Success; Assign course blocks Id: '.$id);
					$this->session->set_flashdata('system_message', '<div class="alert alert-success">Successfully assign course.</div>');
					redirect('block_section_settings/edit/'.$hashid);
				}
			}else
			{
				$this->session->set_flashdata('system_message', '<div class="alert alert-danger"><span class="glyphicon glyphicon-remove"></span>&nbsp; Course and year already added.</div>');
				redirect(current_url());
			}
		}
	}
	
	public function display($id)
	{
		$this->load->model(array('M_block_section_settings','M_course_blocks','M_block_subjects'));
		
		$this->view_data['block_section_settings'] = $this->M_block_section_settings->get_block_section_settings($id);
		
		$this->view_data['course_blocks'] = $this->M_course_blocks->get_course_blocks($id);
		
		$this->view_data['block_subjects'] = $this->M_block_subjects->get_block_subjects($id);
		
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		
		// var_dump($this->view_data['block_subjects']);
	}
	
	// Update
	public function edit($hashid = false)
	{
		$id = $this->check_hash($hashid);

		$this->load->model(array('M_block_section_settings','M_course_blocks','M_block_subjects','M_subjects'));
		
		$this->view_data['block_section_settings'] = $b = $this->M_block_section_settings->get_block_section_settings($id);
		$this->view_data['course_blocks'] = $cb = $this->M_course_blocks->get_course_blocks($id);
		$this->view_data['block_subjects'] = $bs = $this->M_block_subjects->get_block_subjects($id);
		$this->view_data['hash'] = $hashid;

		//get exclude subjects
		$ex = false;
		if($bs){
			foreach ($bs as $k => $v) { $ex[] = $v->subject_id; }
				$ex = implode(',', $ex);
		}
		
		//schedules
		$this->view_data['subject_list'] = $sl = $this->M_subjects->available_schedules($ex);
		$this->view_data['subject_master'] = $s = $this->sort_data_alpha($sl, 'code');
		
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		
		if($_POST)
		{
			if($id)
			{
				$data = $this->input->post('block_section_settings');
				if(trim($data['name']) != ""){
				
					$data['updated_at'] = date('Y-m-d H:i:s');
					$data['semester_id'] = $this->input->post('semester_id');
					
					$result = $this->M_block_section_settings->update_block_section_settings($data, $id);
					
					if(count($this->input->post('subject')) > 0){
						foreach($this->input->post('subject') as $subject_id){
							unset($data);
							$data['subject_id'] = $subject_id;
							$data['block_system_setting_id'] = $id;
							$data['created_at'] = date('Y-m-d H:i:s');
							$data['updated_at'] = date('Y-m-d H:i:s');
							$res = $this->M_block_subjects->create_block_subjects($data);
						}
						
						activity_log('updated Block section settings',$this->userlogin,'Block_section_settings Updated by: '.$this->user.'Success; Block_section_settings Id: '.$id);
						
						log_message('error','Block_section_settings Updated by: '.$this->user.'Success; Block_section_settings Id: '.$id);
						$this->session->set_flashdata('system_message', '<div class="alert alert-success">Block Section Settings successfully updated.</div>');
						
						redirect('block_section_settings/edit/'.$hashid);
					}
				}else{
					$this->session->set_flashdata('system_message', '<div class="alert alert-danger">Name should not be blank.</div>');
				}
			}
		}
	}
	
	// Delete
	public function destroy($hashid = false)
	{
		$id = $this->check_hash($hashid);
		
		$this->load->model(array('M_block_section_settings','M_block_subjects','M_course_blocks'));
		
		//DELETE ASSIGN SUBJECT FIRST
		$result = $this->M_block_subjects->delete_all_block_subjects_per_block_section($id);
		//DELETE ASSIGN COURSE
		$result = $this->M_course_blocks->delete_all_course_blocks_per_block_section($id);
		//DELETE THE BLOCK SECTION
		$result = $this->M_block_section_settings->delete_open_semesters($id);
		
		activity_log('delete Block section settings',$this->userlogin,'Block section Deleted by: '.$this->user.'Success; Block Section Id: '.$id);
		
		log_message('error','Block section Deleted by: '.$this->user.'Success; Block Section Id: '.$id);
		$this->session->set_flashdata('system_message', '<div class="alert alert-success">Block Section settings successfully deleted.</div>');
		$this->_msg('s','Block Section settings successfully deleted.','block_section_settings');
	}
	
	public function destroy_block_subject($block_section_setting_hashid = false, $backurl = false){

		$hashid = $this->uri->segment(5);
		$block_section_setting_id = $this->check_hash($block_section_setting_hashid);

		$id = $this->check_hash($hashid);
		if($id && $block_section_setting_id){
		
			$this->load->model(array('M_block_subjects'));
			
			$result = $this->M_block_subjects->delete_block_subjects($id);
			
			activity_log('destroy block subject',$this->userlogin,'Block Subject Deleted by: '.$this->user.'Success; Block Subject Id: '.$id);
			
			log_message('error','Block Subject Deleted by: '.$this->user.'Success; Block Subject Id: '.$id);
			$this->session->set_flashdata('system_message', '<div class="alert alert-success">Block Subject successfully deleted.</div>');

			if($backurl){
				redirect('block_section_settings/create_schedule/'.$block_section_setting_hashid);
			}else{
				redirect('block_section_settings/edit/'.$block_section_setting_hashid);
			}
		}
		else{
			redirect('block_section_settings');
		}
	}
	
	public function destroy_course_blocks($block_section_setting_hashid = false){
		$hashid = $this->uri->segment(5);

		$block_section_setting_id = $this->check_hash($block_section_setting_hashid);
		$id = $this->check_hash($hashid);

		if($id && $block_section_setting_id){
		
			$this->load->model(array('M_course_blocks'));
			
			$result = $this->M_course_blocks->delete_course_blocks($id);
			
			activity_log('destroy Coure Blocks',$this->userlogin,'Coure Blocks Deleted by: '.$this->user.'Success; Course Block Id: '.$id);
			
			log_message('error','Coure Blocks Deleted by: '.$this->user.'Success; Course Block Id: '.$id);
			$this->session->set_flashdata('system_message', '<div class="alert alert-success">Course Block successfully deleted.</div>');
			redirect('block_section_settings/edit/'.$block_section_setting_hashid);
		}
		else{
			redirect('block_section_settings');
		}
	}
}