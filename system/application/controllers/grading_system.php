<?php
	
class Grading_system Extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_grading_system','m_gs');
	}

	public function setup()
	{
		$this->menu_access_checker('grading_system/setup');

		$this->token->set_token();
		$this->view_data['form_token'] = $this->token->get_token();
		$this->view_data['type'] = $type = $this->m_gs->grading_system_type();
		$this->view_data['grading_system'] = $gs = $this->m_gs->get_system();

		if($this->input->post()){
			$this->load->library(array('form_validation'));

			if(!$this->token->validate_token($this->input->post('token'))){ show_error('Form Token Invalid'); }

			if($this->input->post('create')){

				if($this->form_validation->run('grading_system') !== FALSE){
					
					$rs = $this->m_gs->create_from_post($this->input->post());

					if($rs->code == "s"){
						activity_log('Create Grading System',false,' grading_system table id : '.$rs->id.' data:'.arr_str($_POST['grading_system']));
					}

					$this->_msg($rs->code, $rs->msg,current_url());
				}
			}

			if($this->input->post('update')){
				$rs = $this->m_gs->update_from_post($this->input->post());
				$this->_msg($rs->code, $rs->msg,current_url());
			}			
		}
	}

	public function delete($hash='')
	{
		$this->menu_access_checker('grading_system/setup');
		$id = $this->check_hash($hash);
		$this->m_gs->check_id($id);
		$rs = $this->m_gs->delete_record($id);
		$this->_msg($rs->code, $rs->msg, 'grading_system/setup');
	}
}
