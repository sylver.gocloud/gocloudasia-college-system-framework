<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Payment_plan extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->session_checker->open_semester();
		
		$this->menu_access_checker();
		$this->load->helper(array('money'));
		$this->load->model(array(
			'M_payment_plan',
			'M_grading_periods',
			'M_payment_plan_req'));
	}
	
		// Retrieve
	public function index()
	{
		$this->view_data['payment_plan'] = $this->M_payment_plan->find_all();
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
	}
	
	// Create
	public function create()
	{
		
		$this->view_data['payment_plan'] = FALSE;
		
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		
		$this->view_data['grading_periods'] = $this->M_grading_periods->find_all();
		
		if($_POST)
		{
			$data = $this->input->post('payment_plan');
			$data['created_at'] = date('Y-m-d H:i:s');
			$data['updated_at'] = date('Y-m-d H:i:s');
			
			$result = $this->M_payment_plan->create_payment_plan($data);
			
			if($result['status'])
			{
				$id = $result['id'];
				activity_log('create payment plan',$this->userlogin,'Created by: '.$this->userlogin.'Success;Payment plan Id : '.$id);
				log_message('error','Payment Plan Created by: '.$this->user.'Success; Payment Plan Id: '.$id);


				//CHECK IF PAYMENT DIVISION IS 1 OR GREATER
				//IF 1 (FULLTIME) SAVE TO payment_plan_req
				//IF 1+ REDIRECT TO add_payment_plan_cut_off_period
				$added = $this->M_payment_plan->get($id);
				if($added)
				{
					if($added->division == 1)
					{
						unset($data);
						$data['payment_plan_id'] = $id;
						$data['number'] = 1;
						$data['grading_period_id'] = 'FULLTIME';
						$rs = $this->M_payment_plan_req->insert($data);

						if($rs['status']){
							$this->session->set_flashdata('system_message', '<div class="alert alert-success"><span class = "glyphicon glyphicon-ok"></span>&nbsp; Payment Plan successfully added.</div>');
							redirect(current_url());
						}else{
							$this->session->set_flashdata('system_message', '<div class="alert alert-danger"><span class = "glyphicon glyphicon-ok"></span>&nbsp; Something went wrong. Please try again.</div>');
							redirect(current_url());
						}
					}else{
						$this->session->set_flashdata('system_message', '<div class="alert alert-success"><span class = "glyphicon glyphicon-ok"></span>&nbsp; Payment Plan successfully added.Please set cut off grading period for this plan.</div>');
						redirect('payment_plan/add_payment_plan_cut_off_period/'.$id);
					}
				}
			}
		}
	}

	// Update
	public function edit($id = false)
	{
		if(!$id) { show_404(); }
		
		$this->view_data['edit'] = true;

		$this->view_data['grading_periods'] = $this->M_grading_periods->find_all();
		
		$this->view_data['payment_plan'] = $this->M_payment_plan->get_payment_plan($id);
		
		if($_POST)
		{
			$data = $this->input->post('payment_plan');
			$data['updated_at'] = date('Y-m-d H:i:s');
			
			$result = $this->M_payment_plan->update_payment_plan($data, $id);
			
			if($result['status'])
			{
				activity_log('edit payment plan',$this->userlogin,'Updated by: '.$this->userlogin.'Success;Payment plan Id : '.$id);
				log_message('error','Payment Plan Updated by: '.$this->user.'Success; Payment Plan Id: '.$id);
				$this->session->set_flashdata('system_message', '<div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span>&nbsp; Payment Plan successfully updated. Please set cut off grading period for this plan.</div>');
				redirect(current_url());
			}
		}
	}
	
	public function destroy($id = false)
	{
		if(!$id) { show_404(); }
		$p = $this->M_payment_plan->get_payment_plan($id);
		if($p):
			$result = $this->M_payment_plan->delete_payment_plan($id);
			$result = $this->M_payment_plan_req->delete_record_by_plan_id($id);

			activity_log('delete payment plan',$this->userlogin,'deleted by: '.$this->userlogin.'Success;Payment plan Id : '.$id." Name : ".$p->name);

			log_message('error','Payment Plan Deleted by: '.$this->user.'Success; Payment Plan Id: '.$id);
			$this->session->set_flashdata('system_message', '<div class="alert alert-success"><span class = "glyphicon glyphicon-ok"></span>&nbsp; Payment Plan successfully deleted.</div>');
			redirect('payment_plan');
		endif;
	}

	public function edit_payment_plan_cut_off_period($id = false, $par = false){

		if(!$id) { show_404(); }	

		$this->view_data['grading_periods'] = $grading_periods = $this->M_grading_periods->find_all();
		
		$this->view_data['payment_plan'] = $payment_plan = $this->M_payment_plan->get_payment_plan($id);
		
		$this->view_data['grading_period_req'] = $grading_period_req = $this->M_payment_plan_req->get_record_by_plan_id($id);

		$this->view_data['clear'] =  $par;

		if($_POST && $_POST['submit'] == 'Save Changes'){
			$division = trim($_POST['division']);
			if($division != "" && $division == $payment_plan->division ){

				$cut_off = $this->input->post('cut_off');
				$key_id = $this->input->post('cut_off_id');

				if($division == 1)
				{

				}
				else{

					if(isset($cut_off['DOWNPAYMENT'])){

						unset($data);
						$data['payment_plan_id'] = $id;
						$data['number'] = 1;
						$data['grading_period_id'] = 'DOWNPAYMENT';
						$rs = $this->M_payment_plan_req->insert($data);
						if($rs['status'])
						{
							activity_log('Add Payment Plan cut off period (DOWNPAYMENT)', $this->userlogin,'Payment Plan ID:'.$id.' Payment Plan Req ID:'.$rs['id']);
						}
					}

					foreach ($grading_periods as $key => $obj) {
						unset($data);
						$data['payment_plan_id'] = $id;
						$data['number'] = $cut_off[$obj->id] == "" ? "PAID" : $cut_off[$obj->id];
						$data['grading_period_id'] = $obj->id;

						if($key_id[$obj->id])
						{
							$payment_plan_req_id = $key_id[$obj->id];
							$rs = $this->M_payment_plan_req->update($payment_plan_req_id, $data);
							if($rs['status'])
							{
								activity_log('Update Payment Plan cut off period', $this->userlogin,'Payment Plan ID:'.$id.' Payment Plan Req ID:'.$payment_plan_req_id);
							}
						}
					}
				}

				$this->session->set_flashdata('system_message', '<div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span>&nbsp; Payment Plan Cut Off Period successfully updated.</div>');
				redirect('payment_plan');
				
			}	
			else{
				$this->session->set_flashdata('system_message', '<div class="alert alert-danger">Record failed to process. Please try again</div>');
				redirect(current_url());
			}
		}
	}
	
	public function add_payment_plan_cut_off_period($id = false)
	{
		if(!$id) { show_404(); }
		
		$this->view_data['grading_periods'] = $grading_periods = $this->M_grading_periods->find_all();
		
		$this->view_data['payment_plan'] = $payment_plan = $this->M_payment_plan->get_payment_plan($id);

		if($_POST && $_POST['submit'] == 'Save Changes'){
			$division = trim($_POST['division']);
			if($division != "" && $division == $payment_plan->division ){

				$cut_off = $this->input->post('cut_off');

				if($division == 1)
				{
					
				}
				else{
					if(isset($cut_off['DOWNPAYMENT'])){

						unset($data);
						$data['payment_plan_id'] = $id;
						$data['number'] = 1;
						$data['grading_period_id'] = 'DOWNPAYMENT';
						$rs = $this->M_payment_plan_req->insert($data);
						if($rs['status'])
						{
							activity_log('Add Payment Plan cut off period (DOWNPAYMENT)', $this->userlogin,'Payment Plan ID:'.$id.' Payment Plan Req ID:'.$rs['id']);
						}
					}	
					foreach ($grading_periods as $key => $obj) {
						unset($data);
						$data['payment_plan_id'] = $id;
						$data['number'] = $cut_off[$obj->id] == "" ? "PAID" : $cut_off[$obj->id];
						$data['grading_period_id'] = $obj->id;
						$rs = $this->M_payment_plan_req->insert($data);
						if($rs['status'])
						{
							activity_log('Add Payment Plan cut off period', $this->userlogin,'Payment Plan ID:'.$id.' Payment Plan Req ID:'.$rs['id']);
						}
					}
				}

				$this->session->set_flashdata('system_message', '<div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span>&nbsp; Payment Plan Cut Off Period successfully added.</div>');
				redirect('payment_plan');
				
			}	
			else{
				$this->session->set_flashdata('system_message', '<div class="alert alert-danger">Record failed to process. Please try again</div>');
				redirect(current_url());
			}
		}
	}
}

?>