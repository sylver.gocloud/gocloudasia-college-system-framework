<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Enrollees extends MY_Controller {

	public function __construct(){
		parent::__construct();
		$this->session_checker->open_semester();
		//$this->session_checker->secure_page(array('dean','president','registrar','school_administrator'));
		
		$this->menu_access_checker(array(
			'enrollees/daily_enrollees'
		));
		
		$this->load->model(array('M_enrollments','M_core_model'));
		$this->load->helper(array('url_encrypt','my_dropdown'));
	}
	
	public function index()
	{
		show_404();
	}
		
	public function daily_enrollees($page = 0)
	{
		//PAGINATION
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		
		$filter = false;
		$like = false;
		$order_by = false;
		
		$filter['enrollments.semester_id'] = $this->open_semester->id;
		$filter['enrollments.sy_from'] = $this->open_semester->year_from;
		$filter['enrollments.sy_to'] = $this->open_semester->year_to;
		$filter['enrollments.is_deleted'] = 0;
		$filter['enrollments.is_paid'] = 0;
		
		
		$arr_filters = array();
		$suffix = "";
		
		if($_GET)
		{
			if(isset($_GET['name']) && trim($_GET['name']) != ''){
				$this->view_data['name'] = $name = trim($_GET['name']);
				$like['enrollments.name'] = $name;
				$arr_filters['name'] = $name;
			}
			
			if(isset($_GET['date']) && trim($_GET['date']) != ''){
				$this->view_data['created_at'] = $created_at = trim($_GET['date']);
				$like['DATE(enrollments.created_at)'] = $created_at;
				$arr_filters['created_at'] = $created_at;
				
			}
			
			if(isset($_GET['studid']) && trim($_GET['studid']) != ''){
				$this->view_data['studid'] = $studid = trim($_GET['studid']);
				$like['enrollments.studid'] = $studid;
				$arr_filters['studid'] = $studid;
			}
			
			if(isset($_GET['year_id']) && trim($_GET['year_id']) != ''){
				$this->view_data['year_id'] = $year_id = trim($_GET['year_id']);
				$filter['enrollments.year_id'] = $year_id;
				$arr_filters['year_id'] = $year_id;
			}
			
			$suffix = array_to_geturl($arr_filters);
		}
		else
		{
			// $like['DATE(enrollments.created_at)'] = date('Y-m-d');
		}
		
		//CONFIGURATION
		$get['fields'] = array(
				'enrollments.id',
				'enrollments.studid',
				'enrollments.user_id',
				'enrollments.name' ,
				'years.year',
				'courses.course' 
		);
		
		$get['where'] = $filter;
		$get['like'] = $like;
		
		$get['join'] = array(
			
			1 => array(
				"table" => "courses",
				"on"	=> "courses.id = enrollments.course_id",
				"type"  => "LEFT"
			),
			2 => array(
				"table" => "years",
				"on"	=> "years.id = enrollments.year_id",
				"type"  => "LEFT"
			)
		);
		$get['order'] = "enrollments.name";
		$get['all'] = true; //GET ALL EXCLUDE LIMIT
		$get['count'] = true; //RETURN COUNT NOT ROW
		$get['array'] = false; //RETURN OBJECT NOT ARRAY
		$get['single'] = false; //RETURN ALL NOT SINGLE
		
	
		$this->load->library("pagination");
		$config = $this->pagination_style();
		$config["base_url"] = base_url() ."enrollees/daily_enrollees";
		$config['suffix'] = '?'.http_build_query($_GET, '', "&");
		$config['first_url'] = $config['base_url'].$config['suffix']; 
		
		$this->view_data['total_rows'] = $config["total_rows"] = $this->M_core_model->get_record("enrollments", $get);
		
		$config["per_page"] = 25;
		$config['num_links'] = 10;
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		
		//FOR PAGINATION
		$get['all'] = false;
		$get['count'] = false;
		if($this->input->post('submit') == "Print")
		{
			$get['all'] = true;
		}
		$config['start'] = $page;
		$config['limit'] = $config['per_page'];
		
		$get['start'] = $page;
		$get['limit'] = $config['per_page'];
		
		$this->view_data['search'] = $search = $this->M_core_model->get_record("enrollments", $get);
		$this->view_data['links'] = $this->pagination->create_links();
			
		
		/* $this->load->model(array('M_enrollments'));
		
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		
		if($_POST){
			$date = $this->input->post('date');
			$this->view_data['date'] = $date;
		}else{
			$date = date('Y-m-d');
		}
		
		$this->view_data['enrollees'] = $this->M_enrollments->get_enrollee_bydate($this->open_semester->id, $this->open_semester->year_from, $this->open_semester->year_to, $date); */
	}	
	
	public function nstp_students($page = 0)
	{
		$this->load->model('M_core_model');
		$this->load->helper('my_dropdown');
		
		//PAGINATION
		
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		
		$filter = false;
		$like = false;
		$order_by = false;
		$group_by = 'studentsubjects.enrollmentid';
		
		$filter['enrollments.semester_id'] = $this->open_semester->id;
		$filter['enrollments.sy_from'] = $this->open_semester->year_from;
		$filter['enrollments.sy_to'] = $this->open_semester->year_to;
		$filter['enrollments.is_deleted'] = 0;
		$filter['enrollments.is_paid'] = 1;
		$filter['subjects.is_nstp'] = 1;
		
		if($_GET)
		{
			$this->view_data['lastname'] = $lastname = trim($this->input->get('lastname'));
			$this->view_data['fname'] = $fname = trim($this->input->get('fname'));
			$this->view_data['studid'] = $studid = trim($this->input->get('studid'));
			$this->view_data['year_id'] = $year_id = trim($this->input->get('year_id'));
			
			if($lastname != ""){
				$like['enrollments.lastname'] = $lastname;
			}
			if($fname != ""){
				$like['enrollments.fname'] = $fname;
			}
			if($studid != ""){
				$like['enrollments.studid'] = $studid;
			}
			if($year_id != ""){
				$like['enrollments.year_id'] = $year_id;
			}
			
			$page = 0;
		}
		
		//CONFIGURATION
		$get['fields'] = array(
				'enrollments.studid',
				'enrollments.name',
				'subjects.subject',
				'subjects.code',
				'courses.course',
				'years.year'
		);
		
		$get['where'] = $filter;
		// $get['like'] = $like;
		
		$get['join'] = array(
			1 => array(
				"table" => "enrollments",
				"on"	=> "enrollments.id = studentsubjects.enrollmentid",
				"type"  => "LEFT"
			),
			2 => array(
				"table" => "courses",
				"on"	=> "courses.id = studentsubjects.course_id",
				"type"  => "LEFT"
			),
			3 => array(
				"table" => "subjects",
				"on"	=> "subjects.id = studentsubjects.subject_id",
				"type"  => "LEFT"
			),
			4 => array(
				"table" => "years",
				"on"	=> "years.id = enrollments.year_id",
				"type"  => "LEFT"
			)
		);
		$get['group'] = "studentsubjects.enrollmentid";
		$get['order'] = "enrollments.name";
		$get['all'] = true; //GET ALL EXCLUDE LIMIT
		$get['count'] = true; //RETURN COUNT NOT ROW
		$get['array'] = false; //RETURN OBJECT NOT ARRAY
		$get['single'] = false; //RETURN ALL NOT SINGLE
		
	
		$this->load->library("pagination");
		$config = $this->pagination_style();
		$config["base_url"] = base_url() ."enrollees/nstp_students";
		$config['suffix'] = '?'.http_build_query($_GET, '', "&");
		$config['first_url'] = $config['base_url'].$config['suffix']; 
		
		$this->view_data['total_rows'] = $config["total_rows"] = $this->M_core_model->get_record("studentsubjects", $get);
		
		$config["per_page"] = 30;
		$config['num_links'] = 10;
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		
		//FOR PAGINATION
		$get['all'] = false;
		$get['count'] = false;
		if($this->input->get('submit') == "Print")
		{
			$get['all'] = true;
		}
		$get['start'] = $page;
		$get['limit'] = $config['per_page'];
		
		$this->view_data['search'] = $search = $this->M_core_model->get_record("studentsubjects", $get);
		$this->view_data['links'] = $this->pagination->create_links();
		
		if($this->input->get('submit') == "Print")
		{
			$this->print_nstp_students($this->view_data);
		}
	}
	
	public function print_nstp_students($data = false)
	{
		if($data == false) {show_404();}
		
		if($data['total_rows'] <= 0) {	
			$this->view_data['system_message'] = '<div class="alert alert-danger">No record to be printed.</div>';
			return false;
		}
		
		$search = $data['search'];
		
		if($search)
		{
			$this->load->helper('print');
			
			$html = _html_nstp_students($data); 

			$this->load->library('mpdf');
			
			$mpdf=new mPDF('','FOLIO','','',15,5,5,5,0,0); 

			$mpdf->AddPage('P');

			$mpdf->WriteHTML($html);

			$mpdf->Output();
		}
	}
	
	public function dropped_students($page = 0)
	{
		$this->load->model('M_core_model');
		$this->load->helper('my_dropdown');
		
		//PAGINATION
		
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		
		$filter = false;
		$like = false;
		$order_by = false;
		$group_by = 'studentsubjects.enrollmentid';
		
		$filter['enrollments.semester_id'] = $this->open_semester->id;
		$filter['enrollments.sy_from'] = $this->open_semester->year_from;
		$filter['enrollments.sy_to'] = $this->open_semester->year_to;
		$filter['enrollments.is_deleted'] = 1;
		
		if($_GET)
		{
			$this->view_data['lastname'] = $lastname = trim($this->input->get('lastname'));
			$this->view_data['fname'] = $fname = trim($this->input->get('fname'));
			$this->view_data['studid'] = $studid = trim($this->input->get('studid'));
			$this->view_data['year_id'] = $year_id = trim($this->input->get('year_id'));
			
			if($lastname != ""){
				$like['enrollments.lastname'] = $lastname;
			}
			if($fname != ""){
				$like['enrollments.fname'] = $fname;
			}
			if($studid != ""){
				$like['enrollments.studid'] = $studid;
			}
			if($year_id != ""){
				$like['enrollments.year_id'] = $year_id;
			}
			
			$page = 0;
		}
		
		//CONFIGURATION
		$get['fields'] = array(
				'enrollments.id',
				'enrollments.studid',
				'enrollments.name',
				'enrollments.created_at',
				'years.year'
		);
		
		$get['join'] = array(
			1 => array(
				"table" => "years",
				"on"	=> "years.id = enrollments.year_id",
				"type"  => "LEFT"
			)
		);
		$get['where'] = $filter;
		$get['like'] = $like;
		$get['order'] = "enrollments.name";
		$get['all'] = true;
		$get['count'] = true;
		
		$this->load->library("pagination");
		$config = $this->pagination_style();
		$config["base_url"] = base_url() ."enrollees/dropped_students";
		$config['suffix'] = '?'.http_build_query($_GET, '', "&");
		$config['first_url'] = $config['base_url'].$config['suffix']; 
		
		$this->view_data['total_rows'] = $config["total_rows"] = $this->M_core_model->get_record("enrollments", $get);
		
		$config["per_page"] = 30;
		$config['num_links'] = 10;
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		
		//FOR PAGINATION
		$get['all'] = false;
		$get['count'] = false;
		if($this->input->get('submit') == "Print")
		{
			$get['all'] = true;
		}
		$get['start'] = $page;
		$get['limit'] = $config['per_page'];
		
		$this->view_data['search'] = $search = $this->M_core_model->get_record("enrollments", $get);
		$this->view_data['links'] = $this->pagination->create_links();
		
		if($this->input->get('submit') == "Print")
		{
			$this->print_dropped_students($this->view_data);
		}
	}
	
	public function print_dropped_students($data = false)
	{
		if($data == false) {show_404();}
		
		if($data['total_rows'] <= 0) {	
			$this->view_data['system_message'] = '<div class="alert alert-danger">No record to be printed.</div>';
			return false;
		}
		
		$search = $data['search'];
		
		if($search)
		{
			$this->load->helper('print');
			
			$html = _html_dropped_students($data); 

			$this->load->library('mpdf');
			
			$mpdf=new mPDF('','FOLIO','','',15,5,5,5,0,0); 

			$mpdf->AddPage('P');

			$mpdf->WriteHTML($html);

			$mpdf->Output();
		}
	}
}

?>