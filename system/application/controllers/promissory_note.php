<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Promissory_note extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('url','form'));
		$this->load->library(array('form_validation'));
		$this->load->model(array('M_promissory_notes','M_enrollments'));
		$this->session_checker->open_semester();
		
	}
	
	public function add_new($enrollment_id)
	{
		$this->session_checker->check_if_alive();
		
		$this->view_data['enrollment_id'] = $enrollment_id;
		
		if($_POST)
		{
			$enrollmentid = $_POST['enrollment_id'];
			if($this->form_validation->run('promissory_note') !== FALSE)
			{
				$_POST['date'] = date('Y-m-d', strtotime($_POST['date']));
				$prom = $this->M_promissory_notes->add_promissory_notes($_POST);
				if($prom)
				{
					$this->session->set_flashdata('system_message','<div class="alert alert-success">Promissory Was Successfully Added</div>');
				}
				else
				{
					$this->session->set_flashdata('system_message','<div class="alert alert-danger">An error was encountered while proccessing your request</div>');
				}
				redirect('fees/view_fees/'.$enrollmentid);
			}
		}
	}
	
	public function delete_primossory_note($enrollment_id,$id,$data)
	{
		$this->session_checker->check_if_alive();
		
		$delprom = $this->M_promissory_notes->set_delete_promissory_notes($id,$data);
		echo "true";
		die();
		redirect('fees/view_fees/'.$enrollmentid);
	}
	
	public function print_promissory_note($enrollment_id)
	{
		$this->session_checker->check_if_alive();
		
		$this->load->model(array('M_settings',));
		$setting = $this->M_settings->get_settings();
		$profiler = $this->M_enrollments->profile($enrollment_id);
		
		$this->load->helper('print_promissory_note');
		$h = "8";// Hour for time zone goes here e.g. +7 or -4, just remove the + or -
		$hm = $h * 60;
		$ms = $hm * 60;
		$now = time();
		$gmt = local_to_gmt($now);
		$gmdate = $gmt+($ms); // the "-" can be switched to a plus if that's what your time zone is.
		
		// print_html defined at helpers/print_helper.php
		$html = print_html($setting,$profiler); 
		$this->load->library('mpdf');
		
		$mpdf=new mPDF('','LETTER','','',5,5,5,5,0,0); 

		$mpdf->WriteHTML($html);

		$mpdf->Output();
	}
}