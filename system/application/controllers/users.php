<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->session_checker->open_semester();
		$this->load->model(array('M_users','M_core_model'));
		$this->load->library(array('form_validation'));
		$this->load->helper(array('url','form','my_dropdown'));
	}

	public function activate_users($page = 0){
		$this->menu_access_checker(array(
				'users/activate_users'
			));

		$this->view_data['custom_title'] = 'Activate/Deactivate Employees';

		//PAGINATION
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		$this->view_data['return_url'] = current_url();
		$filter['employees.employeeid <>'] = $this->userlogin;

		$like = false;
		$or_like = false;
		$order_by = false;
		
		
		if($_GET)
		{
			if(isset($_GET['name']) && trim($_GET['name']) != ''){
				$this->view_data['name'] = $name = trim($_GET['name']);
				$or_like['employees.last_name'] = $name;
				$or_like['employees.middle_name'] = $name;
				$or_like['employees.middle_name'] = $name;
				$or_like['employees.first_name'] = $name;
				$or_like['employees.first_name'] = $name;
			}
			
			if(isset($_GET['login']) && trim($_GET['login']) != ''){
				$this->view_data['login'] = $login = trim($_GET['login']);
				$like['users.login'] = $login;
				
			}
			
			if(isset($_GET['role']) && trim($_GET['role']) != ''){
				$this->view_data['role'] = $role = trim($_GET['role']);
				$filter['employees.department'] = $role;
			}

			if(isset($_GET['active']) && trim($_GET['active']) != ''){
				$this->view_data['active'] = $active = trim($_GET['active']);
				$filter['users.is_activated'] = $active;
			}
		}
		
		//CONFIGURATION
		$get['fields'] = array(
				"employees.*",
				"users.is_activated",
				"users.id as users_id",
				"users.activated_at",
				"CONCAT(employees.last_name,',',employees.first_name,' ', employees.middle_name) as fullname"
		);
		
		$get['where'] = $filter;
		$get['like'] = $like;
		$get['or_like'] = $or_like;
		
		$get['join'] = array(
			
			1 => array(
				"table" => "users",
				"on"	=> "users.login = employees.employeeid",
				"type"  => "LEFT"
			)
			
		);
		$get['order'] = "employees.name";
		$get['all'] = true; //GET ALL EXCLUDE LIMIT
		$get['count'] = true; //RETURN COUNT NOT ROW
		$get['array'] = false; //RETURN OBJECT NOT ARRAY
		$get['single'] = false; //RETURN ALL NOT SINGLE
		
	
		$this->load->library("pagination");
		$config = $this->pagination_style();
		$config["base_url"] = base_url() ."employees/index";
		$config['suffix'] = '?'.http_build_query($_GET, '', "&");
		$config['first_url'] = $config['base_url'].$config['suffix']; 
		
		$this->view_data['total_rows'] = $config["total_rows"] = $this->M_core_model->get_record("employees", $get);
		
		$config["per_page"] = 25;
		$config['num_links'] = 10;
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		
		//FOR PAGINATION
		$get['all'] = false;
		$get['count'] = false;
		if($this->input->post('submit') == "Print")
		{
			$get['all'] = true;
		}
		$config['start'] = $page;
		$config['limit'] = $config['per_page'];
		
		$get['start'] = $page;
		$get['limit'] = $config['per_page'];
		
		$this->view_data['search'] = $search = $this->M_core_model->get_record("employees", $get);
		$this->view_data['links'] = $this->pagination->create_links();
	}
	
	public function activate($id = false, $par = false, $emp_id = false, $action = false){
		
		$this->menu_access_checker(array(
				'users/activate_users',
				'employees',
				'employees/index'
			));
		
		$xuser = $this->M_users->get_user_by_id($id);

		if($xuser && ($par == 1 || $par == 0)):

		$data['is_activated'] = $par;

		$xpar = "";
		$xpar2 = "";

		if($par == 0){
			$data['activated_at'] = null;
			$xpar = 'DE-ACTIVATE ';
			$xpar2 = 'Deactivated';
		}else{
			$data['activated_at'] =  NOW;
			$xpar = 'ACTIVATE ';
			$xpar2 = 'Activated';
		}

		$rs = $this->M_users->update_users($data, $id);
		
		if($rs['status'])
		{
			activity_log($xpar.' Employee',$this->userlogin, 'User ID : '.$id.' User Login : '.$xuser->login);
			$this->session->set_flashdata('system_message','<div class="alert alert-success"><span class = "glyphicon glyphicon-ok"></span>&nbsp; Employee successfully '.$xpar2.'</div>');
			// redirect($this->input->post('return_url'));

			if($action === "edit_profile"){
				redirect('employees/edit/'.$emp_id);
			}else{
				redirect('users/activate_users');
			}
		}
		else
		{
			$this->session->set_flashdata('system_message','<div class="alert alert-success"><span class = "glyphicon glyphicon-remove"></span>&nbsp; Process failed. Please try again</div>');
			if($action === "edit_profile"){
				redirect('employees/edit/'.$emp_id);
			}else{
				redirect('users/activate_users');
			}
		}

		endif;
	}
}