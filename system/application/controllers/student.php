<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Student Extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->session_checker->open_semester();
		$this->load->helper(array('url_encrypt'));
		$this->menu_access_checker(array(
			'student',
			'student/search_schedule',
			'student/soa',
			'student/reset_password'
		));
		$this->load->model('M_core_model');
		$this->load->helper('my_dropdown');
		
		$this->load->model(array(
			'M_enrollments',
			'M_student_subjects',
			'M_studentpayments_details'));
	}
		

	public function index()
	{
		$this->session_checker->check_if_alive();
	}
	
	public function search_schedule($page = 0)
	{	
		//PAGINATION
		
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		
		$filter = false;
		$like = false;
		$order_by = false;
		
		$filter['enrollments.semester_id'] = $this->open_semester->id;
		$filter['enrollments.sy_from'] = $this->open_semester->year_from;
		$filter['enrollments.sy_to'] = $this->open_semester->year_to;
		$filter['enrollments.is_deleted'] = 0;
		$filter['enrollments.is_paid'] = 1;
		
		
		$arr_filters = array();
		$suffix = "";
		
		if($_GET)
		{
			if(isset($_GET['lastname'])){
				$this->view_data['lastname'] = $lastname = trim($_GET['lastname']);
				$like['enrollments.lastname'] = $lastname;
				$arr_filters['lastname'] = $lastname;
			}
			
			if(isset($_GET['fname'])){
				$this->view_data['fname'] = $fname = trim($_GET['fname']);
				$like['enrollments.fname'] = $fname;
				$arr_filters['fname'] = $fname;
				
			}
			
			if(isset($_GET['studid'])){
				$this->view_data['studid'] = $studid = trim($_GET['studid']);
				$like['enrollments.studid'] = $studid;
				$arr_filters['studid'] = $studid;
			}
			
			if(isset($_GET['year_id'])){
				$this->view_data['year_id'] = $year_id = trim($_GET['year_id']);
				$filter['enrollments.year_id'] = $year_id;
				$arr_filters['year_id'] = $year_id;
			}
			
			$suffix = array_to_geturl($arr_filters);
		}
		
		//CONFIGURATION
		$get['fields'] = array(
				'enrollments.id',
				'enrollments.studid',
				'enrollments.name' ,
				'years.year',
				'courses.course' 
		);
		
		$get['where'] = $filter;
		$get['like'] = $like;
		
		$get['join'] = array(
			
			1 => array(
				"table" => "courses",
				"on"	=> "courses.id = enrollments.course_id",
				"type"  => "LEFT"
			),
			2 => array(
				"table" => "years",
				"on"	=> "years.id = enrollments.year_id",
				"type"  => "LEFT"
			)
		);
		$get['order'] = "enrollments.name";
		$get['all'] = true; //GET ALL EXCLUDE LIMIT
		$get['count'] = true; //RETURN COUNT NOT ROW
		$get['array'] = false; //RETURN OBJECT NOT ARRAY
		$get['single'] = false; //RETURN ALL NOT SINGLE
		
	
		$this->load->library("pagination");
		$config = $this->pagination_style();
		$config["base_url"] = base_url() ."student/search_schedule";
		$config['suffix'] = '?'.http_build_query($_GET, '', "&");
		$this->view_data['suffix'] = $config['suffix'];
		$config['first_url'] = $config['base_url'].$config['suffix']; 
		
		$this->view_data['total_rows'] = $config["total_rows"] = $this->M_core_model->get_record("enrollments", $get);
		
		$config["per_page"] = 25;
		$config['num_links'] = 10;
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		
		//FOR PAGINATION
		$get['all'] = false;
		$get['count'] = false;
		if($this->input->post('submit') == "Print")
		{
			$get['all'] = true;
		}
		$config['start'] = $page;
		$config['limit'] = $config['per_page'];
		
		$get['start'] = $page;
		$get['limit'] = $config['per_page'];
		
		$this->view_data['search'] = $search = $this->M_core_model->get_record("enrollments", $get);
		$this->view_data['links'] = $this->pagination->create_links();
		
		if($this->input->post('submit') == "Print")
		{
			$this->print_nstp_students($this->view_data);
		}
	}	
	
	public function view_schedule($enrollment_id= false)
	{
		if($enrollment_id == false) { show_404(); }
		$this->view_data['subjects'] = $subjects = $this->M_student_subjects->get_studentsubjects($enrollment_id);
		
		$this->view_data['enrollment_id'] = $enrollment_id;
		$this->view_data['back_url'] = base_url('student/search_schedule').'?'.http_build_query($_GET, '', "&");;
	}
	
	public function promisory_old($id = false)
	{
		if($id == false) { show_404(); }
		
		$enrollment_id = $id;
		
		$this->load->model(array("M_issues", "M_enrollments","M_studentpromisory",'M_student_total_file','M_studentpayments_details'));
		
		$this->view_data['student_profile'] = $this->view_data['student'] = $p = $this->M_enrollments->profile($enrollment_id); // student profile
		$this->view_data['enrollment_id'] = $id;
		
		// Count Issues
		$this->issue_count($p->user_id);
		
		$this->view_data['promisory'] = $this->M_studentpromisory->get_all($enrollment_id);
		
		$this->view_data['current_period'] = $this->current_grading_period;
		
		$this->view_data['student_total'] = $this->M_student_total_file->get_student_total_file($enrollment_id);
		
		$this->view_data['payment_details'] = $this->M_studentpayments_details->get_record_by_enrollment_id($enrollment_id);
	}
	
	public function promisory($page = 0)
	{
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		
		$this->view_data['current_period'] = $this->current_grading_period;
		
		$filter = false;
		$like = false;
		$order_by = false;
		
		$filter['enrollments.semester_id'] = $this->open_semester->id;
		$filter['enrollments.sy_from'] = $this->open_semester->year_from;
		$filter['enrollments.sy_to'] = $this->open_semester->year_to;
		$filter['enrollments.is_deleted'] = 0;
		
		$arr_filters = array();
		$suffix = "";
		
		if($_GET)
		{
			if(isset($_GET['submit']) && $_GET['submit'] == "Search")
			{
				$page = 0;
			}
			
			if(isset($_GET['name']) && trim($_GET['name']) != ''){
				$this->view_data['name'] = $name = trim($_GET['name']);
				$like['enrollments.name'] = $name;
				$arr_filters['name'] = $name;
				
			}
			
			if(isset($_GET['studid']) && trim($_GET['studid']) != ''){
				$this->view_data['studid'] = $studid = trim($_GET['studid']);
				$like['enrollments.studid'] = $studid;
				$arr_filters['studid'] = $studid;
			}
			
			if(isset($_GET['year_id']) && trim($_GET['year_id']) != ''){
				$this->view_data['year_id'] = $year_id = trim($_GET['year_id']);
				$filter['enrollments.year_id'] = $year_id;
				$arr_filters['year_id'] = $year_id;
			}
			
			if(isset($_GET['course_id']) && trim($_GET['course_id']) != ''){
				$this->view_data['course_id'] = $course_id = trim($_GET['course_id']);
				$filter['enrollments.course_id'] = $course_id;
				$arr_filters['course_id'] = $course_id;
			}
			
			$suffix = array_to_geturl($arr_filters);
		}
		
		//CONFIGURATION
		$get['fields'] = array(
				'enrollments.id',
				'enrollments.studid',
				'enrollments.name' ,
				'years.year',
				'courses.course' ,
				'payment_plan.division'
		);
		
		$get['where'] = $filter;
		$get['like'] = $like;
		
		$get['join'] = array(
			
			1 => array(
				"table" => "courses",
				"on"	=> "courses.id = enrollments.course_id",
				"type"  => "LEFT"
			),
			2 => array(
				"table" => "years",
				"on"	=> "years.id = enrollments.year_id",
				"type"  => "LEFT"
			),
			3 => array(
				"table" => "payment_plan",
				"on"	=> "payment_plan.id = enrollments.payment_plan_id",
				"type"  => "LEFT"
			)
		);
		$get['order'] = "enrollments.name";
		$get['all'] = true; //GET ALL EXCLUDE LIMIT
		$get['count'] = true; //RETURN COUNT NOT ROW
		$get['array'] = false; //RETURN OBJECT NOT ARRAY
		$get['single'] = false; //RETURN ALL NOT SINGLE
		
	
		$this->load->library("pagination");
		$config = $this->pagination_style();
		$config["base_url"] = base_url() ."student/promisory";
		$suffix = '?'.http_build_query($_GET, '', "&");
		$suffix = str_replace("&submit=Search", "", $suffix);
		$config['suffix'] = $suffix;
		$config['first_url'] = $config['base_url'].$config['suffix']; 
		
		//$this->view_data['total_rows'] = $config["total_rows"] = $this->M_core_model->get_record("enrollments", $get);
		
		$get['count'] = false;
		$this->view_data['all_rows'] = $all_rows = $this->M_core_model->get_record("enrollments", $get);
		
		$student_w_promisory = false;
		$student_p_details = false;
		
		//LOOP ALL RECORDS AND GET IF STUDENTS HAS PROMISORY
		if($all_rows):
			$i = 0;
			foreach($all_rows as $obj):
				
				$rs = $this->M_studentpayments_details->check_if_has_promisory($obj, $this->current_grading_period);
				
				if($rs)
				{
					$student_w_promisory[$i] = $obj;
					$student_p_details[$i] = $rs;
					
					$i++;
				}
				
			endforeach;
		
		endif;
		
		$this->view_data['total_rows'] = $config["total_rows"] = ($student_w_promisory) ? count($student_w_promisory) : 0;
		$this->view_data['student_w_promisory'] = $student_w_promisory;
		$config["per_page"] = 25;
		$config['num_links'] = 10;
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		
		$get['start'] = $page;
		$get['limit'] = $config['per_page'];
		
		$output = false;
		if($student_w_promisory)
		{
			$output = array_slice($student_w_promisory, $page, $config['per_page']);
		}
		
		$get['start'] = $page;
		$get['limit'] = $config['per_page'];
		
		$this->view_data['search'] = $search = $output;
		$this->view_data['details'] = $student_p_details;
		$this->view_data['links'] = $this->pagination->create_links();
		
		if($this->input->get('submit') == "Print")
		{
			$this->print_promisory_students($this->view_data);
		}
	}
	
	public function print_promisory_students($data = false)
	{
		if($data == false) {show_404();}
		
		if($data['total_rows'] <= 0) {	
			$this->view_data['system_message'] = '<div class="alert alert-danger">No record to be printed.</div>';
			return false;
		}
		
		$search = $data['student_w_promisory'];
		
		if($search)
		{
			$this->load->helper('print');
			
			$html = _html_promisory_students($data); 

			$this->load->library('mpdf');
			
			$mpdf=new mPDF('','FOLIO','','',5,5,5,5,0,0); 

			$mpdf->AddPage('P');

			$mpdf->WriteHTML($html);

			$mpdf->Output();
		}
	}

	public function view_promisory($id = false, $enrollment_id = false)
	{
		if($id == false) { show_404(); }
		if($enrollment_id == false) { show_404(); }
		
		$this->view_data['current_period'] = $this->current_grading_period;
		
		$this->view_data['promisory'] = $this->M_studentpromisory->get($id);
		
		$this->view_data['enrollment_id'] = $enrollment_id;
	}
	
	public function soa($page = 0)
	{
		$this->view_data['custom_title'] = "Statement of Accounts";
	}

	public function reset_password($page = 0)
	{
		//USER ACCESS
		// $this->menu_access_checker(array(
		// 	'student/reset_password'
		// ));

		//PAGINATION
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		
		$filter = false;
		$like = false;
		$order_by = false;
		
		$filter['enrollments.semester_id'] = $this->open_semester->id;
		$filter['enrollments.sy_from'] = $this->open_semester->year_from;
		$filter['enrollments.sy_to'] = $this->open_semester->year_to;
		$filter['enrollments.is_deleted'] = 0;
		$filter['enrollments.is_paid'] = 1;
		
		
		$arr_filters = array();
		$suffix = "";
		
		if($_GET)
		{
			if(isset($_GET['submit']) && $_GET['submit'] == "Search")
			{
				$page = 0;
			}
			
			if(isset($_GET['lastname']) && trim($_GET['lastname']) != ''){
				$this->view_data['lastname'] = $lastname = trim($_GET['lastname']);
				$like['enrollments.lastname'] = $lastname;
				$arr_filters['lastname'] = $lastname;
			}
			
			if(isset($_GET['fname']) && trim($_GET['fname']) != ''){
				$this->view_data['fname'] = $fname = trim($_GET['fname']);
				$like['enrollments.fname'] = $fname;
				$arr_filters['fname'] = $fname;
				
			}
			
			if(isset($_GET['studid']) && trim($_GET['studid']) != ''){
				$this->view_data['studid'] = $studid = trim($_GET['studid']);
				$like['enrollments.studid'] = $studid;
				$arr_filters['studid'] = $studid;
			}
			
			if(isset($_GET['year_id']) && trim($_GET['year_id']) != ''){
				$this->view_data['year_id'] = $year_id = trim($_GET['year_id']);
				$filter['enrollments.year_id'] = $year_id;
				$arr_filters['year_id'] = $year_id;
			}
			
			$suffix = array_to_geturl($arr_filters);
		}
		
		//CONFIGURATION
		$get['fields'] = array(
				'enrollments.id',
				'enrollments.studid',
				'enrollments.name' ,
				'years.year',
				'courses.course',
				'enrollments.user_id',
				'enrollments.date_of_birth'
		);
		
		$get['where'] = $filter;
		$get['like'] = $like;
		
		$get['join'] = array(
			
			1 => array(
				"table" => "courses",
				"on"	=> "courses.id = enrollments.course_id",
				"type"  => "LEFT"
			),
			2 => array(
				"table" => "years",
				"on"	=> "years.id = enrollments.year_id",
				"type"  => "LEFT"
			)
		);
		$get['order'] = "enrollments.name";
		$get['all'] = true; //GET ALL EXCLUDE LIMIT
		$get['count'] = true; //RETURN COUNT NOT ROW
		$get['array'] = false; //RETURN OBJECT NOT ARRAY
		$get['single'] = false; //RETURN ALL NOT SINGLE
		
	
		$this->load->library("pagination");
		$config = $this->pagination_style();
		$config["base_url"] = base_url() ."student/reset_password";
		$suffix = '?'.http_build_query($_GET, '', "&");
		$suffix = str_replace("&submit=Search", "", $suffix);
		$config['suffix'] = $this->view_data['suffix'] = $suffix;
		$config['first_url'] = $config['base_url'].$config['suffix']; 
		
		$this->view_data['total_rows'] = $config["total_rows"] = $this->M_core_model->get_record("enrollments", $get);
		
		$config["per_page"] = 45;
		$config['num_links'] = 10;
		$config["uri_segment"] = 3;
		// $config['use_page_numbers'] = TRUE;
		$this->pagination->initialize($config);
		
		//FOR PAGINATION
		$get['all'] = false;
		$get['count'] = false;
		if($this->input->post('submit') == "Print")
		{
			$get['all'] = true;
		}

		$config['start'] = $page;
		$config['limit'] = $config['per_page'];
		
		$get['start'] = $page;
		$get['limit'] = $config['per_page'];
		
		$this->view_data['search'] = $search = $this->M_core_model->get_record("enrollments", $get);
		$this->view_data['links'] = $this->pagination->create_links();

		if($this->input->post('change_password') && $this->input->post('change_password') === "Change Password"){
			
			$eid = $this->input->post('eid');
			$type = $this->input->post('radios');
			$newpassword = $this->input->post('newpassword');
			$confirmpassword = $this->input->post('confirmpassword');

			//VALIDATE POST VALUES FIRST
			$this->load->model('M_enrollments');
			$pass = true;

			$user_data = $this->M_enrollments->get($eid);
			if($user_data === false){
				$pass = false;
				$this->_msg('e','Process Failed.', current_url());
			}

			$this->view_data['s_eid'] = $eid;
			$this->view_data['s_radios'] = $type;

			if($type === "custom"){
				if($newpassword == "" || $confirmpassword == ""){
					$pass = false;
					$this->view_data['system_error'] = "Password cannot be blank.";
				}
				else{

					if(strlen($newpassword) <= 5 || strlen($confirmpassword) <= 5){
						$pass = false;
						$this->view_data['system_error'] = "Password must atleast 6 letters";
					}else{

						if($newpassword !== $confirmpassword){
							$pass = false;
							$this->view_data['system_error'] = "Password do not match please try again";
						}
					}
				}
			}

			if($pass){
				unset($data);
				$this->load->model('M_enrollments');

				$this->load->library('login');
				$xface_lock = $type == "lastname" ? strtolower($user_data->lastname) : $newpassword;
				$this->login->_generate_password(trim($xface_lock));
				$data['crypted_password'] = $this->login->get_password();
				$data['salt'] = $this->login->get_salt();
				$this->load->model('M_users');
				$rs = $this->M_users->update($user_data->user_id, $data);
				if($rs){
					$this->_msg('s','Student Password was successful changed', current_url());
				}else{
					$this->view_data['system_error'] = "Password was not save successfully. Please try again";
					$this->view_data['s_eid'] = $eid;
					$this->view_data['s_radios'] = $type;
				}
			}
		}
	}
}

?>