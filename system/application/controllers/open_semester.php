<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Open_semester extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->session_checker->open_semester();
		// $this->session_checker->secure_page('admin');
		$this->menu_access_checker('open_semester');
	}
	
	public function edit_employee($data)
	{
		$this->session_checker->check_if_alive();
		
		$this->load->model(array('M_open_semesters'));
		
		$this->view_data['opensemesters'] = $this->M_open_semesters->get_all_list();
		$this->view_data['opensemesteruser'] = $opensemesteruser = $this->M_open_semesters->get_open_semester_userId($this->session->userdata['userid']);
		
		if($_POST)
		{
			$open_semester_id = $_POST['open_semester_id'];
			if($opensemesteruser){
				$updateemployeesetting = $this->M_open_semesters->update_open_semester_userId($open_semester_id,$this->session->userdata['userid']);
				redirect(current_url());
			}else{
				$insertemployeesetting = $this->M_open_semesters->insert_open_semester_userId($open_semester_id,$this->session->userdata['userid']);
				redirect(current_url());
			}
		}
	}
	
	// Create
	public function create()
	{
		$this->load->model(array('M_open_semesters','M_academic_years','M_semesters'));
		
		$this->view_data['open_semester'] = FALSE;
		
		$this->view_data['academic_years'] = $this->M_academic_years->find_all();
		
		$this->view_data['semesters'] = $this->M_semesters->find_all();
		
		if($_POST)
		{
			$data = $this->input->post('open_semesters');
			$data['created_at'] = date('Y-m-d H:i:s');
			$data['updated_at'] = date('Y-m-d H:i:s');
			
			$result = $this->M_open_semesters->create_open_semesters($data);
			
			if($result['status'])
			{
				log_message('error','Open Semester Created by: '.$this->user.'Success; Open Semester Id: '.$id);
				$this->session->set_flashdata('system_message', '<div class="alert alert-success">Open Semester successfully added.</div>');
				redirect('open_semester');
			}
		}
	}
	
	// Retrieve
	public function index()
	{
		$this->session_checker->check_if_alive();
		$this->load->model(array('m_open_semesters'));
		$this->view_data['open_semesters'] = $this->m_open_semesters->get_all_list();
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		
		if($_POST)
		{
			$current = $_POST['current'];
			if($current != ''){
				
				#get the open semester
				unset($get);
				$selected_semester = $this->m_open_semesters->get($current);
				if($selected_semester === false){ show_404(); }

				$data['use'] = 1;
				$data['updated_at'] = date('y-m-d h:i:s');
				
			   	#before updating clone all neccesary data if the new semester is diff. school year
					if($selected_semester->academic_year_id !== $this->cos->user->academic_year_id){
						$this->clone_sy_data($selected_semester->academic_year_id, $this->cos->user->academic_year_id);#clone previous academic year records
					}


				$this->M_open_semesters->reset_open_semester(); //close all open semester
				$this->M_open_semesters->close_all_enrollment(); //close all open enrollment
				$result = $this->M_open_semesters->update_open_semesters($data, $current);
				
				if($result['status'])
				{
					#Change All USER OPEN SEMESTER TO UPDATED SO THAT ADMIN NA LANG MAG SESET NG OPEN SEMESTER SA LAHAT
					$this->load->model('M_open_semester_employee_settings');
					$this->M_open_semester_employee_settings->update_all_open_semester($current);

					log_message('error','Open Semester Set by: '.$this->user.'Success; Open Semester Id: '.$id);
					$this->session->set_flashdata('system_message', '<div class="alert alert-success">Open semester was successfully changed.</div>');
					redirect('open_semester');
				}
			}
		}
	}
	
	public function display($id)
	{
		$this->load->model(array('M_open_semesters'));
		
		$this->view_data['open_semesters'] = $this->M_open_semesters->get_open_semesters($id);
	}
	
	// Update
	public function edit($id)
	{
		$this->load->model(array('M_open_semesters','M_academic_years','M_semesters'));
		
		$this->view_data['open_semesters'] = $this->M_open_semesters->get_open_semesters($id);
		
		$this->view_data['academic_years'] = $this->M_academic_years->find_all();
		
		$this->view_data['semesters'] = $this->M_semesters->find_all();
		
		if($_POST)
		{
			$data = $this->input->post('open_semesters');
			$data['updated_at'] = date('Y-m-d H:i:s');
			
			$result = $this->M_open_semesters->update_open_semesters($data, $id);
			
			if($result['status'])
			{
				log_message('error','Open Semester Updated by: '.$this->user.'Success; Open Semester Id: '.$id);
				$this->session->set_flashdata('system_message', '<div class="alert alert-success">Open Semester successfully updated.</div>');
				redirect('open_semester');
			}
		}
	}
	
	// Update
	public function destroy($id)
	{
		$this->load->model(array('M_open_semesters'));
		
		$result = $this->M_open_semesters->delete_open_semesters($id);
		log_message('error','Grading Period Deleted by: '.$this->user.'Success; Grading Period Id: '.$id);
		$this->session->set_flashdata('system_message', '<div class="alert alert-success">Open Semester successfully deleted.</div>');
		redirect('open_semester');
	}

	/**
	 * Open Enrollment for Selected Year
	 * @param int $id Open semester id
	 * @param boolean $open 1/0
	 */
	public function open_enrollment($id,$open)
	{
		$os = $this->M_open_semesters->pull($id, 'id');

		if($os){	
			$rs = $this->M_open_semesters->open_enrollment($id, $open);
			$this->_msg($rs->code, $rs->msg, 'open_semester');
		}else{ show_404(); }
	}
}
