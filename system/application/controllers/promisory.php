<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Promisory Extends MY_Controller
{
	public function __construct(){
		parent::__construct();
		$this->session_checker->open_semester();
		$this->load->helper(array('url_encrypt'));
		$this->session_checker->secure_page(array('student_affairs','cashier','finance'));
		$this->load->model('M_core_model');
		$this->load->helper('my_dropdown');
		
		$this->load->model(array('M_enrollments',
		'M_student_subjects',
		'M_studentpromisory',
		'M_studentpayments_details'));
	}		

	public function index($id = false)
	{
		if($id == false) { show_404(); }
		
		$enrollment_id = $id;
		
		$this->load->model(array("M_issues", "M_enrollments","M_studentpromisory",'M_student_total_file','M_studentpayments_details'));
		
		$this->view_data['student_profile'] = $this->view_data['student'] = $p = $this->M_enrollments->profile($enrollment_id); // student profile
		$this->view_data['enrollment_id'] = $id;
		
		// Count Issues
		$this->issue_count($p->user_id);
		
		$this->view_data['promisory'] = $this->M_studentpromisory->get_all($enrollment_id);
		
		$this->view_data['current_period'] = $this->current_grading_period;
		
		$this->view_data['student_total'] = $this->M_student_total_file->get_student_total_file($enrollment_id);
		
		$this->view_data['payment_details'] = $this->M_studentpayments_details->get_record_by_enrollment_id($enrollment_id);
	}
	
	public function view($id = false, $enrollment_id = false)
	{
		if($id == false) { show_404(); }
		if($enrollment_id == false) { show_404(); }
		
		$this->view_data['current_period'] = $this->current_grading_period;
		
		$this->view_data['promisory'] = $this->M_studentpromisory->get($id);
		
		$this->view_data['enrollment_id'] = $enrollment_id;
	}
	
	public function grant($id = false, $enrollment_id = false)
	{
		if($id == false) { show_404(); }
		if($enrollment_id == false) { show_404(); }
		
		$data['updated_at'] = NOW;
		$data['is_promisory'] = 1;
		$data['grant_userid'] = $this->session->userdata['userid'];
		
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		
		$current_period = $this->current_grading_period;
		
		$rs = $this->M_studentpayments_details->update_record_by_id($data, $id);
		
		if($rs['status'])
		{
			activity_log('Grant Promisory',$this->userlogin,'Grading Period : '.$current_period->period.' Enrollment ID '.$enrollment_id.'. Student Payment Detail ID : '.$id);
			
			
			$this->session->set_flashdata('system_message' ,'<div class="alert alert-success">Successfully grant promisory.</div>');					
			redirect('promisory/index/'.$enrollment_id);
		}
	}
	
	public function ungrant($id = false, $enrollment_id = false)
	{	
		if($id == false) { show_404(); }
		if($enrollment_id == false) { show_404(); }
		
		$data['updated_at'] = NOW;
		$data['is_promisory'] = 0;
		$data['grant_userid'] = $this->session->userdata['userid'];
		
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		
		$current_period = $this->current_grading_period;
		
		$rs = $this->M_studentpayments_details->update_record_by_id($data, $id);
		
		if($rs['status'])
		{
			activity_log('Remove Promisory',$this->userlogin,'Grading Period : '.$current_period->period.' Enrollment ID '.$enrollment_id.'. Student Payment Detail ID : '.$id);
			
			
			$this->session->set_flashdata('system_message' ,'<div class="alert alert-success">Successfully remove promisory.</div>');					
			redirect('promisory/index/'.$enrollment_id);
		}
	}
}