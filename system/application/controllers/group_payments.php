<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Group_payments extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->session_checker->open_semester();
		$this->menu_access_checker();

		// load models
		$this->load->model(
				'M_student_payment_record'
			);
	}
	
	// Create
	public function create()
	{
		$this->load->model(array('M_group_payments'));
		
		if($_POST){
			
			$start_date = $this->input->post('start_date');
			$end_date = $this->input->post('end_date');
			
			if($start_date != "" && $end_date != "")
			{
				$data['start_date'] = $start_date;
				$data['end_date'] = $end_date;
				$data['created_at'] = NOW;
				$data['updated_at'] = NOW;
				
				$result = $this->M_group_payments->create_group_payments($data);
				// vd($result);
				if ($result['status']) {
					$id = $result['id'];
					activity_log('Create Group Payments',$this->userlogin,'Group Payments Id: '.$id);
					log_message('success','Group_payments Created by: '.$this->user.'Success; Group_payments Id: '.$id);
					$this->session->set_flashdata('system_message', '<div class="alert alert-success">Group payments successfully added.</div>');
					redirect(current_url());
				}else{
					$this->session->set_flashdata('system_message', '<div class="alert alert-danger">Group payments saving failed.</div>');
					redirect(current_url());
				} 
			}
		}
	}
	
	// Retrieve
	public function index()
	{
		$this->load->model(array('M_group_payments'));
		$this->view_data['group_payments'] = $this->M_group_payments->find_all();
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
	}
	
	public function display($id = false)
	{
		if($id == false){ show_404(); }
		
		$this->load->model(array('M_group_payments','M_studentpayments'));
		
		$this->view_data['group_payments'] =  $group_payments = $this->M_group_payments->get_group_payments($id);
		
		// $this->view_data['studentpayments'] = $sp = $this->M_studentpayments->get_group_payments_bydate($group_payments->start_date, $group_payments->end_date);

		$this->view_data['studentpayments'] = $sp = $this->M_student_payment_record->get_student_payment_by_range($group_payments->start_date, $group_payments->end_date);
		
		$this->view_data['id'] = $id;
	}
	
	// Update
	public function edit($id = false)
	{
		if($id == false){ show_404(); }
		
		$this->load->model(array('M_group_payments'));
		
		$this->view_data['group_payments'] = $this->M_group_payments->get_group_payments($id);
		
		if($_POST)
		{
			$data = $this->input->post('years');
			$data['updated_at'] = date('Y-m-d H:i:s');
			
			$result = $this->M_years->update_years($data, $id);
			
			if($result['status'])
			{
				activity_log('Update Group Payments',$this->userlogin,'Group Payments Id: '.$id);
				log_message('error','Years Updated by: '.$this->user.'Success; Years Id: '.$id);
				$this->session->set_flashdata('system_message', '<div class="alert alert-danger">Year successfully updated.</div>');
				redirect('years');
			}
		}
	}
	
	// Update
	public function destroy($id = false)
	{
		if($id == false){ show_404(); }
		
		$this->load->model(array('M_group_payments'));
		
		$result = $this->M_group_payments->delete_group_payments($id);
		activity_log('Destroy Group Payments',$this->userlogin,'Group Payments Id: '.$id);
		log_message('success','Group_payments Deleted by: '.$this->user.'Success; Group_payments Id: '.$id);
		$this->session->set_flashdata('system_message', '<div class="alert alert-success">Group payments successfully deleted.</div>');
		redirect('group_payments');
	}
	
	public function print_show($id = false, $type = false){
		if($id == false){ show_404(); }
		if($type == false){ show_404(); }
		
		$this->load->model(array('M_group_payments','M_studentpayments'));
		
		$this->load->helper('print');
		
		$this->view_data['group_payments'] =  $group_payments = $this->M_group_payments->get_group_payments($id);
		
		$this->view_data['studentpayments'] = $studentpayments = $this->M_student_payment_record->get_student_payment_by_range($group_payments->start_date, $group_payments->end_date);

		// $studentpayments = $this->M_studentpayments->get_group_payments_bydate($group_payments->start_date, $group_payments->end_date);
		// // VD($studentpayments);
		// switch($type){
		// 	case "old_account":
		// 		$data = $studentpayments['old_account'];
		// 		$xtype = "Old Account";
		// 		break;
		// 	case "other":
		// 		$data = $studentpayments['other'];
		// 		$xtype = "Other";
		// 		break;
		// 	default:
		// 		$data = $studentpayments['account_recivable_student'];
		// 		$xtype = "Account Receivable";
		// 		break;
		// }
		
		$html = _html_daily_collections($group_payments->start_date,$group_payments->end_date, $studentpayments); 
		// echo $html;
		// die();
		$this->load->library('mpdf');
		
		$mpdf=new mPDF('','FOLIO','','',15,5,5,5,0,0); 

		$mpdf->AddPage('P');

		$mpdf->WriteHTML($html);

		$mpdf->Output();
				
	
	}
	
	public function print_shows($year_id,$sem_id,$course_id)
	{
		$this->session_checker->check_if_alive();
		
		
		if($course_id !== false AND $year_id !== false AND $sem_id !==false)
		{
			$this->load->model(array('M_enrollments','M_student_subjects','M_settings'));

			$this->load->helper('print_ched');
			
			$setting = $this->M_settings->get_settings();
			$students = $this->M_enrollments->get_enrollments_for_ched_report($course_id,$year_id,$sem_id,$this->setting->year_from,$this->setting->year_to);
			
			$b = 0;
		
		if($students){

		foreach($students as $student):
			$studentsubjects = $this->M_student_subjects->get_student_subjects($student->id,$course_id,$year_id,$sem_id);
		endforeach;
		
		foreach($students as $student):
		$a = 0;
		
		foreach($studentsubjects as $studentsubject):
		if($student->id == $studentsubject->enrollmentid){
			$a++;
		}
		endforeach;
		
		if($a > $b){
		$b = $a;
		}else{
		$b = $b;
		}
		endforeach;
		
		}
		
			// print_html defined at helpers/print_ched_helper.php
			$html = print_html($students,$setting,$b,$studentsubjects); 
			$this->load->library('mpdf');
		
			$mpdf=new mPDF('','FOLIO','','',5,5,5,5,0,0); 

			$mpdf->AddPage('L');

			$mpdf->WriteHTML($html);

			$mpdf->Output();
		}else{
			show_404();
		}
		
	}
}