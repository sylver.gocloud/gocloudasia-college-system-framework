<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Courses extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->session_checker->open_semester();
		// $this->session_checker->secure_page('admin');
		$this->load->library(array('form_validation'));


		$this->load->model(['M_courses']);
		$this->load->model('M_course_specialization','m_specialty');
	}
	
	public function index()
	{	
		$this->menu_access_checker();
		//LOAD CRUD LIBRARY
		unset($config);
		$config['title'] = 'Courses';
		$config['type'] = 'query';
		$config['sql'] = "SELECT id, course_code, course FROM courses";
		$config['table'] = "courses";
		$config['search_columns'] = array(
							'course_code' 	=> 'Code',
							'course'		=> 'Course'
						);
		$config['show_columns'] = array(
							'course_code' 	=> 'Code',
							'course'		=> 'Course'
						);
		$config['hide_id'] = true;
		$config['order'] = "course_code";
		$config['disable_system_message'] = true;
		$config['view_button'] = false;
		$config['edit_button'] = array('controller'=>'courses', 'method'=>'update', 'field'=>'id','caption' => 'View');
		$config['pager'] = array(
			"per_page" => 30,
			"num_links" => 3,
			"uri_segment" => 3
		);
		$config['search'] = true;

		$config['crud'] = array(
			0 => array(					
					'field' => 'course_code',
					'type'	=> 'text',
					'label' => 'Code',
					'rules' => 'required',
					'rules' => 'required|trim|is_unique[courses.course_code]',
					'attr'  => array(
						'name'        	=> 'course_code',
						'id'          	=> 'course_code',
						'placeHolder'   => 'Required',
						'maxlength'   	=> '25',
						'required'		=> true
						)
				),
			1 => array(					
					'field' => 'course',
					'type'	=> 'text',
					'label' => 'Course Name',
					'rules' => 'required',
					'attr'  => array(
						'name'        	=> 'course',
						'id'          	=> 'course',
						'placeHolder'   => 'Required',
						'maxlength'   	=> '100',
						'required'		=> true
						)
				)
			);

		// DELETE BUTTON METHOD
		// $config['delete_button'] = array(
		// 		'controller' 		=> 'courses',
		// 		'method' 			=> 'destroy',
		// 		'field' 			=> array('id')
		// 	);

		$this->load->library('_sylvercrud',$config);

		$this->view_data['data_record'] = $this->_sylvercrud->_html;
	}

	public function update($id = false)
	{
		$this->view_data['custom_title'] = "View or Update";
		$this->menu_access_checker(['courses','courses/index']);
		$this->view_data['courses'] = $c = $this->M_courses->pull($id, 'id, course_code, course');
		$this->view_data['specialty'] = $cs = $this->m_specialty->get_all_specialties($id);

		//Update Course
		if($this->input->post('update_course')){
			$this->load->library('form_validation');

			$this->form_validation->set_rules('courses[course_code]', 'Course Code', 'required|trim|is_unique[courses.course_code]');
			$this->form_validation->set_rules('courses[course]', 'Description', 'required|trim');

			if ($this->form_validation->run() !== FALSE){
				$save = $this->input->post('courses');
				
				$rs = $this->M_courses->update($id, $save);
				if($rs){
					activity_log('Update Course',false, 'Courses Table ID : '.$id.' Data : '.arr_str($save));
					$this->_msg('s','Course Successfully updated.', current_url());
				}else{
					$this->_msg('e','Transaction failed, please try again.', current_url());
				}
			}
		}

		//Update Course
		if($this->input->post('create_specialty')){

			$this->form_validation->set_rules('specialization', 'Course Specialization', 'required|trim');

			if ($this->form_validation->run() !== FALSE){

				$rs = $this->m_specialty->create_specialty($this->input->post(), $id);
				$this->_msg($rs->code, $rs->msg, current_url());
			}
		}
	}
}
