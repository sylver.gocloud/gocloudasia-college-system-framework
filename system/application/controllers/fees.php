<?php
class Fees Extends MY_Controller
{
	public function __construct(){
		parent::__construct();
		$this->session_checker->check_if_alive();
		$this->session_checker->open_semester();
		$this->load->helper(array('url_encrypt'));
		$this->load->Model(array('M_enrollments','M_fees','M_core_model'));
		
		// $this->menu_access_checker(array(
		// 	'search/search_student',
		// 	'enrollees/daily_enrollees',
		// 	'search/master_list_by_course',
		// 	'search/search_enrollee',
		// 	'search/list_students/paid',
		// 	'search/list_students/unpaid',
		// 	'search/list_students/fullpaid',
		// 	'search/list_students/partialpaid',
		// 	'fees/view_fees',
		// ));
	}

	public function index($page = 0, $fee_type = "TUITION")
	{
		$this->load->model(array('M_fees'));
		$this->session_checker->check_if_alive();
		// $this->view_data['fees'] = $this->M_fees->find_all();
		//PAGINATION
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		$this->view_data['fee_type'] = $fee_type;
	
		$filter = false;
		$like = false;
		$order_by = false;
		
		if($_GET)
		{
			if(isset($_GET['name']) && trim($_GET['name']) != ''){
				$this->view_data['name'] = $name = trim($_GET['name']);
				$like['fees.name'] = $name;
			}
		}
		
		switch ($fee_type) {
			case 'TUITION':
				$filter['is_tuition_fee'] = 1;
				break;
			case 'MISC':
				$filter['is_misc'] = 1;
				break;
			case 'LAB':
				$filter['is_lab'] = 1;
				break;
			case 'OTHER':
				$filter['is_other'] = 1;
				break;
			case 'NSTP':
				$filter['is_nstp'] = 1;
				break;
			case 'OTHER_SCHOOL':
				$filter['is_other_school'] = 1;
				break;
		}
		
		$get['where'] = $filter;
		$get['like'] = $like;
		
		// $get['join'] = array(
			
			// 1 => array(
				// "table" => "courses",
				// "on"	=> "courses.id = fees.course_id",
				// "type"  => "LEFT"
			// ),
			// 2 => array(
				// "table" => "years",
				// "on"	=> "years.id = fees.year_id",
				// "type"  => "LEFT"
			// )
		// );
		$get['order'] = "fees.name";
		$get['all'] = true; //GET ALL EXCLUDE LIMIT
		$get['count'] = true; //RETURN COUNT NOT ROW
		$get['array'] = false; //RETURN OBJECT NOT ARRAY
		$get['single'] = false; //RETURN ALL NOT SINGLE
		
	
		$this->load->library("pagination");
		$config = $this->pagination_style();
		$config["base_url"] = base_url() ."fees/index";
		$suffix = '?'.http_build_query($_GET, '', "&");
		$suffix = str_replace("&submit=Search", "", $suffix);
		$config['suffix'] = $suffix;
		$config['first_url'] = $config['base_url'].$config['suffix']; 
		
		$this->view_data['total_rows'] = $config["total_rows"] = $this->M_core_model->get_record("fees", $get);
		
		$config["per_page"] = 30;
		$config['num_links'] = 10;
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		
		//FOR PAGINATION
		$get['all'] = false;
		$get['count'] = false;
		if($this->input->post('submit') == "Print")
		{
			$get['all'] = true;
		}
		$config['start'] = $page;
		$config['limit'] = $config['per_page'];
		
		$get['start'] = $page;
		$get['limit'] = $config['per_page'];
		
		$this->view_data['search'] = $this->view_data['fees'] = $search = $this->M_core_model->get_record("fees", $get);
		$this->view_data['links'] = $this->pagination->create_links();
		
		if($this->input->post('submit') == "Print")
		{
			$this->print_nstp_students($this->view_data);
		}
	}

	public function index2($page = 0)
	{
		$this->load->model(array('M_fees'));
		$this->session_checker->check_if_alive();
		// $this->view_data['fees'] = $this->M_fees->find_all();
		//PAGINATION
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
	
		$filter = false;
		$like = false;
		$order_by = false;
		
		if($_GET)
		{
			if(isset($_GET['name']) && trim($_GET['name']) != ''){
				$this->view_data['name'] = $name = trim($_GET['name']);
				$like['fees.name'] = $name;
			}
		}
		
		//CONFIGURATION
		// $get['fields'] = array(
				// 'fees.id',
				// 'fees.studid',
				// 'fees.name' ,
				// 'years.year',
				// 'courses.course' 
		// );
		
		$get['where'] = $filter;
		$get['like'] = $like;
		
		// $get['join'] = array(
			
			// 1 => array(
				// "table" => "courses",
				// "on"	=> "courses.id = fees.course_id",
				// "type"  => "LEFT"
			// ),
			// 2 => array(
				// "table" => "years",
				// "on"	=> "years.id = fees.year_id",
				// "type"  => "LEFT"
			// )
		// );
		$get['order'] = "fees.name";
		$get['all'] = true; //GET ALL EXCLUDE LIMIT
		$get['count'] = true; //RETURN COUNT NOT ROW
		$get['array'] = false; //RETURN OBJECT NOT ARRAY
		$get['single'] = false; //RETURN ALL NOT SINGLE
		
	
		$this->load->library("pagination");
		$config = $this->pagination_style();
		$config["base_url"] = base_url() ."fees/index";
		$suffix = '?'.http_build_query($_GET, '', "&");
		$suffix = str_replace("&submit=Search", "", $suffix);
		$config['suffix'] = $suffix;
		$config['first_url'] = $config['base_url'].$config['suffix']; 
		
		$this->view_data['total_rows'] = $config["total_rows"] = $this->M_core_model->get_record("fees", $get);
		
		$config["per_page"] = 30;
		$config['num_links'] = 10;
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		
		//FOR PAGINATION
		$get['all'] = false;
		$get['count'] = false;
		if($this->input->post('submit') == "Print")
		{
			$get['all'] = true;
		}
		$config['start'] = $page;
		$config['limit'] = $config['per_page'];
		
		$get['start'] = $page;
		$get['limit'] = $config['per_page'];
		
		$this->view_data['search'] = $this->view_data['fees'] = $search = $this->M_core_model->get_record("fees", $get);
		$this->view_data['links'] = $this->pagination->create_links();
		
		if($this->input->post('submit') == "Print")
		{
			$this->print_nstp_students($this->view_data);
		}
	}
	
	// Create
	public function create($fee_type="TUITION")
	{
		$this->load->model(array('M_fees'));
		
		$this->view_data['fees'] = FALSE;
		
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		$this->view_data['fee_type'] = $fee_type;
		
		if($_POST)
		{
			$data = $this->input->post('fees');
			$data['is_deduction'] = isset($data['is_deduction']) ? 1 : 0;
			$data['is_active'] = isset($data['is_active']) ? 1 : 0;
			if(isset($_POST['fee_type']))
			{
				$type = $_POST['fee_type'];
				$data[$type] = 1;
			}
			
			// $data['is_nstp'] = isset($data['is_nstp']) ? 1 : 0;
			// $data['is_misc'] = isset($data['is_misc']) ? 1 : 0;
			// $data['is_other'] = isset($data['is_other']) ? 1 : 0;
			
			$data['donot_show_in_old'] = isset($data['donot_show_in_old']) ? 1 : 0;
			$data['created_at'] = date('Y-m-d H:i:s');
			$data['updated_at'] = date('Y-m-d H:i:s');
			$data['is_active'] = 1;
			// var_dump($data);
			$result = $this->M_fees->create_fees($data);
			
			if($result['status'])
			{
				$id = $result['id'];
				
				activity_log('create fee',$this->userlogin,'Created by: '.$this->userlogin.' Success; Fee Id: '.$id);
				
				log_message('error','Fees Created by: '.$this->user.'Success; Fee Id: '.$id);
				$this->session->set_flashdata('system_message', '<div class="alert alert-success">New Fee successfully added.</div>');
				redirect('fees/index/0/'.$fee_type);
			}
			else
			{
				$this->_msg('e','Something went wrong, fee was not saved please refresh the page and try again', current_url());
			}
		}
	}
	
	// Update
	public function edit($id = false,$tab="")
	{
		if($id == false) { show_404(); }
		
		$this->load->model(array('M_fees'));
		
		$this->view_data['fees'] = $this->M_fees->get_fees($id);
		
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		
		if($_POST)
		{
			$data = $this->input->post('fees');
			$data['is_deduction'] = isset($data['is_deduction']) ? 1 : 0;
			$data['is_active'] = isset($data['is_active']) ? 1 : 0;
			
			if(isset($_POST['fee_type']))
			{
				$type = $_POST['fee_type'];
				switch($type)
				{
					case "is_misc";
						$data['is_nstp'] = 0;
						$data['is_misc'] = 1;
						$data['is_other'] = 0;
						$data['is_tuition_fee'] = 0;
						$data['is_lab'] = 0;
					break;
					
					case "is_tuition_fee";
						$data['is_nstp'] = 0;
						$data['is_misc'] = 0;
						$data['is_other'] = 0;
						$data['is_tuition_fee'] = 1;
						$data['is_lab'] = 0;
					break;
					
					case "is_other";
						$data['is_nstp'] = 0;
						$data['is_misc'] = 0;
						$data['is_other'] = 1;
						$data['is_tuition_fee'] = 0;
						$data['is_lab'] = 0;
					break;
					
					case "is_nstp";
						$data['is_nstp'] = 1;
						$data['is_misc'] = 0;
						$data['is_other'] = 0;
						$data['is_tuition_fee'] = 0;
						$data['is_lab'] = 0;
					break;
					
					case "is_lab";
						$data['is_nstp'] = 0;
						$data['is_misc'] = 0;
						$data['is_other'] = 0;
						$data['is_tuition_fee'] = 0;
						$data['is_lab'] = 1;
					break;
				}
			}
			$data['donot_show_in_old'] = isset($data['donot_show_in_old']) ? 1 : 0;
			$data['updated_at'] = date('Y-m-d H:i:s');
			// var_dump($data);
			$result = $this->M_fees->update_fees($data, $id);
			
			if($result['status'])
			{
				activity_log('updated fee',$this->userlogin,'Updated by: '.$this->userlogin.'Success; Fee Id: '.$id);
				
				log_message('error','Fees Updated by: '.$this->user.'Success; Fee Id: '.$id);
				$this->session->set_flashdata('system_message', '<div class="alert alert-success">Fees successfully updated.</div>');
				redirect('fees/index/0/'.$tab);
			}
		}
	}
	
	// Destroy
	public function destroy($id = false, $tab = "")
	{
		if($id == false) { show_404(); }
		$this->load->model(array('M_fees'));
		
		$result = $this->M_fees->delete_fees($id);
		activity_log('destroy fee',$this->userlogin,'Deleted by: '.$this->userlogin.'Success; Fee Id: '.$id);
		log_message('error','Fees Deleted by: '.$this->user.'Success; Fee Id: '.$id);
		$this->session->set_flashdata('system_message', '<div class="alert alert-success">Fee successfully deleted.</div>');
		redirect('fees/index/0/'.$tab);
	}
	
	public function view_fees($enrollment_id = false, $search_type = "all")
	{		
		if($enrollment_id == false) { show_404(); }
		
		$this->load->model(array('M_enrollments','M_student_finances','M_student_fees', 'M_student_previous_accounts', 'M_student_subjects', 'M_student_deductions','M_coursefees','M_assign_coursefees','M_studentpayments', 'M_additional_charges','M_student_total_file',
		'M_subjects',
		'M_student_refund',
		'M_studentexcess_prev_sem'));
		
		//CHECK DEPARTMENT STUDENT PROFILE ACCESS 
		$this->check_stud_access('stud_fees');
		
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		
		//LOAD STUDENT OBJECTS FROM LIBRARY
		set_time_limit(0);
		$this->load->library('_student',array('enrollment_id'=>$enrollment_id));
		$this->view_data['_student'] = $this->_student;

		//Top Search Parameter
		$this->view_data['search_type'] = $search_type;

		//Top header data of student
		$this->view_data['student'] = $p = $this->_student->profile;
		$this->view_data['enrollment_id'] = $enrollment_id;
		$this->view_data['show_drop_subjects'] = true;
		
		$this->issue_count($p->user_id);
		
		//GET STUDENT FINANCE
		$this->view_data['student_finance'] = $studentfinances = $this->_student->get_student_course_finance();
		// vd($studentfinances);
		
		//GET STUDENT TOTALS
		$this->view_data['student_total'] = $student_total = $this->M_student_total_file->get_student_total_file($enrollment_id);
		
		$stud_fees = false;
		if($studentfinances)
		{
			//GET STUDENT FEES
			$stud_fees = $this->M_student_fees->get_student_fees($studentfinances->id);

			$this->view_data['_student_fees'] = $this->_student->get_student_fee_profile();
		}
		
		//GET STUDENT REFUNDS
		$this->view_data['student_refund'] = $this->M_student_refund->get_record_by_eid($enrollment_id);
		
		//GET EXCESS OF PREVIOUS SEM
		$this->view_data['excess_prev_sum'] = $this->M_studentexcess_prev_sem->get_sum_studentexcess_by_eid($enrollment_id);
		
		//GET SUBJECTS
		$this->view_data['subjects'] = $subjects = $this->_student->student_subjects;

		//LOOP SUBJECTS TO GET TOTAL NUMBER OF UNIT LAB AND LEC
		$subjects_units = 0;
		$total_lec = 0;
		$total_lab = 0;
		$has_nstp = 0;
		
		if($subjects)
		{
			foreach($subjects as $subject)
			{				
				$subjects_units += $subject->units;
				$total_lec += $subject->lec;
				$total_lab += $subject->lab;
				if($subject->is_nstp == 1)
				{
					$has_nstp++;
				}	
			}				
		}
		
		$this->view_data['subject_units']['subject_units'] = $subjects_units;
		$this->view_data['subject_units']['total_lab'] = $total_lec;
		$this->view_data['subject_units']['total_lec'] = $total_lab;
		$this->view_data['total_lec'] = $total_lec;
		$this->view_data['total_lab'] = $total_lab;
		$this->view_data['has_nstp'] = $has_nstp;
		
		//ARRANGE STUDENT FEES
		
		if($stud_fees)
		{
			$student_fees = array();
			
			foreach($stud_fees as $cf)
			{
				if($cf->is_tuition_fee == 1)
				{
					$student_fees['tuition_fee'][0] = $cf;
					// $total_tuition = $subjects_units * $cf->value;
					// $total_amount_due += $total_tuition;
				}
				else if($cf->is_lab == 1)
				{
					$student_fees['lab'][$cf->id]['data'] = $cf;
					$student_fees['lab'][$cf->id]['value'] = $cf->value * $total_lab;
					//$total_amount_due += $cf->value * $total_lab;
				}
				else if($cf->is_misc == 1)
				{
					$student_fees['misc'][] = $cf;
					// $total_amount_due += $cf->value;
				}
				else if($cf->is_other == 1)
				{
					$student_fees['other'][] = $cf;
					// $total_amount_due += $cf->value;
				}
				else if($cf->is_nstp == 1)
				{
					$student_fees['nstp'][] = $cf;
					// $total_amount_due += $cf->value;
				}
				else{}
			}
			$this->view_data['student_fees'] = $student_fees;
		}

		$this->view_data['course'] = $p->course_id; 
		$this->view_data['year'] = $p->year_id;
		$this->view_data['semester'] = $p->sem_id;
		$this->view_data['enrollment_id'] = $p->id;
		
		
		//Ajax Subject
		$this->view_data['eid'] = $p->id;
		$this->view_data['y'] = $p->year_id;
		$this->view_data['c'] = $p->course_id;
		$this->view_data['s'] = $p->semester_id;
	}
	
	public function view_deducted_fees($enrollment_id = false)
	{
		$this->session_checker->check_if_alive();
		if($enrollment_id !== false AND ctype_digit($enrollment_id))
		{
			$this->view_data['system_message'] = $this->session->flashdata('system_message');
			$this->load->model('M_student_deductions');
			$this->view_data['enrollment_id'] = $enrollment_id;
			$this->view_data['deducted_fees'] = $this->M_student_deductions->get(false,array('id','amount','remarks','created_at'),array('enrollment_id'=>$enrollment_id));	
			
		}else{
			show_404();
		}
	}
	
	public function show_deducted_fee($id = false,$eid = false)
	{
		if(($id !== false AND ctype_digit($id)) AND ($eid !== false AND ctype_digit($eid)))
		{
			$this->token->set_token();
			$this->view_data['form_token'] = $this->token->get_token();
			$this->view_data['this_eid'] = $eid;
			$this->view_data['this_fid'] = $id;
			$this->load->model('M_student_deductions');
			$this->view_data['deducted_fee'] = $this->M_student_deductions->get(false,false,array('enrollment_id'=>$eid,'id'=>$id));

		}else
		{
			if($this->input->post('delete_deducted_fee'))
			{
				// if($this->token->validate_token($this->input->post('token'))){}
				$this->load->model('M_student_deductions');
				$amount = $this->input->post('amount');
				$post_fid = $this->input->post('fid');
				$post_eid = $this->input->post('eid');
				if($this->M_student_deductions->destroy_deducted_fee($post_fid,$post_eid))
				{
						$this->load->model('M_student_totals');
						$student_total = $this->M_student_totals->get_student_total($post_eid);
						$updated_amount = $student_total->remaining_balance + $amount;
						if($this->M_student_totals->update_student_totals(array('remaining_balance'=>$updated_amount),array('enrollment_id'=>$post_eid)))
						{
							log_message('info','Delete from deducted fees by: '.$this->user. '; Amount: '.$amount.'; from student Enrollment Id: '.$post_eid );
							$this->session->set_flashdata('system_message','<div class="alert alert-success">Deducted Fee was deleted from Records</div>');
							$this->token->destroy_token();
							redirect('fees/view_deducted_fees/'.$post_eid);
						}else
						{
							log_message('error','fees/show_deducted_fee/update_student_totals : unable to update student totals remaining_balance to: '.$updated_amount.'; student enrollment id: '.$post_eid);
							$this->view_data['system_message'] = '<div class="alert alert-danger">Fee was deleted but unable to deduct from student totals</div>';
						}
				}else{
					log_message('error','fees/show_deducted_fee/destroy_deducted_fee : unable to destroy fee id: '.$post_fid);
					$this->view_data['system_message'] = '<div class="alert alert-danger">Unable to delete fee</div>';
				}
			
			}else{
				show_404();
			}
		}
	}
	
	public function view_refund($id = false)
	{
		if($id == false) { show_404(); }
		
		$this->load->model(array('M_student_refund'));
		
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		
		$this->view_data['student_refund'] = $this->M_student_refund->get_record_by_eid($id);
		
		$this->view_data['eid'] = $id;
	}
	
	public function add_fees($sfid = false,$enrollment_id = false)
	{
		$this->session_checker->check_if_alive();
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		if($sfid !== false AND (int)$sfid)
		{
			$this->load->model('M_student_fees');
			$this->token->set_token();
			$this->view_data['form_token'] = $this->token->get_token();
			$this->view_data['fees'] = $this->M_student_fees->get_unassigned_fees($sfid);
			$this->view_data['sfid'] = $sfid;
			$this->view_data['eid'] = $enrollment_id;
			
			if($this->input->post('add_fee') !== false)
			{
				if($this->input->post('fee') !== false)
				{
					if($this->token->validate_token($this->input->post('form_token')))
					{
						$this->load->model('M_student_fees');
						$result = $this->M_student_fees->assign_fees($this->input->post('fee'),$this->input->post('sfid'),$this->input->post('eid'));
						if($result['status'] == 'true')
						{
							log_message('info','Added Fees by: '.$this->user.' '.$result['log'].' Enrollment Id: '.$enrollment_id.'; Student FInance id: '.$sfid);
							$this->session->set_flashdata('system_message','<div class="alert alert-success">Fee Was Successfully Added</div>');
							$this->token->destroy_token();
							redirect(current_url());
						}else{
							log_message('info','Added Fees by: '.$this->user.' '.$result['log'].' Enrollment Id: '.$enrollment_id.'; Student FInance id: '.$sfid);
							$this->view_data['system_message'] = '<div class="alert alert-danger">An error was encountered while proccessing your request</div>';
						}
					}else{
						log_message('info','Added Fees by: '.$this->user.' '.$result['log'].' Enrollment Id: '.$enrollment_id.'; Student FInance id: '.$sfid);
						$this->view_data['system_message'] = '<div class="alert alert-danger">Invalid Form Submit</div>';
					}
				}
			}
		}else{
			show_404();
		}
	}
	
	public function destroy_fees($sfid = false,$fid = false,$eid = false)
	{
		if($sfid !== false && $fid !== false && $eid !== false)
		{
			$fid = intval($fid);
			$sfid = intval($sfid);
			$eid = intval($eid);
			$jquery = $this->input->post('jquery');
			if($jquery == false)
			{
				$this->load->model('M_student_fees');
				if($this->M_student_fees->destroy_fee($sfid,$fid,$eid))
				{
					$this->session->set_flashdata('system_message', '<div class="alert alert-success">Fees were successfully destroyed</div>');
					redirect(current_url());
				}else
				{
					 $this->view_data['system_message'] = '<div class="alert alert-danger">An error was encountered while proccessing your request</div>';
				}
			}else
			{
				$this->load->model('M_student_fees');
				$result = $this->M_student_fees->destroy_fee($sfid,$fid,$eid);
				if($result['status'] == 'true')
				{
					log_message('info','Destroy Fees By: '.$this->user.'; Student Finance Id: '.$sfid.'; Fee Id: '.$fid.'; Student Enrollment Id :'.$eid);
					echo 'true';
					//echo $result['log']; // debug
					die();//stops php from sending the views so that browser will only receive true
				}else
				{
					log_message('error','Unable to Destroy Fee ID: '.$fid.'; Student Finance Id: '.$sfid.'; Student Enrollment Id: '.$eid.'; Process Request By: '.$this->user);
					echo 'false';
					//echo $result['log']; // debug
					die();//stops php from sending the views so that browser will only receive false
				}
			}
		}else{
			show_404();
		}
	}
	
	public function delete_previous_account($id, $eid)
	{
	  $this->load->model(array('M_student_previous_accounts'));
	  
	  $result = $this->M_student_previous_accounts->destroy($id);
	  
		log_message('error','Previous Account Deleted by: '.$this->user.'Success; Previous Account Id: '.$id);
		$this->session->set_flashdata('system_message', '<div class="alert alert-success">Previous Account successfully deleted.</div>');
		redirect('/fees/view_fees/'.$eid);
	}
	
	public function delete_additional_charge($id, $eid)
	{
	  $this->load->model(array('M_additional_charges'));
	  
	  $result = $this->M_additional_charges->destroy($id);
	  
		log_message('error','Additional Charge Deleted by: '.$this->user.'Success; Additional Charge Id: '.$id);
		$this->session->set_flashdata('system_message', '<div class="alert alert-success">Additional successfully deleted.</div>');
		redirect('/fees/view_fees/'.$eid);
	}
	
	/* 
		FUNCTIONS FOR SUBJECTS START HERE 
	*/

	public function destroy_subject($id)
	{		
		$jquery = $this->input->post('jquery');
		if($jquery == false)
		{
			$this->load->model('M_student_fees');
			if($this->M_student_fees->destroy_fees($id))
			{
				$this->session->set_flashdata('system_message', '<div class="alert alert-success">Update was successful</div>');
				redirect(current_url());
			}else
			{
				 $this->view_data['system_message'] = '<div class="alert alert-danger">An error was encountered while proccessing your request</div>';
			}
		}else
		{
			$this->load->model('M_student_subjects');
			if($this->M_student_subjects->destroy_subject($id))
			{
				echo 'true';
				die();//stops php from sending the views so that browser will only receive true
			}else
			{
				echo 'false';
				die();//stops php from sending the views so that browser will only receive false
			}
		}
	}

	public function ajax_drop_subject($eid = false, $student_subject_id = false, $redirect = false)
	{
		$this->disable_layout = true;
		$this->disable_menus = true;
		$this->disable_views = true; 

		$ret['status'] = 0;
		$ret['msg'] = "Student Subject deletion failed";

		//check student access
		if($this->departments->stud_edit_subject !== "1"){
			echo json_encode($ret);
			exit(0);
		}

		#load model
		$this->load->model('M_student_subjects');

		#load student library
		$this->load->library('_student',array('enrollment_id'=>$eid));

		if($this->_student->profile){
			$stud_subj = $this->M_student_subjects->pull($student_subject_id, array('id','subject_id')); #get student subject if true
			if($stud_subj){
				#execute delete subject function
				$rs = $this->_student->drop_student_subject($stud_subj->id);
				if($rs->status){
					$ret['status'] = 1;
					$ret['msg'] = $rs->msg;

					#activity log
					unset($log);
					$log['Enrollment Id'] = $eid;
					$log['Student Subject Id'] = $student_subject_id;
					$log['Subject ID'] = $stud_subj->subject_id;

					activity_log('Dropped Subject',$this->userlogin, 'Data '.arr_str($log));

					#redirec to given url
					if($redirect && $redirect == 'yes'){
						$this->_msg('s', $rs->msg, 'fees/view_fees/'.$eid);
					}

				}else{
					$ret['msg'] = $rs->msg;
					#redirec to given url
					if($redirect && $redirect == 'yes'){
						$this->_msg('s', $rs->msg, 'fees/view_fees/'.$eid);
					}
				}
			}
		}

		echo json_encode($ret);
	}

	public function ajax_delete_subject($eid = false, $student_subject_id = false, $redirect = false)
	{
		$this->disable_layout = true;
		$this->disable_menus = true;
		$this->disable_views = true; 

		$ret['status'] = 0;
		$ret['msg'] = "Student Subject deletion failed";

		//check student access
		if($this->departments->stud_edit_subject !== "1"){
			echo json_encode($ret);
			exit(0);
		}

		#load model
		$this->load->model('M_student_subjects');

		#load student library
		$this->load->library('_student',array('enrollment_id'=>$eid));

		if($this->_student->profile){
			$stud_subj = $this->M_student_subjects->pull($student_subject_id, array('id','subject_id')); #get student subject if true
			if($stud_subj){
				#execute delete subject function
				$rs = $this->_student->delete_student_subject($stud_subj->id);
				if($rs->status){
					$ret['status'] = 1;
					$ret['msg'] = $rs->msg;

					#activity log
					unset($log);
					$log['Enrollment Id'] = $eid;
					$log['Student Subject Id'] = $student_subject_id;
					$log['Subject ID'] = $stud_subj->subject_id;

					activity_log('Delete Subject',$this->userlogin, 'Data '.arr_str($log));

					#redirec to given url
					if($redirect && $redirect == 'yes'){
						$this->_msg('s', $rs->msg, 'fees/view_fees/'.$eid);
					}

				}else{
					$ret['msg'] = $rs->msg;
					#redirec to given url
					if($redirect && $redirect == 'yes'){
						$this->_msg('s', $rs->msg, 'fees/view_fees/'.$eid);
					}
				}
			}
		}

		echo json_encode($ret);
	}

	public function add_subject($id = false, $page = 0, $search_type = "all")
	{
		if($id == false){ show_404(); }	

		//CHECK DEPARTMENT STUDENT PROFILE ACCESS 
		$this->check_stud_access('stud_subject');

		//LOAD MODELS
		$this->load->model('M_student_subjects','m_ss');
		$this->load->model('M_subjects2','m_s');
		$this->load->model('M_grading_periods','m_gp');
		$this->load->model('M_grades_file','m_gf');
		$this->load->model('M_subjects');
		$this->load->model('M_curriculum_subjects');
		$this->load->model('M_curriculum');
		$this->load->model('M_subject_pre_requisite');

		$this->load->helper('time');

		#load student library
		set_time_limit(0);
		$this->load->library('_student',array('enrollment_id'=>$id));
		$this->view_data['student'] = $this->_student->profile;
		$this->view_data['grading_periods'] = $this->m_gp->get_all(); 
		$this->view_data['grades'] = $sg = $this->_student->get_student_subject_grades();
		$this->view_data['student_grades'] = $this->_student->student_subjects; 
		$this->view_data['_student'] = $this->_student;
		$this->view_data['id'] = $id;
		$this->view_data['show_tri_menu'] = true;
		if($this->_student->profile == false){ show_404(); }

		## Curriculum Check list ##
		$this->view_data['curriculum_id'] = $curriculum_id =  $this->M_curriculum_subjects->get_current_course_curriculum($this->_student->profile->course_id);
		$this->view_data['curriculum'] = $curriculum = $this->M_curriculum->get_curriculum($curriculum_id);
		$this->view_data['curriculum_subjects'] = $cs = $this->M_curriculum_subjects->get_curriculum_subjects($curriculum_id);
		$this->view_data['get_pre_requisite'] = function($c_id, $s_id){return $this->M_subject_pre_requisite->get_pre_requisite($c_id, $s_id);};

		// Count Issues
		$this->issue_count($this->_student->profile->user_id);

		//Top Search Parameter
		$this->view_data['search_type'] = $search_type;

		#Available subjects

		//PAGINATION
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		$this->view_data['page'] = $page;
		$this->session->set_userdata('subject_get', '?'.http_build_query($_GET, '', "&"));

		$like = false;
		$filter = false;

		$filter['subjects.year_id'] = $this->_student->profile->year_id;
		$filter['subjects.semester_id'] = $this->_student->profile->semester_id;
		$filter['subjects.year_from'] = $this->_student->profile->sy_from;
		$filter['subjects.year_to'] = $this->_student->profile->sy_to;
		
		if($_GET)
		{	
			if(isset($_GET['code']) && trim($_GET['code'])){
				$this->view_data['code'] = $code = trim($_GET['code']);
				$like['master_subjects.code'] = $code;
			}
			
			if(isset($_GET['subject']) && trim($_GET['subject'])){
				$this->view_data['subject'] = $subject = trim($_GET['subject']);
				$like['master_subjects.subject'] = $subject;
			}

			if(isset($_GET['time']) && trim($_GET['time']) != ""){
				$this->view_data['time'] = $time = trim($_GET['time']);
				$like['subjects.time'] = $time;
			}
			
			if(isset($_GET['day']) && trim($_GET['day'])){
				$this->view_data['day'] = $day = trim($_GET['day']);
				$like['subjects.day'] = $day;
			}
			
			if(isset($_GET['instructor']) && trim($_GET['instructor'])){
				$this->view_data['instructor'] = $instructor = trim($_GET['instructor']);
				$like['users.name'] = $instructor;
			}
		}
		
		//CONFIGURATION
		$get['fields'] = array(
				'subjects.id',
				'master_subjects.code',
				'master_subjects.subject',
				'master_subjects.units',
				'master_subjects.lec',
				'master_subjects.lab',
				'subjects.is_open_time',
				'subjects.time',
				'subjects.day',
				'subjects.room_id',
				'subjects.subject_taken',
				'subjects.original_load',
				'subjects.year_to',
				'subjects.year_from',
				'subjects.teacher_user_id',
				'rooms.name AS room',
				'users.name AS instructor' 
		);
		
		$get['where'] = $filter;
		$get['like'] = $like;
		$get['not_in'] = array(
				'field' => 'subjects.id',
				'data' => "SELECT subject_id FROM studentsubjects WHERE enrollment_id = ".$this->db->escape($id)." AND is_deleted = 0"
			);
		
		$get['join'] = array(
			
			0 => array(
				"table" => "master_subjects",
				"on"	=> "master_subjects.ref_id = subjects.ref_id",
				"type"  => "LEFT"
			),
			1 => array(
				"table" => "rooms",
				"on"	=> "rooms.id = subjects.room_id",
				"type"  => "LEFT"
			),
			2 => array(
				"table" => "users",
				"on"	=> "users.id = subjects.teacher_user_id",
				"type"  => "LEFT"
			)
		);
		$get['order'] = "master_subjects.subject";
		$get['all'] = true; //GET ALL EXCLUDE LIMIT
		$get['count'] = true; //RETURN COUNT NOT ROW
		$get['array'] = false; //RETURN OBJECT NOT ARRAY
		$get['single'] = false; //RETURN ALL NOT SINGLE
		
	
		$this->load->library("pagination");
		$config = $this->pagination_style();
		$config["base_url"] = site_url() ."fees/add_subject/".$id;
		$suffix = '?'.http_build_query($_GET, '', "&");
		$suffix = str_replace("&submit=Search", "", $suffix);
		$config['suffix'] = $suffix;
		$config['first_url'] = $config['base_url'].$config['suffix']; 
		
		$this->view_data['total_rows'] = $config["total_rows"] = $this->M_core_model->get_record("subjects", $get);
		
		$config["per_page"] = 10;
		$config['num_links'] = 10;
		$config["uri_segment"] = 4;
		// $config['use_page_numbers'] = TRUE;
		$this->pagination->initialize($config);
		
		//FOR PAGINATION
		$get['all'] = false;
		$get['count'] = false;
		
		$config['start'] = $page;
		$config['limit'] = $config['per_page'];
		
		$get['start'] = $page;
		$get['limit'] = $config['per_page'];
		
		$this->view_data['results'] = $search = $this->M_core_model->get_record("subjects", $get);
		$this->view_data['links'] = $this->pagination->create_links();
	}

	/* ADD THE SELECTED SUBJECT TO STUDENTSUBJECT AND RECOMPUTE FEES  */
	public function save_student_subject($id = false, $subject_id = false, $page = 0)
	{
		if($id === false){ show_404(); }
		if($subject_id === false){ show_404(); }

		$this->check_stud_access('stud_edit_subject');

		set_time_limit(0);

		#load model
		$this->load->model('M_subjects');

		#get subject
		$subject = $this->M_subjects->get_profile($subject_id);
		if($subject == false){
			show_404(); #validation
		}

		#load library
		$this->load->library('_student', array('enrollment_id'=>$id));
		$this->view_data['_student'] = $this->_student;
		if($this->_student->profile == false){
			show_404(); #validation
		}

		unset($data);
		$data['enrollment_id'] = $id;
		$data['subject_id'] = $subject_id;
		$data['year_id'] = $this->_student->profile->year_id;
		$data['semester_id'] = $this->_student->profile->semester_id;
		$data['course_id'] = $this->_student->profile->course_id;
		$data['is_active'] = 1;
		$data['enrollmentid'] = $id; 
		$data['is_deleted'] = 0;

		$rs = $this->_student->add_student_subject($data);

		#Goal is to redirec back to the add_subject method together with the search keywords entered by the user
		$suffix = $this->session->userdata('subject_get') ? $this->session->userdata('subject_get') : '';
		$redirect = site_url('fees/add_subject/'.$id."/".$page).$suffix;
		

		if($rs->status){
			$this->_msg('s',$rs->msg, $redirect);
		}else{
			$this->_msg('e',$rs->msg, $redirect);
		}
	}
	
	/*
		END OF FUNCTIONS FOR SUBJECTS
	*/
	
	public function print_fee_pdf($enrollment_id = false,$c = false,$y = false,$s = false)
	{
		$this->session_checker->check_if_alive();
		if($enrollment_id !== false AND ctype_digit($enrollment_id) AND $c !== false AND $y !== false AND $s !==false)
		{
			$this->load->model(array('M_student_totals','M_student_finances','M_student_fees', 'M_student_previous_accounts', 'M_student_subjects', 'M_promissory_notes','M_enrollments'));
			$this->load->helper('print');
			$h = "8";// Hour for time zone goes here e.g. +7 or -4, just remove the + or -
			$hm = $h * 60;
			$ms = $hm * 60;
			$now = time();
			$gmt = local_to_gmt($now);
			$gmdate = $gmt+($ms); // the "-" can be switched to a plus if that's what your time zone is.
			
			$enrollment= $this->M_enrollments->profile($enrollment_id);
			$student_total =  $this->M_student_totals->get_student_total($enrollment_id);
			$student_subjects =  $this->M_student_subjects->get_student_subjects($enrollment_id,$c,$y,$s);
			$studentfinance =  $this->M_student_finances->get_student_finance($enrollment_id);
			$studentfees =  $this->M_student_fees->get_student_fees($studentfinance->id);
			
			// print_html defined at helpers/print_helper.php
			$html = print_html($enrollment,$student_total,$student_subjects,$studentfinance,$studentfees,$gmdate); 
			$this->load->library('mpdf');
		
			$mpdf=new mPDF('','FOLIO','','',5,5,5,5,0,0); 

			$mpdf->WriteHTML($html);

			$mpdf->Output();
		}else{
			show_404();
		}
	}

	public function student_charges($page=0)
	{
		$this->load->Model(array('M_enrollments','M_fees'));
		$this->load->helper('my_dropdown');
	
		$this->load->library("pagination");
		$config = $this->pagination_style();
		$config["base_url"] = base_url() ."fees/student_charges";
		$this->view_data['total_rows'] = $config["total_rows"] = $this->M_enrollments->count_all_updaid_enrollments();
		$config["per_page"] = 30;
		$config['num_links'] = 10;
		$config["uri_segment"] = 3;
		
		$this->pagination->initialize($config);
		$xfilter[' AND e.full_paid <> '] = 1;
		if($this->input->post('search')){}
		$rs = $this->M_enrollments->get_all_updaid_enrollments($config["per_page"], $page, $xfilter);
		
		$data = false;
		$this->view_data['total_payable'] = 0;
		$this->view_data['total_deduction'] = 0;
		$this->view_data['total_amount_paid'] = 0;
		$this->view_data['total_balance'] = 0;
		$this->view_data['total_amount_due'] = 0;
		
		if($rs){
			$this->load->library('_Student',array('enrollment_id'=>0));
			foreach($rs as $k => $obj)
			{
				$this->_student->enrollment_id = $obj->id;
        $this->_student->load_default_function();
				$p = $this->_student->profile;
				if($p){
					$data[$k]['profile'] = $p;
					$data[$k]['fees'] = $this->_student->get_student_fee_profile();
					$data[$k]['total'] = $t = $this->_student->student_payment_totals;

					$this->view_data['total_payable'] += $t->total_amount_due;
					$this->view_data['total_deduction'] += $t->total_deduction_amount;
					$this->view_data['total_amount_paid'] += $t->total_amount_paid;
					$this->view_data['total_balance'] += $t->total_balance;
					$this->view_data['total_amount_due'] += $t->total_amount_due - $t->total_deduction_amount;
				}
			}	
		}

		$this->view_data['enrollments'] = $data;
		$this->view_data['links'] = $this->pagination->create_links();
	}
	
	public function student_charges_x($page=0)
	{//PAGINATION
		
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		
		$filter = false;
		$like = false;
		$order_by = false;
		
		$filter['fees.semester_id'] = $this->open_semester->id;
		$filter['fees.sy_from'] = $this->open_semester->year_from;
		$filter['fees.sy_to'] = $this->open_semester->year_to;
		$filter['fees.is_deleted'] = 0;
		$filter['fees.is_paid'] = 1;
		
		
		$arr_filters = array();
		$suffix = "";
		
		if($_GET)
		{
			if(isset($_GET['lastname']) && trim($_GET['lastname']) != ''){
				$this->view_data['lastname'] = $lastname = trim($_GET['lastname']);
				$like['fees.lastname'] = $lastname;
				$arr_filters['lastname'] = $lastname;
			}
			
			if(isset($_GET['fname']) && trim($_GET['fname']) != ''){
				$this->view_data['fname'] = $fname = trim($_GET['fname']);
				$like['fees.fname'] = $fname;
				$arr_filters['fname'] = $fname;
				
			}
			
			if(isset($_GET['studid']) && trim($_GET['studid']) != ''){
				$this->view_data['studid'] = $studid = trim($_GET['studid']);
				$like['fees.studid'] = $studid;
				$arr_filters['studid'] = $studid;
			}
			
			if(isset($_GET['year_id']) && trim($_GET['year_id']) != ''){
				$this->view_data['year_id'] = $year_id = trim($_GET['year_id']);
				$filter['fees.year_id'] = $year_id;
				$arr_filters['year_id'] = $year_id;
			}
			
			$suffix = array_to_geturl($arr_filters);
		}
		
		//CONFIGURATION
		$get['fields'] = array(
				'fees.id',
				'fees.studid',
				'fees.name' ,
				'years.year',
				'courses.course' 
		);
		
		$get['where'] = $filter;
		$get['like'] = $like;
		
		$get['join'] = array(
			
			1 => array(
				"table" => "courses",
				"on"	=> "courses.id = fees.course_id",
				"type"  => "LEFT"
			),
			2 => array(
				"table" => "years",
				"on"	=> "years.id = fees.year_id",
				"type"  => "LEFT"
			)
		);
		$get['order'] = "fees.name";
		$get['all'] = true; //GET ALL EXCLUDE LIMIT
		$get['count'] = true; //RETURN COUNT NOT ROW
		$get['array'] = false; //RETURN OBJECT NOT ARRAY
		$get['single'] = false; //RETURN ALL NOT SINGLE
		
	
		$this->load->library("pagination");
		$config = $this->pagination_style();
		$config["base_url"] = base_url() ."search/search_student";
		$config['suffix'] = '?'.http_build_query($_GET, '', "&");
		$config['first_url'] = $config['base_url'].$config['suffix']; 
		
		$this->view_data['total_rows'] = $config["total_rows"] = $this->M_core_model->get_record("fees", $get);
		
		$config["per_page"] = 25;
		$config['num_links'] = 10;
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		
		//FOR PAGINATION
		$get['all'] = false;
		$get['count'] = false;
		if($this->input->post('submit') == "Print")
		{
			$get['all'] = true;
		}
		$config['start'] = $page;
		$config['limit'] = $config['per_page'];
		
		$get['start'] = $page;
		$get['limit'] = $config['per_page'];
		
		$this->view_data['search'] = $search = $this->M_core_model->get_record("fees", $get);
		$this->view_data['links'] = $this->pagination->create_links();
		
		if($this->input->post('submit') == "Print")
		{
			$this->print_nstp_students($this->view_data);
		}
	}
	
	public function student_deductions($page = 0)
	{
		$this->load->Model(array('M_enrollments','M_student_deduction_record'));
		
		$this->load->library("pagination");
		$config = $this->pagination_style();
		$config["base_url"] = base_url() ."fees/student_deductions";
		$this->view_data['total_rows'] = $config["total_rows"] = $this->M_student_deduction_record->get_all_students_with_deduction(0,0, false, true,true);
		$config["per_page"] = 30;
		$config['num_links'] = 10;
		$config["uri_segment"] = 3;
		
		$this->pagination->initialize($config);
	
		$rs = $this->M_student_deduction_record->get_all_students_with_deduction($config["per_page"], $page);

		$data = false;
		
		if($rs){
			
			foreach($rs as $obj)
			{
				$data[$obj->studid] = $this->M_student_deduction_record->get_student_deduction_byenrollment_id($obj->enrollment_id); //GET DEDUCTIONs
			}	
		}
		$this->view_data['students'] = $rs; //GROUP BY STUDENTS
		$this->view_data['deductions'] = $data; //DETAILS OF DEDUCTION PER STUDENT
		$this->view_data['links'] = $this->pagination->create_links();
	}
	
	public function recompute($id = false)
	{
		if($id == false){ show_404(); }

		//LOAD STUDENT LIBRARY
		set_time_limit(0);
		$this->load->library('_student', array('enrollment_id'=>$id));

		//VALIDATE
		if($this->_student->profile == false) { show_404(); }

		//CHECK DEPARTMENT STUDENT PROFILE ACCESS 
		$this->check_stud_access('stud_fees');

		$result = $this->_student->recompute_fees();
		
		if($result->status)
		{
			unset($log);
			$log['Enrollment id'] = $id;
			$log['Users Id'] = $this->userid;

			activity_log('Recompute Fees',$this->userlogin,'Data : '.arr_str($log));

			$this->_msg('s',$result->msg,'fees/view_fees/'.$id);	
		}
		else
		{
			$this->_msg('e',$result->msg,'fees/view_fees/'.$id);
		}
	}
	
	public function reasses_fees($enrollment_id = false)
	{
		show_404(); //DEPRECATED
		if($enrollment_id == false){ show_404(); }
		
		$this->load->model(array('M_assign_coursefees','M_student_finances','M_coursefees','M_student_fees',
		'M_student_total_file',
		'M_additional_charges',
		'M_student_previous_accounts',
		'M_student_deductions',
		'M_studentpayments',
		'M_studentpayments_details'));
		
		unset($data);
		$data = $this->generate_fees($enrollment_id); //GET TOTAL FEES
		
		$total['enrollment_id'] = $enrollment_id;
		$total['status'] = 'UNPAID';
		$total['coursefinance_id'] = $data['coursefinance_id'];
		$total['balance'] = $data['total_amount_due'];
		$total['total_charge'] = $data['total_amount_due'];
		$total['total_amount_due'] = $data['total_amount_due'];
		$total['subject_units'] = $data['subject_units'];
		$total['total_lec'] = $data['total_lec'];
		$total['total_lab'] = $data['total_lab'];
		$total['tuition_fee'] = $data['tuition_fee'];
		$total['total_tuition_fee'] = $data['tuition_fee'] * $data['subject_units'];
		// vd($data);
		
		$additional_charge_amount = 0;
		$previous_account_amount = 0;
		$less_deduction_amount = 0;
		$student_payment_amount = 0;
		
		#RECONSIDER ADDITIONAL CHARGES
		$additional_charge = $this->M_additional_charges->get_additional_charge($enrollment_id);
		
		if($additional_charge)
		{
			foreach($additional_charge as $obj){
				$additional_charge_amount += $obj->value;
			}
		}
		
		#endregion of ADDITIONAL CHARGES
		
		#RECONSIDER PREVIOUS ACCOUNT
		$previous_account = $this->M_student_previous_accounts->get_student_previous_accounts($enrollment_id);
		if($previous_account){
			foreach($previous_account as $obj){
				$previous_account_amount += $obj->value;
			}
		}
		#ENDREGION FOR PREVIOUS ACCOUNT
		
		#RECONSIDER LESS DEDUCTION
		$less_deduction = $this->M_student_deductions->get_student_deduction_byenrollment_id($enrollment_id);
		if($less_deduction){
			foreach($less_deduction as $obj){
				$less_deduction_amount += $obj->amount;
			}
		}
		#ENDREGION FOR LESS DEDUCTION
		
		#RECONSIDER STUDENT PAYMENTS
		$student_payment = $this->M_studentpayments->get_payments_enrollment_id($enrollment_id);
		if($student_payment){
			foreach($student_payment as $obj){
				$student_payment_amount += $obj->total;
			}
		}
		#ENDREGION FOR STUDENT PAYMENTS
		
		//CHARGES
		$total['additional_charge'] = $additional_charge_amount;
		$total['prev_account'] = $previous_account_amount;
		
		//DEDUCTIONS
		$total['less_deduction'] = $less_deduction_amount;
		$total['total_payment'] = $student_payment_amount;
		$total['total_deduction'] = $total['less_deduction'] + $total['total_payment'];
		
		//TOTALS
		$total['total_charge'] += $additional_charge_amount; //ADD ADDITIONAL CHARGE
		$total['total_amount_due'] += $additional_charge_amount + $previous_account_amount; //ADD ADDITIONAL CHARGE & PREVIOUS ACCOUNTS AMOUNT
		$total['balance'] += $additional_charge_amount + $previous_account_amount; //ADD ADDITIONAL CHARGE & PREVIOUS ACCOUNTS AMOUNT
		
		//SUBTRACT DEDUCTION
		$total['balance'] -= $total['total_deduction'];
		
		//DELETE PREVIOUS RECORD AND ADD
		$this->M_student_total_file->delete_student_total_file($enrollment_id);
		
		$result = $this->M_student_total_file->insert($total);
		
		if($result['status'])
		{
			//UPDATE ENROLLMENT TOTAL IF PAID, FULL , PARTIAL AND UPDATE SUBJECT LOADS
			$this->M_student_total_file->update_status($enrollment_id);
			
			//RECALCULATE STUDENT PAYMENT DETAILS
			$this->M_studentpayments_details->recalculate_studentpayment_details($enrollment_id);
			
			//UPDATE ENROLLMENT IF PAID, FULL , PARTIAL AND UPDATE SUBJECT LOADS
			$this->M_enrollments->update_enrollments_status($enrollment_id); 
			
			activity_log('Re-assess fee',$this->user,'Re-assess fee by: '.$this->user.'Enrollment id: '.$enrollment_id);
			
			log_message('success','Re-assess fee by: '.$this->user.'Enrollment id: '.$enrollment_id);
			$this->session->set_flashdata('system_message', '<div class="alert alert-success">Successfully Re-assessed fee.</div>');
		}
		else
		{
			$this->session->set_flashdata('system_message', '<div class="alert alert-success">Re-assessed fail, please try again or contact your school administrator.</div>');
		}
		
		redirect("fees/view_fees/".$enrollment_id);
	}
	
	private function generate_fees($id = false)
	{
		
		if($id == false) { show_404(); }
		
		$this->load->model(array(
		'M_enrollments','M_student_finances','M_student_fees', 'M_student_previous_accounts', 'M_student_subjects', 'M_student_deductions','M_coursefees','M_assign_coursefees',
		'M_studentpayments', 'M_additional_charges',
		'M_course_blocks','M_block_subjects','M_subjects','M_coursefinances'));
		
		//GET SUBJECTS ENROLLED
		$this->view_data['temp_subjects'] =  $temp_subjects = $this->M_student_subjects->get_studentsubjects($id);
		$this->view_data['fees'] =  $fees = $this->M_enrollments->get($id);
		
		if($temp_subjects == false)
		{
			$this->session->set_flashdata('system_message', '<div class="alert alert-success">This student doesn\'t have subject.</div>');
			redirect("fees/view_fees/".$id);
		}
		
		
		//COMPUTE FEES
		$data[] = $fees->year_id;
		$data[] = $fees->course_id;
		$data[] = $this->open_semester->id;
		$data[] = $this->open_semester->academic_year_id;		
		$assigned_course_fee = $this->M_assign_coursefees->get_assign_course_fee($data);
		
		if($assigned_course_fee)
		{
			$this->view_data['coursefinance_id'] = $coursefinance_id = $assigned_course_fee->coursefinance_id;
			$this->view_data['coursefinances'] = $this->M_coursefinances->get_coursefinances($coursefinance_id);
			
			$total_amount_due = 0;
			$subjects_units = 0;
			$total_lec = 0;
			$total_lab = 0;
			$has_nstp = 0;
			
			//LOOP SUBJECTS TO GET TOTAL NUMBER OF UNIT LAB AND LEC
			foreach($temp_subjects as $subject)
			{
				if($subject)
				{
					$subjects_units += $subject->units;
					$total_lec += $subject->lec;
					$total_lab += $subject->lab;
					if($subject->is_nstp == 1)
					{
						$has_nstp++;
					}
				}
			}
			
			$this->view_data['subject_units'] = $subjects_units;
			$this->view_data['subject_total'] = count($temp_subjects);
			$this->view_data['total_lec'] = $total_lec;
			$this->view_data['total_lab'] = $total_lab;
			
			$this->view_data['has_nstp'] = $has_nstp;
			
			#region LOOP COURSE FEES
			$this->view_data['course_fees'] =  $course_fees = $this->M_coursefees->get_all_course_fees($coursefinance_id);
			
			if($course_fees)
			{
				$student_fees = array();
				
				foreach($course_fees as $cf)
				{
					if($cf->is_tuition_fee == 1)
					{
						$student_fees['tuition_fee']['data'] = $cf;
						$student_fees['tuition_fee']['total'] = $total_tuition = $subjects_units * $cf->value;
						$total_amount_due += $total_tuition;
						$this->view_data['tuition_fee'] = $cf->value;
					}
					else if($cf->is_lab == 1)
					{
						$student_fees['lab'][$cf->id]['data'] = $cf;
						$student_fees['lab'][$cf->id]['value'] = $cf->value * $total_lab;
						$total_amount_due += $cf->value * $total_lab;
					}
					else if($cf->is_misc == 1)
					{
						$student_fees['misc'][] = $cf;
						$total_amount_due += $cf->value;
					}
					else if($cf->is_other == 1)
					{
						$student_fees['other'][] = $cf;
						$total_amount_due += $cf->value;
					}
					else if($cf->is_nstp == 1)
					{
						$student_fees['nstp'][] = $cf;
						$total_amount_due += $cf->value;
					}
					else{}
				}
				$this->view_data['student_fees'] = $student_fees;
			}
			
			$this->view_data['total_amount_due'] = $total_amount_due;
			#endregion course fees
			
			
			return $this->view_data;
		}
		else
		{
			$this->session->set_flashdata('system_message', '<div class="alert alert-danger">Course fee was not assigned, please contact your school administrator.</div>');
			redirect("fees/view_fees/".$id);
		}
	}
	
	
	private function generate_fees_old($id = false)
	{
		show_404(); //WAS CHANGED
		if($id == false) { show_404(); }
		
		$this->load->model(array(
		'M_enrollments','M_student_finances','M_student_fees', 'M_student_previous_accounts', 'M_student_subjects', 'M_student_deductions','M_coursefees','M_assign_coursefees',
		'M_studentpayments', 'M_additional_charges',
		'M_course_blocks','M_block_subjects','M_temp_student_subjects',
		'M_temp_start','M_subjects','M_coursefinances'));
		
		$this->view_data['start'] = $start = $this->M_temp_start->get($id);
		$this->view_data['temp_subjects'] =  $temp_subjects = $this->M_temp_student_subjects->get_temp_student_subjects($id);
		
		if($temp_subjects == false)
		{
			//RETURN TO START
			redirect('registration');
		}
		
		#region GET SUBJECTS PROFILES
		$subjects = array();
		foreach($temp_subjects as $obj)
		{
			$subjects[] = $this->M_subjects->get($obj->id);
		}
		
		$this->view_data['subjects'] = $subjects;
		
		#endregion
		
		$this->view_data['open_enrollment'] = $open = $this->M_open_semesters->get_ay_sem();
		$this->view_data['id'] = $id;
		$this->view_data['student'] = $this->student;
		$this->view_data['process'] = "ASSESSMENT";
		$this->view_data['disable_lasten'] = true;
		$this->view_data['block_system_settings'] = $block_system_settings = $this->M_temp_student_subjects->get_block_system_setting_id($id);
		
		
		//COMPUTE FEES AND DISPLAY
		$data[] = $start->year_id;
		$data[] = $start->course_id;
		$data[] = $this->open_semester->semester_id;
		$data[] = $this->open_semester->academic_year_id;		
		$assigned_course_fee = $this->M_assign_coursefees->get_assign_course_fee($data);
		
		if($assigned_course_fee)
		{
			$this->view_data['coursefinance_id'] = $coursefinance_id = $assigned_course_fee->coursefinance_id;
			$this->view_data['coursefinances'] = $this->M_coursefinances->get_coursefinances($coursefinance_id);
			
			$total_amount_due = 0;
			$subjects_units = 0;
			$total_lec = 0;
			$total_lab = 0;
			$has_nstp = 0;
			
			//LOOP SUBJECTS TO GET TOTAL NUMBER OF UNIT LAB AND LEC
			foreach($temp_subjects as $sub)
			{
				$subject = $this->M_subjects->get($sub->id);
				if($subject)
				{
					$subjects_units += $subject->units;
					$total_lec += $subject->lec;
					$total_lab += $subject->lab;
					if($subject->is_nstp == 1)
					{
						$has_nstp++;
					}
				}
			}
			
			$this->view_data['subject_units'] = $subjects_units;
			$this->view_data['subject_total'] = count($temp_subjects);
			$this->view_data['total_lec'] = $total_lec;
			$this->view_data['total_lab'] = $total_lab;
			
			$this->view_data['has_nstp'] = $has_nstp;
			
			#region LOOP COURSE FEES
			$this->view_data['course_fees'] =  $course_fees = $this->M_coursefees->get_all_course_fees($coursefinance_id);
			
			if($course_fees)
			{
				$student_fees = array();
				
				foreach($course_fees as $cf)
				{
					if($cf->is_tuition_fee == 1)
					{
						$student_fees['tuition_fee']['data'] = $cf;
						$student_fees['tuition_fee']['total'] = $total_tuition = $subjects_units * $cf->value;
						$total_amount_due += $total_tuition;
					}
					else if($cf->is_misc == 1)
					{
						$student_fees['misc'][] = $cf;
						$total_amount_due += $cf->value;
					}
					else if($cf->is_other == 1)
					{
						$student_fees['other'][] = $cf;
						$total_amount_due += $cf->value;
					}
					else if($cf->is_nstp == 1)
					{
						$student_fees['nstp'][] = $cf;
						$total_amount_due += $cf->value;
					}
					else{}
				}
				$this->view_data['student_fees'] = $student_fees;
			}
			$this->view_data['total_amount_due'] = $total_amount_due;
			#endregion course fees
			
			
			return $this->view_data;
		}
		else
		{
			$this->session->set_flashdata('system_message', '<div class="alert alert-danger">Course fee was not assigned, please contact your school administrator.</div>');
			redirect("fees/view_fees/".$enrollment_id);
		}
	}
	
	public function view_fees_pdf($enrollment_id = false)
	{
			if($enrollment_id == false){show_404();}
			
			$this->load->model(array('M_enrollments','M_student_finances','M_student_fees', 'M_student_previous_accounts', 'M_student_subjects', 'M_student_deductions','M_coursefees','M_assign_coursefees','M_studentpayments', 'M_additional_charges'));
			
			// Top header data of student
			$this->view_data['student'] = $p = $this->M_enrollments->profile($enrollment_id);
			$this->view_data['enrollment_id'] = $enrollment_id;
			
			// Count Issues
			$this->issue_count($p->user_id);
			
			// Used for layouts/student_data
			$this->view_data['subjects']	 = $subjects =  $this->M_student_subjects->get_studentsubjects($enrollment_id);
			$this->view_data['subject_units'] = $subject_units = $this->M_student_subjects->get_studentsubjects_total($subjects);
			
			$this->view_data['student_profile'] = $this->view_data['student'] = $p = $this->M_enrollments->profile($enrollment_id); // student profile
			
			$this->view_data['student_finance'] =  $this->M_student_finances->get_student_finance($enrollment_id);
			
			// Previous Account
			$this->view_data['previous'] =  $this->M_student_previous_accounts->get_student_previous_accounts($enrollment_id);
			
			// Additional Charge
			$act = 0.0;
			$this->view_data['additional_charge'] =  $ac = $this->M_additional_charges->get_additional_charge($enrollment_id);
			$this->view_data['additional_charge_total'] =  $additional_charge_total = $this->M_additional_charges->get_additional_charge_total($enrollment_id);
			if ($additional_charge_total) {
			  $act += $additional_charge_total;
			}
			
			
			
			if(!empty($this->view_data['student_finance']))
			{
				// for total payment variables
				$totalprevious_account = 0.00;
				$additional_charge = 0.00;
				
				if ($ac) {
				  $additional_charge += $ac->value;
				}
				
				$previous = $this->M_student_previous_accounts->get_student_previous_accounts($enrollment_id);
				if ($previous) {
				  $totalprevious_account +=  $previous->value;
				}
				
				//Get course Finance
				$enrollee = $this->M_enrollments->profile($enrollment_id);
				$data[] = $enrollee->year_id;
				$data[] = $enrollee->course_id;
				$data[] = $enrollee->sem_id;
				$data[] = $this->open_semester->academic_year_id;
				$this->view_data["assigned_course_fee"] = $assigned_course_fee = $this->M_assign_coursefees->get_assign_course_fee($data);
				
				if(!empty($assigned_course_fee)){
				$studentfinance['coursefinance_id'] = $assigned_course_fee->coursefinance_id;
				$studentfinance['student_id']  = $enrollee->user_id;
				$studentfinance['enrollmentid']  = $enrollment_id;
				$studentfinance['year_id'] = $enrollee->year_id;
				$studentfinance['semester_id'] = $enrollee->sem_id;
				$studentfinance['course_id'] = $enrollee->course_id;
		
				$this->M_student_finances->add_student_finance($studentfinance);
				$this->view_data['student_finance'] =  $this->M_student_finances->get_student_finance($enrollment_id);
				//$studfin = $this->M_student_finances->get_student_finance($enrollment_id);
				$studfin = $this->M_student_finances->get_student_fees($enrollment_id);
				//vd($studfin);
				}
			
				$nstp_exist = 0;
				$lab_exist = 0;
				$lec_exist = 0;
				$bsba_units = 0;
				$lab1_units = 0;
				$lab2_units = 0;
		    
		    if (!empty($subjects)){
				foreach($subjects as $k => $s):
				  
					if(preg_match("/^nstp/", strtolower($s->code))){
						$nstp_exist++;
					}
					
					if($s->lab_kind == "BSBA"){
						$bsba_units += $s->lab;
					}
					
					if($s->lab_kind == "LAB1"){
						$lab1_units += $s->lab;
					}
					
					if($s->lab_kind == "LAB2"){
						$lab2_units += $s->lab;
					}
				endforeach;
				}
				
					
				$this->view_data["nstp"] = $nstp_exist;
				$student_total_units = $subject_units['total_units'];
				//End Student Subject Units
		
				// Loop through Coursefees
				//Temp-Variable
				$tuition_fee = 0.0;
				$lab_fee = 0.0;
				$rle_fee = 0.0;
				$misc_fee = 0.0;
				$other_fee = 0.0;
				$lab_a_fee = 0.0;
				$lab_b_fee = 0.0;
				$lab_c_fee = 0.0;
				$bsba_fee = 0.0;
				$lab1_fee = 0.0;
				$lab2_fee = 0.0;
				$totalmisc = 0.0;
		
		
				//$coursefees = $this->M_coursefees->get_all_course_fees($assigned_course_fee->coursefinance_id);
				if(!empty($studfin)){
				foreach($studfin as $cf):
	      
					if( strtolower($cf->name) == "tuition fee per unit" )
					{
						$studentfee['fee_id'] = $cf->fee_id;
						$tuition_fee += $cf->value;
					}
	      
					if( strtolower($cf->name) == "laboratory fee per unit" )
					{
						$studentfee['fee_id'] = $cf->fee_id;
						$lab_fee += $cf->value;
					}
					
					if (preg_match("/bsba laboratory/",$cf->name)){
	          $bsba_fee += $cf->value;
	        }
	        
	        if (preg_match("/laboratory 1/",$cf->name)){
	          $lab1_fee += $cf->value;
	        }
	        
	        if (preg_match("/laboratory 2/",$cf->name)){
	          $lab2_fee += $cf->value;
	        }
					
					
	      
					if( $cf->is_misc == 1 )
					{
						$misc_fee += $cf->value;
						$fees['misc'][] = $cf;
					}
	      
					if( $cf->is_other == 1 )
					{
						$other_fee += $cf->value;
						$fees['other'][] = $cf;
					}
	      
					$studentfee['fee_id'] = $cf->fee_id;
					$studentfee['value'] = $cf->value;
					$studentfee['position'] = $cf->position;
	        
					#$this->M_student_fees->insert_student_fees($studentfee);
				endforeach;
				}
				if (!empty($fees)) {
				  $this->view_data['stud_fees'] = $fees;
				}
			
			  $this->view_data['lab_a'] = $bsba_units;
			  $this->view_data['lab_b'] = $lab1_units;
			  $this->view_data['lab_c'] = $lab2_units;
			
			  $this->view_data['lab_a_fee'] = $bsba_fee;
			  $this->view_data['lab_b_fee'] = $lab1_fee;
			  $this->view_data['lab_c_fee'] = $lab2_fee;
			
			  $this->view_data['lab_a_fee_total'] = $lab1_total_fee = $bsba_units*198.00;
			  $this->view_data['lab_b_fee_total'] = $lab2_total_fee = $lab1_units*263.00;
			  $this->view_data['lab_c_fee_total'] = $lab3_total_fee = $lab2_units*318.00;
			  $total_custom_lab = $lab1_total_fee + $lab2_total_fee + $lab3_total_fee;
				
				
		
				# Computations for student total
				$student_total['enrollment_id'] = $enrollment_id;
				$student_total['total_units'] = $student_total_units;
				$student_total['tuition_fee_per_unit'] = $tuition_fee;
				$student_total['lab_fee_per_unit'] = $lab_fee;
				$student_total['total_misc_fee'] = $misc_fee;
				$student_total['total_other_fee'] = $other_fee;
				$student_total['additional_charge'] = $additional_charge;
				$student_total['previous_account'] = $totalprevious_account;
    
    
				$total_tuition_fee = $tuition_fee*$student_total_units;
				$total_misc = $misc_fee;
				$total_other = $other_fee;
				//End Computations
	  
				//Save Computation Totals
	  
				//Tuition Fee
				$student_total['tuition_fee'] = $total_tuition_fee;
				//RLE Fee
				
				$total_stud_payment = $this->M_studentpayments->get_sum_student_payments($enrollment_id);
                                $last_payment = $this->M_studentpayments->get_last_payment_enrollment_id($enrollment_id);
				$total_stud_dec = $this->M_student_deductions->get_sum_of_deductions($enrollment_id);
				if($total_stud_payment->account_recivable_student != '')
				{
					$student_total['total_payment'] = $total_stud_payment->account_recivable_student;
					if(!empty($last_payment)){
						$student_total['or_no'] = $last_payment->or_no;
						$student_total['payment_date'] = $last_payment->date;
					}
				}
				else
				{
					$student_total['total_payment'] = 0.00;
					$student_total['or_no'] = '';
					$student_total['payment_date'] = '';
				}
				if($total_stud_dec->amount != '')
				{
					$student_total['total_deduction'] = $total_stud_dec->amount;
				}
				else
				{
					$student_total['total_deduction'] = 0.00;
				}
				
				$student_total["total_student_deduction"] = $total_stud_payment->account_recivable_student + $total_stud_dec->amount;
    
    
				# Total Charge
				$student_total['total_charge'] = $total_charge = $total_tuition_fee  + $misc_fee + $other_fee +$total_custom_lab + $act;
				
				# Total Payable/ Amount
				$student_total['total_amount'] = $student_total['total_charge'] + $totalprevious_account;
				
				# Calculate Remaining Balance
				$student_total['remaining_balance'] = $student_total['total_amount'] - ($total_stud_dec->amount + $total_stud_payment->account_recivable_student);
				
				
				$this->view_data['student_total'] = $student_total;

				# End Save Computation Totals
			}
			
			
			$this->view_data['total_deductions'] = $this->M_student_deductions->sum_of_deductions($enrollment_id);
			$studentfinance =  $this->M_student_finances->get_student_finance($enrollment_id);
			if (!empty($studentfinance)) {
			$this->view_data['sfid'] = $studentfinance->id;		
			$this->view_data['studentfees'] =  $this->M_student_fees->get_student_fees($studentfinance->id);
			
			$student_fees = $this->M_student_fees->get_student_fees($studentfinance->id);
			}
			
			
			
			/* get student subjects where student course_id,sem_id,year_id equal to subjects*/
			$student_subjects = $this->M_student_subjects->get_student_subjects($enrollment_id,$p->course_id,$p->year_id,$p->sem_id);
			$this->view_data['student_subjects'] =  $student_subjects;
			
			
			
			
			$this->view_data['course'] = $p->course_id; 
			$this->view_data['year'] = $p->year_id;
			$this->view_data['semester'] = $p->sem_id;
			$this->view_data['enrollment_id'] = $p->id;
			
			
			

			//Variable For Printing
			$data["enrollment"] = $p;
                        $data["studentsubjects"] = $subjects ;
			$data["subject_units"] = $subject_units;
			$data["misc_fees"] = $fees["misc"]; 
			$data["total_tuition_fee"] = $total_tuition_fee;
			$data["total_lab_fee"] = $total_custom_lab ;
			$data["total_misc_fee"] = $misc_fee ; 
                        $data["total_other_fee"] = $other_fee; 
                        $data["previous_account"] = $totalprevious_account ;
			$data["total_charge"] = $total_charge ;
			$data["payment_category"] = $studentfinance->category2 ;
                        $data["total_student_payment"] = $student_total['total_payment'];
                        $data['or_no'] = $student_total['or_no'] ;
			$data['payment_date'] = $student_total['payment_date'];
			$data["payment_division"] = ($total_charge - 3200) / 3;  
			$this->load->library('mpdf');
	                $html = $this->load->view('printables/view_fees_pdf', $data, true);	
			$mpdf=new mPDF('','FOLIO','','',5,5,5,5,0,5); 
                        $mpdf->SetHTMLFooter('
                        
			<div id="footer">
			<table id="signatory">
			 <tr>
			   <td colspan=4><b>CONFORME:</b></td>
			 </tr>
			 <tr>
			   <td>STUDENT\'S SIGNATURE: </td>
			   <td colspan=3>_______________________________________________________________________</td>
			 </tr>
			 <tr>
			   <td>Subjects encoded by:</td>
			   <td>&nbsp;</td>
			   <td>Student load verified & confirmed by:</td>
			   <td>____________________________________________________</td>
			 </tr>
			 <tr>
			   <td colspan=3>&nbsp;</td>
			   <td align=center>Signature of Authorized Personnel</td>
			 </tr>
			</table>
			</div>
			');
			$mpdf->WriteHTML($html);

			$mpdf->Output();
			//Ajax Subject
			$this->view_data['eid'] = $p->id;
		  $this->view_data['y'] = $p->year_id;
		  $this->view_data['c'] = $p->course_id;
		  $this->view_data['s'] = $p->semester_id;
	}
	
	public function add_other_fees($eid = false)
	{
		
		if($eid == false) { show_404(); }

		//LOAD MODELS
		$this->load->model('M_enrollments2','m_en2');
		$this->load->model('M_enrollments','m_en');
		$this->load->model('M_fees2','m_f');

		//LOAD VARIABLES
		$this->view_data['enrollment_id'] = $eid;
		$this->view_data['system_message'] = $this->session->flashdata('system_message');

		//GET NECCESSARY DATA
		$x_enroll = $this->m_en2->pull($eid, array('id'));

		if($x_enroll)
		{
			//GET STUDENT ENROLLMENT FEES (OTHER FEES)
			$this->view_data['other_fees'] = $this->m_f->get_other_fees($eid);

			//SAVING
			if($this->input->post('add_other_fee')){
				set_time_limit(0);

				//LOAD STUDENT LIBRARY
				$this->load->library('_student', array('enrollment_id'=>$eid));

				$other_fee = $this->input->post('other_fee');

				if(isset($other_fee) && is_array($other_fee)){
					foreach ($other_fee as $key => $fee_id) {
						$this->_student->add_other_fee($fee_id);
					}

					$this->_msg('s','Other fee/s were successfully added','fees/view_fees/'.$eid);
				}else{
					$this->_msg('e','No Other Fee Selected','fees/add_other_fees/'.$eid);
				}
			}
		}
		else
		{
			$this->_msg('e','Enrollment does not exist','home');
		}
	}
	
	//DESTROY STUDENT ENROLLMENT OTHER FEES
	public function destroy_other_fee($eid = false, $sef_id = false)
	{
		//VALIDATE
		if($eid == false){ show_404(); }
		if($sef_id == false){ show_404(); }

		//LOAD MODELS
		$this->load->model('M_enrollments2','m_en2');
		$this->load->model('M_student_enrollment_fees','m_sef');
		$this->load->model('M_fees2','m_f');

		//LOAD VARIABLES
		$this->view_data['enrollment_id'] = $eid;
		$this->view_data['system_message'] = $this->session->flashdata('system_message');

		//GET NECCESSARY DATA
		$x_enroll = $this->m_en2->pull($eid, array('id'));
		$x_sef = $this->m_sef->pull($sef_id, array('sef_id','sef_fee_name','sef_fee_rate'));

		if($x_enroll && $x_sef)
		{
			if($x_sef->amount_paid <= 0)
			{
				//LOAD STUDENT LIBRARY
				$this->load->library('_student', array('enrollment_id'=>$eid));

				$x_d = $this->_student->delete_other_fee($sef_id);
				if($x_d){
					$this->_msg('s',$x_sef->sef_fee_name.' was successfully removed.','fees/view_fees/'.$eid);		
				}else{
					$this->_msg('e','Process Failed. Please try again.','fees/view_fees/'.$eid);		
				}
			}
			else
			{
				$this->_msg('e','Cannot delete this fee because payments was already made.','fees/view_fees/'.$eid);
			}
		}
		else
		{
			$this->_msg('e','Process Failed. Please try again.','home');
		}
	}

	//////////////////////////SYLVER POGI///////////////////////////////////DEDUCTION FUNCTIONS ///////////////////////////////////////////////////////////////

	//ADD DEDUCTION FEES 
	public function add_deduction_fees($eid = false)
	{
		
		if($eid == false) { show_404(); }
		
		//LOAD MODELS
		$this->load->model('M_enrollments2','m_en2');

		//LOAD VARIABLES
		$this->view_data['enrollment_id'] = $eid;
		$this->view_data['system_message'] = $this->session->flashdata('system_message');

		//LOAD LIBRARY
		$this->load->library('form_validation');

		//GET NECCESSARY DATA
		$x_enroll = $this->m_en2->pull($eid, array('id'));

		if($x_enroll)
		{
			//SAVING
			if($this->input->post('add_deduction') || $this->input->post('add_deduction_add')){

				set_time_limit(0);

				if($this->form_validation->run('student_deduction_record') === FALSE){
					
				}else
				{

					//LOAD STUDENT LIBRARY
					$this->load->library('_student', array('enrollment_id'=>$eid));
					unset($data);
					$data['enrollment_id'] = $eid;
					$data['deduction_name'] = $this->input->post('deduction_name');
					$data['remarks'] = $this->input->post('remarks');
					$data['amount'] = $this->input->post('amount');

					$x_add = $this->_student->add_deduction_fee($data);

					if($x_add)
					{
						if($this->input->post('add_deduction'))
						{
							$this->_msg('s','Other fee/s were successfully added','fees/view_fees/'.$eid); #Save and exit
						}else
						{
							$this->_msg('s','Other fee/s were successfully added','fees/add_deduction_fees/'.$eid); #Save and add another
						}
					}else{
						$this->_msg('e','Process failed to finish. Please try again','fees/add_deduction_fees/'.$eid);
					}
				}
			}
		}
		else
		{
			$this->_msg('e','Enrollment does not exist','home');
		}
	}

	//DESTROY STUDENT DEDUCTION RECORD
	public function destroy_deduction($eid = false, $sdr_id = false)
	{
		//VALIDATE
		if($eid == false){ show_404(); }
		if($sdr_id == false){ show_404(); }

		//LOAD MODELS
		$this->load->model('M_enrollments2','m_en2');
		$this->load->model('M_student_deduction_record','m_sdr');

		//LOAD VARIABLES
		$this->view_data['enrollment_id'] = $eid;
		$this->view_data['system_message'] = $this->session->flashdata('system_message');

		//GET NECCESSARY DATA
		$x_enroll = $this->m_en2->pull($eid, array('id'));
		$x_sdr = $this->m_sdr->pull($sdr_id, array('id','deduction_name','amount'));

		if($x_enroll && $x_sdr)
		{
			//LOAD STUDENT LIBRARY
			$this->load->library('_student', array('enrollment_id'=>$eid));

			$x_d = $this->_student->delete_deduction_record($sdr_id);
			if($x_d){
				$this->_student->recompute_fees('Delete Deduction Fees');
				$this->_msg('s',$x_sdr->deduction_name.' was successfully removed.','fees/view_fees/'.$eid);		
			}else{
				$this->_msg('e','Process Failed. Please try again.','fees/view_fees/'.$eid);		
			}
		}
		else
		{
			$this->_msg('e','Process Failed. Please try again.','home');
		}
	}

	//ADD SCHOLARSHIP
	public function add_student_scholarship($eid = false)
	{
		
		if($eid == false) { show_404(); }
		
		//LOAD MODELS
		$this->load->model('M_enrollments2','m_en2');
		$this->load->model('M_student_scholarship','m_ss');
		$this->load->model('M_student_scholarship_record','m_ssr');

		//LOAD VARIABLES
		$this->view_data['enrollment_id'] = $eid;
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		$this->view_data['student_scholarship'] = $this->m_ss->get_student_scholarship($eid);
		
		//GET NECCESSARY DATA
		$x_enroll = $this->m_en2->pull($eid, array('id'));
		
		if($x_enroll)
		{
			//SAVING
			if($this->input->post('add_scholarship')){

				set_time_limit(0);

				$scholarships = $this->input->post('scholarship');

				if(isset($scholarships) && is_array($scholarships))
				{
					//LOAD STUDENT LIBRARY
					$this->load->library('_student', array('enrollment_id'=>$eid));

					$ctr = 0;
					foreach ($scholarships as $key => $scholarship_id) {
						
						$scho = $this->m_ss->get($scholarship_id);
						if($scho){
							unset($data);
							$data['enrollment_id'] = $eid;
							$data['stud_e_id'] = $eid;
							$data['scholarship_id'] = $scholarship_id;
							$data['scholarship_name'] = $scho->name;
							$data['scholarship_desc'] = $scho->desc;
							$data['academic_year_id'] = $this->open_semester->academic_year_id;

							//GET SCHOLARSHIP AMOUNT
							if($scho->type == 'CASH'){
								$data['scho_amount'] = $scho->cash_amount;
							}else if($scho->type == 'PERCENTAGE'){

								$applied_to_amount = 0;
								if($scho->applied_to == "TUITION"){
									$applied_to_amount = $this->_student->profile->tuition_fee;
								}else if($scho->applied_to == 'MISC'){
									$applied_to_amount = $this->_student->profile->misc_fee;
								}else if($scho->applied_to == 'TUITION & MISC'){
									$applied_to_amount = $this->_student->profile->misc_fee + $this->_student->profile->tuition_fee;
								}else{}

							
								if($scho->percentage > 0){
									$data['scho_amount'] = round(($applied_to_amount / 100) * $scho->percentage,2);
								}

							}else{}
							
							

							$x_add = $this->_student->add_student_scholarship($data);
							if($x_add->status){ $ctr++; }
						}
					}

					if($ctr > 0)
					{
						$this->_msg('s','Scholarship/s were successfully added','fees/view_fees/'.$eid);
					}else{
						$this->_msg('e','Saving Failed. Please try again.','fees/add_student_scholarship/'.$eid);
					}
				}
				else
				{
					$this->_msg('e','No Scholarship Selected','fees/add_student_scholarship/'.$eid);
				}
			}
		}
		else
		{
			$this->_msg('e','Enrollment does not exist','home');
		}
	}
	
	//DESTROY STUDENT SCHOLARSHIP RECORD
	public function destroy_scholarship($eid = false, $ssr_id = false)
	{
		//VALIDATE
		if($eid == false){ show_404(); }
		if($ssr_id == false){ show_404(); }

		//LOAD MODELS
		$this->load->model('M_enrollments2','m_en2');
		$this->load->model('M_student_scholarship_record','m_ssr');

		//LOAD VARIABLES
		$this->view_data['enrollment_id'] = $eid;
		$this->view_data['system_message'] = $this->session->flashdata('system_message');

		//GET NECCESSARY DATA
		$x_enroll = $this->m_en2->pull($eid, array('id'));
		$x_ssr = $this->m_ssr->pull($ssr_id, array('id','scholarship_name','scho_amount'));

		if($x_enroll && $x_ssr)
		{
			//LOAD STUDENT LIBRARY
			$this->load->library('_student', array('enrollment_id'=>$eid));

			$x_d = $this->_student->delete_scholarship_record($ssr_id);
			if($x_d){
				$this->_student->recompute_fees('Destroy Scholarship');
				$this->_msg('s',$x_ssr->scholarship_name.' was successfully removed.','fees/view_fees/'.$eid);		
			}else{
				$this->_msg('e','Process Failed. Please try again.','fees/view_fees/'.$eid);		
			}
		}
		else
		{
			$this->_msg('e','Process Failed. Please try again.','home');
		}
	}

	//ADD STUDENT PAYMENT FROM EXCESS ACCOUNT OF PREVIOUS ENROLLMENTS
	public function add_excess_payment($eid = false)
	{
		if($eid === false){ show_404(); }

		//load models
		$this->load->model('M_studentexcess_prev_sem','m_sps');

		#load student library
		$this->load->library('_student', array('enrollment_id'=>$eid));
		$this->view_data['_student'] = $this->_student;
		if($this->_student->profile === false){ show_404(); }

		$this->view_data['account_with_excess'] = $this->_student->get_accounts_with_excess();

		if($this->input->post('add_excess_accounts') && $this->input->post('add_excess_accounts') == "Add Selected As Payments"){

			$selected_ids = $this->input->post('excess');

			$ctr = 0;

			if(is_array($selected_ids) && count($selected_ids) > 0){

				set_time_limit(0);

				foreach ($selected_ids as $key_id => $en_id) {
					
					unset($get);
					$get['enrollment_id'] = $eid;
					$get['prev_sem_eid'] = $key_id;
					$get['is_deleted'] = 0;
					$already_added = $this->m_sps->pull($get, array('id'));
					if($already_added === false){

						$rs = $this->_student->add_excess_payments($key_id);
						
						if($rs->status){
							$ctr++;
						}
					}
				}

				if($ctr > 0){
					$this->_msg('s','Student excess payments was successfully added to student current fees as payment.','fees/view_fees/'.$eid);
				}else{
					$this->_msg('e','Excess payments failed to be process. Please try again.',current_url());	
				}

			}else{
				$this->_msg('e','No Excess Accounts Selected',current_url());
			}
		}
	}

	//DESTROY EXCESS PAYMENTS
	public function destroy_excess_payment($eid = false, $id = false)
	{
		if($eid === false || $id === false){ show_404(); }

		//load library
		$this->load->model('M_studentexcess_prev_sem','m_sps');

		//Validate if record exist
		$sps = $this->m_sps->pull($id, array('id'));

		if($sps === false) { show_404(); }

		//LOAD STUDENT LIBRARY
		$this->load->library('_student', array('enrollment_id'=>$eid));

		$x_d = $this->_student->delete_excess_payments($id);
		if($x_d){
			$this->_student->recompute_fees('Destroy Excess/Advanced Payment');
			$this->_msg('s',$x_ssr->scholarship_name.' was successfully removed.','fees/view_fees/'.$eid);		
		}else{
			$this->_msg('e','Process Failed. Please try again.','fees/view_fees/'.$eid);		
		}
	}

	//////////////////////////SYLVER POGI///////////////////////////////////END DEDUCTION FUNCTIONS ///////////////////////////////////////////////////////////////

	// ADD OLD ACCOUNT
	public function add_old_accounts($eid = false){

		if($eid === false){ show_404(); }

		#load model
		$this->load->model('M_enrollments');

		#load library
		$this->load->library('_student', array('enrollment_id'=>$eid));
		$this->view_data['_student'] = $this->_student;
		$this->view_data['account_with_balance'] = $this->_student->get_accounts_with_balance();

		if($this->input->post('add_old_accounts') && $this->input->post('add_old_accounts') == "Add Selected"){

			$selected_ids = $this->input->post('balance');

			$ctr = 0;

			if(is_array($selected_ids) && count($selected_ids) > 0){

				set_time_limit(0);

				foreach ($selected_ids as $key_id => $en_id) {
					
					$rs = $this->_student->add_previous_accounts($key_id);

					if($rs->status){
						$ctr++;
					}
				}

				if($ctr > 0){
					$this->_msg('s','Student Old Account/s was successfully added to student current fees.','fees/view_fees/'.$eid);
				}else{
					$this->_msg('e','Old Accounts failed to be process. Please try again.',current_url());	
				}

			}else{
				$this->_msg('e','No Old Accounts Selected',current_url());
			}
		}
	}

	//DESTROY OLD ACCOUNT
	public function destroy_old_account($eid = false, $spa_id = false)
	{
		//VALIDATE
		if($eid == false){ show_404(); }
		if($spa_id == false){ show_404(); }

		//LOAD MODELS
		$this->load->model('M_enrollments2','m_en2');
		$this->load->model('M_student_previous_accounts','m_spa');
		$this->load->model('M_fees2','m_f');

		//LOAD VARIABLES
		$this->view_data['enrollment_id'] = $eid;
		$this->view_data['system_message'] = $this->session->flashdata('system_message');

		//GET NECCESSARY DATA
		$x_enroll = $this->m_en2->pull($eid, array('id'));
		$x_spa = $this->m_spa->get(array('id'=>$spa_id,'is_deleted'=>0));

		if($x_enroll && $x_spa)
		{
			if($x_spa->amount_paid <= 0)
			{
				//LOAD STUDENT LIBRARY
				$this->load->library('_student', array('enrollment_id'=>$eid));

				$x_d = $this->_student->delete_student_previous_account($spa_id);
				if($x_d){
					$this->_msg('s','Old Account was successfully removed.','fees/view_fees/'.$eid);		
				}else{
					$this->_msg('e','Process Failed. Please try again.','fees/view_fees/'.$eid);		
				}
			}
			else
			{
				$this->_msg('e','Cannot delete this old account because payments was already made.','fees/view_fees/'.$eid);
			}
		}
		else
		{
			$this->_msg('e','Process Failed. Please try again.','home');
		}
	}

	public function reapply_payments($id){

		$this->load->library('_student', array('enrollment_id'=>$id));
		$this->_student->re_apply_payment();
	}

	//ADD STUDENT PAYMENT RECORD
	public function add_student_payment($eid = false, $action = false)
	{
		//CHECK DEPARTMENT STUDENT PROFILE ACCESS 
		$this->check_stud_access('stud_add_fees');

		//VALIDATE
		if($eid == false){ show_404(); }

		//LOAD MODELS
		$this->load->model('M_enrollments','m_en');
		$this->load->model('M_enrollments2','m_en2');
		$this->load->model('M_student_plan_modes');
		$this->load->model('M_student_plan_modes','m_spm');
		$this->load->model('M_enrollments','en');
		$this->load->model('M_student_enrollment_fees','m_sef');
		$this->load->model('M_student_previous_accounts','m_spa');
		$this->load->model('M_payment_breakdown','m_pb');
		$this->load->model('M_payment_type','m_pt');
		$this->load->model('M_student_payment_record');
		$this->load->model('M_users');

		//LOAD helper
		$this->load->helper('misc');

		//LOAD VARIABLES
		$this->view_data['enrollment_id'] = $eid;
		$this->view_data['action'] = $this->session->flashdata('print_form') ? 'print_form' : $action;
		$this->view_data['system_message'] = $this->session->flashdata('system_message');

		//LOAD STUDENT LIBRARY
		set_time_limit(0);
		$this->load->library('_student', array('enrollment_id'=>$eid));
		$this->view_data['_student'] = $this->_student;
		$this->view_data['payment_totals'] = $this->_student->student_payment_totals;
		$this->view_data['show_tri_menu'] = true;
		$this->view_data['show_payment_btn'] = true;
		$en = $this->_student->profile;

		//FOR DROPDOWN
		$this->view_data['payment_type'] = $this->m_pt->get_for_dd(array('payment_type','payment_type'),false,'Select Payment Type');

		if($en)
		{
			if($this->_student->is_full_paid){
				// $this->_msg('s','This student is already full in payment.', 'fees/view_fees/'.$eid);
			}

			if($this->input->post('add_payment_record'))
			{
				$this->load->library('form_validation');
				$this->form_validation->set_error_delimiters('<div class="alert-danger">', '</div>'); 
				if($this->form_validation->run('add_payment_record'))
				{
					//GET POST DATA
					$payment_for = $this->input->post('payment_for');
					$tuition_payment = isset($payment_for['tuition']) ? $payment_for['tuition'] : false;	
					$other_payment = isset($payment_for['other']) ? $payment_for['other'] : false;		
					$previous_payment = isset($payment_for['previous']) ? $payment_for['previous'] : false;	
					$enrollment_id_post = $eid;	
					
					if($tuition_payment || $other_payment || $previous_payment)
					{
						unset($input);
						$input['spr_user_trans_id'] = $this->session->userdata('userid');
						$input['spr_user_trans_name'] = $this->session->userdata('username');
						$input['spr_enrollment_id'] = $log['Enrollment ID'] = $enrollment_id_post;
						$input['spr_or_no'] = $log['OR NO'] = $this->input->post('spr_or_no');
						$input['spr_remarks'] = $this->input->post('remarks');
						$input['spr_payment_date'] = $log['Date of Payment'] =  $this->input->post('date_of_payment');
						$input['spr_schoolyear_id'] = $this->cos->user->academic_year_id;// defined in mycontroller
						$input['spr_gperiod_id'] = $this->current_grading_period->id;// defined in mycontroller
						$input['spr_ammt_paid'] = floatval($this->input->post('ammount_paid'));
						$input['spr_mode_of_payment'] = $this->input->post('payment_type');
						$input['is_applied'] = 1; #if payment is not check

						if($input['spr_mode_of_payment'] == 'CHECK'){
							$input['check_date'] = $this->input->post('check_date');
							$input['check_number'] = $this->input->post('check_number');
							$input['is_applied'] = 0; #if payment is check, will be applied if cleared by finance
						}

						//SAVE TO STUDENT PAYMENT RECORD
						$rs_spr = (object)$this->M_student_payment_record->insert($input);
						
						if($rs_spr->status){

							//IF SUCCESS
							//SAVE THE PAYMENT BREAKDOWN
							$new_spr_id = $rs_spr->id;
							$new_spr = $this->M_student_payment_record->get(array('spr_id'=>$new_spr_id));
							if($new_spr){

								$bal_spr_amount_paid = $new_spr->spr_ammt_paid; //DISTRIBUTE THIS AMOUNT TO SELECTED PAYMENT FILES
								$cheque_ids = "";
								$checks_amount = 0; //EXCLUDE CHECKS AMOUNT, TO BE APPLY ONLY WHEN CONFIRM BY THE FINANCE

								if($tuition_payment && is_array($tuition_payment))
								{
									#region CHECK PAYMENT PLAN MODES
									//DISTRIBUTE PAYMENT TO STUDENT PLAN MODES
									foreach ($tuition_payment as $key => $stud_plan_mod_id) {
											
										$spm = $this->m_spm->pull(array('id'=>$stud_plan_mod_id,'is_paid'=>0), array('id','spr_id','value','amount_paid'));
										if($spm && $bal_spr_amount_paid > 0){
											$balance = positive_val($spm->value - $spm->amount_paid); //GET POSITIVE BALANCE ONLY

											//GET AMOUNT TO BE APPLIED FOR STUDENT PLAN MODES DEPENDS ON THE BALANCE AMOUNT PAID
											$applied_amount = 0;
											if($bal_spr_amount_paid < $balance){
												$applied_amount = $bal_spr_amount_paid;
											}else{
												$applied_amount = $balance;
											}

											unset($data);
											$data['spr_id'] = $new_spr_id;

											if($new_spr->spr_mode_of_payment == "CHECK"){//IF CHECK, DO NOT APPLY THE PAYMENT ONLY IF CONFIRM BY FINANCE
												$data['is_check'] = 1;
												$data['check_applied'] = $spm->check_applied + $applied_amount;
												$checks_amount += $applied_amount;
											}else{
												$data['is_check'] = 0;	
												$data['amount_paid'] = $spm->amount_paid + $applied_amount;
											}

											$cheque_ids .= 'T_'.$stud_plan_mod_id.'|';
											
											$rs = $this->m_spm->update($spm->id, $data); //UPDATE RECORD

											$this->m_spm->update_payment_status($spm->id);

											//SAVE TO STUDENT PAYMENT BREAKDOWN
											//TO BE ABLE TO KNOW HOW MUCH AMOUNT IS APPLIED 
											//FIX THE ISSUE WHEN USER ONLY PAYS HALF OF THE AMOUNT DUE
											if($rs){
												$this->m_pb->insert_payment_breakdown($spm->id,$applied_amount,'TUITION',$new_spr->spr_mode_of_payment);
											}

											$bal_spr_amount_paid -= $applied_amount;
											
										}
									}
									#endregion END STUDENT PLAN MODES
								}

								#region CHECK ADDITIONAL / OPTIONAL FEE
								if($other_payment && is_array($other_payment)){

									//DISTRIBUTE PAYMENT TO ADDITIONAL / OPTIONAL FEES (student_enrollment_fees)
									foreach ($other_payment as $key => $stud_en_fee_id) {
										
										$sef = $this->m_sef->pull(array('sef_id'=>$stud_en_fee_id), array('sef_id','spr_id','sef_fee_rate','amount_paid'));
										if($sef && $bal_spr_amount_paid > 0){
											$balance = positive_val($sef->sef_fee_rate - $sef->amount_paid); //GET POSITIVE BALANCE ONLY

											//GET AMOUNT TO BE APPLIED FOR ADDITIONAL FEES DEPENDS ON THE BALANCE AMOUNT PAID
											$applied_amount = 0;
											if($bal_spr_amount_paid < $balance){
												$applied_amount = $bal_spr_amount_paid;
											}else{
												$applied_amount = $balance;
											}

											unset($data);
											$data['spr_id'] = $new_spr_id;

											if($new_spr->spr_mode_of_payment == "CHECK"){//IF CHECK TO NOT APPLY THE PAYMENT ONLY IF CONFIRM BY FINANCE
												$data['is_check'] = 1;
												$data['check_applied'] = $sef->amount_paid + $applied_amount;
												$checks_amount += $applied_amount;
											}else{
												$data['is_check'] = 0;	
												$data['amount_paid'] = $sef->amount_paid + $applied_amount;
											}
											$cheque_ids .= 'O_'.$stud_en_fee_id.'|';
											$rs = $this->m_sef->update($sef->sef_id, $data); //UPDATE RECORD
											$this->m_sef->update_payment_status($sef->sef_id);

											//SAVE TO STUDENT PAYMENT BREAKDOWN
											//TO BE ABLE TO KNOW HOW MUCH AMOUNT IS APPLIED 
											//FIX THE ISSUE WHEN USER ONLY PAYS HALF OF THE AMOUNT DUE
											if($rs){
												$this->m_pb->insert_payment_breakdown($sef->sef_id,$applied_amount,'OTHER',$new_spr->spr_mode_of_payment);
											}

											$bal_spr_amount_paid -= $applied_amount;
											
										}
									}
								}
								#endregion END OF ADDITIONAL / OPTIONAL FEE

								#region CHECK ADDITIONAL / OPTIONAL FEE
								if($previous_payment && is_array($previous_payment)){

									//DISTRIBUTE PAYMENT TO ADDITIONAL / OPTIONAL FEES (student_enrollment_fees)
									foreach ($previous_payment as $key => $stud_prev_id) {
										
										$spa = $this->m_spa->pull(array('id'=>$stud_prev_id), array('id','spr_id','value','amount_paid','prev_enrollment_eid'));
										if($spa && $bal_spr_amount_paid > 0){
											$balance = positive_val($spa->value - $spa->amount_paid); //GET POSITIVE BALANCE ONLY

											//GET AMOUNT TO BE APPLIED FOR PREVIOUS ACCOUNTS DEPENDS ON THE BALANCE AMOUNT PAID
											$applied_amount = 0;
											if($bal_spr_amount_paid < $balance){
												$applied_amount = $bal_spr_amount_paid;
											}else{
												$applied_amount = $balance;
											}

											unset($data);
											$data['spr_id'] = $new_spr_id;

											if($new_spr->spr_mode_of_payment == "CHECK"){//IF CHECK TO NOT APPLY THE PAYMENT ONLY IF CONFIRM BY FINANCE
												$data['is_check'] = 1;
												$data['check_applied'] = $spa->amount_paid + $applied_amount;
												$checks_amount += $applied_amount;
											}else{
												$data['is_check'] = 0;	
												$data['amount_paid'] = $spa->amount_paid + $applied_amount;
											}
											$cheque_ids .= 'P_'.$stud_prev_id.'|';
											$rs = $this->m_spa->update($spa->id, $data); //UPDATE RECORD
											$this->m_spa->update_payment_status($spa->id);

											//SAVE TO STUDENT PAYMENT BREAKDOWN
											//TO BE ABLE TO KNOW HOW MUCH AMOUNT IS APPLIED 
											//FIX THE ISSUE WHEN USER ONLY PAYS HALF OF THE AMOUNT DUE
											if($rs){
												$this->m_pb->insert_payment_breakdown($spa->id,$applied_amount,'PREVIOUS',$new_spr->spr_mode_of_payment);
											}

											#update the previous enrollment balance
											unset($data);
											$prev_enroll = $this->m_en2->get($spa->prev_enrollment_eid);
											$data['is_paid_as_previous'] = 1;
											$data['paid_by_eid'] = $eid;
											$data['total_balance'] = $prev_enroll->total_balance - $applied_amount;
											$rs = $this->m_en2->update($spa->prev_enrollment_eid, $data);
											$this->m_en->update_payment_status($spa->prev_enrollment_eid);
											

											$bal_spr_amount_paid -= $applied_amount;
											
										}
									}
								}
								
								#endregion END OF ADDITIONAL / OPTIONAL FEE

								//UPDATE PAYMENT RECORD FOR IDS
								unset($data);
								$data['check_ids'] = $cheque_ids; //THE ID or the student_plan_mode or other fee selected in the form
								$rs_u = $this->M_student_payment_record->update($new_spr_id, $data);

								//APPLY PAYMENT TO TOTALS AND DEDUCT TO STUDENT BALANCE
								unset($data);
								
								$data['total_balance'] = $en->total_balance - ($new_spr->spr_ammt_paid - $checks_amount);//EXCLUDE CHECKS AMOUNT, TO BE APPLY ONLY WHEN CONFIRM BY THE FINANCE
								$data['total_amount_paid'] = $en->total_amount_paid + ($new_spr->spr_ammt_paid - $checks_amount);//EXCLUDE CHECKS AMOUNT, TO BE APPLY ONLY WHEN CONFIRM BY THE FINANCE
								
								$rs_u = $this->en->update($eid, $data);
								if($rs_u){//UPDATE PAYMENT STATUS
									$this->en->update_payment_status($eid);
								}

								#update subject load taken - if first payment update all subjects load - meaning taken na ung subjects
								$this->_student->reserve_all_student_subject();
								$this->M_users->activate_by_enrollment_id($eid,1);

								activity_log('Add Payment Record',$this->userlogin, 'Data : '.arr_str($log));
								
								//Check if first payment, if yes print the registration form
								if($this->_student->count_applied_payment() == 1){
									$this->session->set_flashdata('print_form','print');
									$this->_msg('s','Payment Record was successfully added.','fees/add_student_payment/'.$enrollment_id_post);	
								}
								else
								{	
									$this->_msg('s','Payment Record was successfully added.','fees/add_student_payment/'.$enrollment_id_post);	
								}
							}	
						}
					}
					else
					{
						$this->session->set_flashdata('inputsave',$save);
						$this->_msg('e',"No selected items to be paid.",'fees/add_payment_record/'.$enrollment_id_post);
					}
				}
			}
		}
		else
		{
			show_404();
		}
	}

	/* FEES - CHECKS FUNCTIONS */

	//STUDENT LIST OF PAYMENTS
	public function payment_record($id = FALSE,$pid = FALSE)
	{
		//CHECK DEPARTMENT STUDENT PROFILE ACCESS 
		$this->check_stud_access('stud_fees');

		$this->disable_browser_cache = TRUE;
		if(is_numeric($id) AND $id !== FALSE)
		{	
			set_time_limit(0);
			$this->load->library('_student', array('enrollment_id'=>$id));
			$this->view_data['_student'] = $this->_student;
			$this->view_data['show_tri_menu'] = true;
			$this->view_data['show_payment_btn'] = true;

			if($this->_student->profile == false) { show_404(); }

			$this->view_data['payments_made'] = $this->_student->get_payment_record();
			$this->view_data['payments_made_as_old'] = $this->_student->get_payment_record_as_an_old_account();
			$this->view_data['enrollment_id'] = $id;
		}else{
			show_404();
		}
	}

	public function delete_payment($eid = false,$id= false)
	{
		//CHECK DEPARTMENT STUDENT PROFILE ACCESS 
		$this->check_stud_access('stud_add_fees');

		if($eid == false || $id == false){show_404();}

		//LOAD MODEL
		$this->load->model('M_student_payment_record','m_spr');

		//LOAD STUDENT LIBRARY
		set_time_limit(0);
		$this->load->library('_student', array('enrollment_id'=>$eid));
		$en = $this->_student->profile;
		$spr = $this->m_spr->get($id);

		//VALIDATE AGAIN
		if($en == false){ show_404(); }
		if($spr == false){ show_404(); }

		$result = $this->_student->delete_payment_record($id);

		if($result->status)
		{
			//SAVE LOGS
			$log['Enrollment id'] = $eid;
			$log['Student Payment Record ID'] = $id;
			$log['Amount'] = $result->data->spr_ammt_paid;
			$log['Payment Type'] = $result->data->spr_mode_of_payment;
			$log['Deleted by'] = $this->userlogin;

			activity_log("DELETE PAYMENT", $this->userlogin, 'DATA : '.arr_str($log));
			$this->_msg('s','Payment Was Successfully Deleted.','fees/payment_record/'.$eid);
		}
		else
		{
			$this->_msg('e','Payment record was not successfully deleted. Please try again','fees/payment_record/'.$eid);
		}
	}

	public function show_payment($eid = false, $spr_id = false)
	{
		//CHECK DEPARTMENT STUDENT PROFILE ACCESS 
		$this->check_stud_access('stud_add_fees');

		//VALIDATE
		if($eid == false){ show_404(); }

		//LOAD MODELS
		$this->load->model('M_enrollments2','m_en2');
		$this->load->model('M_student_plan_modes');
		$this->load->model('M_student_plan_modes','m_spm');
		$this->load->model('M_enrollments','en');
		$this->load->model('M_student_enrollment_fees','m_sef');
		$this->load->model('M_student_previous_accounts','m_spa');
		$this->load->model('M_payment_breakdown','m_pb');
		$this->load->model('M_payment_type','m_pt');
		$this->load->model('M_student_payment_record');
		$this->load->model('M_student_payment_record','m_spr');

		//LOAD helper && library
		$this->load->helper('misc');
		$this->load->library('form_validation');

		//LOAD VARIABLES
		$this->view_data['enrollment_id'] = $eid;
		$this->view_data['system_message'] = $this->session->flashdata('system_message');

		//LOAD STUDENT LIBRARY
		set_time_limit(0);
		$this->load->library('_student', array('enrollment_id'=>$eid));
		$this->view_data['_student'] = $this->_student;
		$this->view_data['payment_totals'] = $this->_student->student_payment_totals;
		$this->view_data['show_tri_menu'] = true;
		$en = $this->_student->profile;

		//FOR DROPDOWN
		$this->view_data['payment_type'] = $this->m_pt->get_for_dd(array('payment_type','payment_type'));

		$this->view_data['payment_record_data'] = $spr = $this->m_spr->get($spr_id);

		if($en && $spr)
		{	
			$this->view_data['fee_ids'] = $this->_student->get_feeid_of_payment_breakdown($spr_id);

			//POST
			if($this->input->post('update_payment_record'))
			{
				//VALIDATE FORM
				$this->form_validation->set_message('is_unik_edit', 'OR NO already in used.');
				if($this->form_validation->run('update_payment_record') == FALSE){	
					$this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>'); 
				}
				else
				{
					unset($data);
					$data['spr_or_no'] = $log['New OR NO'] = $this->input->post('spr_or_no');
					if($data['spr_or_no'] != $spr->spr_or_no){
						$data['old_or_no'] = $spr->old_or_no.'|'.$spr->spr_or_no;
					}
					$data['spr_payment_date'] = $this->input->post('date_of_payment');
					$data['spr_remarks'] = $this->input->post('remarks');

					if($spr->spr_mode_of_payment == "CHECK"){
						$data['check_number'] = $this->input->post('check_number');
						$data['check_date'] = $this->input->post('check_date');
					}

					$rs_update = $this->m_spr->update($spr_id, $data);
					if($rs_update){

						//SAVE ACTIVITY
						$log['Enrollment ID'] = $eid;	
						$log['Student Payment Record ID'] = $spr_id;
						activity_log("update payment record", $this->userlogin, "DATA : ".arr_str($log));	
						$this->_msg('s',"Payment Record was successfully updated.",'fees/show_payment/'.$eid.'/'.$spr_id);
					}else{
						$this->_msg('e',"Update Failed. Please try again",'fees/show_payment/'.$eid.'/'.$spr_id);
					}
				}
			}
		}
		else
		{
			// $this->_msg('e','Process Failed. Please try again.','home');
			show_404();
		}
	
	}

	public function pending_cheque($e_id = false)
	{
		if($e_id == false){show_404();}

		//LOAD MODEL
		$this->load->model('M_enrollments', 'm_e');

		//LOAD HELPER
		$this->load->helper('misc');

		//LOAD STUDENT LIBRARY
		set_time_limit(0);
		$this->load->library('_student', array('enrollment_id'=>$e_id));
		$this->view_data['_student'] = $this->_student;
		$en = $this->_student->profile;

		$this->view_data['enrollment_id'] = $e_id;
		$this->view_data['enrollee_data'] = $en;
		$this->view_data['show_tri_menu'] = true;

		if($en)
		{
			$this->view_data['pending_checks'] = $this->_student->get_pending_cheque();
		}
		else
		{
			show_404();
		}

	}

	public function declined_cheque($e_id = false){
		
		if($e_id == false){show_404();}

		//LOAD MODEL
		$this->load->model('M_enrollments', 'm_e');

		//LOAD HELPER
		$this->load->helper('misc');

		//LOAD STUDENT LIBRARY
		set_time_limit(0);
		$this->load->library('_student', array('enrollment_id'=>$e_id));
		$this->view_data['_student'] = $this->_student;
		$en = $this->_student->profile;

		$this->view_data['enrollment_id'] = $e_id;
		$this->view_data['enrollee_data'] = $en;
		$this->view_data['show_tri_menu'] = true;

		if($en)
		{
			$this->view_data['pending_checks'] = $this->_student->get_declined_cheque();
		}
		else
		{
			show_404();
		}

	}

	public function clear_check($e_id = false, $spr_id = false){
		
		if($e_id == false || $spr_id == false){ show_404(); }

		//LOAD MODEL
		$this->load->model('M_enrollments','m_e');
		$this->load->model('M_student_payment_record','m_spr');
		$this->load->model('M_student_plan_modes', 'm_spm');
		$this->load->model('M_student_enrollment_fees', 'm_sef');
		$this->load->model('M_student_previous_accounts', 'm_spa');
		$this->load->model('M_payment_breakdown', 'm_pb');

		//LOAD STUDENT LIBRARY
		set_time_limit(0);
		$this->load->library('_student', array('enrollment_id'=>$e_id));

		$en = $this->m_e->get($e_id);
		$spr = $this->m_spr->get($spr_id);
		
		//WHEN CHECK IS CLEARED
		if($this->input->post('clear_check'))
		{
			$bal_spr_amount_paid = $spr->spr_ammt_paid;
			$check_ids = $spr->check_ids;

			//GET WHERE THE PAYMENT WILL BE APPLIED
			$arr_check_ids = explode('|', $check_ids);
			

			foreach ($arr_check_ids as $key => $xid) {
				
				//SUFFIX
				//T_ = TUITION
				//O_ = OTHER/ADDITIONAL FEES
				//P_ = PREVIOUS ACCOUNT
							
				//CHECK AND APPLY FOR TUITION FEES
				if(strrpos($xid, "T_") !== false){
					$get_id = substr($xid, 2);
					if(is_numeric($get_id)){

						$spm = $this->m_spm->pull(array('id'=>$get_id,'is_paid'=>0), array('id','spr_id','value','amount_paid','check_applied'));
						
						if($spm && $bal_spr_amount_paid > 0){
							$balance = $this->m_pb->get_amount($spr->spr_id, $spm->id);

							//GET AMOUNT TO BE APPLIED FOR STUDENT PLAN MODES DEPENDS ON THE BALANCE AMOUNT PAID
							$applied_amount = 0;
							if($bal_spr_amount_paid < $balance){
								$applied_amount = $bal_spr_amount_paid;
							}else{
								$applied_amount = $balance;
							}

							unset($data);
							$data['spr_id'] = $spr->spr_id;
							$data['is_check'] = 0; //CLEAR is_check
							$data['amount_paid'] = $spm->amount_paid + $applied_amount;
							
							$rs = $this->m_spm->update($spm->id, $data); //UPDATE RECORD
							$this->m_spm->update_payment_status($spm->id);

							$bal_spr_amount_paid -= $balance;
						}
					}
				}
				
				//CHECK AND APPLY FOR ADDITIONAL / OTHER FEES
				if(strrpos($xid, "O_") !== false){
					$get_id = substr($xid, 2);
					if(is_numeric($get_id)){
						
						$sef = $this->m_sef->pull(array('sef_id'=>$get_id,'is_paid'=>0), array('sef_id','spr_id','sef_fee_rate','amount_paid','check_applied'));
						if($sef && $bal_spr_amount_paid > 0){
							$balance = $this->m_pb->get_amount($spr->spr_id, $sef->sef_id);

							//GET AMOUNT TO BE APPLIED FOR STUDENT PLAN MODES DEPENDS ON THE BALANCE AMOUNT PAID
							$applied_amount = 0;
							if($bal_spr_amount_paid < $balance){
								$applied_amount = $bal_spr_amount_paid;
							}else{
								$applied_amount = $balance;
							}

							unset($data);
							$data['spr_id'] = $spr->spr_id;
							$data['is_check'] = 0; //CLEAR is_check
							$data['amount_paid'] = $sef->amount_paid + $applied_amount;
							
							$rs = $this->m_sef->update($sef->sef_id, $data); //UPDATE RECORD
							$this->m_sef->update_payment_status($sef->sef_id);

							$bal_spr_amount_paid -= $balance;
						}
					}
				}

				//CHECK AND APPLY FOR PREVIOUS ACCOUNTS
				if(strrpos($xid, "P_") !== false){
					$get_id = substr($xid, 2);
					if(is_numeric($get_id)){

						$spa = $this->m_spa->pull(array('id'=>$get_id,'is_paid'=>0), array('id','spr_id','value','amount_paid','check_applied'));
						if($spa && $bal_spr_amount_paid > 0){
							$balance = $this->m_pb->get_amount($spr->spr_id, $spa->id);

							//GET AMOUNT TO BE APPLIED FOR STUDENT PLAN MODES DEPENDS ON THE BALANCE AMOUNT PAID
							$applied_amount = 0;
							if($bal_spr_amount_paid < $balance){
								$applied_amount = $bal_spr_amount_paid;
							}else{
								$applied_amount = $balance;
							}

							unset($data);
							$data['spr_id'] = $spr->spr_id;
							$data['is_check'] = 0; //CLEAR is_check
							$data['amount_paid'] = $spa->amount_paid + $applied_amount;
							
							$rs = $this->m_spa->update($spa->id, $data); //UPDATE RECORD
							$this->m_spa->update_payment_status($spa->id);

							$bal_spr_amount_paid -= $balance;
						}
					}
				}
			}
			

			//UPDATE STUDENT PAYMENT RECORD
			unset($data);
			$data['check_status'] = 'CLEARED';
			$data['check_confirm_userid'] = $this->userid;
			$data['check_confirm_date'] = $log['Date'] = NOW;
			$data['check_remarks'] = $this->input->post('remarks');
			$data['is_applied'] = 1;
			$rs = $this->m_spr->update($spr->spr_id, $data);

			//UPDATE AND APPLYA STUDENT PAYMENT TOTAL
			unset($data);
			$data['total_balance'] = $en->total_balance - $spr->spr_ammt_paid;
			$data['total_amount_paid'] = $en->total_amount_paid + $spr->spr_ammt_paid;
			
			if($rs = $this->m_e->update($e_id, $data)){
				//UPDATE STATUS
				// die('x');
				$this->m_e->update_payment_status($e_id);
			}
			
			#update subject load taken - if first payment update all subjects load - meaning taken na ung subjects
			$this->_student->reserve_all_student_subject();


			$log['Enrollment ID'] = $e_id;
			$log['Student Payment Record ID'] = $spr->spr_id;
			$log['Confirm By'] = $this->userlogin;
			$log['Check Number'] = $spr->check_number;
			activity_log('CLEAR CHECK',$this->userlogin, 'Data : '.arr_str($log));
			$this->_msg('s','Check was successfully cleared and applied to student payment record.','fees/pending_cheque/'.$e_id);
		}
		else if($this->input->post('decline_check'))
		{
			//WHEN CHECK IS DECLINED
			unset($data);
			$data['check_status'] = 'DECLINED';
			$data['check_confirm_userid'] = $this->userid;
			$data['check_confirm_date'] = $log['Date'] = NOW;
			$data['check_remarks'] = $this->input->post('remarks');
			$rs = $this->m_spr->update($spr->spr_id, $data);
			if($rs){
					//CLEAR THE FEE FROM CHECK PAYMENT APPLIED SO THAT IT WILL BE AVAILABLE AGAIN FOR PAYMENT
					$check_ids = $spr->check_ids;

					//GET WHERE THE PAYMENT WILL BE APPLIED
					$arr_check_ids = explode('|', $check_ids);

					foreach ($arr_check_ids as $key => $xid) {
						
						//SUFFIX
						//T_ = TUITION
						//O_ = OTHER/ADDITIONAL FEES
						//P_ = PREVIOUS ACCOUNT
									
						//UPDATE FOR TUITION FEES
						if(strrpos($xid, "T_") !== false){
							$get_id = substr($xid, 2);
							if(is_numeric($get_id)){
								$spm = $this->m_spm->pull(array('id'=>$get_id));
								if($spm){
									$amount = $this->m_pb->get_amount($spr_id, $get_id);
									unset($data);
									$data['is_check'] = 0;
									$data['check_applied'] = $spm->check_applied - $amount;
									$rs = $this->m_spm->update($spm->id, $data); //UPDATE RECORD
									$this->m_spm->update_payment_status($spm->id);
								}
							}
						}
						
						//UPDATE FOR ADDITIONAL / OTHER FEES
						if(strrpos($xid, "O_") !== false){
							$get_id = substr($xid, 2);
							if(is_numeric($get_id)){
								$sef = $this->m_sef->pull(array('sef_id'=>$get_id));
								if($sef){
									$amount = $this->m_pb->get_amount($spr_id, $get_id);
									unset($data);
									$data['is_check'] = 0;
									$data['check_applied'] = $sef->check_applied - $amount;
									$rs = $this->m_sef->update($sef->sef_id, $data); //UPDATE RECORD
									$this->m_sef->update_payment_status($sef->sef_id);
								}
							}
						}

						//UPDATE FOR PREVIOUS ACCOUNTS
						if(strrpos($xid, "P_") !== false){
							$get_id = substr($xid, 2);
							if(is_numeric($get_id)){
								$spa = $this->m_spa->pull(array('id'=>$get_id));
								if($spa){
									$amount = $this->m_pb->get_amount($spr_id, $get_id);
									unset($data);
									$data['is_check'] = 0;
									$data['check_applied'] = $spa->check_applied - $amount;
									$rs = $this->m_spa->update($spa->id, $data); //UPDATE RECORD
									$this->m_spa->update_payment_status($spa->id);
								}
							}
						}
					}

					$log['Enrollment ID'] = $e_id;
					$log['Student Payment Record ID'] = $spr->spr_id;
					$log['Declined By'] = $this->userlogin;
					$log['Check Number'] = $spr->check_number;
					activity_log('DECLINED CHECK',$this->userlogin, 'Data : '.arr_str($log));
					$this->_msg('s','Check was successfully declined.','fees/pending_cheque/'.$e_id);

			}

		}else{
			show_404();
		}
	}

	#udpate student_payment_record is_applied
	private function update_is_applied($id){

		$this->load->model('M_student_payment_record','m_spm');

		#update student payment record
		unset($data);
		$data['is_applied'] = 1;
		$rs = $this->m_spm->update($get_id, $data);
		return $rs;
	}

	/* END OF FEES - CHECKS FUNCTIONS */

	/*
		FUNCTION FOR STUDENT PLAN MODES START HERE
	*/
	
	public function set_payment_plan($eid = false)
	{
		if($eid == false){ show_404(); }	

		//LOAD Models
		$this->load->model('M_payment_plan','m_pp');
		$this->load->model('M_enrollments','m_en');

		#load student library
		set_time_limit(0);
		$this->load->library('_student', array('enrollment_id'=>$eid));
		$this->view_data['_student'] = $this->_student;
		$this->view_data['custom_title'] = "Set Payment Plan";
		$this->view_data['payment_plans'] = $pp = $this->m_pp->fetch_all();
		$this->view_data['enrollment_id'] = $eid;

		if($pp == false){
			$this->_msg('e','No Available Payment Plans.','fees/view_fees/'.$eid);
		}

		#Generate Payment Divisions
		$payment_division = false;
		foreach ($pp as $key => $value) {

			if($value->division <= 0) { continue; }

			$division = $value->division;
			$amount_divided = $this->_student->student_payment_totals->total_plan_due / $division;

			for($g = 1; $g <= $division; $g++){

				unset($student_plan_modes);
				$student_plan_modes['value'] = $amount_divided;
				if($g == 1){
					$student_plan_modes['name'] = "Registration";
				}else{
					$student_plan_modes['name'] = num_to_th($g).' Payment';
				}
				$payment_division[$value->id][$g] = (object)$student_plan_modes;
			}
		}

		$this->view_data['payment_division'] = $payment_division;

		if($this->input->post('set_payment_plan') && $this->input->post('set_payment_plan') == "Choose this as Payment Plan")
		{
			#save process
			$pid = $this->input->post('payment_plan_id');
			$plan = $this->m_pp->get($pid);

			if($plan== false){ show_404(); }

			unset($data);
			$data['payment_plan_id'] = $pid;
			$rs = $this->m_en->update($eid, $data);

			if($rs){
				unset($log);
				$log['enrollment id'] = $eid;
				$log['user id'] = $this->userid;
				$log['plan id set'] = $pid;
				$log['division'] = $plan->division;

				activity_log('Set Payment Plan',$this->userlogin, 'Data : '.arr_str($log));

				$this->_msg('s','Plan was successfully set. Please click the re-compute button to create fees for the student.', 'fees/view_fees/'.$eid);
			}else{
				$this->_msg('e','Transaction Failed. Please try again.', current_url());
			}
		}
	}

	/*
		END OF STUDENT PLAN MODES FUNCTIONS
	*/
	
	public function statement_of_account($id = false)
	{
		if($id === false){ show_404(); }
		#load student library
		$this->load->library('_Student',array('enrollment_id'=>$id));
		$this->view_data['_student'] = $this->_student;
		$this->view_data['student_of_account'] = $soa = $this->_student->get_student_fee_profile();
		$this->view_data['req_payment'] = $req = $this->_student->get_required_payment();
	}

	public function print_statement_of_account($id = false){
		if($id === false){ show_404(); }

		$this->output->enable_profiler(FALSE);
		$debug = true;
		$debug = false;

		$this->layout_view = 'blank';

		#load student library
		$this->load->library('_Student',array('enrollment_id'=>$id));
		if($this->_student->profile === false) { show_404(); }
		$this->view_data['_student'] = $this->_student;
		$this->view_data['student_of_account'] = $soa = $this->_student->get_student_fee_profile();
		$this->view_data['req_payment'] = $req = $this->_student->get_required_payment();
		
		$this->load->helper('print');
		$this->load->library('mpdf');

		$html = $this->load->view('fees/pdf_statement_of_account', $this->view_data, true);

		if($debug){
			echo $html;
		}else{			
			$mpdf = new mPDF('utf-8', array(82,336),8,8,3,3,5,5);
			$mpdf->WriteHTML($html);
			$mpdf->Output('statement_of_account.pdf','I');
			exit(0);
		}
	}

	/**
	 * Student Ledger
	 *@param integer $enrollment_id
	 */
	public function student_ledger($enrollment_id = false)
	{
		if($enrollment_id === false){ show_404(); }
		#load student library
		$this->load->library('_Student',array('enrollment_id'=>$enrollment_id));
		$this->view_data['_student'] = $this->_student;
		$this->view_data['student_ledger'] = $ledger = $this->_student->ledger();
	}

	/**
	 * redirect search - from dynamic student search
	 */
	public function redirect_search()
	{
		// Change the third uri string and redirect
		// Enrollment ID must be in this format Controller/Method/ENrollment_ID
		if($this->input->post()){
			$id = $this->input->post('search_enrollment_id');
			$url = $this->input->post('search_current_url');
			if($id && $url){
				$ex_url = explode('/', $url);
				$ex_url[2] = $id;
				$im_url = implode('/', $ex_url);
				redirect($im_url);
			}else{
				show_404();	
			}
		}else{
			show_404();
		}
	}
}
