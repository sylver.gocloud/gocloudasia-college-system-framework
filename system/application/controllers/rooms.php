<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rooms extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->menu_access_checker('rooms');
	}

	public function index($page=0,$sy = false)
	{	
		if(!$this->cos->user){
			$this->_msg('e','Open Semester was not properly set.','home');
		}
		else
		{
			$year_id = $sy ? $sy : $this->db->escape($this->cos->user->academic_year_id);
			
			//LOAD CRUD LIBRARY
			unset($config);
			$config['title'] = 'Rooms';
			$config['type'] = 'query';
			$config['sql'] = "SELECT id, name, description FROM rooms WHERE academic_year_id = $year_id";
			$config['table'] = "rooms";
			$config['hide_id'] = true;
			$config['order'] = "name";
			$config['disable_system_message'] = true;
			$config['pager'] = array(
				"per_page" => 30,
				"num_links" => 3,
				"uri_segment" => 3
			);
			$config['search'] = true;

			$config['crud'] = array(
				0 => array(					
						'field' => 'name',
						'type'	=> 'text',
						'label' => 'Room Name',
						// 'rules' => 'required',
						'rules' => 'required|is_unique[rooms.name]',
						'attr'  => array(
							'name'        	=> 'name',
							'id'          	=> 'name',
							'placeHolder'   => 'Required',
							'maxlength'   	=> '100',
							'required'		=> null
							)
					),
				1 => array(					
						'field' => 'description',
						'type'	=> 'text',
						'label' => 'Room Description',
						'rules' => 'trim',
						'attr'  => array(
							'name'        	=> 'description',
							'id'          	=> 'description',
							'placeHolder'   => '',
							'maxlength'   	=> '100'
							)
					)
				);

			//WHEN ADDING = this is autoadded in the record : ex : user_id, date_created etc
			$config['crud_extension']['academic_year_id'] = $this->cos->user->academic_year_id;

			//SPECIAL FEATURE - run after the creation
			$config['after_create'] = 'rooms/clone_to_other_semester/';

			$this->load->library('_sylvercrud',$config);

			$this->view_data['data_record'] = $this->_sylvercrud->_html;
			// vd($this->db->last_query());
		}
	}
}
