<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
class Pdf_cpanel Extends Dev_Controller
{
	private $student_name;
	private $student_idno;
	private $student_level;
	
	private $watermark_image;
	private $watermark_text;

	private $my_data;
	
	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('mpdf'));
		$this->load->model(array('M_settings'));
		$settings = $this->M_settings->get_settings();
		
		$this->my_data['school_name'] = $settings->school_name;
		$this->my_data['school_address'] = $settings->school_address;
		$this->my_data['school_telephone'] = $settings->school_telephone;
		$this->my_data['report_name'] = "";
	}
	
	public function index()
	{
		show_404();
	}
	
	// class mPDF ([ string $mode [, mixed $format [, float $default_font_size [, string $default_font [, float $margin_left , float $margin_right , float $margin_top , float $margin_bottom , float $margin_header , float $margin_footer [, string $orientation ]]]]]])
	private function __create_pdf($html,$filename = FALSE,$footer = FALSE,$header = FALSE,$watermark = FALSE, $dimensions = FALSE, $orientation = "P")
	{
		set_time_limit(0);
		if($filename == FALSE)
		{
			$filename = str_replace(' ','_',trim($this->student_name)).'_'.trim($this->student_idno);
		}
		
		$now = date('F d, Y - h:i:s');
		
		if($dimensions == FALSE)
		{
			$mpdf=new mPDF('c','A4','','' , 10 , 10 , 10 , 10 , 5 , 5);
		}else{
			$s = $dimensions;

			if(is_array($s)){
				$size = isset($s[6]) ? [$s[6][0],$s[6][1]] :'A4';
				$mpdf=new mPDF('c',$size,'','' , $s[0] , $s[1] , $s[2] , $s[3] , $s[4] , $s[5], $orientation);
			}else{
				$size = $s;
				$mpdf=new mPDF('c',$size,'','');
			}
		}
		$mpdf->SetDisplayMode('fullpage');
		$mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
			if($footer !== FALSE)
			{
				if(is_bool($footer) === TRUE)
				{
					$mpdf->SetFooter('Page {PAGENO} of {nbpg} -  Generated At '.$now);
				}else{
					$mpdf->SetFooter($footer);
				}
			}
			
			if($header !== FALSE)
			{
				if(is_bool($header) === TRUE)
				{
					$mpdf->SetHeader($this->school_name);
				}else{
					$mpdf->SetHeader($header);
				}
			}
			
			if($watermark !== FALSE)
			{
				if($watermark == 'image')
				{
					$text = $this->watermark_image;
					$mpdf->SetWatermarkImage(school_logo(),0.2);
					$mpdf->showWatermarkImage = true;
				
				}elseif($watermark == 'text')
				{
					$text = $this->watermark_text;
					$mpdf->SetWatermarkText('DRAFT');
					$mpdf->showWatermarkText = true;
					$mpdf->watermarkImageAlpha = 0.1;
				}
			}
			
		$mpdf->SetDisplayMode('fullpage');
		$mpdf->WriteHTML($html);
		$mpdf->Output($filename,'I');
	}

	/**
	 * Package Summary Report
	 */
	public function package_summary_report($data)
	{
		$this->my_data['record'] = $data;
		$this->my_data['report_name'] = "Package Summary Report";
		$html = $this->load->view('_pdf/package_summary_report',$this->my_data, true);
		// echo $html;
		// exit(0);
		$this->__create_pdf($html,false, true, true, false,[10,10,10,10,15,15,[215.9,279.4]], "P");
		exit(0);
	}

	/**
	 * Package Detailed Report
	 */
	public function package_detailed_report($data)
	{
		$this->my_data['record'] = $data;
		$this->my_data['report_name'] = "Package Detailed Report";
		$html = $this->load->view('_pdf/package_detailed_report',$this->my_data, true);
		// echo $html;
		// exit(0);
		$this->__create_pdf($html,false, true, true, false,[10,10,10,10,10,10,[215.9,279.4]], "P");
		exit(0);
	}
}