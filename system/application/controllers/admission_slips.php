<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admission_slips extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->session_checker->open_semester();
		$this->menu_access_checker();
		$this->load->helper('my_dropdown');
		$this->load->model(array('M_core_model'));
	}
	
	public function index($page = 0){
				
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		
		$filter = false;
		$like = false;
		$order_by = false;
		
		$filter['enrollments.semester_id'] = $this->open_semester->id;
		$filter['enrollments.sy_from'] = $this->open_semester->year_from;
		$filter['enrollments.sy_to'] = $this->open_semester->year_to;
		$filter['enrollments.is_deleted'] = 0;
		$filter['enrollments.is_paid'] = 1;
		
		
		$arr_filters = array();
		$suffix = "";
		
		if($_GET)
		{
			if(isset($_GET['submit']) && $_GET['submit'] == "Search")
			{
				$page = 0;
			}
			
			if(isset($_GET['lastname']) && trim($_GET['lastname']) != ''){
				$this->view_data['lastname'] = $lastname = trim($_GET['lastname']);
				$like['enrollments.lastname'] = $lastname;
				$arr_filters['lastname'] = $lastname;
			}
			
			if(isset($_GET['fname']) && trim($_GET['fname']) != ''){
				$this->view_data['fname'] = $fname = trim($_GET['fname']);
				$like['enrollments.fname'] = $fname;
				$arr_filters['fname'] = $fname;
				
			}
			
			if(isset($_GET['studid']) && trim($_GET['studid']) != ''){
				$this->view_data['studid'] = $studid = trim($_GET['studid']);
				$like['enrollments.studid'] = $studid;
				$arr_filters['studid'] = $studid;
			}
			
			if(isset($_GET['year_id']) && trim($_GET['year_id']) != ''){
				$this->view_data['year_id'] = $year_id = trim($_GET['year_id']);
				$filter['enrollments.year_id'] = $year_id;
				$arr_filters['year_id'] = $year_id;
			}
			
			$suffix = array_to_geturl($arr_filters);
		}
		
		//CONFIGURATION
		$get['fields'] = array(
				'enrollments.id', 
				'enrollments.studid', 
				'enrollments.name', 
				'years.year', 
				'courses.course', 
				'enrollments.user_id' 
		);
		
		$get['where'] = $filter;
		$get['like'] = $like;
		
		$get['join'] = array(
			
			1 => array(
				"table" => "courses",
				"on"	=> "courses.id = enrollments.course_id",
				"type"  => "LEFT"
			),
			2 => array(
				"table" => "years",
				"on"	=> "years.id = enrollments.year_id",
				"type"  => "LEFT"
			),
			3 => array(
				"table" => "semesters",
				"on"	=> "semesters.id = enrollments.semester_id",
				"type"  => "LEFT"
			)
		);
		$get['order'] = "enrollments.name";
		$get['all'] = true; //GET ALL EXCLUDE LIMIT
		$get['count'] = true; //RETURN COUNT NOT ROW
		$get['array'] = false; //RETURN OBJECT NOT ARRAY
		$get['single'] = false; //RETURN ALL NOT SINGLE
		
	
		$this->load->library("pagination");
		$config = $this->pagination_style();
		$config["base_url"] = base_url() ."admission_slips/index";
		$suffix = '?'.http_build_query($_GET, '', "&");
		$suffix = str_replace("&submit=Search", "", $suffix);
		$config['suffix'] = $suffix;
		$config['first_url'] = $config['base_url'].$config['suffix']; 
		
		$this->view_data['total_rows'] = $config["total_rows"] = $this->M_core_model->get_record("enrollments", $get);
		
		$config["per_page"] = 30;
		$config['num_links'] = 10;
		$config["uri_segment"] = 3;
		// $config['use_page_numbers'] = TRUE;
		$this->pagination->initialize($config);
		
		//FOR PAGINATION
		$get['all'] = false;
		$get['count'] = false;
		if($this->input->post('submit') == "Print")
		{
			$get['all'] = true;
		}
		$config['start'] = $page;
		$config['limit'] = $config['per_page'];
		
		$get['start'] = $page;
		$get['limit'] = $config['per_page'];
		
		$this->view_data['search'] = $search = $this->M_core_model->get_record("enrollments", $get);
		$this->view_data['links'] = $this->pagination->create_links();
		
	}
	
	// Create
	public function issue($id = false)
	{
		if($id == false){ show_404(); }
		
		$this->load->model(array('M_enrollments','M_admission_slips'));
		
		$this->load->helper('my_dropdown');
		
		$this->view_data['student'] = $stud = $this->M_enrollments->profile($id);
		
		if($stud)
		{
			if($_POST){
				$data = $this->input->post('admission_slip');
				$data['enrollment_id'] = $id;
				$data['name'] = $stud->full_name;
				$data['course_and_year'] = $stud->course.', '.$stud->year;
				$data['created_at'] = NOW;
				$data['updated_at'] = NOW;
				
				$rs = $this->M_admission_slips->create_admission_slip($data);
				if($rs){
					activity_log('create admission slip',$this->userlogin,'Admission_slips Created by: '.$this->user.'Success; Admission_slips Id: '.$rs['id']);
					
					log_message('success','Admission_slips Created by: '.$this->user.'Success; Admission_slips Id: '.$rs['id']);
					$this->session->set_flashdata('system_message', '<div class="alert alert-danger">Admission_slips successfully added.</div>');
					redirect(current_url());
				}
			}
		}else{
			show_404();
		}
	}
	
	public function edit($id = false, $enrollment_id = false)
	{
		if($id == false){  show_404(); }
		if($enrollment_id == false){  show_404(); }
		
		$this->load->model(array('M_enrollments','M_admission_slips'));
		
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		
		$this->view_data['admission_slip'] = $this->M_admission_slips->get_admission_slip($id);
		
		$this->view_data['enrollment_id'] = $id;
		
		$this->view_data['student'] = $stud = $this->M_enrollments->profile($enrollment_id);
		
		if($stud)
		{
			if($_POST){
				$data = $this->input->post('admission_slip');
				$data['updated_at'] = NOW;
				
				$rs = $this->M_admission_slips->update_admission_slip($data, $id);
				
				if($rs){
					activity_log('update admission slip',$this->userlogin,'Admission_slips Updated by: '.$this->user.'Success; Admission_slips Id: '.$id);
					
					log_message('success','Admission_slips Updated by: '.$this->user.'Success; Admission_slips Id: '.$id);
					$this->session->set_flashdata('system_message', '<div class="alert alert-danger">Admission_slips successfully updated.</div>');
					redirect(current_url());
				}
			}
		}else{
			show_404();
		}
	}
	
	public function view($id = false,$page=0)
	{
		if($id == false){  show_404(); }
		
		$this->load->model(array('M_admission_slips'));
		
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		$this->view_data['id'] = $id;
		
		$filter = false;
		
		if($_POST){
			if($this->input->post('submit') == "Search"){
				$this->view_data['fields'] = $field = $this->input->post('fields');
				$this->view_data['keyword'] = $keyword = $this->input->post('keyword');
				$this->view_data['category'] = $category = $this->input->post('category');
				if(trim($keyword) != ""){
					$filter[$field.' LIKE '] = "%".trim($keyword)."%";
				}
				
				if(trim($category)!=""){
					$filter['category'] = trim($category);
				}
			}
			
			$page = 0;
		}
		
		$this->load->library("pagination");
		$config = $this->pagination_style();
		$config["base_url"] = base_url() ."admission_slips/view/".$id;
		$this->view_data['total_rows'] = $config["total_rows"] = $this->M_admission_slips->find_all(0,0,$filter, true, true);
		$config["per_page"] = 2;
		$config['num_links'] = 10;
		$config["uri_segment"] = 4;
		$this->pagination->initialize($config);
		$this->view_data['admission_slip'] = $this->M_admission_slips->find_all($page, $config["per_page"], $filter);
		$this->view_data['links'] = $this->pagination->create_links();
		
	}
	
	public function destroy($id = false)
	{
		if($id == false) { show_404(); }
		$this->load->model(array('M_admission_slips'));
		
		$result = $this->M_admission_slips->delete_admission_slip($id);
		
		activity_log('delete admission slip',$this->userlogin,'Admission_slips Deleted by: '.$this->user.'Success; Admission_slips Id: '.$id);
		
		log_message('error','Admission_slips Deleted by: '.$this->user.'Success; Admission_slips Id: '.$id);
		$this->session->set_flashdata('system_message', '<div class="alert alert-danger">Admission slips successfully deleted.</div>');
		redirect('admission_slips/view/'.$id);
	}
}
	
	