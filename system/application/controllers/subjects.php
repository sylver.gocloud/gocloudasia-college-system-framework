<?php


class Subjects Extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->session_checker->open_semester();
		$this->load->helper(['my_dropdown','time']);
		$this->load->library(array('form_validation'));

		// load models
		$this->load->model('M_subjects');
		$this->load->model('M_master_subjects');
		// $this->layout_view = "application_sbadmin";
	}
	
	public function index($page = 0, $return = false)
	{	
		$this->menu_access_checker();

		$this->view_data['custom_title'] = "Schedule";
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		
		//load models
		$this->load->model(['M_courses']);

		//FOR DROPDOWN
		$this->view_data['course_list'] = $cl = $this->M_courses->get_for_dd(['id','course_code'], false, 'All', 'course_code');
		
		$like = false;
		$filter = false;
		
		if($_GET)
		{	
			if(isset($_GET['code']) && trim($_GET['code'])){
				$this->view_data['code'] = $code = trim($_GET['code']);
				$like['master_subjects.code'] = $code;
			}

			if(isset($_GET['course_id']) && trim($_GET['course_id'])){
				$this->view_data['course_id'] = $course_id = trim($_GET['course_id']);
				$filter['subjects.course_id'] = $course_id;
			}

			if(isset($_GET['major']) && trim($_GET['major'])){
				$this->view_data['major'] = $major = trim($_GET['major']);
				$like['course_specialization.specialization'] = $major;
			}
			
			if(isset($_GET['subject']) && trim($_GET['subject'])){
				$this->view_data['subject'] = $subject = trim($_GET['subject']);
				$like['master_subjects.subject'] = $subject;
			}

			if(isset($_GET['semester_id']) && trim($_GET['semester_id'])){
				$this->view_data['semester_id'] = $semester_id = trim($_GET['semester_id']);
				$filter['subjects.semester_id'] = $semester_id;
			}

			if(isset($_GET['year_id']) && trim($_GET['year_id'])){
				$this->view_data['year_id'] = $year_id = trim($_GET['year_id']);
				$filter['subjects.year_id'] = $year_id;
			}

			if(isset($_GET['academic_year_id']) && trim($_GET['academic_year_id'])){
				$this->view_data['academic_year_id'] = $academic_year_id = trim($_GET['academic_year_id']);

				$this->load->model('M_academic_years');
				$sy = $this->M_academic_years->get($academic_year_id);
				$filter['subjects.year_from'] = $sy->year_from;
				$filter['subjects.year_to'] = $sy->year_to;
			}
		}
		else
		{
			$filter['subjects.year_from'] = $this->cos->user->year_from;
			$filter['subjects.year_to'] = $this->cos->user->year_to;
			$filter['subjects.semester_id'] = $this->cos->user->semester_id;
		}
		
		//CONFIGURATION
		$get['fields'] = array(
				'subjects.id',
				'master_subjects.code',
				'master_subjects.subject',
				'master_subjects.units',
				'master_subjects.lec',
				'master_subjects.lab',
				'subjects.is_open_time',
				'subjects.time',
				'subjects.day',
				'subjects.room_id',
				'subjects.subject_load',
				'subjects.subject_taken',
				'subjects.original_load',
				'subjects.year_to',
				'subjects.year_from',
				'subjects.teacher_user_id',
				'rooms.name AS room',
				'courses.course_code',
				'courses.course as course_name',
				'course_specialization.specialization as major',
				'users.name AS instructor',
				'years.year' 
		);
		
		$get['where'] = $filter;
		$get['like'] = $like;
		
		$get['join'] = array(
			
			1 => array(
				"table" => "master_subjects",
				"on"	=> "master_subjects.ref_id = subjects.ref_id",
				"type"  => "LEFT"
			),
			2 => array(
				"table" => "rooms",
				"on"	=> "rooms.id = subjects.room_id",
				"type"  => "LEFT"
			),
			3 => array(
				"table" => "courses",
				"on"	=> "courses.id = subjects.course_id",
				"type"  => "LEFT"
			),
			4 => array(
				"table" => "users",
				"on"	=> "users.id = subjects.teacher_user_id",
				"type"  => "LEFT"
			),
			5 => array(
				"table" => "years",
				"on"	=> "years.id = subjects.year_id",
				"type"  => "LEFT"
			),
			6 => array(
				"table" => "course_specialization",
				"on"	=> "course_specialization.id = subjects.specialization_id",
				"type"  => "LEFT"
			)
		);
		$get['order'] = "master_subjects.subject";
		$get['all'] = true; //GET ALL EXCLUDE LIMIT
		$get['count'] = true; //RETURN COUNT NOT ROW
		$get['array'] = false; //RETURN OBJECT NOT ARRAY
		$get['single'] = false; //RETURN ALL NOT SINGLE
		
	
		$this->load->library("pagination");
		$config = $this->pagination_style();
		$config["base_url"] = base_url() ."subjects/index";
		$suffix = '?'.http_build_query($_GET, '', "&");
		$suffix = str_replace("&submit=Search", "", $suffix);
		$this->view_data['get_url'] = $config['suffix'] = $suffix;
		$config['first_url'] = $config['base_url'].$config['suffix']; 
		
		$this->view_data['total_rows'] = $config["total_rows"] = $this->M_core_model->get_record("subjects", $get);
		
		$config["per_page"] = 30;
		$config['num_links'] = 10;
		$config["uri_segment"] = 3;
		// $config['use_page_numbers'] = TRUE;
		$this->pagination->initialize($config);
		
		//FOR PAGINATION
		$get['all'] = false;
		$get['count'] = false;
		
		$config['start'] = $page;
		$config['limit'] = $config['per_page'];
		
		$get['start'] = $page;
		$get['limit'] = $config['per_page'];
		
		$this->view_data['results'] = $search = $this->M_core_model->get_record("subjects", $get);
		$this->view_data['links'] = $this->pagination->create_links();

		if($return === TRUE){
			//GET ALL 
			$get['all'] = TRUE;
			$get['count'] = false;
			return $this->M_core_model->get_record("subjects", $get);
		}
	}
	
	public function create(){
		
		$this->menu_access_checker();
		
		$this->view_data['custom_title'] = "Create Schedule";

		$this->load->model(array(
			'M_subjects',
			'M_years',
			'M_courses',
			'M_semesters',
			'M_academic_years',
			'M_coursefinances',
			'M_fees',
			'M_rooms',
			'M_users2',
			'M_master_subjects'
		));
		
		$this->view_data['system_message'] = $this->_msg();
		$this->view_data['cos'] = $this->cos->user; // current open semester
		
		//FOR DROPDOWN
		$get = array('is_deleted'=>0);
		$this->view_data['subject_list'] = $this->M_master_subjects->get_for_dd(['ref_id','code','subject'],$get,'Choose Subject','code'); //available master subjects

		$this->view_data['years'] = $this->M_years->find_all();
		$this->view_data['courses'] = $this->M_courses->find_all();
		$this->view_data['semesters'] = $this->M_semesters->find_all();
		$this->view_data['academic_years'] = $this->M_academic_years->find_all();
		$this->view_data['lab_fees'] = $this->M_fees->get_lab_fees();
		$this->view_data['nstp_fees'] = $this->M_fees->get_nstp_fees();

		$room_fld = array(
				'id',
				'name',
				'description'
			);
		$room_where = array(
				'academic_year_id' => $this->cos->user->academic_year_id
			);
		$this->view_data['rooms'] = $r = $this->M_rooms->get_for_dd($room_fld, $room_where,'Select Room','name');

		$this->view_data['teachers'] = $t = $this->M_users2->get_for_dd(array('id','name'), array('department'=>'teacher'), 'Choose Instructor');
		
		if($_POST){
			
			if($this->form_validation->run('subjects') !== FALSE){
				
				$data = $this->input->post('subjects');
				if(isset($_POST['is_open_time'])){
					$data['is_open_time'] = 1;
				}

				//Get Master Subject
				$ms_subject = $this->M_master_subjects->pull(array('ref_id'=>$data['ref_id']));

				if($ms_subject){
					
					$result = $this->M_subjects->create_subject_from_post($data);
					
					if($result['status'])
					{
						$id = $result['id'];
						
						activity_log('Subject Schedule Add',false,"Subjects Table Id:$id Data : ".arr_str($data));
						$this->_msg('s','Subject schedule ('.$ms_subject->subject.') was successfully created.', current_url());
					}else{
						
						$this->view_data['system_message'] = "<div class='alert alert-danger'>Something went wrong, transaction not saved. Please try again.</div>";
					}
				}else{
					$this->_msg('e','Subject selected was invalid please try again.', current_url());
				}	
			}
		}
	}
	
	public function edit($id){
	
		$this->menu_access_checker();
		
		$this->load->model(array('M_years','M_courses','M_semesters','M_academic_years','M_fees','M_rooms','M_users2'));

		$this->view_data['subjects'] = $s = $this->M_subjects->get_profile($id);

		if(!$s){ show_404(); }
		
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		
		//FOR DROPDOWN
		$get = array('is_deleted'=>0);
		$this->view_data['subject_list'] = $this->M_master_subjects->get_for_dd(['ref_id','code','subject'],$get,'Choose Subject','code'); //available master subjects

		$this->view_data['years'] = $this->M_years->find_all();
		$this->view_data['courses'] = $this->M_courses->find_all();
		$this->view_data['semesters'] = $this->M_semesters->find_all();
		$this->view_data['academic_years'] = $this->M_academic_years->find_all();
		$this->view_data['lab_fees'] = $this->M_fees->get_lab_fees();
		$this->view_data['nstp_fees'] = $this->M_fees->get_nstp_fees();
		$this->view_data['rooms'] = $t = $this->M_rooms->get_for_dd(array('id','name','description'), array('academic_year_id'=>$this->cos->user->academic_year_id), 'Choose Room');
		$this->view_data['teachers'] = $t = $this->M_users2->get_for_dd(array('id','name'), array('department'=>'teacher'), 'Choose Instructor');

		/*GET DAYS*/
		$this->load->helper('my_string');
		$this->view_data['monday'] = check_day_if_exist('M',$s->day);
		$this->view_data['tuesday'] = check_day_if_exist('t',$s->day);
		$this->view_data['wednesday'] = check_day_if_exist('w',$s->day);
		$this->view_data['thursday'] = check_day_if_exist('th',$s->day);
		$this->view_data['friday'] = check_day_if_exist('f',$s->day);
		$this->view_data['saturday'] = check_day_if_exist('s',$s->day);
		$this->view_data['sunday'] = check_day_if_exist('su',$s->day);
		// vd($this->view_data['monday']);
		
		if($_POST){
			
			if($this->form_validation->run('subjects') !== FALSE){
				
				$data = $this->input->post('subjects');

				//Get Master Subject
				$ms_subject = $this->M_master_subjects->pull(array('ref_id'=>$data['ref_id']));

				if($ms_subject){

					$result = $this->M_subjects->create_subject_from_post($data, $id);
				
					if($result)
					{	
						$this->_msg('s','Subject schedule ('.$ms_subject->subject.') was successfully updated.', current_url());
					}else{
						
						$this->view_data['system_message'] = "<div class='alert alert-danger'>Something went wrong, transaction not saved. Please try again.</div>";
					}
				}else{
					$this->_msg('e','Subject selected was invalid please try again.', current_url());
				}
			}
		}
	}
	
	public function edit_subject($id= false)
	{
		$this->session_checker->check_if_alive();
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		if($id !== false AND ctype_digit($id))
		{
			if($this->input->post('update_subjects') !== false)
			{
				$id = $this->input->post('subject_id',TRUE);
				$data['day'] = implode('',$this->input->post('days'));
				$data['sc_id'] = $this->input->post('sc_id');
				$data['code'] = $this->input->post('code');
				$data['subject'] = $this->input->post('subject');
				$data['units'] = $this->input->post('units');
				$data['lec'] = $this->input->post('lec');
				$data['lab'] = $this->input->post('lab');
				$data['time'] = trim($this->input->post('time_from')).'-'.trim($this->input->post('time_to'));
				$data['room'] = $this->input->post('room');
				$data['original_load'] = $this->input->post('original_load');
				$data['rle'] = $this->input->post('rle');
				$data['year_id'] = $this->input->post('years');
				$data['semester_id'] = $this->input->post('semesters');
				$data['course_id'] = $this->input->post('courses');
				$data['prerequisite_subject_id'] = $this->input->post('prerequisite_subject_id');
				$data['year_from'] = $this->input->post('year_from');
				$data['year_to'] = $this->input->post('year_to');
				
				
				$subject_load = $this->input->post('subject_load');
				$sub_value = $this->input->post('sub_value');
				$add_value = $this->input->post('add_value');
				$load_type  = $this->input->post('load');
				$load_res    = $this->input->post('reset_subject_load');
		
				
				//if($this->form_validation->run() !== FALSE)
				//{
						$this->load->model('M_subjects');
						if($load_res == 1)
						{
							if($this->M_subjects->reset_subject_load($id) == TRUE)
							{
								switch($load_type)
								{
									case strtolower($load_type) ==  1:
										$data['subject_load'] = $subject_load + $add_value; 
									break;
									case strtolower($load_type) ==  2:
										$data['subject_load'] = $subject_load - $sub_value; 
									break;
								}
								
							}else
							{
								$this->view_data['system_message'] = 'Subject Reset and Update failed';
							}
						}else
						{
								switch($load_type)
								{
									case strtolower($load_type) == 1:
										$data['subject_load'] = $subject_load + $add_value; 
									break;
									case strtolower($load_type) == 2:
										$data['subject_load'] = $subject_load - $sub_value; 
									break;
								}
						}
						
						if($this->M_subjects->update_table($data,$id) == TRUE)
						{
						    log_message('info','Subject Updated by: '.$this->user.'; Subject Id: '.$id);
							$this->view_data['system_message'] = '<div class="alert alert-success">Subject updated</div>';
						}else
						{
							log_message('error','Subject Updated by: '.$this->user.'FAILED; Subject Id: '.$id);
							$this->view_data['system_message'] = '<div class="alert alert-danger">Update failed</div>';
						}
				//}
			}
			
			$this->load->model(array('M_subjects','M_years','M_courses','M_semesters'));
			$this->view_data['subject_data'] = $this->M_subjects->get($id,array('id','sc_id','code','subject','units','lec','lab','time','day','room','subject_load','original_load','year_to','year_from','rle','prerequisite_subject_id','course_id','semester_id','year_id'));
			$this->view_data['subjects'] 	 = $this->M_subjects->get('',array('id','code','subject'),array('semester_id'=>$this->open_semester->id,'year_from'=>$this->open_semester->year_from,'year_to'=>$this->open_semester->year_to));
			$this->view_data['semesters'] = $this->M_semesters->get('',array('name','id'));
			$this->view_data['courses'] 	 = $this->M_courses->get('',array('course','id'));
			$this->view_data['years'] 		 = $this->M_years->get('',array('year','id'));	
		}
	}
	
	// Delete
	public function destroy($id = false)
	{
		if(!$id) { show_404(); }
		$this->menu_access_checker();
		$this->load->model(array('M_subjects'));
		$subjects = $this->M_subjects->get_profile($id); if($subjects === false){ show_404(); }

		$this->load->model('M_student_subjects');
	  $e = $this->M_student_subjects->get_subjects_students($id, true);
	  if($e){ // Subjects has students, cannot be deleted
	  	$this->_msg('e','Subject cannot be deleted, already enrolled by students', 'subjects');
	  }

		$result = $this->M_subjects->delete_subjects($id);
		if($result){
			activity_log('Delete Subjects Schedule',false,'Table Subjects ID : '.$id." Subject Code : ".$subjects->code);
			$this->_msg('s',"Subject ($subjects->subject) was successfully deleted.",'subjects');
		}else{
			$this->_msg('e',"Something went wrong, transaction was not saved. Please try again.",'subjects');
		}
	}
	
	public function add_subject_ajax($subject_id, $y, $c, $s, $eid)
	{
		$this->load->model(array('M_student_subjects', 'M_subjects', 'M_enrollments'));
		$data['enrollmentid'] = $eid;
		$data['subject_id'] = $subject_id;
		$data['year_id'] = $y;
		$data['semester_id'] = $s;
		$data['course_id'] = $c;
		
		//echo json_encode($data);
	}
	
	public function add_subject($y, $c, $s,$eid)
	{
		$this->menu_access_checker(array(
			'subjects',
			'search/search_student',
			'enrollees/daily_enrollees',
			'search/master_list_by_course',
			'search/search_enrollee',
			'search/list_students/paid',
			'search/list_students/unpaid',
			'search/list_students/fullpaid',
			'search/list_students/partialpaid',
		));
		
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		$this->load->model(array('M_subjects', "M_student_subjects"));
		
		$this->token->set_token();
		$this->view_data['form_token'] = $this->token->get_token();
		$this->view_data['eid'] = $eid;
		$this->view_data['y'] = $y;
		$this->view_data['c'] = $c;
		$this->view_data['s'] = $s;
		
			$this->load->library("pagination");
			$config = $this->pagination_style();
			$config["base_url"] = base_url() ."subjects/add_subject/$y/$c/$s/$eid" ;
			$this->view_data['total_rows'] = $config["total_rows"] = $this->M_subjects->subjects_count($s,$this->open_semester->year_from,$this->open_semester->year_to);
			$config["per_page"] = 50;
			$config['num_links'] = 10;
			$config["uri_segment"] = 7;
			$page = ($this->uri->segment(7)) ? $this->uri->segment(7) : 0;
			$this->pagination->initialize($config);
			$this->view_data['subject_list'] = $this->M_subjects->subjectlist
			($s,$this->open_semester->year_from,$this->open_semester->year_to , $eid,$config["per_page"], $page);
			$this->view_data['links'] = $this->pagination->create_links();
		

		
		
		$this->view_data['subjects']	 = $subjects=  $this->M_student_subjects->get_studentsubjects($eid);
		$this->view_data['subject_units'] =  $this->M_student_subjects->get_studentsubjects_total($subjects);
	}
	
	public function view_class_list($subject_id)
	{
		$this->menu_access_checker();
	  $this->load->model('m_subjects');
	  $this->load->model('m_enrollments');
	  $this->load->model('M_student_subjects');
	  $this->view_data['subject'] = $this->m_subjects->get_profile($subject_id);
	  $this->view_data['enrollments'] = $e = $this->M_student_subjects->get_subjects_students($subject_id, true);
	}

	public function download_in_excel()
	{
		$this->menu_access_checker('subjects/index');
		$search = $this->index(0,TRUE);
		if($search)
		{
			// inlcude download controller
			$this->load->library('../controllers/_the_downloadables');
			$this->_the_downloadables->_download_all_subject($search);
		}
		else
		{
			$this->session->set_flashdata('system_message', '<div class="alert alert-danger"><span class="glyphicon glyphicon-remove"></span>&nbsp; No record to download.</div>');
			redirect('subjects');
		}
	}

	public function print_all_in_pdf()
	{
		$this->menu_access_checker('subjects/index');
		$search = $this->index(0,TRUE);
		if($search)
		{
			// $this->disable_menus = true;
			// $this->disable_views = true;

			$this->load->helper('print');
			
			$html = _html_subject_list($this->view_data); 
			// echo $html;
			// exit(0);
			$this->load->library('mpdf');
			
			$mpdf=new mPDF('','FOLIO','','',5,5,5,5,0,0); 

			$mpdf->AddPage('L');
			
			$mpdf->WriteHTML($html);
			
			$mpdf->Output();
		}
		else
		{
			$this->session->set_flashdata('system_message', '<div class="alert alert-danger"><span class="glyphicon glyphicon-remove"></span>&nbsp; No record to print.</div>');
			redirect('subjects');
		}
	}

	/* For Developers */
	public function set_subjects_refid()
	{
		$this->M_subjects->set_subject_ref_id();
		$this->_msg('s','Subjects Reference I.D. successfully set.', 'subjects');
	}

	/**
	 *Clone Subjects of Previous Semester
	*/
	public function clone_subject_of_prev_semester($current_academic_year_id = false, $prev_academic_year_id = false, $current_semester_id = false, $prev_semester_id = false){

		$current_academic_year_id = 2;
		$prev_academic_year_id = 2;
		$current_semester_id = 2;
		$prev_semester_id = 1;

		$this->M_subjects->cloned_subjects_from_previous_semester($current_academic_year_id, $prev_academic_year_id, $current_semester_id, $prev_semester_id);
	}
}
