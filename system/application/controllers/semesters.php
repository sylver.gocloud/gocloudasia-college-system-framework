<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Semesters extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->session_checker->open_semester();
		// $this->session_checker->secure_page('admin');
		$this->menu_access_checker('semesters');
	}
	
	// Create
	public function create()
	{
		$this->load->model(array('M_semesters'));
		
		$this->view_data['semester'] = FALSE;
		
		if($_POST)
		{
			$data = $this->input->post('semesters');
			$data['created_at'] = date('Y-m-d H:i:s');
			$data['updated_at'] = date('Y-m-d H:i:s');
			
			$result = $this->M_semesters->create_semesters($data);
			
			if($result['status'])
			{
				log_message('error','Semester Created by: '.$this->user.'Success; Semester Id: '.$id);
				$this->session->set_flashdata('system_message', '<div class="alert alert-danger">Semester successfully added.</div>');
				redirect('semesters');
			}
		}
	}
	
	// Retrieve
	public function old_index()
	{
		$this->load->model(array('M_semesters'));
		$this->view_data['semesters'] = $this->M_semesters->find_all();
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
	}

	// SYLVER CRUD
	public function index()
	{
		//LOAD CRUD LIBRARY
		unset($config);
		$config['title'] = 'Semester';
		$config['type'] = 'query';
		$config['sql'] = "SELECT id, name, level FROM semesters";
		$config['table'] = "semesters";
		$config['order'] = "level";
		$config['hide_id'] = true;
		$config['column_name'] = array(
				'id'   => 'id',
				'name' => 'Semester Name',
				'level' => 'Order'
			);
		$config['disable_system_message'] = true;
		$config['disable_title'] = true;
		$config['pager'] = array(
			"per_page" => 10,
			"num_links" => 3,
			"uri_segment" => 3
		);
		$config['search'] = true;

		$config['crud'] = array(
			0 => array(					
					'field' => 'name',
					'type'	=> 'text',
					'label' => 'Semester Name',
					'rules' => 'required|trim',
					// 'help'  => 'Name of the Controller to be used.',
					'attr'  => array(
						'name'        	=> 'name',
						'id'          	=> 'name',
						'placeHolder'   => 'Required',
						'maxlength'   	=> '100',
						'required'		=> null
						)
				),
			1 => array(					
					'field' => 'level',
					'type'	=> 'text',
					'label' => 'Order',
					'rules' => 'required|numeric',
					'attr'  => array(
						'name'        	=> 'level',
						'id'          	=> 'level',
						'placeHolder'   => 'Required',
						'maxlength'   	=> '3',
						'required'		=> null
						)
				)
			);

		$this->load->library('_sylvercrud',$config);

		$this->view_data['data_record'] = $this->_sylvercrud->_html;
	}
	
	public function display($id)
	{
		$this->load->model(array('M_semesters'));
		
		$this->view_data['semesters'] = $this->M_semesters->get_semesters($id);
	}
	
	// Update
	public function edit($id)
	{
		$this->load->model(array('M_semesters'));
		
		$this->view_data['semesters'] = $this->M_semesters->get_semesters($id);
		
		if($_POST)
		{
			$data = $this->input->post('semesters');
			$data['updated_at'] = date('Y-m-d H:i:s');
			
			$result = $this->M_semesters->update_semesters($data, $id);
			
			if($result['status'])
			{
				log_message('error','Semesters Updated by: '.$this->user.'Success; Semesters Id: '.$id);
				$this->session->set_flashdata('system_message', '<div class="alert alert-danger">Semester successfully updated.</div>');
				redirect('semesters');
			}
		}
	}
	
	// Update
	public function destroy($id)
	{
		$this->load->model(array('M_semesters'));
		
		$result = $this->M_semesters->delete_semesters($id);
		log_message('error','Semesters Deleted by: '.$this->user.'Success; Semesters Id: '.$id);
		$this->session->set_flashdata('system_message', '<div class="alert alert-danger">Semester successfully deleted.</div>');
		redirect('semesters');
	}
}