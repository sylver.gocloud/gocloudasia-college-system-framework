<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Logout extends MY_Controller {
	
	public function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{
		$msg = $this->_msg() ? $this->_msg() : "Thank you for using the system. You have been successfully logout.";
		$this->view_data['system_message'] = '<div class="alert alert-success"><b><span class="glyphicon glyphicon-ok"></span>&nbsp; '.$msg.'</b></div>';
		$this->layout_view = $this->welcome_view;
	}
}

?>
