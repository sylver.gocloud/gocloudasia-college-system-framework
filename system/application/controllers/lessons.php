<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Lessons extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->session_checker->open_semester();
		$this->menu_access_checker();
		
	  $this->load->model(array("M_lessons"));
	}
	
	public function index()
	{
	  $this->view_data["lessons"] = $this->M_lessons->get_lessons_of_employee($this->session->userdata["userid"]);
	  
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
	}
	
	public function create()
	{
	  
	  if($_POST)
		{
			$data = $this->input->post('lessons');
			$data["user_id"] = $this->session->userdata['userid'];
			$data['created_at'] = date('Y-m-d H:i:s');
			$data['updated_at'] = date('Y-m-d H:i:s');
			
			
			$result = $this->M_lessons->create_lesson($data);
			
			if($result['status'])
			{
				log_message('error','Lesson Created by: '.$this->user.'Success; Lesson Id: '.$id);
				$this->session->set_flashdata('system_message', '<div class="alert alert-success">Lesson successfully created.</div>');
				redirect('lessons');
			}
		}
	}
	
	// Update
	public function edit($id)
	{
		
		$this->view_data['lessons'] = $this->M_lessons->get_lesson($id);
		
		if($_POST)
		{
			$data = $this->input->post('lessons');
			$data['updated_at'] = date('Y-m-d H:i:s');
			
			$result = $this->M_lessons->update_lesson($data, $id);
			
			if($result['status'])
			{
				log_message('error','Lesson Updated by: '.$this->user.'Success; Lesson Id: '.$id);
				$this->session->set_flashdata('system_message', '<div class="alert alert-success">Lesson successfully updated.</div>');
				redirect('lessons');
			}
		}
	}
	
	// Update
	public function destroy($id)
	{
		
		$result = $this->M_lessons->delete_lesson($id);
		log_message('error','Lesson Deleted by: '.$this->user.'Success; Lesson Id: '.$id);
		$this->session->set_flashdata('system_message', '<div class="alert alert-success">Lesson successfully deleted.</div>');
		redirect('lessons');
	}
}
