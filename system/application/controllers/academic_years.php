<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Academic_years extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->session_checker->open_semester();
		// $this->session_checker->secure_page('admin');
		$this->menu_access_checker('academic_years');
	}
	
	// Create
	public function create()
	{
		$this->load->model(array('M_academic_years','M_rooms','M_subjects',"M_block_section_settings",'M_open_semesters'));
		
		$this->view_data['academic_year'] = FALSE;
		
		if($_POST)
		{
			$data = $this->input->post('academic_years');
			
			$rs_exist = $this->M_academic_years->pull($data);
			if($rs_exist){
				#record already exist
				$this->_msg('e','Record was already existed',current_url());
			}
			
			$result = $this->M_academic_years->insert($data);
			
			if($result['status'])
			{
				$this->M_open_semesters->auto_create_open_semesters($result['id']);

				activity_log('create academic years',$this->userlogin,'Academic Years Created by: '.$this->userlogin.'Success; Academic Years Id: '.$id);
				
				log_message('create','Academic Years Created by: '.$this->user.'Success; Academic Years Id: '.$id);
				$this->session->set_flashdata('system_message', '<div class="alert alert-success">Academic Year successfully added.</div>');
				redirect('academic_years');
			}
		}
	}
	
	// Retrieve
	public function index()
	{
		$this->load->model(array('M_academic_years'));
		unset($get);
		$get['order'] = "year_from DESC";
		$this->view_data['academic_years'] = $this->M_academic_years->get_record('academic_years', $get);
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
	}
	
	public function display($id)
	{
		$this->load->model(array('M_academic_years'));
		
		$this->view_data['academic_years'] = $this->M_academic_years->get_academic_years($id);
	}
	
	// Update
	public function edit($id)
	{
		$this->load->model(array('M_academic_years'));
		
		$this->view_data['academic_years'] = $this->M_academic_years->get_academic_years($id);
		
		if($_POST)
		{
			$data = $this->input->post('academic_years');
			$data['updated_at'] = date('Y-m-d H:i:s');
			
			$result = $this->M_academic_years->update_academic_years($data, $id);
			
			if($result['status'])
			{
				activity_log('update academic years',$this->userlogin,'Academic Years Updated by: '.$this->user.'Success; Academic Years Id: '.$id);
				
				log_message('update','Academic Years Updated by: '.$this->user.'Success; Academic Years Id: '.$id);
				$this->session->set_flashdata('system_message', '<div class="alert alert-danger">Academic Year successfully updated.</div>');
				redirect('academic_years');
			}
		}
	}
	
	// Update
	public function destroy($id)
	{
		$this->load->model(array('M_academic_years'));
		
		$result = $this->M_academic_years->delete_academic_years($id);
		
		activity_log('destroy academic years',$this->userlogin,'Academic Years Deleted by: '.$this->user.'Success; Academic Years Id: '.$id);
		
		log_message('error','Academic Years Deleted by: '.$this->user.'Success; Academic Years Id: '.$id);
		$this->session->set_flashdata('system_message', '<div class="alert alert-success">Academic Year successfully deleted.</div>');
		redirect('academic_years');
	}
}