<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Curriculum Extends MY_Controller
{
	public function __construct(){
		parent::__construct();
		$this->session_checker->check_if_alive();
		$this->session_checker->open_semester();
		$this->load->helper(array('url_encrypt','my_dropdown'));
		$this->load->library(array('form_validation'));
		// LOAD MODELS
		$this->load->Model(array('M_curriculum','M_master_subjects','M_curriculum_subjects','M_subject_pre_requisite','M_years'));
		$this->session_checker->check_if_alive();
		$this->menu_access_checker('curriculum/index');
	}

	public function index($page = 0)
	{
		//PAGINATION
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		
	
		$filter = false;
		$like = false;
		$order_by = false;

		$filter['curriculum.is_deleted'] = 0;
		
		if($_GET)
		{
			if(isset($_GET['name']) && trim($_GET['name']) != ''){
				$this->view_data['name'] = $name = trim($_GET['name']);
				$like['curriculum.name'] = $name;
			}

			if(isset($_GET['course']) && trim($_GET['course']) != ''){
				$this->view_data['course'] = $course = trim($_GET['course']);
				$like['courses.course'] = $course;
			}
		}
		
		$get['where'] = $filter;
		$get['like'] = $like;

		$get['fields'] = array(
				'curriculum.id',
				'curriculum.name',
				'curriculum.course_id',
				'curriculum.is_active',
				'courses.course',
				'courses.course_code',
				'course_specialization.specialization as major',
				'course_specialization.id as major_id',
				'academic_years.year_from',
				'academic_years.year_to'
			);
		$get['group'] = "curriculum.course_id";
		$get['join'] = array(
			
			1 => array(
				"table" => "courses",
				"on"	=> "courses.id = curriculum.course_id",
				"type"  => "LEFT"
			),
			2 => array(
				"table" => "academic_years",
				"on"	=> "academic_years.id = curriculum.academic_year_id",
				"type"  => "LEFT"
			),
			3 => array(
				"table" => "course_specialization",
				"on"	=> "course_specialization.id = curriculum.specialization_id",
				"type"  => "LEFT"
			)
		);
		$get['order'] = "curriculum.name DESC";
		$get['all'] = true; //GET ALL EXCLUDE LIMIT
		$get['count'] = true; //RETURN COUNT NOT ROW
		$get['array'] = false; //RETURN OBJECT NOT ARRAY
		$get['single'] = false; //RETURN ALL NOT SINGLE
		
	
		$this->load->library("pagination");
		$config = $this->pagination_style();
		$config["base_url"] = base_url() ."curriculum/index";
		$suffix = '?'.http_build_query($_GET, '', "&");
		$suffix = str_replace("&submit=Search", "", $suffix);
		$config['suffix'] = $suffix;
		$config['first_url'] = $config['base_url'].$config['suffix']; 
		
		$this->view_data['total_rows'] = $config["total_rows"] = $this->M_curriculum->get_record(false, $get);
		
		$config["per_page"] = 10;
		$config['num_links'] = 10;
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		
		//FOR PAGINATION
		$get['all'] = false;
		$get['count'] = false;
		
		$config['start'] = $page;
		$config['limit'] = $config['per_page'];
		
		$get['start'] = $page;
		$get['limit'] = $config['per_page'];
		
		//Records Are Group BY COURSE
		$this->view_data['results'] = $search = $this->M_curriculum->get_record(false, $get);
		$this->view_data['links'] = $this->pagination->create_links();
		$this->view_data['back_url'] = $config['first_url'];

		$get_each = $get;
		unset($get_each['where']);
		unset($get_each['group']);
		$get_each['all'] = true;

		$group_result = false;

		//Get Records of each Course
		if($search){
			$c = 0;
			// Loop Record of Group Course
			foreach ($search as $k => $v) {

				// Get Record Group by Course & Major
				$get_each['where']['curriculum.course_id'] = $v->course_id;
				$get_each['group'] = 'curriculum.course_id,curriculum.specialization_id';
				$get_each['order'] = 'courses.course_code,course_specialization.specialization';
				
				$group_course_major = $this->M_curriculum->get_record(false, $get_each);
				// vp($this->db->last_query());
				$group_result[$k]['course'] = $v;
				$group_result[$k]['course_major'] = array();
				if($group_course_major){
					// Loop Record Group by Course & Major
					foreach ($group_course_major as $k1 => $v2) {
						unset($get_each['group']);
						$get_each['where']['curriculum.course_id'] = $v2->course_id;
						if($v2->major_id){
							$get_each['where']['curriculum.specialization_id'] = $v2->major_id;
						}
						$course_major = $this->M_curriculum->get_record(false,$get_each);

						$group_result[$k]['course_major'][$c]['head'] = $v2;
						$group_result[$k]['course_major'][$c]['tail'] = $course_major;
						$c++;
					}
				}

				unset($get_each['where']['curriculum.course_id']);
				unset($get_each['where']['curriculum.specialization_id']);
			}
		}
		// ud($group_result);
		$this->view_data['group_result'] = $group_result;
	}

	/** 
	 * CREATE NEW CURRICULUM STEP 1
	 */
	public function create(){

		$this->view_data['custom_title'] = "Step 1 : Create Curriculum";
		
		if($this->input->post('save_and_continue')){

			$data['name'] = $this->input->post('name', true);
			$data['course_id'] = $this->input->post('course_id', true);
			$data['specialization_id'] = $this->input->post('specialization_id') ? $this->input->post('specialization_id') : '';			
			$data['academic_year_id'] = $this->cos->user->academic_year_id;
			$data['sy_from'] = $this->cos->user->year_from;
			$data['sy_to'] = $this->cos->user->year_to;

			// Check record if course exist if no set is_active to 1
			unset($get);
			$get['where']['course_id'] = $this->input->post('course_id', true);
			$get['single'] = true;
			$rs_exist = $this->M_curriculum->get_record(false, $get);
			if($rs_exist){
				$data['is_active'] = 0; //deactivate
			}else{
				$data['is_active'] = 1; //activate
			}
			

			if($this->form_validation->run('curriculum') !== FALSE){

				// check if course already has a curriculum record if yes - check if the record was activate
				// only allow 1 course to be active


				$rs = (object)$this->M_curriculum->insert($data);
				if($rs->status){
					$this->M_curriculum->activate_if_possible($rs->id);
					activity_log('create curriculum',$this->userlogin,'Data : '. arr_str($data)); /*SAVE ACTIVITY*/
					$this->_msg('s','Curriculum was successfully created, please continue to add subjects', 'curriculum/add_subjects/'.__link($rs->id));
				}else{
					$this->_msg('e','Something went wrong, please try again', current_url());
				}
			}
		}
	}

	/** 
	 * ADD SUBJECTS FOR THE CURRICULUM STEP 2
	 */
	public function add_subjects($hash_id = NULL, $type = "multiple", $parhash_id = false)
	{
		$id = $this->check_hash($hash_id); //decrypt hash

		$this->load->helper('my_string');

		$this->view_data['curriculum'] = $curriculum = $this->M_curriculum->get_curriculum($id);
		if($curriculum === false){ show_404(); }

		if($type === "edit_subject"){
			$par_id = $this->check_hash($parhash_id);
			$this->view_data['subject_profile'] = $c_subject = $this->M_curriculum_subjects->get_profile($par_id); if($c_subject === false){ show_404(); }
		}

		$this->view_data['hash_id'] = $hash_id;
		$this->view_data['custom_title'] = "Step 2 : Add Subjects";
		$this->view_data['add_type'] = $type;
		$this->view_data['curriculum_id'] = $id;
		$this->view_data['with_action'] = true;
		unset($get);
		
		// get subjects already added
		$this->view_data['curriculum_subjects'] = $cs = $this->M_curriculum_subjects->get_curriculum_subjects($id);
		
		$this->view_data['subject_list'] = $sl = $this->M_master_subjects->available_subjects_for_curriculum($id);
		$fdd = $this->M_master_subjects->available_subjects_for_curriculum($id, false);
		$this->view_data['subject_dropdown'] = $sd = make_dropdown_array($fdd, array('ref_id','code','subject'),'Select Subject');
		$this->view_data['subject_master'] = $s = $this->sort_data_alpha($sl, 'code');
		
		//closure function to get pre-requisite
		$this->view_data['get_pre_requisite'] = function($c_id, $s_id){return $this->M_subject_pre_requisite->get_pre_requisite($c_id, $s_id);};

		// CATCH THE SAVING OF MULTIPLE SUBJECTS
		if(isset($_POST['event_action']) && $_POST['event_action'] === "save_multiple"){
			
			$subject_selected = $this->input->post('subject');
			$year_id = $this->input->post('year_id');
			$semester_id = $this->input->post('semester_id');

			if($this->form_validation->run('curriculum_add_subjects') !== FALSE){

				if($subject_selected){

					$ctr = 0;

					foreach($subject_selected as $subj_id){

						// Get Subject
						$rs_subj = $this->M_master_subjects->pull($subj_id, array('id','ref_id'));
						if($rs_subj === false){ continue; }

						// Check subject if already added in the curriculum_subject
						unset($get);
						$get['curriculum_id'] = $id;
						$get['subject_refid'] = $rs_subj->ref_id;
						$get['is_deleted'] = 0;
						$exist = $this->M_curriculum_subjects->pull($get);
						if($exist){ continue; }

						unset($data);
						$data['curriculum_id'] = $id;
						$data['subject_id'] = $rs_subj->id;
						$data['subject_refid'] = $rs_subj->ref_id;
						$data['year_id'] = $year_id;
						$data['semester_id'] = $semester_id;

						$rs = (object)$this->M_curriculum_subjects->insert($data);
						if($rs->status){

							activity_log('create curriculum subject',$this->userlogin,'Data : '. arr_str($data)); /*SAVE ACTIVITY*/

							$ctr++;
						}
					}

					$this->M_curriculum->update_curriculum_units($id);

					if($ctr > 0){

						$this->_msg('s','curriculum Subjects was successfully added.', current_url());
					}else{
						$this->_msg('e','No subject was added. Maybe because the subject you have selected was already added. Please try again.', current_url());
					}
				}

				$this->M_curriculum->recalculate_units($id);
			}
		}

		// CATCH THE SAVING OF SINGLE SUBJECTS
		if(isset($_POST['event_action']) && $_POST['event_action'] === "save_single"){
			$subject_refid = $this->input->post('subject_id');
			$year_id = $this->input->post('year_id');
			$semester_id = $this->input->post('semester_id');
			
			if($this->form_validation->run('curriculum_add_subjects2') !== FALSE){
				$rs = $this->M_curriculum_subjects->add_curriculum_subject($subject_refid, $this->input->post(), $id);
				$this->_msg($rs->code,$rs->msg, 'curriculum/add_subjects/'.$hash_id.'/single');
				$this->M_curriculum->recalculate_units($id); //recalculate units
			}
		}

		//CATCH THE EDITING OF SUBJECT
		if(isset($_POST['event_action']) && $_POST['event_action'] === "update_subject" && $c_subject){

			$year_id = $this->input->post('year_id');
			$semester_id = $this->input->post('semester_id');
			if($this->form_validation->run('curriculum_add_subjects3') !== FALSE){

				unset($data);
				$data['year_id'] = $year_id;
				$data['semester_id'] = $semester_id;
				$data['req_year_id'] = "";
				$data['total_units_req'] = "";
				$data['enrollment_units_req'] = "";
				if($this->input->post('req_year_id')){
					$data['req_year_id'] = $this->input->post('req_year_id');
				}
				if($this->input->post('total_units_req')){
					$data['total_units_req'] = floatval($this->input->post('total_units_req'));
				}
				if($this->input->post('enrollment_units_req')){
					$data['enrollment_units_req'] = floatval($this->input->post('enrollment_units_req'));
				}

				$rs = $this->M_curriculum_subjects->update($c_subject->id, $data);

				if($rs){

					// CHECK pre-requisite subject & insert
					$subject_pre = $this->input->post('subject_pre');
					$subject_pre_coincide = $this->input->post('subject_pre_coincide');
					if($subject_pre){

						//REMOVE CURRENT ADDED AND RE-INSERT
						$this->M_subject_pre_requisite->delete(array('curriculum_id'=>$id,'curriculum_sub_id'=>$c_subject->id));	

						foreach($subject_pre as $k => $subj_refid){

							$rs_subj_ref_id = $this->M_master_subjects->pull(array('ref_id'=>$subj_refid),'id,ref_id');
							if($rs_subj_ref_id){

								// check if already added
								unset($get);
								$get['where']['curriculum_id'] = $id;
								$get['where']['curriculum_sub_id'] = $c_subject->id;
								$get['where']['subject_refid'] = $subj_refid;
								$get['where']['is_deleted'] = 0;
								$get['single'] = true;
								$exist = $this->M_subject_pre_requisite->get_record(false, $get);
								if($exist){ continue; }

								//Save to subject_pre_requisite table
								unset($data);
								$data['curriculum_id'] = $id;
								$data['curriculum_sub_id'] = $c_subject->id;
								$data['subject_refid'] = $subj_refid;
								$data['subject_id'] = $rs_subj_ref_id->id;
								$data['taken_at_once'] = $subject_pre_coincide ? ( isset($subject_pre_coincide[$k]) ? 1 : 0 ) : 0 ;
								$rs_pre = (object)$this->M_subject_pre_requisite->insert($data);
								if($rs_pre->status){

									activity_log('create Subject pre-requisite',$this->userlogin,'ID : ' .$rs_pre->id. 'Data : '. arr_str($data)); /*SAVE ACTIVITY*/
								}
							}
						}
					}

					activity_log('Update curriculum subject',$this->userlogin,'ID : ' .$rs->id. 'Data : '. arr_str($data)); /*SAVE ACTIVITY*/

					$this->_msg('s','Curriculum Subject ('.$c_subject->code.') was successfully saved.', current_url());
				}
				else{
					$this->_msg('e','Curriculum Subject updating failed, please try again', current_url());
				}
			}
		}

		//CATCH THE SAVING OF CREATE SUBJECT OR SCHEDULE
		if(isset($_POST['event_action']) && $_POST['event_action'] === "create_subject"){

			$this->load->library(array('form_validation'));

			if($this->form_validation->run('master_subjects') !== FALSE){
				
				$result = (object)$this->M_master_subjects->save_subject($this->input->post());
				if($result->status){

					$rs_cs = $this->M_curriculum_subjects->add_curriculum_subject($result->ref_id, $this->input->post(), $id);
					$this->M_curriculum->recalculate_units($id); //recalculate units
					if($rs_cs->status){

						$this->_msg('s','Subject was successfully created and added to this curriculum', 'curriculum/add_subjects/'.$hash_id.'/single');
					}else{

						$this->_msg('s','Subject was successfully created but failed to be assigned to this curriculum. You can add the subject manually.', current_url());
					}
				}
			}
		}
	}

	// EDIT CURRICULUM
	public function edit($hashid = false, $tab = false){

		$id = $this->check_hash($hashid);

		// get curriculum
		$this->view_data['curriculum'] = $curriculum = $this->M_curriculum->get($id);
		$this->check_rec($curriculum);

		if($this->input->post()){
			$data['name'] = $this->input->post('name', true);
			$data['course_id'] = $this->input->post('course_id', true);
			$data['specialization_id'] = $this->input->post('specialization_id', true);

			if($this->form_validation->run('curriculum') !== FALSE){

				$rs = (object)$this->M_curriculum->update($id, $data);
				if($rs){
					activity_log('create curriculum',$this->userlogin,'Data : '. arr_str($data)); /*SAVE ACTIVITY*/
					$this->_msg('s','Curriculum was successfully updated', current_url());
				}else{
					$this->_msg('e','Something went wrong, please try again', current_url());
				}
			}
		}
	}

	/**
	 * Activate Selected Curriculum
	 */
	public function activate($curriculum_hash){

		$id = $this->check_hash($curriculum_hash);
		$c = $this->M_curriculum->get_curriculum($id);
		if($c==false){ show_404(); }

		$rs = $this->M_curriculum->activate($id);
		
		if($rs){
			$this->_msg('s', $c->name .' was successfully activated.', 'curriculum');
		}else{
			$this->_msg('e','Something went wrong, transaction not saved. Please try again.', 'curriculum');
		}
	}

	/**
	 * De-Activate Selected Curriculum
	 */
	public function deactivate($curriculum_hash){

		$id = $this->check_hash($curriculum_hash);
		$c = $this->M_curriculum->get_curriculum($id);
		if($c==false){ show_404(); }

		$rs = $this->M_curriculum->deactivate($id);
		
		if($rs){
			$this->_msg('s', $c->name .' was successfully deactivated.', 'curriculum');
		}else{
			$this->_msg('e','Something went wrong, transaction not saved. Please try again.', 'curriculum');
		}
	}

	public function remove_prerequisite($pre_id = false, $tab = false, $c_id = false)
	{
		if($pre_id === false){ show_404(); }

		$preq = $this->M_subject_pre_requisite->get($pre_id);
		if($preq === false){ show_404(); }

		$log['subject_pre_requisite Id'] = $preq->subject_id;
		$log['Subject Id'] = $preq->subject_id;
		$log['Curriculum Id'] = $preq->curriculum_id;
		$log['Curriculum Subjects Id'] = $preq->curriculum_sub_id;

		$rs = $this->M_subject_pre_requisite->delete($pre_id);

		if($rs){

			activity_log('Delete Subject pre-requisite', $this->userlogin, 'Data : '.arr_str($log));
			$this->_msg('s','Pre-requisite subject was successfully removed.', 'curriculum/edit/'.$c_id.'/'.$tab);
		}else{
			$this->_msg('e','Pre-requisite subject was not deleted. Someting went wrong please try again.', 'curriculum/edit/'.$c_id.'/'.$tab);
		}
	}

	public function remove_prerequisite2($pre_id = false, $c_id = false, $c_sub_id = false, $tab = false)
	{
		if($pre_id === false){ show_404(); }

		$preq = $this->M_subject_pre_requisite->get($pre_id);
		if($preq === false){ show_404(); }

		$log['subject_pre_requisite Id'] = $preq->subject_id;
		$log['Subject Id'] = $preq->subject_id;
		$log['Curriculum Id'] = $preq->curriculum_id;
		$log['Curriculum Subjects Id'] = $preq->curriculum_sub_id;

		$rs = $this->M_subject_pre_requisite->delete($pre_id);

		if($rs){

			activity_log('Delete Subject pre-requisite', $this->userlogin, 'Data : '.arr_str($log));
			$this->_msg('s','Pre-requisite subject was successfully removed.', 'curriculum/edit_prerequisite/'.$c_id.'/'.$c_sub_id.'/'.$tab);
		}else{
			$this->_msg('e','Pre-requisite subject was not deleted. Someting went wrong please try again.', 'curriculum/edit_prerequisite/'.$c_id.'/'.$c_sub_id.'/'.$tab);
		}
	}

	public function remove_subject($c_hashid = false, $c_hashsub_id = false, $tab = false)
	{
		$c_id = $this->check_hash($c_hashid);
		$c_sub_id = $this->check_hash($c_hashsub_id);

		// get curriculum
		$rs_curriculum = $this->M_curriculum->get($c_id);
		if($rs_curriculum === false){ show_404(); }

		// get curriculum subject
		$rs_curriculum_subjects = $this->M_curriculum_subjects->get($c_sub_id);
		if($rs_curriculum_subjects === false){ show_404(); }
		
		unset($log);
		$log['Curriculum ID'] = $c_id;
		$log['Curriculum Subject ID'] = $c_sub_id;
		$log['Subject Id'] = $rs_curriculum_subjects->subject_id;

		unset($u);
		$u['is_deleted'] = 1;
		$u['deleted_by'] = $this->userid;
		$u['deleted_date'] = NOW;

		$rs = $this->M_curriculum_subjects->update($c_sub_id, $u);

		if($rs){

			// Remove Pre-requisite if any
			$this->M_subject_pre_requisite->remove_pre_requisite($c_id, $c_sub_id);

			//recalculate total units
			$this->M_curriculum->recalculate_units($c_id);

			activity_log('Delete Curriculum Subject', $this->userlogin, 'Data : '.arr_str($log));
			// DnARzAmY/edit_subject/K5W6wXyJ
			$this->_msg('s','Curriculum subject was successfully removed', 'curriculum/add_subjects/'.__link($c_id).'/'.$tab);
		}else{

			$this->_msg('e','Someting went wrong. Subject not deleted, please try again', 'curriculum/add_subjects/'.__link($c_id).'/'.$tab);
		}
	}

	public function edit_prerequisite($c_id = false, $c_sub_id = false, $tab = false)
	{
		if($c_id === false){ show_404(); }
		if($c_sub_id === false){ show_404(); }

		$this->view_data['curriculum_id'] = $c_id;
		$this->view_data['curriculum_sub_id'] = $c_sub_id;
		$this->view_data['tab'] = $tab;

		// get curriculum
		$this->view_data['curriculum'] = $rs_curriculum = $this->M_curriculum->get_curriculum($c_id);
		if($rs_curriculum === false){ show_404(); }

		// get curriculum subject
		$this->view_data['subject'] = $rs_curriculum_subjects = $this->M_curriculum_subjects->get_profile($c_sub_id);
		if($rs_curriculum_subjects === false){ show_404(); }

		// get subject prerequisite
		$this->view_data['subject_prereq'] = $spre = $this->M_subject_pre_requisite->get_pre_requisite($c_id, $c_sub_id);

		unset($get);
		$get['where']['year_from'] = $this->cos->user->year_from;
		$get['where']['year_to'] = $this->cos->user->year_to;
		$get['where']['semester_id'] = $this->cos->user->semester_id;
		$this->view_data['subject_list'] = $sl = $this->M_subjects->get_record('subjects', $get);
		$this->view_data['subject_dropdown'] = $sd = $this->M_subjects->get_where_dd('subjects',$get,'Select Subject', array('id','sc_id','subject'));
		$this->view_data['subject_master'] = $s = $this->sort_data_alpha($sl, 'sc_id');

		if($this->input->post()){
			unset($data);
			$data['req_year_id'] = $this->input->post('req_year_id');
			$data['total_units_req'] = floatval($this->input->post('total_units_req'));
			$data['enrollment_units_req'] = floatval($this->input->post('enrollment_units_req'));

			$rs = $this->M_curriculum_subjects->update($c_sub_id, $data);

			// CHECK pre-requisite subject & insert
			$subject_pre = $this->input->post('subject_pre');
			if($subject_pre){
				foreach($subject_pre as $subj_id){

					if($subj_id){

						// check if already added
						unset($get);
						$get['where']['curriculum_id'] = $c_id;
						$get['where']['curriculum_sub_id'] = $c_sub_id;
						$get['where']['subject_id'] = $subj_id;
						$get['single'] = true;
						$exist = $this->M_subject_pre_requisite->get_record(false, $get);
						if($exist){ continue; }

						unset($data);
						$data['curriculum_id'] = $c_id;
						$data['curriculum_sub_id'] = $c_sub_id;
						$data['subject_id'] = $subj_id;
						$rs_pre = (object)$this->M_subject_pre_requisite->insert($data);
						if($rs_pre->status){

							activity_log('create Subject pre-requisite',$this->userlogin,'ID : ' .$rs_pre->id. 'Data : '. arr_str($data)); /*SAVE ACTIVITY*/
						}
					}
				}
			}

			$this->_msg('s','Subject Prerequisite was successfully saved',current_url());
		}
	}

	public function print_curriculum($hashid = false)
	{
		$id = $this->check_hash($hashid);

		// get curriculum
		$this->view_data['curriculum'] = $curriculum = $this->M_curriculum->get_curriculum($id);
		$this->view_data['tab'] = $this->M_years->get_first_record_id();
		if($curriculum === false){ show_404(); }

		$this->layout_view = 'blank';

		$this->view_data['curriculum_id'] = $id;

		// get subjects
		$this->view_data['curriculum_subjects'] = $cs = $this->M_curriculum_subjects->get_curriculum_subjects($id);
		
		$this->view_data['get_pre_requisite'] = function($c_id, $s_id){return $this->M_subject_pre_requisite->get_pre_requisite($c_id, $s_id);};
		
		$this->load->helper('print');

		$html = $this->load->view('curriculum/print_curriculum', $this->view_data, true);
		// echo $html;
		// exit(0);
		$this->load->library('mpdf');
		
		$mpdf=new mPDF('','FOLIO','','',15,5,5,5,0,0); 

		$mpdf->AddPage('P');

		$mpdf->WriteHTML($html);

		$mpdf->Output();

		// exit();
	}

	/**
	 * View All Subjects of Curicullum
	 * @param int $id Curicullum ID
	 */
	public function subjects($id = false)
	{
		if($id == false) { show_404(); }		
		$this->M_curriculum->recalculate_units($id);

		// get curriculum
		$this->view_data['curriculum'] = $curriculum = $this->M_curriculum->get_curriculum($id);
		$this->view_data['tab'] = $this->M_years->get_first_record_id();
		if($curriculum === false){ show_404(); }

		$this->view_data['curriculum_id'] = $id;

		// get subjects
		$this->view_data['curriculum_subjects'] = $cs = $this->M_curriculum_subjects->get_curriculum_subjects($id);	
		$this->view_data['get_pre_requisite'] = function($c_id, $s_id){return $this->M_subject_pre_requisite->get_pre_requisite($c_id, $s_id);};
	}

	public function recalculate_units($hashid=false)
	{
		$id = $this->check_hash($hashid);
		$rs = $this->M_curriculum->recalculate_units($id);
		if($rs){
			$this->_msg('s','Curriculum units was successfully recalculated.', 'curriculum/add_subjects/'.$hashid);
		}
		$this->_msg('e','Transaction proccess failed, please try again', 'curriculum/add_subjects/'.$hashid);
	}
}
?>
