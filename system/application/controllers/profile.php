<?php
	
class Profile Extends MY_Controller
{
		public function __construct()
		{
			parent::__construct();
			$this->load->helper(array('url','form','my_dropdown','url_encrypt'));
			$this->load->library(array('form_validation','token'));
			$this->load->model(array('M_settings','M_enrollments', "M_issues","M_users"));
			$this->session_checker->open_semester();
		}
		
		public function index()
		{
			show_404();
		}
		
		public function view($id = false, $search_type = "all")
		{
			if($id == false) { show_404(); }

			//IF THIS METHOD IS USED BUT NOT INCLUDED IN THE CURRENT DEPARTMENT MENU LIST
			//PARAM(string or array) - PUT CONTROLLERS THE DEPARTMENT CAN ACCESS TO OVERRIDE THE ACCESS PAGE
			$this->check_access_studentprofile_menu();
			
			//CHECK DEPARTMENT STUDENT PROFILE ACCESS 
			$this->check_stud_access('stud_profile');
			
			#load student library
			set_time_limit(0);
			$this->load->library('_student',array('enrollment_id'=>$id));
			$this->view_data['_student'] = $this->_student;
			$this->view_data['enroll'] = $this->_student->profile;
			$this->token->set_token();
			
			//Top Search Parameter
			$this->view_data['search_type'] = $search_type;
			
			$this->view_data['form_token'] = $this->token->get_token();
			$this->view_data['employeid'] = $id;
			$data =$id;
			
			if($_POST && $this->departments->stud_edit_profile === "1")
			{
				if($this->token->validate_token($this->input->post('form_token',TRUE))){
				
					if($this->form_validation->run('mavc_enrollment') !== FALSE)
					{
						$profile_id = $this->input->post('profile_id');
						$enrollment_id = $data = $this->input->post('enrollment_id');
						$form_token = $this->input->post('form_token');
						$x_enrollment = $this->M_enrollments->get($enrollment_id); if($x_enrollment === false){ show_404(); }
						
						$s = $this->M_enrollments->update_profile($this->input->post(), $id);
						$this->_msg($s->code,$s->msg,current_url());

						$dob = DateTime::createFromFormat("m/d/Y",$this->input->post('date_of_birth'));
						
						$for_enrollment_table = array
							(
								'course_id' => $this->input->post('course'),
								'status' => $this->input->post('student_type'),
								'semester_id' => $this->input->post('semester'),
								'year_id' => $this->input->post('year'),
								'sy_from' => $this->input->post('sy_from'),
								'sy_to'  => $this->input->post('sy_to'),
								'fname' => $this->input->post('fname'),
								'middle' => $this->input->post('middle'),
								'lastname'  => $this->input->post('lastname'),
								'name' => $this->input->post('lastname').', '.$this->input->post('middle').' '.$this->input->post('fname'),
								'civil_status' => $this->input->post('civil_status'),
								'date_of_birth' => $dob->format('Y-m-d'),
								'place_of_birth' => $this->input->post('place_of_birth'),
								'age' => $this->input->post('age'),
								'sex' => $this->input->post('gender'),
								'disability' => $this->input->post('disability'),
								'nationality' => $this->input->post('nationality'),
								'religion' => $this->input->post('religion'),
								'mobile' => $this->input->post('mobile'),
								'fake_email' => $this->input->post('fake_email'),
								'present_address' => $this->input->post('present_address'),
								'father_name' => $this->input->post('father_name'),
								'father_occupation' => $this->input->post('father_occupation'),
								'father_contact_no' => $this->input->post('father_contact_no'),
								'mother_name' => $this->input->post('mother_name'),
								'mother_occupation' => $this->input->post('mother_occupation'),
								'mother_contact_no' => $this->input->post('mother_contact_no'),
								'parents_address' => $this->input->post('parents_address'),
								'guardian' => $this->input->post('guardian'),
								'guardian_relation' => $this->input->post('guardian_relation'),
								'guardian_contact_no' => $this->input->post('guardian_contact_no'),
								'guardian_address' => $this->input->post('guardian_address'),
								'elementary' => $this->input->post('elementary'),
								'elementary_address' => $this->input->post('elementary_address'),
								'elementary_date'  => $this->input->post('elementary_date'),
								'secondary' => $this->input->post('secondary'),
								'secondary_address' => $this->input->post('secondary_address'),
								'secondary_date' => $this->input->post('secondary_date'),
								'tertiary' => $this->input->post('tertiary'),
								'tertiary_address' => $this->input->post('tertiary_address'),
								'tertiary_date' => $this->input->post('tertiary_date'),
								'tertiary_degree' => $this->input->post('tertiary_degree'),
								'vocational' => $this->input->post('vocational'),
								'vocational_address' => $this->input->post('vocational_address'),
								'vocational_date' => $this->input->post('vocational_date'),
								'vocational_degree' => $this->input->post('vocational_degree'),
								'others' => $this->input->post('others'),
								'others_address' => $this->input->post('others_address'),
								'others_date' => $this->input->post('others_date'),
								'others_degree' => $this->input->post('others_degree')
							);
						
							if($this->token->validate_token($form_token) == true)
							{
								if($this->M_enrollments->update_table($for_enrollment_table,$enrollment_id) == true)
								{
									log_message('error','Update Enrollment Table by: '.$this->user. ';FAILED Enrollment Id: '.$enrollment_id.';');
									$this->view_data['system_message'] = '<div class="alert alert-success">Successfully updated enrollment.</div>';

									// update users table also for email and name
									unset($for_users);
									$for_users['name'] = $for_enrollment_table['name'];
									$for_users['email'] = $for_enrollment_table['fake_email'];
									$rs = $this->M_users->update($x_enrollment->user_id, $for_users);
									
								}else
								{	
									log_message('error','Update Enrollment Table by: '.$this->user. ';FAILED Enrollment Id: '.$enrollment_id.';');
									$this->view_data['system_message'] = '<div class="alert alert-danger">Failed to update student profile.</div>';
								}
							}else{
								 $this->view_data['system_message'] = '<div class="alert alert-danger">Invalid Form Submit</div>';
							}
							
					}

				}else{
					$this->token->destroy_token();
					$this->_msg('e','Form security has been breached', current_url());
				}
			}
			if($data !== NULL){
			$this->load->model(array('M_years','M_semesters','M_courses','M_student_subjects','M_student_totals'));
			$this->view_data['student_profile'] = $this->view_data['student'] = $p = $this->M_enrollments->profile($data); // student profile
			// $this->view_data['subjects']	 = $subjects =  $this->M_student_subjects->get_studentsubjects($data);
			
			// $subject_units = 0;
			// $subject_lab = 0;
			// $subject_lec = 0;
			
			// if($subjects){
			// 	foreach($subjects as $obj){
			// 		$subject_units += $obj->units;
			// 		$subject_lab += $obj->lab;
			// 		$subject_lec += $obj->lec;
			// 	}
			// }
			
			// $this->view_data['subject_units']['subject_units'] = $subject_units;
			// $this->view_data['subject_units']['total_lab'] = $subject_lab;
			// $this->view_data['subject_units']['total_lec'] = $subject_lec;
			// $this->view_data['total_lec'] = $subject_lec;
			// $this->view_data['total_lab'] = $subject_lab;
			
			$this->view_data['course'] = $p->course_id;
			$this->view_data['year'] = $p->year_id;
			$this->view_data['semester'] = $p->sem_id;
			$this->view_data['enrollment_id'] = $p->id;
			
			// Ajax Subject Deletion
		  $this->view_data['eid'] = $p->id;
		  $this->view_data['y'] = $p->year_id;
		  $this->view_data['c'] = $p->course_id;
		  $this->view_data['s'] = $p->semester_id;
			
			// Count Issues
	    $this->issue_count($p->user_id);
	    
			}else{
				show_404();
			}
		}
		
#		public function destroy_subject($id)
#		{		
#		  vd(current_url());
#			$jquery = $this->input->post('jquery');
#			if($jquery == false)
#			{
#				$this->load->model('M_student_subjects');
#				if($this->M_student_subjects->destroy_subject($id))
#				{
#					$this->session->set_flashdata('system_message', '<div class="alert alert-success">Update was successful</div>');
#					redirect(current_url());
#				}else
#				{
#					 $this->view_data['system_message'] = '<div class="alert alert-danger">An error was encountered while proccessing your request</div>';
#				}
#			}else
#			{
#				$this->load->model('M_student_subjects');
#				if($this->M_student_subjects->destroy_subject($id))
#				{
#					echo 'true';
#					die();//stops php from sending the views so that browser will only receive true
#				}else
#				{
#					echo 'false';
#					die();//stops php from sending the views so that browser will only receive false
#				}
#			}
#		}

    public function destroy_subject($y,$c, $s,$eid, $id,  $subject_id)
    {
      $this->load->model(array('M_student_subjects', "M_subjects", "M_enrollments"));
      $this->M_student_subjects->destroy_subject($id);

      if ($this->M_enrollments->check_if_paid($eid)) {
		    $this->M_subjects->recalculate_subject_load($subject_id, 'add');
		  }
      $units = $this->M_student_subjects->get_units($eid);
      $data_units['units'] = $units->total_units;
      $data_units['labs'] = $units->total_labs;

 
      $this->session->set_flashdata('system_message', '<div class="alert alert-success">Successfully deleted student subject.</div>');
		  //redirect("/subjects/add_subject/$y/$c/$s/$eid");
      echo json_encode($data_units);
    }

    public function activate_portal($hash_id='', $type = false)
    {
    	$id = $this->check_hash($hash_id);
    	if($type == "0" OR $type == "1"){}
    	else{ show_404(); }
    	$this->check_access_studentprofile_menu();

    	//CHECK DEPARTMENT STUDENT PROFILE ACCESS 
			$this->check_stud_access('stud_portal_activation');

    	$this->load->model('M_users');
    	$r = $this->M_users->activate_by_enrollment_id($id,$type);
    	$this->_msg($r->code,$r->msg,'profile/view/'.$id);
    }
}
