<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class International_student Extends MY_Controller
{
	public function __construct(){
		parent::__construct();
		$this->session_checker->open_semester();
		$this->load->model(array('M_enrollments'));
		$this->load->helper(array('url_encrypt'));
		$this->menu_access_checker();
		$this->load->model('M_core_model');
		$this->load->helper('my_dropdown');
		
	}
	
	public function index($page = 0)
	{	
		//PAGINATION
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		
		$filter = false;
		$like = false;
		$order_by = false;
		
		$filter['enrollments.semester_id'] = $this->open_semester->id;
		$filter['enrollments.sy_from'] = $this->open_semester->year_from;
		$filter['enrollments.sy_to'] = $this->open_semester->year_to;
		$filter['enrollments.is_deleted'] = 0;
		$filter['enrollments.is_paid'] = 1;
		$filter['enrollments.is_foreigner'] = 1;
		
		
		$arr_filters = array();
		$suffix = "";
		
		if($_GET)
		{
			if(isset($_GET['lastname'])){
				$this->view_data['lastname'] = $lastname = trim($_GET['lastname']);
				$like['enrollments.lastname'] = $lastname;
				$arr_filters['lastname'] = $lastname;
			}
			
			if(isset($_GET['fname'])){
				$this->view_data['fname'] = $fname = trim($_GET['fname']);
				$like['enrollments.fname'] = $fname;
				$arr_filters['fname'] = $fname;
				
			}
			
			if(isset($_GET['studid'])){
				$this->view_data['studid'] = $studid = trim($_GET['studid']);
				$like['enrollments.studid'] = $studid;
				$arr_filters['studid'] = $studid;
			}
			
			if(isset($_GET['year_id'])){
				$this->view_data['year_id'] = $year_id = trim($_GET['year_id']);
				$filter['enrollments.year_id'] = $year_id;
				$arr_filters['year_id'] = $year_id;
			}
			
			$suffix = array_to_geturl($arr_filters);
		}
		
		//CONFIGURATION
		$get['fields'] = array(
				'enrollments.id',
				'enrollments.studid',
				'enrollments.nationality',
				'enrollments.name' ,
				'years.year',
				'courses.course' 
		);
		
		$get['where'] = $filter;
		$get['like'] = $like;
		
		$get['join'] = array(
			
			1 => array(
				"table" => "courses",
				"on"	=> "courses.id = enrollments.course_id",
				"type"  => "LEFT"
			),
			2 => array(
				"table" => "years",
				"on"	=> "years.id = enrollments.year_id",
				"type"  => "LEFT"
			)
		);
		$get['order'] = "enrollments.name";
		$get['all'] = true; //GET ALL EXCLUDE LIMIT
		$get['count'] = true; //RETURN COUNT NOT ROW
		$get['array'] = false; //RETURN OBJECT NOT ARRAY
		$get['single'] = false; //RETURN ALL NOT SINGLE
		
		
	
		$this->load->library("pagination");
		$config = $this->pagination_style();
		$config["base_url"] = base_url() ."international_student/index";
		$config['suffix'] = '?'.http_build_query($_GET, '', "&");
		$config['first_url'] = $config['base_url'].$config['suffix']; 
		
		$this->view_data['total_rows'] = $config["total_rows"] = $this->M_core_model->get_record("enrollments", $get);
		
		$get['count'] = false; //TO RETURN ALL ROWS
		$this->view_data['all_rows'] = $all_rows = $this->M_core_model->get_record("enrollments", $get);
		
		$config["per_page"] = 25;
		$config['num_links'] = 10;
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		
		//FOR PAGINATION
		$get['all'] = false;
		$get['count'] = false;
		if($this->input->post('submit') == "Print")
		{
			$get['all'] = true;
		}
		$config['start'] = $page;
		$config['limit'] = $config['per_page'];
		
		$get['start'] = $page;
		$get['limit'] = $config['per_page'];
		
		$this->view_data['search'] = $search = $this->M_core_model->get_record("enrollments", $get);
		$this->view_data['links'] = $this->pagination->create_links();
		
		if($this->input->get('submit') == "Print")
		{
			$this->print_international_students($this->view_data);
		}
	}
	
	public function print_international_students($data)
	{
		if($data == false) {show_404();}
		
		if($data['total_rows'] <= 0) {	
			$this->view_data['system_message'] = '<div class="alert alert-danger">No record to be printed.</div>';
			return false;
		}
		
		$search = $data['all_rows'];
		$total_records = $data['total_rows'];
		
		if($search)
		{
			$this->load->helper('print');
			
			$html = _html_international_students($search, $total_records); 

			$this->load->library('mpdf');
			
			$mpdf=new mPDF('','FOLIO','','',10,5,5,5,0,0); 

			$mpdf->AddPage('P');

			$mpdf->WriteHTML($html);

			$mpdf->Output();
		}
	}	
}
