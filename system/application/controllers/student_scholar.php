<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Student_scholar extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->session_checker->open_semester();
		//$this->session_checker->secure_page('all');
		$this->menu_access_checker();
	}
	
	public function index($page = 0)
	{
		$this->load->model(array('M_enrollments'));
		$this->load->helper(array('my_url'));
		
		//PAGINATION
		
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		
		$filter = false;
		$like = false;
		$filter['enrollments.is_deleted'] = 0;
		$filter['enrollments.is_scholar'] = 1;
		$filter['enrollments.semester_id'] = $this->open_semester->id;
		$filter['enrollments.sy_from'] = $this->open_semester->year_from;
		$filter['enrollments.sy_to'] = $this->open_semester->year_to;
		$all = false;
		if($_POST){
			
			
			$this->view_data['lastname'] = $lastname = trim($this->input->post('lastname'));
			$this->view_data['fname'] = $fname = trim($this->input->post('fname'));
			$this->view_data['studid'] = $studid = trim($this->input->post('studid'));
			if($lastname != ""){
				$like['enrollments.lastname'] = $lastname;
			}
			if($fname != ""){
				$like['enrollments.fname'] = $fname;
			}
			if($studid != ""){
				$like['enrollments.studid'] = $studid;
			}
			
			$page = 0;
			
			if($this->input->post('submit') == "Print")
			{
				$all = true;
			}
		}
		
		$this->load->library("pagination");
		$config = $this->pagination_style();
		$config["base_url"] = base_url() ."student_scholar/index";
		$this->view_data['total_rows'] = $config["total_rows"] = $this->M_enrollments->find(0,0, $filter, true, true, $like);
		$config["per_page"] = 3;
		$config['num_links'] = 10;
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		$this->view_data['search'] = $search = $this->M_enrollments->find($page,$config["per_page"], $filter, $all, false, $like);
		
		$this->view_data['links'] = $this->pagination->create_links();
		
		
		if($this->input->post('submit') == "Print")
		{
			$this->print_scholar($this->view_data);
		}
		
	}
	
	public function print_scholar($data = false)
	{
		if($data == false) {show_404();}
		
		if($data['total_rows'] <= 0) {	
			$this->view_data['system_message'] = '<div class="alert alert-danger">No record to be printed.</div>';
			return false;
		}
		
		$search = $data['search'];
		
		if($search)
		{
			$this->load->helper('print');
			
			$html = _html_student_scholars($data); 

			$this->load->library('mpdf');
			
			$mpdf=new mPDF('','FOLIO','','',15,5,5,5,0,0); 

			$mpdf->AddPage('P');

			$mpdf->WriteHTML($html);

			$mpdf->Output();
		}
	}
}

?>