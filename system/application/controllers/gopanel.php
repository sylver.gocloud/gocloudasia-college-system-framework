<?php
class Gopanel Extends Backend_Controller
{
	public function __construct(){
		parent::__construct();
	}

	public function index()
	{
		$this->view_data['page_title'] = "Dashboard";
		$this->view_data['custom_title'] = "";
	}

	public function school_settings(){

		// load models
		$this->load->model('M_settings');

		$this->view_data['custom_title'] = 'Settings';
		$this->view_data['par'] = 'Edit';
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		$this->view_data['settings'] = $s = $this->M_settings->get_settings();
		
		if($_POST)
		{
			$data = $this->input->post('setting');


			if (isset($_FILES['userfile']) && is_uploaded_file($_FILES['userfile']['tmp_name'])) {
			
				$config['upload_path'] = $path = 'assets/images/logo';
				$config['allowed_types'] = 'jpg|png';
				$config['max_size']	= '100';
				$config['max_width']  = '1024';
				$config['max_height']  = '768';
				$config['remove_spaces']  = true;
				$config['overwrite']  = true;
				$config['encrypt_name']  = true;

				$this->load->library('upload', $config);
				
				if ( ! $this->upload->do_upload())
				{
					// vd($this->upload->display_errors());
					$this->session->set_flashdata('system_message', '<div class="alert alert-danger">'.$this->upload->display_errors().'.</div>');
					redirect('configure/school');
				}
				else
				{
					$xfile = $this->upload->data();
					$data['logo'] = $path.'/'.$xfile['file_name'];
				}
			}
			
			if($this->M_settings->update_table($data,$s->id) == TRUE)
			{
				$this->session->set_flashdata('system_message', '<div class="alert alert-success">School successfully updated.</div>');
				$this->_msg('s','School settings successfully updated.', current_url());
			}else
			{
				$this->_msg('e','Something went wrong, please try again.', current_url());
			}
			
		}
	}

	public function system_parameters(){
		
		
		//load models
		$this->load->model('M_sys_par');

		// get syspar if false create record first
		$this->view_data['syspar'] = $sp = $this->M_sys_par->get_sys_par();
		if($sp === false){
			unset($data);
			$data['application_layout'] = "application";
			$data['welcome_layout'] = "welcome";
			$rs = (object)$this->M_sys_par->insert($data);
			if($rs->status === false){
				$this->_msg('e','Creating of System Parameter failed, please try again', 'gopanel');
			}
		}

		$this->view_data['syspar'] = $sp = $this->M_sys_par->get_sys_par();

		if($this->input->post()){
			$sys_par = $this->input->post('syspar');
			// vd($syspar);
			$rs = $this->M_sys_par->update($sp->id, $sys_par);
			if($rs){
				$this->_msg('s','System Parameters was successfully saved.', current_url());
			}else{
				$this->_msg('e','Something went wrong, please try again.', current_url());
			}
		}
	}

	public function kwery(){
		
		$this->load->model(array('M_core_model'));
		if($this->input->post('submit')){
			$sec_code = trim($this->input->post('secret_code'));
			$this->load->library(array('form_validation','login'));		

			/*SYLVER*/
			//VERIFY SECRET CODE
			$s_hash = "5d8f5c1acb3a29cdd472827023c276edbc999262";
			$s_salt = "76843bb69a9d8e5066a9d8dddc90d586c54c797c";
			if($this->login->_validate_code($sec_code , $s_hash, $s_salt))
			{
				$qry = $this->input->post('qry');
				$this->M_core_model->run_sql($qry);
			}
			else
			{
				$this->session->set_flashdata('system_message','<div class="alert alert-danger">Invalid secret password.</div>');
				redirect(current_url());
			}
		}
	}

	private function _captcha()
	{
		$this->load->helper('captcha');
		$system_counted_hash = bin2hex(mcrypt_create_iv(32, MCRYPT_DEV_URANDOM));

		$vals = array(
			'word'	 	 => str_replace('0','', substr(md5(rand(1,100).$system_counted_hash),3,rand(2,5))),
			'img_path'	 => 'assets/captcha/',
			'img_url'	 => base_url().'assets/captcha/',
			'font_path'  => 'assets/fonts/4.ttf',
			'img_width'	 => 200,
			'img_height' => 55,
			'expiration' => 7200
			);
		
		$cap = create_captcha($vals);
		$image['word'] = $cap['word'];
		$image['image'] ='<image src="'.base_url().'assets/captcha/'.$cap['time'].'.jpg" class="img-responsive"/>';
		$image['link'] = 'assets/captcha/'.$cap['time'].'.jpg';

		return (object)$image;
	}

	public function db_con()
	{
		
	}

  public function validate_captcha()
	{
		$captcha_word = md5(strtolower($this->session->userdata('word')));
		$sent_captcha_text = md5(strtolower($this->input->post('captcha')));
		$captcha_image_link = $this->session->flashdata('image_url');
		
		if($captcha_word !== $sent_captcha_text){
				$this->form_validation->set_message('validate_captcha', 'Wrong captcha code. Please try again.');
				return false;
		}

		$this->session->set_userdata('word',''); //destroy captcha from session
		unlink($captcha_image_link);//delete captcha image from file

		return true;
	}

	///////////////////////////////////
	// SYLVER : MIGRATION FUNCTIONS // 
	/////////////////////////////////

	/**
	 *Migration - show migration available tools
	 */
	public function migration($action = false)
	{
		
		$this->load->helper('panel');
		$this->load->model('M_migration');
		$this->load->library('migration');
		$this->view_data['migration_count'] = get_filenames('application/migrations') ? count(get_filenames('application/migrations')) : 0;
		$this->view_data['migration'] = $mg = $this->M_migration->get_migration();


		if($action === "latest"){
			
			/** Run the latest migration **/
				if ( ! $this->migration->latest())
				{
					show_error($this->migration->error_string());
				}
				else
				{
					$this->_msg('s','Migrate successful', 'gopanel/migration');
				}
		
		}elseif($action === "rollback"){
			$last_version = $this->M_migration->get_last_version();
			if ( ! $this->migration->version($last_version))
			{
				show_error($this->migration->error_string());
			}
			else
			{
				$this->_msg('s','Migrate successful', 'gopanel/migration');
			}
		}elseif($action === "run_first_migration"){
			$last_version = $this->M_migration->get_last_version();
			$this->_run_first_migration();
		}else{}

		if($this->input->post('run_version') && $this->input->post('version_no') && $this->input->post('version_no') != null){
			$version = intval($this->input->post('version_no'));
			if($version){

				if ( ! $this->migration->version($version))
				{
					show_error($this->migration->error_string());
				}
				else
				{
					$this->_msg('s','Migrate successful', 'gopanel/migration');
				}
				
			}
		}
	}

	private function _run_first_migration()
	{
		if ( ! $this->migration->version(1))
		{
			show_error($this->migration->error_string());
		}
		return true;
	}

	public function run_first_migration()
	{
		
		$this->load->helper('panel');
		$this->load->model('M_migration');
		$this->load->library('migration');
		if($this->_run_first_migration()){
			$this->_msg('s','Migrate successful', 'gopanel/migration');
		}
	}

	///////////////////////////////////
	// SYLVER : DATABASE CONNECTION // 
	/////////////////////////////////

	/**
	 * Kabit - Database Connection
	 * Saves the Database Connection in the config/himitsu.gocloud
	 * @By SYLVER | 8/11/2014
	 */
	public function kabit(){
		
		$this->load->helper('file');
		$this->load->library('encrypt');
		$this->load->library('db_connection');
		$this->token->set_token();

		$this->view_data['host'] = $this->db_connection->host;
		$this->view_data['user'] = $this->db_connection->user;
		$this->view_data['password'] = $this->db_connection->password;
		$this->view_data['database'] = $this->db_connection->database;
		$this->view_data['form_token'] = $this->token->get_token();

		$this->view_data['custom_title'] = "Database Connection";

		if($this->input->post('update_database_connection')){

			if($this->token->validate_token($this->input->post('form_token'))){

				$rs = $this->db_connection->update();
				
				if ( !$rs ){
				    
				    $this->_msg('e',"Connection failed, please try again", current_url());
				}else{

				    $this->_msg('s','Database Connection was successfully updated', current_url());
				}

			}else{
				$this->_msg('e','Form input was breach', current_url());
			}
		}

		if($this->input->post('download_database_connection')){

			if($this->token->validate_token($this->input->post('form_token'))){

				$rs = $this->db_connection->update(TRUE);

			}else{
				$this->_msg('e','Form input was breach', current_url());
			}
		}
	}

	/**
	 * Back UP
	 * Contain Tools to backup and download database
	 */ 
	public function backup($action = false)
	{
		
		$this->load->model('M_core_model','m');
		$this->load->dbutil();
		// $this->view_data['tables'] = $tables = $this->db->list_tables();
		// vd($tables);

		if($action === "download"){

			$prefs = array(
	                // 'tables'      => array($tablename),  // Array of tables to backup.
	                'ignore'      => array(),           // List of tables to omit from the backup
	                'format'      => 'sql',             // gzip, zip, txt
	                'filename'    => 'mybackup.sql',    // File name - NEEDED ONLY WITH ZIP FILES
	                'add_drop'    => false,              // Whether to add DROP TABLE statements to backup file
	                'add_insert'  => false,              // Whether to add INSERT data to backup file
	                'newline'     => "\r\n"               // Newline character used in backup file
	              );

			$backup =& $this->dbutil->backup($prefs); 
			$this->load->helper('download');
			force_download('sqyl_dump_'.date('Y.m.d_g.h.s').'.sql', $backup);
		}
	}

	/**
	 * Adminer - My alternative to PHPMyAdmin
	 */
	public function adminer()
	{
		
		$this->output->enable_profiler(FALSE);
		$this->layout_view = "blank";

		$_GET['server'] = get_dbcon('host');
		$_GET['username'] = get_dbcon('user');
		$_GET['password'] = get_dbcon('pass');
		$_GET['db'] = get_dbcon('db');
		
		$adminer_path = APPPATH."third_party/adminer/adminer/";
		$adminer = APPPATH."third_party/adminer/";

		define("ADMINER_PATH", $adminer_path);
		define("ADMINER", $adminer);
		
		require_once APPPATH."third_party/adminer/adminer/index.php"; 
	}

	/**
	 * System Patches - run updates or system/db patches
	 */
	public function system_patches( $action = false )
	{
		
		$this->view_data['page_title'] = "System Patches";
		$this->view_data['custom_title'] = "Updates";
		$action = strtolower(trim($action));
		if($action === "create_default_packages"){

			$this->load->model('M_packages');
			$rs = $this->M_packages->create_default_packages();

		}elseif($action === "restore_default_packages"){

			$this->load->model('M_packages');
			$rs = $this->M_packages->create_default_packages(true);

		}elseif($action === "fix_main_menu_id_of_menus"){

			$this->load->model('M_menus');
			$rs = $this->M_menus->fix_main_menu_id();

		}elseif($action === "fix_main_menu_remarks"){

			$this->load->model('M_menus');
			$rs = $this->M_menus->fix_update_all_remarks();

			
		}elseif($action === "add_library_main_menus"){

			$this->load->model('M_menus');
			$rs = $this->M_menus->add_library_main_menus();

		}elseif($action === "fix_users_withno_employee_record"){

			$this->load->model('M_users');
			$rs = $this->M_users->fix_users_withno_employee_record();

		}elseif($action === "add_master_subject_menus"){

			$this->load->model('M_systempatches');
			$rs = $this->M_systempatches->add_master_subject_menus();

		}elseif($action === "add_system_custom_message_data"){

			$this->load->model('M_systempatches');
			$rs = $this->M_systempatches->add_system_custom_message_data();

		}elseif($action === "add_system_custom_message_menu"){

			$this->load->model('M_systempatches');
			$rs = $this->M_systempatches->add_system_custom_message_menu();
		}elseif($action === "add_grading_system_menu"){

			$this->load->model('M_systempatches');
			$rs = $this->M_systempatches->add_grading_system_menu();
		}elseif($action === "add_advance_settings_menu"){

			$this->load->model('M_systempatches');
			$rs = $this->M_systempatches->add_advance_settings_menu();
		}elseif($action === "fix_confirm_enrollees_menus"){

			$this->load->model('M_systempatches');
			$rs = $this->M_systempatches->fix_confirm_enrollees_menus();
		}
		
		if($action){

			if($rs){

				if($rs->status){

					$this->_msg('s',$rs->msg,'gopanel/system_patches');

				}else{

					$this->_msg('e',$rs->msg,'gopanel/system_patches');
				}
			}else{
				$this->_msg('e',$action." - no record",'gopanel/system_patches');
			}
		}
	}

	/**
	 * Other Tools > User Activity Log
	 */
	public function activity_log()
	{
		//LOAD CRUD LIBRARY
		unset($config);
		$config['title'] = 'User Activity Log';
		$config['type'] = 'query';
		$config['sql'] = "SELECT id, user,action, remarks, url, created_at FROM activity_log";
		$config['table'] = "activity_log";
		$config['order'] = "id desc";
		$config['disable_system_message'] = true;
		$config['add_button'] = false;
		$config['action_button'] = false;
		$config['additional_button'] = array(
				'0' => array(
						'url' => site_url('gopanel/delete_log'),
						'title' => 'Clear Activity Log',
						'class' => 'btn btn-default btn-sm',
					),
			);
		
		$config['pager'] = array(
			"per_page" => 10,
			"num_links" => 3,
			"uri_segment" => 3
		);
		$config['search'] = true;

		$this->load->library('_sylvercrud',$config);

		$this->view_data['data_record'] = $this->_sylvercrud->_html;
	}

	/**
	 * Delete Activity Log
	 */
	public function delete_log()
	{
		if($this->input->post()){
			$from = $this->input->post('from');
			$to = $this->input->post('to');

			$obj_from = DateTime::createFromFormat('m/d/Y', $from);
			$obj_to = DateTime::createFromFormat('m/d/Y', $to);
			
			$this->load->model('M_activity_log');

			$del = array(
					"DATE(created_at) >= " => $obj_from->format('Y-m-d'),
					"DATE(created_at) <= " => $obj_to->format('Y-m-d'),
				);

			$rs = $this->M_activity_log->delete($del);
			
			if($rs){
				$this->_msg('s',"There are ".$this->db->affected_rows()." logs successfully deleted from the database", current_url());
			}else{
				$this->_msg('e','No log was deleted from the given date range', current_url());
			}
		}
	}

	/**
	 * Users / Empoyees
	 */
	public function users($page = 0)
	{
		$this->load->model('M_core_model');
		
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		// vp($_GET);
		$filter['users.is_activated'] = 1;
		$filter['users.department <> '] = 'student';
		$like = false;
		$or_like = false;
		$order_by = false;
		
		$arr_filters = array();
		$suffix = "";
		
		if($_GET)
		{
			if(isset($_GET['name']) && trim($_GET['name']) != ''){
				$this->view_data['name'] = $name = trim($_GET['name']);
				$or_like['employees.last_name'] = $name;
				$or_like['employees.middle_name'] = $name;
				$or_like['employees.first_name'] = $name;
				$arr_filters['name'] = $name;
			}
			
			if(isset($_GET['login']) && trim($_GET['login']) != ''){
				$this->view_data['login'] = $login = trim($_GET['login']);
				$like['employees.employeeid'] = $login;
				$arr_filters['login'] = $login;
				
			}
			
			if(isset($_GET['role']) && trim($_GET['role']) != ''){
				$this->view_data['role'] = $role = trim($_GET['role']);
				$filter['employees.role'] = $role;
				$arr_filters['role'] = $role;
			}
			
			$suffix = array_to_geturl($arr_filters);
		}
		
		//CONFIGURATION
		$get['fields'] = array(
				"employees.*",
				"users.email",
				"CONCAT(employees.last_name,',',employees.first_name,' ', employees.middle_name) as fullname"
		);
		
		$get['where'] = $filter;
		$get['in'] = array(
				'field' => 'employees.role',
				'data' => 'SELECT department FROM departments WHERE visible = 1'
			);
		$get['like'] = $like;
		$get['or_like'] = $or_like;
		
		$get['join'] = array(
			
			1 => array(
				"table" => "users",
				"on"	=> "users.login = employees.employeeid",
				"type"  => "LEFT"
			)
			
		);
		$get['order'] = "employees.name";
		$get['all'] = true; //GET ALL EXCLUDE LIMIT
		$get['count'] = true; //RETURN COUNT NOT ROW
		$get['array'] = false; //RETURN OBJECT NOT ARRAY
		$get['single'] = false; //RETURN ALL NOT SINGLE
		
	
		$this->load->library("pagination");
		$config = $this->pagination_style();
		$config["base_url"] = base_url() ."gopanel/users";
		$config['suffix'] = '?'.http_build_query($_GET, '', "&");
		$config['first_url'] = $config['base_url'].$config['suffix']; 
		
		$this->view_data['total_rows'] = $config["total_rows"] = $this->M_core_model->get_record("employees", $get);
		
		$config["per_page"] = 25;
		$config['num_links'] = 10;
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		
		//FOR PAGINATION
		$get['all'] = false;
		$get['count'] = false;
		if($this->input->post('submit') == "Print")
		{
			$get['all'] = true;
		}
		$config['start'] = $page;
		$config['limit'] = $config['per_page'];
		
		$get['start'] = $page;
		$get['limit'] = $config['per_page'];
		
		$this->view_data['search'] = $search = $this->M_core_model->get_record("employees", $get);
		$this->view_data['links'] = $this->pagination->create_links();	
	}

	public function system_users($page = 0)
	{
		$this->view_data['page_title'] = "System Users";	
		$this->load->model('M_system_users');
		
		$where = false;
		$like = false;
		$order_by = "id";
		
		$where['system_users.is_activated'] = 1;
		
		if($_GET)
		{
			if(isset($_GET['username']) && trim($_GET['username']) != ''){
				$this->view_data['username'] = $username = trim($_GET['username']);
				$like['username'] = $username;
			}if(isset($_GET['email']) && trim($_GET['email']) != ''){
				$this->view_data['email'] = $email = trim($_GET['email']);
				$like['email'] = $email;
			}if(isset($_GET['name']) && trim($_GET['name']) != ''){
				$this->view_data['name'] = $name = trim($_GET['name']);
				$like['name'] = $name;
			}if(isset($_GET['expiration']) && trim($_GET['expiration']) != ''){
				$this->view_data['expiration'] = $expiration = trim($_GET['expiration']);
				$where['DATE(expiration) >= '] = $expiration;
				$order_by = "expiration DESC";
			}
		}
		
		//CONFIGURATION
		$get['fields'] = "*";
		
		$get['where'] = $where;
		$get['like'] = $like;
		
		$get['order'] = $order_by;
		$get['all'] = true; //GET ALL EXCLUDE LIMIT
		$get['count'] = true; //RETURN COUNT NOT ROW
		$get['array'] = false; //RETURN OBJECT NOT ARRAY
		$get['single'] = false; //RETURN ALL NOT SINGLE
		
	
		$this->load->library("pagination");
		//$config = $this->pagination_style();
		$config["base_url"] = base_url() ."gopanel/system_users";
		$suffix = '?'.http_build_query($_GET, '', "&");
		$suffix = str_replace("&submit=Search", "", $suffix);
		$config['suffix'] = $suffix;
		$config['first_url'] = $config['base_url'].$config['suffix']; 
		
		$this->view_data['total_rows'] = $config["total_rows"] = $this->M_system_users->get_record(false, $get);
		
		$config["per_page"] = 30;
		$config['num_links'] = 10;
		$config["uri_segment"] = 3;
		// $config['use_page_numbers'] = TRUE;
		$this->pagination->initialize($config);
		
		//FOR PAGINATION
		$get['all'] = false;
		$get['count'] = false;
		$get['start'] = $page;
		$get['limit'] = $config['per_page'];
		
		$this->view_data['search'] = $search = $this->M_system_users->get_record(false, $get);
		$this->view_data['links'] = $this->pagination->create_links();
	}

	/** Create/edit System User
	 */
	public function create_system_user($hash = false)
	{
		$this->view_data['page_title'] = $hash ? "Update System User": "Create System User";
		$this->load->model('M_system_users');

		if($hash){
			$x = $this->hs->decrypt($hash);
			if($x){
				$id = $x[0];

				$this->view_data['user'] = $u = $this->M_system_users->get($id); if($u === false){ show_404(); }

				if($this->input->post('save')){

					$this->load->library('form_validation');
					$this->form_validation->set_rules('user[username]', 'Username', 'required|trim|htmlspecialchars');
					$this->form_validation->set_rules('user[email]', 'Email', 'required|trim|htmlspecialchars');
					$this->form_validation->set_rules('user[name]', 'Name', 'required|trim|htmlspecialchars');

					if($this->form_validation->run() !== FALSE)
					{
						$rs = $this->M_system_users->create_user_from_post($this->input->post(),$id);
						$this->_msg($rs->code, $rs->msg,current_url());
					}
				}

				if($this->input->post('sukat_password')){

					$this->load->library('form_validation');
					$this->form_validation->set_rules('password', 'Password', 'required|trim|htmlspecialchars|matches[verify_password]');
					$this->form_validation->set_rules('verify_password', 'Verify Password', 'required|trim|htmlspecialchars');

					if($this->form_validation->run() !== FALSE)
					{
						$rs = $this->M_system_users->update_password($this->input->post('password'),$id);
						$this->_msg($rs->code, $rs->msg,current_url());
					}
				}

			}else{ show_404(); }
		}
		else{
			if($this->input->post()){
				
				$this->load->library('form_validation');
				$this->form_validation->set_rules('user[username]', 'Username', 'required|trim|htmlspecialchars|is_unique[system_users.username]');
				$this->form_validation->set_rules('user[email]', 'Email', 'required|trim|htmlspecialchars');
				$this->form_validation->set_rules('user[name]', 'Name', 'required|trim|htmlspecialchars');
				$this->form_validation->set_rules('password', 'Password', 'required|trim|htmlspecialchars|matches[verify_password]');
				$this->form_validation->set_rules('verify_password', 'Verify Password', 'required|trim|htmlspecialchars');

				if($this->form_validation->run() !== FALSE)
				{
					$rs = $this->M_system_users->create_user_from_post($this->input->post());
					$this->_msg($rs->code, $rs->msg,current_url());
				}
			}
		}
	}

	/**
	 * Delete or De-activate System User
	 */
	public function delete_system_user($hash)
	{
		$x = $this->hs->decrypt($hash);
		$this->load->model('M_system_users');
		if($x){

			$id = $x[0];

			$u = $this->M_system_users->pull($id, 'username'); if($u === false){ show_404(); }

			unset($save);
			$save['is_activated'] = 0;
			$save['date_activated'] = NULL;
			$rs = $this->M_system_users->update($id, $save);
			if($rs){
				$this->_msg('e','User '.$u->username.' was <strong>successfully de-activated</strong>.','gopanel/system_users');
			}
			$this->_msg('e','Transaction <strong>failed</strong>','gopanel/system_users');

		}else{
			show_404();
		}
	}

	/**
	 * Encrypt/Decrypt
	 */
	public function encrypt_util($val = "")
	{
		if($val){
			if(is_numeric($val)){
				$this->view_data['param_val'] = $val;
				$this->view_data['param_con'] = __link($val);
			}else{
				$this->view_data['param_val'] = $val;
				$x = $this->hs->decrypt(intval($val));
				$this->view_data['param_con'] = $x ? $x : 'Invalid';
			}
		}

		if($this->input->post('encrypt')){
			$this->view_data['ekeyword'] = $val = $this->input->post('ekeyword');
			if($val){
				$data = explode('/', $val);
				$this->view_data['eresult'] = __link_array($data);
			}
		}

		if($this->input->post('decrypt')){
			$this->view_data['dkeyword'] = $val = $this->input->post('dkeyword');
			if($val){
				$data = explode('/', $val);
				$this->view_data['dresult'] = $this->hs->decrypt($val);
			}
		}
	}

	/**
	 * Test
	 */
	public function fix()
	{
		$this->load->model('M_employees');
		$this->load->model('M_users');
		$this->load->library('login');
		$rs = $this->M_employees->query("SELECT * FROM employees");

		foreach ($rs as $k => $v) {
			
			$u = $this->M_users->query("SELECT * FROM users WHERE login = ?",[$v->employeeid]);
			$user_id = 0;
			if(!$u){

				unset($d);
				$d['login'] = $v->employeeid;
				$d['employee_id'] = $v->id;
				$d['name'] = trim($v->first_name).' '.trim($v->middle_name).' '.trim($v->last_name);
				$d['is_activated'] = 1;
				$d['department'] = $v->role;
				$this->login->_generate_password($v->employeeid);
				$d['crypted_password'] = $this->login->get_password();
				$d['salt'] = $this->login->get_salt();
				$add_u = $this->M_users->create_users($d);
				$user_id = $this->db->insert_id();
			}

			unset($data);
			$data['joining_date'] = NOW;
			$data['dob'] = "1990-04-23";
			$data['department'] = $v->department ? $v->department : "teacher";
			$data['user_id'] = $v->user_id ? $v->user_id : $user_id;
			$up_emp = $this->M_employees->update($v->id, $data);
		}

		/*		$this->load->model('M_master_subjects','m');
		$this->load->model('M_sys_par');
		$this->load->model('M_open_semesters');
		$cos = $this->M_open_semesters->get_current_semesters();
		$rs = $this->m->get_record();
		
		$c = 0;
		foreach ($rs as $k => $v) {
			
			unset($data);
			// $data['units'] = intval($v->lec) + intval($v->lab);
			// $data['lec'] = intval($v->lec);
			// $data['lab'] = intval($v->lab);
			// $data['ref_id'] = $this->M_sys_par->get_subj_refid_and_update_to_next_series();
			$data['year_from'] = $cos->system->year_from;
			$data['year_to'] = $cos->system->year_to;
			$data['ay_id'] = $cos->system->academic_year_id;
			
			$u = $this->m->update($v->id, $data);
			if($u){
				$c++;
			}
		}

		vp($c);*/
	}
}
