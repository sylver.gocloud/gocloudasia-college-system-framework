<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Coursefinances extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->session_checker->open_semester();
		// $this->session_checker->secure_page('admin');
		$this->menu_access_checker();
		$this->load->model(array('M_coursefinances','M_coursefees','M_fees','M_core_model'));
		
		$this->menu_access_checker();
	}
	
	// Create
	public function create()
	{
		$this->load->model(array('M_coursefinances','M_fees','M_academic_years','M_semesters','M_coursefees'));
		
		$this->view_data['block_section_settings'] = FALSE;
		
		$this->view_data['fees_list'] = $this->M_fees->find_all();
		
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
	
		if($_POST)
		{
			$this->load->library('form_validation');
			if($this->form_validation->run('coursefinances') !== FALSE)
			{
				$data = $this->input->post('coursefinances');
				
				$data['created_at'] = date('Y-m-d H:i:s');
				$data['updated_at'] = date('Y-m-d H:i:s');
				
				$result = $this->M_coursefinances->create_coursefinances($data);
				
				if($result['status'])
				{
					$id = $result['id'];
					
					activity_log('create course finance',$this->userlogin,'Course Finances Created by: '.$this->user.'Success; Course Finances Id: '.$id);
					
					log_message('error','Course Finances Created by: '.$this->user.'Success; Course Finances Id: '.$id);
					$this->session->set_flashdata('system_message', '<div class="alert alert-success">Course Finance successfully added.</div>');

					//SAVE TUITION FEE
					if($this->input->post('tuition_fee')){
						unset($data);
						$data['coursefinance_id'] = $id;
						$data['fee_id'] = $this->input->post('tuition_fee');
						$xfees = $this->M_fees->get($data['fee_id']);
						$data['value'] = $xfees != false ? $xfees->value : 0;
						$data['created_at'] = date('Y-m-d H:i:s');
						$data['updated_at'] = date('Y-m-d H:i:s');
						$res = $this->M_coursefees->create_coursefees($data);
					}

					//SAVE OTHER, MISC, LAB
					if(count($this->input->post('fees')) > 0){
						foreach($this->input->post('fees') as $fees_id){
							unset($data);
							$data['coursefinance_id'] = $id;
							$data['fee_id'] = $fees_id;
							$xfees = $this->M_fees->get($fees_id);
							$data['value'] = $xfees != false ? $xfees->value : 0;
							$data['created_at'] = date('Y-m-d H:i:s');
							$data['updated_at'] = date('Y-m-d H:i:s');
							$res = $this->M_coursefees->create_coursefees($data);
						}
					}

					//SAVE NSTP
					if(count($this->input->post('nstp_fee')) > 0){
						unset($data);
						$fees_id = $this->input->post('nstp_fee');
						$data['coursefinance_id'] = $id;
						$data['fee_id'] = $fees_id;
						$xfees = $this->M_fees->get($fees_id);
						$data['value'] = $xfees != false ? $xfees->value : 0;
						$data['created_at'] = date('Y-m-d H:i:s');
						$data['updated_at'] = date('Y-m-d H:i:s');
						$res = $this->M_coursefees->create_coursefees($data);
					}
					
					redirect('coursefinances');
				}
			}
			
		}
	}
	
	// Retrieve
	public function index($page = 0)
	{
		$this->session_checker->check_if_alive();
		
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		
		$filter = false;
		$like = false;
		$order_by = false;
		
		if($_GET)
		{
			if(isset($_GET['category']) && trim($_GET['category']) != ''){
				$this->view_data['category'] = $category = trim($_GET['category']);
				$like['coursefinances.category'] = $category;
			}
			
			if(isset($_GET['category2']) && trim($_GET['category2']) != ''){
				$this->view_data['category2'] = $category2 = trim($_GET['category2']);
				$like['coursefinances.category2'] = $category2;
			}
			
			if(isset($_GET['code']) && trim($_GET['code']) != ''){
				$this->view_data['code'] = $code = trim($_GET['code']);
				$like['coursefinances.code'] = $code;
			}
		}
		
		$get['where'] = $filter;
		$get['like'] = $like;
		
		// $get['join'] = array(
			
			// 1 => array(
				// "table" => "courses",
				// "on"	=> "courses.id = fees.course_id",
				// "type"  => "LEFT"
			// ),
			// 2 => array(
				// "table" => "years",
				// "on"	=> "years.id = fees.year_id",
				// "type"  => "LEFT"
			// )
		// );
		$get['order'] = "coursefinances.category2";
		$get['all'] = true; //GET ALL EXCLUDE LIMIT
		$get['count'] = true; //RETURN COUNT NOT ROW
		$get['array'] = false; //RETURN OBJECT NOT ARRAY
		$get['single'] = false; //RETURN ALL NOT SINGLE
		
	
		$this->load->library("pagination");
		$config = $this->pagination_style();
		$config["base_url"] = base_url() ."coursefinances/index";
		$suffix = '?'.http_build_query($_GET, '', "&");
		$suffix = str_replace("&submit=Search", "", $suffix);
		$config['suffix'] = $suffix;
		$config['first_url'] = $config['base_url'].$config['suffix']; 
		
		$this->view_data['total_rows'] = $config["total_rows"] = $this->M_core_model->get_record("coursefinances", $get);
		
		$config["per_page"] = 25;
		$config['num_links'] = 10;
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		
		//FOR PAGINATION
		$get['all'] = false;
		$get['count'] = false;
		if($this->input->post('submit') == "Print")
		{
			$get['all'] = true;
		}
		$config['start'] = $page;
		$config['limit'] = $config['per_page'];
		
		$get['start'] = $page;
		$get['limit'] = $config['per_page'];
		
		$this->view_data['search'] = $this->view_data['coursefinances'] = $search = $this->M_core_model->get_record("coursefinances", $get);
		$this->view_data['links'] = $this->pagination->create_links();
		
		if($this->input->post('submit') == "Print")
		{
			$this->print_nstp_students($this->view_data);
		}
	}
	
	public function course_blocks($id = false){
		if($id == false){
			show_404();
		}
		$this->load->model(array('M_coursefinances','M_years','M_courses','M_course_blocks'));
		
		$this->view_data['years'] = $this->M_years->find_all();
		
		$this->view_data['courses'] = $this->M_courses->find_all();
		
		$this->view_data['block_system_settings'] = $this->M_coursefinances->get_block_section_settings($id);
		
		if($_POST){
			
			$data = $this->input->post('courses');
			$data['created_at'] = date('Y-m-d H:i:s');
			$data['updated_at'] = date('Y-m-d H:i:s');
			$result = $this->M_course_blocks->create_course_blocks($data);
			
			if($result['status'])
			{
				$id = $result['id'];
				
				activity_log('Assign course blocks',$this->userlogin,'Assign course blocks Set by: '.$this->user.'Success; Assign course blocks Id: '.$id);
				
				log_message('error','Assign course blocks Set by: '.$this->user.'Success; Assign course blocks Id: '.$id);
				$this->session->set_flashdata('system_message', '<div class="alert alert-success">Successfully assign course.</div>');
				redirect('block_section_settings/edit/'.$data['block_system_setting_id']);
			}
		}
	}
	
	public function display($id = false)
	{
		if($id == false){
			show_404();
		}
		$this->load->model(array('M_coursefinances','M_course_blocks','M_block_subjects'));
		
		$this->view_data['block_section_settings'] = $this->M_coursefinances->get_block_section_settings($id);
		
		$this->view_data['course_blocks'] = $this->M_course_blocks->get_course_blocks($id);
		
		$this->view_data['block_subjects'] = $this->M_block_subjects->get_block_subjects($id);
		
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		
		// var_dump($this->view_data['block_subjects']);
	}
	
	// Update
	public function edit($id=false)
	{
		if($id == false){
			show_404();
		}
		$this->load->model(array('M_coursefinances','M_fees','M_academic_years','M_semesters','M_coursefees'));
		
		$this->view_data['coursefinances'] = $this->M_coursefinances->get_coursefinances($id);
		
		$this->view_data['fees_list'] = $this->M_fees->find_all();
		
		$this->view_data['academic_years'] = $this->M_academic_years->find_all();
		
		$this->view_data['semesters'] = $this->M_semesters->find_all();
		
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		
		$this->view_data['id'] = $id;
		
		if($_POST)
		{
			$data = $this->input->post('coursefinances');
			$data['updated_at'] = date('Y-m-d H:i:s');
			$result = $this->M_coursefinances->update_coursefinances($data,$id);
			
			if($result['status'])
			{	
				activity_log('update course finance',$this->userlogin,'Course Finances Updated by: '.$this->user.'Success; Course Finances Id: '.$id);
				log_message('error','Course Finances Updated by: '.$this->user.'Success; Course Finances Id: '.$id);
				$this->session->set_flashdata('system_message', '<div class="alert alert-success">Successfully updated course.</div>');
				redirect(current_url());
			}
		}
	}
	
	public function create_fees($id=false){
		
		if($id == false){
			show_404();
		}
	
		$this->load->model(array('M_coursefinances','M_coursefees','M_fees','M_fees2'));
		
		$this->view_data['coursefinance_id'] = $id;
		
		$this->view_data['fees'] = $this->M_fees2->get_all_tuition_fees();
		// vd($this->db->last_query());
		$this->view_data['coursefees'] = $this->M_coursefees->get_course_fee_by_finance_id($id);
		
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		
		$this->view_data['id'] = $id;
		
		if($_POST)
		{
			$data = $this->input->post('coursefees');
			#REGION CHECK IF THERE ARE TWO TUITION FEE IF YES ONLY ALLOW ONE
				//GET FEES
				$rs_fee = $this->M_fees->get($data['fee_id']);
				if($rs_fee && $rs_fee->is_tuition_fee == 1){
					$is_tuition_fee_exist = $this->M_coursefees->check_tuition_fee_if_exist($id);
					if($is_tuition_fee_exist){
						$this->session->set_flashdata('system_message', '<div class="alert alert-success">Adding failed. This course finance already has fee that is set as "tuition fee". You can only add one tuition fee per course finance.</div>');
						redirect(current_url());
					}
				}
			#ENDREGION
			$this->load->library('form_validation');
			if($this->form_validation->run('coursefinances_create_fees') !== FALSE)
			{
				
				$data['coursefinance_id'] = $id;
				$data['created_at'] = date('Y-m-d H:i:s');
				$data['updated_at'] = date('Y-m-d H:i:s');
				
				$result = $this->M_coursefees->create_coursefees($data);
				
				if($result['status'])
				{
					activity_log('create course fee',$this->userlogin,'Course Fee Created by: '.$this->user.'Success; Coure Fee Id: '.$id);
					
					log_message('error','Course Fee Created by: '.$this->user.'Success; Coure Fee Id: '.$id);
							$this->session->set_flashdata('system_message', '<div class="alert alert-success">Course fee successfully created.</div>');
					
					redirect(current_url());
				}
			}
		}
	}
	
	public function edit_fees($id=false){
		if($id == false){
			show_404();
		}
		$this->load->model(array('M_coursefinances','M_coursefees'));
		
		$this->view_data['coursefees'] = $cf = $this->M_coursefees->get_course_fee_by_finance_id($id);
		
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		
		$this->view_data['id'] = $id;
		
		if($_POST){
			$coursefees = $this->input->post('coursefees');
			$ctr  = 0;
			if(isset($coursefees)){
				foreach($coursefees as $coursefee_id => $value){
					$data['value'] = floatval($value);
					$result = $this->M_coursefees->update_coursefees($data, $coursefee_id);
					
					if($result['status']){
						
						activity_log('update course fee',$this->userlogin,'Course Fee Updated by: '.$this->user.'Success; Course Fee Id: '.$coursefee_id);
						
						log_message('error','Course Fee Updated by: '.$this->user.'Success; Course Fee Id: '.$coursefee_id);
						$ctr++;
					}
				}
				if($ctr > 0){
					$this->session->set_flashdata('system_message', '<div class="alert alert-success">Course Fee successfully deleted.</div>');
					redirect('coursefinances/edit_fees/'.$id);
				}
			}
		}
	}
	
	public function display_created_fees($id=false){
		if($id == false){
			show_404();
		}
		$this->load->model(array('M_coursefees'));
		
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		
		$this->view_data['coursefees'] = $this->M_coursefees->get_coursefees($id);
	}
	
	// Delete
	public function destroy($id=false)
	{
		if($id == false){
			show_404();
		}
		$this->load->model(array('M_coursefinances','M_coursefees','M_assign_coursefees'));
		
		$result = $this->M_coursefees->delete_all_by_coursefinance_id($id);
		$result = $this->M_coursefinances->delete_coursefinances($id);
		$result = $this->M_assign_coursefees->delete(array('coursefinance_id'=>$id));
		
		activity_log('delete course fee',$this->userlogin,'Course Fee Deleted by: '.$this->user.'Success; Course Fee Id: '.$id);
		
		log_message('error','Course Fee Deleted by: '.$this->user.'Success; Course Fee Id: '.$id);
		$this->session->set_flashdata('system_message', '<div class="alert alert-success">Course Fee successfully deleted.</div>');
		redirect('coursefinances');
	}
	
	public function destroy_coursefees($id=false){
		if($id == false){
			show_404();
		}
		$coursefinance_id = $this->uri->segment(4);
		$this->load->model(array('M_coursefees'));
		
		$result = $this->M_coursefees->delete_coursefees($id);
		
		activity_log('delete course fee',$this->userlogin,'Course Fee Deleted by: '.$this->user.'Success; Course Fee Id: '.$id);
		
		log_message('error','Course Fee Deleted by: '.$this->user.'Success; Course Fee Id: '.$id);
		$this->session->set_flashdata('system_message', '<div class="alert alert-success">Course fee successfully deleted.</div>');
		redirect('coursefinances');
	}
	
	public function destroy_assign_course($id = false, $coursefinance_id = false){
	
		if($id == false){
			show_404();
		}
		
		if($coursefinance_id == false){
			show_404();
		}
		
		$this->load->model(array('M_assign_coursefees'));
		
		$result = $this->M_assign_coursefees->delete_assign_coursefees($id);
		activity_log('delete assign course fee',$this->userlogin,'Assign Course Deleted by: '.$this->user.'Success; Assign Course Id: '.$id);
		log_message('error','Assign Course Deleted by: '.$this->user.'Success; Assign Course Id: '.$id);
		$this->session->set_flashdata('system_message', '<div class="alert alert-success">Assign course successfully deleted.</div>');
		
		redirect('coursefinances/view_assigned_course/'.$coursefinance_id);
	}
	
	public function view_assigned_course($id=false){
		if($id == false){ show_404(); }
		
		$this->load->model(array('M_coursefinances','M_coursefees','M_fees','M_assign_coursefees'));
		
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		
		$this->view_data['assign_coursefees'] = $this->M_assign_coursefees->get_all_by_coursefinance_id($id);
		
		$this->view_data['id'] = $id;
	}
	
	public function assign_course($id = false){
		if($id == false){ show_404(); }
		
		$this->load->helper('my_dropdown');
		$this->load->model(array('M_coursefinances','M_coursefees','M_fees','M_assign_coursefees'));
		$this->view_data['id'] = $id;
		if($_POST){

			$this->load->library('form_validation');
			$this->form_validation->set_rules('assign_coursefee[academic_year_id]', 'Academic Year', 'required');

			if ($this->form_validation->run() !== FALSE)
			{
				$acf = $this->input->post('assign_coursefee');

				//additional null validation
				if(!isset($acf['create_all_year'])){
					if(!isset($acf['year_id'])){
						$this->view_data['system_message'] = "<div class='alert alert-danger'>No Year Selected</div>";
						return false;
					}
				}
				if(!isset($acf['create_all_semester'])){
					if(!isset($acf['semester_id'])){
						$this->view_data['system_message'] = "<div class='alert alert-danger'>No Semester Selected</div>";
						return false;
					}
				}

				$rs = $this->M_assign_coursefees->assigned_course($id, $this->input->post());
				$this->_msg($rs->code, $rs->msg, current_url());
			}
		}
	}
}