<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cashier extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->session_checker->open_semester();
		$this->session_checker->secure_page(array('finance','cashier'));
		$this->load->helper(array('url_encrypt'));
	}

	public function index()
	{

	
	}
	
	public function enrollee()
	{
		$semester_id_eq = $this->open_semester->id;
		$sy_from_eq = $this->open_semester->year_from;
		$sy_to_eq = $this->open_semester->year_to;
		$this->view_data['search'] = $this->M_enrollments->get_enrollee($semester_id_eq, $sy_from_eq, $sy_to_eq);
	}
	
	public function reasses($enrollment_id)
	{	
	  $this->load->model(array('M_enrollments','M_student_totals','M_student_finances','M_student_fees', 'M_student_previous_accounts', 'M_student_subjects', 'M_student_deductions','M_coursefees','M_assign_coursefees','M_studentpayments', 'M_additional_charges'));
		$student_finance = $this->M_student_finances->get_student_finance($enrollment_id);
		$studtotal = $this->M_student_totals->get_student_total($enrollment_id);
		if($studtotal)
		{
			$totalprevious_account = $studtotal->previous_account;
			$totalpreliminary = $studtotal->preliminary;
			$totalmidterm = $studtotal->midterm;
			$totalsemi_finals = $studtotal->semi_finals;
			$totalfinals = $studtotal->finals; 
		}
		else
		{
			$totalprevious_account = 0.00;
			$totalpreliminary = 0.00;
			$totalmidterm = 0.00;
			$totalsemi_finals = 0.00;
			$totalfinals = 0.00; 
		}
		
		
		$this->M_student_fees->delete_all_fees($student_finance->id);
		$this->M_student_finances->delete_student_finance($enrollment_id);
		
		$this->M_student_totals->delete_student_total($enrollment_id);
		
		$enrollee = $this->M_enrollments->profile($enrollment_id);
		$data[] = $enrollee->year_id;
		$data[] = $enrollee->course_id;
		$data[] = $enrollee->sem_id;
		$data[] = $this->open_semester->academic_year_id;
		$assigned_course_fee = $this->M_assign_coursefees->get_assign_course_fee($data);
		
		$studentfinance['coursefinance_id'] = $assigned_course_fee->coursefinance_id;
		$studentfinance['student_id']  = $enrollee->user_id;
		$studentfinance['enrollmentid']  = $enrollment_id;
		$studentfinance['year_id'] = $enrollee->year_id;
		$studentfinance['semester_id'] = $enrollee->sem_id;
		$studentfinance['course_id'] = $enrollee->course_id;
		
		$this->M_student_finances->add_student_finance($studentfinance);
		$studfin = $this->M_student_finances->get_student_finance($enrollment_id);
		
		// Student Subject Units
		$student_subjects = $this->M_student_subjects->get_student_subjects($enrollment_id,$enrollee->course_id,$enrollee->year_id,$enrollee->sem_id);
		$student_subjects_units = $this->M_student_subjects->get_student_subjects_units($enrollment_id,$enrollee->course_id,$enrollee->year_id,$enrollee->sem_id);
		
		$units = 0.0;
		$lab = 0.0;
		$rle = 0.0;
		$nstp_exist = 0;
		$lab_exist = 0;
		$lec_exist = 0;
		
		foreach($student_subjects as $studentsubject):
			if($studentsubject->code == "NSTP01" || $studentsubject->code == "NSTP02"){
				$nstp_exist = 1;
			}
#			if($studentsubject->lab != ''){
#				$lab_exist = 1;
#			}
#			if($studentsubject->speech_lab != ''){
#				$lec_exist = 1;
#			}
		endforeach;
		
		$student_total_units = $student_subjects_units->total_units;
		$student_total_lab = $student_subjects_units->total_labs;
#		$student_total_rle = $student_subjects_units->total_rle;
		//End Student Subject Units
		
		// Loop through Coursefees
		//Temp-Variable
		$tuition_fee = 0.0;
		$lab_fee = 0.0;
		$rle_fee = 0.0;
		$misc_fee = 0.0;
		$other_fee = 0.0;
		$lab_a_fee = 0.0;
		$lab_b_fee = 0.0;
		$lab_c_fee = 0.0;
		
		
		$coursefees = $this->M_coursefees->get_all_course_fees($assigned_course_fee->coursefinance_id);

	    foreach($coursefees as $cf):
		  $studentfee['student_id'] = $enrollee->user_id;
	      $studentfee['studentfinance_id'] = $studfin->id;
	      
	      if( strtolower($cf->name) == "tuition fee per unit" )
		    {
	        $studentfee['fee_id'] = $cf->fee_id;
	        $tuition_fee += $cf->value;
	      }
	      
	      if( strtolower($cf->name) == "laboratory fee per unit" )
		    {
	        $studentfee['fee_id'] = $cf->fee_id;
	        $lab_fee += $cf->value;
	      }
	      
	          if (strpos(strtolower($cf->name),'bsba') !== false) {
					    $lab_a_fee += $cf->value;
					  }
					
					   if (strpos(strtolower($cf->name),'laboratory 1') !== false) {
					    $lab_b_fee += $cf->value;
					  }
					
					   if (strpos(strtolower($cf->name),'laboratory 2') !== false) {
					    $lab_c_fee += $cf->value;
					  }
	      
	      if( $cf->is_misc == 1 )
		    {
	        $misc_fee += $cf->value;
	      }
	      
	      if( $cf->is_other == 1 )
		    {
	        $other_fee += $cf->value;
	      }
	      
	        $studentfee['fee_id'] = $cf->fee_id;
	        $studentfee['value'] = $cf->value;
	        $studentfee['position'] = $cf->position;
	        
			$this->M_student_fees->insert_student_fees($studentfee);
	    endforeach;
	  $lab_a = $this->M_student_subjects->get_lab_kind($student_subjects, "BSBA");
		$lab_b = $this->M_student_subjects->get_lab_kind($student_subjects, "LAB1");
		$lab_c = $this->M_student_subjects->get_lab_kind($student_subjects, "LAB2");
		$lab_a_fee_total = $lab_a*$lab_a_fee ;
		$lab_b_fee_total = $lab_b*$lab_b_fee ;
		$lab_c_fee_total = $lab_c*$lab_c_fee ;
		$total_labs = $lab_a_fee_total + $lab_b_fee_total + $lab_c_fee_total ;
	    
		$additional_charge = $this->M_additional_charges->get_total_by_enrollment_id($enrollment_id);
		
		# Computations for student total
		$student_total['enrollment_id'] = $enrollment_id;
		$student_total['total_units'] = $student_total_units;
		$student_total['total_lab'] = $student_total_lab;
#		$student_total['total_rle_units'] = $student_total_rle;
		$student_total['tuition_fee_per_unit'] = $tuition_fee;
		$student_total['lab_fee_per_unit'] = $lab_fee;
		$student_total['total_misc_fee'] = $misc_fee;
		$student_total['total_other_fee'] = $other_fee;
		$student_total['additional_charge'] = $additional_charge;
#		$student_total['rle_fee'] = $rle_fee;
    
    
		$total_tuition_fee = $tuition_fee*$student_total_units;
		$total_lab_fee = $lab_fee*$student_total_lab;
#		$total_rle = $rle_fee*$student_total_rle;
		$total_misc = $misc_fee;
		$total_other = $other_fee;
		# End Computations
	  
		# Save Computation Totals
	  
		# Tuition Fee
		$student_total['tuition_fee'] = $total_tuition_fee;
		# Laboratory Fee
		$student_total['lab_fee'] = $total_lab_fee;
		# RLE Fee
#		$student_total['total_rle'] = $total_rle;
		
		$total_stud_payment = $this->M_studentpayments->get_sum_student_payments($enrollment_id);
		$total_stud_dec = $this->M_student_deductions->get_sum_of_deductions($enrollment_id);
		if($total_stud_payment->account_recivable_student != '')
		{
			$student_total['total_payment'] = $total_stud_payment->account_recivable_student;
		}
		else
		{
			$student_total['total_payment'] = 0.00;
		}
		if($total_stud_dec->amount != '')
		{
			$student_total['total_deduction'] = $total_stud_dec->amount;
		}
		else
		{
			$student_total['total_deduction'] = 0.00;
		}
    
    
		# Total Charge
		$student_total['total_charge'] = $total_tuition_fee + $total_lab_fee  + $total_misc + $total_other+$total_labs+$additional_charge;
		# Total Payable/ Amount
		$student_total['total_amount'] = $student_total['total_charge'] + $totalprevious_account;
		# Calculate Remaining Balance
		$student_total['remaining_balance'] = $student_total['total_amount'] - ($total_stud_dec->amount + $total_stud_payment->account_recivable_student);
		
		$student_total['preliminary'] = $totalpreliminary;
		$student_total['midterm'] = $totalmidterm;
		$student_total['semi_finals'] =	$totalsemi_finals;
		$student_total['finals'] = $totalfinals; 

		$studtotal = $this->M_student_totals->insert_student_total($student_total);
		# End Save Computation Totals
		redirect('fees/view_fees/'.$enrollment_id);
	}
	
	public function block_grade($enrollment_id,$data)
	{
		$block_view = $this->M_enrollments->view_grade($enrollment_id,$data);
		if($block_view)
		{
			if($data == 1)
			{
				$this->session->set_flashdata('system_message','<div class="alert alert-success">Successfully Updated To Block.</div>');
			}
			else
			{
				$this->session->set_flashdata('system_message','<div class="alert alert-success">Successfully Updated To Unblock</div>');
			}
		}
		else
		{
			$this->session->set_flashdata('system_message','<div class="alert alert-danger">An error was encountered while proccessing your request</div>');
		}
		redirect('fees/view_fees/'.$enrollment_id);
	}
	
	public function add_promissory_note()
	{
		$enrollment_id = $_POST['enrollment_id'];
		$input_data['preliminary'] = $_POST['preliminary'] ;
		$input_data['midterm'] = $_POST['midterm'] ;
		$input_data['semi_finals'] = $_POST['semi_finals'] ;
		$input_data['finals'] = $_POST['finals'] ;
		$input_data['enrollment_id'] = $enrollment_id ;
		
		$prom = $this->M_promissory_notes->create_promissory_notes($input_data);
		if($prom)
		{
			$this->session->set_flashdata('system_message','<div class="alert alert-success">Promissory Was Successfully Added</div>');
		}
		else
		{
			$this->session->set_flashdata('system_message','<div class="alert alert-danger">An error was encountered while proccessing your request</div>');
		}
		redirect('fees/view_fees/'.$enrollment_id);
	}
}
