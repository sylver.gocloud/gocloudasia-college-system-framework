<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Student_master_list Extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->session_checker->open_semester();
		$this->load->helper(array('url_encrypt'));
		$this->load->model('M_core_model');
		$this->load->helper('my_dropdown');
		
		$this->load->model(array(
			'M_enrollments',
			'M_student_subjects',
			'M_studentpayments_details',
			'M_years',
			'M_courses'));
	}
		

	public function index($page = 0)
	{
		$this->session_checker->check_if_alive();
		
		//PAGINATION
		
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		
		$filter = false;
		$like = false;
		$order_by = false;
		
		$filter['enrollments.semester_id'] = $this->open_semester->id;
		$filter['enrollments.sy_from'] = $this->open_semester->year_from;
		$filter['enrollments.sy_to'] = $this->open_semester->year_to;
		$filter['enrollments.is_deleted'] = 0;
		$filter['enrollments.is_paid'] = 1;
		
		
		$arr_filters = array();
		$suffix = "";
		
		if($_GET)
		{	
			if(isset($_GET['submit']) && $_GET['submit'] == "Sort")
			{
				$page = 0;
			}
			
			if(isset($_GET['sort_by']) && trim($_GET['sort_by']) != ''){
				$get['order'] = $_GET['sort_by'];
				$this->view_data['sort_by'] = $_GET['sort_by'];
			}
			
			if(isset($_GET['sort_type']) && trim($_GET['sort_type']) != ''){
				$get['order'] .= ' '.$_GET['sort_type'];
				$this->view_data['sort_type'] = $_GET['sort_type'];
				
			}
		}
		
		//CONFIGURATION
		$get['fields'] = array(
				'enrollments.id',
				'enrollments.studid',
				'enrollments.name' ,
				'years.year',
				'courses.course',
				'enrollments.date_of_birth',
				'enrollments.sex',
				'enrollments.province'
		);
		
		$get['where'] = $filter;
		$get['like'] = $like;
		
		$get['join'] = array(
			
			1 => array(
				"table" => "courses",
				"on"	=> "courses.id = enrollments.course_id",
				"type"  => "LEFT"
			),
			2 => array(
				"table" => "years",
				"on"	=> "years.id = enrollments.year_id",
				"type"  => "LEFT"
			)
		);
		
		$get['all'] = true; //GET ALL EXCLUDE LIMIT
		$get['count'] = true; //RETURN COUNT NOT ROW
		$get['array'] = false; //RETURN OBJECT NOT ARRAY
		$get['single'] = false; //RETURN ALL NOT SINGLE
		
	
		$this->load->library("pagination");
		$config = $this->pagination_style();
		$config["base_url"] = base_url() ."student_master_list/index";
		$suffix = '?'.http_build_query($_GET, '', "&");
		$suffix = str_replace("&submit=Sort", "", $suffix);
		$config['suffix'] = $suffix;
		$config['first_url'] = $config['base_url'].$config['suffix']; 
		
		$this->view_data['total_rows'] = $config["total_rows"] = $this->M_core_model->get_record("enrollments", $get);
		
		$config["per_page"] = 30;
		$config['num_links'] = 10;
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		
		//FOR PAGINATION
		$get['all'] = false;
		$get['count'] = false;
		$config['start'] = $page;
		$config['limit'] = $config['per_page'];
		
		$get['start'] = $page;
		$get['limit'] = $config['per_page'];
		
		$this->view_data['search'] = $search = $this->M_core_model->get_record("enrollments", $get);
		$this->view_data['links'] = $this->pagination->create_links();
		
		if($this->input->get('submit') == "Print")
		{
			$get['all'] = true;
			$this->view_data['all_rows'] = $all_rows = $this->M_core_model->get_record("enrollments", $get);
			$this->_print_all($this->view_data);
		}
	}
	
	public function year($page = 0)
	{
		$this->load->model(array(
			'M_years',
			'M_enrollments'
		));
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		
		$sort_type = "ASC";
		
		if(isset($_GET['sort_type']) && $_GET['sort_type'] != "")
		{
			$this->view_data['sort_type'] = $sort_type = $_GET['sort_type'];
		}
		
		$this->view_data['years'] = $years = $this->M_years->find_all_order_by_year($sort_type);
		
		$this->view_data['search'] = false;
		
		$male = 0;
		$female =0;
		
		if($years)
		{
			foreach($years as $key => $obj)
			{
				$filter = array(
					'year_id'=>$obj->id,
					'is_paid'=>1,
				);
				$rs = $this->M_enrollments->get_record_by($filter);
				if($rs)
				{
					foreach($rs as $obj2)
					{
						if(strtoupper($obj2->sex) == 'MALE'):
							$male++;
						endif;
						if(strtoupper($obj2->sex) == 'FEMALE'):
							$female++;
						endif;
					}
					
					$this->view_data['search'][$key]['data'] = $obj;
					$this->view_data['search'][$key]['male'] = $male;
					$this->view_data['search'][$key]['female'] = $female;
					$this->view_data['search'][$key]['total'] = count($rs);
					
					$male = 0;
					$female =0;
				}
			}
		}
		
		if($this->input->get('submit') == "Print")
		{
			$this->_print_year($this->view_data);
		}
	}
	
	public function course($page = 0)
	{
		$this->view_data['system_message'] = $this->session->flashdata('system_message');
		
		$sort_type = "ASC";
		
		if(isset($_GET['sort_type']) && $_GET['sort_type'] != "")
		{
			$this->view_data['sort_type'] = $sort_type = $_GET['sort_type'];
		}
		
		$this->view_data['courses'] = $courses = $this->M_courses->find_all_order_by($sort_type);
		
		$this->view_data['search'] = false;
		
		$male = 0;
		$female =0;
		
		if($courses)
		{
			foreach($courses as $key => $obj)
			{
				$filter = array(
					'course_id'=>$obj->id,
					'is_paid'=>1,
				);
				$rs = $this->M_enrollments->get_record_by($filter);
				if($rs)
				{
					foreach($rs as $obj2)
					{
						if(strtoupper($obj2->sex) == 'MALE'):
							$male++;
						endif;
						if(strtoupper($obj2->sex) == 'FEMALE'):
							$female++;
						endif;
					}
					
					$this->view_data['search'][$key]['data'] = $obj;
					$this->view_data['search'][$key]['male'] = $male;
					$this->view_data['search'][$key]['female'] = $female;
					$this->view_data['search'][$key]['total'] = count($rs);
					
					$male = 0;
					$female =0;
				}
			}
		}
	}
	
	public function view_students($type = false, $id = false, $page = 0)
	{
		if(!$type) { show_404(); }
		if(!$id) { show_404(); }
		
		$title = "";
		
		$this->view_data['type'] = $type;
		$this->view_data['id'] = $id;
		$this->view_data['by_data'] = $by_data = false;
		
		switch($type)
		{
			case "year":
				$this->view_data['by_data'] = $rs = $by_data =$this->M_years->get($id);
				if($rs):
				$title = "List of ".$rs->year." Students";
				endif;
			break;
			case "course":
				$this->view_data['by_data'] = $rs = $by_data =$this->M_courses->get_courses($id);
				if($rs):
				$title = "List of ".$rs->course." Students";
				endif;
			break;
		}
		
		$this->view_data['custom_title'] = $title;
		
		$filter = false;
		$like = false;
		$order_by = false;
		
		$filter['enrollments.semester_id'] = $this->open_semester->id;
		$filter['enrollments.sy_from'] = $this->open_semester->year_from;
		$filter['enrollments.sy_to'] = $this->open_semester->year_to;
		$filter['enrollments.is_deleted'] = 0;
		$filter['enrollments.is_paid'] = 1;
		
		if($by_data){
			switch($type)
			{
				case "year":
					$filter['enrollments.year_id'] = $by_data->id;
				break;
				case "course":
					$filter['enrollments.course_id'] = $by_data->id;
				break;
			}
		}
		
		
		$arr_filters = array();
		$suffix = "";
		
		if($_GET)
		{	
			if(isset($_GET['submit']) && $_GET['submit'] == "Sort")
			{
				$page = 0;
			}
			
			if(isset($_GET['sort_by']) && trim($_GET['sort_by']) != ''){
				$get['order'] = $_GET['sort_by'];
				// $this->view_data['sort_by'] = $_GET['sort_by'];
			}
			
			if(isset($_GET['sort_type']) && trim($_GET['sort_type']) != ''){
				$get['order'] .= ' '.$_GET['sort_type'];
				// $this->view_data['sort_type'] = $_GET['sort_type'];
				
			}
		}
		
		//CONFIGURATION
		$get['fields'] = array(
				'enrollments.id',
				'enrollments.studid',
				'enrollments.name' ,
				'years.year',
				'courses.course',
				'enrollments.date_of_birth',
				'enrollments.sex',
				'enrollments.province'
		);
		
		$get['where'] = $filter;
		$get['like'] = $like;
		
		$get['join'] = array(
			
			1 => array(
				"table" => "courses",
				"on"	=> "courses.id = enrollments.course_id",
				"type"  => "LEFT"
			),
			2 => array(
				"table" => "years",
				"on"	=> "years.id = enrollments.year_id",
				"type"  => "LEFT"
			)
		);
		
		$get['all'] = true; //GET ALL EXCLUDE LIMIT
		$get['count'] = true; //RETURN COUNT NOT ROW
		$get['array'] = false; //RETURN OBJECT NOT ARRAY
		$get['single'] = false; //RETURN ALL NOT SINGLE
		
	
		$this->load->library("pagination");
		$config = $this->pagination_style();
		$config["base_url"] = base_url() ."student_master_list/view_students/{$type}/{$id}";
		$suffix = '?'.http_build_query($_GET, '', "&");
		$suffix = str_replace("&submit=Sort", "", $suffix);
		$config['suffix'] = $suffix;
		$config['first_url'] = $config['base_url'].$config['suffix']; 
		
		$this->view_data['total_rows'] = $config["total_rows"] = $this->M_core_model->get_record("enrollments", $get);
		
		$config["per_page"] = 30;
		$config['num_links'] = 10;
		$config["uri_segment"] = 5;
		$this->pagination->initialize($config);
		
		//FOR PAGINATION
		$get['all'] = false;
		$get['count'] = false;
		
		$config['start'] = $page;
		$config['limit'] = $config['per_page'];
		
		$get['start'] = $page;
		$get['limit'] = $config['per_page'];
		
		$this->view_data['search'] = $search = $this->M_core_model->get_record("enrollments", $get);
		$this->view_data['links'] = $this->pagination->create_links();
		
		if($this->input->get('submit') == "Print")
		{	
			$get['all'] = true;
			$this->view_data['all_rows'] = $all_rows = $this->M_core_model->get_record("enrollments", $get);
			$this->print_student_list($this->view_data);
		}
	}

	public function _print_all($data = false)
	{
		if($data == false) {show_404();}
		
		if($data['total_rows'] <= 0) {	
			$this->view_data['system_message'] = '<div class="alert alert-danger">No record to be printed.</div>';
			return false;
		}
		
		$search = $data['all_rows'];
		
		if($search)
		{
			$this->load->helper('print');
			
			$html = _html_student_master_list_all($data); 

			$this->load->library('mpdf');
			
			$mpdf=new mPDF('','FOLIO','','',5,5,5,5,0,0); 

			$mpdf->AddPage('P');

			$mpdf->WriteHTML($html);

			$mpdf->Output();
		}
	}
	
	public function _print_year($data = false)
	{
		if($data == false) {show_404();}
		
		if($data['search'] == false) {	
			$this->view_data['system_message'] = '<div class="alert alert-danger">No record to be printed.</div>';
			return false;
		}
		
		$search = $data['search'];
		
		if($search)
		{
			$this->load->helper('print');
			
			$html = _html_student_master_list_year($data); 

			$this->load->library('mpdf');
			
			$mpdf=new mPDF('','FOLIO','','',5,5,5,5,0,0); 

			$mpdf->AddPage('P');

			$mpdf->WriteHTML($html);

			$mpdf->Output();
		}
	}
	
	public function _print_course($data)
	{
		if($data == false) {show_404();}
		
		if($data['total_rows'] <= 0) {	
			$this->view_data['system_message'] = '<div class="alert alert-danger">No record to be printed.</div>';
			return false;
		}
		
		$search = $data['all_rows'];
		
		if($search)
		{
			$this->load->helper('print');
			
			$html = _html_student_master_list_course($data); 

			$this->load->library('mpdf');
			
			$mpdf=new mPDF('','FOLIO','','',5,5,5,5,0,0); 

			$mpdf->AddPage('P');

			$mpdf->WriteHTML($html);

			$mpdf->Output();
		}
	}
	
	public function print_student_list($data = false)
	{
		if($data == false) {show_404();}
		
		if($data['search'] == false) {	
			$this->view_data['system_message'] = '<div class="alert alert-danger">No record to be printed.</div>';
			return false;
		}
		
		$search = $data['all_rows'];
		
		if($search)
		{
			$this->load->helper('print');
			
			$html = _html_student_master_list_list($data); 

			$this->load->library('mpdf');
			
			$mpdf=new mPDF('','FOLIO','','',5,5,5,5,0,0); 

			$mpdf->AddPage('P');

			$mpdf->WriteHTML($html);

			$mpdf->Output();
		}
		else
		{
			$this->view_data['system_message'] = '<div class="alert alert-danger">No record to be printed.</div>';
		}
	}
}