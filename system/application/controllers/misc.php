<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Misc extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->session_checker->open_semester();
	}

	public function index()
	{
		$this->session_checker->check_if_alive();
		
		$this->load->model(array('M_announcements','M_events'));
		
		$this->view_data['announcements'] = $this->M_announcements->get_all();
		$this->view_data['events'] = $this->M_events->get_events();
	}
}