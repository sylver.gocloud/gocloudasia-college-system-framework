<?php	
class M_migration Extends MY_Model
{
	protected $_table = 'migrations';
	protected $_timestamp = false;

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function get_migration(){

		$q = $this->db->select('*')->from($this->_table)->limit(1)->get();
		$rs = $q->num_rows() > 0 ? $q->row() : false;
		
		if(!$rs){
			unset($data);
			$data['version'] = 0;
			$this->insert($data);
			$q = $this->db->select('*')->from($this->_table)->limit(1)->get();
			$rs = $q->num_rows() > 0 ? $q->row() : false;
		}

		return $rs;
	}

	public function get_last_version(){
		$migrate = $this->get_migration();
		if($migrate){
			return floatval($migrate->version) - 1;
		}else
		{
			show_error('Migration not Set');
		}
	}	

	public function get_latest_version(){
		$migrate = $this->get_migration();
		if($migrate){
			return floatval($migrate->version);
		}else
		{
			show_error('Migration not Set');
		}
	}

	public function update_version()
	{
		$sql = "UPDATE $this->_table set version = version + 1";
		$this->db->query($sql);
		return $this->db->affected_rows() > 0 ? TRUE : FALSE;
	}	
}
?>