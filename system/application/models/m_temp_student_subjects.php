<?php
class M_temp_student_subjects Extends MY_Model
{
	protected $_table = 'temp_studentsubjects';
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	public function reset_student_subjects($id)
	{
		$sql = "
			DELETE
			FROM $this->_table
			WHERE temp_start_id = ?
		";
		$query = $this->db->query($sql, array($id));
		return $this->db->affected_rows() > 0 ? true : false;
	}
	
	public function get_temp_student_subjects($id)
	{
		$this->load->model('M_temp_start','ms');
		$this->load->model('M_temp_enrollments','mte');

		$start = $this->ms->pull($id);
		$enroll = $this->mte->pull(array('temp_start_id'=>$id),'block_system_setting_id');
		if($start->type === "irregular"){
			$sql = "
				SELECT
					tss.id,
					ms.ref_id,
					ms.code,
					ms.subject,
					ms.units,
					ms.lab,
					ms.lec,
					s.time,
					s.is_open_time,
					s.day,
					s.course_id,
					s.room_id,
					r.name as room,
					s.year_id,
					s.semester_id,
					s.lab_fee_id,
					s.original_load,
					s.subject_load,
					s.subject_taken,
					s.year_from,
					s.year_to,
					s.academic_year,
					s.teacher_user_id,
					s.is_nstp,
					u.name as instructor
				FROM $this->_table tss
				LEFT JOIN subjects s ON s.id = tss.subject_id
				LEFT JOIN master_subjects ms on ms.ref_id = s.ref_id
				LEFT JOIN users u ON u.id = s.teacher_user_id
				LEFT JOIN rooms r ON r.id = s.room_id
				WHERE tss.temp_start_id = ?
				ORDER BY ms.code
			";
			return  $this->query($sql,[$id]);
		}else{
			$block_id = $enroll->block_system_setting_id;
			$this->load->model('M_block_subjects');
			return $this->M_block_subjects->get_block_subjects_a($block_id);
		}
	}
	
	public function get_student_subjects($id)
	{
		$sql = "
			SELECT 
				ss.id,
				master_subjects.ref_id,
				master_subjects.code,
				master_subjects.subject,
				master_subjects.units,
				master_subjects.lab,
				master_subjects.lec,
				subjects.time,
				subjects.is_open_time,
				subjects.day,
				subjects.course_id,
				subjects.room_id,
				rooms.name as room,
				subjects.year_id,
				subjects.semester_id,
				subjects.lab_fee_id,
				subjects.original_load,
				subjects.subject_load,
				subjects.subject_taken,
				subjects.year_from,
				subjects.year_to,
				subjects.academic_year,
				subjects.teacher_user_id,
				subjects.class_start,
				subjects.class_end,
				subjects.hour,
				subjects.min,
				subjects.is_nstp,
				users.name as teacher
			FROM temp_studentsubjects as ss
			LEFT JOIN subjects ON ss.subject_id = subjects.id
			LEFT JOIN master_subjects on master_subjects.ref_id = subjects.ref_id
			LEFT JOIN users ON users.id = subjects.teacher_user_id
			LEFT JOIN rooms ON rooms.id = subjects.room_id
			WHERE ss.temp_start_id = ? 
				";
		$query = $this->db->query($sql,array($id));
		
		return $query->num_rows() > 0 ? $query->result() : FALSE;
	}

		/** Get Subject Profile
	 * @param $id Subject ID
	 * @return object
	 */
	public function get_profile($id)
	{
		$sql = "
			SELECT
			ms.ref_id,
			ms.code,
			ms.subject,
			ms.units,
			ms.lab,
			ms.lec,
			s.id,
			s.time,
			s.is_open_time,
			s.day,
			s.course_id,
			s.room_id,
			r.name as room,
			s.year_id,
			s.semester_id,
			s.lab_fee_id,
			s.original_load,
			s.subject_load,
			s.subject_taken,
			s.year_from,
			s.year_to,
			s.academic_year,
			s.teacher_user_id,
			s.class_start,
			s.class_end,
			s.hour,
			s.min,
			s.is_nstp,
			u.name as teacher
			FROM $this->_table s 
			LEFT JOIN master_subjects ms ON ms.ref_id = s.ref_id
			LEFT JOIN users u ON u.id = s.teacher_user_id
			LEFT JOIN rooms r ON r.id = s.room_id
			WHERE s.id = ?
		";
		return $this->query($sql, array($id), true);
	}
	
	public function get_block_system_setting_id($id)
	{
		$sql = "
			SELECT
			t.id,
			b.name
			FROM $this->_table t
			LEFT JOIN block_system_settings b on b.id = t.block_system_setting_id
			WHERE temp_start_id = ?
			AND t.block_system_setting_id <> ''
			LIMIT 1
		";
		
		$query = $this->db->query($sql, array($id));
		return $query->num_rows() > 0 ? $query->row() : false;
	}
}
