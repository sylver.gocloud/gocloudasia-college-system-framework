<?php

class M_studentpayments_details Extends CI_Model
{

	private $_table = 'studentpayments_details';
	
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function get($id = false,$array = false,$where = false)
	{
		if($id == false)
		{
			if($array == false)
			{
				if($where == false)
				{
					$query = $this->db->get($this->_table);
					return $query->num_rows() > 0 ? $query->result() : FALSE;
				}else
				{
					$query = $this->db->where($where)->get($this->_table);
					return $query->num_rows() > 0 ? $query->result() : FALSE;
				}
			}else
			{
				if($where == false)
				{
					$query = $this->db->select($array)->get($this->_table);
					return $query->num_rows() > 0 ? $query->result() : FALSE;
				}else
				{
					$query = $this->db->select($array)->where($where)->get($this->_table);
					return $query->num_rows() > 0 ? $query->result() : FALSE;
				}
			}
		}else
		{
			if($array == false)
			{
				if($where == false)
				{
					$query = $this->db->where('id',$id)->get($this->_table);
					return $query->num_rows() > 0 ? $query->row() : FALSE;
				}else
				{
					$query = $this->db->where('id',$id)->where($where)->get($this->_table);
					return $query->num_rows() > 0 ? $query->row() : FALSE;
				}
			}else
			{	
				if($where == false)
				{
					$query = $this->db->select($array)->where('id',$id)->get($this->_table);
					return $query->num_rows() > 0 ? $query->row() : FALSE;
				}else
				{
					$query = $this->db->select($array)->where('id',$id)->where($where)->get($this->_table);
					return $query->num_rows() > 0 ? $query->row() : FALSE;
				}
			}
		}
	}

	public function get_record_by_enrollment_id($id)
	{
		$sql = "
			SELECT
			gp.grading_period, 
			$this->_table.* 
			FROM $this->_table 
			LEFT JOIN grading_periods gp ON gp.id = $this->_table.grading_period_id 
			WHERE $this->_table.enrollment_id = ? 
			ORDER BY $this->_table.id
		";
		
		$query = $this->db->query($sql, array($id));
		return $query->num_rows() > 0 ? $query->result() : false;
	}
	
	public function update_record_by_id($data = false, $where = false)
	{
		$this->db->set($data)->where('id',$where)->update($this->_table);
		return $this->db->affected_rows() > 0 ? array('status'=>'true') : array('status'=>'false');
	}
	
	public function reset_current(){
		$this->db->set('is_current',0)->update($this->_table);
		return $this->db->affected_rows() > 0 ? array('status'=>'true') : array('status'=>'false');
	}
	
	public function get_record_with_amount_paid($id)
	{
		$sql = "
			SELECT
			* 
			FROM $this->_table
			WHERE amount_paid > 0
			AND enrollment_id = ?
			ORDER BY id DESC
		";
		
		$query = $this->db->query($sql, array($id));
		return $query->num_rows() > 0 ? $query->result() : false;
	}
	
	public function recalculate_studentpayment_details($enrollment_id = false)
	{
		$this->load->model(array('M_enrollments','M_student_total_file'));
		if($enrollment_id){
		
			$payment_details = $this->get_record_by_enrollment_id($enrollment_id);
			$student_total = $this->M_student_total_file->get_student_total_file($enrollment_id);
			$profile = $this->M_enrollments->profile($enrollment_id);
			
			if($payment_details){
				
				
				//RECALCULATE PAYMENT DIVISION
				//RECALCULATE PAYMENT APPLIED
				$total_amount_due = $student_total->total_amount_due - $student_total->less_deduction;
				$total_payment = $student_total->total_payment;
				
				$division = $total_amount_due / $profile->payment_division;
				
				foreach($payment_details as $obj){
					
					unset($update);
					$update['updated_at'] = NOW;
					$update['amount'] = $division;
					$update['amount_paid'] = 0;
					$update['is_paid'] = 0;
					$update['balance'] = $balance = $division;
				
					if($total_payment > 0)
					{
						$balance = $division - $total_payment;
						$balance = $balance <= 0 ? 0 : $balance; //BALANCE is 0 when paid all or excess
						$amount_paid = $division - $balance;
						$is_paid = $balance <= 0 ? 1 : 0;
						
						$update['amount_paid'] = $amount_paid;
						$update['balance'] = $balance;
						$update['is_paid'] = $is_paid;
					}
					
					$rs = $this->update_record_by_id($update, $obj->id);
					$total_payment -= $division;
				}
			}
			
		}
	}
	
	public function reverse_studentpayment_details($enrollment_id)
	{
		$this->load->model(array('M_enrollments','M_student_total_file'));
		if($enrollment_id){
		
			$payment_details = $this->get_record_by_enrollment_id($enrollment_id);
			$student_total = $this->M_student_total_file->get_student_total_file($enrollment_id);
			$profile = $this->M_enrollments->profile($enrollment_id);
			
			if($payment_details){
			
				//RECALCULATE PAYMENT DIVISION
				//RECALCULATE PAYMENT APPLIED
				$total_amount_due = $student_total->total_amount_due - $student_total->less_deduction;
				$total_payment = $student_total->total_payment;
				
				$division = $total_amount_due / $profile->payment_division;
				
				foreach($payment_details as $obj){
					
					unset($update);
					$update['updated_at'] = NOW;
					$update['amount'] = $division;
					$update['amount_paid'] = 0;
					$update['is_paid'] = 0;
					$update['balance'] = $balance = $division;
				
					if($total_payment > 0)
					{
						$balance = $division - $total_payment;
						$balance = $balance <= 0 ? 0 : $balance; //BALANCE is 0 when paid all or excess
						$amount_paid = $division - $balance;
						$is_paid = $balance <= 0 ? 1 : 0;
						
						$update['amount_paid'] = $amount_paid;
						$update['balance'] = $balance;
						$update['is_paid'] = $is_paid;
					}
					
					$rs = $this->update_record_by_id($update, $obj->id);
					
					$total_payment -= $division;
				}
			
			}
			
		}
	}
	
	public function get_downpayment_by_id($id)
	{
		$sql = "
			SELECT
			*
			FROM $this->_table
			WHERE enrollment_id = ?
			AND is_downpayment = 1
		";
		
		$query = $this->db->query($sql, array($id));
		
		return $query->num_rows() > 0 ? $query->row() : false;
	}
	
	public function check_if_has_promisory($obj = false, $current_period = false)
	{
		/*
		GRADING PERIOD ID LEGEND
			DOWNPAYMENT - First Payment
			1 - 4 - Grading Periods
			CASH/FULLPAYMENT - One time payment 
		*/
		
		if($obj->division == 1)
		{
			$grading_period_id = "CASH/FULLPAYMENT";
		}
		else
		{
			//FIRST PAYMENT AND DOWNPAYMENT IS THE SAME LEVEL
			//DETERMINE IF DOWNPAYMENT HAS PAID OR PROMISORY
			$rs_downpayment = $this->get_downpayment_by_id($obj->id);
			$grading_period_id = false;
			if($rs_downpayment)
			{
				if($rs_downpayment->is_paid == 0 && $rs_downpayment->is_promisory == 0):
					$grading_period_id = $rs_downpayment->grading_period_id;
				else:
					$grading_period_id = $current_period->id;
				endif;
			}
		}
		
		$sql = "
			SELECT *
			FROM $this->_table
			WHERE enrollment_id = ?
			AND grading_period_id = ?
			AND is_promisory = 1
		";
		
		$query = $this->db->query($sql, array($obj->id, $grading_period_id));
		
		return $query->num_rows() > 0 ? $query->row() : FALSE;
	}
	
	public function get_record_by_period($obj = false, $current_period = false)
	{	
		$rs_downpayment = $this->get_downpayment_by_id($obj->id);
		$grading_period_id = false;
		if($rs_downpayment)
		{
			if($rs_downpayment->is_paid == 0 && $rs_downpayment->is_promisory == 0):
				$grading_period_id = $rs_downpayment->grading_period_id;
			else:
				$grading_period_id = $current_period->id;
			endif;
		}
		
		$sql = "
			SELECT *
			FROM $this->_table
			WHERE enrollment_id = ?
			AND grading_period_id = ?
		";
		
		$query = $this->db->query($sql, array($obj->id, $grading_period_id));
		
		return $query->num_rows() > 0 ? $query->row() : FALSE;
	}

	public function x_old_get_record_by_period($obj = false, $current_period = false)
	{
		/*
		GRADING PERIOD ID LEGEND
			DOWNPAYMENT - First Payment
			1 - 4 - Grading Periods
			CASH/FULLPAYMENT - One time payment 
		*/
		
		if($obj->division == 1)
		{
			$grading_period_id = "CASH/FULLPAYMENT";
		}
		else
		{
			//FIRST PAYMENT AND DOWNPAYMENT IS THE SAME LEVEL
			//DETERMINE IF DOWNPAYMENT HAS PAID OR PROMISORY
			$rs_downpayment = $this->get_downpayment_by_id($obj->id);
			$grading_period_id = false;
			if($rs_downpayment)
			{
				if($rs_downpayment->is_paid == 0 && $rs_downpayment->is_promisory == 0):
					$grading_period_id = $rs_downpayment->grading_period_id;
				else:
					$grading_period_id = $current_period->id;
				endif;
			}
		}
		
		$sql = "
			SELECT *
			FROM $this->_table
			WHERE enrollment_id = ?
			AND grading_period_id = ?
		";
		
		$query = $this->db->query($sql, array($obj->id, $grading_period_id));
		
		return $query->num_rows() > 0 ? $query->row() : FALSE;
	}
}

