<?php

class M_subjects_load_file Extends MY_Model
{
	protected $_table = 'subjects_load_file';
	
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	
	public function get_subjects_load_file($subject_id , $enrollment_id)
	{
		$sql = "
			SELECT
			*
			FROM $this->_table
			WHERE subject_id = ?
			AND enrollment_id = ?
		";
		
		$query = $this->db->query($sql, array($subject_id, $enrollment_id));
		
		return $query->num_rows() > 0 ? $query->row() : false;
	}
	
	public function delete_subjects_load_file($subject_id, $enrollment_id)
	{
		$sql = "
			DELETE 
			FROM $this->_table
			WHERE subject_id = ?
			AND enrollment_id = ?
		";
		
		$this->db->query($sql, array($subject_id, $enrollment_id));
		return $this->db->affected_rows() > 0 ? array('status'=>'true') : array('status'=>'false');
	}
	
	public function create_subjects_load_file($data)
	{
		$this->db->insert($this->_table,$data);
		return $this->db->affected_rows() > 0 ? array('status'=>'true') : array('status'=>'false');
	}
	
	public function update_subjects_load_file($subject_id, $enrollment_id, $condition = false)
	{
		if($condition == "remove")
		{
			$this->delete_subjects_load_file($subject_id, $enrollment_id);
		}
		else
		{
			//CHECK FIRST IF ALREADY INSERTED
			$result = $this->get_subjects_load_file($subject_id, $enrollment_id);
			
			if($result == false)
			{
				$data['created_at'] = NOW;
				$data['updated_at'] = NOW;
				$data['subject_id'] = $subject_id;
				$data['enrollment_id'] = $enrollment_id;
				$rs = $this->create_subjects_load_file($data);
			}
		}
	}
	
}