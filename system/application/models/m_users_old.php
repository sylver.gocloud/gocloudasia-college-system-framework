<?php

class M_users extends CI_Model{

  private $_table = "users";
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	function get_login_by_id($id = false)
	{
	  $sql = "select id, login, crypted_password, salt
				FROM $this->_table
				WHERE id = ?
				ORDER BY id";
		$query = $this->db->query($sql,array($id));
		return $query->num_rows() > 0 ? $query->row() : FALSE;
	}
	
	function get_user_by_login($id = false)
	{
		$sql = "select id, login, crypted_password, salt
				FROM $this->_table
				WHERE login = ?
				ORDER BY id";
		$query = $this->db->query($sql,array($id));
		return $query->num_rows() > 0 ? $query->row() : FALSE;
	}
	
	function fix_users(){
		
		$sql = "select department
				FROM departments
				";
		$query = $this->db->query($sql);
		$rs = $query->result();
		
		foreach($rs as $obj){
			$sql1 = "update users set department = '$obj->department' where $obj->department = 1";
			$query1 = $this->db->query($sql1);
		}	
	}
	
	function get_profile($id){
		$q = $this->db->where('id', $id)->where('is_activated', 1)->get('users');
		if($q->num_rows() >= 1){
			$row = $q->row();
			
			//REMOVE 
			/*
			if($row->admin == 1)
			{
				$userType = 'admin';
			}
			else if($row->student == 1)
			{
				$userType = 'student';
			}
			else if($row->registrar == 1)
			{
				$userType = 'registrar';
			}
			else if($row->finance == 1)
			{
				$userType = 'finance';
			}
			else if($row->teacher == 1)
			{
				$userType = 'teacher';
			}
			else if($row->custodian == 1)
			{
				$userType = 'custodian';
			}
			else if($row->dean == 1)
			{
				$userType = 'dean';
			}
			else if($row->hrd == 1)
			{
				$userType = 'hrd';
			}
			else if($row->president == 1)
			{
				$userType = 'president';
			}
			else if($row->librarian == 1)
			{
				$userType = 'librarian';
			}
			else if($row->cashier == 1)
			{
				$userType = 'cashier';
			}
			else if($row->office_assistant == 1)
			{
				$userType = 'office_assistant';
			}
			else if($row->student_examiner == 1)
			{
				$userType = 'student_examiner';
			}
			*/
			
			//GET USER DATA AND PUT IT TO SESSION
			$userdata = array(
						'userid' => $row->id,
						'username' => $row->name,
						// 'userType' => $userType,
						'userType' => $row->department,
						'logged_in' => TRUE
					);
			
			$this->session->set_userdata($userdata);
			
			
			return true;
		}else{
			return false;
		}
	}
	
	public function activate_user_account($array)
	{
		foreach ($array['user_ids'] as $key => $id)
		{
			$data[$key]['is_activated'] = 1;
			$data[$key]['activated_at'] = date('Y-m-d H:i:s');
			$data[$key]['activation_code'] = '';
			$data[$key]['id'] = $id;
		}
		$this->db->update_batch($this->_table, $data, 'id');
	}
	
	public function create_users($input = false)
	{
		$this->db->insert($this->_table,$input);
		return $this->db->affected_rows() > 0 ? array('status'=>'true','id'=>$this->db->insert_id()) : array('status'=>'false');
	}
	
	public function update_users($data = false, $where = false)
	{
		$this->db->set($data)->where('id',$where)->update($this->_table);
		return $this->db->affected_rows() > 0 ? array('status'=>'true') : array('status'=>'false');
	}
	
	public function update_users_by_login($data = false, $where = false)
	{
		$this->db->set($data)->where('login',$where)->update($this->_table);
		return $this->db->affected_rows() > 0 ? array('status'=>'true') : array('status'=>'false');
	}
	
	public function get_users_by($where, $single = false, $array = false)
	{
			$this->db->where($where);
		
			if(!$array)
			{
				if($single)
				{
					$query = $this->db->get($this->_table, $single);
					return $query->num_rows() > 0 ? $query->row() : FALSE;
				}
				else
				{
					$query = $this->db->get($this->_table, $single);
					return $query->num_rows() > 0 ? $query->result() : FALSE;
				}
			}
			else
			{
				if($single)
				{
					$query = $this->db->get($this->_table, $single);
					return $query->num_rows() > 0 ? $query->first_row('array') : FALSE;
				}
				else
				{
					$query = $this->db->get($this->_table, $single);
					return $query->num_rows() > 0 ? $query->result_array() : FALSE;
				}
			}
	}

	public function find_all($where = false)
	{
		if($where != false)
		{
			$query = $this->db->select(array('id', 'login', 'name'))->where($where)->get($this->_table)->order_by('name','ASC');
			return $query->num_rows() > 0 ? $query->result() : FALSE;
		}
		else
		{
			$sql = "select id, login, name
				FROM $this->_table
				ORDER BY name";
			$query = $this->db->query($sql);
			return $query->num_rows() > 0 ? $query->result() : FALSE;
		}
	}
	
	public function find($start=0,$limit=100, $filter = false, $all = false, $ret_count = false){
		
		$start = $this->db->escape_str($start);
		$limit = $this->db->escape_str($limit);
		$ci =& get_instance();
		
		//GET Library FIELDS
		// $sql = "DESCRIBE $this->_table";
		// $query = $this->db->query($sql);
		// $fields = $query->result();
		// $fields_array = array();
		// foreach($fields as $val){
			
			// $fields_array[] = $this->_table.'.'.$val->Field;
		// }
		
		//ADD FILTERS
		$fields_array[] = 'users.id';
		$fields_array[] = 'users.login';
		$fields_array[] = 'users.name';
		
		$param = array();
		if($filter != false){
			//if filter is array
			if(is_array($filter)){
				foreach($filter as $key => $value){
		
					$param[$key] = $value;
				}
			}
			
		}
		
		$this->db->select($fields_array);
		$this->db->from($this->_table);
		$this->db->where($param);
		$this->db->order_by("users.name", "ASC"); 
		// $this->db->join('librarycategory', 'librarycategory.id = '.$this->_table.'.librarycategory_id','LEFT');
		
		
		if($all == false){
			$this->db->limit($limit, $start);
		}
		
		$query = $this->db->get();
		// vp($this->db->last_query());
		if($ret_count == false){
			return $query->num_rows() > 0 ? $query->result() : FALSE;
		}else{
			return $query->num_rows();
		}
	}
	
	public function get_usertype($id){
		
		$sql = "select id, login, crypted_password, salt
				FROM $this->_table
				WHERE id = ?
				ORDER BY id";
		$query = $this->db->query($sql,array($id));
		return $query->num_rows() > 0 ? $query->row() : FALSE;
		
	}
	
	public function fix_users_table()
	{
		$department = array(
			'admin',
			'registrar',
			'finance',
			'teacher',
			'custodian',
			'dean',
			'hrd',
			'president',
			'librarian',
			'cashier',
			'guidance',
			'vp_assistant',
			'assistant_to_the_president',
			'department',
			'student'
			
		);
		
		$affected_rows = 0;
		
		foreach($department as $dep)
		{
			if($dep == "vp_assistant" || $dep == "assistant_to_the_president")
			{
				$sql = "UPDATE users 
						set department = ?
						WHERE vp_assistant = 1
						OR assistant_to_the_president = 1";
				$query = $this->db->query($sql, array($dep));
				
				$affected_rows += $this->db->affected_rows();
			}
			else
			{
				$sql = "UPDATE users 
						set department = ?
						WHERE $dep = ?";
				$query = $this->db->query($sql, array($dep, 1));
				
				$affected_rows += $this->db->affected_rows();
			}
		}
		
		return $affected_rows > 0 ? true : false;
	}
}

?>
