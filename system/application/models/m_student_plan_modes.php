<?php

class M_student_plan_modes Extends MY_Model
{
	protected $_table = 'student_plan_modes';
	
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function get_current_student_plan_mode_by_eid($e_id)
	{
		$sql = "
			select 
			*
			from $this->_table
			where enrollment_id = ?
			AND is_deleted = 0
			order by number
		";

		$query = $this->db->query($sql, array($e_id));
		
		return $query->num_rows() > 0 ? $query->result() : false;
	}

	public function update_payment_status($id){
		$rs = $this->pull($id, array('id','value','amount_paid','is_paid'));
		if($rs){
			$balance = $rs->value - $rs->amount_paid;
			unset($data);
			if($balance <= 0){
				$data['is_paid'] = 1;
			}else{
				$data['is_paid'] = 0;
			}

			if($rs->is_paid != $data['is_paid']){
				$this->update($rs->id, $data);
			}
		}
	}

	public function reverse_payment($type = false, $id, $amount = 0, $payment_type = false)
	{
		if($type == "tuition")
		{
			$spm = $this->get($id);
			
			if($spm && $amount > 0 && $payment_type){
				unset($data);
				$data['spr_id'] = null;
				$data['amount_paid'] = $spm->amount_paid - $amount;

				if($payment_type == "check"){
					$data['check_applied'] = $spm->check_applied - $amount;
					$data['is_check'] = 0;
				}
				
				$rs = $this->update($id, $data);
				if($rs){
					$this->update_payment_status($id);
				}
			}

		}else if($type == "other")
		{
			$this->load->model('m_student_enrollment_fees','m_sef');
			$sef = $this->m_sef->get($id);

			if($sef && $amount > 0 && $payment_type){
				unset($data);
				$data['spr_id'] = null;
				$data['amount_paid'] = $sef->amount_paid - $amount;

				if($payment_type == "check"){
					$data['check_applied'] = $sef->check_applied - $amount;
					$data['is_check'] = 0;
				}
				
				$rs = $this->m_sef->update($id, $data);
				if($rs){
					$this->m_sef->update_payment_status($id);
				}

			}
		}else if($type == "previous")
		{
			$this->load->model('m_student_previous_accounts','m_spa');
			$spa = $this->m_spa->get($id);
			if($spa && $amount > 0 && $payment_type){
				unset($data);
				$data['spr_id'] = null;
				$data['amount_paid'] = $spa->amount_paid - $amount;

				if($payment_type == "check"){
					$data['check_applied'] = $spa->check_applied - $amount;
					$data['is_check'] = 0;
				}
				$rs = $this->m_spa->update($id, $data);
				if($rs){
					$this->m_spa->update_payment_status($id);
				}

			}
		}else{}
		
	}

}