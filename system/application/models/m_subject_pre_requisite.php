<?php
class M_subject_pre_requisite Extends MY_Model
{
	protected $_table = 'subject_pre_requisite';
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function get_pre_requisite($c_id, $s_id)
	{
		unset($get);
		$get['fields'] = array(
				'subject_pre_requisite.id',
				'subject_pre_requisite.curriculum_id',
				'subject_pre_requisite.taken_at_once',
				'subject_pre_requisite.subject_id as subject_id',
				'master_subjects.ref_id',
				'master_subjects.code',
				'master_subjects.subject',
				'master_subjects.units',
				'master_subjects.lec',
				'master_subjects.lab',
			);
		$get['where']['subject_pre_requisite.curriculum_id'] = $c_id;
		$get['where']['subject_pre_requisite.curriculum_sub_id'] = $s_id;
		$get['join'][] = array(
				'table' => 'master_subjects',
				'on' => 'master_subjects.ref_id = subject_pre_requisite.subject_refid',
				'type' => 'left',
			);
		return $this->get_record(false, $get);
	}

	/**
	 * Remove Curriculum Subject Pre-requisite
	 * @param int $curriculum_id Table Curriculum Id
	 * @param int $curriculum_sub_id Table curriculum_subjects Id
	 */
	public function remove_pre_requisite($curriculum_id, $curriculum_sub_id)
	{
		unset($u);
		$u['is_deleted'] = 1;
		$u['deleted_date'] = NOW;
		$u['deleted_by'] = $this->ci->userid;

		$where = array(
				'curriculum_id' => $curriculum_id,
				'curriculum_sub_id' => $curriculum_sub_id
			);
		return $this->update($where, $u);
	}
}
