<?php

class M_temp_start Extends MY_Model
{
	protected $_table = 'temp_start';
	
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	public function delete_temp_start_by_studid($id)
	{
		$this->db->where('studid',$id)->delete($this->_table);
		return $this->db->affected_rows() > 0 ? array('status'=>'true') : array('status'=>'false');
	}
	
	public function delete_temp_start($id)
	{
		$this->db->where('id',$id)->delete($this->_table);
		return $this->db->affected_rows() > 0 ? array('status'=>'true') : array('status'=>'false');
	}
	
	public function get_temp_start($studid)
	{
		$query = $this->db->where('studid', $studid)->get($this->_table);
		return $query->num_rows() > 0 ? $query->row() : false;
	}
	
	public function get_by_id($id)
	{
		$query = $this->db->where('id', $id)->get($this->_table);
		$sql = "
			SELECT ts.*, te.status as enrollment_type
			FROM $this->_table ts
			LEFT JOIN temp_enrollments te ON te.temp_start_id = ts.id
			WHERE ts.id = ?
		 ";
		return $this->query($sql,[$id], true);
	}

	/**
	 * Remove or Delete registration together with its linked tables
	 * @param  int $temp_start_id temp_start table id
	 * @return bool                result
	 */
	public function remove_registration($temp_start_id)
	{
		# remove temp subjects
		$sql = "DELETE FROM temp_studentsubjects WHERE temp_start_id = ?";
		$rs = $this->query2($sql,[$temp_start_id]);

		# remove temp payment
		$sql = "DELETE FROM temp_payment WHERE temp_start_id = ?";
		$rs = $this->query2($sql,[$temp_start_id]);

		# remove temp enrollments
		$sql = "DELETE FROM temp_enrollments WHERE temp_start_id = ?";
		$rs = $this->query2($sql,[$temp_start_id]);

		# remove temp start
		$sql = "DELETE FROM temp_start WHERE temp_start_id = ?";
		$rs = $this->query2($sql,[$temp_start_id]);

		return $rs;
	}
}