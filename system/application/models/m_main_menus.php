<?php	
class M_main_menus Extends MY_Model
{
	protected $_table = 'main_menus';
	
	public function get_all_main_menus()
	{
		$sql = "
			SELECT
			*
			FROM main_menus
			WHERE visible = 1
			ORDER BY caption
		";
		
		$query = $this->db->query($sql);
		
		return $query->num_rows() > 0 ? $query->result() : false;
	}
	
	public function menu_list()
	{
		
		$sql = "
			DESCRIBE menus;
		";
		
		$fields = $this->db->query($sql)->result_array();
		 
		// vd($fields); 
		
		$sql2 = "SELECT * FROM menus WHERE menu_lvl = 2 GROUP BY controller ORDER BY menu_num";
		$mg = $this->db->query($sql2)->result();
		
		foreach($mg as $obj)
		{
			unset($data);
			$data['controller'] = $obj->controller;
			$data['caption'] = $obj->caption;
			$data['menu_grp'] = $obj->menu_grp;
			$data['menu_sub'] = $obj->menu_sub;
			$data['menu_num'] = $obj->menu_num;
			$data['menu_lvl'] = $obj->menu_lvl;
			$data['menu_icon'] = $obj->menu_icon;
			$data['attr'] = $obj->attr;
			
			$rs = $this->insert($data);
			
			if($rs['status'])
			{
				$id = $rs['id'];
				unset($data);
				$data['main_menu_id'] = $id;
				$this->db->where('controller', $obj->controller);
				$this->db->update('menus', $data); 
			}
		}
	}


	/*
		SYLVER 2-27-2014
		FIX
		ADD DEFAULT DATA TO main_menu_methods
		Data (create, read, update, destroy)
	*/
	public function set_main_menu_methods()
	{
		$this->load->model('M_main_menus_method');
		$rs = $this->fetch_all();

		$methods = array(
			'create',
			'read',
			'update',
			'destroy',
		);

		foreach ($rs as $key => $value) {
			foreach ($methods as $m) {
				unset($data);
				$data['main_menu_id'] = $value->id;
				$data['method'] = $m;
				$res  = $this->M_main_menus_method->insert($data);					
			}
		}
	}

	/**
	 * Get Menu list for Dropdown  - used in adding menus for packages
	 * @param int $p_id Package ID
	 * @return array
	 */
	public function get_menu_list_except_package_dd($p_id, $d_id)
	{
		$sql = "
			SELECT
			*
			FROM $this->_table mm
			WHERE id NOT IN (SELECT main_menu_id FROM package_menus WHERE package_id = ? AND department_id = ? AND main_menu_id IS NOT NULL)
			AND menu_lvl = 2
			ORDER BY caption
		";

		return $this->query($sql, array($p_id, $d_id));
	}
}
?>