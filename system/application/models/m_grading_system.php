<?php
class M_grading_system Extends MY_Model
{
	protected $_table = 'grading_system';
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->type = $this->grading_system_type();
	}

	/**
	 * Get Grading System Type from System Parameter
	 * @return string
	 */
	public function grading_system_type()
	{
		$this->load->model('M_sys_par');
		$sp = $this->M_sys_par->get_sys_par();
		if($sp){
			return $sp->grading_system;
		}
		return false;
	}

	/**
	 * Get Current Grading Sytem
	 * @return object
	 */
	public function get_system()
	{
		$type = $this->grading_system_type();
		if($type){
			$sql = "
				SELECT 
				*
				FROM $this->_table
				WHERE type = ?
				AND is_deleted = 0
				ORDER BY range_start DESC
			";
			return $this->query($sql,[$type]);
		}
		return false;
	}

	/**
	 * Create Grading Table Record from POST form
	 * @param  array $p from html form
	 * @return object    result
	 */
	public function create_from_post($p)
	{
		$ret = new StdClass;
		$ret->code = 'e';
		$ret->id = '';
		$ret->msg = "Transacion failed, please try again.";

		$data = $p['grading_system'];
		$type = $this->grading_system_type();
		$data['type'] = $type ? $type : 'numeric';
		$rs = (object)$this->insert($data);
		if($rs->status){
			$ret->id = $rs->id;
			$ret->code = 's';
			$ret->msg = "New record was successfully saved";
		}
		return $ret;
	}

	/**
	 * Update Grading System from Posted Form
	 * @param  array $p from html form
	 * @return object    result
	 */
	public function update_from_post($p)
	{
		$gt = $p['gs'];

		$ctr = 0;
		if($gt){
			foreach ($gt as $id => $v) {
				$v['id'] = $id;
				$same = $this->pull($v);
				if(!$same){
					unset($v['id']);
					$update = $this->update($id, $v);
					if($update){
						activity_log('Update Grading System',false, "ID:$id Data:".arr_str($v));
						$ctr++;
					}
				}
			}
		}

		if($ctr>0){
			$ret['code'] = 's';
			$ret['msg'] = 'Grading Table was successfully updated.';
			return (object)$ret;
		}else{
			$ret['code'] = 'e';
			$ret['msg'] = 'No record was updated.';
			return (object)$ret;
		}
	}

	public function delete_record($id)
	{
		unset($data);
		$data['is_deleted'] = 1;
		$data['deleted_date'] = NOW;
		$data['deleted_by'] = $this->ci->userid;
		$rs = $this->update($id, $data);
		if($rs){
			$ret['code'] = 's';
			$ret['msg'] = 'Grading Table record was successfully deleted.';
			return (object)$ret;
		}else{
			$ret['code'] = 'e';
			$ret['msg'] = 'Grading Table record failed to be removed, please try again';
			return (object)$ret;
		}
	}

	/**
	 * Convert Numeric to letter
	 * @param float $value
	 * @param string
	 */
	public function convert_grade($value, $system, $round = true, $return_obj = false)
	{
		$sql = "SELECT 
							`id`, 
							`value`,
							`desc`
						FROM grading_system
						WHERE (? >= range_start AND ? <= range_end )
						AND is_deleted = 0
						AND type = ?
						LIMIT 1";
		if($round){
			$rs = $this->query($sql,[round($value), round($value), $system], true);
		}else{
			$rs = $this->query($sql,[intval($value), intval($value), $system], true);
		}
		
		if($return_obj){
			return $rs;
		}

		if($rs){
			return $rs->value;
		}

		return false;
	}

	/**
	 * Get Highest Maximum Grade / Score
	 * @return float
	 */
	public function maximum_score()
	{
		$type = $this->grading_system_type();
		$sql = "SELECT MAX(range_end) AS maximum FROM $this->_table WHERE type = ? AND is_deleted = 0";
		$rs = $this->query($sql,[$this->type], true);
		if($rs){
			return $rs->maximum ? $rs->maximum : 100;
		}
		return 0;
	}

	/**
	 * Get Lowest Mininum Grade / Score
	 * @return float
	 */
	public function minimum_score()
	{
		$type = $this->grading_system_type();
		$sql = "SELECT MIN(range_start) AS minimum FROM $this->_table WHERE type = ? AND is_deleted = 0";
		$rs = $this->query($sql,[$this->type], true);
		if($rs){
			return $rs->minimum ? $rs->minimum : 0;
		}
		return 0;
	}
}
