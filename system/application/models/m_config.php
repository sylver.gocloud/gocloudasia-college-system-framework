<?php
class M_config Extends MY_Model
{
	protected $_table = '';
	public function __construct()
	{
		parent::__construct();
	}

	public function get_day_legend($day)
	{
		$r = "";
		switch ( strtolower( trim($day)) ) {
			case 'monday':
				$r = "M";
				break;
			case 'tuesday':
				$r = "T";
				break;
			case 'wednesday':
				$r = "W";
				break;
			case 'thursday':
				$r = "TH";
				break;
			case 'friday':
				$r = "F";
				break;
			case 'saturday':
				$r = "S";
				break;
			case 'sunday':
				$r = "SU";
				break;
		}
		return $r;
	}
}