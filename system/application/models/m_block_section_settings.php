<?php

class M_block_section_settings extends MY_Model
{
	protected $_table = 'block_system_settings';
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function get_all_list()
	{
		$sql = "SELECT open_semesters.use,open_semesters.id, CONCAT( academic_years.year_from,  ' - ', academic_years.year_to ) AS academic_year, semesters.name FROM open_semesters
				LEFT JOIN academic_years ON (academic_years.id = open_semesters.academic_year_id) 
				LEFT JOIN semesters ON (semesters.id = open_semesters.semester_id) 
				";
		$query = $this->db->query($sql);
		return $query->num_rows() > 0 ? $query->result() : FALSE;
	}
	
	public function find_all()
	{
		$sql = "select 	
					id,
					name,
					created_at,
					updated_at
				FROM $this->_table
				ORDER BY name";
		$query = $this->db->query($sql);
		return $query->num_rows() > 0 ? $query->result() : FALSE;
	}
	
	function get_ay_sem()
	{
		$sql = "SELECT CONCAT( ay.year_from,  ' - ', ay.year_to ) AS academic_year, s.name, ay.year_from,ay.year_to,semester_id
					 FROM  `open_semesters` AS os
					RIGHT JOIN semesters AS s ON s.id = os.semester_id
					RIGHT JOIN academic_years AS ay ON ay.id = os.academic_year_id
					WHERE os.use =1
					LIMIT 1";
		$query = $this->db->query($sql);
		return $query->num_rows() > 0 ? $query->row() : FALSE;
	}
	
	function update_open_semester_userId($open_semester_id,$userid)
	{
		$input['open_semester_id'] = $open_semester_id;
		$input['updated_at'] = NOW;

		$this->db->where('user_id', $userid);
		$this->db->update('open_semester_employee_settings', $input); 
		return $this->db->affected_rows() > 0 ? array('status'=>'true') : array('status'=>'false');
	}
	
	function insert_open_semester_userId($open_semester_id,$userid)
	{
		$input['open_semester_id'] = $open_semester_id;
		$input['user_id'] = $user_id;
		$input['created_at'] = NOW;
		$input['updated_at'] = NOW;

		$this->db->insert('open_semester_employee_settings', $input); 
		return $this->db->affected_rows() > 0 ? array('status'=>'true') : array('status'=>'false');
	}
	
	public function create_block_section_settings($input = false)
	{
		$this->db->insert($this->_table,$input);
		return $this->db->affected_rows() > 0 ? array('status'=>'true','id'=>$this->db->insert_id()) : array('status'=>'false');
	}
	
	public function get_block_section_settings($id = false)
	{
		$sql = "select b.*,s.name as semester
				FROM $this->_table b
				LEFT JOIN semesters s ON s.id = b.semester_id
				WHERE b.id = ?
				ORDER BY b.id";
		$query = $this->db->query($sql,array($id));
		return $query->num_rows() > 0 ? $query->row() : FALSE;
	}
	
	public function update_block_section_settings($data = false, $where = false)
	{
		$this->db->set($data)->where('id',$where)->update($this->_table);
		return $this->db->affected_rows() > 0 ? array('status'=>'true') : array('status'=>'false');
	}
	
	public function delete_open_semesters($where = false)
	{
		$this->db->where('id',$where)->delete($this->_table);
		return $this->db->affected_rows() > 0 ? array('status'=>'true') : array('status'=>'false');
	}
	
	public function reset_open_semester(){
		$sql = "UPDATE
				$this->_table
				SET `use` = ?";
		$query = $this->db->query($sql,array('0'));
	}

	#CLONE SUBJECTS OF PREVIOUS ACADEMIC YEAR
		public function cloned_blocks_from_previous_sy($current_academic_year_id =  false, $prev_academic_year_id = false){

			if($current_academic_year_id === false){ return; }
			$this->load->model('M_subjects');
			$this->load->model('M_block_subjects');
			$this->load->model('M_course_blocks');

			#get the current academic year
				unset($get);
				$get['where']['id'] = $current_academic_year_id;
				$get['single'] = true;
				$c_ay = $this->get_record('academic_years', $get);
				if($c_ay === false){ return;}

			#check if there are already block for the current AY - if yes do not continue
				unset($get);
				$get['where']['academic_year_id'] = $current_academic_year_id;
				$rm_chk = $this->get_record(false, $get);
				if($rm_chk){ return false; }


			#get previous academic year
				if($prev_academic_year_id){
					unset($get);
					$get['where']['id'] = $prev_academic_year_id;
					$get['single'] = true;
					$p_ay = $this->get_record('academic_years', $get);
				}else{
					unset($get);
					$get['where']['year_from < '] = $c_ay->year_from;
					$get['single'] = true;
					$p_ay = $this->get_record('academic_years', $get);
				}
				if($p_ay === false){ return;}

			#get blocks in the previous AY
				unset($get);
				$get['where']['academic_year_id'] = $p_ay->id;
				$get['array'] = true;
				$get['not_in'] = array(
						'field' => 'name',
						'data' => "SELECT name FROM block_system_settings WHERE academic_year_id = '".$c_ay->id."'"
					);
				$prev_blocks = $this->get_record('block_system_settings', $get);
				if($prev_blocks === false){ return; }
				foreach ($prev_blocks as $key => $block) {
					$data = $block;
					$block = (object)$block;
					unset($data['id']);
					unset($data['created_at']);
					unset($data['updated_at']);

					$data['academic_year_id'] = $c_ay->id;
					$new_block = (object)$this->insert($data);
					if($new_block->status){
						
						$created_block = $this->M_block_section_settings->get($new_block->id);
						if($created_block === false){ continue; }
						#GET SUBJECT OF THE PREVIOUS BLOCK AND CLONE
							unset($get);
							$get['where']['block_system_setting_id'] = $block->id;
							$blocks_subjects = $this->get_record('block_subjects', $get);
							if($blocks_subjects){
								foreach ($blocks_subjects as $key => $value) {
									
									#Get the subject based on subject_id 
									unset($get);
									$get['where']['id'] = $value->subject_id;
									$get['array'] = true;
									$get['single'] = true;
									$subject = $this->get_record('subjects', $get);
									
									if($subject === false){ continue; }
									$obj_subject = (object)$subject;
									#search subjects in the previous SY - if none - then add
									unset($get);
									$get['where']['year_from'] = $c_ay->year_from;
									$get['where']['year_to'] = $c_ay->year_to;
									$get['where']['sc_id'] = $obj_subject->sc_id;
									$get['where']['code'] = $obj_subject->code;
									$get['where']['course_id'] = $obj_subject->course_id;
									$get['where']['year_id'] = $obj_subject->year_id;
									$get['where']['semester_id'] = $obj_subject->semester_id;
									$get['single'] = true;
									$get['array'] = true;
									$prev_subject = $this->get_record('subjects', $get);
									// vd($prev_subject);
									if($prev_subject){

										#clone & save block subject
										unset($data);
										$data['block_system_setting_id'] = $created_block->id;
										$data['subject_id'] = $prev_subject['id'];
										$this->M_block_subjects->insert($data);
									}else{

										$data = $subject;
										unset($data['id']);
										unset($data['created_at']);
										unset($data['updated_at']);
										$data['year_from'] = $c_ay->year_from;
										$data['year_to'] = $c_ay->year_to;
										$this->load->model('M_subjects');
										$add_subject = (object)$this->M_subjects->insert($data);
										if($add_subject->status){

											#clone & save block subject	
											unset($data);
											$data['block_system_setting_id'] = $created_block->id;
											$data['subject_id'] = $add_subject->id;
											$this->M_block_subjects->insert($data);
										}
									}

								}
							}
						
						#GET ASSIGN COURSES OF THE PREVIOUS BLOCK AND CLONE
							unset($get);
							$get['where']['block_system_setting_id'] = $block->id;
							$get['array'] = true;
							$course_blocks = $this->M_course_blocks->get_record('course_blocks', $get);
							if($course_blocks === false){ continue; }

							foreach ($course_blocks as $key => $value) {
								$data = $value;
								$value = (object)$value;

								unset($data['id']);
								unset($data['created_at']);
								unset($data['updated_at']);
								$data['block_system_setting_id'] = $new_block->id;
								$data['academic_year_id'] = $c_ay->id;

								#check if already added
								unset($get);
								$get['block_system_setting_id'] = $new_block->id;
								$get['academic_year_id'] = $c_ay->id;
								$get['course_id'] = $value->course_id;
								$exist = $this->M_course_blocks->pull($get);
								if($exist){ continue; }

								#Save course block
								$rs = $this->M_course_blocks->insert($data);

							}
					}
				}
		}
}

?>