<?php

class M_open_semesters extends MY_Model
{
	protected $_table = 'open_semesters';
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function get_current_semesters()
	{
		$ret['system'] = false;
		$ret['user'] = false;
		$ret['grading_period'] = false;
		$ret['summary'] = false;
		$ret['enrollment'] = false;

		$ret = (object)$ret;

		/** SYSTEM OPEN SEMESTER
		 * Get the Current Open Semester of the System
		 */
			$sql = "
				SELECT
					os.id as open_semester_id,
					os.academic_year_id, 
					ay.year_from, 
					ay.year_to, 
					s.id, 
					s.name
				FROM $this->_table os
				LEFT JOIN academic_years ay ON (ay.id = os.academic_year_id) 
				LEFT JOIN semesters s ON (s.id = os.semester_id) 
				WHERE (os.use = '1')
			";

			$q = $this->db->query($sql);

			if($q->num_rows() > 0){
				$r = $q->row();
				unset($a);
				$a['open_semester_id'] = $r->open_semester_id;
				$a['academic_year_id'] = $r->academic_year_id;
				$a['school_year'] = $r->year_from.'-'.$r->year_to;
				$a['year_from'] = $r->year_from;
				$a['year_to'] = $r->year_to;
				$a['semester_id'] = $r->id;
				$a['semester'] = $r->name;
				$ret->system = (object)$a;
			}

		/** USER OPEN SEMESTER
		 * Get the Current Open Semester of the User
		 * If None, set the current system open semester
		 */
			$user_op = $this->get_user_open_semester();
			
			if($user_op === false){
				$this->load->model('M_open_semester_employee_settings','m_open_ses');

				//create record base from system open semester
				unset($save);
				$save['user_id'] = $this->session->userdata('userid');
				$save['open_semester_id'] = $ret->system->open_semester_id;
				$this->m_open_ses->insert($save);

				//after saving get record again
				$user_op = $this->get_user_open_semester();	
			}
			
			if($user_op){
				unset($a);
				$a['open_semester_id'] = $user_op->open_semester_id;
				$a['academic_year_id'] = $user_op->academic_year_id;
				$a['school_year'] = $user_op->year_from.'-'.$user_op->year_to;
				$a['year_from'] = $user_op->year_from;
				$a['year_to'] = $user_op->year_to;
				$a['semester_id'] = $user_op->id;
				$a['semester'] = $user_op->name;
				$ret->user = (object)$a;
			}

		/** SYSTEM GRADING PERIOD
		 * Get the Current Grading Period
		 */
			$sql = "
				SELECT
					id, grading_period
				FROM grading_periods
				WHERE (is_set = '1')
			";

			$q = $this->db->query($sql);

			if($q->num_rows() > 0){
				$r = $q->row();
				unset($a);
				$a['id'] = $r->id;
				$a['grading_period'] = $r->grading_period;
				$ret->grading_period = (object)$a;
			}

		/** ENROLLMENT OPEN SEMESTER
		 */
						$sql = "
				SELECT
					os.id as open_semester_id,
					os.academic_year_id, 
					ay.year_from, 
					ay.year_to, 
					s.id, 
					s.name
				FROM $this->_table os
				LEFT JOIN academic_years ay ON (ay.id = os.academic_year_id) 
				LEFT JOIN semesters s ON (s.id = os.semester_id) 
				WHERE (os.open_enrollment = '1')
			";

			$q = $this->db->query($sql);

			if($q->num_rows() > 0){
				$r = $q->row();
				unset($e);
				$e['open_semester_id'] = $r->open_semester_id;
				$e['academic_year_id'] = $r->academic_year_id;
				$e['school_year'] = $r->year_from.'-'.$r->year_to;
				$e['year_from'] = $r->year_from;
				$e['year_to'] = $r->year_to;
				$e['semester_id'] = $r->id;
				$e['semester'] = $r->name;
				$ret->enrollment = (object)$e;
			}

		/** Summary */
			unset($a);
			$a = array();
			if($ret->system){
				$a['system'] = $ret->system->school_year.'('.$ret->system->semester.')';
			}else{ $a['system'] = 'No System Open Semester Set'; }
			if($ret->user){
				$a['user'] = $ret->user->school_year.'('.$ret->user->semester.')';
			}else{ $a['user'] = 'No User Open Semester Set'; }
			if($ret->grading_period){
				//$a['user'] .= "|".$ret->grading_period->grading_period;
			}

			$ret->summary = (object)$a;

		return $ret;
	}

	/**
	 * Get User Open Semester
	 */
	public function get_user_open_semester()
	{
		$id = $this->session->userdata('userid');
		$sql = "SELECT
			open_semesters.id as open_semester_id, 
			open_semesters.academic_year_id, 
			academic_years.year_from, 
			academic_years.year_to, 
			semesters.id, semesters.name 
			FROM open_semester_employee_settings 
			LEFT JOIN open_semesters ON (open_semester_employee_settings.open_semester_id = open_semesters.id) 
			LEFT JOIN academic_years ON (academic_years.id = open_semesters.academic_year_id) 
			LEFT JOIN semesters ON (semesters.id = open_semesters.semester_id) 
			WHERE (open_semester_employee_settings.user_id = '$id')";
		$q = $this->db->query($sql); 
		return $q->num_rows() > 0 ? $q->row() : false;
	}

	/**
   	 * Get Current Open Semester for the System and the User
   	 * Remove / Change : current open semester will be the system open system for all users
   	 * Changes suggested by Mam Vivian
   	 * Sylver : 8/13/2014
	 */
	function get_current_semesters_old()
	{
		$ret['system'] = false;
		$ret['user'] = false;
		$ret['grading_period'] = false;
		$ret['summary'] = false;

		$ret = (object)$ret;

		/*SYSTEM OPEN SEMESTER*/
		$sql = "
			SELECT
				os.id as open_semester_id,
				os.academic_year_id, 
				ay.year_from, 
				ay.year_to, 
				s.id, 
				s.name
			FROM $this->_table os
			LEFT JOIN academic_years ay ON (ay.id = os.academic_year_id) 
			LEFT JOIN semesters s ON (s.id = os.semester_id) 
			WHERE (os.use = '1')
		";

		$q = $this->db->query($sql);

		if($q->num_rows() > 0){
			$r = $q->row();
			unset($a);
			$a['open_semester_id'] = $r->open_semester_id;
			$a['academic_year_id'] = $r->academic_year_id;
			$a['school_year'] = $r->year_from.'-'.$r->year_to;
			$a['year_from'] = $r->year_from;
			$a['year_to'] = $r->year_to;
			$a['semester_id'] = $r->id;
			$a['semester'] = $r->name;
			$ret->system = (object)$a;
		}
		/*END OF OPEN SEMESTER*/

		/*USER OPEN SEMESTER*/
		$id = $this->userid;
		$sql = "SELECT 
			open_semesters.academic_year_id, 
			academic_years.year_from, 
			academic_years.year_to, 
			semesters.id, semesters.name 
		FROM open_semester_employee_settings 
		LEFT JOIN open_semesters ON (open_semester_employee_settings.open_semester_id = open_semesters.id) 
		LEFT JOIN academic_years ON (academic_years.id = open_semesters.academic_year_id) 
		LEFT JOIN semesters ON (semesters.id = open_semesters.semester_id) 
		WHERE (open_semester_employee_settings.user_id = '$id')";
		$q = $this->db->query($sql); 
		$q = $this->db->query($sql);

		if($q->num_rows() > 0){
			$r = $q->row();
			unset($a);
			$a['academic_year_id'] = $r->academic_year_id;
			$a['school_year'] = $r->year_from.'-'.$r->year_to;
			$a['year_from'] = $r->year_from;
			$a['year_to'] = $r->year_to;
			$a['semester_id'] = $r->id;
			$a['semester'] = $r->name;
			$ret->user = (object)$a;
		}
		/*END OF USER OPEN SEM*/

		/*SYSTEM GRADING PERIOD*/
		$sql = "
			SELECT
				id, grading_period
			FROM grading_periods
			WHERE (is_set = '1')
		";

		$q = $this->db->query($sql);

		if($q->num_rows() > 0){
			$r = $q->row();
			unset($a);
			$a['id'] = $r->id;
			$a['grading_period'] = $r->grading_period;
			$ret->grading_period = (object)$a;
		}
		/*END OF GP*/

		unset($a);
		$a = array();
		if($ret->system){
			$a['system'] = $ret->system->school_year.'|'.$ret->system->semester;
		}else{ $a['system'] = 'No System Open Semester Set'; }
		if($ret->user){
			$a['user'] = $ret->user->school_year.'|'.$ret->user->semester;
		}else{ $a['user'] = 'No User Open Semester Set'; }
		if($ret->grading_period){
			// $a['system'] .= "|".$ret->grading_period->grading_period;
			$a['user'] .= "|".$ret->grading_period->grading_period;
		}

		$ret->summary = (object)$a;

		return $ret;
	}
	
	function get_open_semester_userId($id){
		$sql = "SELECT open_semester_id,id FROM open_semester_employee_settings WHERE user_id = '$id'";
		$q = $this->db->query($sql); 
		
		return $q->num_rows() >= 1 ? $q->row() : FALSE;
	}
	
	function get_open_semester($id){
		$sql = "SELECT open_semesters.academic_year_id, academic_years.year_from, academic_years.year_to, semesters.id, semesters.name FROM open_semester_employee_settings 
		LEFT JOIN open_semesters ON (open_semester_employee_settings.open_semester_id = open_semesters.id) 
		LEFT JOIN academic_years ON (academic_years.id = open_semesters.academic_year_id) 
		LEFT JOIN semesters ON (semesters.id = open_semesters.semester_id) 
		WHERE (open_semester_employee_settings.user_id = '$id')";
		$q = $this->db->query($sql); 
		
		$sql2 = "SELECT open_semesters.academic_year_id, academic_years.year_from, academic_years.year_to, semesters.id, semesters.name FROM open_semesters 
		LEFT JOIN academic_years ON (academic_years.id = open_semesters.academic_year_id) 
		LEFT JOIN semesters ON (semesters.id = open_semesters.semester_id) 
		WHERE (open_semesters.use = '1')";
		$q2 = $this->db->query($sql);
		
		return $q->num_rows() >= 1 ? $q->row() : $q2->row();
	}

	function get_all_list()
	{
		$sql = "SELECT 
							open_semesters.use,
							open_semesters.id, 
							CONCAT( academic_years.year_from,  ' - ', academic_years.year_to ) AS academic_year, 
							semesters.name,
							open_semesters.open_enrollment
						FROM open_semesters
						LEFT JOIN academic_years ON (academic_years.id = open_semesters.academic_year_id) 
						LEFT JOIN semesters ON (semesters.id = open_semesters.semester_id) 
						ORDER BY academic_years.year_from DESC, semesters.level DESC
						";
		$query = $this->db->query($sql);
		return $query->num_rows() > 0 ? $query->result() : FALSE;
	}
	
	function get_ay_sem()
	{
		$sql = "SELECT 
					CONCAT( ay.year_from,  ' - ', ay.year_to ) AS academic_year, 
					os.id,
					s.name, 
					ay.year_from,
					ay.year_to,
					semester_id,
					open_enrollment
				FROM  `open_semesters` AS os
				RIGHT JOIN semesters AS s ON s.id = os.semester_id
				RIGHT JOIN academic_years AS ay ON ay.id = os.academic_year_id
				WHERE os.use =1
				LIMIT 1";
		$query = $this->db->query($sql);
		return $query->num_rows() > 0 ? $query->row() : FALSE;
	}
	
	function update_open_semester_userId($open_semester_id,$userid)
	{
		$input['open_semester_id'] = $open_semester_id;
		$input['updated_at'] = NOW;

		$this->db->where('user_id', $userid);
		$this->db->update('open_semester_employee_settings', $input); 
		return $this->db->affected_rows() > 0 ? array('status'=>'true') : array('status'=>'false');
	}
	
	function insert_open_semester_userId($open_semester_id,$userid)
	{
		$input['open_semester_id'] = $open_semester_id;
		$input['user_id'] = $user_id;
		$input['created_at'] = NOW;
		$input['updated_at'] = NOW;

		$this->db->insert('open_semester_employee_settings', $input); 
		return $this->db->affected_rows() > 0 ? array('status'=>'true') : array('status'=>'false');
	}
	
	public function create_open_semesters($input = false)
	{
		$this->db->insert($this->_table,$input);
		return $this->db->affected_rows() > 0 ? array('status'=>'true') : array('status'=>'false');
	}
	
	public function get_open_semesters($id = false)
	{
		$sql = "select *
				FROM $this->_table
				WHERE id = ?
				ORDER BY id";
		$query = $this->db->query($sql,array($id));
		return $query->num_rows() > 0 ? $query->row() : FALSE;
	}
	
	public function update_open_semesters($data = false, $where = false)
	{
		$this->db->set($data)->where('id',$where)->update($this->_table);
		return $this->db->affected_rows() > 0 ? array('status'=>'true') : array('status'=>'false');
	}
	
	public function delete_open_semesters($where = false)
	{
		$this->db->where('id',$where)->delete($this->_table);
		return $this->db->affected_rows() > 0 ? array('status'=>'true') : array('status'=>'false');
	}
	
	public function reset_open_semester(){
		$sql = "UPDATE
				$this->_table
				SET `use` = ?";
		$query = $this->db->query($sql,array('0'));
	}

	public function auto_create_open_semesters($academic_year_id = false){

		unset($get);
		$get['where']['id'] = $academic_year_id;
		$get['single'] = true;
		$sy = $this->get_record('academic_years', $get);
		if($sy === false){ return; }

		unset($get);
		$sems = $this->get_record('semesters');
		if($sems === false){ return; }
		if($sems){
			foreach ($sems as $key => $value) {
			  #check if already added
				unset($get);
				$get['where']['academic_year_id'] = $academic_year_id;
				$get['where']['semester_id'] = $value->id;
				$get['single'] = true;
				$exist = $this->get_record('open_semesters', $get);
				if($exist){ continue; }

				unset($data);
				$data['academic_year_id'] = $academic_year_id;
				$data['semester_id'] = $value->id;
				$rs = $this->insert($data);
			}
		}
	}

	/**
	 * Update Open Enrollment
	 * @param int $id OPen Semester Id
	 * @param int $value 1/0
	 */
	public function open_enrollment($id , $value)
	{
		$r = new stdClass;
		$r->code = "e";
		$r->msg = "Transaction failed, please try again";

		$this->close_all_enrollment(); //Update all enrollment to zero first

		$save['open_enrollment'] = $value;
		$rs = $this->update($id, $save);
		if($rs){

			$r->code = "s";
			if($value === "1"){
				$r->msg = "Enrollment was successfully opened.";
				activity_log('Open Enrollment',false, "Open Semester ID : $id");
			}else{
				$r->msg = "Enrollment was successfully closed.";
				activity_log('Close Enrollment',false, "Open Semester ID : $id");
			}

			return $r;
		}

		return $r;
	}

	/**
	 * Close All Enrollment in open semester
	 */
	public function close_all_enrollment()
	{
		return $this->query2("Update $this->_table set open_enrollment = 0");
	}
}
