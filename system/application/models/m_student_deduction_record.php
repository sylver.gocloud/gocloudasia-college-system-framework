<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_student_deduction_record Extends MY_Model
{
	protected $_table = 'student_deductions_record';
	protected $_uid = 'id';
	
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}	

	public function get_all_students_with_deduction($start=0,$limit=100, $filter = false, $all = false, $ret_count = false){
		
		$start = $this->db->escape_str($start);
		$limit = $this->db->escape_str($limit);
		$ci =& get_instance();

		$semester_id = $ci->cos->user->semester_id;
		$sy_from = $ci->cos->user->year_from;
		$sy_to = $ci->cos->user->year_to;
		
		$param[] = $semester_id;
		$param[] = $sy_from;
		$param[] = $sy_to;
		
		//ADD FILTERS
		$xfilter = "";
		if($filter != false){
			//if filter is array
			if(is_array($filter)){
				foreach($filter as $key => $value){
					$xfilter .= " $key ? ";
					$param[] = $value;
				}
			}
			else{ //if filter is string
				$xfilter = $filter;
			}
			
		}

		$sql = "
			SELECT
				sdr.id,
				CONCAT(e.lastname,', ',e.fname,' ',e.middle) as fullname,
				e.studid,
				e.id as enrollment_id,
				c.course,
				c.course_code,
				y.year, 
				y.id as year_id
			FROM $this->_table sdr
			LEFT JOIN enrollments e ON e.id = sdr.enrollment_id
			LEFT JOIN years y ON y.id = e.year_id
			LEFT JOIN courses c ON c.id = e.course_id
			WHERE e.semester_id = ?
			AND e.sy_from = ?
			AND e.sy_to = ?
			$xfilter
			GROUP BY sdr.enrollment_id
			ORDER BY studid ASC
		";

		if($all == false){
			$sql .= " LIMIT ".$limit.",".$start;
		}
		
		$query = $this->db->query($sql,$param);

		if($ret_count == false)
		{
			return $query->num_rows() > 0 ? $query->result() : FALSE;
		}else
		{
			return $query->num_rows(); 
		}
	}

		public function get_all_deduction($start=0,$limit=100, $filter = false, $all = false, $ret_count = false){
		
		$start = $this->db->escape_str($start);
		$limit = $this->db->escape_str($limit);
		$ci =& get_instance();

		$semester_id = $ci->cos->user->semester_id;
		$sy_from = $ci->cos->user->year_from;
		$sy_to = $ci->cos->user->year_to;
		
		$param[] = $semester_id;
		$param[] = $sy_from;
		$param[] = $sy_to;
		
		//ADD FILTERS
		$xfilter = "";
		if($filter != false){
			//if filter is array
			if(is_array($filter)){
				foreach($filter as $key => $value){
					$xfilter .= " $key ? ";
					$param[] = $value;
				}
			}
			else{ //if filter is string
				$xfilter = $filter;
			}
			
		}

		$sql = "
			SELECT
				sdr.id,
				CONCAT(e.lastname,', ',e.fname,' ',e.middle) as fullname,
				e.studid,
				e.id as enrollment_id,
				c.course,
				c.course_code,
				y.year, 
				y.id as year_id,
				sdr.deduction_name,
				sdr.amount,
				sdr.remarks
			FROM $this->_table sdr
			LEFT JOIN enrollments e ON e.id = sdr.enrollment_id
			LEFT JOIN years y ON y.id = e.year_id
			LEFT JOIN courses c ON c.id = e.course_id
			WHERE e.semester_id = ?
			AND e.sy_from = ?
			AND e.sy_to = ?
			$xfilter
			ORDER BY e.name
		";

		if($all == false){
			$sql .= " LIMIT ".$limit.",".$start;
		}
		
		$query = $this->db->query($sql,$param);

		if($ret_count == false)
		{
			return $query->num_rows() > 0 ? $query->result() : FALSE;
		}else
		{
			return $query->num_rows(); 
		}
	}

	public function get_student_deduction_byenrollment_id($id = false)
	{
		$sql = "select *
				FROM $this->_table
				WHERE enrollment_id = ?
				ORDER BY id";
		$query = $this->db->query($sql,array($id));
		return $query->num_rows() > 0 ? $query->result() : FALSE;
	}	
}