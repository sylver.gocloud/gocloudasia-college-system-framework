<?php

class M_block_subjects extends MY_Model
{
	protected $_table = 'block_subjects';
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	public function find_all()
	{
		$sql = "select 	
					*
				FROM $this->_table
				ORDER BY id";
		$query = $this->db->query($sql);
		return $query->num_rows() > 0 ? $query->result() : FALSE;
	}
	
	public function get_block_subjects($id){

		// $query = $this->db->where('block_system_setting_id',$id)->get($this->_table);
		// return $query->num_rows() > 0 ? $query->result() : FALSE;
		
		$sql = "SELECT 
					block_subjects.id,
					block_subjects.subject_id,
					block_subjects.block_system_setting_id,
					users.name as teacher,
					master_subjects.code,
					master_subjects.subject,
					master_subjects.units,
					master_subjects.lec,
					master_subjects.lab,
					subjects.is_open_time,
					subjects.time,
					subjects.day,
					r.name as room,
					subjects.subject_taken,
					subjects.original_load
				FROM
				$this->_table
				LEFT JOIN subjects ON subjects.id = block_subjects.subject_id
				LEFT JOIN master_subjects ON master_subjects.ref_id = subjects.ref_id
				LEFT JOIN users ON users.id = subjects.teacher_user_id
				LEFT JOIN rooms r ON r.id = subjects.room_id
				WHERE block_subjects.block_system_setting_id = ?
				ORDER BY block_subjects.id";
		
		$query = $this->db->query($sql,array($id));
		// vd($this->db->last_query());
		return $query->num_rows() > 0 ? $query->result() : FALSE;
	}

	public function get_block_subjects_a($id){
		
		$sql = "SELECT 
					block_subjects.subject_id,
					block_subjects.block_system_setting_id,
					ms.code,
					ms.subject,
					ms.units,
					ms.lec,
					ms.lab,
					subjects.is_open_time,
					subjects.time,
					subjects.day,
					subjects.subject_load,
					subjects.subject_taken,
					rooms.name as room,
					users.name as instructor
				FROM
				$this->_table
				LEFT JOIN subjects ON subjects.id = block_subjects.subject_id
				LEFT JOIN master_subjects ms ON ms.ref_id = subjects.ref_id
				LEFT JOIN rooms ON rooms.id = subjects.room_id
				LEFT JOIN users ON users.id = subjects.teacher_user_id
				WHERE block_subjects.block_system_setting_id = ?
				ORDER BY block_subjects.id";
			
		$query = $this->db->query($sql,array($id));
		return $query->num_rows() > 0 ? $query->result() : FALSE;
	}
	
	public function create_block_subjects($input = false)
	{
		$this->db->insert($this->_table,$input);
		return $this->db->affected_rows() > 0 ? array('status'=>'true','id'=>$this->db->insert_id()) : array('status'=>'false');
	}
	
	public function update_open_semesters($data = false, $where = false)
	{
		$this->db->set($data)->where('id',$where)->update($this->_table);
		return $this->db->affected_rows() > 0 ? array('status'=>'true') : array('status'=>'false');
	}
	
	public function delete_block_subjects($where = false)
	{
		$this->db->where('id',$where)->delete($this->_table);
		return $this->db->affected_rows() > 0 ? array('status'=>'true') : array('status'=>'false');
	}
	
	public function delete_all_block_subjects_per_block_section($where = false)
	{
		$this->db->where('block_system_setting_id',$where)->delete($this->_table);
		return $this->db->affected_rows() > 0 ? array('status'=>'true') : array('status'=>'false');
	}

}

?>