<?php
class M_transcript Extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $ci =& get_instance();
    }
    
    public function get_transcript($studid, $print = false)
    {
        $transcript = false;

        #STEP 1 : Get All Enrollments 
            $sql = "
                SELECT e.id as enrollment_id
                FROM enrollments e
                LEFT JOIN semesters s ON s.id = e.semester_id
                WHERE e.studid = ?
                AND e.is_deleted = 0
                AND e.is_drop = 0
                ORDER BY e.sy_from DESC, s.level ASC
            ";

            $rs = $this->query($sql, array($studid));

        #STEP 2 : LOOP enrollments and get subjects plus final grade
        // Load Student Library
        $this->load->library('_Student', array('enrollment_id'=> 0));
        if($rs){
            foreach ($rs as $k => $e) {
                
                $this->_student->enrollment_id = $e->enrollment_id;
                $this->_student->load_default_function();
                $transcript[] = array(
                        'profile' => $this->_student->profile,
                        'subjects' => $this->_student->student_subjects,
                    );
            }
            // vd($transcript);
            return $transcript;
        }

        return false;
    }
}
