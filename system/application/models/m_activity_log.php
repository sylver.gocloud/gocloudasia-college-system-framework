<?php

class M_activity_log Extends MY_Model
{
	protected $_table = 'activity_log';
	
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	
	public function create_log($input = false)
	{
		$this->db->insert($this->_table,$input);
		return $this->db->affected_rows() > 0 ? array('status'=>'true') : array('status'=>'false');
	}
}