<?php

class M_systempatches Extends MY_Model
{
	protected $_table;
	protected $_uid;
	protected $_timestamp = false;

	public function __construct()
	{
		parent::__construct();
		$this->load->database();

		$this->_uid = "id";
	}

	public function set_table($table="", $_uid = false)
	{
		$this->_table = $table;
		if($_uid){
			$this->_uid = $_uid;
		}else{
			$this->_uid = "id";
		}
	}

	/**
	 * System patch to add system custom message menu
	 */
	public function add_system_custom_message_menu()
	{
		$sly = new stdClass;
		$sly->status = false;
		$sly->msg = "Transaction failed";

		$menu = "insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `created_at`, `remarks`) values('system_settings/system_custom_message','System Custom Message','sys_cus_m',NULL,'100','2',NULL,'1',NULL,'2014-12-20 16:52:20','Contains system global messages')";

		$this->load->model('M_main_menus');
		$check_if_exist = $this->M_main_menus->pull(array('controller'=>'system_settings/system_custom_message'),'id');
		if($check_if_exist){
			$sly->msg = "Menu already added.";
			return $sly;
		}

		$rs = $this->query2($menu);

		if($rs){
			$sly->status = TRUE;
			$sly->msg = "System Custom Message Menu where successfully added. Please add this manually to department menu and package menu.";
		}

		return $sly;
	}

	/**
	 * System patch to add grading system menu
	 */
	public function add_grading_system_menu()
	{
		$sly = new stdClass;
		$sly->status = false;
		$sly->msg = "Transaction failed";

		$menu = "insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `created_at`, `updated_at`, `remarks`) values('grading_system/setup','Grading System','grdsys',NULL,'100','2',NULL,'1',NULL,'2015-03-06 15:29:24','2015-03-06 15:29:46','Grading System Setup');";

		$this->load->model('M_main_menus');
		$check_if_exist = $this->M_main_menus->pull(array('controller'=>'grading_system/setup'),'id');
		if($check_if_exist){
			$sly->msg = "Menu already added.";
			return $sly;
		}

		$rs = $this->query2($menu);

		if($rs){
			$sly->status = TRUE;
			$sly->msg = "Grading System Menu where successfully added. Please add this manually to department menu and package menu.";
		}

		return $sly;
	}

	/**
	 * System patch to add Advance settings menu
	 */
	public function add_advance_settings_menu()
	{
		$sly = new stdClass;
		$sly->status = false;
		$sly->msg = "Transaction failed";

		$menu = "insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `created_at`, `updated_at`, `remarks`) values('system_settings/advance_settings','Advance Settings','advnce_stng',NULL,'100','2',NULL,'1',NULL,'2015-03-06 15:29:24','2015-03-06 15:29:46','Advance Setup / Settings for the System');";

		$this->load->model('M_main_menus');
		$check_if_exist = $this->M_main_menus->pull(array('controller'=>'system_settings/advance_settings'),'id');
		if($check_if_exist){
			$sly->msg = "Menu already added.";
			return $sly;
		}

		$rs = $this->query2($menu);

		if($rs){
			$sly->status = TRUE;
			$sly->msg = "Advance Setting Menu where successfully added. Please add this manually to department menu and package menu.";
		}

		return $sly;
	}

	/**
	 * Add Default data for system custom message
	 */
	public function add_system_custom_message_data()
	{
		$sly = new stdClass;
		$sly->status = false;
		$sly->msg = "Transaction failed";

		$message[1]['type'] = "offline_portal";
		$message[1]['desc'] = "System message when student portal is offline or under development.";
		$message[1]['message'] = "&lt;p style=&quot;text-align: center;&quot;&gt;&lt;span style=&quot;color: #000000;&quot;&gt;&lt;strong&gt;&lt;span style=&quot;font-size: 36pt;&quot;&gt;Welcome to&nbsp;&lt;/span&gt;&lt;/strong&gt;&lt;strong&gt;&lt;span style=&quot;font-size: 36pt;&quot;&gt;The Student Portal&lt;/span&gt;&lt;/strong&gt;&lt;/span&gt;&lt;/p&gt;
			&lt;p style=&quot;text-align: center;&quot;&gt;&nbsp;&lt;/p&gt;
			&lt;p style=&quot;text-align: center;&quot;&gt;&lt;span style=&quot;font-size: 14pt; color: #333300;&quot;&gt;&lt;strong&gt;Student Portal is currently offline.&lt;/strong&gt;&lt;/span&gt;&lt;/p&gt;
			&lt;p style=&quot;text-align: center;&quot;&gt;&lt;span style=&quot;color: #333300;&quot;&gt;&nbsp;&lt;/span&gt;&lt;/p&gt;
			&lt;p style=&quot;text-align: center;&quot;&gt;&lt;span style=&quot;font-size: 14pt; color: #333300;&quot;&gt;&lt;strong&gt;Please return in a moment.&lt;/strong&gt;&lt;/span&gt;&lt;/p&gt;";

		$message[2]['type'] = "offline_enrollment";
		$message[2]['desc'] = "System message when enrollment or registration is offline or under development.";
		$message[2]['message'] = "&lt;p style=&quot;text-align: center;&quot;&gt;&lt;span style=&quot;color: #000000;&quot;&gt;&lt;strong&gt;&lt;span style=&quot;font-size: 36pt;&quot;&gt;Welcome To Student Registration&lt;/span&gt;&lt;/strong&gt;&lt;strong&gt;&lt;span style=&quot;font-size: 36pt;&quot;&gt;&lt;br /&gt;&lt;/span&gt;&lt;/strong&gt;&lt;/span&gt;&lt;/p&gt;
			&lt;p style=&quot;text-align: center;&quot;&gt;&nbsp;&lt;/p&gt;
			&lt;p style=&quot;text-align: center;&quot;&gt;&lt;span style=&quot;font-size: 14pt; color: #333300;&quot;&gt;&lt;strong&gt;Enrollment is not yet open.&lt;/strong&gt;&lt;/span&gt;&lt;/p&gt;
			&lt;p style=&quot;text-align: center;&quot;&gt;&lt;span style=&quot;color: #333300;&quot;&gt;&nbsp;&lt;/span&gt;&lt;/p&gt;
			&lt;p&gt;&nbsp;&lt;/p&gt;
			&lt;p style=&quot;text-align: center;&quot;&gt;&lt;span style=&quot;font-size: 14pt; color: #333300;&quot;&gt;&lt;strong&gt;Please return in a moment.&lt;/strong&gt;&lt;/span&gt;&lt;/p&gt;";

		$message[3]['type'] = "enrollment_success";
		$message[3]['desc'] = "System message when the student enrollment was a success.";
		$message[3]['message'] = "&lt;p style=&quot;text-align: center;&quot;&gt;&lt;span style=&quot;color: #000000;&quot;&gt;&lt;strong&gt;&lt;span style=&quot;font-size: 36pt;&quot;&gt;Congratulations, your enrollment was successfully submitted.&lt;/span&gt;&lt;/strong&gt;&lt;/span&gt;&lt;/p&gt;";

		$message[4]['type'] = "enrollment_confirmation";
		$message[4]['desc'] = "System message when the student registration is waiting for confirmation.";
		$message[4]['message'] = "&lt;p&gt;&lt;strong&gt;&lt;span style=&quot;font-size: 14pt;&quot;&gt;&there4;&lt;/span&gt; &lt;span style=&quot;font-size: 10pt;&quot;&gt;Waiting for Confirmation :&lt;/span&gt;&lt;/strong&gt;&lt;/p&gt;
			&lt;p&gt;&lt;em&gt;&lt;span style=&quot;font-size: 12pt;&quot;&gt;&nbsp; &nbsp; &nbsp;To continue your enrollment, the registrar&#039;s office must confirm your registration. &lt;/span&gt;&lt;/em&gt;&lt;/p&gt;
			&lt;p&gt;&lt;em&gt;&lt;span style=&quot;font-size: 12pt;&quot;&gt;&nbsp; &nbsp; &nbsp;You can go back and continue upon the confirmation. &lt;/span&gt;&lt;/em&gt;&lt;/p&gt;
			&lt;p&gt;&lt;em&gt;&lt;span style=&quot;font-size: 12pt;&quot;&gt;&nbsp; &nbsp; &nbsp;Please take note your &quot;Enrollment Reference Number&quot; above.&lt;/span&gt;&lt;/em&gt;&lt;/p&gt;
			&lt;p&gt;&nbsp;&lt;/p&gt;";

		$this->set_table('system_custom_message');

		$ctr = 0;

		foreach ($message as $k => $v) {
			$exist = $this->pull(array('type'=>$v['type']));
			if($exist){continue;}

			$rs = (object)$this->insert($v);
			if($rs->status){
				$ctr++;
			}
		}

		if($ctr>0){
			$sly->status = TRUE;
			$sly->msg = "System Custom Message default data was successfully added.";
		}

		return $sly;
	}

	/**
	 * Create Master Subject Main Menu
	 */
	public function add_master_subject_menus()
	{
		$sly = new stdClass;
		$sly->status = false;
		$sly->msg = "Transaction failed";

		$usertype = "admin";

		//update subject caption to subject schedule
		$this->db->query("UPDATE menus SET caption = 'Subject Schedule' WHERE caption = 'subject' AND controller = 'subjects'");
		$this->db->query("UPDATE package_menus SET caption = 'Subject Schedule' WHERE caption = 'subject' AND controller = 'subjects'");

		$this->load->model(['M_main_menus','M_menus','M_package_menus']);

		/** Create New Library Menus
		 * Shoould Manually add to Department Menus and Package Menus Afterwards
		*/
			$menus = array(
					"master_subjects/index"=>"insert into `main_menus` (`controller`, `caption`, `menu_grp`, `menu_sub`, `menu_num`, `menu_lvl`, `menu_icon`, `visible`, `attr`, `created_at`, `updated_at`, `remarks`) values('master_subjects/index','Master Subjects','ms_subj',NULL,'100','2',NULL,'1',NULL,'2015-01-13 02:22:22','2015-01-13 02:22:22','The master list of subjects or course being offered.')",
				);			

			$ctr = 0;
			
			// for main menus
			foreach ($menus as $c => $qry) {
				
				// check first if menu already there
				$main = $this->M_main_menus->pull(array('controller'=>$c)); if($main) { continue; }
				$rs_insert_main = $this->M_main_menus->query2($qry);

				if($rs_insert_main){
					$ctr++;
				}
			}
				
		if($ctr > 0){
			$sly->status = true;
			$sly->msg = "Master Subject Menu was successfully added.";
		}else{
			$sly->msg = "No menus have been added. Either menu already exists or the process failed.";
		}

		return $sly;
	}

	/**
	 * Update Menus
	 * confirm enrollees to unconfirm enrollees
	 * list of confirm enrollees to confirmed enrollees
	 */
	public function fix_confirm_enrollees_menus()
	{
		$this->load->model('M_menus');
		$this->M_menus->query2("UPDATE menus SET caption = 'Unconfirmed enrollees' WHERE caption = 'confirm enrollees'");
		$this->M_menus->query2("UPDATE main_menus SET caption = 'Unconfirmed enrollees' WHERE caption = 'confirm enrollees'");
		$this->M_menus->query2("UPDATE package_menus SET caption = 'Unconfirmed enrollees' WHERE caption = 'confirm enrollees'");

		$this->M_menus->query2("UPDATE menus SET caption = 'Confirmed Enrollees' WHERE caption LIKE '%list of confirm%'");
		$this->M_menus->query2("UPDATE main_menus SET caption = 'Confirmed Enrollees' WHERE caption LIKE '%list of confirm%'");
		$this->M_menus->query2("UPDATE package_menus SET caption = 'Confirmed Enrollees' WHERE caption LIKE '%list of confirm%'");

		$this->M_menus->query2("UPDATE menus SET caption = 'Download Account Receivable' WHERE caption LIKE '%Download Remaining Balance Report%'");
		$this->M_menus->query2("UPDATE menus SET caption = 'Account Receivable' WHERE caption LIKE '%Remaining Balance Report%'");

		$this->M_menus->query2("UPDATE package_menus SET caption = 'Download Account Receivable' WHERE caption LIKE '%Download Remaining Balance Report%'");
		$this->M_menus->query2("UPDATE package_menus SET caption = 'Account Receivable' WHERE caption LIKE '%Remaining Balance Report%'");

		$this->M_menus->query2("UPDATE main_menus SET caption = 'Download Account Receivable' WHERE caption LIKE '%Download Remaining Balance Report%'");
		$this->M_menus->query2("UPDATE main_menus SET caption = 'Account Receivable' WHERE caption LIKE '%Remaining Balance Report%'");

		$sly = new stdClass;
		$sly->status = true;
		$sly->msg = "Menu Caption was successfully updated";
		return $sly;
	}
}