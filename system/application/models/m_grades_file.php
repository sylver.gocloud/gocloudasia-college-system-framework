<?php

class M_grades_file Extends MY_Model
{
	protected $_table = 'grades_file';
	protected $_uid = 'id';
	protected $_timestamp = false;
	
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->helper('grade');
	}

	/**
	 * Update Grade from Form
	 * @param  array $post from form
	 * @return object
	 */
	public function update_grade($post)
	{	
		$grades = $post['subject'];
		$remarks = $post['final'];
		$this->load->helper(['my_string','grade']);

		$ctr = 0;
		foreach ($grades as $id => $v) {
			
			$g = $this->pull($id,'id,raw,value');
			
			// if value was changed
			if(($v) != ($g->value)){
				unset($data);
				$data['value'] = cleanNull($v) == "" ? NULL : (trim($v));
				$data['updated_by'] = $this->session->userdata('userid');
				$data['converted'] = cleanNull($v) == "" ? "" : convert_grade(trim($v));
				if($g->raw){}
				else{
					$data['raw'] = $v;
					$data['user_id'] = $this->session->userdata('userid');
				}
				
				//update
				$u = $this->update($id, $data);
				if($u){
					activity_log('Update Grade',false, ' Data : '.arr_str($data));
					$ctr++;
				}
			}
		}

		$this->load->model('M_student_subjects','mss');
		foreach ($remarks as $id => $v) {
			
			$s = $this->mss->pull($id,'id,remarks');
			$r = $v['remarks'];
			// if value was changed
			if(cleanNull($r) != cleanNull($s->remarks)){
				unset($data);
				$data['remarks'] = trim($r);
				
				$u = $this->mss->update($id, $data);
				if($u){
					activity_log('Update Subject Remarks',false, ' Data : '.arr_str($data));
					$ctr++;
				}
			}

			# for final grade
			$fg = $v['value'];
			if(cleanNull($fg) != cleanNull($s->value)){
				unset($data);
				if(!$s->raw){
					$data['raw'] = trim($fg);
				}
				$data['value'] = trim($fg);
				$data['converted'] = cleanNull($fg) == "" ? "" : convert_grade(trim($fg));
				
				$u = $this->mss->update($id, $data);
				if($u){
					activity_log('Update Subject Final Grade',false, ' Data : '.arr_str($data));
					$ctr++;
				}
			}
		}

		if($ctr>0){
			$ret['code'] = "s";
			$ret['msg'] = "Student Grade was successfully updated.";

			return (object)$ret;
		}else{
			$ret['code'] = "e";
			$ret['msg'] = "No grade updated.";
			return (object)$ret;
		}
	}

	/**
	 * Update Grade from Form - for teacher users page
	 * @param  array $post from form
	 * @return object
	 */
	public function teacher_update_grade($post)
	{
		$this->load->helper(['my_string','grade']);
		$grades = $post['studentsubject'];
		$remarks =  $post['finalgrade'];

		$ctr = 0;
		foreach ($grades as $id => $v) {
			
			$g = $this->pull($id,'id,raw,value');
			
			// if value was changed
			if(($v) != ($g->value)){
				unset($data);
				$data['value'] = cleanNull($v) == "" ? NULL : (trim($v));
				$data['updated_by'] = $this->session->userdata('userid');
				$data['converted'] = cleanNull($v) == "" ? "" : convert_grade(trim($v));
				if($g->raw){}
				else{
					$data['raw'] = $v;
					$data['user_id'] = $this->session->userdata('userid');
				}

				//update
				$u = $this->update($id, $data);
				if($u){
					activity_log('Teacher Update Grade',false, ' Data : '.arr_str($data));
					$ctr++;
				}
			}
		}

		$this->load->model('M_student_subjects','mss');
		foreach ($remarks as $id => $v) {
			
			$s = $this->mss->pull($id,'id,remarks');
			$r = $v['remarks'];
			// if value was changed
			if(cleanNull($r) != cleanNull($s->remarks)){
				unset($data);
				$data['remarks'] = trim($r);
				
				$u = $this->mss->update($id, $data);
				if($u){
					activity_log('Teacher Update Subject Remarks',false, ' Data : '.arr_str($data));
					$ctr++;
				}
			}
		}

		if($ctr>0){
			$ret['code'] = "s";
			$ret['msg'] = "Student Grade was successfully updated.";

			return (object)$ret;
		}else{
			$ret['code'] = "e";
			$ret['msg'] = "No grade updated.";
			return (object)$ret;
		}
	}

	/**
	 * Update Students Grade from Form - for teacher users page
	 * @param  array $post from form
	 * @return object
	 */
	public function teacher_update_student_grade($post)
	{
		$this->load->helper(['my_string','grade']);
		$grades = $post['studentsubject'];
		$remarks =  $post['finalgrade'];

		$ctr = 0;
		foreach ($grades as $id => $v) {
			
			$g = $this->pull($id,'id,raw,value');
			
			// if value was changed
			if(($v) != ($g->value)){
				unset($data);
				$data['value'] = cleanNull($v) == "" ? NULL : (trim($v));
				$data['updated_by'] = $this->session->userdata('userid');
				$data['converted'] = cleanNull($v) == "" ? "" : convert_grade(trim($v));
				if($g->raw){}
				else{
					$data['raw'] = $v;
					$data['user_id'] = $this->session->userdata('userid');
				}

				//update
				$u = $this->update($id, $data);
				if($u){
					activity_log('Teacher Update Grade',false, ' Data : '.arr_str($data));
					$ctr++;
				}
			}
		}

		$this->load->model('M_student_subjects','mss');
		foreach ($remarks as $id => $v) {
			
			$s = $this->mss->pull($id,'id,remarks');
			$r = $v['remarks'];
			// if value was changed
			if(cleanNull($r) != cleanNull($s->remarks)){
				unset($data);
				$data['remarks'] = trim($r);
				
				$u = $this->mss->update($id, $data);
				if($u){
					activity_log('Teacher Update Subject Remarks',false, ' Data : '.arr_str($data));
					$ctr++;
				}
			}
		}

		if($ctr>0){
			$ret['code'] = "s";
			$ret['msg'] = "Student Grade was successfully updated.";

			return (object)$ret;
		}else{
			$ret['code'] = "e";
			$ret['msg'] = "No grade updated.";
			return (object)$ret;
		}
	}

	/**
	 * Recompute grade
	 * @param  int $id Enrollment
	 * @return object Result
	 */
	public function recompute($id)
	{

		$sql = "
			SELECT 
			id, value, converted
			FROM $this->_table
			WHERE enrollment_id = ?
			AND is_deleted = 0
		";
		$g = $this->query($sql,[$id]);
		
		$ctr = 0;

		if($g){
			foreach ($g as $k => $v) {
				
				$old = cleanNull($v->converted);
				$new = convert_grade($v->value);
				if($old != $new){
					unset($data);
					$data['converted'] = $v->value ? $new : '';
					$s = $this->update($v->id,$data);
					if($s){
						$ctr++;
					}
				}
			}
		}

		return $ctr > 0 ? true : false;
	}

	/**
	 * Recompute grade by Subject & Enrollment
	 */
	public function recompute_by_subject($enrollment_id,$ref_id)
	{

		$sql = "
			SELECT 
			gf.id, gf.value, gf.converted
			FROM $this->_table gf
			LEFT JOIN studentsubjects ss ON ss.id = gf.ss_id
			LEFT JOIN subjects s ON s.id = ss.subject_id
			WHERE gf.enrollment_id = ?
			AND gf.is_deleted = 0
			AND s.ref_id = ?

		";
		$g = $this->query($sql,[$enrollment_id,$ref_id]);
		
		$ctr = 0;

		if($g){
			foreach ($g as $k => $v) {
				
				$old = cleanNull($v->converted);
				$new = convert_grade($v->value);
				if($old != $new){
					unset($data);
					$data['converted'] = $v->value ? $new : '';
					$s = $this->update($v->id,$data);
					if($s){
						$ctr++;
					}
				}
			}
		}

		return $ctr > 0 ? true : false;
	}

	/**
	 * Recompute grade per record
	 * @param int  $gf_id grades_file id
	 * @return object result
	 */
	public function recalculate_per_record($gf_id)
	{
		$v = $this->pull($gf_id,'id,value,converted');
		
		if($v){

			$old = cleanNull($v->converted);
			$new = convert_grade($v->value);
			
			if($old != $new){
				unset($data);
				$data['converted'] = $v->value ? $new : '';
				$s = $this->update($v->id,$data);
				if($s){
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Get Final Grade for Student Subject
	 * @param  int $enrollment_id     enrollments table id
	 * @param  int $studentsubject_id studentsubjects table id
	 * @return string                    final grade
	 */
	public function get_final_grade($enrollment_id, $studentsubject_id)
	{
		$this->load->model('M_grading_periods');
		$gp = $this->M_grading_periods->get_last_gp(); 
		if($gp){
			//get grades file
			$sql = "
				SELECT
				id, converted
				FROM $this->_table
				WHERE enrollment_id = ?
				AND ss_id = ?
				AND gp_id = ?
				AND is_deleted = 0
				LIMIT 1
			";
			$g = $this->query($sql,[$enrollment_id,$studentsubject_id, $gp->id], true);
			// vp($g);
			if($g){
				return $g->converted;
			}
		}
		return "";
	}
}