<?php
	
class M_assign_courses Extends CI_Model
{

  private $_table = "assign_courses";
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	public function insert_to_db($data)
	{
	  $this->db->insert($this->_table,$data);
	}
	
	public function get_employee_assigned_courses($id)
	{
	  $sql = "SELECT assign_courses.id, assign_courses.course_id, assign_courses.employee_id, courses.course AS course_name
	          FROM assign_courses
	          LEFT JOIN courses on courses.id = assign_courses.course_id
	          WHERE assign_courses.employee_id = ?";
	          
	  $query = $this->db->query($sql,array($id));
	  // vd($this->db->last_query());
	  return $query->num_rows() > 0 ? $query->result() : FALSE;
	}
	
	public function delete_from_db($id)
	{
		$this->db->where('id', $id);
		$this->db->delete($this->_table); 
		return $this->db->affected_rows() > 0 ? array('status'=>'true') : array('status'=>'false');
	}
	
	public function get($id = false,$array = false,$where = false)
	{
		if($id == false)
		{
			if($array == false)
			{
				if($where == false)
				{
					$query = $this->db->get($this->_table);
					return $query->num_rows() > 0 ? $query->result() : FALSE;
				}else
				{
					$query = $this->db->where($where)->get($this->_table);
					return $query->num_rows() > 0 ? $query->result() : FALSE;
				}
			}else
			{
				if($where == false)
				{
					$query = $this->db->select($array)->get($this->_table);
					return $query->num_rows() > 0 ? $query->result() : FALSE;
				}else
				{
					$query = $this->db->select($array)->where($where)->get($this->_table);
					return $query->num_rows() > 0 ? $query->result() : FALSE;
				}
			}
		}else
		{
			if($array == false)
			{
				if($where == false)
				{
					$query = $this->db->where('id',$id)->get($this->_table);
					return $query->num_rows() > 0 ? $query->row() : FALSE;
				}else
				{
					$query = $this->db->where('id',$id)->where($where)->get($this->_table);
					return $query->num_rows() > 0 ? $query->row() : FALSE;
				}
			}else
			{	
				if($where == false)
				{
					$query = $this->db->select($array)->where('id',$id)->get($this->_table);
					return $query->num_rows() > 0 ? $query->row() : FALSE;
				}else
				{
					$query = $this->db->select($array)->where('id',$id)->where($where)->get($this->_table);
					return $query->num_rows() > 0 ? $query->row() : FALSE;
				
				}
			}
		}
	}
}
