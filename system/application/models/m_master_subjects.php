<?php
class M_master_subjects Extends MY_Model
{
	protected $_table = 'master_subjects';
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	/**
	 * Check if Master Subject (table master_subjects) already have a schedule (table subjects)
	 * @param int $c_id Curriculum ID
	 * @param int $ref_id Master Subjects ref_id
	 */

	public function is_subject_has_schedule($c_id, $ref_id, $major_id = false)
	{
		$cos = $this->get_cos();
		$cos = $cos->user;

		$this->load->model('M_curriculum');
		$c = $this->M_curriculum->pull($c_id, 'course_id');
		if($c == false){ return false; }

		$sql = "
			SELECT 
			id 
			FROM subjects 
			WHERE ref_id = ? 
			AND course_id = ? ";
		$param[] = $ref_id;
		$param[] = $c->course_id;

		if($major_id){
			$sql .= " AND specialization_id = ?";
			$param[] = $major_id;
		}

		$sql .= " AND semester_id = ? 
			AND year_from = ? 
			AND year_to = ? 
			LIMIT 1
		";
		
		$param[] = $cos->semester_id;
		$param[] = $cos->year_from;
		$param[] = $cos->year_to;
		$rs = $this->query($sql,$param,true);
		// vd($this->db->last_query());
		return $rs ? TRUE : FALSE;
	}

	/**
	 * Save Subject from Post 
	 * is used by master_subject and curriculum
	 * @param array $post POST FORM
	 */
	public function save_subject($post)
	{
		$this->load->model('M_sys_par');

		$data = $post['subjects'];
		$data['created_by'] = $this->ci->userid;
		$data['ay_id'] = $this->ci->cos->user->academic_year_id;
		$data['year_from'] = $this->ci->cos->user->year_from;
		$data['year_to'] = $this->ci->cos->user->year_to;
		$data['is_deleted'] = 0;
		$data['ref_id'] = $this->M_sys_par->get_subj_refid_and_update_to_next_series();
		$r = $this->insert($data);
		$r['ref_id'] = "";
		if($r['status']){
			$r['ref_id'] = $data['ref_id'];
			$data['master_subjects ID'] = $id = $r['id'];
			activity_log('Master Subject Create', false, "Table master_subjects id : $id : Data - ".arr_str($data));
		}

		return $r;
	}

	public function ajax_search_subject($post)
	{
		$key = $post['keyword'];

		$sql = "
			SELECT * 
				FROM $this->_table 
			WHERE 
			is_deleted = 0 AND 
			(code like ? OR subject like ?) 
			ORDER BY code
			";
		return $this->query($sql,['%'.$key.'%','%'.$key.'%']);
	}
	
	public function search_subjects($start=0,$limit=100,$data, $ret_count = false, $ret_all = false)
	{
		
		$start = $this->db->escape_str($start);
		$limit = $this->db->escape_str($limit);
		$filtered = array();
		unset($data['search_subjects']);
		foreach($data as $key => $value)
		{
			if($value && $value != "")
			{
				$filtered[trim($key)] = trim($value);
			}
		}
		
		if($ret_count)
		{
			$query = $this->db->select(array('id','sc_id','code','subject','units','lec','lab','time','day','room','subject_load','original_load','year_to','year_from'))->like($filtered)->get($this->_table);
		}
		else
		{
			if($ret_all){
				$query = $this->db->select(array('id','sc_id','code','subject','units','lec','lab','time','day','room','subject_load','original_load','year_to','year_from'))->like($filtered)->get($this->_table);
			}else{
				$query = $this->db->select(array('id','sc_id','code','subject','units','lec','lab','time','day','room','subject_load','original_load','year_to','year_from'))->like($filtered)->get($this->_table, $start, $limit);
			}
			
		}
		
		if($ret_count){
			return $query->num_rows();
		}else{
			return $query->num_rows() > 0 ? $query->result() : FALSE;
		}
	
	}

	public function get_subject($id)
	{
		$sql = "
			SELECT
			*,
			HOUR(class_start) as class_start_hr,
			HOUR(class_end) as class_end_hr,
			MINUTE(class_start) as class_start_min,
			MINUTE(class_end) as class_end_min
			FROM $this->_table
			WHERE id = ?
		";
		$q = $this->db->query($sql, array($id));
		return $q->num_rows() > 0 ? $q->row() : false;
	}

	/** Get Subject Profile
	 * @param $id Subject ID
	 * @return object
	 */
	public function get_profile($id)
	{
		$sql = "
			SELECT
			s.*,
			u.name as teacher
			FROM $this->_table s 
			LEFT JOIN users u ON u.id = s.teacher_user_id
			WHERE s.id = ?
		";
		return $this->query($sql, array($id), true);
	}

	/**
	 * Get All Available Subjects
	 */
	public function available_subjects()
	{
		$sql = "
			SELECT
				* 
			FROM $this->_table
			WHERE is_deleted = 0
			ORDER BY code
		";
		return $this->query($sql);
	}

	/**
	 * Get All Available Subjects 
	 * Exclude Curriculum Subjects
	 * @param int $id Curriculum Id
	 */
	public function available_subjects_for_curriculum($id, $exclude_curriculum = true)
	{
		unset($get);
		$get['where']['is_deleted'] = 0;
		$get['order_by'] = "code";
		if($exclude_curriculum){
			$get['not_in'] = array(
				'field' => 'ref_id',
				'data' => "Select subject_refid FROM curriculum_subjects WHERE curriculum_id = ".$this->db->escape($id)." AND is_deleted = 0"
			);
		}
		$rs = $this->get_record($this->_table,$get);
		return $rs;
	}

	/**
	 * Format for CSV upload
	 * @param string $type
	 * @return array       
	 */
	public function _master_subject_format($type='csv')
	{

		switch ($type) {
			case 'csv':
					$fields = array();
					$fields[0][0] = 'Course No.';
					$fields[0][1] = 'Description';
					$fields[0][2] = 'Lec Unit';
					$fields[0][3] = 'Lab Unit';
					return $fields;
				break;
			
			default:
				# code...
				break;
		}

		return false;
	}

	/**
	 * Upload from Uploaded CSV file
	 * @path path of the csv file
	 */
	public function upload_from_csv($path)
	{
		$this->load->helper('file');
		$this->load->model('M_sys_par');

		$sly = new stdClass;
		$sly->status = false;

		$string = trim(read_file($path)); // READ and Extract String from the file
		$arr_str = explode("\n", $string); // Explode the string per line and save to array
		
		/**  LOOP ARRAY STRING AND SAVE DATA ***/
		if($arr_str){
			$ctr = 0;
			foreach ($arr_str as $key => $l) {
				
				if($key == 0){ continue; } //EXCLUDE THE FIRST, CONTAINS THE HEADER FILE
				if(!trim($l)){ continue; }

				$ara = explode(',', $l); // EXTRACT BY COMMA

				/* FORMAT
				$fields[0][0] = 'Course No.';
				$fields[0][1] = 'Description';
				$fields[0][2] = 'Lec Unit';
				$fields[0][3] = 'Lab Unit';*/

				unset($data);
				
				$data['code'] = $code = trim(isset($ara[0]) ? trim($ara[0]) : '');
				$data['subject'] = $subject = trim(isset($ara[1]) ? trim($ara[1]) : '');
				$data['lec'] = $lec = trim(isset($ara[2]) ? trim($ara[2]) : '');
				$data['lab'] = $lab = trim(isset($ara[3]) ? trim($ara[3]) : '');
				$data['units'] = intval($lec) + intval($lab);

				$dup = $this->pull(array('code'=>$code,'is_deleted'=>0),'id');

				if(!$dup){
					$data['created_by'] = $this->ci->userid;
					$data['ay_id'] = $this->ci->cos->user->academic_year_id;
					$data['year_from'] = $this->ci->cos->user->year_from;
					$data['year_to'] = $this->ci->cos->user->year_to;
					$data['is_deleted'] = 0;
					$data['ref_id'] = $this->M_sys_par->get_subj_refid_and_update_to_next_series();
					$add = (object)$this->insert($data);
					if($add->status){
						$ctr++;
					}
				}
			}

			if($ctr > 0){
				$sly->msg = "There are $ctr subjects has been successfully added to your database.";
				$sly->status = true;
				return $sly;
			}
		}

		$sly->msg = "No media has been upload, please follow the format given and try again";
		return $sly;
	}

	/**
	 * Remove Subject from user - is_deleted = 1
	 * @param  int $id     master_subjects table id
	 * @param  int $userid users table id - current user
	 * @return bool
	 */
	public function delete_master_subjects($id,$userid)
	{
		unset($data);
		$data['is_deleted'] = 1;
		$data['deleted_by'] = $userid;
		$rs = $this->update($id, $data);
		if($rs){
			activity_log('Delete Master Subejcts',false,' Data : '.arr_str($data));
			return true;
		}
		return false;
	}
}
