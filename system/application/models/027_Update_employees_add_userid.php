<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class Migration_Update_employees_add_userid extends CI_Migration {
	public function up(){
		
		if($this->db->table_exists('employees'))
		{
			if (!$this->db->field_exists('user_id', 'employees'))
			{
				$this->db->query("ALTER TABLE `employees` ADD COLUMN `user_id` INT(12) NULL COMMENT 'Link to Users Table' AFTER `employee_status`; ");
			}
		}	
	}
	public function down(){
		if($this->db->table_exists("employees")){
			
			if ($this->db->field_exists('user_id', 'employees'))
			{
				$this->dbforge->drop_column('employees', 'user_id');
			}
		}
	}
}