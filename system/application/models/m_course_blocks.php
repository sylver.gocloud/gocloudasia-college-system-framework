<?php

class M_course_blocks extends MY_Model
{
	protected $_table = 'course_blocks';
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	public function find_all()
	{
		$sql = "select 	
					*
				FROM $this->_table
				ORDER BY id";
		$query = $this->db->query($sql);
		return $query->num_rows() > 0 ? $query->result() : FALSE;
	}
	
	public function get_course_blocks($id){
		
		$sql = "SELECT
					course_blocks.id,
					course_blocks.year_id,
					course_blocks.course_id,
					course_blocks.semester_id,
					course_blocks.academic_year_id,
					course_blocks.block_system_setting_id,
					course_blocks.specialization_id,
					years.year,
					courses.course,
					courses.course_code,
					cs.specialization as major
				FROM $this->_table
				LEFT JOIN years ON (years.id = course_blocks.year_id) 
				LEFT JOIN courses ON (courses.id = course_blocks.course_id)
				LEFT JOIN course_specialization cs ON (cs.id = course_blocks.specialization_id)
				WHERE course_blocks.block_system_setting_id = ?
				ORDER BY course_blocks.id";
			
		$query = $this->db->query($sql,array($id));
		return $query->num_rows() > 0 ? $query->result() : FALSE;
		
	}
	
	public function create_course_blocks($input = false)
	{
		$this->db->insert($this->_table,$input);
		return $this->db->affected_rows() > 0 ? array('status'=>'true','id'=>$this->db->insert_id()) : array('status'=>'false');
	}
	
	public function delete_course_blocks($where = false)
	{
		$this->db->where('id',$where)->delete($this->_table);
		return $this->db->affected_rows() > 0 ? array('status'=>'true') : array('status'=>'false');
	}
	
	public function delete_all_course_blocks_per_block_section($where = false)
	{
		$this->db->where('block_system_setting_id',$where)->delete($this->_table);
		return $this->db->affected_rows() > 0 ? array('status'=>'true') : array('status'=>'false');
	}
}

?>