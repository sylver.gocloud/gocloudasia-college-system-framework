<?php

class M_settings extends CI_Model{

	private $_table = 'settings';
	
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	function get_settings(){
		$q = $this->db->get('settings');
		return $q->num_rows() >= 1 ? $q->row() : FALSE;
	}
	
	public function update_table($data,$id)
	{
		foreach($data as $key => $value)
		{
			$columns_array[] = $key; 
		}
		$columns = implode(',',$columns_array);
		$query = $this->db->select($columns)->where($data)->get($this->_table);
		if($query->num_rows() > 0)
		{
			return true;
		}else
		{
			$this->db->set($data)->where('id',$id)->update($this->_table);
			return $this->db->affected_rows() > 0 ? TRUE : FALSE;
		}
	}

}