<?php


class M_student_grades Extends MY_Model
{
	protected	$_table = "student_grades";
	protected	$_table2 = "student_grade_categories";
	protected	$_table3 = "student_grade_files";
	protected $_timestamp = true;
	
	public function create_student_grade($input = false)
	{
		$this->db->insert($this->_table,$input);
		return $this->db->affected_rows() > 0 ? array('status'=>'true', 'id'=>$this->db->insert_id()) : array('status'=>'false');
	}


	public function get_student_grades_for_subject($id)
	{
		$ci =& get_instance();
		$yf = $this->db->escape_str($this->open_semester->year_from);
		$yt = $this->db->escape_str($this->open_semester->year_to);
		$s = $this->db->escape_str($this->open_semester->id);
		$sql = 'SELECT 
				DISTINCT sgf.user_id,sgc.category,sgc.value,e.studid,sg.remarks, e.name as fullname
				FROM student_grade_files sgf
				LEFT JOIN student_grades sg ON sg.student_grade_file_id = sgf.id
				LEFT JOIN student_grade_categories sgc ON sgc.student_grade_id = sg.id
				LEFT JOIN enrollments e ON e.id = sgf.user_id
				LEFT JOIN subjects  s ON s.id = sg.subjectid
				WHERE sg.subjectid = ?';
		
		return $this->db->query($sql,array($id))->result();
		// vd($this->db->last_query());
	}
	
	public function get_student_grade_by_student_grade_file_id($id)
	{
		$sql = 'SELECT 
					subjects.sc_id,
					subjects.subject,
					subjects.code,
					student_grades.value,
					student_grades.remarks,
					subjects.lec,
					subjects.lab
				FROM student_grades
				LEFT JOIN subjects ON subjects.id = subject_id
				WHERE  student_grades.student_grade_file_id = ?
				ORDER BY subjects.subject';
		return $this->db->query($sql,array($id))->result();
	}
	
	public function get_student_grade_subjects($id)
	{
		$ci =& get_instance();
		$yf = $this->db->escape_str($this->open_semester->year_from);
		$yt = $this->db->escape_str($this->open_semester->year_to);
		$s = $this->db->escape_str($this->open_semester->id);
		$sql = 'SELECT 
				DISTINCT sgf.user_id,sgc.category,sgc.value, sg.remarks, e.name, e.studid, e.id
				FROM student_grade_files sgf
				LEFT JOIN student_grades sg ON sg.student_grade_file_id = sgf.id
				LEFT JOIN student_grade_categories sgc ON sgc.student_grade_id = sg.id
				LEFT JOIN enrollments e ON e.id = sgf.user_id
				LEFT JOIN subjects  s ON s.id = sg.subjectid
				WHERE sg.subjectid = ? 
				AND sgc.category != "Remedial"
				AND sgc.category != "Semi-Finals"
				AND e.is_paid = 1
				AND e.sy_from = '.$yf.' 
				AND e.sy_to = '.$yt.'
				AND e.semester_id = '.$s.'
				ORDER BY e.name ASC, FIELD(sgc.category, "Preliminary, Midterm, Semi-Finals, Finals") DESC
				';
		$result = $this->db->query($sql,array($id));
		if ($result->num_rows() > 0) {
		  foreach ($result->result() as $key => $result) {
		    $data[$result->studid]["grades"][] = $result->value;
		    $data[$result->studid]['name'] = $result->name;
		    $data[$result->studid]['studid'] = $result->studid;
		    $data[$result->studid]['remarks'] = $result->remarks;
		    $data[$result->studid]['id'] = $result->id;
		  }
		  
		}
		return (object)$data;
	}
	
	
	public function get_student_grades($id)
	{
		$sql = 'SELECT 
				 sgf.user_id,sgc.category,sgc.value, sg.remarks, e.name, e.studid, e.id, s.subject, s.code, s.id as subject_id, s.sc_id,sg.id as student_grade_id, sgc.id as sgcid
				FROM student_grade_files sgf
				LEFT JOIN student_grades sg ON sg.student_grade_file_id = sgf.id
				LEFT JOIN student_grade_categories sgc ON sgc.student_grade_id = sg.id
				LEFT JOIN enrollments e ON e.id = sgf.user_id
				LEFT JOIN subjects  s ON s.id = sg.subjectid
				WHERE sgf.user_id = ?
				AND sgc.category != "Remedial"
				AND sgc.category != "Semi-Finals"
				ORDER BY FIELD(sgc.category, "Preliminary, Midterm, Finals") DESC
				';
		$result = $this->db->query($sql,array($id));
		if ($result->num_rows() > 0) {
		 foreach ($result->result() as $key => $result) { 
		    $data[$result->subject_id]["grades"][$key]['value'] = $result->value;
		    $data[$result->subject_id]["grades"][$key]['sgcid'] = $result->sgcid;

		    $data[$result->subject_id]['remarks'] = $result->remarks;
		    $data[$result->subject_id]['subject_desc'] = $result->subject;
		    $data[$result->subject_id]['code'] = $result->code;
		    $data[$result->subject_id]['sc_id'] = $result->sc_id;
		  }
			return (object)$data;
		}
		
		return false;
	}
	
	public function get_student_grades_transcript($id)
	{
		$sql = 'SELECT sgf.user_id,sgc.category,sgc.value, sg.remarks, e.name, e.studid, e.id, s.subject, s.code,s.sc_id, s.id as subject_id, sg.id as student_grade_id, e.studid, e.created_at, c.course, y.year, sem.name as semester_name, e.id as enrollment_id
				FROM enrollments e
				LEFT JOIN student_grade_files sgf ON sgf.user_id  = e.id
				LEFT JOIN student_grades sg ON sg.student_grade_file_id = sgf.id
				LEFT JOIN student_grade_categories sgc ON sgc.student_grade_id = sg.id
				LEFT JOIN subjects  s ON s.id = sg.subjectid
				LEFT JOIN years  y ON y.id = e.year_id
				LEFT JOIN semesters sem ON sem.id = e.semester_id
				LEFT JOIN courses  c ON c.id = e.course_id
				WHERE e.studid = ?
				AND sgc.category != "Preliminary"
				AND sgc.category != "Midterm"
				AND sgc.category != "Remedial"
				AND sgc.category != "Semi-Finals"
				ORDER BY e.created_at DESC
				';
		
		$result = $this->db->query($sql,array($id));
		if ($result->num_rows() > 0) {
		  foreach ($result->result() as $key => $result) {
		    $data[$result->enrollment_id]["year"] = $result->year;
		    $data[$result->enrollment_id]["semester"] = $result->semester_name;
		    $data[$result->enrollment_id]["course"] = $result->course;
		    
		    $data[$result->enrollment_id]["student_grades"][$result->subject_id]["value"] = $result->value;
		    $data[$result->enrollment_id]["student_grades"][$result->subject_id]['remarks'] = $result->remarks;
		    $data[$result->enrollment_id]["student_grades"][$result->subject_id]['description'] = $result->subject;
		    $data[$result->enrollment_id]["student_grades"][$result->subject_id]['code'] = $result->code; 	   
		    $data[$result->enrollment_id]["student_grades"][$result->subject_id]['sc_id'] = $result->sc_id; 	     
		  }
		  
		}
		return !empty($data) ? (object)$data : FALSE;
	}
	
	public function get_student_grade_file_id($enrollment_id)
	{
	 	$sql = "Select id
		       FROM student_grade_files
		       WHERE student_grade_files.user_id = ?";

			
		$query = $this->db->query($sql, array($enrollment_id));
		return $query->num_rows() > 0 ? $query->row()->id : FALSE;
	}	
	
	public function check_student_grade($sgc_id, $subject_id)
	{
	 	$sql = "Select id 
		       FROM student_grades
		       WHERE student_grades.subjectid = ?
                       WHERE student_grades.student_grade_file_id = ?";

			
		$query = $this->db->query($sql, array($sgc_id, $subject_id));
		return $query->num_rows() > 0 ? TRUE : FALSE;
	}	
	
	public function create_student_grade_file($input = false)
	{
		$this->db->insert($this->_table3,$input);
		return $this->db->affected_rows() > 0 ? array('status'=>'true', 'id'=>$this->db->insert_id()) : array('status'=>'false');
	}
	
	public function create_student_grade_categories($input = false)
	{
		$this->db->insert($this->_table2,$input);
	}

	public function get_sgc($id = false)
	{
		$result = $this->db->where('id', $id)->get($this->_table2);
		return $result->num_rows() > 0 ? $result->row() : FALSE;
	}
	
	
	public function delete_student_grade($where = false)
	{
		$this->db->where('id',$where)->delete($this->_table);
	}

	
	public function get_student_grade_id($where = false, $subject_id = false)
	{
		$id = $this->db->select('id')->where(array('student_grade_file_id'=>$where, 'subjectid'=>$subject_id))->get($this->_table)->row();
		return !empty($id) ? $id->id : FALSE; 
	}


	
	public function delete_student_grade_categories($where = false)
	{
		$this->db->where('student_grade_id',$where)->delete($this->_table2);
	}

	
	public function check_containing_grade($where = false)
	{
	
		$sql = "Select * 
			FROM $this->_table2 sgc  
			WHERE sgc.student_grade_id = ?
			AND sgc.value > 0.0";

		$result = $this->db->query($sql, array($where));
		return $result->num_rows() >= 1 ? TRUE : FALSE;
	}

	
	public function update_student_grade_category($data = false, $where = false)
	{
		$this->db->set($data)->where('id',$where)->update($this->_table2);
		return $this->db->affected_rows() > 0 ? array('status'=>'true', 'value'=>$data['value']) : array('status'=>'false');

	}
	
	public function	check_subjects_no_grade($eid, $sgc_id){
	 $subjects=  $this->M_student_subjects->get_studentsubjects($eid);
         foreach ($subjects as $subject) {
          $sg = $this->check_student_grade($sgc_id, $subject->id);
          if (!empty($sg)) {
          	
          } 	
         }
        }
	
	
	public function get_student_grades_transcript_asc($id)
	{
		$sql = 'SELECT sgf.user_id,sgc.category,sgc.value, sg.remarks, e.name, e.studid, e.id, s.subject, s.code,s.sc_id, s.id as subject_id, sg.id as student_grade_id, e.studid, e.created_at, c.course, y.year, sem.name as semester_name, e.id as enrollment_id,  s.units, e.sy_from, e.sy_to
				FROM enrollments e
				LEFT JOIN student_grade_files sgf ON sgf.user_id  = e.id
				LEFT JOIN student_grades sg ON sg.student_grade_file_id = sgf.id
				LEFT JOIN student_grade_categories sgc ON sgc.student_grade_id = sg.id
				LEFT JOIN subjects  s ON s.id = sg.subjectid
				LEFT JOIN years  y ON y.id = e.year_id
				LEFT JOIN semesters sem ON sem.id = e.semester_id
				LEFT JOIN courses  c ON c.id = e.course_id
				WHERE e.studid = ?
				AND sgc.category != "Preliminary"
				AND sgc.category != "Midterm"
				AND sgc.category != "Remedial"
				AND sgc.category != "Semi-Finals"
				ORDER BY e.created_at ASC
				';
		
		$result = $this->db->query($sql,array($id));
		if ($result->num_rows() > 0) {
		  foreach ($result->result() as $key => $result) {
		    $data[$result->enrollment_id]["year"] = $result->year;
		    $data[$result->enrollment_id]["semester"] = $result->semester_name;
		    $data[$result->enrollment_id]["course"] = $result->course;
		    $data[$result->enrollment_id]["school_year"] = $result->sy_from . ' - ' . $result->sy_to;
		    
		    $data[$result->enrollment_id]["student_grades"][$result->subject_id]["value"] = $result->value;
		    $data[$result->enrollment_id]["student_grades"][$result->subject_id]['remarks'] = $result->remarks;
		    $data[$result->enrollment_id]["student_grades"][$result->subject_id]['description'] = $result->subject;
		    $data[$result->enrollment_id]["student_grades"][$result->subject_id]['code'] = $result->code; 	   
		    $data[$result->enrollment_id]["student_grades"][$result->subject_id]['sc_id'] = $result->sc_id; 	     
		    $data[$result->enrollment_id]["student_grades"][$result->subject_id]['units'] = $result->units; 	     
		  }
		  
		}
		return !empty($data) ? (object)$data : FALSE;
	}
}
