<?php
	
class M_departments Extends MY_Model
{
	protected $_table = 'departments';
	
	public function get_all_department_for_welcome_screen($visible = false, $array = false)
	{
		$param = array();
		$filter = " AND department <> 'student'";
		
		if($visible != false){
			$param[] = $visible;
			$filter .= " AND visible = ?";
		}
		
		$sql = "select *
				FROM $this->_table
				WHERE TRUE
				$filter
				ORDER BY level,ord";
		$query = $this->db->query($sql, $param);
		if($array){
			return $query->num_rows() > 0 ? $query->result_array() : FALSE;
		}else{
			return $query->num_rows() > 0 ? $query->result() : FALSE;
		}
	}
	
	public function get_departments_group_by_levels($visible = false, $array = false)
	{
		$param = array();
		$filter = "";
		
		if($visible != false){
			$param[] = $visible;
			$filter = " AND visible = ?";
		}
		
		$sql = "select level
				FROM $this->_table
				WHERE TRUE
				$filter
				GROUP BY level
				ORDER BY level";
		$query = $this->db->query($sql, $param);
		if($array){
			return $query->num_rows() > 0 ? $query->result_array() : FALSE;
		}else{
			return $query->num_rows() > 0 ? $query->result() : FALSE;
		}
	}

	public function get_departments($department)
	{
		$sql = "SELECT * FROM $this->_table WHERE department = ?";
		$query = $this->db->query($sql, array($department));
		return $query->num_rows() > 0 ? $query->row() : FALSE;
	}

	/**
	 * Get Available Department Except those already added in the package
	 * @param int $p_id Package ID
	 * @return array of objects
	 */
	public function get_available_department_except($p_id)
	{
		$sql = "
			SELECT
			id, department, description, visible
			FROM $this->_table d
			WHERE id NOT IN (SELECT department_id from package_departments WHERE package_id = ?)
			AND department <> 'hrd'
			ORDER BY department
		";
		return $this->query($sql, array($p_id));
	}

}