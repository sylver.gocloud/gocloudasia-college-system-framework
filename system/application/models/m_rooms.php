<?php

class M_rooms extends MY_Model{

	  	protected $_table = "rooms";

		public function __construct()
		{
			parent::__construct();
			$this->load->database();
		}

#CLONE ROOMS OF PREVIOUS ACADEMIC YEAR
	public function cloned_rooms_from_previous_sy($current_academic_year_id =  false, $prev_academic_year_id = false){
		
		#check if there are already rooms for the current AY - if yes do not continue
		unset($get);
		$get['where']['academic_year_id'] = $current_academic_year_id;
		$rm_chk = $this->get_record(false, $get);
		if($rm_chk){ return false; }


		#get the current academic year
			unset($get);
			$get['where']['id'] = $current_academic_year_id;
			$get['single'] = true;
			$c_ay = $this->get_record('academic_years', $get);
			if($c_ay === false){ return;}

		#get previous academic year
			if($prev_academic_year_id){
				unset($get);
				$get['where']['id'] = $prev_academic_year_id;
				$get['single'] = true;
				$p_ay = $this->get_record('academic_years', $get);
			}else{
				unset($get);
				$get['where']['year_from < '] = $c_ay->year_from;
				$get['single'] = true;
				$p_ay = $this->get_record('academic_years', $get);
			}
			if($p_ay === false){ return;}

		#get rooms in the previous AY
			unset($get);
			$get['where']['academic_year_id'] = $p_ay->id;
			$get['not_in'] = array(
					'field' => 'name',
					'data' => "SELECT name FROM rooms WHERE academic_year_id = '$c_ay->id' "
				);
			$rooms = $this->get_record('rooms', $get);
			if($rooms === false){ return; }
			foreach ($rooms as $key => $value) {
				unset($data);
				$data['academic_year_id'] = $c_ay->id;
				$data['name'] = $value->name;
				$data['description'] = $value->description;
				$rs = $this->insert($data);

			}
	}
}

?>
