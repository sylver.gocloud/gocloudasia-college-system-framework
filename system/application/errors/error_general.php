<!DOCTYPE html>
<html lang="en">
<head>
<title>Error</title>
<?
	$config = get_config();
	$base_url = $config['base_url'];
?>
<link href="<?php echo $base_url.'assets/css/bootstrap.min.css'; ?>" rel="stylesheet" type="text/css" media="screen" />
<link href="<?php echo $base_url.'assets/css/pace.css'; ?>" rel="stylesheet" type="text/css" />
</head>
<body>
	<div id="container" class='container'>
		<div class="row">
			<div class="col-md-12">
				<div class="well">
					<b><?php echo '<i class="glyphicon glyphicon-exclamation-sign" style="color:red" ></i>&nbsp; '.$heading; ?></b>
				</div>
				<div class="alert alert-danger">
					<?php echo $message; ?>
				</div>
				<div class="wellx">
					<button class='btn btn-danger' onclick="window.history.back()">
						BACK TO PREVIOUS PAGE
					</button>
				</div>
			</div>
		</div>
	</div>
	<hr>
	<div id="footer" class="shadow">
		<div class="container">
			<p class="text-muted credit">Copy &copy; GoCloudAsia 2014</p>
		</div>
	</div>

</body>
</html>