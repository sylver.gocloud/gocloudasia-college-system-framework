<?php

class _Student
{
	/* VARIABLES */
		private $ci;
		
		public $studid;
		public $fullname;
		public $enrollment_id;
		public $profile;
		public $has_nstp;
		public $has_payment;
		public $has_old_account;
		public $total_lab;
		public $total_lec;
		public $total_units;
		public $is_full_paid;

		public $student_subjects;
		public $student_subject_grades;
		public $dropped_subjects;
		public $coursefinance_id;

		public $student_fees;
		public $student_plan_modes;
		public $student_other_fees;
		public $student_previous_account;
		public $student_payment_totals;

		public $student_deductions;
		public $student_scholarships;
		public $student_excess_payments;

		public $current_semester;
		public $current_grading;
		public $current_sy;

		public $userlogin;
		public $userid;
		public $valid;

		#Special Variables
		private $total_excess_in_deduction; #if scholarship/deduction is greater than the tuition plan due - excess will be applied to otherfees/old accounts
	/* END OF VARIABLES */	
	
	/* CONTRUCT */
		public function __construct($config)
		{
			set_time_limit(0);
			$this->enrollment_id = $config['enrollment_id'];
			$this->ci =& get_instance();
			$this->ci->load->model('M_core_model','m');

			$this->load_default_function();
		}
	/* END OF CONTRUCT */

	/* DEFAULT FUNCTIONS && PRIVATE FUNCTIONS */
		public function load_default_function()
		{
			$this->load_default();
			$this->load_profile();
			$this->load_subjects();
			$this->load_fees();
			$this->load_student_plan_modes();
			$this->load_student_other_fees();
			$this->load_student_previous_account();
			$this->check_if_has_payment();	
		}

		public function load_default()
		{
			$this->studid = false;
			$this->fullname = false;
			$this->profile = false;
			$this->has_nstp = false;
			$this->has_payment = false;
			$this->total_lab = 0;
			$this->total_lec = 0;
			$this->total_units = 0;
			$this->is_full_paid = false;

			$this->student_subjects = false;
			$this->dropped_subjects = false;
			$this->student_fees = false ;

			$this->coursefinance_id = false;

			$this->student_plan_modes = false;
			$this->student_other_fees = false;
			$this->student_previous_account = false;

			$this->student_deductions = false;
			$this->student_scholarships = false;
			$this->student_excess_payments = false;

			$this->userlogin = $this->ci->session->userdata['userlogin'];
			$this->userid = $this->ci->userid;

			//STUDENT PAYMENT TOTALS
			$student_payment_totals['total_plan_due'] = 0;
			$student_payment_totals['total_deduction'] = 0;
			$student_payment_totals['total_scholarship'] = 0;
			$student_payment_totals['total_excess_amount'] = 0;
			$student_payment_totals['total_deduction_amount'] = 0;

			$student_payment_totals['total_previous_amount'] = 0;
			$student_payment_totals['total_amount_due'] = 0;
			$student_payment_totals['total_amount_paid'] = 0;
			$student_payment_totals['total_balance'] = 0;
			$student_payment_totals['payment_status'] = 0;
			$this->student_payment_totals = (object)$student_payment_totals; //CONVERT ARRAY TO OBJECT

			$this->current_semester = false;
			$this->current_grading = false;
			$this->current_sy = false;

			$this->valid = false;
		}
		
		public function load_profile($id = false)
		{
			unset($get);
			$get['fields'] = array(
					'enrollments.*',
					"DATE_FORMAT(enrollments.date_of_birth,'%Y-%m-%d') as date_of_birth",
					'enrollments.guardian as guardian_name',
					'enrollments.fake_email as email',
					'enrollments.fname as first_name',
					'enrollments.name as full_name',
					'enrollments.middle as middle_name',
					'enrollments.lastname as last_name',
					'CONCAT(enrollments.lastname,", ",enrollments.fname," ",enrollments.middle) as fullname',
					'enrollments.sex as gender',
					'enrollments.id as enrollment_id',
					'courses.course',
					'courses.course_code',
					'years.year', 
					'years.id as year_id',
					'semesters.id as sem_id',
					'semesters.name',
					'users.login',
					'users.is_activated',
					'payment_plan.name as payment_plan',
					'payment_plan.division as payment_division',
					'course_specialization.specialization'
			);
			$get['where']['enrollments.id'] = !$id ? $this->enrollment_id : $id;
			$get['join'] = array(
					array(
					"table" => "courses",
					"on"	=> "courses.id = enrollments.course_id",
					"type"  => "LEFT"
					),
					array(
					"table" => "years",
					"on"	=> "years.id = enrollments.year_id",
					"type"  => "LEFT"
					),
					array(
					"table" => "semesters",
					"on"	=> "semesters.id = enrollments.semester_id",
					"type"  => "LEFT"
					),
					array(
					"table" => "users",
					"on"	=> "users.id = enrollments.user_id",
					"type"  => "LEFT"
					),
					array(
					"table" => "payment_plan",
					"on"	=> "payment_plan.id = enrollments.payment_plan_id",
					"type"  => "LEFT"
					),
					array(
					"table" => "course_specialization",
					"on"	=> "course_specialization.id = enrollments.course_specialization",
					"type"  => "LEFT"
					)
				);
			$get['single'] = true;
			$profile = $this->ci->m->get_record('enrollments',$get);
		
			if($profile){
				$this->studid = $profile->studid;
				$this->fullname = $profile->full_name;
				$this->profile = $profile;

				$this->coursefinance_id = $profile->coursefinance_id;

				$this->total_units = $profile->total_units;
				$this->total_lab = $profile->total_lab_units;
				$this->total_lec = $profile->total_lec_units;

				//LOAD Payments total as object
				$this->student_payment_totals->total_plan_due = $profile->total_plan_due; //Total PLan Amount
				$this->student_payment_totals->total_deduction = $profile->total_deduction;
				$this->student_payment_totals->total_scholarship = $profile->total_scholarship;
				$this->student_payment_totals->total_excess_amount = $profile->total_excess_amount;
				$this->student_payment_totals->total_deduction_amount = $profile->total_deduction + $profile->total_scholarship + $profile->total_excess_amount;

				$this->student_payment_totals->total_previous_amount = $profile->total_previous_amount;
				$this->student_payment_totals->total_amount_due = $profile->total_amount_due; // Total Plan - All deductions
				$this->student_payment_totals->total_amount_paid = $profile->total_amount_paid; // Total Amoun Paid
				$this->student_payment_totals->total_balance = $profile->total_balance;
				$this->student_payment_totals->payment_status = $profile->payment_status;

				$this->is_full_paid = $profile->total_balance <= 0 ? true : false;
				$this->valid = true;
			}
		}

		private function load_subjects_new()
		{
			unset($get);
			$get['fields'] = array(
				'studentsubjects.id',
				'subjects.code', 
				'subjects.subject', 
				'subjects.units', 
				'subjects.lab', 
				'subjects.lec', 
				'subjects.time', 
				'subjects.day', 
				'subjects.lab_fee_id', 
				'studentsubjects.enrollmentid', 
				'subjects.lab_kind', 
				'subjects.subject_load', 
				'subjects.subject_taken', 
				'subjects.id as subjectid', 
				'subjects.id as subject_id', 
				'CONCAT(rooms.name,"-",rooms.description) as room',
				'subjects.lec',
				'subjects.is_nstp',
				'subjects.nstp_fee_id',
				'studentsubjects.remarks',
				'studentsubjects.final_grade',
				'studentsubjects.is_drop',
				'block_system_settings.name as block_name'
			);
			$get['where']['studentsubjects.enrollment_id'] = $this->enrollment_id;
			$get['where']['studentsubjects.is_deleted'] = 0;
			$get['join'] = array(
					array(
					"table" => "subjects",
					"on"	=> "subjects.id = studentsubjects.subject_id",
					"type"  => "LEFT"
					),
					array(
					"table" => "block_system_settings",
					"on"	=> "block_system_settings.id = studentsubjects.block_system_setting_id",
					"type"  => "LEFT"
					),
					array(
					"table" => "rooms",
					"on"	=> "rooms.id = subjects.room_id",
					"type"  => "LEFT"
					)
				);
			$get['order'] = 'day, time';
			$this->student_subjects = $this->ci->m->get_record("studentsubjects",$get);
			if($this->student_subjects){
				#check if has nstp
				foreach ($this->student_subjects as $key => $value) {
					if($value->is_nstp == 1){
						$this->has_nstp = true;
						break;
					}
				}

				#load dropped_subjects
				$drop_sub = false;
				foreach ($this->student_subjects as $key => $value) {
					if($value->is_drop == 1){
						$drop_sub[] = $value;
					}
				}
				$this->dropped_subjects = $drop_sub;
			}
		}

		/**
		 * Load Student Subjects
		 * @param int $user_id - if you want to return its subject under the teacher Used ID
		 */
		public function load_subjects($user_id = false)
		{
			unset($get);
			$get['fields'] = array(
				'studentsubjects.id',
				'master_subjects.code', 
				'master_subjects.subject', 
				'master_subjects.units', 
				'master_subjects.lab', 
				'master_subjects.lec', 
				'subjects.time', 
				'subjects.day', 
				'subjects.lab_fee_id', 
				'studentsubjects.enrollmentid', 
				'subjects.lab_kind', 
				'subjects.subject_load', 
				'subjects.subject_taken', 
				'subjects.id as subjectid', 
				'subjects.id as subject_id', 
				'CONCAT(rooms.name,"-",rooms.description) as room',
				'users.name as instructor',
				'subjects.is_nstp',
				'subjects.nstp_fee_id',
				'studentsubjects.raw',
				'studentsubjects.value',
				'studentsubjects.converted',
				'studentsubjects.remarks',
				'studentsubjects.is_drop',
				'block_system_settings.name as block_name'
			);
			$get['where']['studentsubjects.enrollment_id'] = $this->enrollment_id;
			$get['where']['studentsubjects.is_deleted'] = 0;
			$get['where']['studentsubjects.is_drop'] = 0;

			if($user_id){
				$get['where']['subjects.teacher_user_id'] = $user_id;
			}

			$get['join'] = array(
					array(
					"table" => "subjects",
					"on"	=> "subjects.id = studentsubjects.subject_id",
					"type"  => "LEFT"
					),array(
					"table" => "master_subjects",
					"on"	=> "master_subjects.ref_id = subjects.ref_id",
					"type"  => "RIGHT"
					),
					array(
					"table" => "users",
					"on"	=> "users.id = subjects.teacher_user_id",
					"type"  => "LEFT"
					),
					array(
					"table" => "block_system_settings",
					"on"	=> "block_system_settings.id = studentsubjects.block_system_setting_id",
					"type"  => "LEFT"
					),
					array(
					"table" => "rooms",
					"on"	=> "rooms.id = subjects.room_id",
					"type"  => "LEFT"
					)
				);
			$get['order'] = 'day, time';
			$this->student_subjects = $this->ci->m->get_record("studentsubjects",$get);

			if($this->student_subjects){
				#check if has nstp
				foreach ($this->student_subjects as $key => $value) {
					if($value->is_nstp == 1){
						$this->has_nstp = true;
						break;
					}
				}
			}

			$get['where']['studentsubjects.is_drop'] = 1;
			$this->dropped_subjects = $this->ci->m->get_record("studentsubjects",$get);
		}

		private function load_fees()
		{
			unset($get);
			$get['fields'] = array(
					'sef_id as id',
					'sef_fee_name as name',
					'sef_fee_rate as rate',
					'fee_id',
					'is_tuition_fee',
					'is_misc_fee',
					'is_other_school',
					'is_other_fee',
					'is_lab_fee',
					'is_nstp_fee',
					'amount_paid',
					'is_paid',
					'spr_id',
					'is_check',
					'check_applied',
				);
			$get['where']['sef_enrollment_id'] = $this->enrollment_id;
			$get['where']['is_deleted'] = 0;
			$this->student_fees = $this->ci->m->get_record("student_enrollment_fees",$get);
		}

		private function load_student_plan_modes()
		{
			unset($get);
			$get['fields'] = array(
					'id',
					'enrollment_id',
					'payment_plan_id',
					'number',
					'name',
					'value',
					'amount_paid',
					'check_applied',
					'is_check',
					'is_paid',
					'spr_id'
				);
			$get['where']['enrollment_id'] = $this->enrollment_id;
			$get['where']['is_deleted'] = 0;
			$get['order'] = 'number';
			$this->student_plan_modes =  $this->ci->m->get_record("student_plan_modes",$get);
			return $this->student_plan_modes;
		}

		private function load_student_other_fees()
		{
			unset($get);
			$get['where']['sef_enrollment_id'] = $this->enrollment_id;
			$get['where']['is_other_fee'] = 1;
			$get['where']['is_deleted'] = 0;
			$this->student_other_fees = $this->ci->m->get_record("student_enrollment_fees",$get);
			return $this->student_other_fees;
		}

		private function load_student_previous_account()
		{
			unset($get);
			$get['fields'] = array(
					'student_previous_accounts.id',
					'student_previous_accounts.value',
					'student_previous_accounts.amount_paid',
					'student_previous_accounts.is_paid',
					'student_previous_accounts.spr_id',
					'student_previous_accounts.is_check',
					'student_previous_accounts.check_applied',
					'student_previous_accounts.enrollment_id',
					'student_previous_accounts.prev_enrollment_eid',
					'enrollments.sy_from',
					'enrollments.sy_to',
					'years.year',
				);
			$get['where']['student_previous_accounts.enrollment_id'] = $this->enrollment_id;
			$get['where']['student_previous_accounts.is_deleted'] = 0;
			$get['join'][] = array(
					'table' => 'enrollments',
					'on' => 'enrollments.id = student_previous_accounts.prev_enrollment_eid',
					'type' => 'left'
				);
			$get['join'][] = array(
					'table' => 'years',
					'on' => 'years.id = enrollments.year_id',
					'type' => 'left'
				);
			$this->student_previous_account = $this->ci->m->get_record("student_previous_accounts",$get);
			$this->has_old_account = $this->student_previous_account ? true : false;
			return $this->student_previous_account;
		}

		private function load_return_default()
		{
			$ret['status'] = false;
			$ret['msg'] = "Process Failed";
			return (object)$ret;
		}

		private function check_if_has_payment()
		{
			unset($get);
			$this->ci->m->set_table('student_payment_records','spr_id');
			$get['where']['spr_enrollment_id'] = $this->enrollment_id;
			$get['where']['spr_is_deleted'] = 0;
			$get['where']['is_applied'] = 1;
			$get['single'] = true;
			$q = $this->ci->m->get_record('student_payment_records',$get);
			$this->has_payment = $q ? true : false;
			return $this->has_payment;
		}
	/* END OF DEFAULT FUNCTIONS */

	/*FUNCTION TO GET CURRENT SEMESTER"|GRADING|SY*/
		public function load_current_school_profile()
		{
			$this->current_semester = $this->get_current_semester();
			$this->current_grading = $this->get_current_grading();
			$this->current_sy = $this->get_current_sy();
		}

		public function get_current_semester()
		{	
			$ret = array(
					'id' => '0',
					'academic_year_id' => '0',
					'semester_id' => '0',
					'semester' => ''
				);

			$cos = $this->ci->cos->user;
			if($cos){
				$ret = array(
					'id' => $cos->open_semester_id,
					'academic_year_id' => $cos->academic_year_id,
					'semester_id' => $cos->semester_id,
					'semester' => $cos->semester
				);
			}
			
			return (object)$ret;
		}

		public function get_current_grading()
		{
			unset($get);
			$get['fields'] = array(
					'id',
					'grading_period',
					'orders'
				);
			$get['where']['is_set'] = 1;
			$get['where']['is_deleted'] = 0;
			$get['single'] = true;
			return $this->ci->m->get_record('grading_periods',$get);
		}

		public function get_current_sy()
		{
			$open_semester = $this->get_current_semester();
			
			if($open_semester){
				unset($get);
				$get['fields'] = array(
						'id',
						'year_from',
						'year_to'
					);
				$get['where']['id'] = $open_semester->academic_year_id;
				$get['single'] = true;
				return $this->ci->m->get_record('academic_years',$get);
			}else{
				return false;
			}
		}
	/*END OF FUNCTION GET CURRENT SEMESTER"|GRADING|SY*/

	
	public function get_student_course_finance()
	{
		unset($get);
		$get['fields'] = array(
				'id',
				'category',
				'code'
			);
		$get['where']['id'] = $this->coursefinance_id;
		$get['single'] = true;
		return $this->ci->m->get_record('coursefinances',$get);
	}

	//GET STUDENT FEES READY FOR VIEWING
	public function get_student_fee_profile()
	{
		unset($ret);
		$ret['tuition_fee'] = false;
		$ret['lab_fee'] = false;
		$ret['misc_fee'] = false;
		$ret['other_school_fee'] = false;
		$ret['other_fee'] = false;
		$ret['nstp_fee'] = false;
		$ret['old_accounts'] = false;

		$ret['deductions'] = false;
		$ret['scholarships'] = false;
		$ret['excess'] = false;

		$ret['totals'] = array(
				'tuition_fee' => 0,
				'lab_fee' => 0,
				'misc_fee' => 0,
				'other_school_fee' => 0,
				'other_fee' => 0,
				'nstp_fee' => 0,
				'old_accounts' => 0,
				'deductions' => 0,
				'scholarships' => 0,
				'excess' => 0
			);

		//GET LAB FEES ACCORDING TO SUBJECTS LAB UNITS
		$lab_fee = false;
		if($this->student_subjects){
			foreach ($this->student_subjects as $key => $subject) {
				if($subject->lab && $subject->lab > 0 && $subject->lab_fee_id)//VALIDATE LAB UNIT && LAB FEE ID
				{
					unset($get);
					$get['fields'] = array('coursefees.id','coursefees.value','fees.name');
					$get['where']['coursefees.coursefinance_id'] = $this->coursefinance_id;
					$get['where']['coursefees.fee_id'] = $subject->lab_fee_id;
					$get['join'][] = array(
							'table' => 'fees',
							'on' => 'fees.id = coursefees.fee_id',
							'type' => 'LEFT'
						);
					$get['single'] = true;

					$lab_f = $this->ci->m->get_record('coursefees',$get);
					if($lab_f){ //GET ALL LAB FEES FROM COURSE FINANCE
						$lab_fee[$subject->lab_fee_id]['data'] = $lab_f;
						$lab_fee[$subject->lab_fee_id]['value'] = $lab_f->value;
						$lab_fee[$subject->lab_fee_id]['unit'] = floatval((isset($lab_fee[$subject->lab_fee_id]['unit']) ? $lab_fee[$subject->lab_fee_id]['unit'] : 0)) + floatval($subject->lab);
						// vp($subject->lab);
					}
				}
			}
		}
		
		if($this->student_fees){
			foreach ($this->student_fees as $key => $value) {
				if($value->is_tuition_fee == 1){
					$ret['tuition_fee']['id'] = $value->id;
					$ret['tuition_fee']['name'] = $value->name;
					$ret['tuition_fee']['amount'] = $this->profile->tuition_fee; // TUITION FEE RATE * TOTAL UNITS
					$ret['tuition_fee']['rate'] = $value->rate;
					$ret['tuition_fee']['units'] = $this->total_units;

					$ret['totals']['tuition_fee'] += $this->total_units * $value->rate;
				}
				if($value->is_lab_fee == 1){
					//LAB FEE DEPENDS ON HOW MANY LAB SUBJECT(WITH FEE) ENROLLED
					if(isset($lab_fee[$value->fee_id]['data'])){
						$ret['lab_fee'][$value->fee_id]['name'] = $value->name;
						$ret['lab_fee'][$value->fee_id]['amount'] = $amt =  floatval($value->rate) * floatval($lab_fee[$value->fee_id]['unit']);
						$ret['lab_fee'][$value->fee_id]['rate'] = $value->rate;
						$ret['lab_fee'][$value->fee_id]['units'] = $lab_fee[$value->fee_id]['unit'];

						$ret['totals']['lab_fee'] += $amt;
					}
				}
				if($value->is_misc_fee == 1){
					$ret['misc_fee'][$key]['id'] = $value->id;
					$ret['misc_fee'][$key]['name'] = $value->name;
					$ret['misc_fee'][$key]['amount'] = $value->rate;
					$ret['misc_fee'][$key]['rate'] = $value->rate;
					$ret['misc_fee']['total'] = $this->profile->misc_fee;

					$ret['totals']['misc_fee'] += $value->rate;
				}
				if($value->is_other_school == 1){
					$ret['other_school_fee'][$key]['id'] = $value->id;
					$ret['other_school_fee'][$key]['name'] = $value->name;
					$ret['other_school_fee'][$key]['amount'] = $value->rate;
					$ret['other_school_fee'][$key]['rate'] = $value->rate;
					$ret['other_school_fee']['total'] = $this->profile->other_school_fee;

					$ret['totals']['other_school_fee'] += $value->rate;
				}
				if($value->is_other_fee == 1){
					$ret['other_fee'][$key]['id'] = $value->id;
					$ret['other_fee'][$key]['name'] = $value->name;
					$ret['other_fee'][$key]['amount'] = $value->rate;
					$ret['other_fee'][$key]['rate'] = $value->rate;
					$ret['other_fee'][$key]['is_paid'] = $value->is_paid;
					$ret['other_fee'][$key]['amount_paid'] = $value->amount_paid;
					$ret['other_fee']['total'] = $this->profile->other_fee;

					$ret['totals']['other_fee'] += $value->rate;
				}
				if($value->is_nstp_fee == 1 && $this->has_nstp){
					$ret['nstp_fee']['id'] = $value->id;
					$ret['nstp_fee']['name'] = $value->name;
					$ret['nstp_fee']['amount'] = $value->rate;
					$ret['nstp_fee']['rate'] = $value->rate;
					$ret['nstp_fee']['total'] = $this->profile->nstp_fee;

					$ret['totals']['nstp_fee'] += $value->rate;
				}
			}
			// vd($ret['lab_fee']);
		}
		
		//GET STUDENT DEDUCTION RECORD
		$ret['deductions'] = $this->get_student_deduction_record();
		$ret['scholarships'] = $this->get_student_scholarship_record();
		$ret['excess_payments'] = $this->get_student_excess_payment_record();
		$ret['old_accounts'] = $this->student_previous_account;
		$ret['payment_record'] = $this->get_payment_record();
		
		//Get Deduction Totals
		if($ret['deductions']){
			foreach ($ret['deductions'] as $k => $v) {
				$ret['totals']['deductions'] += $v->amount;
			}
		}

		//Get Scholarship Totals
		if($ret['scholarships']){
			foreach ($ret['scholarships'] as $k => $v) {
				$ret['totals']['scholarships'] += $v->scho_amount;
			}
		}

		//Get Excess Totals
		if($ret['excess_payments']){
			foreach ($ret['excess_payments'] as $k => $v) {
				$ret['totals']['excess'] += $v->amount;
			}
		}

		//Get Old Account Totals
		if($ret['old_accounts']){
			foreach ($ret['old_accounts'] as $k => $v) {
				$ret['totals']['old_accounts'] += $v->value;
			}
		}

		return (object)$ret;
	}

	//GET STUDENT FEES READY FOR VIEWING BY ENROLLMENT ID
	public function get_student_fee_profile_by_eid($id)
	{
		unset($ret);
		$ret['tuition_fee'] = false;
		$ret['lab_fee'] = false;
		$ret['misc_fee'] = false;
		$ret['other_school_fee'] = false;
		$ret['other_fee'] = false;
		$ret['nstp_fee'] = false;
		$ret['old_accounts'] = false;

		$ret['deductions'] = false;
		$ret['scholarships'] = false;
		$ret['excess'] = false;

		//GET LAB FEES ACCORDING TO SUBJECTS LAB UNITS
		$lab_fee = false;
		if($this->student_subjects){
			foreach ($this->student_subjects as $key => $subject) {
				if($subject->lab && $subject->lab > 0 && $subject->lab_fee_id)//VALIDATE LAB UNIT && LAB FEE ID
				{
					unset($get);
					$get['fields'] = array('coursefees.id','coursefees.value','fees.name');
					$get['where']['coursefees.coursefinance_id'] = $this->coursefinance_id;
					$get['where']['coursefees.fee_id'] = $subject->lab_fee_id;
					$get['join'][] = array(
							'table' => 'fees',
							'on' => 'fees.id = coursefees.fee_id',
							'type' => 'LEFT'
						);
					$get['single'] = true;

					$lab_f = $this->ci->m->get_record('coursefees',$get);
					if($lab_f){ //GET ALL LAB FEES
						$lab_fee[$subject->lab_fee_id]['data'] = $lab_f;
						$lab_fee[$subject->lab_fee_id]['value'] = $lab_f->value;
						$lab_fee[$subject->lab_fee_id]['unit'] = floatval((isset($lab_fee[$subject->lab_fee_id]['unit']) ? $lab_fee[$subject->lab_fee_id]['unit'] : 0)) + floatval($subject->lab);
						// vp($subject->lab);
					}
				}
			}
		}
		
		if($this->student_fees){
			foreach ($this->student_fees as $key => $value) {
				if($value->is_tuition_fee == 1){
					$ret['tuition_fee']['id'] = $value->id;
					$ret['tuition_fee']['name'] = $value->name;
					$ret['tuition_fee']['amount'] = $this->profile->tuition_fee; // TUITION FEE RATE * TOTAL UNITS
					$ret['tuition_fee']['rate'] = $value->rate;
					$ret['tuition_fee']['units'] = $this->total_units;
				}
				if($value->is_lab_fee == 1){
					//LAB FEE DEPENDS ON HOW MANY LAB SUBJECT(WITH FEE) ENROLLED
					if(isset($lab_fee[$value->fee_id]['data'])){
						$ret['lab_fee'][$value->fee_id]['name'] = $value->name;
						$ret['lab_fee'][$value->fee_id]['amount'] =  floatval($value->rate) * floatval($lab_fee[$value->fee_id]['unit']);
						$ret['lab_fee'][$value->fee_id]['rate'] = $value->rate;
						$ret['lab_fee'][$value->fee_id]['units'] = $lab_fee[$value->fee_id]['unit'];
					}
				}
				if($value->is_misc_fee == 1){
					$ret['misc_fee'][$key]['id'] = $value->id;
					$ret['misc_fee'][$key]['name'] = $value->name;
					$ret['misc_fee'][$key]['amount'] = $value->rate;
					$ret['misc_fee'][$key]['rate'] = $value->rate;
					$ret['misc_fee']['total'] = $this->profile->misc_fee;
				}
				if($value->is_other_school == 1){
					$ret['other_school_fee'][$key]['id'] = $value->id;
					$ret['other_school_fee'][$key]['name'] = $value->name;
					$ret['other_school_fee'][$key]['amount'] = $value->rate;
					$ret['other_school_fee'][$key]['rate'] = $value->rate;
					$ret['other_school_fee']['total'] = $this->profile->other_school_fee;
				}
				if($value->is_other_fee == 1){
					$ret['other_fee'][$key]['id'] = $value->id;
					$ret['other_fee'][$key]['name'] = $value->name;
					$ret['other_fee'][$key]['amount'] = $value->rate;
					$ret['other_fee'][$key]['rate'] = $value->rate;
					$ret['other_fee'][$key]['is_paid'] = $value->is_paid;
					$ret['other_fee'][$key]['amount_paid'] = $value->amount_paid;
					$ret['other_fee']['total'] = $this->profile->other_fee;
				}
				if($value->is_nstp_fee == 1 && $this->has_nstp){
					$ret['nstp_fee']['id'] = $value->id;
					$ret['nstp_fee']['name'] = $value->name;
					$ret['nstp_fee']['amount'] = $value->rate;
					$ret['nstp_fee']['rate'] = $value->rate;
					$ret['nstp_fee']['total'] = $this->profile->nstp_fee;
				}
			}
		}
		
		//GET STUDENT DEDUCTION RECORD
		$ret['deductions'] = $this->get_student_deduction_record();
		$ret['scholarships'] = $this->get_student_scholarship_record();
		$ret['excess_payments'] = $this->get_student_excess_payment_record();
		$ret['old_accounts'] = $this->student_previous_account;
		$ret['payment_record'] = $this->get_payment_record();
		// vd('x');
		return (object)$ret;
	}

	//GET STUDENT DEDUCTION RECORD
	public function get_student_deduction_record($id = false)
	{
		unset($get);
		$get['where']['enrollment_id'] = !$id ? $this->enrollment_id : $id;
		$this->student_deductions = $this->ci->m->get_record('student_deductions_record', $get); 
		return $this->student_deductions;
	}

	//GET STUDENT SCHOLARSHIP RECORD
	public function get_student_scholarship_record($id = false)
	{
		unset($get);
		$get['where']['enrollment_id'] = !$id ? $this->enrollment_id : $id;
		$this->student_scholarships =  $this->ci->m->get_record('student_scholarship_record', $get);
		return $this->student_scholarships;
	}

	/* STUDENT DEDUCTION/SCHOLARSHIP/PREVIOIUS ACCOUNT FUNCTIONS START HERE*/

		//ADD STUDENT DEDUCTION FEE
			public function add_deduction_fee($data = array())
			{
				$log = $data;
				$data['academic_year_id'] = $this->ci->open_semester->academic_year_id;
				$data['user_trans_id'] = $this->userid;
				$this->ci->m->set_table('student_deductions_record');
				$x_add = (object)$this->ci->m->insert($data);
				if($x_add->status)
				{
					$this->recompute_fees('Add Deduction Fees');
					$this->reserve_all_student_subject();
					// $this->deduct_balance_total('DEDUCTION', $x_add->id, "REMOVE");

					activity_log('Add Deduction Fee',$this->userlogin,'Data : '.arr_str($log));
					
				}

				return $x_add;
			}

		//REMOVE STUDENT DEDUCTION RECORD
			public function delete_deduction_record($sdr_id = false)
			{
				$this->ci->m->set_table('student_deductions_record');
				$x_sdr = $this->ci->m->pull($sdr_id,array('id','deduction_name','amount'));
				if($x_sdr)
				{
					$log['enrollment_id'] = $this->enrollment_id;
					$log['deduction_name'] = $x_sdr->deduction_name;
					$log['amount'] = $x_sdr->amount;
					$log['student deduction record id'] = $sdr_id;

					// $rs = $this->recompute_fees('Delete Deduction Fees');
					// $this->deduct_balance_total('DEDUCTION', $sdr_id, "ADD");
					// vd($rs);

					$this->ci->m->set_table('student_deductions_record');
					$x_del = $this->ci->m->delete($sdr_id);
					if($x_del)
					{
						$this->update_payment_status(); #update payment_status
						activity_log('Delete Student Deduction',$this->userlogin,'Data : '.arr_str($log));
						return true;
					}
				}

				return false;
			}

		//ADD STUDENT SCHOLARSHIP
			public function add_student_scholarship($data = array())
			{
				$this->ci->m->set_table('student_scholarship_record');
				$x_add = (object)$this->ci->m->insert($data);
				if($x_add->status)
				{
					$this->recompute_fees('Add Scholarship');
					$this->reserve_all_student_subject();
					// $this->deduct_balance_total('SCHOLARSHIP', $x_add->id, "REMOVE");

					$log['enrollment_id'] = $data['enrollment_id'];
					$log['scholarship_id'] = $data['scholarship_id'];
					$log['scholarship_name'] = $data['scholarship_name'];
					$log['scholarship_amount'] = $data['scho_amount'];

					activity_log('Add Student Scholarship',$this->userlogin,'Data : '.arr_str($log));
					
				}

				return $x_add;
			}

		//REMOVE STUDENT SCHOLARSHIP
			public function delete_scholarship_record($ssr_id = false)
			{
				$this->ci->m->set_table('student_scholarship_record');			
				$x_ssr = $this->ci->m->pull($ssr_id,array('id','scholarship_name','scho_amount'));
				if($x_ssr)
				{
					$log['scholarship_name'] = $x_ssr->scholarship_name;
					$log['amount'] = $x_ssr->scho_amount;
					$log['student scholarship record id'] = $ssr_id;
					
					// $this->deduct_balance_total('SCHOLARSHIP', $ssr_id, "ADD"); #update total balance
					
					$this->ci->m->set_table('student_scholarship_record');
					$x_del = $this->ci->m->delete($ssr_id);
					if($x_del)
					{
						$this->update_payment_status(); #update payment_status
						activity_log('Delete Student Scholarship',$this->userlogin,'Data : '.arr_str($log));
						return true;
					}
				}

				return false;
			}

		// ADD STUDENT EXCESS PAYMENT FROM PREVIOUS ENROLLMENTS
			public function add_excess_payments($id=false)
			{
				$ret = $this->load_return_default();

				if($id === false){
					return $ret;
				}

				unset($get);
				$get['enrollments.id'] = $id;
				$get['enrollments.total_balance < '] = 0;
				$get['enrollments.is_deleted'] = 0;
				$get['enrollments.is_drop'] = 0;
				$get['enrollments.is_activated'] = 1;
				$this->ci->m->set_table('enrollments');
				$excess = $this->ci->m->pull($get);
				if($excess === false){
					$ret->msg = "Process Failed. Enrollment is invalid";
					return $ret;
				}

				unset($data);
				$data['enrollment_id'] = $log['Enrollmend ID'] = $this->enrollment_id;
				$data['prev_sem_eid'] = $log['Enrollmend ID of Excess Payments'] = $id;
				$data['date'] = $log['Date'] = NOW;
				$data['amount'] = $log['Amount to Apply'] = ($excess->total_balance*-1);
				$this->ci->m->set_table('studentexcess_prev_sem');
				
				$rs = (object)$this->ci->m->insert($data);
				
				if($rs->status === false){
					$ret->msg = "Process Failed. There was an error in saving. Please try again.";
					return $ret;
				}

				activity_log('Add Student excess Payment',$this->userlogin,'Data : '.arr_str($data));

				$this->recompute_fees('Add Excess/Payment or Advance Payments');
				$this->reserve_all_student_subject();
				// $this->deduct_balance_total('EXCESS', $rs->id, "REMOVE");

				$ret->status = true;
				$ret->msg = "Excess Payment was successfully applied as payment to current account";
				return $ret;
			}

		//REMOVE STUDENT EXCESS PAYMENT FROM 
			public function delete_excess_payments($id = false)
			{
				$this->ci->m->set_table('studentexcess_prev_sem');			
				$rs = $this->ci->m->get($id);
				
				if($rs)
				{
					$this->ci->m->set_table('enrollments');
					$prev_enroll = $this->ci->m->get($rs->prev_sem_eid);

					$log['Enrollment Id'] = $this->enrollment_id;
					$log['Excess Enrollment Id'] = $id;
					$log['amount'] = $rs->amount;
					$log['S.Y.'] = $prev_enroll->sy_from.'-'.$prev_enroll->sy_to;
					
					// $this->deduct_balance_total('EXCESS', $id, "ADD"); #update total balance
					
					$this->ci->m->set_table('studentexcess_prev_sem');
					unset($data);
					$data['is_deleted'] = 1;
					$data['deleted_by'] = $this->userid;
					$data['deleted_date'] = NOW;

					$x_del = $this->ci->m->update($id, $data);
					if($x_del)
					{
						$this->update_payment_status(); #update payment_status
						activity_log('Delete Student Excess Payments',$this->userlogin,'Data : '.arr_str($log));
						return true;
					}
				}

				return false;
			}

		/* DEDUCT (DEDUCTION/SCHOLARSHIP/EXCESS) TO STUDENT PAYMENT TOTAL
			operator [REMOVE] - when user add an excess amount
			operator [ADD] - when user delete an excess record
		*/
			private function deduct_balance_total($type, $id, $operator) {

				$type = strtoupper($type);
				$record_data = false;
				$amount = 0;
				switch (strtoupper($type)) {
					case 'DEDUCTION':
						unset($get);
						$get['where']['id'] = $id;
						$get['single'] = true;
						$rs = $record_data = $this->ci->m->get_record('student_deductions_record', $get);
						if($rs){
							$amount = $rs->amount;	
						}

						break;
					case 'SCHOLARSHIP':
						unset($get);
						$get['where']['id'] = $id;
						$get['single'] = true;
						$rs = $record_data = $this->ci->m->get_record('student_scholarship_record', $get);
						if($rs){
							$amount = $rs->scho_amount;
						}

						break;
					case 'EXCESS':
						unset($get);
						$get['where']['id'] = $id;
						$get['single'] = true;
						$rs = $record_data = $this->ci->m->get_record('studentexcess_prev_sem', $get);
						if($rs){
							$amount = $rs->amount;
						}

						break;
					
					default:
						# code...
						break;
				}
				
				if($amount > 0){
					#reload student profile
					$this->load_profile();

					unset($data);

					if($type == "DEDUCTION"){
						if($operator == "REMOVE"){
							$data['total_deduction'] = $this->student_payment_totals->total_deduction + $amount;
						}else{
							$data['total_deduction'] = $this->student_payment_totals->total_deduction - $amount; #reverse
						}

					}else if($type == "SCHOLARSHIP"){
						if($operator == "REMOVE"){
							$data['total_scholarship'] = $this->student_payment_totals->total_scholarship + $amount;
						}else{
							$data['total_scholarship'] = $this->student_payment_totals->total_scholarship - $amount; #reverse
						}

					}else if($type == "EXCESS"){
						if($operator == "REMOVE"){
							$data['total_excess_amount'] = $this->student_payment_totals->total_excess_amount + $amount;
						}else{

							$data['total_excess_amount'] = $this->student_payment_totals->total_excess_amount - $amount; #reverse
						}

					}else{ $data = false; }

					if($data){
						if($operator == "REMOVE"){
							$data['total_balance'] = $this->student_payment_totals->total_balance - $amount;
						}else{
							$data['total_balance'] = $this->student_payment_totals->total_balance + $amount;  #reverse
						}

						#UPDATE ENROLLMENT TABLE - BALANCE
						$this->ci->m->set_table('enrollments');
						$rs = $this->ci->m->update($this->enrollment_id, $data);
						
						$this->update_payment_status(); #update payment_status

						//CHECK IF EXCESS, IF TRUE UPDATE THE TOTAL BALANCE AND THE PAYMENT STATUS
						if($type == "EXCESS"){
							$this->ci->m->set_table('enrollments');
							$excess_enrollment = $this->ci->m->get($record_data->prev_sem_eid);
							if($excess_enrollment){
								unset($data);
								if($operator == "REMOVE"){
									$data['total_balance'] = $excess_enrollment->total_balance + ($amount);
									$data['is_apply_as_excess'] = 1;
									$data['excess_payment_eid'] = $record_data->prev_sem_eid;
								}else{
									$data['total_balance'] = $excess_enrollment->total_balance - ($amount);
									$data['is_apply_as_excess'] = 0;
									$data['excess_payment_eid'] = '';
								}
								
								$this->ci->m->set_table('enrollments');
								$rs = $this->ci->m->update($record_data->prev_sem_eid, $data);
								$this->update_payment_status($record_data->prev_sem_eid);
							}
						}
					}
				}
			}
	/* END OF STUDENT DEDUCTION FUNCTION */

	/* FUNCTION FOR FEES */

		public function get_pending_cheque()
		{
			$get['where']['spr_enrollment_id'] = $this->enrollment_id;
			$get['where']['spr_is_deleted'] = 0;
			$get['where']['spr_mode_of_payment'] = 'CHECK';
			$get['where']['check_status'] = 'PENDING';
			$get['all'] = true;
			$get['order'] = 'check_date';
			return $this->ci->m->get_record('student_payment_records',$get);
		}

		public function get_declined_cheque()
		{
			$get['where']['spr_enrollment_id'] = $this->enrollment_id;
			$get['where']['spr_is_deleted'] = 0;
			$get['where']['spr_mode_of_payment'] = 'CHECK';
			$get['where']['check_status'] = 'DECLINED';
			$get['all'] = true;
			$get['order'] = 'check_date';
			return $this->ci->m->get_record('student_payment_records',$get);
		}

		public function get_payment_record()
		{
			$this->load_current_school_profile();

			unset($get);
			$get['fields'] = array(
					'spr_id as id',
					'spr_id',
					'spr_or_no',
					'spr_payment_date as date',
					'spr_payment_date',
					'spr_ammt_paid',
					'spr_mode_of_payment',
					'spr_remarks',
					'check_number',
					'check_status'
				);
			$get['where']['spr_enrollment_id'] = $this->enrollment_id;
			$get['where']['spr_schoolyear_id'] = $this->current_sy->id;
			$get['where']['spr_is_deleted'] = 0;
			$get['order'] = "spr_id ASC";

			$payments_made = $this->ci->m->get_record('student_payment_records',$get);
			$ret = false;
			//LOOP RECORD AND EXCLUDE PENDING && DECLINED CHECKS
			if($payments_made){
				foreach ($payments_made as $key => $value) {
					if($value->spr_mode_of_payment == "CHECK" && $value->check_status != "CLEARED"){
						continue;
					}
					$ret[$key] = $value;
				}
			}

			return $ret;
		}

		public function get_payment_record_by_id($id)
		{
			$ay_id = $this->get_sy_id($id);

			unset($get);
			$get['fields'] = array(
					'spr_id as id',
					'spr_id',
					'spr_or_no',
					'spr_payment_date as date',
					'spr_payment_date',
					'spr_ammt_paid',
					'spr_mode_of_payment',
					'spr_remarks',
					'check_number',
					'check_status'
				);
			$get['where']['spr_enrollment_id'] = $id;
			$get['where']['spr_schoolyear_id'] = $ay_id;
			$get['where']['spr_is_deleted'] = 0;
			$get['order'] = "spr_id ASC";

			$payments_made = $this->ci->m->get_record('student_payment_records',$get);
			$ret = false;
			//LOOP RECORD AND EXCLUDE PENDING && DECLINED CHECKS
			if($payments_made){
				foreach ($payments_made as $key => $value) {
					if($value->spr_mode_of_payment == "CHECK" && $value->check_status != "CLEARED"){
						continue;
					}
					$ret[$key] = $value;
				}
			}

			return $ret;
		}

		/* GET PAYMENT RECORD AS OLD ACCOUNT
			- if enrollment fees was used as student_previous_account and was paid get the record of that payment
		*/
		public function get_payment_record_as_an_old_account($return_sum_amount = false)
		{
			$ret = 0;
			$rs = false;

			if($this->profile && $this->profile->is_paid_as_previous == 1){

				$paid_by_eid = $this->profile->paid_by_eid;

				$sql = "
					SELECT
					spr.spr_or_no,
					spr.`spr_payment_date`,
					spr.`spr_mode_of_payment`,
					spr.`check_number`,
					spr.`spr_remarks`,
					pb.id,
					pb.spr_id,
					SUM(pb.amount) As amount
				FROM payments_breakdown pb
				LEFT JOIN student_payment_records spr ON spr.spr_id = pb.spr_id
				WHERE pb.enrollment_id = ?
				AND pb.fee_type = 'PREVIOUS'
				AND pb.is_deleted = 0
				GROUP BY pb.spr_id;
				";
				$param[] = $paid_by_eid;

				$this->ci->m->set_table('payments_breakdown');
				$rs = $this->ci->m->query($sql, $param);
			}

			if($return_sum_amount === false){
				return $rs;
			}else{
				if($rs){
					foreach ($rs as $key => $value) {
						$ret += $value->amount;
					}
				}

				return $ret;
			}
		}

		//DELETE OR ONLY UPDATE PAYMENT RECORD is_deleted = 1
		public function delete_payment_record($id = false)
		{
			$ret['status'] = false;

			//GET DATA TOBE REMOVE
			unset($get);
			$get['where']['spr_id'] = $id;
			$get['single'] = true;
			$spr = $this->ci->m->get_record('student_payment_records',$get);
			if($spr)
			{
				$this->ci->m->set_table('student_payment_records','spr_id'); //SET TABLE TO BE USED
				unset($data);	
				$data['spr_is_deleted'] = 1;
				$data['spr_date_deleted'] = NOW;
				$data['spr_deleted_by'] = $this->userid;
				
				$rs_update = $this->ci->m->update(array('spr_id'=>$spr->spr_id), $data);
				if($rs_update){
					$ret['status'] = true;
					$ret['data'] = $spr;
					$ret['msg'] = "Payment Record with OR NO. " . $spr->spr_or_no. " is successfully deleted";

					//GET & LOOP PAYMENT BREAKDOWNS
					$this->ci->m->set_table('payments_breakdown'); //SET TABLE TO BE USED
					unset($get);
					$get['enrollment_id'] = $this->enrollment_id;
					$get['spr_id'] = $id;
					$get['is_deleted'] = 0;
					$pay_bd = $this->ci->m->fetch_all($get);		
					
					//REVERSE PAYMENT TO TUITION, OTHERS AND PREVIOUS
					if($pay_bd){
						foreach ($pay_bd as $key => $obj) {

							$this->reverse_student_plan_modes($obj->fee_type, $obj->table_id, $obj->amount, $obj->payment_type);
							
							//UPDATE is_deleted
							unset($data);
							$data['is_deleted'] = 1;
							$data['delete_date'] = date('Y-m-d g:h:s');
							$data['delete_remarks'] = "Delete Payment Records";
							$this->ci->m->set_table('payments_breakdown');
							$rs = $this->ci->m->update($obj->id, $data);						

							#check if previous, if true reverse the balance of that previous enrollments
							if($obj->fee_type == "PREVIOUS"){
								$this->ci->m->set_table('student_previous_accounts');
								$old_prev_acct = $this->ci->m->get($obj->table_id);
								if($old_prev_acct){
									$this->reverse_old_account_payment_total($old_prev_acct->prev_enrollment_eid, $obj->amount);
								}
							}
						}
					}

					//REVERSE PAYMENT TO PAYMENT TOTALS
					$this->reverse_payment_total($spr->spr_ammt_paid);

					$this->update_payment_status(); #update payment_status

					#Update/reverse subject taken - update only if no payment record is left
					$this->check_if_has_payment();
					if(!$this->has_payment){
						$this->update_all_subject_taken('MINUS');
					}
				}
			}

			return (object)$ret;
		}

		private function reverse_student_plan_modes($type = false, $id, $amount = 0, $payment_type = false){
			if($type == "TUITION")
			{
				$this->ci->m->set_table('student_plan_modes');
				$spm = $this->ci->m->get($id);
				
				if($spm && $amount > 0 && $payment_type){
					unset($data);
					$data['spr_id'] = null;
					$data['amount_paid'] = $spm->amount_paid - $amount;

					if($payment_type == "CHECK"){
						$data['check_applied'] = $spm->check_applied - $amount;
						$data['is_check'] = 0;
					}
					
					$rs = $this->ci->m->update($id, $data);

					if($rs){
						$this->update_student_plan_modes_status($id);
					}
				}

			}else if($type == "OTHER")
			{
				$this->ci->m->set_table('student_enrollment_fees','sef_id');
				$sef = $this->ci->m->get($id);

				if($sef && $amount > 0 && $payment_type){
					unset($data);
					$data['spr_id'] = NULL;
					$data['amount_paid'] = $sef->amount_paid - $amount;

					if($payment_type == "CHECK"){
						$data['check_applied'] = $sef->check_applied - $amount;
						$data['is_check'] = 0;
					}
					
					$rs = $this->ci->m->update($id, $data);
					if($rs){
						$this->update_student_enrollment_fee_status($id);
					}

				}
			}else if($type == "PREVIOUS")
			{
				$this->ci->m->set_table('student_previous_accounts');
				$spa = $this->ci->m->get($id);
				if($spa && $amount > 0 && $payment_type){
					unset($data);
					$data['spr_id'] = NULL;
					$data['amount_paid'] = $spa->amount_paid - $amount;

					if($payment_type == "CHECK"){
						$data['check_applied'] = $spa->check_applied - $amount;
						$data['is_check'] = 0;
					}
					$rs = $this->ci->m->update($id, $data);
					if($rs){
						$this->update_student_previous_account_status($id);
					}

				}
			}else{}
		}

		/* add other_fee and old_account to payment_totals
			- Add / Update payment total 
			- Used when the user adds other fee / old accounts to the enrollment
			- total_amount_due, other_fee, total_balance should be updated
		*/
		public function add_other_fee_and_old_account_to_payment_totals($type, $id)
		{
			$ret = $this->load_return_default();

			if(strtolower($type) === "other_fee"){

				#get data if exist
				unset($where);
				$where['sef_id'] = $id;
				$where['is_other_fee'] = 1;
				$where['is_added'] = 1;
				$where['is_deleted'] = 0;
				$this->ci->m->set_table('student_enrollment_fees');
				$rs = $this->ci->m->pull($where, array('sef_fee_rate'));
				if($rs === false){
					$ret->status = false;
					$ret->msg = "Something wend wrong. Student Other Fee / Clearance Fee was not added. Please try again.";
					return $ret;
				}
				
				$this->load_profile(); #reload profile
				$amount = floatval($rs->sef_fee_rate); #get amount/value of fee

				if($amount > 0){

					unset($data);
					$data['other_fee'] = $this->profile->other_fee + $amount;
					$data['total_amount_due'] = $this->profile->total_amount_due + $amount;
					$data['total_balance'] = $this->profile->total_balance + $amount;
					$this->ci->m->set_table('enrollments');
					$rs = $this->ci->m->update($this->enrollment_id, $data);
					
					if($rs){
						$ret->status = true;
						$ret->msg = "Student Payment was successfully adjusted";
						return $ret;
					}

					$ret->msg = "Student Payment was not adjusted properly, please run the recompute fees";
					return $ret;
				}

				$ret->msg = "Student fee was not adjusted properly because of zero or invalid amount";
				return $ret;

			}else if(strtolower($type) === "old_account" ){

				#get data if exist
				unset($where);
				$where['id'] = $id;
				$where['is_deleted'] = 0;
				$this->ci->m->set_table('student_previous_accounts');
				$rs = $this->ci->m->pull($where, array('value'));
				if($rs === false){
					$ret->msg = "Something wend wrong. Student Old Account Fee was not added. Please try again.";
					return $ret;
				}

				$this->load_profile(); #reload profile
				$amount = floatval($rs->value); #get amount/value of fee

				if($amount > 0){

					unset($data);
					$data['total_previous_amount'] = $this->profile->total_previous_amount + $amount;
					$data['total_amount_due'] = $this->profile->total_amount_due + $amount;
					$data['total_balance'] = $this->profile->total_balance + $amount;
					$this->ci->m->set_table('enrollments');
					$rs = $this->ci->m->update($this->enrollment_id, $data);

					if($rs){
						$ret->status = true;
						$ret->msg = "Student Old Account fee was successfully added to student fees";
						return $ret;
					}

					$ret->msg = "Student Old Account was not adjusted properly, please run the recompute fees";
					return $ret;
				}

				$ret->msg = "Student was not adjusted properly because of zero or invalid amount";
				return $ret;
			}else{}

			$this->update_payment_status();

			return $ret;
		}

		/* reverse other_fee and old_account to payment_totals
			- reverse/subtract payment total 
			- Used when the user delete/remove other fee / old accounts to the enrollment
			- total_amount_due, other_fee, total_balance should be updated
		*/
		public function reverse_other_fee_and_old_account_to_payment_totals($type, $id)
		{
			$ret = $this->load_return_default();

			if(strtolower($type) === "other_fee"){

				#get data if exist
				unset($where);
				$where['sef_id'] = $id;
				$where['is_other_fee'] = 1;
				$where['is_added'] = 1;
				$where['is_deleted'] = 0;
				$this->ci->m->set_table('student_enrollment_fees');
				$rs = $this->ci->m->pull($where, array('sef_fee_rate'));
				if($rs === false){
					$ret->status = false;
					$ret->msg = "Something wend wrong. Student Other Fee / Clearance Fee was not added. Please try again.";
					return $ret;
				}
				
				$this->load_profile(); #reload profile
				$amount = floatval($rs->sef_fee_rate); #get amount/value of fee

				if($amount > 0){

					unset($data);
					$data['other_fee'] = $this->profile->other_fee - $amount;
					$data['total_amount_due'] = $this->profile->total_amount_due - $amount;
					$data['total_balance'] = $this->profile->total_balance - $amount;
					$this->ci->m->set_table('enrollments');
					$rs = $this->ci->m->update($this->enrollment_id, $data);
					
					if($rs){
						$ret->status = true;
						$ret->msg = "Student Payment was successfully adjusted";
						return $ret;
					}

					$ret->msg = "Student Payment was not adjusted properly, please run the recompute fees";
					return $ret;
				}

				$ret->msg = "Student fee was not adjusted properly because of zero or invalid amount";
				return $ret;

			}else if(strtolower($type) === "old_account" ){

				#get data if exist
				unset($where);
				$where['id'] = $id;
				$where['is_deleted'] = 0;
				$this->ci->m->set_table('student_previous_accounts');
				$rs = $this->ci->m->pull($where, array('value'));
				if($rs === false){
					$ret->msg = "Something wend wrong. Student Old Account Fee was not added. Please try again.";
					return $ret;
				}

				$this->load_profile(); #reload profile
				$amount = floatval($rs->value); #get amount/value of fee

				if($amount > 0){

					unset($data);
					$data['total_previous_amount'] = $this->profile->total_previous_amount - $amount;
					$data['total_amount_due'] = $this->profile->total_amount_due - $amount;
					$data['total_balance'] = $this->profile->total_balance - $amount;
					$this->ci->m->set_table('enrollments');
					$rs = $this->ci->m->update($this->enrollment_id, $data);

					if($rs){
						$ret->status = true;
						$ret->msg = "Student Old Account fee was successfully added to student fees";
						return $ret;
					}

					$ret->msg = "Student Old Account was not adjusted properly, please run the recompute fees";
					return $ret;
				}

				$ret->msg = "Student was not adjusted properly because of zero or invalid amount";
				return $ret;
			}else{}

			return $ret;
		}

		private function reverse_payment_total($amount=0)
		{
			if($this->profile && $amount > 0)
			{
				unset($data);
				$this->load_profile();
				$data['total_balance'] = $this->student_payment_totals->total_balance + $amount;
				$data['total_amount_paid'] = $this->student_payment_totals->total_amount_paid - $amount;

				$this->ci->m->set_table('enrollments');
				$rs = $this->ci->m->update($this->enrollment_id, $data);
				if($rs){
					$this->update_enrollment_payment_status();
				}
			}
		}

		private function reverse_old_account_payment_total($id = false, $amount=0)
		{
			if($id === false || $amount <= 0){ return false; }
			$this->ci->m->set_table('enrollments');
			$old_account = $this->ci->m->get($id);
			if($old_account)
			{
				unset($data);
				$data['total_balance'] = $old_account->total_balance + $amount;

				$this->ci->m->set_table('enrollments');

				$rs = $this->ci->m->update($id, $data);
				
				if($rs){
					$this->update_enrollment_payment_status($id);
					
					return true;
				}
			}

			return false;
		}

		/* GET FEE ID in payment_breakdown - its the selected checkbox in add payment form */
		public function get_feeid_of_payment_breakdown($spr_id){
			$this->ci->m->set_table('payments_breakdown');
			unset($get);
			$get['fields'] = array(
					'table_id',
					'fee_type'
				);
			$get['where']['spr_id'] = $spr_id;
			$get['where']['enrollment_id'] = $this->enrollment_id;
			$get['group'] = "table_id";
			$get['order'] = 'fee_type';
			
			$r = $this->ci->m->get_record('payments_breakdown',$get);

			$ret['TUITION'] = array();
			$ret['OTHER'] = array();
			$ret['PREVIOUS'] = array();

			if($r){
				foreach ($r as $key => $value) {
					$ret[$value->fee_type][$value->table_id] = $value->table_id;
				}
			}

			return $ret;
		}

		/* UPDATE student_plan_modes table payment_status */
		public function update_student_plan_modes_status($id){
			$this->ci->m->set_table('student_plan_modes');
			$rs = $this->ci->m->pull($id, array('id','value','amount_paid','is_paid'));
			if($rs){
				$balance = $rs->value - $rs->amount_paid;

				if($rs->value <= 0){
					$data['is_paid'] = 1;
				}else{
					unset($data);
					if($balance <= 0){
						$data['is_paid'] = 1;
					}else{
						$data['is_paid'] = 0;
					}
				}

				if($rs->is_paid != $data['is_paid']){
					$rs = $this->ci->m->update($rs->id, $data);
				}
			}
		}

		/* UPDATE student_enrollment_fees table payment_status */
		public function update_student_enrollment_fee_status($id){
			$this->ci->m->set_table('student_enrollment_fees','sef_id');
			$rs = $this->ci->m->pull($id, array('sef_id','sef_fee_rate','amount_paid','is_paid'));
			if($rs){
				$balance = $rs->sef_fee_rate - $rs->amount_paid;
				unset($data);
				if($rs->sef_fee_rate <= 0){
					$data['is_paid'] = 1;
				}else{
					
					if($balance <= 0){
						$data['is_paid'] = 1;
					}else{
						$data['is_paid'] = 0;
					}
				}

				if($rs->is_paid != $data['is_paid']){
					$this->ci->m->update($rs->sef_id, $data);
				}
			}
		}

		/* UPDATE student_previous_accounts table payment_status */
		public function update_student_previous_account_status($id){
			$this->ci->m->set_table('student_previous_accounts');
			$rs = $this->ci->m->pull($id, array('id','value','amount_paid','is_paid'));
			if($rs){
				$balance = $rs->value - $rs->amount_paid;
				unset($data);
				if($rs->value <= 0){
					$data['is_paid'] = 1;
				}else{
					unset($data);
					if($balance <= 0){
						$data['is_paid'] = 1;
					}else{
						$data['is_paid'] = 0;
					}
				}

				if($rs->is_paid != $data['is_paid']){
					$this->ci->m->update($rs->id, $data);
				}
			}
		}

		/* UPDATE enrollments table payment_status */
		public function update_enrollment_payment_status($id = false){
			$this->ci->m->set_table('enrollments');
			if($id === false){
				$en = $this->profile;
			}else{
				$en = $this->ci->m->get($id);
			}

			// Update Is Paid Field
			unset($data);
			$data['is_paid'] = $this->has_made_first_payment();
			$this->ci->m->update($id?$id:$this->enrollment_id,$data);

			if($en){
				//BALANCE IS LESS THAN 0 : NO PAYMENT
				if($en->total_balance <= 0)
				{
					$stat = "FULLY PAID";
				}else
				{
					if($en->total_amount_paid >= $en->total_amount_due)
					{
						//PAID
						$stat = "FULLY PAID";
					}
					else if($en->total_amount_paid < $en->total_amount_due && $en->total_amount_paid > 0)
					{
						//PARTIAL PAID
						$stat = "PARTIAL PAID";
					}
					else
					{
						//UNPAID
						$stat = "UNPAID";
					}
				}

				if($stat != $en->payment_status){
					//UPDATE ENROLLMENTS
					unset($data);
					$data['payment_status'] = $stat;

					if($id === false){
						$rs = $this->ci->m->update($this->enrollment_id,$data);
					}else{
						$rs = $this->ci->m->update($id,$data);
					}
				}
			}
		}

		/* COMPUTE DEDUCTIONS - return the value of deductions, scholarship and excess payments */
		public function compute_deductions(){
			#reload getting of deductions
			$this->get_student_scholarship_record();
			$this->get_student_deduction_record();
			$this->get_student_excess_payment_record();

			unset($total);
			$total['total_deduction_amount'] = 0;
			$total['total_scholarship_amount'] = 0;
			$total['total_excess_amount'] = 0;

			if($this->student_deductions){
				foreach ($this->student_deductions as $key => $value) {
					$total['total_deduction_amount'] += $value->amount;
				}
			}

			if($this->student_scholarships){
				foreach ($this->student_scholarships as $key => $value) {
					$total['total_scholarship_amount'] += $value->scho_amount;
				}
			}

			if($this->student_excess_payments){
				foreach ($this->student_excess_payments as $key => $value) {
					$total['total_excess_amount'] += $value->amount;
				}
			}

			return (object)$total;
		}

		public function compute_fees(){	
			#Defaults
			$return_data['subject_units'] = 0;
			$return_data['subject_total'] = 0;
			$return_data['total_lec'] = 0;
			$return_data['total_lab'] = 0;
			$return_data['lab_fee_data'] = false;
			
			$return_data['has_nstp'] = 0;
			$return_data['tuition_fee'] = 0;
			$return_data['total_amount_due'] = 0;
			$return_data['other_fee_total'] = 0;
			$return_data['old_account_total'] = 0;

			$return_data['tuition_fee_multiplier'] = 0;
			$return_data['tot_tuition_fee'] = 0;

			$return_data['tot_misc_fee'] = 0;
			$return_data['tot_lab_fee'] = 0;
			$return_data['tot_nstp_fee'] = 0;
			$return_data['tot_other_fee'] = 0;
			$return_data['tot_other_school_fee'] = 0;
			#end of defaults

			if($this->profile == false){
				return (object)$return_data;
			}

			#Get Enrollment Subjects
			$subjects = array();
			$this->load_subjects();
			
			$total_amount_due = 0;
			$subjects_units = 0;
			$subject_total = 0;
			$total_lec = 0;
			$total_lab = 0;
			$has_nstp = 0;
			
			//LOOP SUBJECTS TO GET TOTAL NUMBER OF UNIT LAB AND LEC

			$lab_fee = false; //LAB CAN BE MULTIPLE

			if($this->student_subjects){

				foreach($this->student_subjects as $sub)
				{
					$this->ci->m->set_table('subjects');
					$subject = $this->get_subject_profile($sub->subject_id);#GET SUBJECT MASTER FILE
					if($subject)
					{
						$subjects_units += $subject->units;
						$subject_total++;
						$total_lec += $subject->lec;
						$total_lab += $subject->lab;
						if($subject->lab && $subject->lab > 0 && $subject->lab_fee_id)//VALIDATE LAB UNIT && LAB FEE ID
						{
							unset($get);
							$get['fields'] = array(
									'coursefees.value',
									'fees.name'
									);
							$get['where']['coursefees.coursefinance_id'] = $this->coursefinance_id;
							$get['where']['coursefees.fee_id'] = $subject->lab_fee_id;
							$get['join'][] = array(
									'table' => 'fees',
									'on' => 'fees.id = coursefees.fee_id',
									'type' => 'left'
								);
							$get['single'] = true;

							$lab_f = $this->ci->m->get_record('coursefees', $get);

							if($lab_f){
								$lab_fee[$subject->lab_fee_id][$subject->id]['data'] = $lab_f;
								$lab_fee[$subject->lab_fee_id][$subject->id]['value'] = $lab_f->value;
								$lab_fee[$subject->lab_fee_id][$subject->id]['unit'] = (isset($lab_fee[$subject->id]['unit']) ? $lab_fee[$subject->id]['unit'] : 0) + $subject->lab;
							}
						}
						if($subject->is_nstp == 1)
						{
							$has_nstp++;
						}
					}
				}
			
			}

			$return_data['subject_units'] = $subjects_units;
			$return_data['subject_total'] = $subject_total;
			$return_data['total_lec'] = $total_lec;
			$return_data['total_lab'] = $total_lab;
			$return_data['lab_fee_data'] = $lab_fee;
			$return_data['has_nstp'] = $has_nstp;
			$total_amount_due = 0;
			
			#region LOOP COURSE FEES
			//$return_data['course_fees'] =  $course_fees = $this->get_student_coursefees();
			$return_data['course_fees'] =  $course_fees = $this->get_student_enrollment_fees();
			// vd($this->ci->db->last_query());
			if($course_fees)
			{
				$student_fees = array();
				
				foreach($course_fees as $cf)
				{
					
					if($cf->is_tuition_fee == 1)
					{
						
						$student_fees['tuition_fee']['data'] = $cf;
						$student_fees['tuition_fee']['total'] = $total_tuition = $subjects_units * $cf->value;
						$total_amount_due += $total_tuition;
						$return_data['tuition_fee'] = $cf->value;
						$return_data['tuition_fee_multiplier'] = $cf->value;
						$return_data['tot_tuition_fee'] = $student_fees['tuition_fee']['total']; //TUITION TOTAL
					}
					else if($cf->is_lab == 1)
					{
						if(isset($lab_fee[$cf->fee_id]) && $lab_fee[$cf->fee_id]){

							foreach ($lab_fee[$cf->fee_id] as $s_id => $lab_val) {
								$lab_val = (object)$lab_val;
								$student_fees['lab'][$cf->id][$s_id]['data'] = $cf;
								$student_fees['lab'][$cf->id][$s_id]['unit'] = $lab_val->unit;
								$student_fees['lab'][$cf->id]['value'] =  $lab_tot = $lab_val->value * $lab_val->unit;
								$total_amount_due += $lab_tot; 
								$return_data['tot_lab_fee'] += $lab_tot;//LAB TOTALS
							}
						}
					}
					else if($cf->is_misc == 1)
					{
						$student_fees['misc'][] = $cf;
						$total_amount_due += $cf->value;
						$return_data['tot_misc_fee'] += $cf->value; //TOTAL MISC
					}
					else if($cf->is_other_school == 1)
					{
						$student_fees['other_school'][] = $cf;
						$total_amount_due += $cf->value;
						$return_data['tot_other_school_fee'] += $cf->value; //TOTAL OTHER SCHOOOL FEE
					}
					else if($cf->is_other == 1)
					{
						$student_fees['other'][] = $cf;
						$total_amount_due += $cf->value;
						$return_data['other_fee_total'] += $cf->value;
						$return_data['tot_other_fee'] += $cf->value; //TOTAL OTHER
					}
					else if($cf->is_nstp == 1 && $has_nstp > 0)
					{
						$student_fees['nstp'][] = $cf;
						$total_amount_due += $cf->value;
						$return_data['tot_nstp_fee'] += $cf->value; //TOTAL NSTP
					}
					else{}
				}
				$return_data['student_fees'] = $student_fees;
			}

			#check for total_previous_accounts/old accounts
			$previous_acounts = $this->load_student_previous_account();
			if($previous_acounts){
				foreach ($previous_acounts as $key => $value) {
					$return_data['old_account_total'] += $value->value;
				}
			}

			$total_deductions = $this->compute_deductions(); #return total amount of deduction, scholarship and excess
			$return_data['total_deduction'] = $total_deductions->total_deduction_amount;
			$return_data['total_scholarship'] = $total_deductions->total_scholarship_amount;
			$return_data['total_excess_amount'] = $total_deductions->total_excess_amount;

			$return_data['tuition_total_amount_due'] = $total_amount_due - $return_data['other_fee_total'];
			$return_data['total_amount_due'] = $total_amount_due;
			#endregion course fees
			// vd($return_data);
			return (object)$return_data;
		
		}

		/* RE-APPLY PAYMENT RECORD - used after recompute, re-apply payment already made to student_plan_modes
			1. Get Available Student Payment Record
		*/
		public function re_apply_payment($remarks=false)
		{
			$ret = $this->load_return_default();
			$remarks = $remarks ? $remarks : "Re Apply Payment";

			if($this->profile == false){
				return $ret;
			}

			#1. Get Available Student Payment Record - applied and not deleted
			unset($get);
			$get['where']['spr_enrollment_id'] = $this->enrollment_id;
			$get['where']['is_applied'] = 1; //APPLIED or ALREADY DEDUCTION TO BALANCE
			$get['where']['spr_is_deleted'] = 0; 
			$get['order'] = 'spr_id';
			$this->ci->m->set_table('student_payment_records','spr_id');
			$payments = $this->ci->m->get_record('student_payment_records', $get);

			if($payments === false){
				$ret->status = false;
				$ret->msg = "No Payment Record to be apply";
				return $ret;
			}

			$balance = 0; #Sobrang amount

			#LOOP PAYMENTS - RE APPLY EACH
			foreach ($payments as $key => $pay) {

				$rs = $this->apply_payment($pay->spr_id, $remarks);
			}

			$ret->status = true;
			$ret->msg = "Payment Record was successfully re-applied";
			
			return $ret;
		}

		/* APPLY PAYMENT IN THE STUDENT PAYMENT RECORD - USED WHEN RE-APPLY PAYMENT OCCURS 
			1 Get Records Student Payment Record by ID which is_applied = 1 and spr_is_delete = 0
			2 Loop student payment record
				//SUFFIX
				//T_ = TUITION
				//O_ = OTHER/ADDITIONAL FEES
				//P_ = PREVIOUS ACCOUNT
				a. locate unpaid student_plan_modes and apply the payment
				b. locate unpaid other fees and apply the payment
				c. Locate previous account and apply the payment

		*/
		private function apply_payment($spr_id, $remarks = ""){

			//TO BE RETURN VARIABLES
			$ret['status'] = false;
			$ret['msg'] = "Process Failed";
			$ret['balance'] = 0;
			$ret = (object)$ret;

			#1 Get Records Student Payment Record by ID
			$this->ci->m->set_table('student_payment_records', 'spr_id');
			$spr = $this->ci->m->get($spr_id);

			if($spr == false){
				$ret->status = false;
				$ret->msg = "Invalid Payment Record Id.";
				return $ret;
			}

			$bal_spr_amount_paid = $spr->spr_ammt_paid;
			// vp($bal_spr_amount_paid);
			$total_applied_amount = 0;
			$cheque_ids = "";

			#Get Payment Breakdown
			unset($get);
			
			$get['where']['enrollment_id'] = $this->enrollment_id;
			$get['where']['spr_id'] = $spr_id;
			$get['where']['is_deleted'] = 0;
			$get['order'] = "id, fee_type";
			$this->ci->m->set_table('payments_breakdown');
			$payments_breakdown = $this->ci->m->get_record('payments_breakdown', $get);
			
			if($payments_breakdown){
				foreach ($payments_breakdown as $key => $pb) {
					
					if(strtoupper($pb->fee_type) == "TUITION"){
						
						#get record and payment number if meron, we will based to the paymen number
						unset($get);
						$get['fields'] = array('id','number');
						$get['where']['id'] = $pb->table_id; #student_plan_modes id
						$get['single'] = true;
						$this->ci->m->set_table('student_plan_modes');
						$old_spm = $this->ci->m->get_record('student_plan_modes', $get); #old student_plan_modes

						if($old_spm){

							#look for new created student_plan_modes
							unset($get);
							$get['where']['enrollment_id'] = $this->enrollment_id;
							$get['where']['number'] = $old_spm->number;
							$get['where']['is_deleted'] = 0;
							$get['single'] = true;
							$this->ci->m->set_table('student_plan_modes');
							$new_spm = $this->ci->m->get_record('student_plan_modes', $get);

							if($new_spm){ #if record exist apply the amount to this payment division

								if($bal_spr_amount_paid > 0){

									$balance = $this->positive_val($new_spm->value - $new_spm->amount_paid); //GET POSITIVE BALANCE ONLY
									
									//GET AMOUNT TO BE APPLIED FOR STUDENT PLAN MODES DEPENDS ON THE BALANCE AMOUNT PAID
									$applied_amount = 0;
									if($bal_spr_amount_paid < $balance){
										$applied_amount = $bal_spr_amount_paid;
									}else{
										$applied_amount = $balance;
									}
									
									unset($data);
									$data['spr_id'] = $spr_id;
									$data['is_check'] = 0;
									$data['check_applied'] = 0;
									$data['is_check'] = 0;	
									$data['amount_paid'] = $new_spm->amount_paid + $applied_amount;
									$total_applied_amount += $applied_amount;

									$cheque_ids .= 'T_'.$new_spm->id.'|';
									
									$this->ci->m->set_table('student_plan_modes');
									$rs = $this->ci->m->update($new_spm->id, $data); //UPDATE RECORD
									$this->update_payment_mode_status("TUITION", $new_spm->id);

									$bal_spr_amount_paid -= $applied_amount;

									//SAVE TO STUDENT PAYMENT BREAKDOWN
									//TO BE ABLE TO KNOW HOW MUCH AMOUNT IS APPLIED 
									//FIX THE ISSUE WHEN USER ONLY PAYS HALF OF THE AMOUNT DUE
									if($rs){
										$this->insert_payment_breakdown($new_spm->id,$applied_amount,'TUITION',$spr->spr_mode_of_payment);
									}
								}
							}
						}
					}
					
					if(strtoupper($pb->fee_type) == "OTHER"){
						
						#get record other fees from student_enrollment_fees
						unset($get);
						$get['where']['sef_enrollment_id'] = $this->enrollment_id;
						$get['where']['fee_id'] = $pb->fee_id; 
						$get['where']['is_deleted'] = 0;
						$get['where']['is_paid'] = 0;
						$get['single'] = true;
						$this->ci->m->set_table('student_enrollment_fees','sef_id');
						$old_sef = $this->ci->m->get_record('student_enrollment_fees', $get); 
						// vd($old_sef);
						if($old_sef){

							if($bal_spr_amount_paid > 0){

								$balance = $this->positive_val($old_sef->sef_fee_rate - $old_sef->amount_paid); //GET POSITIVE BALANCE ONLY

								//GET AMOUNT TO BE APPLIED FOR ADDITIONAL FEES DEPENDS ON THE BALANCE AMOUNT PAID
								$applied_amount = 0;
								if($bal_spr_amount_paid < $balance){
									$applied_amount = $bal_spr_amount_paid;
								}else{
									$applied_amount = $balance;
								}

								unset($data);
								$data['spr_id'] = $spr_id;
								$data['is_check'] = 0; 
								$data['check_applied'] = 0;
								$data['is_check'] = 0;	
								$data['amount_paid'] = $old_sef->amount_paid + $applied_amount;

								$cheque_ids .= 'O_'.$old_sef->sef_id.'|';

								$this->ci->m->set_table('student_enrollment_fees','sef_id');
								$rs = $this->ci->m->update($old_sef->sef_id, $data); //UPDATE RECORD
								$this->update_payment_mode_status("OTHER", $old_sef->sef_id);

								$bal_spr_amount_paid -= $applied_amount;
								//SAVE TO STUDENT PAYMENT BREAKDOWN
								//TO BE ABLE TO KNOW HOW MUCH AMOUNT IS APPLIED 
								//FIX THE ISSUE WHEN USER ONLY PAYS HALF OF THE AMOUNT DUE
								if($rs){
									
									$this->insert_payment_breakdown($old_sef->sef_id,$applied_amount,'OTHER',$spr->spr_mode_of_payment);
								}
							}
						}
					}
					
					if(strtoupper($pb->fee_type) == "PREVIOUS"){
						
						#get record other fees from student_previous_accounts
						unset($get);
						$get['where']['enrollment_id'] = $this->enrollment_id;
						$get['where']['is_deleted'] = 0;
						$get['single'] = true;
						$this->ci->m->set_table('student_previous_accounts');
						$spa = $this->ci->m->get_record('student_previous_accounts', $get); 
						
						if($spa){

							if($bal_spr_amount_paid > 0){

								$balance = $this->positive_val($spa->value - $spa->amount_paid); //GET POSITIVE BALANCE ONLY

								//GET AMOUNT TO BE APPLIED FOR ADDITIONAL FEES DEPENDS ON THE BALANCE AMOUNT PAID
								$applied_amount = 0;
								if($bal_spr_amount_paid < $balance){
									$applied_amount = $bal_spr_amount_paid;
								}else{
									$applied_amount = $balance;
								}

								unset($data);
								$data['spr_id'] = $spr_id;
								$data['is_check'] = 0; 
								$data['check_applied'] = 0;
								$data['is_check'] = 0;	
								$data['amount_paid'] = $spa->amount_paid + $applied_amount;

								$cheque_ids .= 'O_'.$spa->id.'|';

								$this->ci->m->set_table('student_previous_accounts');
								$rs = $this->ci->m->update($spa->id, $data); //UPDATE RECORD
								$this->update_payment_mode_status("PREVIOUS", $spa->id);

								$bal_spr_amount_paid -= $applied_amount;
								
								//SAVE TO STUDENT PAYMENT BREAKDOWN
								//TO BE ABLE TO KNOW HOW MUCH AMOUNT IS APPLIED 
								//FIX THE ISSUE WHEN USER ONLY PAYS HALF OF THE AMOUNT DUE
								if($rs){
									
									$this->insert_payment_breakdown($spa->id,$applied_amount,'PREVIOUS',$spr->spr_mode_of_payment);
								}
							}
						}
					}
					
					$this->remove_payment_breakdown($pb->id, $remarks);
				}

				//UPDATE PAYMENT RECORD FOR IDS
				unset($data);
				$data['check_ids'] = $cheque_ids; //THE ID or the student_plan_mode or other fee selected in the form
				$this->ci->m->set_table('student_payment_records','spr_id');
				$rs_u = $this->ci->m->update($spr_id, $data);
			}

			#if balance exist search and apply balance to unpaid account
			$ret->balance = $bal_spr_amount_paid;
			
			if($ret->balance > 0){

				$this->search_and_apply_payment($spr_id, $ret->balance);
			}

			#Deduct To Total Balance
			$this->deduct_total_balance($spr->spr_ammt_paid);
			
			$ret->status = true;
			$ret->msg = "Payment was applied successfully.";
			
			return $ret;
		}

		/* Search and apply payment - used in apply_payment when there is balance left it will search unpaid planmodes/other fee and apply to it
			1. Get Student Payment record if exist
			2. Search for unpaid records - (student_plan_modes, student_enrollment_fees, student_previous_accounts)
			3. APPLY payment to the search result
		*/
		private function search_and_apply_payment($spr_id, $amount = 0, $type = "TUITION"){

			//TO BE RETURN VARIABLES
			$ret['status'] = false;
			$ret['msg'] = "Process Failed";
			$ret['balance'] = 0;
			$ret = (object)$ret;

			if($amount <= 0){ return $ret; }

			#1 Get Records Student Payment Record by ID
			$this->ci->m->set_table('student_payment_records', 'spr_id');
			$spr = $this->ci->m->get($spr_id);

			if($spr == false){
				$ret->status = false;
				$ret->msg = "Invalid Payment Record Id.";
				return $ret;
			}

			$bal_spr_amount_paid = $amount;
			$total_applied_amount = 0;
			$cheque_ids = "";

			#2. Search for unpaid records
			#Search in Student_plan_modes

			if($type == "TUITION")
			{

				unset($get);
				$get['where']['enrollment_id'] = $this->enrollment_id;
				$get['where']['is_deleted'] = 0;
				$get['where']['is_paid'] = 0;
				$get['order'] = "number";
				$get['single'] = true;
				$this->ci->m->set_table('student_plan_modes');
				$spm = $this->ci->m->get_record('student_plan_modes', $get);
				if($spm && $bal_spr_amount_paid > 0){

					#apply to this student_plan_modes
					$balance = $this->positive_val($spm->value - $spm->amount_paid); //GET POSITIVE BALANCE ONLY

					$applied_amount = $this->apply_payment_to_student_plan_modes($spm, $spr, $balance, $bal_spr_amount_paid);

					$bal_spr_amount_paid -= $applied_amount;

					if($bal_spr_amount_paid > 0){

						#if balance still exist loop to this function again it search again
						$this->search_and_apply_payment($spr_id, $bal_spr_amount_paid);
					}
				}
				else{

					#if balance still exist loop to this function again it search again
					$this->search_and_apply_payment($spr_id, $bal_spr_amount_paid, "OTHER");
				}
			}
			
			if($type == "OTHER")
			{

				#search naman sa student_enrollment_fees - other fees
				unset($get);
				$get['where']['sef_enrollment_id'] = $this->enrollment_id;
				$get['where']['is_deleted'] = 0;
				$get['where']['is_paid'] = 0;
				$get['where']['is_other_fee'] = 1;
				$get['order'] = "sef_id";
				$get['single'] = true;
				$this->ci->m->set_table('student_enrollment_fees');
				$sef = $this->ci->m->get_record('student_enrollment_fees', $get);
				if($sef){

					$balance = $this->positive_val($sef->sef_fee_rate - $sef->amount_paid); //GET POSITIVE BALANCE ONLY

					$applied_amount = $this->apply_payment_to_student_enrollment_fees($sef, $spr, $balance, $bal_spr_amount_paid);

					$bal_spr_amount_paid -= $applied_amount;

					if($bal_spr_amount_paid > 0){

						#if balance still exist loop to this function again it search again
						$this->search_and_apply_payment($spr_id, $bal_spr_amount_paid, "OTHER");
					}
					else
					{
						#if balance still exist loop to this function again to search for old accounts
						$this->search_and_apply_payment($spr_id, $bal_spr_amount_paid, "OLD_ACCOUNTS");
					}
				}
			}

			if($type == "OLD_ACCOUNTS")
			{

				#search naman sa student_previous_accounts - old accounts
				unset($get);
				$get['where']['enrollment_id'] = $this->enrollment_id;
				$get['where']['is_deleted'] = 0;
				$get['where']['is_paid'] = 0;
				$get['order'] = "id";
				$get['single'] = true;
				$this->ci->m->set_table('student_previous_accounts');
				$spa = $this->ci->m->get_record('student_previous_accounts', $get);
				
				if($spa){

					$balance = $this->positive_val($spa->value - $spa->amount_paid); //GET POSITIVE BALANCE ONLY

					$applied_amount = $this->apply_payment_to_student_previous_accounts($spa, $spr, $balance, $bal_spr_amount_paid);

					$bal_spr_amount_paid -= $applied_amount;

					if($bal_spr_amount_paid > 0){

						#if balance still exist loop to this function again it search again
						$this->search_and_apply_payment($spr_id, $bal_spr_amount_paid, "OLD_ACCOUNTS");
					}
					else
					{
						//BREAK THE LOOP KASI WALA NA
						return $ret;
					}
				}
			}

			return $ret;
		}

		private function apply_payment_to_student_plan_modes($spm, $spr, $balance, $bal_spr_amount_paid){

			//GET AMOUNT TO BE APPLIED FOR STUDENT PLAN MODES DEPENDS ON THE BALANCE AMOUNT PAID
			$applied_amount = 0;
			if($bal_spr_amount_paid < $balance){
				$applied_amount = $bal_spr_amount_paid;
			}else{
				$applied_amount = $balance;
			}
			
			unset($data);
			$data['spr_id'] = $spr->spr_id;

			if($spr->spr_mode_of_payment == "CHECK"){//IF CHECK, DO NOT APPLY THE PAYMENT ONLY IF CONFIRM BY FINANCE
				$data['is_check'] = 1;
				$data['check_applied'] = $spm->check_applied + $applied_amount;
			}else{
				$data['is_check'] = 0;	
			}
			$data['amount_paid'] = $spm->amount_paid + $applied_amount;
			
			$this->ci->m->set_table('student_plan_modes');
			$rs = $this->ci->m->update($spm->id, $data); //UPDATE RECORD
			$this->update_payment_mode_status("TUITION", $spm->id);

			//UPDATE PAYMENT RECORD FOR IDS
			unset($data);
			$cheque_ids = $spr->check_ids.'T_'.$spm->id.'|';
			$data['check_ids'] = $cheque_ids; //THE ID or the student_plan_mode or other fee selected in the form
			$this->ci->m->set_table('student_payment_records','spr_id');
			$rs_u = $this->ci->m->update($spr->spr_id, $data);

			//SAVE TO STUDENT PAYMENT BREAKDOWN
			//TO BE ABLE TO KNOW HOW MUCH AMOUNT IS APPLIED 
			//FIX THE ISSUE WHEN USER ONLY PAYS HALF OF THE AMOUNT DUE
			if($rs){
				$this->insert_payment_breakdown($spm->id,$applied_amount,'TUITION',$spr->spr_mode_of_payment);
			}

			return $applied_amount;
		}

		private function apply_deduction_to_student_plan_modes($spm, $deduction_record, $balance, $bal_spr_amount_paid, $deduction_type){

			//GET AMOUNT TO BE APPLIED FOR STUDENT PLAN MODES DEPENDS ON THE BALANCE AMOUNT PAID
			$applied_amount = 0;
			if($bal_spr_amount_paid < $balance){
				$applied_amount = $bal_spr_amount_paid;
			}else{
				$applied_amount = $balance;
			}
			
			unset($data);
			$data['spr_id'] = $spr->spr_id;

			if($spr->spr_mode_of_payment == "CHECK"){//IF CHECK, DO NOT APPLY THE PAYMENT ONLY IF CONFIRM BY FINANCE
				$data['is_check'] = 1;
				$data['check_applied'] = $spm->check_applied + $applied_amount;
			}else{
				$data['is_check'] = 0;	
			}
			$data['amount_paid'] = $spm->amount_paid + $applied_amount;
			
			$this->ci->m->set_table('student_plan_modes');
			$rs = $this->ci->m->update($spm->id, $data); //UPDATE RECORD
			$this->update_payment_mode_status("TUITION", $spm->id);

			//UPDATE PAYMENT RECORD FOR IDS
			unset($data);
			$cheque_ids = $spr->check_ids.'T_'.$spm->id.'|';
			$data['check_ids'] = $cheque_ids; //THE ID or the student_plan_mode or other fee selected in the form
			$this->ci->m->set_table('student_payment_records','spr_id');
			$rs_u = $this->ci->m->update($spr->spr_id, $data);

			//SAVE TO STUDENT PAYMENT BREAKDOWN
			//TO BE ABLE TO KNOW HOW MUCH AMOUNT IS APPLIED 
			//FIX THE ISSUE WHEN USER ONLY PAYS HALF OF THE AMOUNT DUE
			if($rs){
				$this->insert_payment_breakdown($spm->id,$applied_amount,$deduction_type,$spr->spr_mode_of_payment);
			}

			return $applied_amount;
		}

		private function apply_payment_to_student_enrollment_fees($sef, $spr, $balance, $bal_spr_amount_paid){

			//GET AMOUNT TO BE APPLIED FOR ADDITIONAL FEES DEPENDS ON THE BALANCE AMOUNT PAID
			$applied_amount = 0;
			if($bal_spr_amount_paid < $balance){
				$applied_amount = $bal_spr_amount_paid;
			}else{
				$applied_amount = $balance;
			}

			unset($data);
			$data['spr_id'] = $spr->spr_id;

			if($spr->spr_mode_of_payment == "CHECK"){//IF CHECK TO NOT APPLY THE PAYMENT ONLY IF CONFIRM BY FINANCE
				$data['is_check'] = 1;
				$data['check_applied'] = $sef->amount_paid + $applied_amount;
			}else{
				$data['is_check'] = 0;	
			}
			$data['amount_paid'] = $sef->amount_paid + $applied_amount;

			$this->ci->m->set_table('student_enrollment_fees','sef_id');
			$rs = $this->ci->m->update($sef->sef_id, $data); //UPDATE RECORD
			$this->update_payment_mode_status("OTHER", $sef->sef_id);

			//UPDATE PAYMENT RECORD FOR IDS
			unset($data);
			$cheque_ids = $spr->check_ids.'T_'.$sef->sef_id.'|';
			$data['check_ids'] = $cheque_ids; //THE ID or the student_plan_mode or other fee selected in the form
			$this->ci->m->set_table('student_payment_records','spr_id');
			$rs_u = $this->ci->m->update($spr->spr_id, $data);

			//SAVE TO STUDENT PAYMENT BREAKDOWN
			//TO BE ABLE TO KNOW HOW MUCH AMOUNT IS APPLIED 
			//FIX THE ISSUE WHEN USER ONLY PAYS HALF OF THE AMOUNT DUE
			if($rs){
				
				$this->insert_payment_breakdown($sef->sef_id,$applied_amount,'OTHER',$spr->spr_mode_of_payment);
			}

			return $applied_amount;
		}

		private function apply_payment_to_student_previous_accounts($spa, $spr, $balance, $bal_spr_amount_paid){

			//GET AMOUNT TO BE APPLIED FOR ADDITIONAL FEES DEPENDS ON THE BALANCE AMOUNT PAID
			$applied_amount = 0;
			if($bal_spr_amount_paid < $balance){
				$applied_amount = $bal_spr_amount_paid;
			}else{
				$applied_amount = $balance;
			}

			unset($data);
			$data['spr_id'] = $spr->spr_id;

			if($spr->spr_mode_of_payment == "CHECK"){//IF CHECK TO NOT APPLY THE PAYMENT ONLY IF CONFIRM BY FINANCE
				$data['is_check'] = 1;
				$data['check_applied'] = $spa->amount_paid + $applied_amount;
			}else{
				$data['is_check'] = 0;	
			}
			$data['amount_paid'] = $spa->amount_paid + $applied_amount;

			$this->ci->m->set_table('student_previous_accounts');
			$rs = $this->ci->m->update($spa->id, $data); //UPDATE RECORD
			$this->update_payment_mode_status("PREVIOUS", $spa->id);

			//UPDATE PAYMENT RECORD FOR IDS
			unset($data);
			$cheque_ids = $spr->check_ids.'T_'.$spa->sef_id.'|';
			$data['check_ids'] = $cheque_ids; //THE ID or the student_plan_mode or other fee selected in the form
			$this->ci->m->set_table('student_payment_records','spr_id');
			$rs_u = $this->ci->m->update($spr->spr_id, $data);

			//SAVE TO STUDENT PAYMENT BREAKDOWN
			//TO BE ABLE TO KNOW HOW MUCH AMOUNT IS APPLIED 
			//FIX THE ISSUE WHEN USER ONLY PAYS HALF OF THE AMOUNT DUE
			if($rs){
				
				$this->insert_payment_breakdown($spa->id,$applied_amount,'PREVIOUS',$spr->spr_mode_of_payment);
			}

			return $applied_amount;
		}

		public function deduct_total_balance($amount=0){
			
			if($this->profile && $amount > 0){
				#Deduct Total Balance
				unset($data);	
				$data['total_balance'] = $this->student_payment_totals->total_balance - ($amount);
				$data['total_amount_paid'] = $this->student_payment_totals->total_amount_paid + ($amount);
				$this->ci->m->set_table('enrollments');

				$rs_u = $this->ci->m->update($this->enrollment_id, $data);
				if($rs_u){//UPDATE PAYMENT STATUS
					$this->update_payment_status();
				}
			}
		}

		private function remove_payment_breakdown($id, $remarks=""){

			$ret = $this->load_return_default();

			#remove the old breakdown
			unset($data);
			$data['is_deleted'] = 1;
			$data['delete_remarks'] = $remarks;
			$data['delete_date'] = NOW;
			$this->ci->m->set_table('payments_breakdown');
			$rs = $this->ci->m->update($id, $data);
			if($rs){
				$ret->status = true;
				$ret->msg = "Breakdown Successfully removed";
				return $ret;
			}
			return $ret;
		}

		private function insert_payment_breakdown($id,$values, $fee_type, $payment_type){

			unset($data);
			$data['fee_type'] = $fee_type;
			$data['amount'] = $values; //AMOUNT OF THE PAYMENT
			$data['payment_type'] = $payment_type; //CHECK or CASH etc

			switch (strtoupper($fee_type)) {
				case 'TUITION':
					$this->ci->m->set_table('student_plan_modes');
					$spm = $this->ci->m->get($id);
					if($spm){
						
						$data['enrollment_id'] = $spm->enrollment_id;
						$data['spr_id'] = $spm->spr_id;
						$data['table_id'] = $spm->id;
						$this->ci->m->set_table('payments_breakdown');
						$this->ci->m->insert($data);
					}

					break;
				case 'OTHER':
					$this->ci->m->set_table('student_enrollment_fees','sef_id');
					$sef = $this->ci->m->get($id);
					if($sef){
						
						$data['enrollment_id'] = $sef->sef_enrollment_id;
						$data['spr_id'] = $sef->spr_id;
						$data['fee_id'] = $sef->fee_id;
						$data['table_id'] = $sef->sef_id;
						$this->ci->m->set_table('payments_breakdown');
						$this->ci->m->insert($data);
					}
					break;
				case 'PREVIOUS':
					$this->ci->m->set_table('student_previous_accounts');
					$spa = $this->ci->m->get($id);
					if($spa){
						$data['enrollment_id'] = $spa->enrollment_id;
						$data['spr_id'] = $spa->spr_id;
						$data['table_id'] = $spa->id;
						$this->ci->m->set_table('payments_breakdown');
						$this->ci->m->insert($data);
					}
					break;
			}

		}

		private function update_payment_mode_status($type, $id){

			if(strtoupper($type) == "TUITION"){
				$this->ci->m->set_table('student_plan_modes');
				$rs = $this->ci->m->pull($id, array('id','value','amount_paid','is_paid'));
				if($rs){
					$balance = $rs->value - $rs->amount_paid;
					unset($data);
					if($balance <= 0){
						$data['is_paid'] = 1;
					}else{
						$data['is_paid'] = 0;
					}

					if($rs->is_paid != $data['is_paid']){
						$this->ci->m->update($rs->id, $data);
					}
				}
			}else if(strtoupper($type) == "OTHER"){

				$this->ci->m->set_table('student_enrollment_fees','sef_id');
				$rs = $this->ci->m->pull($id, array('sef_id','sef_fee_rate','amount_paid','is_paid'));

				if($rs){
					$balance = $rs->sef_fee_rate - $rs->amount_paid;
					unset($data);
					if($balance <= 0){
						$data['is_paid'] = 1;
					}else{
						$data['is_paid'] = 0;
					}
					
					if($rs->is_paid != $data['is_paid']){
						$this->ci->m->update($rs->sef_id, $data);
					}
				}

			}else if(strtoupper($type) == "PREVIOUS"){
				$this->ci->m->set_table('student_previous_accounts');
				$rs = $this->ci->m->pull($id, array('id','value','amount_paid','is_paid'));
				if($rs){
					$balance = $rs->value - $rs->amount_paid;
					unset($data);
					if($balance <= 0){
						$data['is_paid'] = 1;
					}else{
						$data['is_paid'] = 0;
					}

					if($rs->is_paid != $data['is_paid']){
						$this->ci->m->update($rs->id, $data);
					}
				}
			}else{}
		}

		/* GET STUDENT ENROLLMENT FEES */
		public function get_student_enrollment_fees(){
			
			unset($get);			
			$get['fields'] = array(
					'sef_id as id',
					'fee_id',
					'sef_fee_name as name',
					'sef_fee_rate as value',
					'is_tuition_fee',
					'is_misc_fee as is_misc',
					'is_other_fee as is_other',
					'is_other_school',
					'is_lab_fee as is_lab',
					'is_nstp_fee as is_nstp',
					'amount_paid',
					'is_paid',
					'spr_id',
					'is_check',
					'check_applied'
				);
			$get['where']['sef_enrollment_id'] = $this->enrollment_id;
			$get['where']['is_deleted'] = 0;
			$this->ci->m->set_table('student_enrollment_fees');
			return $this->ci->m->get_record('student_enrollment_fees', $get);
		}

		/* RECOMPUTE FEES - PROCESS
			1st Process - Get Course Finance ID base on course id
			2nd Process - GET COURSE FEES AND UPDATE STUDENT ENROLLMENT FEES
			3rd Process - Compute for student tuition fees && update enrollment table
			4th Process - CREATE student plan modes according to payment plan 

			@remarks - will be used as delete_remarks in student_plan_modes and student_enrollment_fees(will be removed and added again)
		*/

		public function recompute_fees($remarks = false){
			set_time_limit(0);

			$remarks = $remarks ? $remarks : 'recompute fees';

			$ret['status'] = false;
			$ret['msg'] = "Failed to Re-compute.";

			//CHECK IF ENROLLMENT EXIST
			if($this->profile == false){
				return (object)$ret;
			}

			//RUN ONLY THIS FUNCTION IF AND ONLY IF STUDENT HAS NO PAYMENTS YET
			// if($this->has_payment == true){
			// 	$ret['status'] = false;
			// 	$ret['msg'] = "Cannot run this process if the student have payment records.";
			// 	return (object)$ret;
			// }

			#LOAD CURRENT OPEN SEMESTERS
			$this->load_current_school_profile();
			
			$academic_year_id = $this->get_academic_year_id($this->profile->sy_from, $this->profile->sy_to);
			if($academic_year_id == false){
				$ret['status'] = false;
				$ret['msg'] = "Academic Year was not set.";
				return (object)$ret;
			}

			/* 1st Process - Get Course Finance ID base on course id */
			$assign_coursefees = $this->get_assign_course_fees();

			//IF THIS IS FALSE - means the course has no course finance record, this should be set by the admin at course finance management
			if($assign_coursefees == false){
				$ret['status'] = false;
				$ret['msg'] = "No Available course finance for the student course. The course should be set at the course fee management by the admin.";
				return (object)$ret;	
			}

			#UPDATE enrollment table for coursefinance_id
			unset($data);
			$data['coursefinance_id'] = $assign_coursefees->coursefinance_id;
			$this->ci->m->set_table('enrollments');
			$rs = $this->ci->m->update($this->enrollment_id, $data);
			if($rs == false){
				$ret['status'] = false;
				$ret['msg'] = "Updating coursefinance failed. Please try again.";
				return (object)$ret;	
			}
			
			$this->load_profile(); #reload STUDENT PROFILE

			/* 2nd Process - GET COURSE FEES AND UPDATE STUDENT ENROLLMENT FEES*/

			$student_course_fees = $this->get_student_coursefees();
			if($student_course_fees == false){
				$ret['status'] = false;
				$ret['msg'] = "The course finance set doesnt have course fee record. Please add a fee in the course finance management and try again.";
				return (object)$ret;
			}

			#clear student_enrollment fees first except those added manually by registrar/finance - dont delete update only is_deleted = 1
			$this->ci->m->set_table('student_enrollment_fees','sef_id');
			unset($data);
			$data['is_deleted'] = 1;
			$data['deleted_by'] =  $this->userid;
			$data['delete_date'] = date('Y-m-d g:h:s');
			$data['delete_remarks'] = $remarks;

			unset($where);
			$where['sef_enrollment_id'] = $this->enrollment_id;
			$where['is_added'] = 0;
			$where['is_deleted'] = 0;
			$this->ci->m->update($where, $data);

			#clear other fees which is added not the other fee in the course fees
			unset($data);
			$data['is_paid'] = 0;
			$data['amount_paid'] = 0;
			$data['spr_id'] = "";
			$data['is_check'] = 0;
			$data['check_applied'] = 0;

			unset($where);
			$where['sef_enrollment_id'] = $this->enrollment_id;
			$where['is_added'] = 1;
			$where['is_deleted'] = 0;
			$this->ci->m->set_table('student_enrollment_fees','sef_id');
			$this->ci->m->update($where, $data);

			#clear student_previous_accounts
			unset($data);
			$data['is_paid'] = 0;
			$data['amount_paid'] = 0;
			$data['spr_id'] = "";
			$data['is_check'] = 0;
			$data['check_applied'] = 0;

			unset($where);
			$where['enrollment_id'] = $this->enrollment_id;
			$where['is_deleted'] = 0;
			$this->ci->m->set_table('student_previous_accounts');
			$this->ci->m->update($where, $data);



			foreach($student_course_fees as $cf)
			{
				unset($studentfee);
				$studentfee['sef_enrollment_id'] = $this->enrollment_id;
				$studentfee['sef_fee_name'] = $cf->name;
				$studentfee['sef_fee_rate'] = $cf->value;
				$studentfee['sef_gperiod_id'] = $this->current_grading->id;
				$studentfee['sef_schoolyear_id'] = $this->current_sy->id;
				$studentfee['fee_id'] = $cf->fee_id;
				$studentfee['is_tuition_fee'] = $cf->is_tuition_fee;
				$studentfee['is_misc_fee'] = $cf->is_misc;
				$studentfee['is_other_school'] = $cf->is_other_school;
				$studentfee['is_other_fee'] = $cf->is_other;
				$studentfee['is_lab_fee'] = $cf->is_lab;
				$studentfee['is_nstp_fee'] = $cf->is_nstp;
				$studentfee['amount_paid'] = 0;
				$studentfee['is_paid'] = 0;
				$studentfee['recompute_by'] = $this->userid;
				$studentfee['recompute_date'] = date('Y-m-d g:h:s');
				$this->ci->m->set_table('student_enrollment_fees','sef_id');
				$rs = $this->ci->m->insert($studentfee); #insert to student enrollment fees
			}

			/* 3rd Process - Compute for student tuition fees && update enrollment table*/
			unset($data);
			$data = $this->compute_fees(); #Return Student Payment Total
			$rs = $this->update_payment_totals($data, true); #update enrollment table

			/* 4th Process - CREATE student plan modes according to payment plan */
			$rs2 = $this->create_student_plan_modes($remarks);
			// die('x');
			if($rs && $rs2->status){

				#re-apply student payment record
				if($this->has_payment == true){
					$this->re_apply_payment();
				}

				$this->apply_excess_deduction($this->total_excess_in_deduction); #apply the excess deduction to other fees or old accounts
				$this->total_excess_in_deduction = 0; #clear

				$ret['status'] = true;
				$ret['msg'] = "Recompute process was completed. Student Fees was successfully re-assess.";
				$this->update_enrollment_payment_status();
				return (object)$ret;
			}else{
				$this->update_enrollment_payment_status();
				if($rs == false){
					$ret['status'] = false;
					$ret['msg'] = "Updating of enrollment fees failed. Please try again.";
					return (object)$ret;
				}

				if($rs2->status == false){
					$ret['status'] = false;
					$ret['msg'] = $rs2->msg;
					return (object)$ret;
				}
			}
			return (object)$ret;
		}

		/* Create Student_plan_modes according the paymen division */
		private function create_student_plan_modes($remarks = ""){
			$this->load_profile(); #update profile record

			//DIVIDE AMOUNT AND CREATE STUDENT PLAN MODES
			if($this->profile && $this->profile->payment_division && $this->profile->payment_division > 0){

				$ctr = 0;
				unset($data);
				$data['is_deleted'] = 1;
				$data['deleted_by'] = $this->userid;
				$data['deleted_date'] = date('Y-m-d g:h:s');
				$data['delete_remarks'] = $remarks;
				$this->ci->m->set_table('student_plan_modes','id'); #set table to be used by model
				$this->ci->m->update(array('enrollment_id'=>$this->enrollment_id), $data);
				#$this->ci->m->delete(array('enrollment_id'=>$this->enrollment_id));

				$division = $this->profile->payment_division;
				$amount_divided = $this->student_payment_totals->total_plan_due / $division;

				for($g = 1; $g <= $division; $g++){

					unset($student_plan_modes);
					$student_plan_modes['enrollment_id'] = $this->enrollment_id;
					$student_plan_modes['payment_plan_id'] = $this->profile->payment_plan_id;
					$student_plan_modes['number'] = $g;
					$student_plan_modes['check_applied'] = 0;
					$student_plan_modes['is_check'] = 0;
					$student_plan_modes['amount_paid'] = 0;
					$student_plan_modes['value'] = $amount_divided;
					$student_plan_modes['is_paid'] = $amount_divided <= 0 ? 1 : 0;

					if($g == 1){
						$student_plan_modes['name'] = "Registration";
					}else{
						$student_plan_modes['name'] = $this->num_to_th($g).' Payment';
					}

					$this->ci->m->set_table('student_plan_modes','id'); #set table to be used by model
					$rs = (object)$this->ci->m->insert($student_plan_modes);
					if($rs->status){
						$ctr++;
					}
				}
				if($ctr > 0){
					$ret['status'] = true;
					return (object)$ret;
				}else{
					$ret['status'] = false;
					$ret['msg'] = "Saving of student plan failed. Please try again";
					return (object)$ret;
				}
			}

			$ret['status'] = false;
			$ret['msg'] = "Transaction Failed. No Payment Plan / Payment Plan Division Set";
			return (object)$ret;
		}

		# Compute Fee && Update Enrollment Payment Totals (Tuition Fee, misc fee , other fee, lab fee, nstp fee etc.)
		private function update_payment_totals($data, $recompute = false)
		{
			unset($total);
			$total['total_units'] = $data->subject_units;
			$total['total_lec_units'] = $data->total_lec;
			$total['total_lab_units'] = $data->total_lab;

			$total['tuition_fee_multiplier'] = $data->tuition_fee_multiplier;
			$total['tuition_fee'] = $data->tot_tuition_fee;
			$total['misc_fee'] = $data->tot_misc_fee;
			$total['other_school_fee'] = $data->tot_other_school_fee;
			$total['other_fee'] = $data->tot_other_fee;
			$total['lab_fee'] = $data->tot_lab_fee;
			$total['nstp_fee'] = $data->tot_nstp_fee;
			$total['total_previous_amount'] = $data->old_account_total;
			
			$total['total_deduction'] = $data->total_deduction;
			$total['total_scholarship'] = $data->total_scholarship;
			$total['total_excess_amount'] = $data->total_excess_amount;

			#Subtract the deduction to total plan due if my sobra apply the deduction to unpaid other fees, old accounts
			$total_plan_due = $data->tuition_total_amount_due  - ($data->total_deduction + $data->total_scholarship + $data->total_excess_amount);
			$this->total_excess_in_deduction = $total_plan_due < 0 ? $total_plan_due*-1 : 0; //my sobra ung deduction apply to other fees later
			$total_plan_due = $total_plan_due <= 0 ? 0 : $total_plan_due;
			// vd($data);
			$total['total_plan_due'] = $total_plan_due;
			$total['total_amount_due'] = $data->total_amount_due + $data->old_account_total; //TUITION FEE + MISC FEE + LAB FEE + NSTP FEE + old accounts
			$total['total_amount_paid'] = 0;

			$old_account_account = $this->get_payment_record_as_an_old_account(true);
			$total['total_balance'] = $total['total_amount_due'] - ($data->total_deduction + $data->total_scholarship + $data->total_excess_amount);

			if($recompute){
				$total['recompute_by'] = $this->userid;
				$total['recompute_date'] = date('Y-m-d g:h:s');
			}
			$this->ci->m->set_table('enrollments');
			// vp($data);
			// vd($total);
			$rs = $this->ci->m->update($this->enrollment_id, $total); //UPDATE ENROLLMENT FOR FEE TOTALS

			$this->update_payment_status();

			return $rs;
		}

		/* Compute Fee && Update Enrollment Payment Totals (Tuition Fee, misc fee , other fee, lab fee, nstp fee etc.)
			-old function - deduction is only subtracted on the total_balance.
		*/
		private function update_payment_totals_1($data, $recompute = false)
		{
			unset($total);
			$total['total_units'] = $data->subject_units;
			$total['total_lec_units'] = $data->total_lec;
			$total['total_lab_units'] = $data->total_lab;

			$total['tuition_fee_multiplier'] = $data->tuition_fee_multiplier;
			$total['tuition_fee'] = $data->tot_tuition_fee;
			$total['misc_fee'] = $data->tot_misc_fee;
			$total['other_fee'] = $data->tot_other_fee;
			$total['lab_fee'] = $data->tot_lab_fee;
			$total['nstp_fee'] = $data->tot_nstp_fee;
			$total['total_previous_amount'] = $data->old_account_total;
			
			$total['total_deduction'] = $data->total_deduction;
			$total['total_scholarship'] = $data->total_scholarship;
			$total['total_excess_amount'] = $data->total_excess_amount;

			$total['total_plan_due'] = $data->tuition_total_amount_due;
			$total['total_amount_due'] = $data->total_amount_due + $data->old_account_total; //TUITION FEE + MISC FEE + LAB FEE + NSTP FEE + old accounts
			$total['total_amount_paid'] = 0;

			$old_account_account = $this->get_payment_record_as_an_old_account(true);
			$total['total_balance'] = $total['total_amount_due'] - ($data->total_deduction + $data->total_scholarship + $data->total_excess_amount + $old_account_account);

			if($recompute){
				$total['recompute_by'] = $this->userid;
				$total['recompute_date'] = date('Y-m-d g:h:s');
			}
			$this->ci->m->set_table('enrollments');
			$rs = $this->ci->m->update($this->enrollment_id, $total); //UPDATE ENROLLMENT FOR FEE TOTALS

			$this->update_payment_status();

			return $rs;
		}

		/* */
		private function apply_excess_deduction($amount = 0, $type = "OTHER"){

			//TO BE RETURN VARIABLES
			$ret['status'] = false;
			$ret['msg'] = "Process Failed";
			$ret['balance'] = 0;
			$ret = (object)$ret;

			if($amount <= 0){ return $ret; }

			$bal_spr_amount_paid = $amount;
			$total_applied_amount = 0;
			
			if($type == "OTHER")
			{

				#search naman sa student_enrollment_fees - other fees
				unset($get);
				$get['where']['sef_enrollment_id'] = $this->enrollment_id;
				$get['where']['is_deleted'] = 0;
				$get['where']['is_paid'] = 0;
				$get['where']['is_other_fee'] = 1;
				$get['order'] = "sef_id";
				$get['single'] = true;
				$this->ci->m->set_table('student_enrollment_fees');
				$sef = $this->ci->m->get_record('student_enrollment_fees', $get);
				if($sef){

					$balance = $this->positive_val($sef->sef_fee_rate - $sef->amount_paid); //GET POSITIVE BALANCE ONLY

					// $applied_amount = $this->apply_payment_to_student_enrollment_fees($sef, $spr, $balance, $bal_spr_amount_paid);

					//GET AMOUNT TO BE APPLIED FOR ADDITIONAL FEES DEPENDS ON THE BALANCE AMOUNT PAID

					$applied_amount = 0;
					if($bal_spr_amount_paid < $balance){
						$applied_amount = $bal_spr_amount_paid;
					}else{
						$applied_amount = $balance;
					}

					unset($data);
					$data['is_deduction_paid'] = 1;
					$data['is_check'] = 0;
					$data['amount_paid'] = $sef->amount_paid + $applied_amount;

					$this->ci->m->set_table('student_enrollment_fees','sef_id');
					$rs = $this->ci->m->update($sef->sef_id, $data); //UPDATE RECORD
					$this->update_payment_mode_status("OTHER", $sef->sef_id);

					$bal_spr_amount_paid -= $applied_amount;

					if($bal_spr_amount_paid > 0){

						#if balance still exist loop to this function again it search again
						$this->apply_excess_deduction($bal_spr_amount_paid, "OTHER");
					}
					else
					{
						#if balance still exist loop to this function again to search for old accounts
						$this->apply_excess_deduction($bal_spr_amount_paid, "OLD_ACCOUNTS");
					}
				}
			}

			if($type == "OLD_ACCOUNTS")
			{

				#search naman sa student_previous_accounts - old accounts
				unset($get);
				$get['where']['enrollment_id'] = $this->enrollment_id;
				$get['where']['is_deleted'] = 0;
				$get['where']['is_paid'] = 0;
				$get['order'] = "id";
				$get['single'] = true;
				$this->ci->m->set_table('student_previous_accounts');
				$spa = $this->ci->m->get_record('student_previous_accounts', $get);
				
				if($spa){

					$balance = $this->positive_val($spa->value - $spa->amount_paid); //GET POSITIVE BALANCE ONLY

					//GET AMOUNT TO BE APPLIED FOR ADDITIONAL FEES DEPENDS ON THE BALANCE AMOUNT PAID
					$applied_amount = 0;
					if($bal_spr_amount_paid < $balance){
						$applied_amount = $bal_spr_amount_paid;
					}else{
						$applied_amount = $balance;
					}

					unset($data);
					$data['spr_id'] = $spr->spr_id;

					if($spr->spr_mode_of_payment == "CHECK"){//IF CHECK TO NOT APPLY THE PAYMENT ONLY IF CONFIRM BY FINANCE
						$data['is_check'] = 1;
						$data['check_applied'] = $spa->amount_paid + $applied_amount;
					}else{
						$data['is_check'] = 0;	
					}
					$data['amount_paid'] = $spa->amount_paid + $applied_amount;

					$this->ci->m->set_table('student_previous_accounts');
					$rs = $this->ci->m->update($spa->id, $data); //UPDATE RECORD
					$this->update_payment_mode_status("PREVIOUS", $spa->id);

					$bal_spr_amount_paid -= $applied_amount;

					if($bal_spr_amount_paid > 0){

						#if balance still exist loop to this function again it search again
						$this->apply_excess_deduction($bal_spr_amount_paid, "OLD_ACCOUNTS");
					}
					else
					{
						//BREAK THE LOOP KASI WALA NA
						return $ret;
					}
				}
			}

			return $ret;
		}

		/*GET course FINANCE*/
		public function get_assign_course_fees()
		{
			if($this->profile)
			{
				$academic_year_id = $this->get_academic_year_id($this->profile->sy_from, $this->profile->sy_to);
				if($academic_year_id == false){
					return false;
				}

				unset($get);
				$get['where']['year_id'] = $this->profile->year_id;
				$get['where']['course_id'] = $this->profile->course_id;
				$get['where']['semester_id'] = $this->profile->semester_id;
				$get['where']['academic_year_id'] = $academic_year_id;
				$get['single'] = true;
				$this->ci->m->set_table('assign_coursefees');
				$assign_coursefees = $this->ci->m->get_record('assign_coursefees',$get);
				return $assign_coursefees;
			}

			return false;
		}

		/*GET STUDENT COURSE FEES BASED ON THE COURSE FINANCE ID*/
		public function get_student_coursefees()
		{
			if($this->coursefinance_id){

				unset($get);
				$get['fields'] = array(
						'coursefees.id', 
						'coursefees.fee_id', 
						'fees.name', 
						'coursefees.value', 
						'fees.is_misc', 
						'fees.is_other_school', 
						'fees.is_lab', 
						'fees.is_other', 
						'fees.position',
						'fees.is_tuition_fee',
						'fees.is_nstp'
					);
				$get['where']['coursefees.coursefinance_id'] = $this->coursefinance_id;
				$get['join'][] = array(
						'table' => 'fees',
						'on' => 'coursefees.fee_id = fees.id',
						'type' => 'right'
					);
				return $this->ci->m->get_record('coursefees',$get);
			}

			return false;
		}

		/* 
			GET Available School Year based on year_from and year_to
			Reason - hindi kasi nakasave ung academic_year_id sa table ng enrollments
		 */
		private function get_academic_year_id($year_from, $year_to)
		{
			unset($get);
			$get['where']['year_from'] = $year_from;
			$get['where']['year_to'] = $year_to;
			$get['single'] = true;
			$this->ci->m->set_table('academic_years');
			$q = $this->ci->m->get_record('academic_years',$get);
			return $q ? $q->id : false;
		}

		/*
			END OF RECOMPUTE FUNCTIONS
		*/

		/* UPDATE IF FULLY PAID, PARTIAL PAID or UNPAID */
		public function update_payment_status(){

			#reload profile
			$this->load_profile();
			$en = $this->profile;

			if($en){

				// Update Is Paid Field
				$this->ci->m->set_table('enrollments');
				unset($data);
				$data['is_paid'] = $this->has_made_first_payment();
				$this->ci->m->update($this->enrollment_id,$data);

				//BALANCE IS LESS THAN 0 : NO PAYMENT
				if($en->total_balance <= 0)
				{
					$stat = "FULLY PAID";
				}else
				{
					if($en->total_amount_paid >= $en->total_amount_due)
					{
						//PAID
						$stat = "FULLY PAID";
					}
					else if($en->total_amount_paid < $en->total_amount_due && $en->total_amount_paid > 0)
					{
						//PARTIAL PAID
						$stat = "PARTIAL PAID";
					}
					else
					{
						//UNPAID
						$stat = "UNPAID";
					}
				}

				if($stat != $en->payment_status){
					//UPDATE ENROLLMENTS
					unset($data);
					$data['payment_status'] = $stat;
					$this->ci->m->update($this->enrollment_id, $data);
				}
			}
		}

		/*GET required payment of student this current grading
			1. Get Current Grading Period
			2. Get Order/number of grading period in the payment_plan_req
			3. Get Student Plan Modes Balance using the number in 2 Desc 
			4. Get Balance of other fees, previous account
			5. Subtract Deductions, Scholarship & Advance/Excess Payment
		*/
		public function get_required_payment(){
			$ret = 0;

			#VALIDATE PAYMENT STATUS
			if($this->student_payment_totals->total_balance <= 0){
				return $ret;
			}

			#1 .re-load current open semesters
			$this->load_current_school_profile();

			#2. Get Order/number of grading period in the payment_plan_req
			unset($par);
			$par['payment_plan_id'] = $this->profile->payment_plan_id;
			$par['grading_period_id'] = $this->current_grading->id;
			$this->ci->m->set_table('payment_plan_req');
			$req = $this->ci->m->pull($par);
			
			if($req){

				#3. Get Student Plan Modes Balance using the number in 2 Desc 
				unset($get);
				$get['where']['enrollment_id'] = $this->enrollment_id;
				$get['where']['is_deleted'] = 0;
				$get['where']['number <= '] = $req->number;
				$this->ci->m->set_table('student_plan_modes');
				$sp = $this->ci->m->get_record('student_plan_modes', $get);
				
				if($sp){
					foreach ($sp as $key => $value) {
						$ret += $value->value - $value->amount_paid;
					}
				}
			}

			#4. Get Balance of other fees, previous account

			#Get Other Fees Balance
			$fees = $this->get_student_fee_profile();
			if(isset($fees->other_fee) && $fees->other_fee){
				foreach ($fees->other_fee as $key => $value) {
					if(strtolower($key) == "total"){ continue; }
					$value = (object)$value;
					$ret += $value->amount - $value->amount_paid;
				}
			}

			#Get Previous Account Balance
			if(isset($fees->old_accounts) && $fees->old_accounts){
				foreach ($fees->old_accounts as $key => $value) {
					$ret += $value->value - $value->amount_paid;
				}
			}

			#5. Subtract Deductions, Scholarship & Advance/Excess Payment
			//$ret = $ret - ($this->student_payment_totals->total_deduction + $this->student_payment_totals->total_scholarship + $this->student_payment_totals->total_excess_amount);

			$ret = $ret <= 0 ? 0 : $ret;

			return $ret;
		}

		//GET STUDENT EXCESS PAYMENTS
		public function get_student_excess_payment_record($id = false)
		{
			unset($get);
			$get['fields'] = array(
					'studentexcess_prev_sem.id',
					'studentexcess_prev_sem.enrollment_id',
					'studentexcess_prev_sem.prev_sem_eid',
					'studentexcess_prev_sem.amount',
					'enrollments.sy_from',
					'enrollments.sy_to',
					'years.year',
				);
			$get['where']['studentexcess_prev_sem.enrollment_id'] = !$id ? $this->enrollment_id : $id;
			$get['where']['studentexcess_prev_sem.is_deleted'] = 0;
			$get['join'][] = array(
					'table' => 'enrollments',
					'on' => 'enrollments.id = studentexcess_prev_sem.prev_sem_eid',
					'type' => 'left'
				);
			$get['join'][] = array(
					'table' => 'years',
					'on' => 'enrollments.year_id = years.id',
					'type' => 'left'
				);

			$this->student_excess_payments =  $this->ci->m->get_record('studentexcess_prev_sem', $get);
			return $this->student_excess_payments;
		}

		// GET ACCOUNTS WITH BALANCE
		public function get_accounts_with_balance()
		{
			unset($get);
			$get['fields'] = array(
					'enrollments.id',
					'enrollments.sy_from',
					'enrollments.sy_to',
					'years.year',
					'enrollments.total_deduction',
					'enrollments.total_scholarship',
					'enrollments.total_excess_amount',
					'total_amount_due',
					'total_amount_paid',
					'total_balance'
				);
			$get['where']['enrollments.studid'] = $this->profile->studid;
			$get['where']['enrollments.id <>'] = $this->enrollment_id;
			$get['where']['enrollments.total_balance > '] = 0;
			$get['where']['enrollments.is_deleted'] = 0;
			$get['where']['enrollments.is_drop'] = 0;
			$get['order'] = 'id';
			$get['not_in'] = array(
				'field' => 'enrollments.id',
				'data' => "SELECT spa.prev_enrollment_eid FROM student_previous_accounts spa WHERE spa.enrollment_id = $this->enrollment_id AND spa.is_deleted = 0"
				);
			$get['join'][] = array(
					'table' => 'years',
					'on' => 'enrollments.year_id = years.id',
					'type' => 'left'
				);
			$this->ci->m->set_table('enrollments');
			return $this->ci->m->get_record('enrollments', $get);
		}

		// GET ACCOUNTS WITH EXCESS
			public function get_accounts_with_excess()
			{
				unset($get);
				$get['fields'] = array(
						'enrollments.id',
						'enrollments.sy_from',
						'enrollments.sy_to',
						'years.year',
						'enrollments.total_deduction',
						'enrollments.total_scholarship',
						'enrollments.total_excess_amount',
						'total_amount_due',
						'total_amount_paid',
						'total_balance'
					);
				$get['where']['enrollments.studid'] = $this->profile->studid;
				$get['where']['enrollments.id <>'] = $this->enrollment_id;
				$get['where']['enrollments.total_balance < '] = 0;
				$get['where']['enrollments.is_deleted'] = 0;
				$get['where']['enrollments.is_drop'] = 0;
				$get['order'] = 'id';
				$get['not_in'] = array(
					'field' => 'enrollments.id',
					'data' => "SELECT sps.prev_sem_eid FROM studentexcess_prev_sem sps WHERE sps.enrollment_id = $this->enrollment_id AND sps.is_deleted = 0"
					);
				$get['join'][] = array(
						'table' => 'years',
						'on' => 'enrollments.year_id = years.id',
						'type' => 'left'
					);
				$this->ci->m->set_table('enrollments');
				return $this->ci->m->get_record('enrollments', $get);
			}	


		//ADD OTHER/CLEARANCE TO STUDENT ENROLLMENTS
			public function add_other_fee($fee_id = false)
			{
				$ret['status'] = false;
				$ret['msg'] = "Process Failed. Please Try again.";
				$ret = (object)$ret;

				//check if fee exist && not added already
				$this->ci->m->set_table('fees');
				$x_fee = $this->ci->m->pull($fee_id, array('id','name','value'));
				$this->ci->m->set_table('student_enrollment_fees');
				unset($where);
				$where['sef_enrollment_id'] = $this->enrollment_id;
				$where['fee_id'] = $fee_id;
				$where['is_deleted'] = 0;
				$x_sef = $this->ci->m->pull($where,array('sef_id'));
				
				if($x_fee && $x_sef == false)
				{
					unset($data);
					$data['sef_enrollment_id'] = $log['enrollment_id'] = $this->enrollment_id;
					$data['sef_fee_name'] = $log['fee_name'] = $x_fee->name;
					$data['sef_fee_rate'] = $log['fee_rate'] = $x_fee->value;
					$data['is_other_fee'] = 1;
					$data['is_added'] = 1;
					$data['added_by'] = $this->userid;
					$data['fee_id'] = $log['fee_id'] = $fee_id;
					$data['sef_gperiod_id'] = $this->ci->current_grading_period->id;
					$data['sef_schoolyear_id'] = $this->ci->open_semester->academic_year_id;
					
					$x_rs = (object)$this->ci->m->insert($data);
					
					if($x_rs->status)
					{
						$rs_adjusted = $this->add_other_fee_and_old_account_to_payment_totals('other_fee', $x_rs->id);
						if($rs_adjusted->status){
							$ret->status = true;
							$ret->msg = "Student Other Fee was added and fees was updated successfully.";
						}else{
							$ret->status = true;
							$ret->msg = "Student Other Fee was added but something went wrong during the updating of fees. ".$rs_adjusted->msg;
						}

						$log['Student Enrollment Fee ID'] = $x_fee->id;
						activity_log('Add Student Other Fee',$this->userlogin,'Data : '.arr_str($log));

						return $ret;	
					}
					else
					{
						$ret->status = false;
						$ret->msg = "Something went wrong. Student Other Fees was not added. Please try again";
					}
				}

				return $ret;
			}

		//REMOVE OTHER FEE
		public function delete_other_fee($sef_id = false)
			{
				$this->ci->m->set_table('student_enrollment_fees','sef_id');
				$x_sef = $this->ci->m->pull($sef_id,array('sef_id','sef_fee_name','sef_fee_rate','fee_id'));
				if($x_sef)
				{
					$log['fee_id'] = $x_sef->fee_id;
					$log['fee_name'] = $x_sef->name;
					$log['fee_rate'] = $x_sef->value;
					$log['student_enrollment_fee_id'] = $sef_id;

					unset($data);
					$data['is_deleted'] = 1;
					$data['deleted_by'] = $this->userid;
					$data['delete_date'] = date('Y-m-d g:h:s');

					$this->reverse_other_fee_and_old_account_to_payment_totals('other_fee', $sef_id);

					$this->ci->m->set_table('student_enrollment_fees','sef_id');
					$x_del = $this->ci->m->update(array('sef_id'=>$sef_id), $data);
					if($x_del)
					{
						activity_log('Delete Student Other Fee',$this->userlogin,'Data : '.arr_str($log));
						return true;
					}
				}

				return false;
			}

		// ADD STUDENT_PREVIOUS_ACCOUNTS
		public function add_previous_accounts($eid = false)
			{
				$ret = $this->load_return_default();

				if($eid === false){
					return $ret;
				}

				unset($get);
				$get['where']['id'] = $eid;
				$get['where']['total_balance > '] = 0;
				$get['single'] = true;
				$this->ci->m->set_table('enrollments');
				$sly_enrollments = $this->ci->m->get_record('enrollments', $get);
				if($sly_enrollments == false){
					$ret->msg = "This enrollment was not found or already paid";
					return $ret;
				}

				unset($data);
				$data['enrollment_id'] = $this->enrollment_id;
				$data['prev_enrollment_eid'] = $eid;
				$data['value'] = $sly_enrollments->total_balance;
				$data['amount_paid'] = 0;
				$data['is_paid'] = 0;
				$this->ci->m->set_table('student_previous_accounts');
				$rs = (object)$this->ci->m->insert($data);
				if($rs->status == false){
					$ret->msg = "Saving Failed.";
					return $ret;
				}

				activity_log('Add Student Old Account',$this->userlogin,'Data : '.arr_str($data));

				#update total fees
				$rs_adjusted = $this->add_other_fee_and_old_account_to_payment_totals('old_account', $rs->id);
				if($rs_adjusted->status){
					$ret->status = true;
					$ret->msg = "Student Old Account was added and fees was updated successfully.";
				}else{
					$ret->status = true;
					$ret->msg = "Student Old Account was added but something went wrong during the updating of fees. ".$rs_adjusted->msg;
				}

				return $ret;
			}
		
		//REMOVE OTHER FEE
		public function delete_student_previous_account($spa_id = false)
			{
				if($spa_id === false){ return false; }

				$this->ci->m->set_table('student_previous_accounts');
				$x_spa = $this->ci->m->get($spa_id);

				if($x_spa)
				{
					$log['student_previous_accounts ID'] = $x_spa->id;
					$log['Enrollment Id'] = $this->enrollment_id;
					$log['Enrollment ID Of Unsettled'] = $x_spa->prev_enrollment_eid;
					$log['Amount'] = $x_spa->value;

					unset($data);
					$data['is_deleted'] = 1;
					$data['deleted_by'] = $this->userid;
					$data['date_deleted'] = date('Y-m-d g:h:s');

					$this->reverse_other_fee_and_old_account_to_payment_totals('old_account', $spa_id);

					$this->ci->m->set_table('student_previous_accounts');
					$x_del = $this->ci->m->update($spa_id, $data);
					if($x_del)
					{
						activity_log('Delete Student previous Account',$this->userlogin,'Data : '.arr_str($log));
						return true;
					}
				}

				return false;
			}
		
		/**
		 * Count Number of Payment Record regardless if applied or not(check)
		 * Return Integer (Count)
		*/
		public function count_applied_payment()
		{
			unset($get);
			$get['where']['spr_enrollment_id'] = $this->enrollment_id;
			$get['where']['spr_is_deleted'] = 0;
			$get['count'] = true;
			$this->ci->m->set_table('student_payment_records','spr_id');
			return $this->ci->m->get_record('student_payment_records', $get);
		}

		/**
		 * Check if student made its first payment whether as CASH, SCHOLARSHIP, DEDUCTION OR EXCESS PAYMENT
		 * @param  int  $id enrollment table id
		 * @return bool true/false
		 */
		public function has_made_first_payment()
		{
			$fees = $this->get_student_fee_profile();

			$ctr = 0;
			if($fees->deductions){
				$ctr += count($fees->deductions);
			}
			if($fees->scholarships){
				$ctr += count($fees->scholarships);	
			}
			if($fees->excess_payments){
				$ctr += count($fees->excess_payments);		
			}
			if($fees->payment_record){
				$ctr += count($fees->payment_record);			
			}
			return $ctr === 1 ? true : false;
		}

		/** Student Ledger
		 *@return object and array
		 */
		public function ledger()
		{
			function sortFunction( $a, $b ) {
			    return strtotime($a["date"]) - strtotime($b["date"]);
			}

			$ledger = false;
			#STEP 1 : Get All enrollment Group by School Year ID
			#STEP 2 : Loop result of {step 1} and get enrollment again by semester and school year
			#STEP 3 : Loop result of {step 2} and get enrollment payment details
			/*
				RETURN STRUCTURE
				ARRAY[]
							SCHOOL YEAR INDEX => SCHOOL YEAR INFO OBJ
							ENROLLMENTS INDEX => ARRAY OF ENROLLMENTS[]									
																													INFO INDEX => ENROLLMENT INFO OBJECT
																													PAYMENTS INDEX => PAYMENTS ARRAY[]
																													SCHOLARSHIP INDEX => SCHOLARSHIP ARRAY[]
																													DEDUCTION INDEX => DEDUCTION ARRAY[]
																													EXCESS INDEX => EXCESS ARRAY[]
			*/
			######STEP 1#######
			$get = array(
										'fields' => 'id,sy_from, sy_to',
										'where' => array(
												'studid' => $this->profile->studid,
												'is_deleted' => 0,
											),
										'group' => 'sy_from, sy_to',
										'order' => 'id DESC'
									);
			$x_sy = $this->ci->m->get_record('enrollments', $get);
			if($x_sy){
				foreach ($x_sy as $k => $sy) {

					#####STEP 2######
					$get = array(
							'fields' => 'enrollments.id, enrollments.semester_id, semesters.name as semester, years.year,enrollments.total_plan_due',
							'where' => array(
									'enrollments.sy_from' => $sy->sy_from,
									'enrollments.sy_to' => $sy->sy_to,
									'enrollments.studid' => $this->profile->studid,
									'enrollments.is_deleted' => 0,
								),
							'join' => array(
										array(
											'table' => 'semesters',
											'on'	=> 'semesters.id = enrollments.semester_id',
											'type' => 'left'
										),array(
											'table' => 'years',
											'on'	=> 'years.id = enrollments.year_id',
											'type' => 'left'
										)
									),
							'order' => 'enrollments.id',
						);
					$x_sem = $this->ci->m->get_record('enrollments', $get);
					
					if($x_sem){

						$ledger[$k]['sy'] = $sy;

						######STEP 3######
						foreach ($x_sem as $ks => $enrollments) {
							$ledger[$k]['enrollments'][$ks]['profile'] = $enrollments;
							$ledger[$k]['enrollments'][$ks]['totals'] = $t = $this->get_totals($enrollments->id);
							$ledger[$k]['enrollments'][$ks]['payment'] = $p = $this->get_payment_record_by_id($enrollments->id);
							$ledger[$k]['enrollments'][$ks]['deduction'] = $d = $this->get_student_deduction_record($enrollments->id);
							$ledger[$k]['enrollments'][$ks]['scholarship'] = $s = $this->get_student_scholarship_record($enrollments->id);
							$ledger[$k]['enrollments'][$ks]['excess'] = $e = $this->get_student_excess_payment_record($enrollments->id);
							
							/** FOR VIEWING PURPOSE COMBINE THE ARRAY AND ORDER IT BY DATE **/
								$combined = false;
								if($p){
									foreach ($p as $k => $val) {
										$combined[] = array('date'=>$val->spr_payment_date,'value'=>$val,'type'=>'payment');
									}
								}
								if($s){
									foreach ($s as $k => $val) {
										$combined[] = array('date'=>$val->created_at,'value'=>$val,'type'=>'scholarship');
									}
								}
								if($d){
									foreach ($d as $k => $val) {
										$combined[] = array('date'=>$val->created_at,'value'=>$val,'type'=>'deduction');
									}
								}
								if($e){
									foreach ($e as $k => $val) {
										$combined[] = array('date'=>$val->created_at,'value'=>$val,'type'=>'excess');
									}
								}

								if($combined){
									usort($combined, "sortFunction");

									$ledger[$k]['enrollments'][$ks]['combined'] = $combined;
								}
						}
					}
					
				}
			}
			// vd($ledger);
			return $ledger;
		}

	/* END OF FEES FUNCTION */

	/*FUNCTION/METHODS FOR STUDENT SUBJECTS STARTS HERE*/

		/*	ADD STUDENT SUBJECT - PROCESS
			1 - add subject to studentsubjects table
			2 - recompute fee && re-apply payment if possible
			3 - update subject load
		*/
		public function add_student_subject($data = false){

			$ret['status'] = false;
			$ret['msg'] = "Process Failed. Please try again";
			$ret = (object)$ret;

			#1 - add subject to studentsubjects table
			$this->ci->m->set_table('studentsubjects');
			$rs = (object)$this->ci->m->insert($data);
			if($rs->status === false) {
				$ret->msg = "The transaction failed. The subject was not added. Please try again.";
				return $ret;
			}

			//Get SUBJECT that was added
			$this->ci->m->set_table('studentsubjects');
			$subject = $this->ci->m->get($rs->id);
			
			$this->check_if_has_payment();

			#3 - update subject load
			if($this->has_payment){
				$this->increased_subject_taken($subject->id);
			}

			#4 - recompute fees
			$rs = $this->recompute_fees("Add Subject");
			
			if($rs->status === false)
			{
				$ret->status = true;
				$ret->msg = "Student Subject was successfully added but something went wrong during the re-calculation of fees. ".$rs->msg;
			}else{
				$ret->status = true;
				$ret->msg = "Student Subject was successfully added. ".$rs->msg;
			}

			return $ret;
		}

		/*	DROPPED STUDENT SUBJECT - PROCESS
			1 - get student subject if exist , check also if coursefinance_id is present
			2 - drop subject - update is_drop = 1
			3 - update subject load - if student has payment record update subject load if not dont update
		*/
		public function drop_student_subject($ss_id = false){

			$ret['status'] = false;
			$ret['msg'] = "Process Failed";

			#1 - get student subject if exist get studentsubject
			unset($get);
			$get['fields'] = array(
					'studentsubjects.id',
					'subjects.id as subject_id',
					'master_subjects.units',
					'master_subjects.lab',
					'master_subjects.lec',
					'subjects.lab_fee_id',
					'subjects.is_nstp',
					'subjects.nstp_fee_id',
					'master_subjects.code',
					'master_subjects.subject'
				);
			$get['where']['studentsubjects.id'] = $ss_id;
			$get['where']['studentsubjects.is_deleted'] = 0;
			$get['where']['studentsubjects.is_drop'] = 0;
			$get['join'][] = array(
					'table' => 'subjects',
					'on' => 'subjects.id = studentsubjects.subject_id',
					'type' => 'left'
				);
			$get['join'][] = array(
					'table' => 'master_subjects',
					'on' => 'master_subjects.ref_id = subjects.ref_id',
					'type' => 'left'
				);
			$get['single'] = true;
			$rs_ss = $this->ci->m->get_record('studentsubjects',$get);

			if(empty($this->coursefinance_id) || $this->coursefinance_id == null){
				$ret['msg'] = "Transaction Failed. It seems the this student does not have course finance yet, please consider to recompute first";
				return (object)$ret;
			}
			
			if($rs_ss){
				#2 - drop subject - update is_drop = 1
				unset($data);
				$data['is_drop'] = 1;
				$data['dropped_by'] = $this->userid;
				$data['dropped_date'] = date('Y-m-d g:h:s');
				$this->ci->m->set_table('studentsubjects');
				$rs = $this->ci->m->update($ss_id, $data);
				if($rs == false){
					$ret['status'] = false;
					$ret['msg'] = "Process failed. Subject was not dropped. Please try again";
					return (object)$ret;
				}

				#3 - update subject load
				if($this->has_payment){
					$this->decreased_subject_taken($rs_ss->subject_id);
				}

				$ret['status'] = true;
				$ret['msg'] = "Student Subject was successfully dropped.";
								
			}	

			return (object)$ret;
		}

		/*	DELETE STUDENT SUBJECT - PROCESS
			1 - get student subject if exist , check also if coursefinance_id is present
			2 - delete subject - update is_deleted = 1
			3 - update subject load - if student has payment record update subject load if not dont update
			4 - recompute fees && re-apply payments made
		*/
		public function delete_student_subject($ss_id = false){

			$ret['status'] = false;
			$ret['msg'] = "Process Failed";

			#1 - get student subject if exist get studentsubject
			unset($get);
			$get['fields'] = array(
					'studentsubjects.id',
					'subjects.id as subject_id',
					'subjects.units',
					'subjects.lab',
					'subjects.lec',
					'subjects.lab_fee_id',
					'subjects.is_nstp',
					'subjects.nstp_fee_id',
					'subjects.code',
					'subjects.subject'
				);
			$get['where']['studentsubjects.id'] = $ss_id;
			$get['where']['is_deleted'] = 0;
			$get['where']['is_drop'] = 0;
			$get['join'][] = array(
					'table' => 'subjects',
					'on' => 'subjects.id = studentsubjects.subject_id',
					'type' => 'left'
				);
			$get['single'] = true;
			$rs_ss = $this->ci->m->get_record('studentsubjects',$get);

			if(empty($this->coursefinance_id) || $this->coursefinance_id == null){
				$ret['msg'] = "Transaction Failed. It seems the this student does not have course finance yet, please consider to recompute first";
				return (object)$ret;
			}
			
			if($rs_ss){
				#2 - delete subject - update is_deleted = 1
				unset($data);
				$data['is_deleted'] = 1;
				$data['deleted_by'] = $this->userid;
				$data['delete_date'] = date('Y-m-d g:h:s');
				$this->ci->m->set_table('studentsubjects');
				$rs = $this->ci->m->update($ss_id, $data);
				if($rs == false){
					$ret['status'] = false;
					$ret['msg'] = "Process failed. Subject was not removed. Please try again";
					return (object)$ret;
				}

				$this->check_if_has_payment();

				#3 - update subject load
				if($this->has_payment){
					$this->decreased_subject_taken($rs_ss->subject_id);
				}

				#4 - recompute fees
				$rs = $this->recompute_fees("Delete Subject");

				if($rs->status)
				{
					$ret['status'] = true;
					$ret['msg'] = "Student Subject was successfully removed and student fees was recomputed.";
				}else{
					$ret['status'] = false;
					$ret['msg'] = "Student Subject was successfully removed but something went wrong during the re-calculation of fees. ".$rs->msg;
				}
								
			}	

			return (object)$ret;
		}

		private function increased_subject_taken($subject_id)
		{
			unset($get);
			$get['where']['id'] = $subject_id;
			$get['single'] = true;
			$this->ci->m->set_table('subjects');
			$subj = $this->ci->m->pull($subject_id, array('id','subject_taken'));
			if($subj){

				unset($data);
				$data['subject_taken'] = $subj->subject_taken + 1;
				$rs = $this->ci->m->update($subject_id, $data);
				return $this->ci->db->affected_rows() > 0 ? true : false;
			}

			return false;
		}

		private function decreased_subject_taken($subject_id)
		{
			unset($get);
			$get['where']['id'] = $subject_id;
			$get['single'] = true;
			$this->ci->m->set_table('subjects');
			$subj = $this->ci->m->pull($subject_id, array('id','subject_taken'));
			if($subj){
				if($subj->subject_taken > 0){
					unset($data);
					$data['subject_taken'] = $subj->subject_taken - 1;
					$rs = $this->ci->m->update($subject_id, $data);
					return $this->ci->db->affected_rows() > 0 ? true : false;
				}
			}

			return false;
		}

		private function update_all_subject_taken($type)
		{
			if($this->student_subjects)
			{
				foreach ($this->student_subjects as $key => $value) {
					if($value->is_drop == 1){ continue; } #exclude dropped subjects, already updated subject taken when it was dropped
					if($type=="ADD"){
						$rs = $this->increased_subject_taken($value->subject_id);
					}else if($type == "MINUS"){
						$rs = $this->decreased_subject_taken($value->subject_id);
					}else{}	
				}
			}
		}

		/*update subject load - if first payment update all subjects load - meaning reserve na sya*/
		public function reserve_all_student_subject()
		{
			$yes = $this->has_made_first_payment();
			if($yes)
			{
				$this->update_all_subject_taken('ADD');
			}
		}
	/*END OF FUNCTIONS FOR STUDENT SUBJECTS*/

	/* STUDENT SUBJECT GRADE FUNCTIONS */

		/** GET THE STUDENT GRADES - IF NO RECORD FOUND CREATE RECORD FIRST
		 * @param int @user_id - If get the specific subjects under the teacher userid
		 * @return OBJECTS / false
		 */
		public function get_student_subject_grades($user_id = false, $gp_id = false){

			if($user_id){ $this->load_subjects($user_id); }

			$this->create_student_grades_file();
			
			$this->student_subject_grades = false;
			$grades = false;

			unset($get);
			$get['fields'] = array('*');
			if($this->student_subjects){
				foreach ($this->student_subjects as $k => $v) {
					// vd($v);
					unset($get);//get grades_file record
					$get['fields'] = array(
							'grades_file.id',
							'grades_file.gp_id',
							'grading_periods.grading_period',
							'grades_file.type',
							'grades_file.raw',
							'grades_file.value',
							'grades_file.converted',
							);
					$get['where']['enrollment_id'] = $this->enrollment_id;
					$get['where']['ss_id'] = $v->id;

					if($gp_id !== false){
						//Add Grading
						$get['where']['grades_file.gp_id'] = $gp_id;
						// $get['single'] = true;
					}

					$get['where']['grades_file.is_deleted'] = 0;
					$get['join'][] = array(
							'table' => 'grading_periods',
							'on' => 'grading_periods.id = grades_file.gp_id',
							'type' => 'left'
						);
					$get['order'] = 'grades_file.position';

					$grade = $this->ci->m->get_record('grades_file', $get);

					$grades[] = array(
							'subject' => $v,
							'grade' => $grade,
						);
				}			
			}

			# Arrange Grades for viewing - add grading period for table header
			# reason - grading period is dynamic and the student grade is attach to it.
			$gps = false;
			if($grades){
				foreach ($grades as $k => $ss) {
					$grade = $ss['grade'];
					//get grading period and break
					
					if($grade){
						foreach ($grade as $g => $p) {
							$gps[$p->gp_id] = $p->grading_period;
						}
						break;
					}
				}
			}

			$this->student_subject_grades = new StdClass;
			$this->student_subject_grades->periods = $gps;
			$this->student_subject_grades->subjects = $grades;
			
			return $this->student_subject_grades;
		}

		/** Create Student Grades File
			** Get Subjects and create a record in grades_file table
			** Check for New Subjects Added and add them
			** Check for New Period and Add them
			**/
		public function create_student_grades_file(){
			
			unset($get); // Get Student Subjects
			$get['fields'] = array('id');
			$get['where']['enrollment_id'] = $this->enrollment_id;
			$get['where']['is_deleted'] = 0;
			$get['where']['is_drop'] = 0;
			$get['not_in'] = array(
					'field' => 'id',
					'data' => "SELECT ss_id FROM grades_file WHERE enrollment_id = $this->enrollment_id AND is_deleted = 0"
				);

			$subjects = $this->ci->m->get_record('studentsubjects', $get, false);
			if(!$subjects){ return false; }
			
			unset($get); // get grading periods
			$get['fields'] = array('id');
			$get['order'] = 'orders';
			$get['where']['is_deleted'] = 0;
			$gp = $this->ci->m->get_record('grading_periods', $get);
			if(!$gp){ return false; }
			$ctr = 0;
			foreach ($subjects as $k => $s) {
				
				$pos = 0;
				foreach ($gp as $g => $p) {
					unset($data);
					$data['enrollment_id'] = $this->enrollment_id;
					$data['ss_id'] = $s->id;
					$data['gp_id'] = $p->id;
					$data['type'] = "GRADE";
					$data['position'] = ++$pos;
					$this->ci->m->set_table('grades_file');
					$rs = $this->ci->m->insert($data);
					if($rs){
						$ctr++;
					}
				}
			}
			
			return $ctr > 0 ? TRUE : FALSE;
		}

		/** Get Grades of Specific Subject
		 * @param $subject_id
		 * @param $gp_id Grading Period ID
		 * @return array of student grades
		 */
		public function get_subject_grades($subject_id, $gp_id = false)
		{
			//Find the subject in studentsubjects
			$get = array(
					'fields' => 'id, remarks',
					'where' => array(
							'enrollment_id' => $this->enrollment_id,
							'subject_id' => $subject_id,
							'is_deleted' => 0,
						),
					'single' =>  true
				);
			$rs_ss = $this->ci->m->get_record('studentsubjects', $get);
			if(!$rs_ss){ return false; }

			unset($get);//get grades_file record
			if($gp_id){
				$get['where']['grades_file'] = $gp_id;
				$get['single'] = true;
			}

			$get['fields'] = array(
					'grades_file.id',
					'grades_file.gp_id',
					'grading_periods.grading_period',
					'grades_file.type',
					'grades_file.raw',
					'grades_file.value',
					'grades_file.converted'
					);
			$get['where']['grades_file.enrollment_id'] = $this->enrollment_id;
			$get['where']['grades_file.ss_id'] = $rs_ss->id;
			$get['where']['grades_file.is_deleted'] = 0;
			$get['join'][] = array(
					'table' => 'grading_periods',
					'on' => 'grading_periods.id = grades_file.gp_id',
					'type' => 'left'
				);
			$get['order'] = 'grades_file.position';

			return array(
					'subject_profile' => $rs_ss,
					'subject_grade' => $this->ci->m->get_record('grades_file', $get)
				);
		}

	/* END OF STUDENT SUBJECT GRADE FUNCTIONS */

	/*HELPER FUNCTIONS TO BE PUT HERE*/
		private function positive_val($value=0){
			$ret = floatval($value);

			if($ret < 0){
				return 0;
			}

			return $ret;
		}

		private function num_to_th($num = false)
		{
			$ret = "";
			if($num && $num > 0){
				
				switch($num)
				{
					case 1;
						$ret = "1st";
					break;
					case 2;
						$ret = "2nd";
					break;
					case 3;
						$ret = "3rd";
					break;
					case 4;
						$ret = "4th";
					break;
					case 5;
						$ret = "5th";
					break;
					case 6;
						$ret = "6th";
					break;
					case 7;
						$ret = "7th";
					break;
					case 8;
						$ret = "8th";
					break;
					case 9;
						$ret = "9th";
					break;
					case 10;
						$ret = "10th";
					break;
					case 11;
						$ret = "11th";
					break;
					case 12;
						$ret = "12th";
					break;
					default:
						$ret = "";
					break;
				}
				
			}
			return $ret;
		}

		/**
		 * Get Academic Year Id by enrollment_id
		 *@param int $id Enrollment Id
		 *@return int academic year id
		 */
		private function get_sy_id($id)
		{
			$get = array(
					'fields' => 'sy_from, sy_to',
					'where' => array('id'=>$id),
					'single' => true
				);
			$x_en = $this->ci->m->get_record('enrollments', $get);
			if ($x_en) {
				
				$get = array(
						'fields' => 'id',
						'where' => array('year_from'=>$x_en->sy_from,'year_to'=>$x_en->sy_to),
						'single' => true
					);
				$x_ay = $this->ci->m->get_record('academic_years', $get);
				if($x_ay){
					return $x_ay->id;
				}
			}

			return false;
		}

		/**
		 * return student payment totals
		 * @return object
		 */
		private function get_totals($id = false)
		{
			unset($get);
			$get['fields'] = array(
					'enrollments.*',
					"DATE_FORMAT(enrollments.date_of_birth,'%m/%d/%Y') as date_of_birth",
					'enrollments.guardian as guardian_name',
					'enrollments.fake_email as email',
					'enrollments.fname as first_name',
					'enrollments.name as full_name',
					'enrollments.middle as middle_name',
					'enrollments.lastname as last_name',
					'enrollments.sex as gender',
					'enrollments.id as enrollment_id',
					'courses.course',
					'years.year', 
					'years.id as year_id',
					'semesters.id as sem_id',
					'semesters.name',
					'users.login',
					'payment_plan.name as payment_plan',
					'payment_plan.division as payment_division'
			);
			$get['where']['enrollments.id'] = !$id ? $this->enrollment_id : $id;
			$get['join'] = array(
					array(
					"table" => "courses",
					"on"	=> "courses.id = enrollments.course_id",
					"type"  => "LEFT"
					),
					array(
					"table" => "years",
					"on"	=> "years.id = enrollments.year_id",
					"type"  => "LEFT"
					),
					array(
					"table" => "semesters",
					"on"	=> "semesters.id = enrollments.semester_id",
					"type"  => "LEFT"
					),
					array(
					"table" => "users",
					"on"	=> "users.id = enrollments.user_id",
					"type"  => "LEFT"
					),
					array(
					"table" => "payment_plan",
					"on"	=> "payment_plan.id = enrollments.payment_plan_id",
					"type"  => "LEFT"
					)
				);
			$get['single'] = true;
			$profile = $this->ci->m->get_record('enrollments',$get);
		
			if($profile){

				//LOAD Payments total as object
				$student_payment_totals = new StdClass;
				$student_payment_totals->total_plan_due = $profile->total_plan_due; //Total PLan Amount
				$student_payment_totals->total_deduction = $profile->total_deduction;
				$student_payment_totals->total_scholarship = $profile->total_scholarship;
				$student_payment_totals->total_excess_amount = $profile->total_excess_amount;
				$student_payment_totals->total_deduction_amount = $profile->total_deduction + $profile->total_scholarship + $profile->total_excess_amount;

				$student_payment_totals->total_previous_amount = $profile->total_previous_amount;
				$student_payment_totals->total_amount_due = $profile->total_amount_due; // Total Plan - All deductions
				$student_payment_totals->total_amount_paid = $profile->total_amount_paid; // Total Amoun Paid
				$student_payment_totals->total_balance = $profile->total_balance;
				$student_payment_totals->payment_status = $profile->payment_status;

				return $student_payment_totals;
			}

			return false;
		}

		/** Get Subject Profile
		 * @param $id Subject ID
		 * @return object
		 */
		private function get_subject_profile($id)
		{
			$sql = "
				SELECT
				ms.ref_id,
				ms.code,
				ms.subject,
				ms.units,
				ms.lab,
				ms.lec,
				s.id,
				s.time,
				s.is_open_time,
				s.day,
				s.course_id,
				s.room_id,
				r.name as room,
				s.year_id,
				s.semester_id,
				s.lab_fee_id,
				s.original_load,
				s.subject_load,
				s.subject_taken,
				s.year_from,
				s.year_to,
				s.academic_year,
				s.teacher_user_id,
				s.class_start,
				s.class_end,
				s.hour,
				s.min,
				s.is_nstp,
				u.name as teacher
				FROM subjects s 
				LEFT JOIN master_subjects ms ON ms.ref_id = s.ref_id
				LEFT JOIN users u ON u.id = s.teacher_user_id
				LEFT JOIN rooms r ON r.id = s.room_id
				WHERE s.id = ?
			";
			return $this->ci->m->query($sql, array($id), true);
		}
	/* END OF HELPER FUNCTIONS */
}

?>