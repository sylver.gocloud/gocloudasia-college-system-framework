<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/*
checks and validates session
*/
class Session_checker
{
	private $ci;
	
	public function __construct()
	{

		$this->ci =& get_instance();
	}
	
	
	public function open_semester()
	{
		$this->ci->load->model('M_open_semesters');
		if(isset($this->ci->session->userdata['userid']) || !empty($this->ci->session->userdata['userid']))
		{
			$id = $this->ci->session->userdata['userid'];
			$existingUserId = $this->ci->M_open_semesters->get_open_semester_userId($id);
			if($existingUserId)
			{
				
			}
			else{
				// redirect('open_semester_employee_settings/create/'.$id);
				#create OPEN SEMESTER FOR THE USER
				$this->ci->load->model('M_open_semester_employee_settings','m_os');
				unset($data);
				if($this->ci->cos->system){
					$data['user_id'] = $id;
					$data['open_semester_id'] = $this->ci->cos->system->open_semester_id;
					$rs = $this->ci->m_os->insert($data);
				}
			}
		}	
	}
	
	public function verify_user($id)
	{
		if(empty($id) or ctype_digit($id) == FALSE)
		{
			redirect(base_url());
		}else{
			$this->ci->load->model('M_users');
			$user = $this->ci->M_users->get_profile($id);
			$user_ = $this->ci->M_users->get_user_by_id($id);
			if($user){

				// $this->check_system_setup(); //CHECK system if properly setup (My_controller)

				$this->ci->session->set_flashdata('system_message', '<div class="alert alert-success"><b><span class="glyphicon glyphicon-ok"></span>&nbsp; Welcome '.$user_->name.'. You have successfully logged in.</b></div>');
				redirect('home');
			}else{
				redirect(base_url());
			}
		}	
	}
	
	public function check_if_alive($check = false)
	{
		if($check == false)
		{
			if(!isset($this->ci->session->userdata['userid']) || empty($this->ci->session->userdata['userid']))
			{
				$this->ci->session->set_flashdata('system_message', 'You have been forced <strong>logout</strong> because you have been idle for too long or you do not have access to the page. Please re-consider to re-login');
				redirect('auth/logout');
			}
		}else{
			if(!isset($this->ci->session->userdata['userid']) || empty($this->ci->session->userdata['userid']))
			{
				return FALSE;
			}else{
				return TRUE;
			}
		}
	}
	
	/*
	* secure_page
	* secure specific controller or method by letting only specific usertypes access it
	*
	*/
	public function secure_page($usertype, $except_user = false)
	{
		if($this->check_if_alive(true))
		{
			$current_session = $this->ci->session->userdata('userType');
			if(is_array($usertype))
			{
				if(in_array($current_session,$usertype))
				{
					if($except_user != false){
						
						if(is_array($except_user)) //IF EXCEPT USER IS ARRAY
						{
							if(in_array($current_session,$except_user))
							{
								//IF CURRENT USER IS IN THE EXCEPT USER
								redirect($current_session);
							}
						}
						else
						{
							if(strtolower($current_session) == strtolower($except_user)){
								//IF CURRENT USER IS EQUAL TO THE EXCEP USERS
								redirect($current_session);
							}
						}
						
					}
				}else{
					redirect($current_session);
				}
			}else{
				if($usertype == $current_session OR strtolower($usertype) == 'all')
				{
					if($except_user != false){
						
						if(strtolower($usertype) == 'all') //ALL EXFCEPT
						{
							
							if(is_array($except_user)) //IF EXCEPT USER IS ARRAY
							{
								if(in_array($current_session,$except_user))
								{
									//IF CURRENT USER IS IN THE EXCEPT USER
									redirect($current_session);
								}
							}
							else
							{
								if(strtolower($current_session) == strtolower($except_user)){
									//IF CURRENT USER IS EQUAL TO THE EXCEP USERS
									redirect($current_session);
								}
							}
						}
						else
						{
							if(is_array($except_user)) //IF EXCEPT USER IS ARRAY
							{
								if(in_array($current_session,$except_user))
								{
									//IF CURRENT USER IS IN THE EXCEPT USER
									redirect($current_session);
								}
							}
							else
							{
								if(strtolower($current_session) == strtolower($except_user)){
									//IF CURRENT USER IS EQUAL TO THE EXCEP USERS
									redirect($current_session);
								}
							}
						}
					}
				}else{
					redirect($current_session);
				}
			}
			return TRUE;
		}
		else
		{
			show_404();
		}
	}
}
