<?php

class MY_Model extends CI_Model
{
	protected $_table = '';
	protected $before_create = array();
	protected $after_create = array();

	public function __construct()
	{
		parent::__construct();
		$this->load->database();

#		$this->load->helper('inflector');

#		if ( ! $this->_table)
#		{
#			
#			$this->_table = strtolower(plural(str_replace('m_', '', get_class($this))));
#		}
	}

	public function get()
	{
		$args = func_get_args();
	
		if (count($args) > 1 || is_array($args[0]))
		{
			$this->db->where($args);
		}
		else
		{
			$this->db->where('id', $args[0]);
		}

		return $this->db->get($this->_table)->row();
	}
	
	public function get_all()
	{
		$args = func_get_args();
	
		if (count($args) > 1 || is_array($args[0]))
		{
			$this->db->where($args);
		}
		else
		{
			$this->db->where('id', $args[0]);
		}
		
		return $this->db->get($this->_table)->result();
	}
	
	public function insert($data, $skip_validation = FALSE)
	{
		$data['created_at'] = $data['updated_at'] = date('Y-m-d H:i:s');

		$data = $this->observe('before_create', $data);

		if (!$skip_validation && !$this->validate($data))
		{
			$success = FALSE;
		}
		else
		{
			$success = $this->db->insert($this->_table, $data);
		}

		if ($success)
		{
			$this->observe('after_create', $data);

			return $this->db->insert_id();
		}
		else
		{
			return FALSE;
		}
	}
	
	public function update()
	{
		$args = func_get_args();
		$args[1]['updated_at'] = date('Y-m-d H:i:s');

		if (is_array($args[0]))
		{
			$this->db->where($args);
		}
		else
		{
			$this->db->where('id', $args[0]);
		}
		
		return $this->db->update($this->_table, $args[1]);
	}
	
	public function delete()
	{
		$args = func_get_args();
	
		if (count($args) > 1 || is_array($args[0]))
		{
			$this->db->where($args);
		}
		else
		{
			$this->db->where('id', $args[0]);
		}

		return $this->db->delete($this->_table);
	}

	public function observe($event, $data)
	{
		if (isset($this->$event) && is_array($this->$event))
		{
			foreach ($this->$event as $method)
			{
				$data = call_user_func_array(array($this, $method), array($data));
			}
		}
		
		return $data;
	}

	public function validate($data)
	{
		if (!empty($this->validate))
		{
			foreach ($data as $key => $value)
			{
				$_POST[$key] = $value;
			}

			$this->load->library('form_validation');
			$this->form_validation->set_rules($this->validate);

			return $this->form_validation->run();
		}
		else
		{
			return TRUE;
		}
	}
	
	
	/*table - table name
	@start - start record for pagination
	@limit - limit record for pagination
	@filter (array) - add filter or where
			//index - add operator ex filter['id =']
			//value - value of the filter ex filter['id ='] = 100
	@order_by (string) - order or the record
	@all - exclude the limit and start return all records 
	@count - return the count or all records
	@par - addition parameter for future purpose
	*/
	
	public function fetch_record($start=0,$limit=100, $filter = false,$order_by = false, $all = false, $ret_count = false, $par = ""){
		
		$start = $this->db->escape_str($start);
		$limit = $this->db->escape_str($limit);
		$ci =& get_instance();
		
		//GET Library FIELDS
		$sql = "DESCRIBE $this->_table";
		$query = $this->db->query($sql);
		$fields = $query->result();
		$fields_array = array();
		foreach($fields as $val){
			
			$fields_array[] = $this->_table.'.'.$val->Field;
		}
		
		//ADD FILTERS
		// $fields_array[] = 'librarycategory.category';
		
		$param = array();
		if($filter != false){
			//if filter is array
			
			if(is_array($filter)){
				foreach($filter as $key => $value){
		
					$param[$key] = $value;
				}
			}
		}
		
		$this->db->select($fields_array);
		$this->db->from($this->_table);
		$this->db->where($param);
		if($order_by == false)
		{
			$this->db->order_by("id", "ASC"); 
		}else{
			$this->db->order_by($order_by); 
		}
		// $this->db->join('librarycategory', 'librarycategory.id = '.$this->_table.'.librarycategory_id','LEFT');
		
		
		if($all == false){
			$this->db->limit($limit, $start);
		}
		
		$query = $this->db->get();
		
		if($ret_count == false){
			return $query->num_rows() > 0 ? $query->result() : FALSE;
		}else{
			return $query->num_rows();
		}
	}
	
	/* Dynamic Get Record */
	/* table - table name */
	/* config (array) - filter, like, order, group, fields */
	public function get_record($table = false, $config = array(), $debug = false)
	{
		if($table == false)
		{
			$table = $this->_table;
		}
		
		//CONFIGURATION
		//$config['fields'] = specific fields
		//$config['where'] = AND conditions
		//$config['like'] = LIKE conditions
		// $config['join'][] = array(
			// "table" = "TABLE NAME",
			// "on"	= "ON STRING",
			// "type"  = "LEFT,RIGHT"
		// )
		//$config['group'] = GROUP BY conditions
		//$config['order'] = ORDER BY conditions
		//$config['start'] = LIMIT START conditions
		//$config['limit'] = LIMIT END conditions
		//$config['all'] = true or false : return all removes limit
		//$config['count'] = true or false : return count not the row
		//$config['array'] = true or false : return array instead of object
		//$config['single'] = true or false : return single record
		
		
		//FIELDS CONFIGURATION
		if(isset($config['fields']) && $config['fields'] != false)
		{
			$this->db->select($config['fields']);
		}
		else
		{
			$this->db->select('*');
		}
		
		$this->db->from($table); //FROM TABLE
		
		//WHERE
		if(isset($config['where']) && $config['where'] != false)
		{
			$this->db->where($config['where']);
		}
		
		//LIKE
		if(isset($config['like']) && $config['like'] != false)
		{
			$this->db->like($config['like']);
		}
		
		//JOIN STATEMENTS
		if(isset($config['join']) && is_array($config['join']))
		{
			foreach($config['join'] as $join)
			{
				if($join['table'] != "")
				{
					$this->db->join($join['table'], $join['on'],strtoupper($join['type']));
				}
			}
		}
		
		//GROUP
		if(isset($config['group']) && $config['group'] != false)
		{
			$this->db->group_by($config['group']);
		}
		
		//ORDER BY
		if(isset($config['order']) && $config['order'] != false)
		{
			$this->db->order_by($config['order']);
		}
		
		//CHECK IF ALL IF TRUE
		if(isset($config['all']) && $config['all'] == true)
		{
		}
		else
		{
			//LIMIT START END
			if(isset($config['limit']) && isset($config['start']))
			{
				$this->db->limit($config['limit'], $config['start']);
			}
			else
			{
				if(isset($config['limit']))
				{
					$this->db->limit($config['limit']);
				}
			}
		}
		
		$query = $this->db->get(); //EXECUTE QUERY
		
		//CHECK IF DEBUG
		if($debug)
		{
			vp($this->db->last_query());
			vd('');
		}
		
		//CHECK IF COUNT TRUE
		if(isset($config['count']) && $config['count'] == true)
		{
			return $query->num_rows();
		}
		else
		{		
			//CHECK IF SINGLE
			if(isset($config['single']) && $config['single'] == true)
			{
				if(isset($config['array']) && $config['array'] == true)
				{
					return $query->num_rows > 0 ? $query->first_row('array') : false;
				}
				else
				{
					return $query->num_rows > 0 ? $query->row() : false;
				}
			}
			else
			{
				if(isset($config['array']) && $config['array'] == true)
				{
					return $query->num_rows > 0 ? $query->result_array : false;
				}
				else
				{
					return $query->num_rows > 0 ? $query->result() : false;
				}
			}
		}
		
		return false;
	}
}
