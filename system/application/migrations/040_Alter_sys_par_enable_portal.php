<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Alter_sys_par_enable_portal extends CI_Migration {

	public function up() 
	{
		if($this->db->table_exists('sys_par'))
		{
			if (!$this->db->field_exists('enable_portal', 'sys_par'))
			{
				$this->db->query("ALTER TABLE `sys_par` ADD COLUMN `enable_portal` TINYINT(1) DEFAULT 1");
			}
		}	
	} 
	public function down() 
	{
		if ($this->db->field_exists('enable_portal', 'sys_par'))
		{
			$this->dbforge->drop_column('sys_par', 'enable_portal');
		}
	}
} 