<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update_issues extends CI_Migration {

	public function up() 
	{
		if($this->db->table_exists('issues'))
		{
			$this->db->query("ALTER TABLE `issues` ADD COLUMN `enrollment_id` INT(11) NULL AFTER `user_id`, ADD COLUMN `issued_id` VARCHAR(11) NULL COMMENT 'User Id of Who Issued' AFTER `issued_by`, ADD COLUMN `resolved_id` VARCHAR(11) NULL COMMENT 'User ID of Who Resolved' AFTER `issued_id`, ADD COLUMN `resolved_date` DATETIME NULL AFTER `resolved_id`, ADD COLUMN `resolved_remark` TEXT NULL;");
		}		
	} 
	public function down() 
	{
		if($this->db->table_exists('issues'))
		{
			if ($this->db->field_exists('enrollment_id', 'issues'))
			{
				$this->dbforge->drop_column('issues', 'enrollment_id');
			}

			if ($this->db->field_exists('issued_id', 'issues'))
			{
				$this->dbforge->drop_column('issues', 'issued_id');
			}

			if ($this->db->field_exists('resolved_id', 'issues'))
			{
				$this->dbforge->drop_column('issues', 'resolved_id');
			}

			if ($this->db->field_exists('resolved_date', 'issues'))
			{
				$this->dbforge->drop_column('issues', 'resolved_date');
			}

			if ($this->db->field_exists('resolved_remark', 'issues'))
			{
				$this->dbforge->drop_column('issues', 'resolved_remark');
			}
		}
	}
} 
