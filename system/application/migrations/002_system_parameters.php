<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_System_parameters extends CI_Migration {

	/**
	 * Create a data
	 */

	private $_table = "sys_par";

	public function up(){
		
		if($this->db->table_exists($this->_table)){	

			$this->load->model('M_core_model','m');
			$this->m->set_table($this->_table);

			$syspar = $this->m->get_record(false);

			if(!$syspar){

				unset($data);
				$data['tmp_studid_series'] = "00000001";
				$data['studid_series'] = "00000001";
				$data['subj_refid_series'] = "0000000001";
				$data['is_auto_id'] = "1";
				$data['bootstrap_theme'] = "YETI";
				$data['reg_success_msg'] = "Your have been successfully registered. Thank you.";
				$data['is_menu_accordion'] = "0";
				$data['show_menu'] = "1";
				$data['application_layout'] = "application";
				$data['welcome_layout'] = "welcome";

				$rs = $this->m->insert($data);

			}
		}
	}

	public function down(){

		if($this->db->table_exists($this->_table)){	

			$this->load->model('M_core_model','m');
			$this->m->set_table($this->_table);

			$syspar = $this->m->get_record(false);
			
			if($syspar){

				$sql = "TRUNCATE $this->_table";
				$this->db->query($sql);
			}
		}
	}
}