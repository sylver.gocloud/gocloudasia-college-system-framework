<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update_student_subject_add_final_grade extends CI_Migration {

	public function up() 
	{
		if($this->db->table_exists('studentsubjects'))
		{
			$fields = array(
              'final_grade' => array(
							'type' => 'VARCHAR',
							'constraint' => '50'
						),
			);

			if (!$this->db->field_exists('final_grade', 'studentsubjects'))
			{
				$this->dbforge->add_column('studentsubjects', $fields);
			}
		}	
	}

	public function down() 
	{
		if($this->db->table_exists('studentsubjects'))
		{
			if ($this->db->field_exists('final_grade', 'studentsubjects'))
			{
				$this->dbforge->drop_column('studentsubjects', 'final_grade');
			}
		}	
	}
}