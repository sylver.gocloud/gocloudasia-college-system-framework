<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Alter_enrollments_add_ref_num extends CI_Migration {

	public function up(){
		
		if($this->db->table_exists("enrollments")){
			if (!$this->db->field_exists('e_ref_num', 'enrollments')){
				$this->db->query("ALTER TABLE `enrollments` ADD COLUMN `e_ref_num` VARCHAR(15) NOT NULL AFTER `studid`");
			}
		}
	}
	public function down(){

		if($this->db->table_exists("enrollments")){
			if ($this->db->field_exists('e_ref_num', 'enrollments')){
				$this->dbforge->drop_column('enrollments', 'e_ref_num');
			}
		}
	}
}  