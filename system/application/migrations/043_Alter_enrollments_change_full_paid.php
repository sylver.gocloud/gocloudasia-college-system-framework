<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Alter_enrollments_change_full_paid extends CI_Migration {

	public function up(){
		
		if($this->db->table_exists("enrollments")){
			$this->db->query("ALTER TABLE `enrollments` CHANGE `payment_status` `payment_status` SET('UNPAID','FULLY PAID','PARTIAL PAID')");
		}
	}
	public function down(){
	}
}  