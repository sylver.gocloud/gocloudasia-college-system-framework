<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update_syspar_add_show_menu_tip extends CI_Migration {

	public function up() 
	{
		if($this->db->table_exists('sys_par'))
		{
			if (!$this->db->field_exists('show_menu_tip', 'sys_par'))
			{
				$this->db->query("ALTER TABLE `sys_par` ADD COLUMN `show_menu_tip` TINYINT(1) DEFAULT 0 NULL AFTER `is_setup`;");
			}
		}	
	} 
	public function down() 
	{
		if ($this->db->field_exists('show_menu_tip', 'sys_par'))
		{
			$this->dbforge->drop_column('sys_par', 'show_menu_tip');
		}
	}
} 