 <?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Alter_coursefees_alter_value extends CI_Migration {

	public function up(){
		
		if($this->db->table_exists("coursefees")){
			if ($this->db->field_exists('value', 'coursefees')){
				$this->db->query("ALTER TABLE `coursefees` CHANGE `value` `value` FLOAT(11) NULL");
			}
		}
	}
	public function down(){

	}
}  