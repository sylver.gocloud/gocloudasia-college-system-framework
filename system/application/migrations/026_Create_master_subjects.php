<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class Migration_Create_master_subjects extends CI_Migration {
	public function up(){
		
		if(!$this->db->table_exists("master_subjects")){
			$this->dbforge->add_field("`id` int(11) NOT NULL AUTO_INCREMENT");
			$this->dbforge->add_field("`ref_id` varchar(25) NOT NULL COMMENT 'Unique ID of Every Subject, To Determine Same subject in every school year'");
			$this->dbforge->add_field("`code` varchar(255) DEFAULT NULL");
			$this->dbforge->add_field("`subject` varchar(255) DEFAULT NULL");
			$this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
			$this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
			$this->dbforge->add_field("`created_by` int(12) DEFAULT NULL COMMENT 'Table User Id'");
			$this->dbforge->add_field("`units` float(11,2) NOT NULL DEFAULT '0.00'");
			$this->dbforge->add_field("`lec` varchar(255) DEFAULT NULL");
			$this->dbforge->add_field("`lab` varchar(255) DEFAULT NULL");
			$this->dbforge->add_field("`year_from` varchar(255) DEFAULT NULL COMMENT 'Year From Created'");
			$this->dbforge->add_field("`year_to` varchar(255) DEFAULT NULL COMMENT 'Year To Created'");
			$this->dbforge->add_field("`ay_id` varchar(255) DEFAULT NULL COMMENT 'Academic Year Created'");
			$this->dbforge->add_field("`is_deleted` tinyint(1) DEFAULT '0'");
			$this->dbforge->add_field("`deleted_by` int(12) DEFAULT NULL COMMENT 'Table User Id'");
			$this->dbforge->add_key('`id`', TRUE);
			$this->dbforge->create_table("master_subjects");
		}
	}
	public function down(){
		if($this->db->table_exists("master_subjects")){
			$this->dbforge->drop_table("master_subjects");
		}
	}
}