<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update_grading_period_add_is_deleted extends CI_Migration {

	public function up() 
	{
		if($this->db->table_exists('grading_periods'))
		{
			$fields = array(
              'is_deleted' => array(
								'type' => 'INT',
								'constraint' => '1',
								'default' => '0'
							),
			);

			if (!$this->db->field_exists('is_deleted', 'grading_periods'))
			{
				$this->dbforge->add_column('grading_periods', $fields);
			}

			$fields = array(
              'deleted_by' => array(
								'type' => 'INT',
								'constraint' => '12',
								'null' => true
							),
			);

			if (!$this->db->field_exists('deleted_by', 'grading_periods'))
			{
				$this->dbforge->add_column('grading_periods', $fields);
			}

			$fields = array(
        'date_deleted' => array(
					'type' => 'DATETIME',
					'null' => true
				),
			);

			if (!$this->db->field_exists('date_deleted', 'grading_periods'))
			{
				$this->dbforge->add_column('grading_periods', $fields);
			}
		}	
	}

	public function down() 
	{
		if($this->db->table_exists('grading_periods'))
		{
			if ($this->db->field_exists('is_deleted', 'grading_periods'))
			{
				$this->dbforge->drop_column('grading_periods', 'is_deleted');
			}
			if ($this->db->field_exists('deleted_by', 'grading_periods'))
			{
				$this->dbforge->drop_column('grading_periods', 'deleted_by');
			}
			if ($this->db->field_exists('date_deleted', 'grading_periods'))
			{
				$this->dbforge->drop_column('grading_periods', 'date_deleted');
			}
		}	
	}
}