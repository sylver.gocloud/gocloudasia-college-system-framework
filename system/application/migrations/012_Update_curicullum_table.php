<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update_curicullum_table extends CI_Migration {

	public function up() 
	{
		if($this->db->table_exists('curriculum'))
		{
			$fields = array(
              'is_active' => array(
								'type' => 'INT',
								'constraint' => '1',
								'default' => '0'
							),
			);

			if (!$this->db->field_exists('is_active', 'curriculum'))
			{
				$this->dbforge->add_column('curriculum', $fields);
			}
		}	
	}

	public function down() 
	{
		if($this->db->table_exists('curriculum'))
		{
			if ($this->db->field_exists('is_active', 'curriculum'))
			{
				$this->dbforge->drop_column('curriculum', 'is_active');
			}
		}	
	}
}