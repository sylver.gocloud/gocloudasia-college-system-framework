<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Insert_default_users extends CI_Migration {

	/**
	 * Create a data
	 */
	private $_table = "users";
	private $_users = array(
				"admin===admin-1" => "insert into `users` (`login`, `name`, `email`, `crypted_password`, `salt`, `created_at`, `updated_at`, `remember_token`, `remember_token_expires_at`, `activation_code`, `activated_at`, `is_activated`, `admin`, `student`, `employee`, `employee_id`, `department`) values('admin-1','Admin Of School','','d623d1f24d9431d9120ae94a07c9f1cd66ea1ab6','1aa5837d60f03041d916f520fa86429cd095a2b5','2014-09-09 10:50:46','2014-09-09 10:50:46',NULL,NULL,NULL,'2014-09-09 10:55:05','1','0','0','0','5','admin')",
				"registrar===registrar-1" => "insert into `users` (`login`, `name`, `email`, `crypted_password`, `salt`, `created_at`, `updated_at`, `remember_token`, `remember_token_expires_at`, `activation_code`, `activated_at`, `is_activated`, `admin`, `student`, `employee`, `employee_id`, `department`) values('registrar-1','Registrar Of School','','8a69725bf27855cbb14266580da4c9fd74e7ff1f','3220ff4755528cfee029f6e969cfee59940c982c','2014-09-09 10:18:26','2014-09-09 10:18:26',NULL,NULL,NULL,'2014-09-09 10:54:54','1','0','0','0','2','registrar')",
				"finance===finance-1" => "insert into `users` (`login`, `name`, `email`, `crypted_password`, `salt`, `created_at`, `updated_at`, `remember_token`, `remember_token_expires_at`, `activation_code`, `activated_at`, `is_activated`, `admin`, `student`, `employee`, `employee_id`, `department`) values('finance-1','Finance Of School','','fb3a5223118a6bae90ad691523fc39d63d5160d0','7b1133c3a00b2efb36d2bab05eb12e27589bab40','2014-09-09 10:20:10','2014-09-09 10:20:10',NULL,NULL,NULL,'2014-09-09 10:54:58','1','0','0','0','3','finance')",
				"cashier===cashier-1" => "insert into `users` (`login`, `name`, `email`, `crypted_password`, `salt`, `created_at`, `updated_at`, `remember_token`, `remember_token_expires_at`, `activation_code`, `activated_at`, `is_activated`, `admin`, `student`, `employee`, `employee_id`, `department`) values('cashier-1','Cashier Of School','','d83a2922f9ed22dfbf7d86bd7e4dbb3597b200e2','750f0daecf07db84b608cf3f534a989a2213f14f','2014-09-09 10:23:24','2014-09-09 10:23:24',NULL,NULL,NULL,'2014-09-09 10:55:01','1','0','0','0','4','cashier')",	
				"hrd===hrd-1" => "insert into `users` (`login`, `name`, `email`, `crypted_password`, `salt`, `remember_token`, `remember_token_expires_at`, `activation_code`, `activated_at`, `is_activated`, `admin`, `employee`, `employee_id`, `department`) values('hrd-1','Human Resource','hrd@gmail.com','534be876e5bcce565b58e01f49af6881652c59ef','ed6038fe4bfe2d9f921cebcaa97ee48022a41294',NULL,NULL,NULL,'2011-05-04 02:42:46','1','0','1',NULL,'hrd')",
			);

	public function up(){

		if($this->db->table_exists($this->_table)){	

			$this->load->model('M_core_model','m');
			$this->m->set_table($this->_table);

			foreach ($this->_users as $dep => $val) {

				$this->m->set_table($this->_table);

				$dep_arr = explode('===', $dep);
				$dep = $dep_arr[0];
				$login = $dep_arr[1];

				unset($get);
				$get['where']['login'] = trim($login);
				$get['single'] = true;

				$department = $this->m->get_record(false, $get);
				
				if(!$department){

					/* RUN THE INSERT QUERY */
					$this->db->query($val);
					// vd($this->db->last_query());
					$id = $this->db->insert_id();

					/* CREATE EMPLOYEE FILE FOR THE RECORD */
					unset($data);
					$data['employeeid'] = $dep;
					$data['first_name'] = ucwords($dep);
					$data['middle_name'] = "Of";
					$data['last_name'] = "School";
					$data['joining_date'] = NOW;
					$data['gender'] = 'Male';
					$data['dob'] = '2000-01-01';
					$data['status'] = 1;
					$data['department'] = $dep;
					
					$this->m->set_table('employees');
					$rs = $this->m->insert($data);

					unset($data);
					$this->m->set_table($this->_table);
					$data['employee_id'] = $id;
					$this->m->update($id, $data);
				}
			}
		}
	}

	public function down(){

		if($this->db->table_exists($this->_table)){	

			$this->load->model('M_core_model','m');
			$this->m->set_table($this->_table);

			foreach ($this->_users as $dep => $val) {

				$dep_arr = explode('===', $dep);
				$dep = $dep_arr[0];
				$login = $dep_arr[1];

				unset($get);
				$get['where']['login'] = trim($login);
				$get['single'] = true;

				$department = $this->m->get_record(false, $get);
				
				if($department){
					$this->m->set_table('employees');
					$this->m->delete(array('id'=>$department->employee_id));

					$this->m->set_table($this->_table);
					$this->m->delete(array('id'=>$department->id));
				}
			}
		}
	}
}