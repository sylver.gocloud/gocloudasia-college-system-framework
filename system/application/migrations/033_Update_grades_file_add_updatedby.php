<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update_grades_file_add_updatedby extends CI_Migration {

	public function up() 
	{
		if($this->db->table_exists('grades_file'))
		{
			if (!$this->db->field_exists('updated_by', 'grades_file'))
			{
				$this->db->query("ALTER TABLE `grades_file` ADD COLUMN `updated_by` varchar(15) DEFAULT NULL");
			}
		}	
	} 
	public function down() 
	{
		if ($this->db->field_exists('updated_by', 'grades_file'))
		{
			$this->dbforge->drop_column('grades_file', 'updated_by');
		}
	}
} 