<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_package_tables extends CI_Migration {
	public function up(){
		if(!$this->db->table_exists("package_departments")){
			$this->dbforge->add_field("`id` bigint(20) unsigned NOT NULL AUTO_INCREMENT");
			$this->dbforge->add_field("`package_id` bigint(20) NOT NULL");
			$this->dbforge->add_field("`department_id` bigint(20) NOT NULL");
			$this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
			$this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
			$this->dbforge->add_key('`id`', TRUE);
			$this->dbforge->create_table("package_departments");
		}
		if(!$this->db->table_exists("package_menus")){
			$this->dbforge->add_field("`id` bigint(20) unsigned NOT NULL AUTO_INCREMENT");
			$this->dbforge->add_field("`package_id` bigint(20) NOT NULL COMMENT 'Package ID'");
			$this->dbforge->add_field("`department_id` bigint(20) NOT NULL COMMENT 'Department ID'");
			$this->dbforge->add_field("`main_menu_id` bigint(20) DEFAULT NULL");
			$this->dbforge->add_field("`department` varchar(255) DEFAULT NULL COMMENT 'Name of Department'");
			$this->dbforge->add_field("`controller` varchar(100) DEFAULT NULL COMMENT 'Name of Controller'");
			$this->dbforge->add_field("`caption` varchar(100) DEFAULT NULL COMMENT 'Title of Menu'");
			$this->dbforge->add_field("`menu_grp` varchar(30) DEFAULT NULL");
			$this->dbforge->add_field("`menu_sub` varchar(30) DEFAULT NULL");
			$this->dbforge->add_field("`menu_num` int(11) DEFAULT '0'");
			$this->dbforge->add_field("`menu_lvl` int(11) DEFAULT '0'");
			$this->dbforge->add_field("`menu_icon` varchar(100) DEFAULT NULL");
			$this->dbforge->add_field("`visible` smallint(1) DEFAULT '1'");
			$this->dbforge->add_field("`help` text");
			$this->dbforge->add_field("`attr` varchar(255) DEFAULT NULL");
			$this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
			$this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
			$this->dbforge->add_key('`id`', TRUE);
			$this->dbforge->create_table("package_menus");
		}
		if(!$this->db->table_exists("packages")){
			$this->dbforge->add_field("`id` bigint(20) unsigned NOT NULL AUTO_INCREMENT");
			$this->dbforge->add_field("`package` varchar(100) NOT NULL");
			$this->dbforge->add_field("`remarks` varchar(255) DEFAULT NULL");
			$this->dbforge->add_field("`level` int(12) DEFAULT '0'");
			$this->dbforge->add_field("`is_set` smallint(1) DEFAULT '0'");
			$this->dbforge->add_field("`color_code` varchar(50) DEFAULT NULL");
			$this->dbforge->add_field("`color` varchar(50) DEFAULT NULL");
			$this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
			$this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
			$this->dbforge->add_key('`id`', TRUE);
			$this->dbforge->create_table("packages");
		}
	}
	public function down(){
		if($this->db->table_exists("package_departments")){
			$this->dbforge->drop_table("package_departments");
		}
		if($this->db->table_exists("package_menus")){
			$this->dbforge->drop_table("package_menus");
		}
		if($this->db->table_exists("packages")){
			$this->dbforge->drop_table("packages");
		}
	}
}