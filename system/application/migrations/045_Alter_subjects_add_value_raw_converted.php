<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Alter_subjects_add_value_raw_converted extends CI_Migration {

	public function up(){
		
		if($this->db->table_exists("studentsubjects")){
			if (!$this->db->field_exists('raw', 'studentsubjects')){
				$this->db->query("ALTER TABLE `studentsubjects` ADD COLUMN `raw` FLOAT NULL COMMENT 'first input by user'");
			}
			if (!$this->db->field_exists('value', 'studentsubjects')){
				$this->db->query("ALTER TABLE `studentsubjects` ADD COLUMN `value` FLOAT NULL COMMENT 'Edited, Computed Value'");
			}
			if (!$this->db->field_exists('converted', 'studentsubjects')){
				$this->db->query("ALTER TABLE `studentsubjects` ADD COLUMN `converted` FLOAT NULL COMMENT 'Converted Value Based From Grading System Table'");
			}
		}
	}
	public function down(){

		if($this->db->table_exists("studentsubjects")){
			if ($this->db->field_exists('raw', 'studentsubjects')){
				$this->dbforge->drop_column('studentsubjects', 'raw');
			}
			if ($this->db->field_exists('value', 'studentsubjects')){
				$this->dbforge->drop_column('studentsubjects', 'value');
			}
			if ($this->db->field_exists('converted', 'studentsubjects')){
				$this->dbforge->drop_column('studentsubjects', 'converted');
			}
		}
	}
}  