<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class Migration_Alter_pre_requisite extends CI_Migration {
	public function up(){
		
		if($this->db->table_exists('subject_pre_requisite'))
		{
			if (!$this->db->field_exists('taken_at_once', 'subject_pre_requisite'))
			{
				$this->db->query("ALTER TABLE `subject_pre_requisite` ADD COLUMN `taken_at_once` TINYINT(1) DEFAULT 0 NULL COMMENT 'If Prerequisite can be taken simontaneously with the subject during enrollment' AFTER `updated_at`"); 
			}
		}	
	}
	public function down(){
		if($this->db->table_exists("subject_pre_requisite")){
			
			if ($this->db->field_exists('taken_at_once', 'subject_pre_requisite'))
			{
				$this->dbforge->drop_column('subject_pre_requisite', 'taken_at_once');
			}
		}
	}
}