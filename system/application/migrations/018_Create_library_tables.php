<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_library_tables extends CI_Migration {
	public function up(){
		if(!$this->db->table_exists("lib_circulationfile1")){
			$this->dbforge->add_field("`id` bigint(20) unsigned NOT NULL AUTO_INCREMENT");
			$this->dbforge->add_field("`trndte` datetime NOT NULL COMMENT 'Transaction Date'");
			$this->dbforge->add_field("`usertype` set('student','employee') NOT NULL DEFAULT 'student' COMMENT 'Student or Employees'");
			$this->dbforge->add_field("`borrower_id` varchar(25) DEFAULT NULL COMMENT 'Emp_id or Enrollment_id'");
			$this->dbforge->add_field("`ay_id` varchar(25) DEFAULT NULL COMMENT 'Academic Year ID'");
			$this->dbforge->add_field("`status` set('UNRETURN','RETURN') DEFAULT 'UNRETURN'");
			$this->dbforge->add_field("`day` int(5) DEFAULT NULL");
			$this->dbforge->add_field("`remarks` varchar(255) DEFAULT NULL");
			$this->dbforge->add_field("`librarian_id` varchar(25) DEFAULT NULL COMMENT 'User ID of Lirarian'");
			$this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
			$this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
			$this->dbforge->add_field("`is_deleted` smallint(1) DEFAULT '0' COMMENT 'If Deleted'");
			$this->dbforge->add_field("`date_deleted` datetime DEFAULT NULL");
			$this->dbforge->add_field("`deleted_by` varchar(25) DEFAULT NULL");
			$this->dbforge->add_key('`id`', TRUE);
			$this->dbforge->create_table("lib_circulationfile1");
		}
		if(!$this->db->table_exists("lib_circulationfile2")){
			$this->dbforge->add_field("`id` bigint(20) unsigned NOT NULL AUTO_INCREMENT");
			$this->dbforge->add_field("`circulation_id` bigint(20) NOT NULL COMMENT 'Id of libcirculationfile1'");
			$this->dbforge->add_field("`media_id` bigint(20) NOT NULL COMMENT 'Id of Media Types'");
			$this->dbforge->add_field("`trndte` datetime DEFAULT NULL COMMENT 'Borrowed Date'");
			$this->dbforge->add_field("`retdte` datetime DEFAULT NULL COMMENT 'Return Date'");
			$this->dbforge->add_field("`day` int(5) DEFAULT '1'");
			$this->dbforge->add_field("`cir_status` set('UNRETURN','RETURN','LOST','DAMAGE') DEFAULT NULL COMMENT 'Circulation Status'");
			$this->dbforge->add_field("`is_late` smallint(1) DEFAULT '0' COMMENT 'Is Media Returned Late'");
			$this->dbforge->add_field("`day_late` int(5) DEFAULT '0' COMMENT 'Days Late'");
			$this->dbforge->add_field("`remarks` varchar(255) DEFAULT NULL");
			$this->dbforge->add_field("`created_at` datetime DEFAULT NULL");
			$this->dbforge->add_field("`updated_at` datetime DEFAULT NULL");
			$this->dbforge->add_field("`is_deleted` smallint(1) DEFAULT '0'");
			$this->dbforge->add_field("`date_deleted` datetime DEFAULT NULL");
			$this->dbforge->add_field("`deleted_by` varchar(25) DEFAULT NULL");
			$this->dbforge->add_key('`id`', TRUE);
			$this->dbforge->create_table("lib_circulationfile2");
		}
		if(!$this->db->table_exists("library_book_category")){
			$this->dbforge->add_field("`id` int(10) unsigned NOT NULL AUTO_INCREMENT");
			$this->dbforge->add_field("`lbc_name` varchar(45) NOT NULL DEFAULT ''");
			$this->dbforge->add_field("`lbc_desc` varchar(255) NOT NULL DEFAULT ''");
			$this->dbforge->add_field("`lbc_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'");
			$this->dbforge->add_field("`lbc_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'");
			$this->dbforge->add_field("`created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'");
			$this->dbforge->add_field("`updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'");
			$this->dbforge->add_key('`id`', TRUE);
			$this->dbforge->create_table("library_book_category");
		}
		if(!$this->db->table_exists("library_books")){
			$this->dbforge->add_field("`id` int(10) unsigned NOT NULL AUTO_INCREMENT");
			$this->dbforge->add_field("`book_category` int(10) unsigned NOT NULL DEFAULT '0'");
			$this->dbforge->add_field("`book_name` varchar(255) NOT NULL DEFAULT ''");
			$this->dbforge->add_field("`book_desc` varchar(255) NOT NULL DEFAULT ''");
			$this->dbforge->add_field("`book_copies` int(10) unsigned NOT NULL DEFAULT '0'");
			$this->dbforge->add_field("`book_isbn` varchar(45) NOT NULL DEFAULT ''");
			$this->dbforge->add_field("`book_author` varchar(45) NOT NULL DEFAULT ''");
			$this->dbforge->add_field("`book_publisher` varchar(45) NOT NULL DEFAULT ''");
			$this->dbforge->add_field("`book_dop` date NOT NULL DEFAULT '0000-00-00'");
			$this->dbforge->add_field("`book_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'");
			$this->dbforge->add_field("`book_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'");
			$this->dbforge->add_field("`created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'");
			$this->dbforge->add_field("`updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'");
			$this->dbforge->add_field("`media_type_id` varchar(20) NOT NULL");
			$this->dbforge->add_field("`book_borrowed` int(5) NOT NULL DEFAULT '0'");
			$this->dbforge->add_field("`book_barcode` varchar(100) NOT NULL");
			$this->dbforge->add_field("`accession_number` varchar(255) NOT NULL");
			$this->dbforge->add_field("`call_number` varchar(255) NOT NULL");
			$this->dbforge->add_key('`id`', TRUE);
			$this->dbforge->create_table("library_books");
		}
		if(!$this->db->table_exists("media_types")){
			$this->dbforge->add_field("`id` bigint(20) unsigned NOT NULL AUTO_INCREMENT");
			$this->dbforge->add_field("`media_type` varchar(125) NOT NULL");
			$this->dbforge->add_field("`created_at` datetime NOT NULL");
			$this->dbforge->add_field("`updated_at` datetime NOT NULL");
			$this->dbforge->add_key('`id`', TRUE);
			$this->dbforge->create_table("media_types");
		}
	}
	public function down(){
		if($this->db->table_exists("lib_circulationfile1")){
			$this->dbforge->drop_table("lib_circulationfile1");
		}
		if($this->db->table_exists("lib_circulationfile2")){
			$this->dbforge->drop_table("lib_circulationfile2");
		}
		if($this->db->table_exists("library_book_category")){
			$this->dbforge->drop_table("library_book_category");
		}
		if($this->db->table_exists("library_books")){
			$this->dbforge->drop_table("library_books");
		}
		if($this->db->table_exists("media_types")){
			$this->dbforge->drop_table("media_types");
		}
	}
}