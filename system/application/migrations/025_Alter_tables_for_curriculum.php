<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class Migration_Alter_tables_for_curriculum extends CI_Migration {
	public function up(){
		
		if($this->db->table_exists('subject_pre_requisite'))
		{
			if (!$this->db->field_exists('subject_refid', 'subject_pre_requisite'))
			{
				$this->db->query("ALTER TABLE `subject_pre_requisite` ADD COLUMN `subject_refid` VARCHAR(12) NULL COMMENT 'Master_subject refid' AFTER `subject_id`; ");
			}
		}	
	}
	public function down(){
		if($this->db->table_exists("subject_pre_requisite")){
			
			if ($this->db->field_exists('subject_refid', 'subject_pre_requisite'))
			{
				$this->dbforge->drop_column('subject_pre_requisite', 'subject_refid');
			}
		}
	}
}