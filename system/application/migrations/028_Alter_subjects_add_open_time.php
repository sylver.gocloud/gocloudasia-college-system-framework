<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class Migration_Alter_subjects_add_open_time extends CI_Migration {
	public function up(){
		
		if($this->db->table_exists('subjects'))
		{
			if (!$this->db->field_exists('is_open_time', 'subjects'))
			{
				$this->db->query("ALTER TABLE `subjects` ADD COLUMN `is_open_time` TINYINT(1) DEFAULT 0 NULL AFTER `updated_at`"); 
			}
		}	
	}
	public function down(){
		if($this->db->table_exists("subjects")){
			
			if ($this->db->field_exists('is_open_time', 'subjects'))
			{
				$this->dbforge->drop_column('subjects', 'is_open_time');
			}
		}
	}
}