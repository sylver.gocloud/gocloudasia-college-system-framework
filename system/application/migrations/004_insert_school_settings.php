<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Insert_school_settings extends CI_Migration {

	/**
	 * Create a data
	 */

	private $_table = "settings";

	public function up(){

		if($this->db->table_exists($this->_table)){	

			$this->load->model('M_core_model','m');
			$this->m->set_table($this->_table);

			$set = $this->m->get_record(false);
			
			if(!$set){

				unset($data);
				
				$data['school_name'] = "School Name";
				$data['school_address'] = "School Address";
				$data['school_telephone'] = "School Telephone";
				$data['email'] = "school@email";
				$data['logo'] = "";

				$this->m->insert($data);
			}
		}
	}

	public function down(){

		if($this->db->table_exists($this->_table)){	

			$this->load->model('M_core_model','m');
			$this->m->set_table($this->_table);

			$set = $this->m->get_record(false);
			
			if($set){

				$sql = "TRUNCATE $this->_table";
				$this->db->query($sql);
			}
		}
	}
}