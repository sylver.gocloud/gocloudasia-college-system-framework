<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update_department_edit_calendar extends CI_Migration {

	public function up() 
	{
		if($this->db->table_exists('departments'))
		{
			if (!$this->db->field_exists('edit_calendar', 'departments'))
			{
				$this->db->query("ALTER TABLE `departments` ADD COLUMN `edit_calendar` TINYINT(1) DEFAULT 0");
			}
		}	

		if($this->db->table_exists('package_departments'))
		{
			if (!$this->db->field_exists('edit_calendar', 'package_departments'))
			{
				$this->db->query("ALTER TABLE `package_departments` ADD COLUMN `edit_calendar` INT(1) DEFAULT 0");
			}
		}	
	} 
	public function down() 
	{
		if ($this->db->field_exists('edit_calendar', 'departments'))
		{
			$this->dbforge->drop_column('departments', 'edit_calendar');
		}

		if ($this->db->field_exists('edit_calendar', 'package_departments'))
		{
			$this->dbforge->drop_column('package_departments', 'edit_calendar');
		}
	}
} 
