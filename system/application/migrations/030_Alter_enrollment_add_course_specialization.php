<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class Migration_Alter_enrollment_add_course_specialization extends CI_Migration {
	public function up(){
		
		if($this->db->table_exists('enrollments'))
		{
			if (!$this->db->field_exists('course_specialization', 'enrollments'))
			{
				$this->db->query("ALTER TABLE `enrollments` ADD COLUMN `course_specialization` VARCHAR(150) NULL"); 
			}
		}	
	}
	public function down(){
		if($this->db->table_exists("enrollments")){
			
			if ($this->db->field_exists('course_specialization', 'enrollments'))
			{
				$this->dbforge->drop_column('enrollments', 'course_specialization');
			}
		}
	}
}