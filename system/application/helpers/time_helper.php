<?php
if(!function_exists('is_time_conflict'))
{
	function is_time_conflict($from, $to, $from_compare, $to_compare){
		$from = strtotime($from);
		$from_compare = strtotime($from_compare);
		$to = strtotime($to);
		$to_compare = strtotime($to_compare);
		$intersect = min($to, $to_compare) - max($from, $from_compare);
			if ( $intersect < 0 ) $intersect = 0;
			$overlap = $intersect / 3600;
			if ( $overlap <= 0 ):
				// There are no time conflicts
				return FALSE;
				else:
				// There is a time conflict
				return '<p>There is a time conflict where the times overlap by ' . $overlap . ' hours.</p>';
				return TRUE;
			endif;
	}
}

if(!function_exists('_convert_to_12'))
{
	/**
	 * Conver Subject Time to 12 hour format
	 * @param string $time 00:00:00-00:00:00
	 * @param int(1/0) $open_time if 0 return
	 * @return string
	 */
	function _convert_to_12($time, $open_time = 0, $format = "g:i a"){
		$cut = explode("-", $time);

		if($open_time === "1"){
			return $time;
		}

		if(count($cut) === 2){
			$from = date($format, strtotime($cut[0]));
			$to = date($format, strtotime($cut[1]));
			return $from.'-'.$to;
		}
		return $time;
	}
}

if(!function_exists('_extract_time'))
{
	/**
	 * Extract Time FROM & Time To
	 * @param string $time 00:00:00-00:00:00
	 * @return object
	 */
	function _extract_time($time){

		$ret = new StdClass;
		$ret->from_hr = "00";
		$ret->from_min = "00";
		$ret->from_sec = "00";

		$ret->to_hr = "00";
		$ret->to_min = "00";
		$ret->to_sec = "00";

		$cut = explode("-", $time);
		if(count($cut) === 2){
			$from = $cut[0];
			$to = $cut[1];
			
			$cut_from = explode(":", $from);
			$ret->from_hr = isset($cut_from[0])?$cut_from[0]:'00';
			$ret->from_min =isset($cut_from[1])?$cut_from[1]:'00';
			$ret->from_sec = isset($cut_from[2])?$cut_from[2]:'00';

			$cut_to = explode(":", $to);
			$ret->to_hr = isset($cut_to[0])?$cut_to[0]:'00';
			$ret->to_min =isset($cut_to[1])?$cut_to[1]:'00';
			$ret->to_sec = isset($cut_to[2])?$cut_to[2]:'00';
		}
		return $ret;
	}
}

if(!function_exists('_day_legend'))
{
	function _day_legend($day){
		$ci =& get_instance();
		$ci->load->model('M_config','m');
		return $ci->m->get_day_legend($day);
	}
}

if(!function_exists('_get_week_days'))
{
	function _get_week_days(){
		$day = ['monday','tuesday','wednesday','thursday','friday','saturday','sunday'];
		$r = array();
		foreach ($day as $k => $v) {
			$r[_day_legend($v)] = ucwords($v);
		}
		return $r;
	}
}

