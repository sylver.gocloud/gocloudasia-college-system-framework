<?php
	function create_random_string($word = false, $ln = 6)
	{
		$ln = rand(2, 5);
		//FOR CAPTCHA PURPOSE
		if ($word == false)
		{
			$pool = '123456789abcdefhjkmnpqrstuvwxyzABCDEFGHJKMNPQRSTUVWXYZ';
		}
		else
		{
			$pool = $word;
		}

		$str = '';
		
		for ($i = 0; $i < $ln; $i++){
	   
			$str .= substr($pool, mt_rand(0, strlen($pool) -1), 1);
		}

		$word = $str;
			
		return $word;
	}

	function generate_unique_id($len = 10)
	{
		//set the random id length 
		$random_id_length = 10; 

		//generate a random id encrypt it and store it in $rnd_id 
		$rnd_id = crypt(uniqid(rand(),1)); 

		//to remove any slashes that might have come 
		$rnd_id = strip_tags(stripslashes($rnd_id)); 

		//Removing any . or / and reversing the string 
		$rnd_id = str_replace(".","",$rnd_id); 
		$rnd_id = strrev(str_replace("/","",$rnd_id)); 

		//finally I take the first 10 characters from the $rnd_id 
		$rnd_id = substr($rnd_id,0,$len); 

		return $rnd_id;
	}

	if(!function_exists('generate_recid')){
		function generate_recid($len = 10, $table, $field = 'recid')
		{
			$CI=& get_instance();

			$id = generate_unique_id($len); // generate the id

			//check if id duplicated from the given table and field
			$q = $CI->db->query("SELECT $field FROM $table LIMIT 1");
			if($q->num_rows() > 0){

				// if duplicated run the function again
				generate_recid($len, $table, $field); 

			}else{

				return $id;
			}
		}
	}
?>