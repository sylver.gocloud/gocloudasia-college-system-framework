<?php

/*
* param decimal,int,float 
* outputs formatted currency e.a 
* input: 1000.00
*output: 1,000.00
*/
function m($money,$sign = 0)
{
	// check if number has a point or not
	$pos = strpos($money,'.');
	if($pos > 0)
	{
			$mon= substr($money,0,$pos);// get  all the inger before the decimal point
			$decimal = substr($money,$pos); //get the number after the decimal point
			
			if($sign == 0)
			{
				// return formatted number with its decimal number
				return number_format($mon).$decimal;
			}elseif($sign == 1)
			{
				// return formatted number with peso sign and decimal number
				return '&#8369; '.number_format($mon).$decimal;
			}
	}else{
		if($sign == 0){
			return number_format($money).'.00';
		}else{
			return '&#8369; '.number_format($money).'.00';
		}
	}
}

/*
* param decimal,int,float 
* outputs formatted currency e.a 
* input: 1000.00
*output: 1,000.00
*/
function money($money,$sign = 0)
{
	// check if number has a point or not
	$pos = strpos($money,'.');
	if($pos > 0)
	{
			$mon= substr($money,0,$pos);// get  all the inger before the decimal point
			$decimal = substr($money,$pos); //get the number after the decimal point
			
			if($sign == 0)
			{
				// return formatted number with its decimal number
				return number_format($mon).$decimal;
			}elseif($sign == 1)
			{
				// return formatted number with peso sign and decimal number
				return '&#8369; '.number_format($mon).$decimal;
			}
	}else{
		//not a decimal attach a .00 after the formatted number
		if($sign == 0){
			return number_format($money).'.00';
		}else{
			return '&#8369; '.number_format($money).'.00';
		}
	}
}

/*
* param decimal,int,float 
* outputs formatted currency e.a 
* input: 1000.00
*output: 1,000.00
*/
function mowney($money,$sign = 0)
{
	// check if number has a point or not
	$pos = strpos($money,'.');
	if($pos > 0)
	{
			$mon= substr($money,0,$pos);// get  all the inger before the decimal point
			$decimal = substr($money,$pos); //get the number after the decimal point
			
			if($sign == 0)
			{
				// return formatted number with its decimal number
				return number_format($mon).$decimal;
			}elseif($sign == 1)
			{
				// return formatted number with peso sign and decimal number
				return '&#8369; '.number_format($mon).$decimal;
			}
	}else{
		if($sign == 0){
			return number_format($money).'.00';
		}else{
			return '&#8369; '.number_format($money).'.00';
		}
	}
}

function num_format($money = 0, $peso = 0)
{
	if($peso == 1){
		return '&#8369; '.number_format($money, 2, '.', ',');
	}

	return number_format($money, 2, '.', ',');
}

/*
	NUMBER TO NUM-TH
*/

function num_to_th($num = false)
{
	$ret = "";
	if($num && $num > 0){
		
		switch($num)
		{
			case 1;
				$ret = "1st";
			break;
			case 2;
				$ret = "2nd";
			break;
			case 3;
				$ret = "3rd";
			break;
			case 4;
				$ret = "4th";
			break;
			case 5;
				$ret = "5th";
			break;
			case 6;
				$ret = "6th";
			break;
			case 7;
				$ret = "7th";
			break;
			case 8;
				$ret = "8th";
			break;
			case 9;
				$ret = "9th";
			break;
			case 10;
				$ret = "10th";
			break;
			case 11;
				$ret = "11th";
			break;
			case 12;
				$ret = "12th";
			break;
			default:
				$ret = "";
			break;
		}
		
	}
	return $ret;
}

function peso()
{
	echo "&#8369; ";
}