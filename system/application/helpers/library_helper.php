<?php
/*********************
	Helper for library
**********************/

if(!function_exists('is_date_late'))
{
	function is_date_late($date1, $date2)
	{
		$CI=& get_instance();
		$datetime1 = new DateTime($date1); //should be in yy-mm-dd format
		$datetime2 = new DateTime($date2); //should be in yy-mm-dd format
		$interval = $datetime1->diff($datetime2);

		return (intval($interval->format('%R%a')) < 0 ) ? true : false;
	}
}

if(!function_exists('day_diff'))
{
	function day_diff($date1, $date2)
	{
		$CI=& get_instance();
		$datetime1 = new DateTime($date1); //should be in yy-mm-dd format
		$datetime2 = new DateTime($date2); //should be in yy-mm-dd format
		$interval = $datetime1->diff($datetime2);

		return (intval($interval->format('%R%a')));
	}
}

if(!function_exists('is_item_late'))
{
	function is_item_late($lib2_id)
	{
		$CI=& get_instance();
		$CI->load->model('M_lib_circulationfile2','m2');

		$det = $CI->m2->get($lib2_id);
		if($det)
		{

			$exp_retdte = date('Y-m-d',strtotime($det->trndte) + (24*3600*$det->day));
			$ret_dte = date('Y-m-d');
			return is_date_late($ret_dte, $exp_retdte);

		}

		return false;
	}
}

if(!function_exists('has_return_item'))
{
	function has_return_item($lib1_id)
	{
		$CI=& get_instance();
		$CI->load->model('M_lib_circulationfile2','m2');
		unset($get);
		$get['where']['circulation_id'] = $lib1_id;
		$get['where']['is_deleted'] = 0;
		$get['where']['cir_status'] = 'RETURN';
		$get['single'] = true;
		return $CI->m2->get_record(false, $get) ? true : false;
	}
}