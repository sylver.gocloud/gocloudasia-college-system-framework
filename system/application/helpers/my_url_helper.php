<?php

if ( ! function_exists('badge_link'))
{
	function badge_link($uri = '', $type='', $value='Default Title', $attr = '', $confirm = false)
	{
		$CI =& get_instance();
		$url_to = $CI->config->base_url($uri);
		$value = ucwords($value);
		$delete = $confirm ? 'confirm' : ''; 
		$html = "<a href='{$url_to}' {$attr} class='{$delete} badge btn-${type}'>{$value}</a>";
		
		
		return $html;
	}
}

if ( ! function_exists('array_to_geturl')){
	function array_to_geturl($arr = array())
	{
		$ret = "?";
		
		foreach($arr as $key => $val)
		{
			if($val){
				if($ret != '?')
				{
					$ret .= '&';
				}
				$ret .= $key."=".$val;
			}
		}
		return $ret;
	}
}


if ( ! function_exists('style_url'))
{
	function style_url($uri = '')
	{
		$CI =& get_instance();
		return $CI->config->base_url('assets/css/'.$uri.'.css');
	}
}

if ( ! function_exists('script_url'))
{
	function script_url($uri = '')
	{
		$CI =& get_instance();
		return $CI->config->base_url('assets/js/'.$uri.'.js');
	}
}

if ( ! function_exists('image_url'))
{
	function image_url($image_name = '')
	{
		$CI =& get_instance();
		return $CI->config->base_url('assets/img/'.$image_name);
	}
}