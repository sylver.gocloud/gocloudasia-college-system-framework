<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
     
    if(!function_exists('get_dbcon'))
    {
        function get_dbcon($var)
        {
            $ci =& get_instance();

            $ret = "";

            switch (strtolower($var)) {
                case 'host':
                case 'hostname':
                    $ret = $ci->db_connection->host;
                    break;
                case 'user':
                case 'username':
                    $ret = $ci->db_connection->user;
                    break;
                case 'pass':
                case 'password':
                    $ret = $ci->db_connection->password;
                    break;
                case 'db':
                case 'database':
                    $ret = $ci->db_connection->database;
                    break;
            }
            return $ret;
        }
    }