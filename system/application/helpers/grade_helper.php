<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if(!function_exists('convert_grade')){

	function convert_grade($value, $grading_system = false) {
		$value = cleanNull($value);
		if($value === ""){
			return "";
		}

		$ci =& get_instance();
		$ci->load->model('M_sys_par');
		$ci->load->model('M_grading_system');
		$ci->load->helper('string');
		$syspar = $ci->M_sys_par->get_sys_par();
		$gs = $grading_system ? $grading_system : ($syspar && $syspar->grading_system ? $syspar->grading_system : 'input');
		if($gs === "input"){
			return $value;
		}

		return $ci->M_grading_system->convert_grade($value, $gs);
	}
}