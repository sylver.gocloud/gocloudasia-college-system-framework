<?php

/** 
 * @function capitalize first letter of words in a string
 * @string - words or sentence to be capitalize
 * @separator - char that will divide you string example space or underscore
 **/
function capital_fwords($string = false, $separator = ' ') {
	
	$ret = "";

	if($string){
		$arr_letter = explode($separator, $string);
		foreach ($arr_letter as $key => $value) {
			if($ret == ""){
				$ret = ucwords(strtolower($value));
			}else{
				$ret .= ' '.ucwords(strtolower($value));
			}
		}
	}

	return $ret;
}

function check_day_if_exist($day, $days_string = false)
{
	if($days_string && $days_string != "")
	{
		$arr = explode('-', $days_string);
		
		foreach ($arr as $value) {
			if(strtoupper($day) == strtoupper($value))
			{
				return TRUE;
				break;
			}
		}
	}

	return FALSE;
}

if(!function_exists('implode_object'))
{
	/**
	 * Implode object into string
	 * @param object $o
	 * @param string $field
	 * @param string $glue Glue String
	 * @return string
	 */
  function implode_object($o, $field, $glue="','")
  {
  	$ret = "";
  	
  	if($o){

  		$to_be_imp = array();
  		foreach ($o as $key => $value) {
  			$to_be_imp[] = $value->$field;
  		}

  		return implode($glue, $to_be_imp);
  	}

  	return $ret;
  }
}

if(!function_exists('cleanNull'))
{
	/**
	 * Check if empty or null - if yes return string '' else return value
	 */
	function cleanNull($question){
			$question = (string)$question;
	    return (!isset($question) || trim($question)==='') ? '' : $question;
	}
}