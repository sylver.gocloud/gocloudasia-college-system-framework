<?php

	function print_html($enrollment,$student_total,$student_subjects,$studentfinance,$studentfees,$gmdate){
		$ci =& get_instance();


		$html = '
		<head>
		<style>

		html, body, div, span, applet, object, iframe,h1, h2, h3, h4, h5, h6, p, blockquote, pre,a, abbr, acronym, address, big, cite, code,del, dfn, em, img, ins, kbd, q, s, samp,small, strike, strong, sub, sup, tt, var,b, u, i, center,dl, dt, dd, ol, ul, li,fieldset, form, label, legend,table, caption, tbody, tfoot, thead, tr, th, td,article, aside, canvas, details, embed, figure, figcaption, footer, header, hgroup, menu, nav, output, ruby, section, summary,time, mark, audio, video 
		{
			margin: 0;
			padding: 0;
			border: 0;
			font-size: 10px;
			font: inherit;
			vertical-align: top;
		}

		/* HTML5 display-role reset for older browsers */
		article, aside, details, figcaption, figure, footer, header, hgroup, menu, nav, section 
		{
			display: block;
		}

		td
		{
		padding: 3px;
		}

		th
		{
		border-bottom: 1px solid #000;
		font-weight: bold;
		font-size:10px;
		padding: 3px;
		margin: 0px;
		text-align: center;
		}

		td.title
		{
		border-top: 1px solid #000;
		border-right: 1px solid #000;
		border-left: 1px solid #000;
		font-weight: bold;
		}

		td.title2
		{
		border-bottom: #000 1px solid;
		}

		</style>
		</head>
		<body>

		<div>

		<table style="height:1238px; width:806px;">
		<tr>
		<td style="height:600px; text-align:top;" valign="top">

		<div>
		<table width="600px" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td style="font-weight: bold;" colspan="7">
		CERTIFICATE OF MATRICULATION
		</td>
		</tr>
		<tr>
		<td colspan="2" style="font-weight: bold; background-color: #EEEEEE; text-align:center; border-top: #000 1px solid; border-bottom: #000 1px solid;">
		STUDENT\'S COPY
		</td>
		<td>
		&nbsp;
		</td>
		<td>
		&nbsp;
		</td>
		<td>
		&nbsp;
		</td>
		<td class="title title2">
		SYSTEM ID No.
		</td>
		<td style="border-top: #000 1px solid; border-right: #000 1px solid; border-bottom: #000 1px solid;">'.
		$enrollment->login
		.'</td>
		</tr>
		<tr>
		<td colspan="7" style="line-height:0; height: 4px;">
		</td>
		</tr>
		<tr>
		<td class="title">
		STUDENT No.
		</td>
		<td colspan="3" style="border-top: #000 1px solid; border-right: #000 1px solid;">
		</td>
		<td>
		</td>
		<td class="title">
		COLLEGE
		</td>
		<td style="border-top: #000 1px solid; border-right: #000 1px solid;">
		</td>
		</tr>
		<tr>
		<td class="title">
		STUDENT NAME
		</td>
		<td colspan="3" style="border-top: #000 1px solid; border-right: #000 1px solid; text-transform:capitalize;">'.
		$enrollment->last_name .', '. $enrollment->first_name .' '. $enrollment->middle_name
		.'</td>
		<td>
		</td>
		<td class="title">
		COURSE
		</td>
		<td style="border-top: #000 1px solid; border-right: #000 1px solid;">'.
		$enrollment->course
		.'</td>
		</tr>
		<tr>
		<td class="title">
		CASHIER\'S CODE
		</td>
		<td colspan="3" style="border-top: #000 1px solid; border-right: #000 1px solid; text-transform:capitalize;">'.
		 $ci->session->userdata['username']
		.'</td>
		<td>
		</td>
		<td class="title">
		MAJOR
		</td>
		<td style="border-top: #000 1px solid; border-right: #000 1px solid;">
		</td>
		</tr>
		<tr>
		<td class="title title2" style="width:150px;">
		DATE
		</td>
		<td style="width:135px; border-top: #000 1px solid; border-bottom: #000 1px solid;">
		'.
		date("F d, Y",$gmdate)
		.'</td>
		<td class="title title2" style="width:50px; text-align:center;">
		TIME
		</td>
		<td style="width:105px; border-top: #000 1px solid; border-right: #000 1px solid; border-bottom: #000 1px solid;">
		'.
		date("h:i:s A",$gmdate)
		.'</td>
		<td style="width:5px;">
		</td>
		<td class="title title2" style="width:100px;">
		S.Y./SEMESTER
		</td>
		<td style="width:250px; border-top: #000 1px solid; border-right: #000 1px solid; border-bottom: #000 1px solid;">
		'.
		$enrollment->sy_from .' - '. $enrollment->sy_to .' / '. $enrollment->name
		.'</td>
		</tr>
		<tr>
		<td colspan="7" style="line-height:0; height: 4px;">
		</td>
		</tr>
		</table>
		</div>

		<div>
		<table width="600px" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td class="title" style="text-align:center; width:340px;">
		ENROLLED SUBJECTS/CLASS SCHEDULE
		</td>
		<td style="width:5px; padding:0px;"></td>
		<td class="title" style="text-align:center; width:255px;">
		MISCELLANEOUS & OTHER FEES
		</td>
		</tr>
		<tr>
		<td style="border-top: #000 1px solid; border-right: #000 1px solid; border-left: #000 1px solid; padding:0px;">

		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<th>Subject Code</th>
		<th>Subject Description</th>
		<th>Units</th>
		<th>Time</th>
		<th>Day</th>
		</tr>';

		foreach($student_subjects as $s)
		{
		$html .= '<tr><td>'.$s->sc_id.'</td><td>'.$s->subject.'</td><td>'.$s->units.'</td><td>'.$s->time.'</td><td>'.$s->day.'</td></tr>';
		}

		$html .= '
		</table>

		</td>
		<td style="width:5px;"></td>
		<td style="border-top: #000 1px solid; border-right: #000 1px solid; border-left: #000 1px solid; padding:0px;">

		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td style="width:100px; font-weight:bold; text-align:center; border-right: #000 1px solid;">
		AMOUNT
		</td>
		<td style="width:155px; font-weight:bold; text-align:center;">
		DESCRIPTION
		</td>
		</tr>
		<tr>
		<td style="width:100px; text-align:right; border-right: #000 1px solid;">'.
		number_format($student_total->total_misc_fee,2,'.',' ')
		.'</td>
		<td style="width:155px;">
		MISCELLANEOUS FEE
		</td>
		</tr>
		<tr>
		<td style="width:100px; text-align:right; border-right: #000 1px solid;">
		&nbsp;
		</td>
		<td style="width:155px; font-weight:bold;">
		OTHER FEES
		</td>
		</tr>';

		foreach($studentfees as $sf):
			if($sf->is_other == 1){
			$html .= '<tr>';
				if(strtolower($sf->name) != "tuition fee per unit" || strtolower($sf->name) != "laboratory fee per unit" || strtolower($sf->name) != "rle"){
					$html .= '<td style="width:100px; text-align:right; border-right: #000 1px solid;">'.number_format($sf->value,2,'.',' ').'</td><td style="width:155px;">'.$sf->name.'</td>';
				}
			$html .= '</tr>';
			}
		endforeach;

		$html .= '</table>

		</td>
		</tr>
		<tr>
		<td class="title" style="text-align:center; width:340px;">
		TOTAL NUMBER OF UNITS - '.
		number_format($student_total->total_units,2,'.',' ')
		.'</td>
		<td style="width:5px;"></td>
		<td class="title" style="text-align:center; width:255px;">
		COMPUTATION
		</td>
		</tr>
		<tr>
		<td class="title" style="text-align:center; width:340px;">
		CERTIFICATION
		</td>
		<td style="width:5px;"></td>
		<td style="border-right: #000 1px solid; border-top: #000 1px solid; border-left: #000 1px solid; padding:0px;">

		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td style="width:100px; text-align:right; border-right: #000 1px solid;">'.
		number_format($student_total->tuition_fee + $student_total->lab_fee,2,'.',' ')
		.'</td>
		<td style="width:155px; font-weight:bold;">
		TUITION FEE
		</td>
		</tr>
		</table>

		</td>
		</tr>
		<tr>
		<td style="border-top: #000 1px solid; border-right: #000 1px solid; border-left: #000 1px solid; width:340px;">
		I hereby certify that I have read and fully understand the terms and conditions of my enrollment in Dr. Yanga\'s Colleges, Inc. written at the back of this Certificate of Matriculation. Furthermore, I hereby agree to pay the total payable fees based on the schedule of payment below:
		</td>
		<td style="width:5px;"></td>
		<td style="border-right: #000 1px solid; border-left: #000 1px solid; padding:0px;">

		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td style="width:100px; text-align:right; border-right: #000 1px solid;">'.
		number_format($student_total->total_misc_fee,2,'.',' ')
		.'</td>
		<td style="width:155px; font-weight:bold;">
		MISCELANEOUS FEE
		</td>
		</tr>
		<tr>
		<td style="width:100px; text-align:right; border-right: #000 1px solid;">'.
		number_format($student_total->total_other_fee,2,'.',' ')
		.'</td>
		<td style="width:155px; font-weight:bold;">
		OTHER FEES
		</td>
		</tr>
		<tr>
		<td style="width:100px; text-align:right; border-right: #000 1px solid;">'.
		number_format($student_total->total_rle,2,'.',' ')
		.'</td>
		<td style="width:155px; font-weight:bold;">
		RELATED EXPERIENCE LEARNING
		</td>
		</tr>
		<tr>
		<td style="width:100px; text-align:right; border-right: #000 1px solid;">'.
		number_format($student_total->total_deduction + $student_total->total_payment,2,'.',' ')
		.'</td>
		<td style="width:155px; font-weight:bold;">
		LESS: DISCOUNT
		</td>
		</tr>
		<tr>
		<td style="width:100px; text-align:right; border-right: #000 1px solid;">
		&nbsp;
		</td>
		<td style="width:155px; font-weight:bold;">
		&nbsp;
		</td>
		</tr>
		</table>

		</td>
		</tr>
		<tr>
		<td style="border-right: #000 1px solid; border-bottom: #000 1px solid; border-left: #000 1px solid; width:340px; font-weight:bold;">
		SIGNATURE OF STUDENT
		</td>
		<td style="width:5px;"></td>
		<td style="border-right: #000 1px solid; border-bottom: #000 1px solid; border-left: #000 1px solid; padding:0px;">

		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td style="width:100px; text-align:right; border-right: #000 1px solid; font-weight:bold;">'.
		number_format($student_total->remaining_balance,2,'.',' ')
		.'</td>
		<td style="width:155px; font-weight:bold;">
		TOTAL PAYABLE FEES
		</td>
		</tr>
		</table>

		</td>
		</tr>
		</table>
		</div>

		</td>
		</tr>
		<tr>
		<td style="height:600px; text-align:top;">

		<div>
		<table width="600px" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td style="font-weight: bold;" colspan="7">
		CERTIFICATE OF MATRICULATION
		</td>
		</tr>
		<tr>
		<td colspan="2" style="font-weight: bold; background-color: #EEEEEE; text-align:center; border-top: #000 1px solid; border-bottom: #000 1px solid;">
		FINANCE COPY
		</td>
		<td>
		&nbsp;
		</td>
		<td>
		&nbsp;
		</td>
		<td>
		&nbsp;
		</td>
		<td class="title title2">
		SYSTEM ID No.
		</td>
		<td style="border-top: #000 1px solid; border-right: #000 1px solid; border-bottom: #000 1px solid;">'.
		$enrollment->login
		.'</td>
		</tr>
		<tr>
		<td colspan="7" style="line-height:0; height: 4px;">
		</td>
		</tr>
		<tr>
		<td class="title">
		STUDENT No.
		</td>
		<td colspan="3" style="border-top: #000 1px solid; border-right: #000 1px solid;">
		</td>
		<td>
		</td>
		<td class="title">
		COLLEGE
		</td>
		<td style="border-top: #000 1px solid; border-right: #000 1px solid;">
		</td>
		</tr>
		<tr>
		<td class="title">
		STUDENT NAME
		</td>
		<td colspan="3" style="border-top: #000 1px solid; border-right: #000 1px solid; text-transform:capitalize;">'.
		$enrollment->last_name .', '. $enrollment->first_name .' '. $enrollment->middle_name
		.'</td>
		<td>
		</td>
		<td class="title">
		COURSE
		</td>
		<td style="border-top: #000 1px solid; border-right: #000 1px solid;">'.
		$enrollment->course
		.'</td>
		</tr>
		<tr>
		<td class="title">
		CASHIER\'S CODE
		</td>
		<td colspan="3" style="border-top: #000 1px solid; border-right: #000 1px solid; text-transform:capitalize;">'.
		 $ci->session->userdata['username']
		.'</td>
		<td>
		</td>
		<td class="title">
		MAJOR
		</td>
		<td style="border-top: #000 1px solid; border-right: #000 1px solid;">
		</td>
		</tr>
		<tr>
		<td class="title title2" style="width:150px;">
		DATE
		</td>
		<td style="width:135px; border-top: #000 1px solid; border-bottom: #000 1px solid;">
		'.
		date("F d, Y",$gmdate)
		.'</td>
		<td class="title title2" style="width:50px; text-align:center;">
		TIME
		</td>
		<td style="width:105px; border-top: #000 1px solid; border-right: #000 1px solid; border-bottom: #000 1px solid;">
		'.
		date("h:i:s A",$gmdate)
		.'</td>
		<td style="width:5px;">
		</td>
		<td class="title title2" style="width:100px;">
		S.Y./SEMESTER
		</td>
		<td style="width:250px; border-top: #000 1px solid; border-right: #000 1px solid; border-bottom: #000 1px solid;">
		'.
		$enrollment->sy_from .' - '. $enrollment->sy_to .' / '. $enrollment->name
		.'</td>
		</tr>
		<tr>
		<td colspan="7" style="line-height:0; height: 4px;">
		</td>
		</tr>
		</table>
		</div>

		<div>
		<table width="600px" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td class="title" style="text-align:center; width:340px;">
		ENROLLED SUBJECTS/CLASS SCHEDULE
		</td>
		<td style="width:5px; padding:0px;"></td>
		<td class="title" style="text-align:center; width:255px;">
		MISCELLANEOUS & OTHER FEES
		</td>
		</tr>
		<tr>
		<td style="border-top: #000 1px solid; border-right: #000 1px solid; border-left: #000 1px solid; padding:0px;">

		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<th>Subject Code</th>
		<th>Subject Description</th>
		<th>Units</th>
		<th>Time</th>
		<th>Day</th>
		</tr>';

		foreach($student_subjects as $s)
		{
		$html .= '<tr><td>'.$s->code.'</td><td>'.$s->subject.'</td><td>'.$s->units.'</td><td>'.$s->time.'</td><td>'.$s->day.'</td></tr>';
		}

		$html .= '
		</table>

		</td>
		<td style="width:5px;"></td>
		<td style="border-top: #000 1px solid; border-right: #000 1px solid; border-left: #000 1px solid; padding:0px;">

		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td style="width:100px; font-weight:bold; text-align:center; border-right: #000 1px solid;">
		AMOUNT
		</td>
		<td style="width:155px; font-weight:bold; text-align:center;">
		DESCRIPTION
		</td>
		</tr>
		<tr>
		<td style="width:100px; text-align:right; border-right: #000 1px solid;">'.
		number_format($student_total->total_misc_fee,2,'.',' ')
		.'</td>
		<td style="width:155px;">
		MISCELLANEOUS FEE
		</td>
		</tr>
		<tr>
		<td style="width:100px; text-align:right; border-right: #000 1px solid;">
		&nbsp;
		</td>
		<td style="width:155px; font-weight:bold;">
		OTHER FEES
		</td>
		</tr>';

		foreach($studentfees as $sf):
			if($sf->is_other == 1){
			$html .= '<tr>';
				if(strtolower($sf->name) != "tuition fee per unit" || strtolower($sf->name) != "laboratory fee per unit" || strtolower($sf->name) != "rle"){
					$html .= '<td style="width:100px; text-align:right; border-right: #000 1px solid;">'.number_format($sf->value,2,'.',' ').'</td><td style="width:155px;">'.$sf->name.'</td>';
				}
			$html .= '</tr>';
			}
		endforeach;

		$html .= '</table>

		</td>
		</tr>
		<tr>
		<td class="title" style="text-align:center; width:340px;">
		TOTAL NUMBER OF UNITS - '.
		number_format($student_total->total_units,2,'.',' ')
		.'</td>
		<td style="width:5px;"></td>
		<td class="title" style="text-align:center; width:255px;">
		COMPUTATION
		</td>
		</tr>
		<tr>
		<td class="title" style="text-align:center; width:340px;">
		CERTIFICATION
		</td>
		<td style="width:5px;"></td>
		<td style="border-right: #000 1px solid; border-top: #000 1px solid; border-left: #000 1px solid; padding:0px;">

		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td style="width:100px; text-align:right; border-right: #000 1px solid;">'.
		number_format($student_total->tuition_fee + $student_total->lab_fee,2,'.',' ')
		.'</td>
		<td style="width:155px; font-weight:bold;">
		TUITION FEE
		</td>
		</tr>
		</table>

		</td>
		</tr>
		<tr>
		<td style="border-top: #000 1px solid; border-right: #000 1px solid; border-left: #000 1px solid; width:340px;">
		I hereby certify that I have read and fully understand the terms and conditions of my enrollment in Dr. Yanga\'s Colleges, Inc. written at the back of this Certificate of Matriculation. Furthermore, I hereby agree to pay the total payable fees based on the schedule of payment below:
		</td>
		<td style="width:5px;"></td>
		<td style="border-right: #000 1px solid; border-left: #000 1px solid; padding:0px;">

		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td style="width:100px; text-align:right; border-right: #000 1px solid;">'.
		number_format($student_total->total_misc_fee,2,'.',' ')
		.'</td>
		<td style="width:155px; font-weight:bold;">
		MISCELANEOUS FEE
		</td>
		</tr>
		<tr>
		<td style="width:100px; text-align:right; border-right: #000 1px solid;">'.
		number_format($student_total->total_other_fee,2,'.',' ')
		.'</td>
		<td style="width:155px; font-weight:bold;">
		OTHER FEES
		</td>
		</tr>
		<tr>
		<td style="width:100px; text-align:right; border-right: #000 1px solid;">'.
		number_format($student_total->total_rle,2,'.',' ')
		.'</td>
		<td style="width:155px; font-weight:bold;">
		RELATED EXPERIENCE LEARNING
		</td>
		</tr>
		<tr>
		<td style="width:100px; text-align:right; border-right: #000 1px solid;">'.
		number_format($student_total->total_deduction + $student_total->total_payment,2,'.',' ')
		.'</td>
		<td style="width:155px; font-weight:bold;">
		LESS: DISCOUNT
		</td>
		</tr>
		<tr>
		<td style="width:100px; text-align:right; border-right: #000 1px solid;">
		&nbsp;
		</td>
		<td style="width:155px; font-weight:bold;">
		&nbsp;
		</td>
		</tr>
		</table>

		</td>
		</tr>
		<tr>
		<td style="border-right: #000 1px solid; border-bottom: #000 1px solid; border-left: #000 1px solid; width:340px; font-weight:bold;">
		SIGNATURE OF STUDENT
		</td>
		<td style="width:5px;"></td>
		<td style="border-right: #000 1px solid; border-bottom: #000 1px solid; border-left: #000 1px solid; padding:0px;">

		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td style="width:100px; text-align:right; border-right: #000 1px solid; font-weight:bold;">'.
		number_format($student_total->remaining_balance,2,'.',' ')
		.'</td>
		<td style="width:155px; font-weight:bold;">
		TOTAL PAYABLE FEES
		</td>
		</tr>
		</table>

		</td>
		</tr>
		</table>
		</div>

		</td>
		</tr>
		</table>

		</div>


		</body>
		';
		return $html;
	}
	
	function _css(){
		$html = "
			<style type='text/css'>
				#report_container{
					font-family: Courier New;
					left-margin: 150px;
				}
				
				.table{
					font-size:8pt;
				}
				
				.school_name{
					font-size:13pt;
					font-weight:bold;
				}
				.title{
					font-size:11pt;
				}
				.subtitle{
					font-size:8pt;
				}
				th{
					border-top: thin solid black;
					border-bottom: thin solid black;
					text-align: left;
				}
				
				.line_top td{
					border-top: thin solid black;
				}

				.text-left{
					text-align:left;
				}
				.text-right{
					text-align:right;
				}
				.text-center{
					text-align:center;
				}
			</style>
		";
		
		return $html;
	}
	
	function _html_daily_collections($start_date, $end_date, $data){
	
		$ci =& get_instance();
		
		$school_name = $ci->setting->school_name;
		
		$title = "Daily Collection";
		$subtitle = "Date From : " . date('m-d-Y', strtotime($start_date))." To : ".date('m-d-Y', strtotime($end_date));
		
		$html ="<html>";
		$html .="<head>";
		$html .="<title>$title</title>";
		$html .= _css(); //GET CSS
		$html .="</head>";
		$html .="<body>";
		$html .="<div id='report_container'>";
		$html .= "
			<div class='school_name'>{$school_name}</div>
			<div class='title'>{$title}</div>
			<div class='subtitle'>{$subtitle}</div><br/>
			<table class='table'>
			<tr>
				<th>#</th>
				<th>Student Name</th>
				<th>Receipt Number</th>
				<th>Date Of Payment</th>
				<th>Mode Of Payment</th>
				<th>Remarks</th>
				<th>Amount Paid</th>
			</tr>
		";
		
		if($data){
		$ctr = 0;
		$tot = 0;
		foreach($data as $obj){
			$tot += $obj->spr_ammt_paid;
			$ctr++;
			$html .= "<tr>
					<td>".$ctr.".</td>
					<td>".strtoupper($obj->name)."</td>
					<td>".$obj->spr_or_no."</td>
					<td>".date('m-d-Y', strtotime($obj->spr_payment_date))."</td>
					<td>".$obj->spr_mode_of_payment."</td>
					<td>".$obj->spr_remarks."</td>
					<td>".number_format($obj->spr_ammt_paid, 2)."</td>
				</tr>";
		}
		$html .= "<tr>
					<td  colspan='6' align='left' ><strong>Total</strong></td>
					<td>".number_format($tot, 2)."</td>
				</tr>";
		}
		$html .= "</table></div>";
		$html .= "</body>";
		$html .= "</html>";
		// vd(htmlspecialchars($html));
		return $html;
	}

	function _html_group_payments($start_date, $end_date, $data, $type = ""){
	
		$ci =& get_instance();
		
		$school_name = $ci->setting->school_name;
		
		$title = "Payment Record Report of $type";
		$subtitle = "Date From : " . date('m-d-Y', strtotime($start_date))." To : ".date('m-d-Y', strtotime($end_date));
		
		$html ="<html>";
		$html .="<head>";
		$html .="<title>$title</title>";
		$html .= _css(); //GET CSS
		$html .="</head>";
		$html .="<body>";
		$html .="<div id='report_container'>";
		$html .= "
			<div class='school_name'>{$school_name}</div>
			<div class='title'>{$title}</div>
			<div class='subtitle'>{$subtitle}</div><br/>
			<table class='table'>
			<tr>
				<th>#</th>
				<th>Student Name</th>
				<th>OR #</th>
				<th>GROUP OR #</th>
				<th>Remarks</th>
				<th>Amount</th>
			</tr>
		";
		
		if($data){
		$ctr = 0;
		foreach($data as $obj){
			$ctr++;
			$html .= "<tr>
					<td>".$ctr.".</td>
					<td>".strtoupper($obj->name)."</td>
					<td>".$obj->or_no."</td>
					<td>".$obj->group_or_no."</td>
					<td>".$obj->remarks."</td>
					<td>".$obj->account_recivable_student."</td>
				</tr>";
		}
		}
		$html .= "</table></div>";
		$html .= "</body>";
		$html .= "</html>";
		// vd(htmlspecialchars($html));
		return $html;
	}
	
	function _html_borrow_items($data)
	{
		$ci =& get_instance();
		
		$school_name = $ci->setting->school_name;
		
		$title = "Borrowed Items Report";
		$subtitle = "Date Printed : ".date('m-d-Y g:h:s');
		
		$borrow_items = $data['borrow_items'];
		$demands_return = $data['demands_return'];
		$demands_return_lost = $data['demands_return_lost'];
		
		$html ="<html>";
		$html .="<head>";
		$html .="<title>$title</title>";
		$html .= _css(); //GET CSS
		$html .="</head>";
		$html .="<body>";
		$html .="<div id='report_container'>";
		$html .= "
			<div class='school_name'>{$school_name}</div>
			<div class='title'>{$title}</div>
			<div class='subtitle'>{$subtitle}</div><br/>
			 <div>
				<label>Borrow No.</label> : <label class = 'label label-info'>".$borrow_items->borrow_no."</label><br/>
				<label>Date Borrowed</label> : <label class = 'label label-info'>".$borrow_items->date."</label><br/>
				<label>Borrower</label> : <label class = 'label label-info'>".$borrow_items->name."</label><br/>
				<label>Items</label> : <label class = 'label label-info'>".$borrow_items->item."</label><br/>
				<label>Unit Borrowed</label> : <label class = 'label label-info'>".$borrow_items->unit."</label><br/>
			 </div>
		";
		
		$html .= "<br/>
			 <table width='100%'>
				<tr>
					<td colspan='3' style='text-align:left'>Item Return</td>
				</tr>
				<tr>
					<th style='text-align:left'>Date</th>
					<th style='text-align:left'>Quantity</th>
					<th style='text-align:left'>Remarks</th>
				</tr>";
				if($demands_return){
					foreach($demands_return as $obj)
						$html .= "<tr>
							<td>$obj->date_return</td>
							<td>$obj->unit_return</td>
							<td>$obj->remarks</td>
						</tr>";
				}
			 $html .= "</table><br/>
			  <table width='100%'>
				<tr>
					<td colspan='4' style='text-align:left'>Item Lost/Damage Return</td>
				</tr>
				<tr>
					<th style='text-align:left;border-top: thin solid black;'>Date</th>
					<th style='text-align:left'>Quantity</th>
					<th style='text-align:left'>Category</th>
					<th style='text-align:left'>Remarks</th>
				</tr>";
				if($demands_return_lost)
				{
					foreach($demands_return_lost as $obj){
						$html .= "<tr>
							<td>$obj->date_return</td>
							<td>$obj->unit_return</td>
							<td>$obj->category</td>
							<td>$obj->remarks</td>
						</tr>";
					}
				}
			 $html .= "</table>";
		
		return $html;
	}
	
	function _html_student_scholars($data)
	{
		$ci =& get_instance();
		$sy = $ci->open_semester->year_from .' - '.$ci->open_semester->year_to;
		$semester = $ci->open_semester->name;
		$school_name = $ci->setting->school_name;
		
		$title = "Student Scholar Report";
		$subtitle = "Date Printed : ".date('m-d-Y g:h:s');
		
		$demands_return = $data['search'];
		// $demands_return = null;
		
		$html ="<html>";
		$html .="<head>";
		$html .="<title>$title</title>";
		$html .= _css(); //GET CSS
		$html .="</head>";
		$html .="<body>";
		$html .="<div id='report_container'>";
		$html .= "
			<div class='school_name'>{$school_name}</div>
			<div class='title'>{$title}</div>
			<div class='subtitle'>{$semester}   of  {$sy}</div>
			<div class='subtitle'>{$subtitle}</div><br/>
		";
		
		$html .= "<br/>
			 <table width='100%'>
				<tr>
					<th style='text-align:left'>Student ID</th>
					<th style='text-align:left'>Name</th>
					<th style='text-align:left'>Year</th>
					<th style='text-align:left'>Course</th>
				</tr>";
				if($demands_return){
					foreach($demands_return as $obj)
					{
						$html .= "<tr>
							<td>$obj->studid</td>
							<td>$obj->name</td>
							<td>$obj->year</td>
							<td>$obj->course</td>
						</tr>";
					}
					$html .= "<tr class='line_top'>
								<td colspan='3' style='text-align:right'><b>Total Scholar</b></td>
								<td><b>".$data['total_rows']."</b></td>
							</tr>";
				}
			 $html .= "</table>";
		
		return $html;
	}
	
	function _html_nstp_students($data)
	{
		$ci =& get_instance();
		$sy = $ci->open_semester->year_from .' - '.$ci->open_semester->year_to;
		$semester = $ci->open_semester->name;
		$school_name = $ci->setting->school_name;
		
		$title = "NSTP Students Report";
		$subtitle = "Date Printed : ".date('m-d-Y g:h:s');
		
		$demands_return = $data['search'];
		// $demands_return = null;
		
		$html ="<html>";
		$html .="<head>";
		$html .="<title>$title</title>";
		$html .= _css(); //GET CSS
		$html .="</head>";
		$html .="<body>";
		$html .="<div id='report_container'>";
		$html .= "
			<div class='school_name'>{$school_name}</div>
			<div class='title'>{$title}</div>
			<div class='subtitle'>{$semester}   of  {$sy}</div>
			<div class='subtitle'>{$subtitle}</div><br/>
		";
		
		$html .= "<br/>
			 <table width='100%'>
				<tr>
					<th style='text-align:left'>Student ID</th>
					<th style='text-align:left'>Name</th>
					<th style='text-align:left'>Year</th>
					<th style='text-align:left'>Course</th>
				</tr>";
				if($demands_return){
					foreach($demands_return as $obj)
					{
						$html .= "<tr>
							<td>$obj->studid</td>
							<td>$obj->name</td>
							<td>$obj->year</td>
							<td>$obj->course</td>
						</tr>";
					}
					$html .= "<tr class='line_top'>
								<td colspan='3' style='text-align:right'><b>Total NSTP Students</b></td>
								<td><b>".$data['total_rows']."</b></td>
							</tr>";
				}
			 $html .= "</table>";
		
		return $html;
	}
	
	function _html_dropped_students($data)
	{
		$ci =& get_instance();
		$sy = $ci->open_semester->year_from .' - '.$ci->open_semester->year_to;
		$semester = $ci->open_semester->name;
		$school_name = $ci->setting->school_name;
		
		$title = "Dropped Students Report";
		$subtitle = "Date Printed : ".date('m-d-Y g:h:s');
		
		$demands_return = $data['search'];
		// $demands_return = null;
		
		$html ="<html>";
		$html .="<head>";
		$html .="<title>$title</title>";
		$html .= _css(); //GET CSS
		$html .="</head>";
		$html .="<body>";
		$html .="<div id='report_container'>";
		$html .= "
			<div class='school_name'>{$school_name}</div>
			<div class='title'>{$title}</div>
			<div class='subtitle'>{$semester}   of  {$sy}</div>
			<div class='subtitle'>{$subtitle}</div><br/>
		";
		
		$html .= "<br/>
			 <table width='100%'>
				<tr>
					<th style='text-align:left'>Student ID</th>
					<th style='text-align:left'>Name</th>
					<th style='text-align:left'>Year</th>
					<th style='text-align:left'>Enrollment Date</th>
				</tr>";
				if($demands_return){
					foreach($demands_return as $obj)
					{
						$html .= "<tr>
							<td>$obj->studid</td>
							<td>$obj->name</td>
							<td>$obj->year</td>
							<td>$obj->created_at</td>
						</tr>";
					}
					$html .= "<tr class='line_top'>
								<td colspan='3' style='text-align:right'><b>Total Dropped Students</b></td>
								<td><b>".$data['total_rows']."</b></td>
							</tr>";
				}
			 $html .= "</table>";
		
		return $html;
	}
	
	function _css_grade_slip($per_page = false)
	{
		$fontsize = 7;
		if($per_page)
		{
			switch($per_page)
			{
				case 3:
					$fontsize = '7';
				break;
					
				case 2:
					$fontsize = '8';
				break;
				
				case 1:
					$fontsize = '8';
				break;
				
				default:
					$fontsize = '7';
				break;
			}
		}
		
		$html .= "
						<style type='text/css'>
							.maindiv, table{
								font-family : arial;
								font-size : {$fontsize}pt;
							}
							.maindiv{
								
							}
							
							.mydiv{
								width : 100%;
								float : left;
							}
							
							.header_title{
								
							}
							
							.grade_slip{
								
							}
							
							.sub_title{
								
							}
							
							.subjects{
								
							}
							
							.center, .center span, .center p {
								text-align:center;
							}
							
						</style>
					";
		return $html;
	}
	
	
	function _html_grade_slip($students, $subjects, $per_page = 1)
	{
		$ci =& get_instance();
		$ci->load->model('M_grades_file');
		// $this->M_grades_file->get_final_grade($eid,$ss_id);
		$sy = $ci->open_semester->year_from .' - '.$ci->open_semester->year_to;
		
		$base_url = base_url();
		
		$height = (100 / (int)$per_page) - 1;
		for($x = 0; $x <= $per_page-1; $x++)
		{
			$html .= "
				<div class='maindiv' style='height: {$height}%; width: 100%; float:left;'>";
					$student = $students[$x];
					
						$html.=	"
								<div class='header_title mydiv' >
									<div class='center'>
											<span class='school_name'>
												{$ci->setting->school_name}
											</span><br/>
											<span>{$ci->setting->school_address}</span><br/>
											<span>{$ci->setting->school_telephone}</span><br/>
											<span>{$ci->setting->email}</span><br/>
									</div>
								</div>
								
								<div class='grade_slip mydiv center'>
									<p>Grade Slip</p>
								</div>
								
								<div class='sub_title mydiv'>
									<table><tr>
										<td style='width:20%;'>Time : &nbsp; ".date('g:h A')."</td>
										<td style='width:20%;'>Date : &nbsp; ".date('M d, Y')."</td>
										<td style='width:20%;'>Term : &nbsp; ".$student->semester."</td>
										<td style='width:20%;'>SY : &nbsp; ".$student->sy_from."-".$student->sy_to."</td>
										<td style='width:20%;'>Admission Status : &nbsp; ".$student->status."</td>
									</tr></table>
								</div>
								
								<div class='sub_title mydiv'>
									<table><tr>
										<td style='width:20%;'><b>ID Number : &nbsp; ".$student->studid."</b></td>
										<td style='width:20%;text-align:right;'><b>Student Name : &nbsp; ".strtoupper($student->name)."</b></td>
									</tr></table>
								</div>
								
								<div class='grade_slip mydiv center'>
									<table><tr>
										<td style='width:20%;'><b>Course : &nbsp; ".$student->course."</b></td>
									</tr></table>
								</div>
								
								<div class='subjects mydiv'>
									<table width='100%'>
										<tr>
											<th>Course No.</th>
											<th>Subject Description</th>
											<th class='text-center' >Units</th>
											<th class='text-center' >Grade</th>
											<th>Remarks</th>
										</tr>";
										
										if(isset($subjects[$student->id]) && is_array($subjects[$student->id])){
											
											foreach($subjects[$student->id] as $sub):
											// $final_grade = $ci->M_grades_file->get_final_grade($student->id, $sub->id);		
									$html .= "
												<tr>
													<td>{$sub->code}</td>
													<td>{$sub->subject}</td>
													<td class='text-center' >{$sub->units}</td>
													<td class='text-center' >{$sub->converted}</td>
													<td>{$sub->remarks}</td>
												</tr>
											";
											
											endforeach;
											
										}
										
										
						$html .=	"</table>
								</div>
								
								<div class='grade_slip mydiv center'>
									<br/>
									<br/>
									<table><tr>
										<td style='border-top: thin solid black;'>".$ci->session->userdata['username']."</td>
									</tr></table>
								</div>
				</div>";
		}
		// echo $html;
		// exit(0);
		return $html;
	}
	
	function _html_exam_permit($students, $subjects, $per_page)
	{
		$ci =& get_instance();
		$sy = $ci->open_semester->year_from .' - '.$ci->open_semester->year_to;
		
		$base_url = base_url();
		
		$height = (100 / (int)$per_page) - 1;
		for($x = 0; $x <= $per_page-1; $x++)
		{
			$student = $students[$x];
			
			if($student)
			{
			$html .= "
				<div class='maindiv' style='height: {$height}%; width: 100%; float:left;'>";		
						$html.=	"
								<div class='header_title mydiv' >
									<div class='center'>
											<span class='school_name'>
												<img style='width:45px;height:45px;' src='{$base_url}{$ci->setting->logo}'/>
												{$ci->setting->school_name}
											</span><br/>
											<span>{$ci->setting->school_address}</span><br/>
											<span>{$ci->setting->school_telephone}</span><br/>
											<span>{$ci->setting->email}</span><br/>
									</div>
								</div>
								
								<div class='grade_slip mydiv center'>
									<p>Exam Permit</p>
								</div>
								
								<div class='sub_title mydiv'>
									<table><tr>
										<td style='width:20%;'>Time : &nbsp; ".date('g:h A')."</td>
										<td style='width:20%;'>Date : &nbsp; ".date('M d, Y')."</td>
										<td style='width:20%;'>Term : &nbsp; ".$student->semester."</td>
										<td style='width:20%;'>SY : &nbsp; ".$student->sy_from."-".$student->sy_to."</td>
										<td style='width:20%;'>Admission Status : &nbsp; ".$student->status."</td>
									</tr></table>
								</div>
								
								<div class='sub_title mydiv'>
									<table><tr>
										<td style='width:20%;'><b>ID Number : &nbsp; ".$student->studid."</b></td>
										<td style='width:20%;text-align:right;'><b>Student Name : &nbsp; ".strtoupper($student->name)."</b></td>
									</tr></table>
								</div>
								
								<div class='grade_slip mydiv center'>
									<table><tr>
										<td style='width:20%;'><b>Course : &nbsp; ".$student->course."</b></td>
									</tr></table>
								</div>
								
								<div class='subjects mydiv'>
									<table width='100%'>
										<tr>
											<th>Course No.</th>
											<th>Subject Description</th>
											<th>Units</th>
											<th>Lab</th>
											<th>Instructor's Signature</th>
											<th>Date &nbsp; &nbsp; &nbsp; </th>
											<th>Remarks &nbsp; &nbsp; &nbsp;</th>
										</tr>";
										// vd($subjects[$student->id]);
										if(isset($subjects[$student->id]) && is_array($subjects[$student->id])){
											
											foreach($subjects[$student->id] as $sub):
									// vd($sub);		
									$html .= "
												<tr>
													<td>{$sub->code}</td>
													<td>{$sub->subject}</td>
													<td>{$sub->units}</td>
													<td>{$sub->lab}</td>
													<td style='border-bottom: thin solid gray;' ></td>
													<td style='border-bottom: thin solid gray;' ></td>
													<td style='border-bottom: thin solid gray;' ></td>
												</tr>
											";
											
											endforeach;
											
										}
										
										
						$html .=	"</table>
								</div>
								
								<div class='grade_slip mydiv center'>
									<br/>
									<br/>
									<table><tr>
										<td style='border-top: thin solid black;'>".$ci->session->userdata['username']."</td>
									</tr></table>
								</div>
				</div>";
			}
		}
		
		return $html;
	}
		
	function _html_total_income($data, $total_income=0)
	{
		$ci =& get_instance();
		$sy = $ci->open_semester->year_from .' - '.$ci->open_semester->year_to;
		$semester = $ci->open_semester->name;
		$school_name = $ci->setting->school_name;
		
		$title = "Total Income Report";
		$subtitle = "Date Printed : ".date('m-d-Y g:h:s');
		
		$html ="<html>";
		$html .="<head>";
		$html .="<title>$title</title>";
		$html .= _css(); //GET CSS
		$html .="</head>";
		$html .="<body>";
		$html .="<div id='report_container'>";
		$html .= "
			<div class='school_name'>{$school_name}</div>
			<div class='title'>{$title}</div>
			<div class='subtitle'>{$semester}   of  {$sy}</div>
			<div class='subtitle'>{$subtitle}</div><br/>
		";
		
		$html .= "<br/>
			 <table width='100%' style='font-size:8pt;'>
				<tr>
					<th style='text-align:left'>OR. #</th>
					<th style='text-align:left'>Date</th>
					<th style='text-align:left'>Student ID</th>
					<th style='text-align:left'>Name</th>
					<th style='text-align:left'>Year</th>
					<th style='text-align:left'>Course</th>
					<th style='text-align:left'>Payment</th>
				</tr>";
				if($data){
					foreach($data as $obj)
					{
						$html .= "<tr>
							<td>$obj->or_no</td>
							<td>".date('m-d-Y', strtotime($obj->date))."</td>
							<td>$obj->studid</td>
							<td>".ucfirst($obj->name)."</td>
							<td>$obj->year</td>
							<td>$obj->course</td>
							<td>".number_format($obj->total, 2, '.', ' ')."</td>
						</tr>";
					}
					$html .= "<tr class='line_top'>
								<td colspan='6' style='text-align:right'><b>Total Income</b></td>
								<td><b>".number_format($total_income, 2, ',', ' ')."</b></td>
							</tr>";
				}
			 $html .= "</table>";
		
		return $html;
	}
	
	function _html_international_students($data, $total_records=0)
	{
		$ci =& get_instance();
		$sy = $ci->open_semester->year_from .' - '.$ci->open_semester->year_to;
		$semester = $ci->open_semester->name;
		$school_name = $ci->setting->school_name;
		
		$title = "International Student Report";
		$subtitle = "Date Printed : ".date('m-d-Y g:h:s');
		
		$html ="<html>";
		$html .="<head>";
		$html .="<title>$title</title>";
		$html .= _css(); //GET CSS
		$html .="</head>";
		$html .="<body>";
		$html .="<div id='report_container'>";
		$html .= "
			<div class='school_name'>{$school_name}</div>
			<div class='title'>{$title}</div>
			<div class='subtitle'>{$semester}   of  {$sy}</div>
			<div class='subtitle'>{$subtitle}</div><br/>
		";
		
		$html .= "<br/>
			 <table width='100%' style='font-size:8pt;'>
				<tr>
					<th style='text-align:left'>Student ID</th>
					<th style='text-align:left'>Name</th>
					<th style='text-align:left'>Nationality</th>
					<th style='text-align:left'>Year</th>
					<th style='text-align:left'>Course</th>
				</tr>";
				if($data){
					foreach($data as $obj)
					{
						$html .= "<tr>
							<td>$obj->studid</td>
							<td>".ucfirst($obj->name)."</td>
							<td>$obj->nationality</td>
							<td>$obj->year</td>
							<td>$obj->course</td>
						</tr>";
					}
					$html .= "<tr class='line_top'>
								<td colspan='4' style='text-align:right'><b>Total Students</b></td>
								<td><b>".$total_records."</b></td>
							</tr>";
				}
			 $html .= "</table>";
		
		return $html;
	}
		
	function _html_promisory_students($view_data)
	{
		
		$ci =& get_instance();
		$sy = $ci->open_semester->year_from .' - '.$ci->open_semester->year_to;
		$semester = $ci->open_semester->name;
		$school_name = $ci->setting->school_name;
		
		$title = "Student Promisory Report";
		$subtitle = "Date Printed : ".date('m-d-Y g:h:s');
		
		$data = $view_data['student_w_promisory'];
		$total_records = $view_data['total_rows'];
		$current_period = $view_data['current_period'];
		$period = $current_period->grading_period;
		
		$html ="<html>";
		$html .="<head>";
		$html .="<title>$title</title>";
		$html .= _css(); //GET CSS
		$html .="</head>";
		$html .="<body>";
		$html .="<div id='report_container'>";
		$html .= "
			<div class='school_name'>{$school_name}</div>
			<div class='title'>{$title}</div>
			<div class='subtitle'>{$period} | {$semester}   of  {$sy}</div>
			<div class='subtitle'>{$subtitle}</div><br/>
		";
		
		$html .= "<br/>
			 <table width='100%' style='font-size:8pt;'>
				<tr>
					<th style='text-align:left'>Student ID</th>
					<th style='text-align:left'>Name</th>
					<th style='text-align:left'>Year</th>
					<th style='text-align:left'>Course</th>
				</tr>";
				if($data){
					foreach($data as $obj)
					{
						$html .= "<tr>
							<td>$obj->studid</td>
							<td>".ucfirst($obj->name)."</td>
							<td>$obj->year</td>
							<td>$obj->course</td>
						</tr>";
					}
					$html .= "<tr class='line_top'>
								<td colspan='3' style='text-align:right'><b>Total Students</b></td>
								<td><b>".$total_records."</b></td>
							</tr>";
				}
			 $html .= "</table>";
		
		return $html;
	}
	
	function _html_promisory_students_list($view_data)
	{
		
		$ci =& get_instance();
		$sy = $ci->open_semester->year_from .' - '.$ci->open_semester->year_to;
		$semester = $ci->open_semester->name;
		$school_name = $ci->setting->school_name;
		
		$contains = $view_data['contains'];
		$students_payment_details = $view_data['students_payment_details'];
		

		switch($contains)
		{
			case "ALL":
				$title = "Students List for exam permit report";
			break;
			
			case "PAID":
				$title = "Paid students elligible for permit report";
			break;
			
			case "PROMISORY":
				$title = "Students with promisory elligible for permit report";
			break;
			
			case "BOTH":
				$title = "Paid students and students with promisory elligible for permit";
			break;
			
			case "NONE":
				$title = "Students without payment or without promisory";
			break;
			
			default:
				$title = "Students List for exam permit";
			break;
			
		}
		
		
		$subtitle = "Date Printed : ".date('m-d-Y g:h:s');
		
		$data = $view_data['students'];
		$total_records = $view_data['total_rows'];
		$current_period = $view_data['current_period'];
		$period = $current_period->grading_period;
		
		$html ="<html>";
		$html .="<head>";
		$html .="<title>$title</title>";
		$html .= _css(); //GET CSS
		$html .="</head>";
		$html .="<body>";
		$html .="<div id='report_container'>";
		$html .= "
			<div class='school_name'>{$school_name}</div>
			<div class='title'>{$title}</div>
			<div class='subtitle'>{$period} | {$semester}   of  {$sy}</div>
			<div class='subtitle'>{$subtitle}</div><br/>
		";
		
		$html .= "<br/>
			 <table width='100%' style='font-size:8pt;'>
				<tr>
					<th style='text-align:left'>#</th>
					<th style='text-align:left'>Student ID</th>
					<th style='text-align:left'>Name</th>
					<th style='text-align:left'>Year</th>
					<th style='text-align:left'>Course</th>
					<th style='text-align:left'>Is Paid</th>
				</tr>";
				if($data){
					$i = 0;
					foreach($data as $key => $obj)
					{
						$i++;
						
						$is_paid = "";
						$is_promisory = "";
						
						if(isset($students_payment_details[$key]))
						{
							$y = $students_payment_details[$key];
							$is_paid = "Yes";
						}
						
						
						$html .= "<tr>
							<td>{$i}.</td>
							<td>$obj->studid</td>
							<td>".ucfirst($obj->name)."</td>
							<td>$obj->year</td>
							<td>$obj->course</td>
							<td style='text-align:center'>$is_paid</td>
						</tr>";
					}
					$html .= "<tr class='line_top'>
								<td colspan='5' style='text-align:right'><b>Total Students : </b></td>
								<td><b>".$total_records."</b></td>
							</tr>";
				}
			 $html .= "</table>";
		
		return $html;
	}

	function _x_old_html_promisory_students_list($view_data)
	{
		
		$ci =& get_instance();
		$sy = $ci->open_semester->year_from .' - '.$ci->open_semester->year_to;
		$semester = $ci->open_semester->name;
		$school_name = $ci->setting->school_name;
		
		$contains = $view_data['contains'];
		$students_payment_details = $view_data['students_payment_details'];
		

		switch($contains)
		{
			case "ALL":
				$title = "Students List for exam permit report";
			break;
			
			case "PAID":
				$title = "Paid students elligible for permit report";
			break;
			
			case "PROMISORY":
				$title = "Students with promisory elligible for permit report";
			break;
			
			case "BOTH":
				$title = "Paid students and students with promisory elligible for permit";
			break;
			
			case "NONE":
				$title = "Students without payment or without promisory";
			break;
			
			default:
				$title = "Students List for exam permit";
			break;
			
		}
		
		
		$subtitle = "Date Printed : ".date('m-d-Y g:h:s');
		
		$data = $view_data['students'];
		$total_records = $view_data['total_rows'];
		$current_period = $view_data['current_period'];
		$period = $current_period->grading_period;
		
		$html ="<html>";
		$html .="<head>";
		$html .="<title>$title</title>";
		$html .= _css(); //GET CSS
		$html .="</head>";
		$html .="<body>";
		$html .="<div id='report_container'>";
		$html .= "
			<div class='school_name'>{$school_name}</div>
			<div class='title'>{$title}</div>
			<div class='subtitle'>{$period} | {$semester}   of  {$sy}</div>
			<div class='subtitle'>{$subtitle}</div><br/>
		";
		
		$html .= "<br/>
			 <table width='100%' style='font-size:8pt;'>
				<tr>
					<th style='text-align:left'>#</th>
					<th style='text-align:left'>Student ID</th>
					<th style='text-align:left'>Name</th>
					<th style='text-align:left'>Year</th>
					<th style='text-align:left'>Course</th>
					<th style='text-align:left'>Is Paid</th>
					<th style='text-align:left'>Is Promisory</th>
				</tr>";
				if($data){
					$i = 0;
					foreach($data as $key => $obj)
					{
						$i++;
						
						$is_paid = "";
						$is_promisory = "";
						
						if(isset($students_payment_details[$key]))
						{
							$y = $students_payment_details[$key];
							$is_paid = $y->is_paid == 1 ? "Yes" : "No";
							$is_promisory = $y->is_promisory == 1 ? "Yes" : "No";
						}
						
						
						$html .= "<tr>
							<td>{$i}.</td>
							<td>$obj->studid</td>
							<td>".ucfirst($obj->name)."</td>
							<td>$obj->year</td>
							<td>$obj->course</td>
							<td style='text-align:center'>$is_paid</td>
							<td style='text-align:center'>$is_promisory</td>
						</tr>";
					}
					$html .= "<tr class='line_top'>
								<td colspan='6' style='text-align:right'><b>Total Students</b></td>
								<td><b>".$total_records."</b></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>";
				}
			 $html .= "</table>";
		
		return $html;
	}
	
	function _html_student_master_list_all($view_data)
	{
		
		$ci =& get_instance();
		$sy = $ci->open_semester->year_from .' - '.$ci->open_semester->year_to;
		$semester = $ci->open_semester->name;
		$school_name = $ci->setting->school_name;
		
		$data = $view_data['all_rows'];
		$total_records = $view_data['total_rows'];
		$title = "Students Master List Report";	
		
		$subtitle = "Date Printed : ".date('m-d-Y g:h:s');
		
		$html ="<html>";
		$html .="<head>";
		$html .="<title>$title</title>";
		$html .= _css(); //GET CSS
		$html .="</head>";
		$html .="<body>";
		$html .="<div id='report_container'>";
		$html .= "
			<div class='school_name'>{$school_name}</div>
			<div class='title'>{$title}</div>
			<div class='subtitle'>{$semester}   of  {$sy}</div>
			<div class='subtitle'>{$subtitle}</div><br/>
		";
		
		$html .= "<br/>
			 <table width='100%' style='font-size:8pt;'>
				<tr>
					<th style='text-align:left'>#</th>
					<th style='text-align:left'>Student ID</th>
					<th style='text-align:left'>Name</th>
					<th style='text-align:left'>Birthdate</th>
					<th style='text-align:left'>Year</th>
					<th style='text-align:left'>Course</th>
					<th style='text-align:left'>Gender</th>
				</tr>";
				if($data){
					$i = 0;
					$male = 0;
					$female = 0;
					foreach($data as $key => $obj)
					{
						$i++;
						if(strtoupper($obj->sex) == "MALE"){
							$male++;
						}
						if(strtoupper($obj->sex) == "FEMALE"){
							$female++;
						}
						$html .= "<tr>
							<td>{$i}.</td>
							<td>$obj->studid</td>
							<td>".ucfirst($obj->name)."</td>
							<td>".date('m-d-Y',strtotime($obj->date_of_birth))."</td>
							<td>$obj->year</td>
							<td>$obj->course</td>
							<td>$obj->sex</td>
						</tr>";
					}
					$html .= "<tr class='line_top' >
								<td colspan='5' style='text-align:right'>
									<b>Total Male &nbsp; $male </b> &nbsp; 
									<b>Total Female &nbsp; $female </b>
								</td>
								<td colspan='1' style='text-align:right'><b>Total Students</b></td>
								<td><b>".$total_records."</b></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>";
				}
			 $html .= "</table>";
		
		return $html;
	}
	
	function _html_student_master_list_year($view_data)
	{
		
		$ci =& get_instance();
		$sy = $ci->open_semester->year_from .' - '.$ci->open_semester->year_to;
		$semester = $ci->open_semester->name;
		$school_name = $ci->setting->school_name;
		
		$data = $view_data['search'];
		$total_records = count($data);
		$title = "Students Master List Report By Year";	
		
		$subtitle = "Date Printed : ".date('m-d-Y g:h:s');
		
		$html ="<html>";
		$html .="<head>";
		$html .="<title>$title</title>";
		$html .= _css(); //GET CSS
		$html .="</head>";
		$html .="<body>";
		$html .="<div id='report_container'>";
		$html .= "
			<div class='school_name'>{$school_name}</div>
			<div class='title'>{$title}</div>
			<div class='subtitle'>{$semester}   of  {$sy}</div>
			<div class='subtitle'>{$subtitle}</div><br/>
		";
		
		$html .= "<br/>
			 <table width='100%' style='font-size:8pt;'>
				<tr>
					<th style='text-align:left'>#</th>
					<th style='text-align:left'>Year</th>
					<th style='text-align:left'>Male</th>
					<th style='text-align:left'>Female</th>
					<th style='text-align:left'>Total</th>
				</tr>";
				if($data){
					$i = 0;
					$t_male = 0;
					$t_female = 0;
					foreach($data as $key => $obj)
					{
						$i++;
						
						$year = $obj['data'];
						$male = $obj['male'];
						$t_male += $male;
						
						$female = $obj['female'];
						$t_female += $female;
						$total = $obj['total'];
						
						$html .= "<tr>
							<td>{$i}.</td>
							<td>$year->year</td>
							<td>$male</td>
							<td>$female</td>
							<td>$total</td>
						</tr>";
					}
					$html .= "<tr class='line_top' >
									<td></td>
									<td></td>
									<td>".$t_male."</td>
									<td>".$t_female."</td>
									<td>".($t_male + $t_female)."</td>
							</tr>";
				}
			 $html .= "</table>";
		
		return $html;
	}
	
	function _html_student_master_list_list($view_data)
	{
		
		$ci =& get_instance();
		$sy = $ci->open_semester->year_from .' - '.$ci->open_semester->year_to;
		$semester = $ci->open_semester->name;
		$school_name = $ci->setting->school_name;
		
		$data = $view_data['all_rows'];
		$by_data = $view_data['by_data'];
		$type = $view_data['type'];
		$total_records = count($data);
		
		$title = "";
		
		switch($type)
		{
			case "year":
				$title = strtoupper($by_data->year." Students  List Report");	
			break;
			
			case "course":
				$title = strtoupper($by_data->course." Students List Report");	
			break;
		}
		
		$subtitle = "Date Printed : ".date('m-d-Y g:h:s');
		
		$html ="<html>";
		$html .="<head>";
		$html .="<title>$title</title>";
		$html .= _css(); //GET CSS
		$html .="</head>";
		$html .="<body>";
		$html .="<div id='report_container'>";
		$html .= "
			<div class='school_name'>{$school_name}</div>
			<div class='title'>{$title}</div>
			<div class='subtitle'>{$semester}   of  {$sy}</div>
			<div class='subtitle'>{$subtitle}</div><br/>
		";
		
		$html .= "<br/>
			 <table width='100%' style='font-size:8pt;'>
				<tr>
					<th style='text-align:left'>#</th>
					<th style='text-align:left'>Student ID</th>
					<th style='text-align:left'>Name</th>
					<th style='text-align:left'>Birthdate</th>
					<th style='text-align:left'>Year</th>
					<th style='text-align:left'>Course</th>
					<th style='text-align:left'>Gender</th>
				</tr>";
				if($data){
					$i = 0;
					$male = 0;
					$female = 0;
					foreach($data as $key => $obj)
					{
						$i++;
						if(strtoupper($obj->sex) == "MALE"){
							$male++;
						}
						if(strtoupper($obj->sex) == "FEMALE"){
							$female++;
						}
						$html .= "<tr>
							<td>{$i}.</td>
							<td>$obj->studid</td>
							<td>".ucfirst($obj->name)."</td>
							<td>".date('m-d-Y',strtotime($obj->date_of_birth))."</td>
							<td>$obj->year</td>
							<td>$obj->course</td>
							<td>$obj->sex</td>
						</tr>";
					}
					$html .= "<tr class='line_top' >
								<td colspan='5' style='text-align:right'>
									<b>Total Male &nbsp; $male </b> &nbsp; 
									<b>Total Female &nbsp; $female </b>
								</td>
								<td colspan='1' style='text-align:right'><b>Total Students</b></td>
								<td><b>".$total_records."</b></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>";
				}
			 $html .= "</table>";
		
		return $html;
	}

	function _html_subject_list($view_data)
	{
		$ci =& get_instance();
		$sy = $ci->open_semester->year_from .' - '.$ci->open_semester->year_to;
		$semester = $ci->open_semester->name;
		$school_name = $ci->setting->school_name;
		
		$data = $view_data['results'];
		$total_records = count($data);

		$title = "Subject List Report";
		
		$subtitle = "Date Printed : ".date('m-d-Y g:h:s');
		
		$html ="<html>";
		$html .="<head>";
		$html .="<title>$title</title>";
		$html .= _css(); //GET CSS
		$html .="</head>";
		$html .="<body>";
		$html .="<div id='report_container'>";
		$html .= "
			<div class='school_name'>{$school_name}</div>
			<div class='title'>{$title}</div>
			<div class='subtitle'>{$semester}   of  {$sy}</div>
			<div class='subtitle'>{$subtitle}</div><br/>
		";
		
		$html .= "<br/>
			 <table width='100%' style='font-size:8pt;'>
				<tr>
					 <th>Course Code</th>
					 <th>Course No.</th>
				    <th>Description</th>
				    <th>Unit</th>
				    <th>Lec</th>
				    <th>Lab</th>
				    <th>Time</th>
				    <th>Day</th>
				    <th>Room</th>
				    <th>Load</th>
				    <th>Instructor</th>
				    <th>Academic Year</th>
				</tr>";
				if($data){
					
					foreach($data as $key => $s)
					{
						$html .= "
							<tr>
								  <td width='10%'>$s->course_code</td>
								  <td>$s->code</td>
								  <td>".trim($s->subject)."</td>
								  <td>$s->units</td>
								  <td>$s->lec</td>
								  <td>$s->lab</td>
								  <td width='10%' >"._convert_to_12($s->time,$s->is_open_time)."</td>
								  <td>$s->day</td>
								  <td>$s->room</td>
								  <td>$s->subject_taken/$s->original_load</td>
								  <td>$s->instructor</td>
								  <td>$s->year_from - $s->year_to</td>
							</tr>";
					}
					$html .= "<tr class='line_top' >
								<td colspan='11' style='text-align:right'><b>Total Subjects</b></td>
								<td><b>".$total_records."</b></td>
							</tr>";
				}
			 $html .= "</table>";
			 $html .= "</body>";
			 $html .= "</html>";
		
		return $html;
	}

	function _html_student_statement_of_account($view_data){
		$ci =& get_instance();
		$html ="<html>";
		$html .="<head>";
		$html .="<title>$title</title>";
		$html .= _css(); //GET CSS
		$html .="</head>";
		$html .="<body>";
	
		$html .= "</body>";
		$html .= "</html>";

		return $html;
	}
