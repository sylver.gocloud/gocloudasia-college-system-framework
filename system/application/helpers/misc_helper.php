<?php
	
if(!function_exists('money'))
{	
	function money($number)
	{
		if(is_numeric($number))
		{
			return number_format($number, 2, '.',',');
		}else{
			return number_format(intval($number), 2, '.',',');
		}
	}
}

if(!function_exists('arrayToObject'))
{
	function arrayToObject($d)
	{
		if (is_array($d)) {
			/*
			* Return array converted to object
			* Using __FUNCTION__ (Magic constant)
			* for recursive call
			*/
			return (object) array_map(__FUNCTION__, $d);
		}else{
			// Return object
			return $d;
		}
	}
}

if(!function_exists('check_year'))
{
	function check_year($year)
	{
		if(@mktime(0,0,0,0,0,$year) == FALSE)
		{
			return date('Y');
		}else{
			return $year;
		}
	}
}

if(!function_exists('check_month'))
{
	function check_month($month)
	{
		if(@mktime(0,0,0,0,$month,0) == FALSE)
		{
			return date('m');
		}else{
			return $month;
		}
	}
}

if(!function_exists('mowney'))
{
	function mowney($value ='',$max = FALSE)
	{
		if($value < 0)
		{
			return number_format(0,2,'.',',');
		}else
		{
			if($max !== FALSE)
			{
				if($value > $max)
				{
					$return = $max;
				}else{
					$return = $value;
				}
			}else{
				$return = $value;
			}

			return number_format($return,2,'.',',');
		}
	}
}

function age($dob,$to = FALSE){
        $dob = date("Y-m-d",strtotime($dob));
        $dobObject = new DateTime($dob);
		
		if($to !== FALSE){
			$from = date("Y-m-d",strtotime($to));
			$nowObject = new DateTime($from);
			$diff = $dobObject->diff($nowObject);
		}else{
			$nowObject = new DateTime();
			$diff = $dobObject->diff($nowObject);
		}
        
        return $diff->y;
}

function positive_val($value=0){
	$ret = floatval($value);

	if($ret < 0){
		return 0;
	}

	return $ret;
}
