$(function(){

	$('.fontawesome-dropdown-li-a').on('click',function(){

		var fa_value = $(this).attr('value');
		var fa_cap = $(this).attr('caption');
		var txtbox = $(this).parents('td').find('input[type="hidden"]');
		var cap = $(this).parents('td').find('span.fontawesome-dropdown-caption');
		if(txtbox){
			txtbox.val(fa_value);
		}
		if(cap){
			cap.html('<span class="'+fa_value+'"></span> &nbsp; '+fa_cap);
		}
	})

})