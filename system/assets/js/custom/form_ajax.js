$(document).ready(function() {

	var ajaxQueueArray = [];
	var load = $('div#tobi-clone').html();
	var edit_loader = '<span class="color-info glyphicon glyphicon-pencil"></span>';
	var remove_loader = '<span class="color-danger glyphicon glyphicon-remove"></span>';
	var ok_loader = '<span class="color-success glyphicon glyphicon-ok"></span>';
	var denied_loader = '<span class="color-danger fa fa-warning"></span>';

	(function($) {

	// jQuery on an empty object, we are going to use this as our Queue
	var ajaxQueue = $({});

	$.ajaxQueue = function( ajaxOpts ) {
		var jqXHR,
			dfd = $.Deferred(),
			promise = dfd.promise();

		// run the actual query
		function doRequest( next ) {
			jqXHR = $.ajax( ajaxOpts );
			jqXHR.done( dfd.resolve )
				.fail( dfd.reject )
				.then( next, next );
		}

		// queue our ajax request
		ajaxQueue.queue( doRequest );

		// add the abort method
		promise.abort = function( statusText ) {

			// proxy abort to the jqXHR if it is active
			if ( jqXHR ) {
				return jqXHR.abort( statusText );
			}

			// if there wasn't already a jqXHR we need to remove from queue
			var queue = ajaxQueue.queue(),
				index = $.inArray( doRequest, queue );

			if ( index > -1 ) {
				queue.splice( index, 1 );
			}

			// and then reject the deferred
			dfd.rejectWith( ajaxOpts.context || ajaxOpts, [ promise, statusText, "" ] );
			return promise;
		};

		return promise;
	};

	})(jQuery);
	
	var run = {
		init:function(e,key){

			if(typeof e !== "undefined" || status == 'unprocessed'){
				jQuery.ajaxQueue({
					type: "POST",
					url: e.attr('url'),
					data:{ option: e.val() }
				}).done(function(output){
					if(output == true){

						//e.siblings('span.loading').html('<i class="icon-ok"></i>');
						e.parents('.click-me-parent').find('span.loader').html(ok_loader);

						ajaxQueueArray[key] = {status : 'processed'};
					}else{
						e.parents('.click-me-parent').find('span.loader').html(remove_loader);
					}
				}).fail(function(){
						e.parents('.click-me-parent').find('span.loader').html(denied_loader);
				});
			}
		},
		holder: function(e){
			var _holder ={
					status : 'unprocessed',
					ajaxObject : e
			};
			
			ajaxQueueArray.push(_holder);
		}
	};

	var run_delete = {
		init:function(e,key,del){

			if(typeof e !== "undefined" || status == 'unprocessed'){
				jQuery.ajaxQueue({
					type: "POST",
					url: e.attr('url'),
					data:{ option: e.val() }
				}).done(function(output){
					if(output == true){
						
						jnotice('Success', 'Record was deleted.');
						e.parents('.'+e.attr('parent')).hide('slow');

						ajaxQueueArray[key] = {status : 'processed'};
					}else{
						e.parents('.click-me-parent').find('span.loader').html(remove_loader);
					}
				}).fail(function(){
						e.parents('.click-me-parent').find('span.loader').html(denied_loader);
				});
			}
		},
		holder: function(e){
			var _holder ={
					status : 'unprocessed',
					ajaxObject : e
			};
			
			ajaxQueueArray.push(_holder);
		}
	};
	
	function isInt(n) {
		return n % 1 === 0;
	} 

	
	/** Changed the view to edit form
		 ### Must Class
		 * click-me-parent - parent div of input & view
		 * edit-me-form - div of the input
		 * view-me-form - div of the view
		 * span.loader - span for loading
		 */

	$('.click-me-link').on('click', function(){
		$(this).parents('.click-me-parent').find('.edit-me-form').show();
		$(this).parents('.click-me-parent').find('.view-me-form').hide();
		$(this).parents('.click-me-parent').find('.edit-me-input').focus();
		
		$(this).parents('.click-me-parent').find('span.loader').html(edit_loader);
	})

	//prevent
	$('.edit-me-input').keydown(function(event){
    if(event.keyCode == 13) {
      event.preventDefault();
      $(this).blur();
    }
  });

	//focus out the input field - save and hide
	$('.edit-me-input').on('focusout', function(){

		/**
		 * Input Must Attributes
		 * url - ajax url
		 * value - value to be save
		 * org - original value
		 * minlen - mininum character of value
		 */

		var xinput = $(this); // Input HTML
		var xajax_url = xinput.attr('url');
		var xvalue = xinput.val().trim();
		var xorig = xinput.attr("org"); //Original Value
		var xminlen = (typeof xinput.attr('minlen') != 'undefined') ? parseFloat( xinput.attr('minlen') ) : 0 ; //mininum characters
		
		if(xvalue.length >= xminlen){

			if(xvalue != xorig){ 
				
				$(this).parents('.click-me-parent').find('span.loader').html(load);
				
				//create a holder for objects
				ajaxQueueArray = [];
				run.holder($(this));
				
				// console.log(ajaxQueue);
				//loop
				$.each(ajaxQueueArray,function(i,e)
				{	
					if(e.status == 'unprocessed')
					{
						run.init(e.ajaxObject,i);
					}
				});
			}
		}else{
			$(this).parents('.click-me-parent').find('span.loader').html(remove_loader);	
		}


		// $(this).parents('.click-me-parent').find('.edit-me-form').hide();
		// $(this).parents('.click-me-parent').find('.view-me-form').show();
		// $(this).parents('.click-me-parent').find('span.loader').html(load);
	})

	/**
	 * Catch Delete Link
	 */
	$('.delete-me-link').on('click', function(e){
		e.preventDefault();
		var parent = $(this).attr('parent');
		
		$(this).parents('.'+parent).find('span.loader').html(load);

		//create a holder for objects
		ajaxQueueArray = [];
		run_delete.holder($(this));
		
		// console.log(ajaxQueue);
		//loop
		$.each(ajaxQueueArray,function(i,e)
		{	
			if(e.status == 'unprocessed')
			{
				run_delete.init(e.ajaxObject,i);
			}
		});

	})

});