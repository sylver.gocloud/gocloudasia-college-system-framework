$(document).ready(function() { 
	
	// ----- Tool tip -----
		$('.tp').tooltip();

		$('.menu_tp').tooltip({
			'animation' : true,
			'delay' : { 'show': 500, 'hide': 100 },
			'container':'body'
		});

	// ----- Focus to specifiv div id when page load -----
		if ($('#focusme').length > 0) {
			$('html, body').animate({ scrollTop: $('#focusme').offset().top }, 'slow');
		}

  // ----- TURN ALL SUBMIT SAVE BUTTON TO BLUE-primary and Search - gray-default -----
	  	$('input[type="submit"]').each(function(){
	  		var xval = $(this).val();
	  		
	  		if(xval.search('Search') == 0){
				$(this).removeClass('btn-success');
	  			$(this).addClass('btn-success btn-sm');  			
	  		}else{
	  			$(this).removeClass('btn-success');
	  			$(this).removeClass('btn-default');
	  			$(this).addClass('btn-primary btn-sm');
	  		}
	  	});

  // ----- TURN ALL BUTTONS WITH CLASS OF btn-add TO BLUE
  	$('.btn-add').addClass('btn btn-sm btn-primary');

  // ----- highlight all required fields
	  	$('input,select').each(function(){
	  		if($(this).attr('required')){

	  			if($(this).val() == ""){
	  				$(this).css('border','1px dotted red');
	  			}

	  			$(this).on('blur', function(){
			  		if($(this).val() != ""){
			  			$(this).css('border','1px solid gray');
			  		}else{
			  			$(this).css('border','1px dotted red');
			  		}
			  	})
	  		}
	  	})

	// ----- Remove System message when x click
		$('.sys_m_close').click(function(){
			$(this).parent().hide('slow');
		})

	// ----- Dynamic Custom Modal
		$('.custom-modal').on('click',function(){
			var modal_id = $(this).attr('modal_id');
			var modal_title = $(this).attr('modal_title');
			if(modal_id){
				modal_title = modal_title ? modal_title : 'Modal';
				if($('#'+modal_id)){
					custom_modal(modal_title, $('#'+modal_id).html());
				}
			}
		})

	// ----- Dynamic Bootstrap Custom Modal
		$('.custom-bs-modal').on('click',function(){
			var modal_id = $(this).attr('modal-id');
			var modal_title = $(this).attr('modal-title');
			var error = $(this).attr('modal-type') ? $(this).attr('modal-type') : 'primary';
			var xsize = $(this).attr('modal-size') ? $(this).attr('modal-size') : 'normal';
			if(modal_id){
				modal_title = modal_title ? modal_title : 'Modal';
				if($('#'+modal_id)){
					bsAlert(modal_title, clean_html($('#'+modal_id).html()), error,xsize)
				}
			}
		})

	/** Colapsible Panel **/
    $(document).on('click', '.panel-heading span.clickable', function(e){
	    var $this = $(this);
		if(!$this.hasClass('panel-collapsed')) {
			$this.parents('.panel').find('.panel-body').slideUp();
			$this.addClass('panel-collapsed');
			$this.find('i').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
		} else {
			$this.parents('.panel').find('.panel-body').slideDown();
			$this.removeClass('panel-collapsed');
			$this.find('i').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
		}
	})

  /** Put SOrt Icon for table-sortable*/
  	$('.table-sortable>thead>tr>th').each(function(){

  		$(this).css('cursor','pointer');

  		if($('.table-sortable').attr('no-sort-icon')){
  		}else{
  			$(this).append("<div class='pull-right'><span class='fa fa-unsorted'></div>");
  		}
  	})
  	//make it sortable
  	$(".table-sortable").each(function(){
			var x = $(this).find("tbody").find("tr").length;
			if(x >= 1){
				$(this).tablesorter(); 
			}
		})
    

	// Animate Department Icon
		$('.welcome_icon').hover(function () {
	    $(this).toggleClass("animated pulse");
	 });
     

    refresh_deletion();
    
    // --- Hide Image ---
    $( '#ajaxLoadAni' ).fadeOut( 'slow' );
    
    // --- Calendar Function ---
    $( ".datepicker" ).datepicker({
            changeMonth: true,
            changeYear: true,
            yearRange:"1970:y"
    });
	
	// ----- Date Picker 
		$( ".date_pick" ).datepicker({
            changeMonth: true,
            changeYear: true,
            yearRange:"1970:y",
			dateFormat:'yy-mm-dd'
    });

    $( ".timeonly" ).datepicker({
			dateFormat:'yy-mm-dd'
    });
    
    $( ".datepicker2" ).datepicker({
            changeMonth: true,
            changeYear: true,
            minDate: "-15y",
            maxDate: "y"
    });
	
    $( ".datepicker-payment" ).datepicker({
		changeMonth: true
    });

    $( ".datepicker_books" ).datepicker({
      changeMonth: true,
      changeYear: true,
	   yearRange: "-40:+0"
    });	
	
    
		$( ".datepicker3" ).datepicker({
            changeMonth: true,
            changeYear: true,
        });
	
    // --- Accordion Function ---
    $('.accordion h3.head').click(function() {
        $(this).next().slideToggle('slow');
        return false;
    })
    $('.accordion h3.head2').click(function() {
        $(this).prev().slideToggle('slow');
        return false;
    });
    
    // --- Student Stat ---
    $( '.studentStat' ).click(function(){
        var a = $('input[name=student_status]:checked').val();
        if(a=='new')
        {
            $('input[name=student_id]').val('');
            $('#studentId').css({ "display" : "none" });
        }
        else
        {
            $('#studentId').css({ "display" : "block" });
        }
    });
	
	// ----- Puts Red Border Color on required fields
		$('.not_blank').each(function(){
			
			var xdata = $(this).val().trim();
			
			if(xdata == "")
			{
				$(this).css('border-color','red');
			}
			
			$(this).focusout(function(){
				
				var xdata = $(this).val().trim();
				
				if(xdata != ""){
					$(this).css('border-color','gray');
				}else{
					
					$(this).css('border-color','red');
				}
			});
		});
	
		$('.is_number').each(function(){
		
		var xdata = $(this).val().trim();
		
		if(xdata == "")
		{
			$(this).css('border-color','red');
		}
		
		$(this).focusout(function(){
			
			var xdata = $(this).val().trim();
			
			if(xdata != ""){
				$(this).css('border-color','gray');
			}else{
				
				$(this).css('border-color','red');
			}
		});
		});

    
  // --- Modal ---
    $( '#msgDialog' ).dialog({
        autoOpen: false,
        
        buttons: {
            'Ok': function() {
                $( this ).dialog( 'close' );
            }
        }
    });
    
  // --- Tab ---
    $('#tabs').tabs();
    
  // --- Get The Base URL ---
    var baseurl = $('#baseurl').val();    
    
  // --- Confirm Deletion ---
		$('a.confirm').click(function(e){
	      e.preventDefault();
	      var xelement = $(this);
	      var url = $(this).attr('href');
	      var title = $(this).attr('title');
	      var click_function = $(this).attr('click_function') != null ? $(this).attr('click_function') : false;
	      var is_redirect = $(this).attr('is_redirect') != null && $(this).attr('is_redirect') == 'yes' ? true : false;

	      $('#myModal #confirm').unbind();
	      $('#myModal').modal({
	      	'show' : true,
	      	'backdrop' : 'static',
	      	'keyboard' : false
	      });

		   	if(title != "" && title != null){ $('#myModal .modal-body').html('<p>'+title+'</p>'); }

	    	$('#myModal #confirm').click(function(){
	    		// $(this).attr('disabled', true);
	    		// $('#myModal #closemodal').attr('disabled', true);
	    		// $('#myModal #xclosemodal').attr('disabled', true);
	      	if(is_redirect)
	      	{
	      		//if attr is_redirect if fount on the link it will go directy to the link and will not call any function
	      		window.location = url;
	      	}else
	      	{

	      		if(click_function == false){
		      		window.location = url;
		      	}else{
		      		//IF attr 'click_function' is found on the link it will call a function which name is value of the click_function attr
		      		window[click_function](url, xelement);
		      	}
					}
	    	});
		});
  
    $('a.confirm-auto').click(function(e){
      e.preventDefault();
      var url = $(this).attr('href');
      $('#myModal').modal('show');
      $('#myModal #confirm').click(function(){
        window.location = url;
      });
    });
	
		$('.confirm-auto2').click(function(e){
      e.preventDefault();
      var url = $(this).attr('href');
      $('#myModal').modal('show');
      $('#myModal #confirm').click(function(){
        window.location = url;
      });
    });
	
	// CHECK ALL CHECKBOXES
		$('.check_all').click(function(){
			
			var id = $(this).attr('id');
			
			if($(this).is(':checked')){
				$('.'+id).attr('checked', 'checked');
			}
			else if($(this).not(':checked')){
				$('.'+id).removeAttr('checked');
			}
		});
	
	// ---- Time Picker ----
		// $('.time').timepicker({
		// 		timeFormat: 'h:mm'
		// });

	// Dynamic Check All Checkbox
		$('.checkall-checkbox').on('click', function(){
			var x_class = $(this).attr('checkbox-class');
			$('.'+ x_class + ' input[type="checkbox"]').prop('checked', true);
		});

	// Dynamic UnCheck All Checkbox
		$('.uncheckall-checkbox').on('click', function(){
			var x_class = $(this).attr('checkbox-class');
			$('.'+ x_class + ' input[type="checkbox"]').prop('checked', false);
		});
	
	// ----- Number Inputs -----
		$('.currency').number( true, 2,'.', '' ); 
		
		$('input.numeric').live('keyup', function(e) {
		  $(this).val($(this).val().replace(/[^0-9]/g, ''));
		});

		$(".decimalonly").keypress(function(event) {
	    // Backspace, tab, enter, end, home, left, right,decimal(.)in number part, decimal(.) in alphabet
		  // We don't support the del key in Opera because del == . == 46.
		  var controlKeys = [8, 9, 13, 35, 36, 37, 39,110,190];
		  // IE doesn't support indexOf
		  var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		  // Some browsers just don't raise events for control keys. Easy.
		  // e.g. Safari backspace.
		  if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
		      (49 <= event.which && event.which <= 57) || // Always 1 through 9
		      (96 <= event.which && event.which <= 106) || // Always 1 through 9 from number section 
		      (48 == event.which && $(this).attr("value")) || // No 0 first digit
		      (96 == event.which && $(this).attr("value")) || // No 0 first digit from number section
		      isControlKey) { // Opera assigns values for control keys.
		    return;
		  } else {
		    event.preventDefault();
		  }
		});
	
	// ----- Please Wait When Ajax is Fired -----
		$(document).ajaxStart(function(){
			run_pleasewait();
		}).ajaxStop($.unblockUI);

	// ----- DOCK BUTTON -----
		$('#dock-button').on("click", function(){
			var status = $(this).attr('hide');
			var baseurl = $(this).attr('base_url');
			if(status == 1){
				$('.menu-container').removeClass('hidden-xs');
				$('.menu-container').hide('slow');
				$(this).attr('hide','0');
				$(this).attr('title','Show Menus');
				$(this).attr('data-original-title','Show Menus');
				$('.dock-fa-icon').removeClass('fa fa-chevron-left');
				$('.dock-fa-icon').addClass('fa fa-chevron-right');
				$('.yield-container').removeClass('col-md-10 col-sm-7');
				$('.yield-container').addClass('col-md-12 col-sm-9');
				update_dock_status(baseurl, 0);
			}else{
				$('.menu-container').show('show');
				$('.menu-container').addClass('hidden-xs');
				$(this).attr('hide','1');
				$(this).attr('title','Hide Menus');
				$(this).attr('data-original-title','Hide Menus');
				$('.dock-fa-icon').removeClass('hide');
				$('.dock-fa-icon').removeClass('fa fa-chevron-right');
				$('.dock-fa-icon').addClass('fa fa-chevron-left');
				$('.yield-container').removeClass('col-md-12 col-sm-9');
				$('.yield-container').addClass('col-md-10 col-sm-7');
				update_dock_status(baseurl, 1);
			}
		})

	// ----- COLLAPSE BUTTON -----
		$('#colap-button').on("click", function(){
			var status = $(this).attr('hide');
			var baseurl = $(this).attr('base_url');

			$.ajax({
				url : baseurl+'ajax/colap_menu',
				type : 'POST',
				data : {to_show:status},
				dataType : 'json',
				success : function (data) {
					window.location.href = window.location.href;
				}
			});
		})

	// ----- DOCK BUTTON for New Layout (3)
		var x = $('span#sidebarhider>a');
		if(x.attr("value") == "0"){
			$('.sidebar-desktop').removeClass('animated bounceOutLeft bounceInLeft');
			$('.sidebar-desktop').addClass('animated bounceOutLeft');
			$('#page-wrapper').css('margin', '0 0 0 0px');
		}
		
		$('span#sidebarhider>a').on('click', function(){
			
			var x = $(this);
			if(x.attr("value") == "1"){
				//hide
				$('.sidebar-desktop').removeClass('animated bounceOutLeft bounceInLeft');
				$(x).text("Show Sidebar");

				$('.sidebar-desktop').addClass('animated bounceOutLeft');
				$('#page-wrapper').css('margin', '0 0 0 0px');
				$(x).attr('value','0');
				update_dock_status(baseurl, 0);

			}else{
				//show
				$('.sidebar-desktop').removeClass('animated bounceOutLeft bounceInLeft');

				$('.sidebar-desktop').addClass('animated bounceInLeft');
				$('#page-wrapper').css('margin', '0 0 0 250px');
				$(x).text("Hide Sidebar");
				$(x).attr('value','1');
				update_dock_status(baseurl, 1);
			}
		})

	// ----- Clear Form BUtton -----
		$('.clear-form').on('click', function(){
			var form = $(this).parents('form');
			$(form).find('input[type="text"]').val('');
		})
  
	// ----- Confirm Action When a form is submitted
		// Downside Warning - the submit button will be added to the post, still searching for solution
		// Downside Warning - use only for single button submit, create alternative if you have to catch value of the buttons
		// solution based here : http://stackoverflow.com/questions/14985537/solved-prevent-submission-of-form-until-specified-button-clicked-thickbox-dia
		$('form.confirm').on('submit', function(event, submit){
      var xfrm = $(this);
      var xtitle = $(this).attr('title');
      xtitle = xtitle ? xtitle : 'Are you sure to continue?';
      
      if (!submit){
      	if(xfrm.valid()){
      		event.preventDefault();
	        bsConfirm(xtitle).then(function (a) {
	          if (a) {
	           xfrm.trigger('submit', [true]); 	
	          }
	        });
      	}
      	else{
      		event.preventDefault();
      		bsAlert('Required Fields','Some required fields need to be filled up.');
      	}
      }
    });

	/**************************************************************************************
		Grid like input , can be focus to next and prev (table only) using keyboard arrow key
		***************************************************************************************/
		$("input.grid").keyup( function (e) {
			var xval = $(this).val().trim();
	    switch(e.which)
	    {
	        // left arrow
	        case 37:
	        	var pass = false;
	        	if(xval == ""){
	        		pass = true;
	        	}else{
	        		if(is_cursor_at($(this))){
	        			pass = true;
	        		}
	        	}

	        	if(pass){
	            $(this).parent()
	                    .prev()
	                    .children("input.grid")
	                    .focus();
	          }
	          break;

	        // right arrow
	        case 39:
	        	var pass = false;
	        	if(xval == ""){
	        		pass = true;
	        	}else{
	        		if(is_cursor_at($(this),'end')){
	        			pass = true;
	        		}
	        	}
	        	if(pass){
	            $(this).parent()
	                    .next()
	                    .children("input.grid")
	                    .focus();
	          }
	          break;

	        // down arrow
	        case 40:
	        		var x = $(this).parents('td');
	        		var $this = x;
	            var cellIndex = $this.index();
	            var tr = $this.closest('tr').next().children().eq(cellIndex).find('input.grid').focus();
	            break;

	        // up arrow
	        case 38:
	        		var x = $(this).parents('td');
	        		var $this = x;
	            var cellIndex = $this.index();
	            var tr = $this.closest('tr').prev().children().eq(cellIndex).find('input.grid').focus();
	            break;
	    }
	  });

	buttons_for_dynamic_modal();

});// --- end document ready ---

/* Update Dock Status in Session - purpose : to save either the menu will be hidden or not during page load */
function update_dock_status(base_url, status){
	$.ajax({
		url : base_url+'ajax/update_dock_status',
		type : 'POST',
		data : {to_show:status},
		dataType : 'json',
		success : function (data) {
			// window.location.href = window.location.href;
		}
	});
}

function notify_modal(title, content){
  $("#notify-modal").modal('toggle');
  $("#notify-modal #notify-modal-title").text(title);
   $("#notify-modal #notify-modal-content").html(content);
}

/**
 * Create Bootstrap Modal on the fly
 * @param  {string} title 
 * @param  {html} html  Message on the body, can be html
 * @param  {[type]} type  success,danger,primary,warning,info
 */
function custom_modal(title, html, type)
{
	if(type){
		$('#alertModal').addClass('bootstrap-dialog type-'+type);	
	}

	$('#alertModal_Body').html(''); 
	$('#alertModal_Label').text(title); //SET MODAL TITLE
	$('#alertModal_Body').html(html); //SET MODAL CONTENT
	$('#alertModal').modal('show'); //SHOW MODAL
}

function refresh_deletion(){
	$('a.delete_subject').unbind();
	$('a.delete_subject').click(function(e){
		e.preventDefault();
		var url = $(this).attr('href');
		var tr = $(this).closest('tr');
	        var me = $(this); 
                me.attr('disabled', true);
		$('#myModal').modal('toggle');
		$('#confirm').unbind();
		$('#confirm').click(function(){	
		
			$('#myModal').modal('hide');
			$.ajax({
				url: url,
				type: "POST",
				dataType: 'json',
				success: function (data) {
				  if (data.status) {
					notify_modal('Note', "Can't remove subject because it contains grade. Please contact your registrar for confirmation.");
					me.removeAttr('disabled');
				  }else{
			            tr.addClass('danger');
				    tr.hide('slow');
				    $('#total_units').text(data.units);
				    $('#total_labs').text(data.labs);
				    $('#total_lec').text(data.units - data.labs);
				  }
				},
				error: function (xhr, status) {
					alert("Sorry, there was a problem!");
				},
				complete: function (xhr, status) {
					var par = url.substr(url.lastIndexOf('/') + 1);
					//DONT REFRESH PAGE
					if(par == 'nf') {}
					else{ 
						location.reload(); 
					}
				}
			});  
		});  
		
	});
}
// End User Subject Ajax
function isNumber(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

function run_pleasewait()
{
	$.blockUI({ css: { 
            border: 'none', 
            padding: '15px', 
            backgroundColor: '#000', 
            '-webkit-border-radius': '10px', 
            '-moz-border-radius': '10px', 
            opacity: .5, 
            color: '#fff' 
        },message: $('#please_wait') }); 
}

function close_pleasewait()
{
	$.unblockUI();
}

function numVal(data)
{
	if(data == "" || data == null)
	{
		return 0;
	}
	else
	{
		return parseFloat(data.replace(/[^0-9.]/g, ""));
	}
}

function custom_jmodal(xtitle, xcontent, xwd, xht)
{
	close_jmodal();
	xtitle = (xtitle) ? xtitle : 'Some Title';
	xcontent = (xcontent) ? xcontent : '';
	xwd = (xwd) ? xwd : 300;
	xht = (xht) ? xht : 300;
	
	$('#j_modal').html(xcontent);
	
	// --- Modal ---
    $( '#j_modal' ).dialog({
        autoOpen: true,
        modal: true,
		title: xtitle,
		width: xwd,
		height: xht,
		position: ['center'],
        buttons: {
            'Close': function() {
                $( this ).dialog( 'close' );
            }
        }
    });
}

function close_jmodal()
{
	$( '#j_modal' ).dialog( 'close' );
}

function close_mymodal(xid)
{
	if(xid == null)
	{
		$('#myModal').modal('hide');
	}
	else
	{
		$('#'+xid).modal('hide');	
	}
}

function growl(title, msg)
{
	// $.growlUI(title, msg);
	
	$('.growl_title').html(title);
	$('.growl_msg').html(msg);
	
	$.blockUI({ 
            message: $('div.growl'), 
            fadeIn: 700, 
            fadeOut: 700, 
            timeout: 2000, 
            showOverlay: false, 
            centerY: false, 
            css: {

                /*
                //TOP RIGHT
                width: '350px', 
                top: '100px', 
                left: '', 
                right: '10px', 
                border: 'none', 
                padding: '5px', 
                backgroundColor: 'black', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .6, 
                color: '#fff'*/

                width: '350px',
                bottom: '10px',
                top: '-100',
                left: '',
                right: '10px',
                border: 'non',
                padding: '5px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .9,
                color: '#fff',
            } 
        }); 	
}


/*
	SLY
	JNOTIFY PLUGIN
	REFERENCE : http://www.myjqueryplugins.com/jquery-plugin/jnotify
*/
function jnotice(title, msg)
{
	 jNotify(
		'<i class="fa fa-info-circle fa-2x"></i> &nbsp; <strong>'+title+'</strong><br /><strong>'+msg+'</strong>',
		{
		  autoHide : true, // added in v2.0
		  clickOverlay : false, // added in v2.0
		  MinWidth : 250,
		  TimeShown : 2000,
		  ShowTimeEffect : 200,
		  HideTimeEffect : 200,
		  LongTrip :20,
		  HorizontalPosition : 'right',
		  VerticalPosition : 'top',
		  ShowOverlay : false,
   		ColorOverlay : 'black',
		  OpacityOverlay : 0.3,
		  onClosed : function(){ // added in v2.0
		   
		  },
		  onCompleted : function(){ // added in v2.0
		   
		  }
		});
	  
}

function str_pad(input, pad_length, pad_string, pad_type) {
  //  discuss at: http://phpjs.org/functions/str_pad/
  // original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // improved by: Michael White (http://getsprink.com)
  //    input by: Marco van Oort
  // bugfixed by: Brett Zamir (http://brett-zamir.me)
  //   example 1: str_pad('Kevin van Zonneveld', 30, '-=', 'STR_PAD_LEFT');
  //   returns 1: '-=-=-=-=-=-Kevin van Zonneveld'
  //   example 2: str_pad('Kevin van Zonneveld', 30, '-', 'STR_PAD_BOTH');
  //   returns 2: '------Kevin van Zonneveld-----'

  var half = '',
    pad_to_go;

  var str_pad_repeater = function(s, len) {
    var collect = '',
      i;

    while (collect.length < len) {
      collect += s;
    }
    collect = collect.substr(0, len);

    return collect;
  };

  input += '';
  pad_string = pad_string !== undefined ? pad_string : ' ';

  if (pad_type !== 'STR_PAD_LEFT' && pad_type !== 'STR_PAD_RIGHT' && pad_type !== 'STR_PAD_BOTH') {
    pad_type = 'STR_PAD_RIGHT';
  }
  if ((pad_to_go = pad_length - input.length) > 0) {
    if (pad_type === 'STR_PAD_LEFT') {
      input = str_pad_repeater(pad_string, pad_to_go) + input;
    } else if (pad_type === 'STR_PAD_RIGHT') {
      input = input + str_pad_repeater(pad_string, pad_to_go);
    } else if (pad_type === 'STR_PAD_BOTH') {
      half = str_pad_repeater(pad_string, Math.ceil(pad_to_go / 2));
      input = half + input + half;
      input = input.substr(0, pad_length);
    }
  }

  return input;
}

function get_day_short(xday)
{
	var ret = "";
	switch(xday.toUpperCase())
	{
		case "MONDAY":
		ret = "M";
		break;
		case "TUESDAY":
		ret = "T";
		break;
		case "WEDNESDAY":
		ret = "W";
		break;
		case "THURSDAY":
		ret = "TH";
		break;
		case "FRIDAY":
		ret = "F";
		break;
		case "SATURDAY":
		ret = "S";
		break;
		case "SUNDAY":
		ret = "SU";
		break;

	}
	return ret;
}

/* For Side Bar Menus */
function collapsemenu (el) {
	var xstatus = $(el).attr('colap');
	var xmenu_k = $(el).attr('menu_key');
	if(xstatus == '0'){
		$('#menu_'+xmenu_k).hide('slow');
		$(el).attr('colap',1);
	}else{
		$('#menu_'+xmenu_k).show('slow');
		$(el).attr('colap',0);
	}
}

// Ref : http://bootstrap-growl.remabledesigns.com/
function bs_growl (xtitle,xmsg,xtype,xicon,xplacement) {

    xicon = xicon == null ? 'fa fa-check' : xicon;
    xtype = xtype == null ? 'info' : xtype;
    
    //alignment or placement
    xplace = {from:"top", align:"right"};
    if(xplacement && typeof xplacement.from != 'undefined'){
        xplace.from = xplacement.from;
    }if(xplacement &&  typeof xplacement.align != "undefined"){
        xplace.align = xplacement.align;
    }

    $.growl({
        icon: xicon,
        title: " " + xtitle + "  ",
        message: " " + xmsg,
        //url: 'https://github.com/mouse0270/bootstrap-growl'
    },{
        element: 'body',
        type: xtype,
        allow_dismiss: true,
        placement: {
            from: xplace.from,
            align: xplace.align
        },
        offset: 20,
        spacing: 10,
        z_index: 1031,
        delay: 5000,
        timer: 1000,
        url_target: '_blank',
        mouse_over: false,
        animate: {
            enter: 'animated zoomInDown',
            exit: 'animated zoomOutUp'
        },
        icon_type: 'class',
    });
}

function focusdiv (divid) {
    if ($('#'+divid).length > 0) {
        $('html, body').animate({ scrollTop: $('#'+divid).offset().top }, 'slow');
    }
}

/**
 * Confirm Dialog
 * Ref : http://nakupanda.github.io/bootstrap3-dialog/#examples
 */

function bsConfirm(question) {
    var defer = $.Deferred();

    BootstrapDialog.show({
        type: BootstrapDialog.TYPE_PRIMARY,
        title: 'Confirmation',
        message: question,
        closeByBackdrop: false,
        closeByKeyboard: false,
        draggable: true,
        buttons: [{
            label: 'Yes',
            action: function (dialog) {
                defer.resolve(true);
                dialog.close();
            }
        }, {
            label: 'No',
            action: function (dialog) {
                defer.resolve(false);
                dialog.close();
            }
        }],
        close: function (dialog) {
            dialog.remove();
        }
    });
    return defer.promise();
}

function bsAlert(title, message, error,xsize) {
    BootstrapDialog.show({
        //type: error ? BootstrapDialog.TYPE_DANGER : BootstrapDialog.TYPE_SUCCESS,
        type: error ? 'type-'+error : BootstrapDialog.TYPE_DANGER, //type-primary, type-danger, type-success etc
        size: xsize ? 'size-'+xsize : BootstrapDialog.SIZE_NORMAL,
        title: title ? title : "Alert",
        message: message,
        closeByBackdrop: false,
        closeByKeyboard: false,
        draggable: true,
        buttons: [{
            label: 'OK',
            action: function (d) {
                d.close();
            }
        }]
    });
}


/**
 * Example of how to use the bsConfirm function
 */
function bstest () {
    
    bsConfirm("Are you sure Bootstrap is what you wanted?").then(function (a) {
    
    if (a) {
        bsAlert("success","Well done! You have made the right choice");
    } else {
        bsAlert("danger","I don't like you!");
    }
});
}

/**
 * Bootstrap Modal
 * display modal when button click
 * @param  {string} title   Title of the Modal
 * @param  {string} message B
 * @param  {[type]} error   [description]
 * @param  {[type]} xsize   [description]
 * @return {[type]}         [description]
 */
function buttons_for_dynamic_modal() {

	$(document).on('click','.button-modal', function(){
		var x = $(this);
		var title = x.attr('modal-title');
		var message_id = x.attr('modal-id');
		var html = clean_html($('#'+message_id).html());
		var error = x.attr('modal-type');
		var xsize = x.attr('modal-size');

		BootstrapDialog.show({
        //type: error ? BootstrapDialog.TYPE_DANGER : BootstrapDialog.TYPE_SUCCESS,
        type: error ? 'type-'+error : BootstrapDialog.TYPE_PRIMARY, //type-primary, type-danger, type-success etc
        size: xsize ? 'size-'+xsize : BootstrapDialog.SIZE_NORMAL,
        title: title ? title : "Alert",
        message: html,
        closeByBackdrop: false,
        closeByKeyboard: false,
        draggable: true,
        buttons: [{
            label: 'OK',
            action: function (d) {
                d.close();
            }
        }]
    });
	})
}

/** FOR ANIMATE.CSS Put Click function*/
function animationClick(element, animation){
  element = $(element);
  element.click(
    function() {
      element.addClass('animated ' + animation);
      //wait for animation to finish before removing classes
      window.setTimeout( function(){
          element.removeClass('animated ' + animation);
      }, 2000);
    }
  );
};

/** FOR ANIMATE>CSS*/
function animateMe(element, animation){
    element = $(element);
    element.addClass('animated ' + animation);
    //wait for animation to finish before removing classes
    window.setTimeout( function(){
        element.removeClass('animated ' + animation);
    }, 2000);
}

/**
 * Convert 24 hour to 12 hour format
 */
function convert_time(date) {
    var d = new Date(date);
    var hh = d.getHours();
    var m = d.getMinutes();
    var s = d.getSeconds();
    var dd = "AM";
    var h = hh;
    if (h >= 12) {
        h = hh-12;
        dd = "PM";
    }
    if (h == 0) {
        h = 12;
    }
    m = m<10?"0"+m:m;

    s = s<10?"0"+s:s;

    /* if you want 2 digit hours:
    h = h<10?"0"+h:h; */

    var pattern = new RegExp("0?"+hh+":"+m+":"+s);

    var replacement = h+":"+m;
    /* if you want to add seconds
    replacement += ":"+s;  */
    replacement += " "+dd;    

    return date.replace(pattern,replacement);
}

/**
 * Clean Html Spaces
 */
function clean_html(xmsg) {
	return xmsg.replace(/(\r\n|\n|\r)/gm," ");
}

/**
 * Check Cursor if at the start of input
 * @param  {object}  el  Jquery Input Element ($('#el_id'))
 * @param  {string}  pos start or end
 * @return {Boolean}
 */
function is_cursor_at (el, pos) {
	var textInput = $(el)[0];
	var val = $(el).val();
	var isAtStart = false, isAtEnd = false;
	
	if (typeof textInput.selectionStart == "number") {
	    // Non-IE browsers
	    isAtStart = (textInput.selectionStart == 0);
	    isAtEnd = (textInput.selectionEnd == val.length);
	} else if (document.selection && document.selection.createRange) {
	    // IE branch
	    textInput.focus();
	    var selRange = document.selection.createRange();
	    var inputRange = textInput.createTextRange();
	    var inputSelRange = inputRange.duplicate();
	    inputSelRange.moveToBookmark(selRange.getBookmark());
	    isAtStart = inputSelRange.compareEndPoints("StartToStart", inputRange) == 0;
	    isAtEnd = inputSelRange.compareEndPoints("EndToEnd", inputRange) == 0;
	}
	
	if(pos == "end"){
		return isAtEnd
	}
	return isAtStart
}

function doGetCaretPosition (oField) {

  // Initialize
  var iCaretPos = 0;

  // IE Support
  if (document.selection) {

    // Set focus on the element
    oField.focus ();

    // To get cursor position, get empty selection range
    var oSel = document.selection.createRange ();

    // Move selection start to 0 position
    oSel.moveStart ('character', -oField.value.length);

    // The caret position is selection length
    iCaretPos = oSel.text.length;
  }

  // Firefox support
  else if (oField.selectionStart || oField.selectionStart == '0')
    iCaretPos = oField.selectionStart;

  // Return results
  return (iCaretPos);
}