$(function(){

  $("table").addClass("table ");
  $('input[type=text]').addClass('form-control');
  $('select').addClass('form-control');
  $('input[type=textarea]').addClass('form-control');
  $('input[type=password]').addClass('form-control');
  $('input[type=submit]').addClass('btn btn-success');
});
